package data

import (
	"encoding/json"
	"io/ioutil"
	"reflect"
	"strings"
	"sync"

	. "gitee.com/tomatomeatman/golang-repository/bricks/model"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/file"
	Log "github.com/cihub/seelog"
)

type CacheTxtUtil struct{}

var (
	CacheTxtUtilLock sync.Mutex //同步锁
)

/**
 * 创建缓存文件
 * @param object 待存储数据对象
 * @param sCacheFile 缓存文件路径及文件名
 * @return
 */
func (this CacheTxtUtil) CreateCacheFile(object interface{}, sCacheFile string) *MsgEmity {
	if strings.HasPrefix(reflect.TypeOf(object).String(), "[]") {
		return this.CreateCacheFileByList(object.([]interface{}), sCacheFile)
	}

	list := []interface{}{object}
	return this.CreateCacheFileByList(list, sCacheFile)
}

/**
 * 创建缓存文件
 * @param list 数据集合
 * @param sCacheFile 缓存文件路径及文件名
 * @return
 */
func (this CacheTxtUtil) CreateCacheFileByList(list []interface{}, sCacheFile string) *MsgEmity {
	if len(list) < 1 {
		return MsgEmity{}.Err(1001, "没有数据需要缓存！")
	}

	ret_json, _ := json.Marshal(list)
	txt := string(ret_json)
	path := sCacheFile + ".tmp." + SequenceUtil{}.Get() //临时文件

	CacheTxtUtilLock.Lock() //加锁
	FileUtil{}.Save(txt, path)
	CacheTxtUtilLock.Unlock() //解锁

	return MsgEmity{}.Success(1999, "转换成功")
}

// /**
//  * 读取缓存文件
//  * @param clazz 缓存内数据对象的格式
//  * @param sCacheFile 缓存文件路径及文件名
//  * @return
//  */
// func (this CacheTxtUtil) ReadCacheFile(clazz interface{}, sCacheFile ...string) []interface{} {
// 	if len(sCacheFile) < 1 {
// 		return make([]interface{}, 0)
// 	}

// 	path := sCacheFile[0]
// 	if (!FileUtil{}.IsExist(path)) {
// 		return make([]interface{}, 0)
// 	}

// 	text, err := ioutil.ReadFile(path)
// 	if err != nil {
// 		Log.Error("读取文件异常:", err)
// 		return make([]interface{}, 0)
// 	}

// 	result := []interface{}{}

// 	err = json.Unmarshal([]byte(text), &result)
// 	if err != nil {
// 		Log.Error("Json字符串转换异常: %+v\n", err)
// 		return result
// 	}

// 	return result
// }

/**
 * 读取缓存文件
 * @param sCacheFile 缓存文件路径及文件名
 * @return
 */
func (this CacheTxtUtil) ReadCacheFile(sCacheFile ...string) []interface{} {
	if len(sCacheFile) < 1 {
		return make([]interface{}, 0)
	}

	path := sCacheFile[0]
	if (!FileUtil{}.IsExist(path)) {
		return make([]interface{}, 0)
	}

	text, err := ioutil.ReadFile(path)
	if err != nil {
		Log.Error("读取文件异常:", err)
		return make([]interface{}, 0)
	}

	result := []interface{}{}

	err = json.Unmarshal([]byte(text), &result)
	if err != nil {
		Log.Error("Json字符串转换异常: %+v\n", err)
		return result
	}

	return result
}

/**
 * 删除缓存文件
 * @param sCacheFile
 */
func (this CacheTxtUtil) DelCacheFile(sCacheFile string) *MsgEmity {
	ok, _ := FileUtil{}.Del(sCacheFile)
	if !ok {
		return MsgEmity{}.Err(1000, "删除失败！")
	}

	return MsgEmity{}.Success(1999, "删除成功！")
}
