package data

import (
	"fmt"
	"strconv"
	"unsafe"
)

type IntegerUtil struct{}

//func (iu IntegerUtil) ToInt(str string, iDefault int) int {
//	if str == "" { //字符串不能判断nil
//		return iDefault
//	}

//	result, err := strconv.Atoi(str)
//	if err != nil {
//		return iDefault
//	}

//	return result
//}

//func (iu IntegerUtil) ToInt64(str string, iDefault int64) int64 {
//	if str == "" { //字符串不能判断nil
//		return iDefault
//	}

//	result, err := strconv.ParseInt(str, 10, 64)
//	if err != nil {
//		return iDefault
//	}

//	return result
//}

//对象(字符串)转整型
func (iu IntegerUtil) ToInt(obj interface{}, iDefault ...int) int {
	var str string
	switch obj.(type) {
	case []uint8:
		str = *(*string)(unsafe.Pointer(&obj))
	default:
		str = fmt.Sprintf("%v", obj)
	}

	if str == "" { //字符串不能判断nil
		if len(iDefault) < 1 {
			return 0
		}

		return iDefault[0]
	}

	result, err := strconv.Atoi(str)
	if err != nil {
		if len(iDefault) < 1 {
			return 0
		}

		return iDefault[0]
	}

	return result
}

//对象(字符串)转64整型
func (iu IntegerUtil) ToInt64(obj interface{}, iDefault int64) int64 {
	var str string
	switch obj.(type) {
	case []uint8:
		str = iu.Byte2Str(obj.([]uint8))
	default:
		str = fmt.Sprintf("%v", obj)
	}

	if str == "" { //字符串不能判断nil
		return iDefault
	}

	result, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		return iDefault
	}

	return result
}

//取int最大值
func (iu IntegerUtil) MaxInt() int {
	//二进制的首位为符号位，0表示正数，1表示负数。因此对uint(0)按位求反，再向右移动一位（将首位的1右移后首位变为0），得到最大值
	return int(^uint(0) >> 1)
}

//取int最小值
func (iu IntegerUtil) MinInt() int {
	//二进制的首位为符号位，0表示正数，1表示负数。因此对uint(0)按位求反，再向右移动一位（将首位的1右移后首位变为0），得到最大值,对最大值按位求反，其二进制首位变为1，其余位变为0，得到最小值
	return ^int(^uint(0) >> 1)
}

// Byte转Str
func (iu IntegerUtil) Byte2Str(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}
