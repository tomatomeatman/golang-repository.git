package app

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"

	. "gitee.com/tomatomeatman/golang-repository/bricks/model"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/system"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/gorm"
	Log "github.com/cihub/seelog"
	"gorm.io/gorm"
)

var (
	recordKeyJamCommonDao = "" //创建sRecordKey用的干扰串
)

type CommonDao struct{}

/**
 * 新增
 * entity 待保存实体
 * currentUser 当前用户
 */
func (dao CommonDao) Add(entity interface{}) *MsgEmity {
	// if (ReflectUtils{}.HasField(entity, "GsPid")) {
	// 	return dao.AddNode(entity, data)
	// }

	return dao.AddCommon(entity)
}

/**
 * 通用新增数据方法
 * entity 待保存实体
 * currentUser 当前用户
 */
func (dao CommonDao) AddCommon(entity interface{}) *MsgEmity {
	var dataEntity interface{}
	typeOf := reflect.TypeOf(entity)  //通过反射获取type定义
	if typeOf.Kind() == reflect.Ptr { //是否指针类型
		dataEntity = entity
	} else if "reflect.Value" == typeOf.String() {
		dataEntity = entity.(reflect.Value).Interface()
	} else {
		dataEntity = reflect.ValueOf(entity)
	}

	dbResult := SqlFactory{}.GetDB().Create(dataEntity)
	if dbResult.Error != nil {
		Log.Error("新增发生异常:", dbResult.Error)
		return MsgEmity{}.Err(8041, "新增发生异常:", dbResult.Error)
	}

	return MsgEmity{}.Success(dataEntity, "新增成功")
}

/**
 * 新增树节点
 * entity 检查用数据结构
 * data 数据
 */
func (dao CommonDao) AddNode(entity interface{}) *MsgEmity {
	var dataEntity interface{}
	typeOf := reflect.TypeOf(entity)  //通过反射获取type定义
	if typeOf.Kind() == reflect.Ptr { //是否指针类型
		dataEntity = entity
	} else if "reflect.Value" == typeOf.String() {
		dataEntity = entity.(reflect.Value).Interface()
	} else {
		dataEntity = reflect.ValueOf(entity)
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	var build strings.Builder
	build.WriteString("INSERT INTO ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" (@@@@@") //要替换'@@@@,'

	//未完
	// columns := TableInfo{}.GetColumnNames(entity)
	// for _, v := range columns {
	// 	if "/sPath/"
	// 	build.WriteString(v)

	// 	"CONCAT(IFNULL((select a.sPath from ${BricksBaseSystem}Department a where a.sId=IFNULL(#{sPid}, '00') ), '/00/'), #{sId}, '/') as sPath,"
	// }

	build.WriteString(" )")

	// params = append(params, sql.Named("iState", iState))

	// sMemo = strings.TrimSpace(sMemo)
	// if "" != sMemo {
	// 	build.WriteString(" ,sMemo=@sMemo")
	// 	params = append(params, sql.Named("sMemo", sMemo))
	// }

	// if 0 != iVersion {
	// 	build.WriteString(" ,iVersion=iVersion+1")
	// }

	dbResult := SqlFactory{}.GetDB().Create(dataEntity)
	if dbResult.Error != nil {
		Log.Error("新增发生异常:", dbResult.Error)
		return MsgEmity{}.Err(8041, "新增发生异常:", dbResult.Error)
	}

	return MsgEmity{}.Success(dataEntity, "新增成功")
}

// 批量新增
func (dao CommonDao) Adds(commons []interface{}) *MsgEmity {
	dbResult := SqlFactory{}.GetDB().Create(&commons)

	if dbResult.Error != nil {
		Log.Error("批量新增发生异常:", dbResult.Error)
		return MsgEmity{}.Err(1001, "批量新增发生异常:", dbResult.Error)
	}

	return MsgEmity{}.Success(&commons, "批量新增成功")
}

/**
 * 修改状态
 * @param entity 实体类
 * @param id 编号
 * @param iState 状态值
 * @param iVersion 记录版本号
 * @param sMemo 备注
 * @param unidirectional 是否单向 设置为单向则新值必须大于旧值才能执行
 * @return MsgEmity 返回执行情况
 */
func (dao CommonDao) ChangeState(entity interface{}, id interface{}, iState int, iVersion int, sMemo string, unidirectional bool) *MsgEmity {
	if "" == fmt.Sprintf("%v", id) {
		return MsgEmity{}.Err(7001, "记录编号参数为空！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	params := []interface{}{}
	params = append(params, sql.Named(tableInfo.GsKeyName+"Where", id))

	var build strings.Builder
	build.WriteString("UPDATE ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" SET ")
	build.WriteString(TableStateName)
	build.WriteString("=@")
	build.WriteString(TableStateName)
	params = append(params, sql.Named(TableStateName, iState))

	sMemo = strings.TrimSpace(sMemo)
	if "" != sMemo {
		build.WriteString(" ,")
		build.WriteString(TablesMemo)
		build.WriteString("=@")
		build.WriteString(TablesMemo)
		params = append(params, sql.Named(TablesMemo, sMemo))
	}

	if 0 != iVersion {
		build.WriteString(" ,")
		build.WriteString(TableVersionName)
		build.WriteString("= 1 + ")
		build.WriteString(TableVersionName)
	}

	build.WriteString(" WHERE ")
	build.WriteString(tableInfo.GsKeyName)
	build.WriteString(" = @")
	build.WriteString(tableInfo.GsKeyName)
	build.WriteString("Where")
	if 0 != iVersion {
		build.WriteString(" AND ")
		build.WriteString(TableVersionName)
		build.WriteString("=@iVersionWhere")
		params = append(params, sql.Named("iVersionWhere", iVersion))
	}

	if unidirectional {
		build.WriteString(" AND ")
		build.WriteString(TableStateName)
		build.WriteString(" < @iStateWhere")
		params = append(params, sql.Named("iStateWhere", iState))
	}

	dbResult := SqlFactory{}.GetDB().Exec(build.String(), params)
	if dbResult.Error != nil {
		Log.Error("更新状态值发生异常:", dbResult.Error)
		return MsgEmity{}.Err(7005, "更新状态值发生异常:", dbResult.Error)
	}

	if 0 < dbResult.RowsAffected {
		return MsgEmity{}.Success(7999, "更新状态值成功！")
	}

	build.Reset()
	build.WriteString("SELECT ")
	build.WriteString(tableInfo.GsKeyName)
	build.WriteString(", ")
	build.WriteString(TableStateName)

	if 0 != iVersion {
		build.WriteString(", ")
		build.WriteString(TableVersionName)
	}

	build.WriteString(" FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE ")
	build.WriteString(tableInfo.GsKeyName)
	build.WriteString(" = @")
	build.WriteString(tableInfo.GsKeyName)

	var iStateOld, iVersionOld int
	row := SqlFactory{}.Raw(build.String(), sql.Named(tableInfo.GsKeyName, id)).Row()
	row.Scan(&iStateOld, &iVersionOld)

	if 0 == iStateOld {
		return MsgEmity{}.Err(7001, "没有找到对应数据！")
	}

	if (0 != iVersion) && (iVersion != iVersionOld) {
		return MsgEmity{}.Err(7002, "更新状态值失败，系统中的数据可能已经被更新！")
	}

	if (iState >= 0) && (iState < iStateOld) {
		return MsgEmity{}.Err(7003, "更新状态值失败，状态禁止逆操作！")
	}

	Log.Error("更新状态值未能得到预期影响值(预期1):", dbResult.RowsAffected)
	return MsgEmity{}.Err(7004, "更新状态值未能得到预期影响值(预期1)", dbResult.RowsAffected)
}

/**
 * 修改步骤值(如果设置为单向则新值必须大于旧值)
 * @param id 编号
 * @param iSetp 步骤值
 * @param iVersion 记录版本号
 * @param sMemo 备注
 * @param unidirectional 是否单向 设置为单向则新值必须大于旧值才能执行
 * @param entity 实体类
 * @return MsgEmity 返回执行情况
 */
func (dao CommonDao) ChangeSetp(entity interface{}, id interface{}, iSetp int, iVersion int, sMemo string, unidirectional bool) *MsgEmity {
	if "" == fmt.Sprintf("%v", id) {
		return MsgEmity{}.Err(7001, "记录编号参数为空！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	params := []interface{}{}
	params = append(params, sql.Named(tableInfo.GsKeyName+"Where", id))

	var build strings.Builder
	build.WriteString("UPDATE ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" SET iSetp=@iSetp")
	params = append(params, sql.Named("iSetp", iSetp))

	sMemo = strings.TrimSpace(sMemo)
	if "" != sMemo {
		build.WriteString(" ,")
		build.WriteString(TablesMemo)
		build.WriteString("=@")
		build.WriteString(TablesMemo)
		params = append(params, sql.Named(TablesMemo, sMemo))
	}

	if 0 != iVersion {
		build.WriteString(" ,")
		build.WriteString(TableVersionName)
		build.WriteString(" = 1 + ")
		build.WriteString(TableVersionName)
	}

	build.WriteString(" WHERE ")
	build.WriteString(tableInfo.GsKeyName)
	build.WriteString(" = @")
	build.WriteString(tableInfo.GsKeyName)
	build.WriteString("Where")
	if 0 != iVersion {
		build.WriteString(" AND ")
		build.WriteString(TableVersionName)
		build.WriteString(" =@iVersionWhere")
		params = append(params, sql.Named("iVersionWhere", iVersion))
	}

	if unidirectional {
		build.WriteString(" AND ")
		build.WriteString(TableSetpName)
		build.WriteString(" < @iSetpWhere")
		params = append(params, sql.Named("iSetpWhere", iSetp))
	}

	dbResult := SqlFactory{}.GetDB().Exec(build.String(), params)
	if dbResult.Error != nil {
		Log.Error("更新步骤值发生异常:", dbResult.Error)
		return MsgEmity{}.Err(7005, "更新步骤值发生异常:", dbResult.Error)
	}

	if 0 < dbResult.RowsAffected {
		return MsgEmity{}.Success(7999, "更新步骤值成功！")
	}

	build.Reset()
	build.WriteString("SELECT ")
	build.WriteString(tableInfo.GsKeyName)
	build.WriteString(", ")
	build.WriteString(TableSetpName)

	if 0 != iVersion {
		build.WriteString(", ")
		build.WriteString(TableVersionName)
	}

	build.WriteString(" FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE ")
	build.WriteString(tableInfo.GsKeyName)
	build.WriteString(" = @")
	build.WriteString(tableInfo.GsKeyName)

	var iSetpOld, iVersionOld int
	row := SqlFactory{}.Raw(build.String(), sql.Named(tableInfo.GsKeyName, id)).Row()
	row.Scan(&iSetpOld, &iVersionOld)

	if 0 == iSetpOld {
		return MsgEmity{}.Err(7001, "没有找到对应数据！")
	}

	if (0 != iVersion) && (iVersion != iVersionOld) {
		return MsgEmity{}.Err(7002, "更新步骤值失败，系统中的数据可能已经被更新！")
	}

	if (iSetp >= 0) && (iSetp < iSetpOld) {
		return MsgEmity{}.Err(7003, "更新步骤值失败，状态禁止逆操作！")
	}

	Log.Error("更新步骤值未能得到预期影响值(预期1):", dbResult.RowsAffected)
	return MsgEmity{}.Err(7004, "更新步骤值未能得到预期影响值(预期1)", dbResult.RowsAffected)
}

// 删除
func (dao CommonDao) Del(entity interface{}, id interface{}, iVersion int, currentUser string, onlyCreator bool) *MsgEmity {
	if id == nil {
		return MsgEmity{}.Err(7001, "记录编号为空")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	if tableInfo.GbHasVersion && (iVersion < 1) && (iVersion != IntegerUtil{}.MaxInt()) {
		return MsgEmity{}.Err(7002, "记录版本号不正确")
	}

	where := map[string]interface{}{}
	where[tableInfo.GsKeyName] = id

	var delSql strings.Builder
	delSql.WriteString("DELETE FROM ")
	delSql.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	delSql.WriteString(tableInfo.GsTableName)
	delSql.WriteString(" WHERE ")
	delSql.WriteString(tableInfo.GsKeyName)
	delSql.WriteString(" = @")
	delSql.WriteString(tableInfo.GsKeyName)

	if tableInfo.GbHasVersion && (iVersion != IntegerUtil{}.MaxInt()) {
		delSql.WriteString(" AND ")
		delSql.WriteString(TableVersionName)
		delSql.WriteString("=@")
		delSql.WriteString(TableVersionName)
		where[TableVersionName] = iVersion
	}

	if onlyCreator {
		delSql.WriteString(" AND ")
		delSql.WriteString(TableCreatorName)
		delSql.WriteString("=@sCreator")
		where["sCreator"] = currentUser
	}

	dbResult := SqlFactory{}.GetDB().Exec(delSql.String(), where)

	if dbResult.Error != nil {
		Log.Error("删除发生异常:", dbResult.Error)
		return MsgEmity{}.Err(1002, "删除发生异常:", dbResult.Error)
	}

	if dbResult.RowsAffected > 0 {
		return MsgEmity{}.Success(1999, "删除成功")
	}

	//--影响数为0,需要进一步判断--//
	var build strings.Builder
	build.WriteString("SELECT * FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE ")
	build.WriteString(tableInfo.GsKeyName)
	build.WriteString(" = ?")

	var iCount int
	SqlFactory{}.Raw(build.String(), id).Scan(&iCount)
	if iCount == 0 {
		return MsgEmity{}.Success(1999, "没有发现此数据")
	}

	if !tableInfo.GbHasVersion {
		return MsgEmity{}.Err(1003, "删除失败,数据还存在")
	}

	var oldVersion int
	build.Reset()
	build.WriteString("SELECT ")
	build.WriteString(TableVersionName)
	build.WriteString(" FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE ")
	build.WriteString(tableInfo.GsKeyName)
	build.WriteString(" = ?")
	SqlFactory{}.Raw(build.String(), id).Scan(&oldVersion)

	if oldVersion == iVersion {
		return MsgEmity{}.Err(1004, "删除失败,数据还存在")
	}

	return MsgEmity{}.Err(1005, "删除失败,提供的版本号不正确,可能数据已经被改变")
}

/**
 * 修改
 * @param entity 对象类型
 * @param id 记录编号值
 * @param iVersion 记录版本号
 * @param data 待更新的字段和值
 * @param currentUser 当前用户
 * @return MsgEmity
 */
func (dao CommonDao) Edit(entity interface{}, id interface{}, iVersion int, data map[string]interface{}, currentUser string, onlyCreator bool) *MsgEmity {
	if "" == fmt.Sprintf("%v", id) {
		return MsgEmity{}.Err(7001, "记录编号参数为空！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)
	params := []interface{}{} //[]sql.NamedArg{}
	params = append(params, sql.Named(tableInfo.GsKeyName, id))

	var build strings.Builder
	build.WriteString("UPDATE ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" SET @@")

	for key, value := range data {
		build.WriteString(", ")
		build.WriteString(key)

		build.WriteString("=@")
		build.WriteString(key)
		params = append(params, sql.Named(key, value))
	}

	if tableInfo.GbHasVersion {
		build.WriteString(", ")
		build.WriteString(TableVersionName)
		build.WriteString("= 1 + ")
		build.WriteString(TableVersionName)
	}

	if tableInfo.GbHasModifieder {
		build.WriteString(",")
		build.WriteString(TableModifiederName)
		build.WriteString("=@")
		build.WriteString(TableModifiederName)
		params = append(params, sql.Named(TableModifiederName, currentUser))
	}

	if tableInfo.GbHasModifiedDate {
		build.WriteString(", ")
		build.WriteString(TableModifiedDateName)
		build.WriteString("=NOW()")
	}

	build.WriteString(" WHERE ")
	build.WriteString(tableInfo.GsKeyName)

	build.WriteString(" = @")
	build.WriteString(tableInfo.GsKeyName)

	if (0 != iVersion) && tableInfo.GbHasVersion {
		build.WriteString(" AND ")
		build.WriteString(TableVersionName)
		build.WriteString("=@sCreator")
		params = append(params, sql.Named("sCreator", iVersion))
	}

	if onlyCreator { //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆
		build.WriteString(" AND ")
		build.WriteString(TableCreatorName)
		build.WriteString("=@sCreator")
		params = append(params, sql.Named("sCreator", currentUser))
	}

	txt := strings.Replace(build.String(), " @@, ", " ", -1)
	dbResult := SqlFactory{}.Exec(txt, params)
	if dbResult.Error != nil {
		Log.Error("更新数据发生异常:", dbResult.Error)
		return MsgEmity{}.Err(7005, "更新数据发生异常:", dbResult.Error)
	}

	if 0 < dbResult.RowsAffected {
		return MsgEmity{}.Success(7999, "更新数据成功！")
	}

	build.Reset()
	build.WriteString("SELECT ")
	build.WriteString(tableInfo.GsKeyName)

	if tableInfo.GbHasVersion {
		build.WriteString(", ")
		build.WriteString(TableVersionName)
	}

	build.WriteString(" FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE ")
	build.WriteString(tableInfo.GsKeyName)
	build.WriteString(" = @")
	build.WriteString(tableInfo.GsKeyName)

	var vId, iVersionOld int
	row := SqlFactory{}.Raw(build.String(), sql.Named(tableInfo.GsKeyName, id)).Row()
	row.Scan(&vId, &iVersionOld)

	if ("" == fmt.Sprintf("%v", vId)) || ("<nil>" == fmt.Sprintf("%v", vId)) {
		return MsgEmity{}.Err(7001, "没有找到对应数据！")
	}

	if tableInfo.GbHasVersion && (iVersion != iVersionOld) {
		return MsgEmity{}.Err(7002, "更新数据失败，系统中的数据可能已经被更新！")
	}

	Log.Error("更新数据未能得到预期影响值(预期1):", dbResult.RowsAffected)
	return MsgEmity{}.Err(7004, "更新数据未能得到预期影响值(预期1)", dbResult.RowsAffected)
}

/**
 * 根据主键查询数据
 * entity 检查用数据结构
 * id 主键
 */
func (dao CommonDao) FindById(entity interface{}, id interface{}, currentUser string, onlyCreator bool) *MsgEmity {
	if id == nil {
		return MsgEmity{}.Err(7001, "记录编号为空")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	where := map[string]interface{}{tableInfo.GsKeyName: id}

	if onlyCreator { //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆
		where[TableCreatorName] = currentUser
	}

	dbResult := SqlFactory{}.GetDB().Where(where).First(&entity)

	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return MsgEmity{}.Err(1004, "查询数据发生异常:", dbResult.Error)
	}

	return MsgEmity{}.Success(&entity, "新增成功")
}

// 查询所有数据
func (dao CommonDao) FindAll(entity interface{}, where map[string]interface{}, currentUser string, onlyCreator bool) *MsgEmity {
	tableInfo := TableInfo{}.GetByEntity(entity)

	if onlyCreator {
		where[TableCreatorName] = currentUser
	}

	var build strings.Builder
	build.WriteString("SELECT * FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)

	if len(where) > 0 {
		build.WriteString(" WHERE 1=1")
		for key := range where {
			build.WriteString(" AND ")
			build.WriteString(key)
			build.WriteString("=@")
			build.WriteString(key)
		}
	}

	text := strings.Replace(build.String(), "1=1 AND ", "", -1)

	rows, err := SqlFactory{}.Raw(text, where).Rows()
	defer rows.Close()

	if nil != err {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(1005, "查询发生异常:", err)
	}

	res := SqlFactory{}.ScanRows2mapI(rows)
	if res == nil {
		return MsgEmity{}.Err(1006, "查询发生异常")
	}

	return MsgEmity{}.Success(res, "查询成功")
}

// 查询指定时间内数据
func (dao CommonDao) FindByDate(entity interface{}, sDateSt string, sDateEd string, currentUser string, onlyCreator bool) *MsgEmity {
	var dDateSt time.Time
	if "" == sDateSt {
		d, _ := time.ParseDuration("-168h") //7天前
		dDateSt = time.Now().Add(d)
	} else {
		temp, err := time.ParseInLocation("2006-01-02 15:04:05", sDateSt, time.Local)
		if err != nil {
			return MsgEmity{}.Err(7001, "'开始时间'格式不符合要求,推荐使用格式:'YYYY-MM-DD HH:mm:ss'")
		}

		dDateSt = temp
	}

	var dDateEd time.Time
	if "" == sDateEd {
		dDateEd = time.Now()
	} else {
		temp, err := time.ParseInLocation("2006-01-02 15:04:05", sDateSt, time.Local)
		if err != nil {
			return MsgEmity{}.Err(7002, "'结束时间'格式不符合要求,推荐使用格式:'YYYY-MM-DD HH:mm:ss'")
		}

		dDateEd = temp
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	var build strings.Builder
	build.WriteString("SELECT * FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE ")
	build.WriteString(TableCreateDateName)
	build.WriteString(" BETWEEN @dDateSt AND @dDateEd")

	where := map[string]interface{}{
		"dDateSt": dDateSt,
		"dDateEd": dDateEd,
	}

	if onlyCreator { //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆
		build.WriteString(" AND ")
		build.WriteString(TableCreatorName)
		build.WriteString("=@sCreator")
		where["sCreator"] = currentUser
	}

	rows, err := SqlFactory{}.Raw(build.String(), where).Rows()
	defer rows.Close()

	if nil != err {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7003, "查询发生异常:", err)
	}

	res := SqlFactory{}.ScanRows2mapI(rows)
	if res == nil {
		return MsgEmity{}.Err(7004, "查询发生异常")
	}

	return MsgEmity{}.Success(res, "查询成功")
}

// 查询指定行数据
func (dao CommonDao) FindByRow(entity interface{}, id interface{}, currentUser string, onlyCreator bool) *MsgEmity {
	if id == nil {
		return MsgEmity{}.Err(7001, "记录编号为空")
	}

	sId := fmt.Sprintf("%v", id)
	if sId == "" {
		return MsgEmity{}.Err(7002, "记录编号为空")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	var s reflect.Type
	typeOf := reflect.TypeOf(entity)  //通过反射获取type定义
	if typeOf.Kind() == reflect.Ptr { //是否指针类型
		s = reflect.TypeOf(entity).Elem() //通过反射获取type定义
	} else {
		s = entity.(reflect.Value).Type() //通过反射获取type定义
	}

	var appendFieldSql strings.Builder
	var dictionarySql strings.Builder
	var oTherJoin = map[string]string{}

	var build strings.Builder
	build.WriteString("SELECT ")
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(".*,")

	for i := 0; i < s.NumField(); i++ {
		//--匿名属性--//
		if s.Field(i).Anonymous {
			continue
		}

		//--非匿名属性--//
		str := s.Field(i).Tag.Get("dataInfo")
		if "" == str {
			continue //没有设置dataInfo则不能参与
		}

		var dataInfo DataInfo
		json.Unmarshal([]byte(str), &dataInfo)

		f, d := ModuleUtil{}.GetFindByPageSelectSqlByField(dataInfo, tableInfo.GsTableName, s.Field(i).Name, oTherJoin)
		if "" != f {
			appendFieldSql.WriteString(f)
		}

		if "" != d {
			dictionarySql.WriteString(d)
		}
	}

	build.WriteString(appendFieldSql.String())
	build.WriteString(" FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" AS ")
	build.WriteString(tableInfo.GsTableName)

	if "" != dictionarySql.String() {
		build.WriteString(dictionarySql.String())
	}

	build.WriteString(" WHERE ")
	build.WriteString(tableInfo.GsKeyName)
	build.WriteString(" =@")
	build.WriteString(tableInfo.GsKeyName)

	where := map[string]interface{}{tableInfo.GsKeyName: id}
	if onlyCreator {
		build.WriteString(" AND ")
		build.WriteString(TableCreatorName)
		build.WriteString("=@sCreator")
		where["sCreator"] = currentUser
	}

	text := strings.Replace(build.String(), ", FROM ", " FROM ", -1)

	rows, err := SqlFactory{}.Raw(text, where).Rows()
	defer rows.Close()

	if nil != err {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(1005, "查询发生异常:", err)
	}

	res := SqlFactory{}.ScanRows2mapI(rows)
	if res == nil {
		return MsgEmity{}.Err(1006, "查询发生异常")
	}

	if len(res) < 0 {
		return MsgEmity{}.Success(res, "查询成功,但没有数据")
	}

	return MsgEmity{}.Success(res[0], "查询成功")
}

// 查询分页数据
func (dao CommonDao) FindByPage(entity interface{}, findByPageParam FindByPageParam, currentUser string, onlyCreator bool) *MsgEmity {
	findByPageCountSql, findByPageSql, params := ModuleUtil{}.GetFindByPageSelectSql(currentUser, entity, findByPageParam, onlyCreator)

	var iCount int
	var dbResult *gorm.DB

	if len(params) < 1 {
		dbResult = SqlFactory{}.Raw(findByPageCountSql).Find(&iCount)
	} else {
		dbResult = SqlFactory{}.Raw(findByPageCountSql, params).Find(&iCount)
	}

	if dbResult.Error != nil {
		Log.Error("查询分页数量发生异常:", dbResult.Error)
		return MsgEmity{}.Err(7001, "查询分页数量发生异常:", dbResult.Error)
	}

	if iCount < 1 {
		res := make([]map[string]interface{}, 0)
		page := findByPageParam.Gpage
		page.GiCountRow = 0
		page.SetRecord(res)

		return MsgEmity{}.Success(page, "查询分页数量成功但没有数据")
	}

	var rows *sql.Rows
	var err error

	if len(params) < 1 {
		rows, err = SqlFactory{}.Raw(findByPageSql).Rows()
	} else {
		rows, err = SqlFactory{}.Raw(findByPageSql, params).Rows()
	}

	defer rows.Close()

	if err != nil {
		Log.Error("查询分页数量发生异常:", err)
		return MsgEmity{}.Err(7002, "查询分页数量发生异常")
	}

	page := findByPageParam.Gpage
	page.GiCountRow = iCount

	iCountPage := (iCount / page.GiSize)
	if (iCount % page.GiSize) > 0 {
		iCountPage = iCountPage + 1
	}
	page.GiCountPage = iCountPage

	res := SqlFactory{}.ScanRows2mapI(rows)

	if res == nil {
		return MsgEmity{}.Err(7003, "查询分页数量成功但解析数据发生异常")
	}

	page.SetRecord(res)

	return MsgEmity{}.Success(page, "查询分页数量成功")
}

// func (dao CommonDao) FindData(OrderInfoList orders, String sLikeStr, Object condition,
// 			onlyCreator bool, entity interface{}) {

// 			}

// 取路径字段
func (dao CommonDao) GetPath(sId, dbName, tableName string) string {
	var build strings.Builder
	build.WriteString("SELECT sPath FROM ")
	build.WriteString(dbName)
	build.WriteString(tableName)
	build.WriteString(" WHERE ")
	build.WriteString(TableMajorKeyString)
	build.WriteString("=?")

	var sPath string
	dbResult := SqlFactory{}.Raw(build.String(), sId).Scan(&sPath)
	if dbResult.Error != nil {
		Log.Error("查询路径信息发生异常:", dbResult.Error)
		return "/00/"
	}

	return sPath
}

/**
 * 取指定节点下的子节点编号
 * @param tableInfo 表信息
 * @param sPid 父节点
 * @return
 */
func (dao CommonDao) NewChildId(tableInfo *TableInfo, sPid string) *MsgEmity {
	var build strings.Builder
	build.WriteString("SELECT (cur + 1) AS newId FROM (")
	build.WriteString(" 		select cast(A.${TableMajorKeyString} AS SIGNED) AS cur, IFNULL(")
	build.WriteString(" 			(select MIN( CAST(B.${TableMajorKeyString} as signed integer)) from ${sDbName}${sTableName} AS B")
	build.WriteString(" 				where cast(B.${TableMajorKeyString} AS SIGNED) > cast(A.${TableMajorKeyString} AS SIGNED)")
	build.WriteString(" 				and B.${TablePidKey} = #{sPid}")
	build.WriteString(" 				and CAST(B.${TableMajorKeyString} as signed integer) != 0")                                                  // 过滤字段中含非数字的记录
	build.WriteString(" 				and CAST(B.${TableMajorKeyString} as signed integer) = CAST(B.${TableMajorKeyString} as signed integer) +0") // 过滤字段中含非数字的记录
	build.WriteString(" 			), 99999999) AS nxt")
	build.WriteString(" 		from ${sDbName}${sTableName} AS A")
	build.WriteString(" 		where A.${TablePidKey} = #{sPid}")
	build.WriteString(" 		and CAST(A.${TableMajorKeyString} as signed integer) != 0")                                                  // 过滤字段中含非数字的记录
	build.WriteString(" 		and CAST(A.${TableMajorKeyString} as signed integer) = CAST(A.${TableMajorKeyString} as signed integer) +0") // 过滤字段中含非数字的记录
	build.WriteString(" 	) AS D")
	build.WriteString(" WHERE (nxt - cur > 1) or (nxt = 99999999)")
	build.WriteString(" ORDER BY cur")
	build.WriteString(" LIMIT 0, 1")

	txt := build.String()
	txt = strings.Replace(txt, "${sDbName}", SqlFactory{}.GetDbName(tableInfo.GsDbName), -1)
	txt = strings.Replace(txt, "${sTableName}", tableInfo.GsTableName, -1)
	txt = strings.Replace(txt, "${TablePidKey}", TablePidKey, -1)
	txt = strings.Replace(txt, "${TableMajorKeyString}", TableMajorKeyString, -1)
	txt = strings.Replace(txt, "#{sPid}", "@sPid", -1)

	newId := ""
	dbResult := SqlFactory{}.Raw(txt, sql.Named(TablePidKey, sPid)).Find(&newId)
	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return MsgEmity{}.Err(1001, "查询数据发生异常:", dbResult.Error)
	}

	return MsgEmity{}.Success(newId, "获取子节点编号成功")
}

/**
 * 验证新增数据是否存在重复
 *
 */
func (dao CommonDao) ValidEntityRepeatByAdd(dbEntity interface{}, currentUser string) *MsgEmity {
	tableInfo := TableInfo{}.GetByEntity(dbEntity)
	customService := GlobalVariable{}.Get(tableInfo.GsTableName + "_ModuleService")

	//-- 树形结构 --//
	if tableInfo.GbHasPid {
		if nil == customService { //如果没有自定义业务层
			return dao.CommonCheckRepeatByAddAndTree(dbEntity, currentUser) //通用树型结构表添加数据时重复检查方法
		}

		method := ReflectUtils{}.GetMethod(customService, "CheckRepeatByAddAndTree")
		if !method.IsValid() { //如果自定义业务层定义了自检方法
			return dao.CommonCheckRepeatByAddAndTree(dbEntity, currentUser) //通用树型结构表添加数据时重复检查方法
		}

		result := ReflectUtils{}.DoMethod(customService, "CheckRepeatByAddAndTree", dbEntity)
		me := result[0].Interface()
		return me.(*MsgEmity)
	}

	//--不是树形数据则使用普通方法检查--//
	if nil == customService { //如果没有自定义业务层
		return dao.CommonCheckRepeatByAdd(dbEntity, currentUser) //通用添加数据时重复检查方法
	}

	method := ReflectUtils{}.GetMethod(customService, "CheckRepeatByAdd")
	if !method.IsValid() { //如果自定义业务层定义了自检方法
		return dao.CommonCheckRepeatByAdd(dbEntity, currentUser) //通用添加数据时重复检查方法
	}

	result := ReflectUtils{}.DoMethod(customService, "CheckRepeatByAdd", dbEntity)
	me := result[0].Interface()
	return me.(*MsgEmity)
}

/**
 * 验证更新数据是否存在重复
 *
 */
func (dao CommonDao) ValidEntityRepeatByEdit(entity interface{}, id interface{}, data map[string]interface{}, currentUser string) *MsgEmity {
	tableInfo := TableInfo{}.GetByEntity(entity)
	customService := GlobalVariable{}.Get(tableInfo.GsTableName + "_ModuleService")

	//-- 树形结构 --//
	if tableInfo.GbHasPid {
		if nil == customService { //如果没有自定义业务层
			return dao.CommonCheckRepeatByEditAndTree(entity, id, data["sName"], currentUser) //通用树型结构表添加数据时重复检查方法
		}

		method := ReflectUtils{}.GetMethod(customService, "CheckRepeatByEditAndTree")
		if !method.IsValid() { //如果自定义业务层定义了自检方法
			return dao.CommonCheckRepeatByEditAndTree(entity, id, data["sName"], currentUser) //通用树型结构表添加数据时重复检查方法
		}

		result := ReflectUtils{}.DoMethod(customService, "CheckRepeatByEditAndTree", id, data["sName"])
		me := result[0].Interface()
		return me.(*MsgEmity)
	}

	//--不是树形数据则使用普通方法检查--//
	if nil == customService { //如果没有自定义业务层
		return dao.CommonCheckRepeatByEdit(entity, id, currentUser) //通用添加数据时重复检查方法
	}

	method := ReflectUtils{}.GetMethod(customService, "CheckRepeatByEdit")
	if !method.IsValid() { //如果自定义业务层定义了自检方法
		return dao.CommonCheckRepeatByEdit(entity, id, currentUser) //通用添加数据时重复检查方法
	}

	result := ReflectUtils{}.DoMethod(customService, "CheckRepeatByEdit", data, id)
	me := result[0].Interface()
	return me.(*MsgEmity)
}

/**
 * 通用树型结构表添加数据时重复检查方法
 * dbEntity
 */
func (dao CommonDao) CommonCheckRepeatByAddAndTree(dbEntity interface{}, currentUser string) *MsgEmity {
	vName := ReflectUtils{}.GetFieldValue(dbEntity, GtableTreeNodeName)
	if nil == vName {
		return MsgEmity{}.Err(1001, "节点名称为空")
	}

	tableInfo := TableInfo{}.GetByEntity(dbEntity)

	sName := vName.(string)

	var sPid string
	vPid := ReflectUtils{}.GetFieldValue(dbEntity, GtablePidKey)
	if nil != vPid {
		sPid = vPid.(string)
	} else {
		sPid = TableTreeRootValue
	}

	if sPid == "" {
		sPid = TableTreeRootValue
	}

	//同一层节点下,展现名不能相同//
	var build strings.Builder
	build.WriteString("SELECT SUM(iCount) AS iCount FROM (")
	build.WriteString(" 	select count(1) as iCount from ${sDbName}${sTableName}")
	build.WriteString(" 	where ${TablePidKey} = #{sPid} and ${TableTreeNodeName} = #{sName}")
	build.WriteString(") TMP")

	txt := build.String()
	txt = strings.Replace(txt, "${sDbName}", SqlFactory{}.GetDbName(tableInfo.GsDbName), -1)
	txt = strings.Replace(txt, "${sTableName}", tableInfo.GsTableName, -1)
	txt = strings.Replace(txt, "${TablePidKey}", TablePidKey, -1)
	txt = strings.Replace(txt, "${TableTreeNodeName}", TableTreeNodeName, -1)
	txt = strings.Replace(txt, "#{sPid}", "@sPid", -1)
	txt = strings.Replace(txt, "#{sName}", "@sName", -1)

	var iCount int
	dbResult := SqlFactory{}.Raw(txt, sql.Named(TablePidKey, sPid), sql.Named("sName", sName)).Find(&iCount)
	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return MsgEmity{}.Err(1002, "查询数据发生异常:", dbResult.Error)
	}

	if iCount != 0 {
		return MsgEmity{}.Err(1003, "节点重复")
	}

	return MsgEmity{}.Success(1999, "节点未重复")
}

/**
 * 通用树型结构表添加数据时重复检查方法
 * entity
 */
func (dao CommonDao) CommonCheckRepeatByEditAndTree(entity interface{}, id interface{}, sName interface{}, currentUser string) *MsgEmity {
	if (nil == sName) || ("" == sName) || ("<nil>" == sName) {
		return MsgEmity{}.Err(1001, "节点名称为空")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	//同一层节点下,展现名不能相同//
	var build strings.Builder
	build.WriteString("SELECT SUM(iCount) AS iCount FROM (")
	build.WriteString(" 	select count(1) as iCount from ${sDbName}${sTableName}")
	build.WriteString(" 	where ${sId} <> @sId")
	build.WriteString(" 	and ${TablePidKey} = (select a.${TablePidKey} from ${sDbName}${sTableName} a where a.${sId} = #{sId})")
	build.WriteString(" 	and ${TableTreeNodeName} = #{sName}")
	build.WriteString(") TMP")

	txt := build.String()
	txt = strings.Replace(txt, "${sDbName}", SqlFactory{}.GetDbName(tableInfo.GsDbName), -1)
	txt = strings.Replace(txt, "${sTableName}", tableInfo.GsTableName, -1)
	txt = strings.Replace(txt, "${sId}", tableInfo.GsKeyName, -1)
	txt = strings.Replace(txt, "${TablePidKey}", TablePidKey, -1)
	txt = strings.Replace(txt, "${TableTreeNodeName}", TableTreeNodeName, -1)
	txt = strings.Replace(txt, "#{sName}", "@sName", -1)

	var iCount int
	dbResult := SqlFactory{}.Raw(txt, sql.Named(tableInfo.GsKeyName, id), sql.Named("sName", sName)).Find(&iCount)
	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return MsgEmity{}.Err(1002, "查询数据发生异常:", dbResult.Error)
	}

	if iCount != 0 {
		return MsgEmity{}.Err(1003, "节点重复")
	}

	return MsgEmity{}.Success(1999, "节点未重复")
}

/**
 * 通用添加数据时重复检查方法
 * dbEntity
 */
func (dao CommonDao) CommonCheckRepeatByAdd(dbEntity interface{}, currentUser string) *MsgEmity {
	tableInfo := TableInfo{}.GetByEntity(dbEntity)

	vCheckRepeatCombination := GlobalVariable{}.Get(tableInfo.GsTableName + "_CheckRepeatCombination")
	vCheckRepeatAlone := GlobalVariable{}.Get(tableInfo.GsTableName + "_CheckRepeatAlone")

	k := 0

	//检查待新增内容是否存在重复数据(多字段组合重复即重复)集合
	if nil != vCheckRepeatCombination {
		checkRepeatCombination := vCheckRepeatCombination.([]string)

		var build strings.Builder
		build.WriteString("SELECT COUNT(1) AS iCount FROM ${sDbName}${sTableName} WHERE 1=1 ")

		var temp strings.Builder
		temp.WriteString("[")

		where := make(map[string]interface{})
		for _, value := range checkRepeatCombination {
			build.WriteString(" AND ")
			build.WriteString(value)
			build.WriteString(" = @")
			build.WriteString(value)

			where[value] = ReflectUtils{}.GetFieldValue(dbEntity, value)
			temp.WriteString("、")
			temp.WriteString(value)
		}

		txt := strings.Replace(build.String(), "1=1 AND ", "", -1)
		txt = strings.Replace(txt, "${sDbName}", SqlFactory{}.GetDbName(tableInfo.GsDbName), -1)
		txt = strings.Replace(txt, "${sTableName}", tableInfo.GsTableName, -1)

		iCount := 0
		dbResult := SqlFactory{}.Raw(txt, where).Find(&iCount)
		if dbResult.Error != nil {
			Log.Error("验证数据是否重复发生异常:", dbResult.Error)
			return MsgEmity{}.Err(1001, "验证数据是否重复发生异常:", dbResult.Error)
		}

		if iCount != 0 {
			temp.WriteString("]组合发现数据重复")
			return MsgEmity{}.Err(1002, strings.Replace(temp.String(), "、", "", 1))
		}

		k++
	}

	//检查待新增内容是否存在重复数据(单独字段重复即重复)集合
	if nil != vCheckRepeatAlone {
		checkRepeatAlone := vCheckRepeatAlone.(map[string]int)

		var build strings.Builder
		build.WriteString("SELECT SUM(iCount) FROM (")

		where := make(map[string]interface{})
		for key, value := range checkRepeatAlone {
			build.WriteString(" union all select (SIGN(COUNT(1)) * ")
			build.WriteString(strconv.Itoa(value))
			build.WriteString(") as iCount ")
			build.WriteString(" from ${sDbName}${sTableName} ")
			build.WriteString(" where ")
			build.WriteString(key)
			build.WriteString("= @")
			build.WriteString(key)

			where[key] = ReflectUtils{}.GetFieldValue(dbEntity, key)
		}

		build.WriteString(") TMP")

		txt := strings.Replace(build.String(), " union all ", " ", 1)
		txt = strings.Replace(txt, "${sDbName}", SqlFactory{}.GetDbName(tableInfo.GsDbName), -1)
		txt = strings.Replace(txt, "${sTableName}", tableInfo.GsTableName, -1)

		iCount := 0
		dbResult := SqlFactory{}.Raw(txt, where).Find(&iCount)
		if dbResult.Error != nil {
			Log.Error("验证数据是否重复发生异常:", dbResult.Error)
			return MsgEmity{}.Err(1003, "验证数据是否重复发生异常:", dbResult.Error)
		}

		if iCount != 0 {
			var temp strings.Builder
			str := fmt.Sprintf("%0*d", len(checkRepeatAlone), iCount)
			array := []rune(str)

			for key, value := range checkRepeatAlone {
				i := len(strconv.Itoa(value))
				if array[i] != 0 {
					continue
				}

				temp.WriteString("、")
				temp.WriteString(key)
			}

			temp.WriteString("存在重复")

			return MsgEmity{}.Err(1004, strings.Replace(temp.String(), "、", "", 1))
		}

		k++
	}

	if 0 == k {
		return MsgEmity{}.Success("没有设定验证函数,通过")
	}

	return MsgEmity{}.Success("经验证,通过")
}

/**
 * 通用更新数据时重复检查方法
 * entity
 */
func (dao CommonDao) CommonCheckRepeatByEdit(entity interface{}, id interface{}, currentUser string) *MsgEmity {
	tableInfo := TableInfo{}.GetByEntity(entity)

	vCheckRepeatCombination := GlobalVariable{}.Get(tableInfo.GsTableName + "_CheckRepeatCombination")
	vCheckRepeatAlone := GlobalVariable{}.Get(tableInfo.GsTableName + "_CheckRepeatAlone")

	k := 0

	//检查待新增内容是否存在重复数据(多字段组合重复即重复)集合
	if nil != vCheckRepeatCombination {
		checkRepeatCombination := vCheckRepeatCombination.([]string)

		var build strings.Builder
		build.WriteString("SELECT COUNT(1) AS iCount FROM ${sDbName}${sTableName} WHERE 1=1 ")

		var temp strings.Builder
		temp.WriteString("[")

		where := make(map[string]interface{})
		where[tableInfo.GsKeyName] = id
		build.WriteString(" AND ")
		build.WriteString(tableInfo.GsKeyName)
		build.WriteString(" = @")
		build.WriteString(tableInfo.GsKeyName)

		for _, value := range checkRepeatCombination {
			build.WriteString(" AND ")
			build.WriteString(value)
			build.WriteString(" = @")
			build.WriteString(value)

			where[value] = ReflectUtils{}.GetFieldValue(entity, value)
			temp.WriteString("、")
			temp.WriteString(value)
		}

		txt := strings.Replace(build.String(), "1=1 AND ", "", -1)
		txt = strings.Replace(txt, "${sDbName}", SqlFactory{}.GetDbName(tableInfo.GsDbName), -1)
		txt = strings.Replace(txt, "${sTableName}", tableInfo.GsTableName, -1)

		iCount := 0
		dbResult := SqlFactory{}.Raw(txt, where).Find(&iCount)
		if dbResult.Error != nil {
			Log.Error("验证数据是否重复发生异常:", dbResult.Error)
			return MsgEmity{}.Err(1001, "验证数据是否重复发生异常:", dbResult.Error)
		}

		if iCount != 0 {
			temp.WriteString("]组合发现数据重复")
			return MsgEmity{}.Err(1002, strings.Replace(temp.String(), "、", "", 1))
		}

		k++
	}

	//检查待新增内容是否存在重复数据(单独字段重复即重复)集合
	if nil != vCheckRepeatAlone {
		checkRepeatAlone := vCheckRepeatAlone.(map[string]int)

		var build strings.Builder
		build.WriteString("SELECT SUM(iCount) FROM (")

		where := make(map[string]interface{})
		where[tableInfo.GsKeyName] = id

		for key, value := range checkRepeatAlone {
			build.WriteString(" union all select (SIGN(COUNT(1)) * ")
			build.WriteString(strconv.Itoa(value))
			build.WriteString(") as iCount ")
			build.WriteString(" from ${sDbName}${sTableName} ")
			build.WriteString(" where ")
			build.WriteString(key)
			build.WriteString("= @")
			build.WriteString(key)
			build.WriteString(" and ")
			build.WriteString(tableInfo.GsKeyName)
			build.WriteString(" = @")
			build.WriteString(tableInfo.GsKeyName)

			where[key] = ReflectUtils{}.GetFieldValue(entity, key)
		}

		build.WriteString(") TMP")

		txt := strings.Replace(build.String(), " union all ", " ", 1)
		txt = strings.Replace(txt, "${sDbName}", SqlFactory{}.GetDbName(tableInfo.GsDbName), -1)
		txt = strings.Replace(txt, "${sTableName}", tableInfo.GsTableName, -1)

		iCount := 0
		dbResult := SqlFactory{}.Raw(txt, where).Find(&iCount)
		if dbResult.Error != nil {
			Log.Error("验证数据是否重复发生异常:", dbResult.Error)
			return MsgEmity{}.Err(1003, "验证数据是否重复发生异常:", dbResult.Error)
		}

		if iCount != 0 {
			var temp strings.Builder
			str := fmt.Sprintf("%0*d", len(checkRepeatAlone), iCount)
			array := []rune(str)

			for key, value := range checkRepeatAlone {
				i := len(strconv.Itoa(value))
				if array[i] != 0 {
					continue
				}

				temp.WriteString("、")
				temp.WriteString(key)
			}

			temp.WriteString("存在重复")

			return MsgEmity{}.Err(1004, strings.Replace(temp.String(), "、", "", 1))
		}

		k++
	}

	if 0 == k {
		return MsgEmity{}.Success("没有设定验证函数,通过")
	}

	return MsgEmity{}.Success("经验证,通过")
}

/**
 * 读取树形结构数据
 * @param entity
 * @param sGroupColumn
 * @param sGroupName
 * @return
 */
func (dao CommonDao) FindByTree(entity interface{}, sGroupColumn, sGroupName string, currentUser string) *MsgEmity {
	if (!ReflectUtils{}.HasField(entity, sGroupName)) {
		return MsgEmity{}.Err(1001, "指定分组字段不存在！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	var build strings.Builder
	build.WriteString("SELECT * FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE ")
	build.WriteString(TableMajorKeyString)
	build.WriteString(" > 0")

	where := make(map[string]interface{})
	if "" != sGroupName {
		build.WriteString(" AND ")
		build.WriteString(TablePathKey)
		build.WriteString(" LIKE (")
		build.WriteString(" 	select CONCAT(a.")
		build.WriteString(TablePathKey)
		build.WriteString(", '%')")
		build.WriteString(" 	from ${sDbName}${sTableName} a")

		if "" != sGroupColumn {
			build.WriteString(" 	where a.")
			build.WriteString(sGroupColumn) //指定字段作为分组标识
			build.WriteString(" = ?")
		} else if tableInfo.GbHasOnlyign {
			build.WriteString(" 	where a.")     //启用唯一标识作为关键字
			build.WriteString(TableOnlyignName) //启用唯一标识作为关键字
			build.WriteString(" = @sGroupName") //启用唯一标识作为关键字
		} else {
			build.WriteString(" 	where a.")
			build.WriteString(TableTreeNodeName)
			build.WriteString(" = @sGroupName")
		}

		build.WriteString(" )")
		where["sGroupName"] = sGroupName
	}

	build.WriteString(" ORDER BY ")
	build.WriteString(TablePathKey)

	rows, err := SqlFactory{}.Raw(build.String(), where).Rows()
	defer rows.Close()

	if nil != err {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(1002, "查询发生异常:", err)
	}

	res := SqlFactory{}.ScanRows2mapI(rows)
	if res == nil {
		return MsgEmity{}.Err(1003, "查询后数据转换发生异常")
	}

	return MsgEmity{}.Success(res, "查询成功")
}

/**
 * 根据字段名取指定记录编号的数据库表中对应字段的值
 * @param entity 实体类
 * @param id
 * @param fieldNames 待取数据的字段名称集合
 * @return MsgEmity 返回内容data中存放的是Map
 */
func (dao CommonDao) GetValueByFieldName(entity interface{}, id interface{}, fieldNames []string, currentUser string) *MsgEmity {
	fieldNames = ReflectUtils{}.HoldByEntityToArray(entity, fieldNames) //按实体保留数组中的数据
	if len(fieldNames) < 1 {
		return MsgEmity{}.Err(7001, "没有对应的数据可查询！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	var build strings.Builder
	build.WriteString("SELECT ")

	for _, val := range fieldNames {
		build.WriteString(",")
		build.WriteString(val)
	}

	build.WriteString(" FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE ")
	build.WriteString(tableInfo.GsKeyName)
	build.WriteString("=@")
	build.WriteString(tableInfo.GsKeyName)

	where := make(map[string]interface{})
	where[tableInfo.GsKeyName] = id

	if (!ModuleUtil{}.EnableTag(tableInfo.GsTableName, 4)) { //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆
		build.WriteString(" AND ")
		build.WriteString(TableCreatorName)
		build.WriteString("=@sCreator")
		where["sCreator"] = currentUser
	}

	sql := strings.Replace(build.String(), ",", "", 1)
	rows, err := SqlFactory{}.Raw(sql, where).Rows()

	defer rows.Close()

	if nil != err {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7002, "查询发生异常:", err)
	}

	res := SqlFactory{}.ScanRows2mapI(rows)
	if res == nil {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7003, "查询发生异常:", err)
	}

	if len(res) < 1 {
		return MsgEmity{}.Err(7004, "数据不存在！")
	}

	return MsgEmity{}.Success(res[0], "查询成功")
}

/**
 * 根据字段名取指定记录编号的数据库表中对应字段的值
 * @param entity 实体类
 * @param id
 * @param fieldName 待取数据的字段名称集合
 * @return MsgEmity 返回内容data中存放的是Map
 */
func (dao CommonDao) GetValueByField(entity interface{}, id interface{}, fieldName string, currentUser string) *MsgEmity {
	fieldName = strings.TrimSpace(fieldName)
	if "" == fieldName {
		return MsgEmity{}.Err(7001, "没有对应的数据可查询！")
	}

	if (!ReflectUtils{}.HasField(entity, fieldName)) {
		return MsgEmity{}.Err(7002, "指定字段不存在！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	var build strings.Builder
	build.WriteString("SELECT ")
	build.WriteString(fieldName)
	build.WriteString(" FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE ")
	build.WriteString(tableInfo.GsKeyName)
	build.WriteString("=@")
	build.WriteString(tableInfo.GsKeyName)

	where := make(map[string]interface{})
	where[tableInfo.GsKeyName] = id

	if (!ModuleUtil{}.EnableTag(tableInfo.GsTableName, 4)) { //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆
		build.WriteString(" AND ")
		build.WriteString(TableCreatorName)
		build.WriteString("=@sCreator")
		where["sCreator"] = currentUser
	}

	sql := strings.Replace(build.String(), ",", "", 1)
	rows, err := SqlFactory{}.Raw(sql, where).Rows()

	defer rows.Close()

	if nil != err {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7003, "查询发生异常:", err)
	}

	res := SqlFactory{}.ScanRows2mapI(rows)
	if res == nil {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7004, "查询发生异常:", err)
	}

	if len(res) < 1 {
		return MsgEmity{}.Err(7005, "数据不存在！")
	}

	return MsgEmity{}.Success(res[0], "查询成功")
}

/**
 * 取记录对应的版本号
 * @param entity 实体类
 * @param idName 编号
 * @return
 */
func (dao CommonDao) GetiVersion(entity interface{}, id interface{}) *MsgEmity {
	if "" == fmt.Sprintf("%v", id) {
		return MsgEmity{}.Err(7001, "记录编号参数为空！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	var build strings.Builder
	build.WriteString("SELECT ")
	build.WriteString(TableVersionName)
	build.WriteString(" FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE ")
	build.WriteString(tableInfo.GsKeyName)
	build.WriteString("=@")
	build.WriteString(tableInfo.GsKeyName)

	where := make(map[string]interface{})
	where[tableInfo.GsKeyName] = id

	iVersion := 0
	dbResult := SqlFactory{}.Raw(build.String(), where).Find(&iVersion)
	if dbResult.Error != nil {
		Log.Error("验证数据是否重复发生异常:", dbResult.Error)
		return MsgEmity{}.Err(7002, "验证数据是否重复发生异常:", dbResult.Error)
	}

	if iVersion == 0 {
		return MsgEmity{}.Err(7003, "没有发现此数据")
	}

	return MsgEmity{}.Success(iVersion, "查询成功")
}

/**
 * 取记录对应的状态值
 * @param entity 实体类
 * @param idName 编号
 * @return
 */
func (dao CommonDao) GetiState(entity interface{}, id interface{}) *MsgEmity {
	if "" == fmt.Sprintf("%v", id) {
		return MsgEmity{}.Err(7001, "记录编号参数为空！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	var build strings.Builder
	build.WriteString("SELECT ")
	build.WriteString(TableStateName)
	build.WriteString(" FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE ")
	build.WriteString(tableInfo.GsKeyName)
	build.WriteString("=@")
	build.WriteString(tableInfo.GsKeyName)

	where := make(map[string]interface{})
	where[tableInfo.GsKeyName] = id

	iState := 0
	dbResult := SqlFactory{}.Raw(build.String(), where).Find(&iState)
	if dbResult.Error != nil {
		Log.Error("验证数据是否重复发生异常:", dbResult.Error)
		return MsgEmity{}.Err(7002, "验证数据是否重复发生异常:", dbResult.Error)
	}

	if iState == 0 {
		return MsgEmity{}.Err(7003, "没有发现此数据")
	}

	return MsgEmity{}.Success(iState, "查询成功")
}

/**
 * 根据关键值取对象集合
 * @Param entity 实体类
 * @Param where 存放查询条件
 * @return MsgEmity
 */
func (dao CommonDao) FindByKey(entity interface{}, where map[string]interface{}, currentUser string, onlyCreator bool) *MsgEmity {
	where = ReflectUtils{}.HoldByEntity(entity, where) //按实体保留map中的数据
	if len(where) < 1 {
		return MsgEmity{}.Err(7001, "没有对应的查询条件！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	var build strings.Builder
	build.WriteString("SELECT * FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE 1=1")

	for key, _ := range where {
		build.WriteString(" AND ")
		build.WriteString(key)
		build.WriteString("=@")
		build.WriteString(key)
	}

	if onlyCreator { //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆
		build.WriteString(" AND ")
		build.WriteString(TableCreatorName)
		build.WriteString("=@sCreator")
		where["sCreator"] = currentUser
	}

	sql := strings.Replace(build.String(), " 1=1 AND ", " ", 1)
	rows, err := SqlFactory{}.Raw(sql, where).Rows()

	defer rows.Close()

	if nil != err {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7002, "查询发生异常:", err)
	}

	res := SqlFactory{}.ScanRows2mapI(rows)
	if res == nil {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7003, "查询发生异常:", err)
	}

	return MsgEmity{}.Success(res, "查询成功")
}

/**
 * 根据关键值取对象集合中的第一个
 * @Param entity 实体类
 * @Param where 存放查询条件
 * @param fields 指定要查询的字段集合
 * @return MsgEmity
 */
func (dao CommonDao) FindOneByKey(entity interface{}, where map[string]interface{}, currentUser string, onlyCreator bool, fields ...string) *MsgEmity {
	where = ReflectUtils{}.HoldByEntity(entity, where) //按实体保留map中的数据
	if len(where) < 1 {
		return MsgEmity{}.Err(7001, "没有对应的查询条件！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	var build strings.Builder
	build.WriteString("SELECT ")

	fields = ReflectUtils{}.HoldByEntityToArray(entity, fields) //按实体保留数组中的数据
	for _, val := range fields {
		build.WriteString(",")
		build.WriteString(val)
	}

	build.WriteString(" FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE ")
	build.WriteString(tableInfo.GsKeyName)
	build.WriteString("=@")
	build.WriteString(tableInfo.GsKeyName)

	for key, _ := range where {
		build.WriteString(" AND ")
		build.WriteString(key)
		build.WriteString("=@")
		build.WriteString(key)
	}

	if onlyCreator {
		build.WriteString(" AND ")
		build.WriteString(TableCreatorName)
		build.WriteString("=@sCreator")
		where["sCreator"] = currentUser
	}

	rows, err := SqlFactory{}.Raw(build.String(), where).Rows()

	defer rows.Close()

	if nil != err {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7002, "查询发生异常:", err)
	}

	res := SqlFactory{}.ScanRows2mapI(rows)
	if res == nil {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7003, "查询发生异常:", err)
	}

	if len(res) < 1 {
		return MsgEmity{}.Err(7004, "数据不存在！")
	}

	return MsgEmity{}.Success(res[0], "查询成功")
}

/**
 * 根据关键值取对象集合中的符合条件的第一条记录的指定字段
 * @Param entity 实体类
 * @Param where 存放查询条件
 * @param fieldName 指定要查询的字段
 * @return MsgEmity
 */
func (dao CommonDao) FindValueByKey(entity interface{}, where map[string]interface{}, fieldName string, currentUser string, onlyCreator bool) *MsgEmity {
	where = ReflectUtils{}.HoldByEntity(entity, where) //按实体保留map中的数据
	if len(where) < 1 {
		return MsgEmity{}.Err(7001, "没有对应的查询条件！")
	}

	fieldName = strings.TrimSpace(fieldName)
	if "" == fieldName {
		return MsgEmity{}.Err(7002, "没有待查字段！")
	}

	if (!ReflectUtils{}.HasField(entity, fieldName)) {
		return MsgEmity{}.Err(7003, "指定字段不存在！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	var build strings.Builder
	build.WriteString("SELECT ")
	build.WriteString(fieldName)
	build.WriteString(" FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE ")
	build.WriteString(tableInfo.GsKeyName)
	build.WriteString("=@")
	build.WriteString(tableInfo.GsKeyName)

	for key, _ := range where {
		build.WriteString(" AND ")
		build.WriteString(key)
		build.WriteString("=@")
		build.WriteString(key)
	}

	if onlyCreator {
		build.WriteString(" AND ")
		build.WriteString(TableCreatorName)
		build.WriteString("=@sCreator")
		where["sCreator"] = currentUser
	}

	rows, err := SqlFactory{}.Raw(build.String(), where).Rows()

	defer rows.Close()

	if nil != err {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7004, "查询发生异常:", err)
	}

	res := SqlFactory{}.ScanRows2mapI(rows)
	if res == nil {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7005, "查询发生异常:", err)
	}

	if len(res) < 1 {
		return MsgEmity{}.Err(7006, "数据不存在！")
	}

	return MsgEmity{}.Success(res[0], "查询成功")
}

/**
 * 根据关键值查数量
 * @Param entity 实体类
 * @Param where 存放查询条件
 * @return MsgEmity
 */
func (dao CommonDao) FindCountByKey(entity interface{}, where map[string]interface{}, currentUser string, onlyCreator bool) *MsgEmity {
	where = ReflectUtils{}.HoldByEntity(entity, where) //按实体保留map中的数据

	if len(where) < 1 {
		return MsgEmity{}.Err(7001, "没有对应的查询条件！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	var build strings.Builder
	build.WriteString("SELECT COUNT(1) AS iCount FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE 1=1")

	for key, _ := range where {
		build.WriteString(" AND ")
		build.WriteString(key)
		build.WriteString("=@")
		build.WriteString(key)
	}

	if onlyCreator {
		build.WriteString(" AND ")
		build.WriteString(TableCreatorName)
		build.WriteString("=@sCreator")
		where["sCreator"] = currentUser
	}

	sql := strings.Replace(build.String(), " 1=1 AND ", " ", 1)

	rows, err := SqlFactory{}.Raw(sql, where).Rows()

	defer rows.Close()

	if nil != err {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7002, "查询发生异常:", err)
	}

	res := SqlFactory{}.ScanRows2mapI(rows)
	if res == nil {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7003, "查询发生异常:", err)
	}

	if len(res) < 1 {
		return MsgEmity{}.Err(7004, "数据不存在！")
	}

	return MsgEmity{}.Success(res[0]["iCount"], "查询成功")
}

/**
 * 以事务方式执行多个sql
 * 注意:Mapper必须存在才能执行
 * @param sqls [sql, params]
 * @return MsgEmity 一个执行失败则回滚
 */
func (dao CommonDao) TransactionSql(sqls map[string]map[string]interface{}) *MsgEmity {
	if len(sqls) < 1 {
		return MsgEmity{}.Err(7001, "没有需要执行的sql")
	}

	tx := SqlFactory{}.GetDB().Begin() // 开始事务

	for sql, params := range sqls {
		if err := tx.Exec(sql, params).Error; err == nil {
			continue
		}

		Log.Error("sql执行失败:", sql)
		tx.Rollback() //回滚事务
		return MsgEmity{}.Err(7002, "没有需要执行的sql")
	}

	tx.Commit() //提交事务

	return MsgEmity{}.Success(7999, "事务执行成功")
}

/**
 * 根据字段名取分组数据
 * @param entity 实体类
 * @param sCreator 指定用户
 * @param fields 字段名与别名对象
 * @return
 */
func (dao CommonDao) GroupByField(entity interface{}, sCreator string, fields map[string]string, currentUser string, onlyCreator bool) *MsgEmity {
	if len(fields) < 1 {
		return MsgEmity{}.Err(7001, "没有对应的待查字段！")
	}

	array := map[string]interface{}{}
	for key, value := range fields {
		array[key] = value
	}

	array = ReflectUtils{}.HoldByEntity(entity, array) //按实体保留map中的数据

	tableInfo := TableInfo{}.GetByEntity(entity)

	var build strings.Builder
	build.WriteString("SELECT @@")

	for key, val := range array {
		build.WriteString(",")
		build.WriteString(key)
		build.WriteString(" AS ")
		build.WriteString(val.(string))
	}

	build.WriteString(" FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" GROUP BY @@")

	for key, _ := range fields {
		build.WriteString(",")
		build.WriteString(key)
	}

	if "" != sCreator {
		build.WriteString(" AND ")
		build.WriteString(TableCreatorName)
		build.WriteString("=@sCreator")
	}

	where := map[string]interface{}{}
	if onlyCreator {
		build.WriteString(" AND ")
		build.WriteString(TableCreatorName)
		build.WriteString("=@sCreator")
		where["sCreator"] = currentUser
	}

	sql := strings.Replace(build.String(), " @@,", " ", -1)

	rows, err := SqlFactory{}.Raw(sql, where).Rows()

	defer rows.Close()

	if nil != err {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7003, "查询发生异常:", err)
	}

	res := SqlFactory{}.ScanRows2mapI(rows)
	if res == nil {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7004, "查询发生异常:", err)
	}

	if len(res) < 1 {
		return MsgEmity{}.Err(7005, "数据不存在！")
	}

	return MsgEmity{}.Success(res, "查询成功")
}

/**
 * 取表中指定字段的最大值
 * @param entity 实体类
 * @param sCreator 指定用户
 * @param field 字段名
 * @param condition 查询条件字符串
 * @return
 */
func (dao CommonDao) MaxByField(entity interface{}, sCreator string, field string, where map[string]interface{}, currentUser string, onlyCreator bool) *MsgEmity {
	field = strings.TrimSpace(field)
	if "" == field {
		return MsgEmity{}.Err(7001, "没有待查字段！")
	}

	if len(where) < 1 {
		return MsgEmity{}.Err(7002, "没有对应的查询条件！")
	}

	if (!ReflectUtils{}.HasField(entity, field)) {
		return MsgEmity{}.Err(7003, "指定字段不存在！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	var build strings.Builder
	build.WriteString("SELECT MAX(")
	build.WriteString(field)
	build.WriteString(") AS data FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE 1=1")

	for key, _ := range where {
		build.WriteString(" AND ")
		build.WriteString(key)
		build.WriteString("=@")
		build.WriteString(key)
	}

	if "" == sCreator {
		build.WriteString(" AND ")
		build.WriteString(TableCreatorName)
		build.WriteString("=@sCreator")
	}

	if (!ModuleUtil{}.EnableTag(tableInfo.GsTableName, 4)) { //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆
		build.WriteString(" AND ")
		build.WriteString(TableCreatorName)
		build.WriteString("=@sCreator")
		where["sCreator"] = currentUser
	}

	sql := strings.Replace(build.String(), " 1=1 AND ", " ", 1)

	rows, err := SqlFactory{}.Raw(sql, where).Rows()
	defer rows.Close()

	if nil != err {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7004, "查询发生异常:", err)
	}

	res := SqlFactory{}.ScanRows2mapI(rows)
	if res == nil {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7005, "查询发生异常:", err)
	}

	if len(res) < 1 {
		return MsgEmity{}.Err(7006, "数据不存在！")
	}

	return MsgEmity{}.Success(res[0]["data"], "查询成功")
}

/**
 * 取表中指定字段的最小值
 * @param entity 实体类
 * @param sCreator 指定用户
 * @param field 字段名
 * @param whereStr 查询条件字符串
 * @return
 */
func (dao CommonDao) MinByField(entity interface{}, sCreator string, field string, where map[string]interface{}, currentUser string, onlyCreator bool) *MsgEmity {
	field = strings.TrimSpace(field)
	if "" == field {
		return MsgEmity{}.Err(7001, "没有待查字段！")
	}

	if len(where) < 1 {
		return MsgEmity{}.Err(7002, "没有对应的查询条件！")
	}

	if (!ReflectUtils{}.HasField(entity, field)) {
		return MsgEmity{}.Err(7003, "指定字段不存在！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	var build strings.Builder
	build.WriteString("SELECT MIN(")
	build.WriteString(field)
	build.WriteString(") AS data FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE 1=1")

	for key, _ := range where {
		build.WriteString(" AND ")
		build.WriteString(key)
		build.WriteString("=@")
		build.WriteString(key)
	}

	if "" == sCreator {
		build.WriteString(" AND ")
		build.WriteString(TableCreatorName)
		build.WriteString("=@sCreator")
	}

	if onlyCreator {
		build.WriteString(" AND ")
		build.WriteString(TableCreatorName)
		build.WriteString("=@sCreator")
		where["sCreator"] = currentUser
	}

	sql := strings.Replace(build.String(), " 1=1 AND ", " ", 1)

	rows, err := SqlFactory{}.Raw(sql, where).Rows()

	defer rows.Close()

	if nil != err {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7004, "查询发生异常:", err)
	}

	res := SqlFactory{}.ScanRows2mapI(rows)
	if res == nil {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7005, "查询发生异常:", err)
	}

	if len(res) < 1 {
		return MsgEmity{}.Err(7006, "数据不存在！")
	}

	return MsgEmity{}.Success(res[0]["data"], "查询成功")
}

/**
 * 检查关键值记录是否存在(返回1:存在;0:不存在)
 * @param entity 实体类
 * @Param id
 * @return MsgEmity
 */
func (dao CommonDao) HasById(entity interface{}, id interface{}) *MsgEmity {
	if "" == fmt.Sprintf("%v", id) {
		return MsgEmity{}.Err(7001, "记录编号参数为空！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	var build strings.Builder
	build.WriteString("SELECT COUNT(1) AS iCount FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE ")
	build.WriteString(tableInfo.GsKeyName)
	build.WriteString(" =@")
	build.WriteString(tableInfo.GsKeyName)

	where := map[string]interface{}{tableInfo.GsKeyName: id}

	var iCount int
	dbResult := SqlFactory{}.Raw(build.String(), where).Find(&iCount)
	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return MsgEmity{}.Err(7003, "查询数据发生异常:", dbResult.Error)
	}

	if iCount == 0 {
		return MsgEmity{}.Err(0, "数据不存在！")
	}

	return MsgEmity{}.Success(1, "数据存在！")
}

/**
 * 检查关键值记录是否存在(返回1:存在;0:不存在)
 * @param entity 实体类
 * @Param keyName 字段名
 * @Param keyValue 字段值
 * @return MsgEmity
 */
func (dao CommonDao) HasByKey(entity interface{}, keyName string, keyValue interface{}) *MsgEmity {
	keyName = strings.TrimSpace(keyName)
	if "" == keyName {
		return MsgEmity{}.Err(7001, "字段名参数为空！")
	}

	if (nil == keyValue) || ("" == fmt.Sprintf("%v", keyValue)) || ("<nil>" == fmt.Sprintf("%v", keyValue)) {
		return MsgEmity{}.Err(7002, "字段值参数为空！")
	}

	if (!ReflectUtils{}.HasField(entity, keyName)) {
		return MsgEmity{}.Err(7003, "指定字段不存在！")
	}

	dataInfo := DataInfo{}.GetDataInfoByName(entity, keyName)
	if nil == dataInfo {
		return MsgEmity{}.Err(7004, "字段备注信息缺失")
	}

	switch dataInfo.GsDbFileType {
	case "int":
		temp, err := strconv.Atoi(keyValue.(string))
		if err != nil {
			return MsgEmity{}.Err(7005, "字段值参数为不符合规范！")
		}
		keyValue = temp
	case "bigint":
		temp, err := strconv.ParseInt(keyValue.(string), 10, 64)
		if err != nil {
			return MsgEmity{}.Err(7006, "字段值参数为不符合规范！")
		}
		keyValue = temp
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	var build strings.Builder
	build.WriteString("SELECT COUNT(1) AS iCount FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE ")
	build.WriteString(keyName)
	build.WriteString(" =@")
	build.WriteString(keyName)

	where := map[string]interface{}{keyName: keyValue}

	var iCount int
	dbResult := SqlFactory{}.Raw(build.String(), where).Find(&iCount)
	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return MsgEmity{}.Err(7007, "查询数据发生异常:", dbResult.Error)
	}

	if iCount == 0 {
		return MsgEmity{}.Err(0, "数据不存在！")
	}

	return MsgEmity{}.Success(1, "数据存在！")
}
