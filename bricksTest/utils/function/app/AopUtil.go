package app

import (
	"strings"

	. "gitee.com/tomatomeatman/golang-repository/bricks/model"
	"github.com/gin-gonic/gin"
)

var (
	beforeAopInfo = make(map[string][]func(ctx *gin.Context, params ...interface{}) *MsgEmity)
	afterAopInfo  = make(map[string][]func(ctx *gin.Context, params ...interface{}) *MsgEmity)
	aroundAopInfo = make(map[string][]func(ctx *gin.Context, params ...interface{}) *MsgEmity)
)

type AopUtil struct{}

/**
 * 注册Aop-函数执行前调用函数
 * @param funcName 被监听函数
 * @param doFunc 被调用函数
 * @return
 */
func (this AopUtil) RegisterBeforeAop(funcName string, doFunc func(ctx *gin.Context, params ...interface{}) *MsgEmity) {
	funcName = strings.TrimSpace(funcName)
	if "" == funcName {
		return
	}

	funcArray, ok := beforeAopInfo[funcName]
	if ok {
		funcArray = append(funcArray, doFunc)
		beforeAopInfo[funcName] = funcArray
		return
	}

	funcArray = []func(ctx *gin.Context, params ...interface{}) *MsgEmity{doFunc}
	beforeAopInfo[funcName] = funcArray
}

/**
 * 注册Aop-函数执行后调用函数
 * @param funcName 被监听函数
 * @param doFunc 被调用函数
 * @return
 */
func (this AopUtil) RegisterAfterAop(funcName string, doFunc func(ctx *gin.Context, params ...interface{}) *MsgEmity) {
	funcName = strings.TrimSpace(funcName)
	if "" == funcName {
		return
	}

	funcArray, ok := afterAopInfo[funcName]
	if ok {
		funcArray = append(funcArray, doFunc)
		afterAopInfo[funcName] = funcArray
		return
	}

	funcArray = []func(ctx *gin.Context, params ...interface{}) *MsgEmity{doFunc}
	afterAopInfo[funcName] = funcArray
}

/**
 * 注册Aop-函数执行中调用函数
 * @param funcName 被监听函数 xxservice.xx
 * @param doFunc 被调用函数
 * @return
 */
func (this AopUtil) RegisterAroundAop(funcName string, doFunc func(ctx *gin.Context, params ...interface{}) *MsgEmity) {
	funcName = strings.TrimSpace(funcName)
	if "" == funcName {
		return
	}

	funcArray, ok := aroundAopInfo[funcName]
	if ok {
		funcArray = append(funcArray, doFunc)
		aroundAopInfo[funcName] = funcArray
		return
	}

	funcArray = []func(ctx *gin.Context, params ...interface{}) *MsgEmity{doFunc}
	aroundAopInfo[funcName] = funcArray
}

/**
 * 调用Aop-函数执行前调用函数
 * @param funcName 被监听函数 xxservice.xx
 * @param doFunc 被调用函数
 * @return
 */
func (this AopUtil) CallBeforeFunc(funcName string, ctx *gin.Context, params ...interface{}) *MsgEmity {
	return this.callFunc(beforeAopInfo, funcName, ctx, params)
}

/**
 * 调用Aop-函数执行后调用函数
 * @param funcName 被监听函数 xxservice.xx
 * @param doFunc 被调用函数
 * @return
 */
func (this AopUtil) CallAfterFunc(funcName string, ctx *gin.Context, params ...interface{}) *MsgEmity {
	return this.callFunc(afterAopInfo, funcName, ctx, params)
}

/**
 * 调用Aop-函数执行中调用函数
 * @param funcName 被监听函数 xxservice.xx
 * @param doFunc 被调用函数
 * @return
 */
func (this AopUtil) CallAroundFunc(funcName string, ctx *gin.Context, params ...interface{}) *MsgEmity {
	return this.callFunc(aroundAopInfo, funcName, ctx, params)
}

/**
 * 调用Aop-函数执行中调用函数
 * @param aopInfo aop信息
 * @param funcName 被监听函数 xxservice.xx
 * @param doFunc 被调用函数
 * @return
 */
func (this AopUtil) callFunc(aopInfo map[string][]func(ctx *gin.Context, params ...interface{}) *MsgEmity,
	funcName string, ctx *gin.Context, params []interface{}) *MsgEmity {
	if "" == funcName {
		return MsgEmity{}.Success(1000, "函数名为空,不处理")
	}

	funcArray, ok := aopInfo[funcName]
	if !ok {
		return MsgEmity{}.Success(1001, "没有函数,不处理")
	}

	if len(funcArray) < 1 {
		return MsgEmity{}.Success(1002, "没有调用函数,结束AOP处理")
	}

	for _, fun := range funcArray {
		me := fun(ctx, params...)
		if !me.Gsuccess {
			return me
		}
	}

	return MsgEmity{}.Success(1003, "调用函数没有错误,结束AOP处理")
}
