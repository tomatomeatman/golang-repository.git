package bridge

import (
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/app"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/gin"
	"github.com/gin-gonic/gin"
)

// @Controller 模块管理控制器
type ModuleManageController struct {
}

/**
 * 初始化
 */
func init() {
	if (AppUtil{}.IsNotCloudApp()) { //没有启用分布式,则不能用桥接接口
		return
	}

	//-- 接口注册 --//
	GinUtil{}.RegisterController("/module/manage/find/tree", POST, ModuleManageController{}.FindByTree)
}

// #region @Api {title=查询树形结构数据}
// @param {name=sGroupName dataType=string paramType=query explain=指定节点名 required=false}
// @return {type=json explain=返回树型数据}
// @RequestMapping {name=FindByTree type=POST value=/module/manage/find/tree}
// #endregion
func (control ModuleManageController) FindByTree(ctx *gin.Context) interface{} {
	return ModuleManageService{}.FindByTree(ctx)
}
