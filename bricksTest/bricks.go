package main

import (
	"fmt"
	"net/http"

	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/app"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data"
)

//简化的权限信息实体
type SimpleRight struct {
	GsRightId        string `json:"sRightId" gorm:"column:sRightId; type:varchar"`               //用户编号
	GsEnglish        string `json:"sEnglish" gorm:"column:sEnglish; type:varchar"`               //权限标识名称
	GsControllerPath string `json:"sControllerPath" gorm:"column:sControllerPath; type:varchar"` //URL路径值
}

type TokenInfo struct {
	GetTokenUrl       string `json:"GetTokenUrl"`
	AppKey            string `json:"AppKey"`
	AppSecret         string `json:"AppSecret"`
	DeviceSerial      string `json:"DeviceSerial"`
	Protocol          string `json:"Protocol"`
	GetHistoryPlayUrl string `json:"GetHistoryPlayUrl"`
	CreateDate        int64  `json:"CreateDate"`
	AccessToken       string `json:"AccessToken"`
	ExpireTime        int64  `json:"ExpireTime"`
}

func main() {

	//text := "《复仇者联盟3：无限战争》是全片使用IMAX摄影机拍摄"
	//text1 := "在IMAX影院放映时"
	//text2 := "全片以上下扩展至IMAX 1.9：1的宽高比来呈现"
	//text4 := "《复仇者联盟4：内战》没看过"

	//// 将文档加入索引，docId 从1开始
	//data.RiotUtil{}.Add("test", "1", text)
	//data.RiotUtil{}.Add("test", "2", text1)
	//data.RiotUtil{}.Add("test", "3", text2)
	//data.RiotUtil{}.Add("test", "4", text4)

	//me := data.RiotUtil{}.Find("test", "复仇者", nil)
	//fmt.Println(me)

	str := data.AesUtil{}.Encrypt("123456", "passkey")
	fmt.Println(str)

	str = data.AesUtil{}.Decrypt(str, "passkey")
	fmt.Println(str)

	str = data.AesUtil{}.EncryptBase64("root", "brick")
	fmt.Println(str)
	str = data.AesUtil{}.DecryptBase64(str, "1122334455")
	fmt.Println(str)

	t1 := "2022-02-02 17:50:00"
	t2 := "2022-02-02 19:14:00"
	fmt.Println(data.TimeUtil{}.GapHour(t1, t2))

	t1 = "2022-02-02 17:50:00"
	t2 = "2022-02-06 19:14:00"
	fmt.Println(data.TimeUtil{}.GapDay(t1, t2))

	me := app.AppUtil{}.ToEntity(TokenInfo{})
	fmt.Println(me)

	wi := app.WebConfigInfo{}.Get()
	fmt.Println(wi)

	// data1, _ := ioutil.ReadFile("./temp/cache/TokenInfo.txt")
	// var pp TokenInfo

	// json.Unmarshal(data1, &pp)
	// fmt.Println(pp.GetTokenUrl)

	// me := data.JsonUtil{}.FormFile("./temp/cache/TokenInfo.txt", TokenInfo{})

	// tokenInfo := me.Gdata.(TokenInfo)
	// fmt.Println(tokenInfo.GetTokenUrl)

	// filePath := "./temp/cache/UserAndRight/00000001_Simpl.txt"

	// list := []SimpleRight{}
	// me := data.JsonUtil{}.FormFile(filePath, list)
	// fmt.Println(me.Gdata)

	// fmt.Println("-----------------------------------")

	// var list1 []SimpleRight
	// me1 := data.JsonUtil{}.FormFile(filePath, list1)
	// fmt.Println(me1.Gdata)

	// fmt.Println("-----------------------------------")

	// var list2 []SimpleRight
	// me2 := data.JsonUtil{}.FormFile(filePath, &list2)
	// fmt.Println(me2.Gdata)

	// fmt.Println(time.Now().Unix())
	// str := data.Md5{}.Lower("123456", "888")

	// fmt.Println(str)

	// //ioutil.WriteFile(path, []byte(str), 0666) // 保存到文件
	// //file.FileUtil{}.Save(str, "./temp/cache/Login/LoginCache.txt")
	// file.FileUtil{}.Save(str, "d:/LoginCache.txt")

	// err := ioutil.WriteFile("d:/LoginCache.txt", []byte(str), 0666) // 保存到文件
	// if err != nil {
	// 	fmt.Println(err)
	// }

	// fmt.Println(time.Now().Add(2).Unix())
	// model.SetupLogger()

	// Debug("程序启动")
	// Error("程序启动222222222222222")
	// initHttpServer()

}

//初始化httpService服务
func initHttpServer() {
	fmt.Println("================ 启动完毕,使用端口: 9000 ================")

	http.Handle("/", http.FileServer(http.Dir("./webroot/")))
	http.ListenAndServe(":9000", nil)
}
