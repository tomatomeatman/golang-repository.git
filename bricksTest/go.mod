module gitee.com/tomatomeatman/golang-repository/bricks

go 1.16

require (
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	github.com/energye/energy v1.109.1184 // indirect
	github.com/gin-contrib/static v0.0.1
	github.com/gin-gonic/gin v1.9.1
	github.com/glebarez/sqlite v1.9.0 // indirect
	github.com/go-ego/riot v0.0.0-20201013133145-f4c30acb3704
	github.com/kr/pretty v0.3.0 // indirect
	github.com/rogpeppe/go-internal v1.8.0 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/shopspring/decimal v1.3.1
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/ini.v1 v1.67.0
	gorm.io/driver/mysql v1.5.1
	gorm.io/driver/sqlite v1.5.2 // indirect
	gorm.io/gorm v1.25.2
)
