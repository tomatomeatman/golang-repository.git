package bridge

import (
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/app"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/gin"
	"github.com/gin-gonic/gin"
)

/**
 * 桥接代理请求控制器
 */
type BridgeController struct{}

/**
 * 初始化
 */
func init() {
	if (AppUtil{}.IsNotCloudSystem()) { //禁用桥接
		return
	}

	//-- 接口注册 --//
	go GinUtil{}.RegisterController("/proxy/*proxy", GET, BridgeController{}.ProxyGet)
	go GinUtil{}.RegisterController("/proxy/*proxy", POST, BridgeController{}.ProxyPost)
}

func (control BridgeController) ProxyGet(ctx *gin.Context) interface{} {
	return BridgeDao{}.ProxyGet(ctx)
}

func (control BridgeController) ProxyPost(ctx *gin.Context) interface{} {
	return BridgeDao{}.ProxyPost(ctx)
}
