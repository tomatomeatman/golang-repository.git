package bridge

import (
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/app"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/url"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/gin"
	"github.com/gin-gonic/gin"
)

// @Controller 桥接服务-免拦截桥接操作接口
type IgnoreURLController struct {
}

/**
 * 初始化
 */
func init() {
	if (AppUtil{}.IsNotCloudSystem()) { //禁用桥接
		return
	}

	//-- 接口注册 --//
	go GinUtil{}.RegisterController("/ignore/url/find/id", POST, IgnoreURLController{}.FindById)
	go GinUtil{}.RegisterController("/ignore/url/check", POST, IgnoreURLController{}.CheckIgnoreUrl)
	go GinUtil{}.RegisterController("/ignore/url/clear/cache", POST, IgnoreURLController{}.ClearCache)
}

// #region @Api {title=根据记录编号取对象}
// @param {name=sId dataType=string paramType=query explain=记录编号 required=true}
// @return {type=json explain=返回对象}
// @RequestMapping {name=FindById type=POST value=/ignore/url/find/id}
// #endregion
func (control IgnoreURLController) FindById(ctx *gin.Context) interface{} {
	sId := UrlUtil{}.GetParam(ctx, "sId", "").(string)
	return IgnoreURLService{}.FindById(ctx, sId)
}

// #region @Api {title=内部方法:验证url是否可免于拦截,注意:限制为内部系统(生产者系统)访问}
// @param {name=sUrl dataType=string paramType=query explain=待检验的url required=true}
// @param {name=iMustLogin dataType=int paramType=query explain=是否必须登录,1:是;2:否 required=false}
// @param {name=sUserType dataType=string paramType=query explain=待检验的用户类型 required=false}
// @return {type=bool explain=返回对象}
// @RequestMapping {name=CheckIgnoreUrl type=POST value=/ignore/url/check}
// #endregion
func (control IgnoreURLController) CheckIgnoreUrl(ctx *gin.Context) interface{} {
	sUrl := UrlUtil{}.GetParam(ctx, "sDateSt", "").(string)
	iMustLogin := UrlUtil{}.GetParam(ctx, "iMustLogin", 2).(int)
	sUserType := UrlUtil{}.GetParam(ctx, "sDateSt", "").(string)
	return IgnoreURLService{}.CheckIgnoreUrl(ctx, sUrl, iMustLogin, sUserType)
}

// #region @Api {title=清理缓存}
// @return {type=json explain=返回对象}
// @RequestMapping {name=ClearCache type=POST value=/ignore/url/clear/cache}
// #endregion
func (control IgnoreURLController) ClearCache(ctx *gin.Context) interface{} {
	return IgnoreURLService{}.ClearCache(ctx)
}
