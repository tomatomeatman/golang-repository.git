package bridge

import (
	. "gitee.com/tomatomeatman/golang-repository/bricks/model"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/app"
	"github.com/gin-gonic/gin"
)

/**
 * 拦截器忽略路径接口桥接业务操作结构体
 */
type IgnoreURLService struct{}

var (
	ignoreURLServerName = "" //桥接到登录服务的服务名,默认GBaseSystemServer
	//ignoreURLServerKey  = "" //桥接到登录服务的服务密钥
)

// 初始化
func init() {
	if (AppUtil{}.IsNotCloudSystem()) { //禁用桥接
		return
	}

	ignoreURLServerName = AppUtil{}.ReadConfigKey("CloudServer", "BaseServerName", "GBaseSystemServer").(string)
	//ignoreURLServerKey = AppUtil{}.ReadConfigKey("CloudServer", "BaseServerKey", "").(string)
}

/**
 * 根据记录编号取对象
 * @param sId 记录编号
 * @return
 */
func (service IgnoreURLService) FindById(ctx *gin.Context, sId string) *MsgEmity {
	m := map[string]interface{}{"sId": sId}

	me := BridgeDao{}.Post(ctx, ignoreURLServerName, "/ignore/url/find/id", m, &MsgEmity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*MsgEmity)
}

/**
 * 验证指定url是否在可忽略的访问路径中(给内部拦截器用,直接返回Boolean)
 * @param sUrl 待检验的url
 * @param isMustLogin 是否必须登录
 * @return
 */
func (service IgnoreURLService) CheckIgnoreUrl(ctx *gin.Context, sUrl string, iMustLogin int, sUserType string) bool {
	m := map[string]interface{}{
		"sUrl":       sUrl,
		"iMustLogin": iMustLogin,
		"sUserType":  sUserType,
	}

	me := BridgeDao{}.Post(ctx, ignoreURLServerName, "/ignore/url/check", m, &MsgEmity{})
	if !me.Gsuccess { //Post请求未成功
		return false
	}

	me = me.Gdata.(*MsgEmity)
	if !me.Gsuccess { //"在指定项目中都没有找到可以免拦截的约定"
		return false
	}

	return me.Gdata.(bool)
}

/**
 * 清理缓存
 * @return
 */
func (service IgnoreURLService) ClearCache(ctx *gin.Context) *MsgEmity {
	me := BridgeDao{}.Post(ctx, ignoreURLServerName, "/ignore/url/clear/cache", nil, "")
	if !me.Gsuccess {
		return me
	}

	return me
}
