package model

// 实体接口定义,用于规范实体结构体
type Entity interface {
	New() Entity                         //创建结构实体,并赋予默认值
	SetDefault()                         //设置默认值
	TableName() string                   //结构体映射表名
	BaseColumnNames() []string           //结构体映射表的字段名集合
	Info() *TableInfo                    //结构体映射表简化信息
	GetDataInfo(name string) *DataInfo   //取数据结构信息
	AllDataInfo(name string) []*DataInfo //取所有数据结构信息
}
