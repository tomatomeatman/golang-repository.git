package model

type GlobalFunc struct{}

// var (
// 	globalFunc = make(map[string]func) //注册的全局函数集合
// )

// //注册全局变量
// func (gv GlobalFunc) RegisterVariable(name string, data interface{}) interface{} {
// 	_, ok := globalFunc[name]
// 	if ok {
// 		Log.Error("全局变量重复:" + name)
// 		return data
// 	}

// 	globalFunc[name] = data

// 	return data
// }

// //取指定全局变量
// func (gv GlobalFunc) Get(name string) interface{} {
// 	name = strings.TrimSpace(name)
// 	if "" == name {
// 		return nil
// 	}

// 	result, ok := globalFunc[name]
// 	if !ok {
// 		return nil
// 	}

// 	return result
// }

// //删除指定全局变量
// func (gv GlobalFunc) Del(name string) bool {
// 	name = strings.TrimSpace(name)
// 	if "" == name {
// 		return false
// 	}

// 	_, ok := globalFunc[name]
// 	if !ok {
// 		return true
// 	}

// 	delete(globalFunc, name)

// 	_, ok = globalFunc[name]
// 	if !ok {
// 		return true
// 	}

// 	return false
// }
