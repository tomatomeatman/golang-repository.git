package model

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
)

// 文件结构体,用于http返回传递消息
type FileEmity struct {
	Gsuccess     bool        `json:"success"`
	Gmsg         string      `json:"msg"`
	Gdata        interface{} `json:"data"` //[]byte
	Gdisposition string      `json:"disposition"`
	Gtype        string      `json:"type"`
	Glength      int         `json:"length"`
	GsLength     string      `json:"sLength"`
}

/**
 * 构造消息结构体,返回包含'错误信息'的结构体
 * @param data 码值
 * @param msg 描述信息
 * @return 返回新创建的结构体
 */
func (fm FileEmity) Err(data int, msg ...interface{}) *FileEmity {
	if len(msg) == 0 {
		return &FileEmity{false, "", data, "", "", 0, "0"}
	}

	var build strings.Builder
	for _, v := range msg {
		build.WriteString(fmt.Sprintf("%v", v))
	}

	return &FileEmity{false, build.String(), data, "", "", 0, "0"}
}

/**
 * 返回包含'错误信息'的结构体Json字符串
 * @param data 码值
 * @param msg 描述信息
 * @return 返回Json字符串
 */
func (fm FileEmity) ErrString(data int, msg ...interface{}) string {
	var build strings.Builder
	for _, v := range msg {
		build.WriteString(fmt.Sprintf("%v", v))
	}

	return fm.Err(data, build.String()).ToStr()
}

/**
 * 构造消息结构体,返回包含'正确信息'的结构体
 * @param data 码值|数据
 * @param msg 描述信息
 * @return 返回新创建的结构体
 */
func (fm FileEmity) Success(data interface{}, disposition, sType string, msg ...interface{}) *FileEmity {
	if len(msg) == 0 {
		iL := len(data.([]byte))
		return &FileEmity{true, "", data, disposition, sType, iL, strconv.Itoa(iL)}
	}

	var build strings.Builder
	for _, v := range msg {
		build.WriteString(fmt.Sprintf("%v", v))
	}

	iL := len(data.([]byte))
	return &FileEmity{true, build.String(), data, disposition, sType, iL, strconv.Itoa(iL)}
}

/**
 * 重设'返回数据'
 * data 码值|数据
 * @return 返回原结构体 m
 */
func (fm *FileEmity) SetData(data interface{}) *FileEmity {
	fm.Gdata = data

	return fm
}

/**
 * 重设'描述信息'
 * msg 描述信息
 * @return 返回原结构体 m
 */
func (fm *FileEmity) SetMsg(msg ...interface{}) *FileEmity {
	result := fm
	if len(msg) == 0 {
		return result
	}

	var build strings.Builder
	for _, v := range msg {
		build.WriteString(fmt.Sprintf("%v", v))
	}

	result.Gmsg = build.String()

	return result
}

/**
 * 累加'码值'
 * iCode 被累加值
 * @return 返回原结构体 m
 */
func (fm *FileEmity) IncCode(iCode int) *FileEmity {
	result := fm

	if result.Gsuccess { //成功情况下不允许累加值
		return fm
	}

	iData, err := strconv.Atoi(fmt.Sprintf("%v", result.Gdata)) //防止返回的类型未float64
	if err != nil {
		iData = 0
	}

	result.Gdata = iData + iCode

	return result
}

/**
 * 累加'码值'
 * iCode 被累加值
 * @return 返回原结构体 m
 */
func (fm *FileEmity) IncData(iCode int) int {
	result := fm

	if result.Gsuccess { //成功情况下不允许累加值
		return 0
	}

	if iCode == 0 {
		return fm.Gdata.(int)
	}

	iData, err := strconv.Atoi(fmt.Sprintf("%v", fm.Gdata)) //防止返回的类型未float64
	if err != nil {
		iData = 0
	}

	return iData + iCode
}

/**
 * 在描述信息后面累加'描述信息'
 * msg 描述信息
 * @return 返回原结构体 m
 */
func (fm *FileEmity) AppendMsg(msg ...interface{}) *FileEmity {
	result := fm

	var build strings.Builder
	build.WriteString(result.Gmsg)

	for _, v := range msg {
		build.WriteString(fmt.Sprintf("%v", v))
	}

	result.Gmsg = build.String()

	return result
}

/**
 * 在描述信息前面插入'描述信息'
 * msg 描述信息
 * @return 返回原结构体 m
 */
func (fm *FileEmity) InsertMsg(msg ...interface{}) *FileEmity {
	result := fm
	if len(msg) == 0 {
		return result
	}

	var build strings.Builder

	for _, v := range msg {
		build.WriteString(fmt.Sprintf("%v", v))
	}

	build.WriteString(result.Gmsg)

	result.Gmsg = build.String()

	return result
}

/**
 * 将结构体转换成json字符串输出
 * @return 返回json结构字符串
 */
func (fm *FileEmity) ToStr() string {
	ret_json, _ := json.Marshal(fm)

	return string(ret_json)
}
