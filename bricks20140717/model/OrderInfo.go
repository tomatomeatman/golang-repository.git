package model

import "strings"

//排序信息
type OrderInfo struct {
	GsOrderDbName string `json:"sOrderDbName"` //排序字段名所属库名
	GsOrderTable  string `json:"sOrderTable"`  //排序字段名所属表
	GsOrderField  string `json:"sOrderField"`  //排序字段名
	GsDirection   string `json:"sDirection"`   //排序方式(升序ASC 降序DESC)
	GiIndex       int    `json:"iIndex"`       //排序顺序
}

//要被过滤的特殊字符串,用'/'分隔
const SPECIAL_STR = "/INTEGER/STRING/INT/NULL/LONG/FLOAT/DOUBLE/FUNCTION/" // /Integer/string/int/null/long/float/double/function/

/**
 * 设置 排序字段名所属库名
 * @param sOrderDbName 排序字段名所属库名
 */
func (o *OrderInfo) SetsOrderDbName(sOrderDbName string) {
	sOrderDbName = strings.TrimSpace(sOrderDbName)
	if "" == sOrderDbName {
		return
	}

	if strings.Contains(SPECIAL_STR, strings.ToUpper(sOrderDbName)) { //排除一些不可能使用的字符串，如在使用swagger操作时，默认赋值string
		return
	}

	o.GsOrderDbName = sOrderDbName
}

/**
 * 设置 排序字段名所属表
 * @param sOrderTable 排序字段名所属表
 */
func (o *OrderInfo) SetsOrderTable(sOrderTable string) {
	sOrderTable = strings.TrimSpace(sOrderTable)
	if "" == sOrderTable {
		return
	}

	if strings.Contains(SPECIAL_STR, strings.ToUpper(sOrderTable)) { //排除一些不可能使用的字符串，如在使用swagger操作时，默认赋值string
		return
	}

	o.GsOrderTable = sOrderTable
}

/**
 * 设置 排序字段名
 * @param sOrderField 排序字段名
 */
func (o *OrderInfo) SetsOrderField(sOrderField string) {
	sOrderField = strings.TrimSpace(sOrderField)
	if "" == sOrderField {
		return
	}

	if strings.Contains(SPECIAL_STR, strings.ToUpper(sOrderField)) { //排除一些不可能使用的字符串，如在使用swagger操作时，默认赋值string
		return
	}

	o.GsOrderField = sOrderField
}

/**
 * 设置 排序方式(升序ASC 降序DESC)
 * @param sDirection 排序方式(升序ASC 降序DESC)
 */
func (o *OrderInfo) SetsDirection(sDirection string) {
	sDirection = strings.TrimSpace(sDirection)
	if "" == sDirection {
		return
	}

	if ("ASC" != strings.ToUpper(sDirection)) && ("DESC" != strings.ToUpper(sDirection)) { //排除一些不可能使用的字符串，如在使用swagger操作时，默认赋值string
		return
	}

	o.GsDirection = sDirection
}

/**
 * 输出排序字符串
 */
func (o *OrderInfo) String() string {
	if "" == o.GsOrderField {
		return "" //如果字段为空则直接为空
	}

	var build strings.Builder

	if ("" != o.GsOrderDbName) && ("" != o.GsOrderTable) {
		build.WriteString(o.GsOrderDbName)
		build.WriteString(".")
	}

	if "" != o.GsOrderTable {
		build.WriteString(o.GsOrderTable)
		build.WriteString(".")
	}

	build.WriteString(o.GsOrderField)

	if "" != o.GsDirection {
		build.WriteString(" ")
		build.WriteString(o.GsDirection)
	} else {
		build.WriteString(" ASC")
	}

	return build.String()
}
