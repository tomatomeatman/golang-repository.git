package monitor

import (
	"os"
	"strings"

	. "gitee.com/tomatomeatman/golang-repository/bricks/model"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/system"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/gin"
	"github.com/gin-gonic/gin"
)

// @Controller 监控信息控制器
type MonitorController struct{}

/**
 * 初始化
 */
func init() {

	//-- 接口注册 --//
	go GinUtil{}.RegisterController("/monitor/load", POST, MonitorController{}.Load)
	go GinUtil{}.RegisterController("/monitor/logs", POST, MonitorController{}.Logs)
	go GinUtil{}.RegisterController("/monitor/pollcode", POST, MonitorController{}.CreatePollCode)
}

// #region @Api {title=读取系统硬件信息}
// @return {type=json explainType=MsgEmity<map> explain=返回对象}
// @RequestMapping {name=Load type=POST value=/monitor/load}
// #endregion
func (control MonitorController) Load(ctx *gin.Context) interface{} {
	result := map[string]interface{}{}

	result["AppMemory"] = SystemUtil{}.AppMemoryInfo()
	result["SystemDisk"] = SystemUtil{}.GetDiskInfo()
	result["SystemMemory"] = SystemUtil{}.GetMemInfo()
	result["CPU"] = SystemUtil{}.GetCpuInfo()
	result["OS"] = SystemUtil{}.GetOsInfo()

	return MsgEmity{}.Success(result, "获取系统信息成功")
}

// #region @Api {title=读取系统日志}
// @return {type=json explainType=MsgEmity<string> explain=返回对象}
// @RequestMapping {name=Load type=POST value=/monitor/logs}
// #endregion
func (control MonitorController) Logs(ctx *gin.Context) interface{} {
	sType := ctx.Request.Header.Get("sType")
	if strings.TrimSpace(sType) == "" {
		sType = "error"
	}

	f, err := os.ReadFile("./logs/" + strings.ToLower(sType) + ".log")
	if nil != err {
		return MsgEmity{}.Err(9003, "读取日志文件失败")
	}

	return MsgEmity{}.Success(string(f), "读取日志文件成功")
}

// #region @Api {title=取系统序列号}
// @return {type=json explainType=MsgEmity<string> explain=返回对象}
// @RequestMapping {name=CreatePollCode type=POST value=/monitor/pollcode}
// #endregion
func (control MonitorController) CreatePollCode(ctx *gin.Context) interface{} {
	result := SystemUtil{}.GetSystemSerial()

	return MsgEmity{}.Success(result, "获取系统序列号成功")
}
