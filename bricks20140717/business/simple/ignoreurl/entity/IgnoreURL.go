package entity

import (
	. "gitee.com/tomatomeatman/golang-repository/bricks/model"
)

var IgnoreURL_tableName = ""

/**
 * 拦截器忽略路径'IgnoreURL'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type IgnoreURL struct {
	IgnoreURLBase
	GsMustLoginText string `json:"sMustLoginText" gorm:"<-:false;column:sMustLoginText"` //必须登录(布尔值,1:是;2:否)
}

/**
 * 结构体映射表名,处理结构体名称与表名不一致的情况
 */
func (iu IgnoreURL) TableName() string {
	return iu.IgnoreURLBase.TableName()
}

/**
 * 结构体映射表简化信息
 */
func (iu IgnoreURL) Info() *TableInfo {
	return iu.IgnoreURLBase.Info()
}

/**
 * 创建结构实体,并赋予默认值
 */
func (IgnoreURL) New() IgnoreURL {
	result := IgnoreURL{}
	result.IgnoreURLBase = IgnoreURLBase{}.New()

	return result
}

/**
 * 设置默认值
 */
func (iu IgnoreURL) SetDefault() {
	iu.IgnoreURLBase.SetDefault()
}
