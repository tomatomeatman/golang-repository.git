package entity

import (
	"strings"
	"time"

	. "gitee.com/tomatomeatman/golang-repository/bricks/model"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/gorm"
)

var IgnoreURLBase_tableName = ""

/**
 * 拦截器忽略路径'IgnoreURL'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type IgnoreURLBase struct {
	GsId           string    `json:"sId" gorm:"column:sId; type:varchar; NOT NULL; primary_key" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"IgnoreURL\", \"sName\":\"sId\", \"sComment\":\"记录编号\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\": \"\", \"bExtra\":false, \"bkey\":true, \"bNull\":false,\"iMaxLength\":8, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":0}"`                                        //记录编号
	GsUrlName      string    `json:"sUrlName" gorm:"column:sUrlName; type:varchar; NOT NULL" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"IgnoreURL\", \"sName\":\"sUrlName\", \"sComment\":\"路径名称类别\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\": \"\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":50, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":1}"`                                  //路径名称类别
	GsIgnoreUrl    string    `json:"sIgnoreUrl" gorm:"column:sIgnoreUrl; type:varchar; NOT NULL" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"IgnoreURL\", \"sName\":\"sIgnoreUrl\", \"sComment\":\"路径集合\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\": \"\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":5000, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":2}"`                            //路径集合
	GiMustLogin    int       `json:"iMustLogin" gorm:"column:iMustLogin; type:int; NOT NULL; DEFAULT '1'" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"IgnoreURL\", \"sName\":\"iMustLogin\", \"sComment\":\"必须登录(布尔值,1:是;2:否)\", \"bDbField\":true, \"sDbFileType\":\"int\", \"sDefaultData\": 1, \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":11, \"bDecimal\":false, \"iIntegralLength\":11, \"iDecimalLength\":0, \"iIndex\":3}"`              //必须登录(布尔值,1:是;2:否)
	GsOnlyUserType string    `json:"sOnlyUserType" gorm:"column:sOnlyUserType; type:varchar" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"IgnoreURL\", \"sName\":\"sOnlyUserType\", \"sComment\":\"限用户类型(分号分隔)\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\": \"\", \"bExtra\":false, \"bkey\":false, \"bNull\":true,\"iMaxLength\":500, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":4}"`                        //限用户类型(分号分隔)
	GsMemo         string    `json:"sMemo" gorm:"column:sMemo; type:varchar" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"IgnoreURL\", \"sName\":\"sMemo\", \"sComment\":\"备注\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\": \"\", \"bExtra\":false, \"bkey\":false, \"bNull\":true,\"iMaxLength\":200, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":5}"`                                                         //备注
	GsCreator      string    `json:"sCreator" gorm:"column:sCreator; type:varchar; NOT NULL; DEFAULT '00000000'" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"IgnoreURL\", \"sName\":\"sCreator\", \"sComment\":\"创建者\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\": \"00000000\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":8, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":6}"`          //创建者
	GdCreateDate   time.Time `json:"dCreateDate" gorm:"column:dCreateDate; type:datetime; NOT NULL" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"IgnoreURL\", \"sName\":\"dCreateDate\", \"sComment\":\"创建时间\", \"bDbField\":true, \"sDbFileType\":\"datetime\", \"sDefaultData\": \"\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":23, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":7}"`                         //创建时间
	GsModifieder   string    `json:"sModifieder" gorm:"column:sModifieder; type:varchar; NOT NULL; DEFAULT '00000000'" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"IgnoreURL\", \"sName\":\"sModifieder\", \"sComment\":\"修改人\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\": \"00000000\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":8, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":8}"` //修改人
	GdModifiedDate time.Time `json:"dModifiedDate" gorm:"column:dModifiedDate; type:datetime; NOT NULL" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"IgnoreURL\", \"sName\":\"dModifiedDate\", \"sComment\":\"修改时间\", \"bDbField\":true, \"sDbFileType\":\"datetime\", \"sDefaultData\": \"\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":23, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":9}"`                   //修改时间
	GiState        int       `json:"iState" gorm:"column:iState; type:int; NOT NULL; DEFAULT '1'" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"IgnoreURL\", \"sName\":\"iState\", \"sComment\":\"版本\", \"bDbField\":true, \"sDbFileType\":\"int\", \"sDefaultData\": 1, \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":11, \"bDecimal\":false, \"iIntegralLength\":11, \"iDecimalLength\":0, \"iIndex\":10}"`                                        //版本
	GiIndex        int       `json:"iIndex" gorm:"column:iIndex; type:int; NOT NULL; DEFAULT '1'" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"IgnoreURL\", \"sName\":\"iIndex\", \"sComment\":\"序号\", \"bDbField\":true, \"sDbFileType\":\"int\", \"sDefaultData\": 1, \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":11, \"bDecimal\":false, \"iIntegralLength\":11, \"iDecimalLength\":0, \"iIndex\":11}"`                                        //序号
	GiVersion      int       `json:"iVersion" gorm:"column:iVersion; type:int; NOT NULL; DEFAULT '1'" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"IgnoreURL\", \"sName\":\"iVersion\", \"sComment\":\"版本号\", \"bDbField\":true, \"sDbFileType\":\"int\", \"sDefaultData\": 1, \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":11, \"bDecimal\":false, \"iIntegralLength\":11, \"iDecimalLength\":0, \"iIndex\":12}"`                                 //版本号
}

/**
 * 初始化
 */
func init() {
	TableInfo{}.RegisterEntity("IgnoreURLBase", IgnoreURLBase{})           //注册注册数据库实体类结构体
	TableInfo{}.RegisterTableInfo("IgnoreURLBase", IgnoreURLBase{}.Info()) //注册数据库表信息
}

func (obj IgnoreURLBase) BaseEntity() IgnoreURLBase {
	return obj.New()
}

/**
 * 创建结构实体,并赋予默认值
 */
func (IgnoreURLBase) New() IgnoreURLBase {
	return IgnoreURLBase{
		GsId:           "",
		GsUrlName:      "",
		GsIgnoreUrl:    "",
		GiMustLogin:    1,
		GsOnlyUserType: "",
		GsMemo:         "",
		GsCreator:      "00000000",
		GdCreateDate:   time.Now(),
		GsModifieder:   "00000000",
		GdModifiedDate: time.Now(),
		GiState:        1,
		GiIndex:        1,
		GiVersion:      1,
	}
}

/**
 * 设置默认值
 */
func (iu *IgnoreURLBase) SetDefault() {
	iu.GsId = ""
	iu.GsUrlName = ""
	iu.GsIgnoreUrl = ""
	iu.GiMustLogin = 1
	iu.GsOnlyUserType = ""
	iu.GsMemo = ""
	iu.GsCreator = "00000000"
	iu.GdCreateDate = time.Now()
	iu.GsModifieder = "00000000"
	iu.GdModifiedDate = time.Now()
	iu.GiState = 1
	iu.GiIndex = 1
	iu.GiVersion = 1
}

/**
 * 结构体映射表名,处理结构体名称与表名不一致的情况
 */
func (IgnoreURLBase) TableName() string {
	if IgnoreURLBase_tableName != "" {
		return IgnoreURLBase_tableName
	}

	IgnoreURLBase_tableName = SqlFactory{}.GetDbName("BaseSystem") + "IgnoreURL"

	return IgnoreURLBase_tableName
}

/**
 * 结构体映射表的字段名集合
 */
func (IgnoreURLBase) BaseColumnNames() []string {
	return strings.Split("sId,sUrlName,sIgnoreUrl,iMustLogin,sOnlyUserType,sMemo,sCreator,dCreateDate,sModifieder,dModifiedDate,iState,iIndex,iVersion", ",")
}

/**
 * 结构体映射表简化信息
 */
func (IgnoreURLBase) Info() *TableInfo {
	return &TableInfo{
		GsDbName:          "",
		GsTableName:       "IgnoreURL",
		GsKeyName:         "sId",
		GiKeyLen:          8,
		GbAutoKey:         false,
		GbHasPid:          false,
		GbHasPath:         false,
		GbHasRecordKey:    false,
		GbHasMemo:         true,
		GbHasCreator:      true,
		GbHasCreateDate:   true,
		GbHasModifieder:   true,
		GbHasModifiedDate: true,
		GbHasState:        true,
		GbHasIndex:        true,
		GbHasVersion:      true,
		GbHasPassword:     false,
		GbHasSign:         false,
		GbHasOnlyign:      false,
		GbHasDelSign:      false,
	}
}
