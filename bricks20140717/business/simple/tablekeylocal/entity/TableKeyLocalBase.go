package entity

import (
	"strings"

	"gitee.com/tomatomeatman/golang-repository/bricks/model"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/app"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/gorm"
)

var (
	TableKeyLocalBase_tableName = ""
	TableKeyLocalBase_dbName    = ""
)

/**
 * 记录编号序列管理表'TableKeyLocal'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type TableKeyLocalBase struct {
	GiId      int64  `json:"iId" gorm:"column:iId; type:bigint; NOT NULL; DEFAULT '0'; primary_key" dataInfo:"{\"sDbName\":\"BaseSystem\", \"sTableName\":\"TableKeyLocal\", \"sName\":\"iId\", \"sComment\":\"记录编号(非自增长)\", \"bDbField\":true, \"sDbFileType\":\"bigint\", \"sDefaultData\": 0, \"bExtra\":false, \"bkey\":true, \"bNull\":false,\"iMaxLength\":20, \"bDecimal\":false, \"iIntegralLength\":20, \"iDecimalLength\":0, \"iIndex\":0}"` //记录编号(非自增长)
	GsType    string `json:"sType" gorm:"column:sType; type:varchar; NOT NULL" dataInfo:"{\"sDbName\":\"BaseSystem\", \"sTableName\":\"TableKeyLocal\", \"sName\":\"sType\", \"sComment\":\"类型\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\": \"\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":30, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":1}"`                        //类型
	GsValue   string `json:"sValue" gorm:"column:sValue; type:varchar; NOT NULL" dataInfo:"{\"sDbName\":\"BaseSystem\", \"sTableName\":\"TableKeyLocal\", \"sName\":\"sValue\", \"sComment\":\"值\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\": \"\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":30, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":2}"`                      //值
	GiIndex   int    `json:"iIndex" gorm:"column:iIndex; type:int; DEFAULT '0'" dataInfo:"{\"sDbName\":\"BaseSystem\", \"sTableName\":\"TableKeyLocal\", \"sName\":\"iIndex\", \"sComment\":\"序号\", \"bDbField\":true, \"sDbFileType\":\"int\", \"sDefaultData\": 0, \"bExtra\":false, \"bkey\":false, \"bNull\":true,\"iMaxLength\":11, \"bDecimal\":false, \"iIntegralLength\":11, \"iDecimalLength\":0, \"iIndex\":3}"`                             //序号
	GiVersion int    `json:"iVersion" gorm:"column:iVersion; type:int; NOT NULL; DEFAULT '1'" dataInfo:"{\"sDbName\":\"BaseSystem\", \"sTableName\":\"TableKeyLocal\", \"sName\":\"iVersion\", \"sComment\":\"版本号\", \"bDbField\":true, \"sDbFileType\":\"int\", \"sDefaultData\": 1, \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":11, \"bDecimal\":false, \"iIntegralLength\":11, \"iDecimalLength\":0, \"iIndex\":4}"`           //版本号
}

/**
 * 初始化
 */
func init() {
	TableKeyLocalBase_tableName = app.AppUtil{}.ReadConfigKey("DbVariables", "TableKeyName", "TableKeyLocal").(string)
	TableKeyLocalBase_dbName = app.AppUtil{}.ReadConfigKey("DbVariables", "MainDb", "BaseSystem").(string)
	model.TableInfo{}.RegisterEntity(TableKeyLocalBase_tableName+"Base", TableKeyLocalBase{})           //注册注册数据库实体类结构体
	model.TableInfo{}.RegisterTableInfo(TableKeyLocalBase_tableName+"Base", TableKeyLocalBase{}.Info()) //注册数据库表信息
}

/**
 * 创建结构实体,并赋予默认值
 */
func (TableKeyLocalBase) New() TableKeyLocalBase {
	return TableKeyLocalBase{
		GiId:      0,
		GsType:    "",
		GsValue:   "",
		GiIndex:   0,
		GiVersion: 1,
	}
}

/**
 * 设置默认值
 */
func (o *TableKeyLocalBase) SetDefault() {
	o.GiId = 0
	o.GsType = ""
	o.GsValue = ""
	o.GiIndex = 0
	o.GiVersion = 1
}

/**
 * 结构体映射表名,处理结构体名称与表名不一致的情况
 */
func (TableKeyLocalBase) TableName() string {
	if TableKeyLocalBase_tableName != "" {
		return TableKeyLocalBase_tableName
	}

	TableKeyLocalBase_tableName = gorm.SqlFactory{}.GetDbName(TableKeyLocalBase_dbName) + "." + TableKeyLocalBase_tableName
	TableKeyLocalBase_tableName = strings.Replace(TableKeyLocalBase_tableName, "..", ".", -1)

	return TableKeyLocalBase_tableName
}

/**
 * 结构体映射表的字段名集合
 */
func (TableKeyLocalBase) BaseColumnNames() []string {
	return strings.Split("iId,sType,sValue,iIndex,iVersion", ",")
}

/**
 * 结构体映射表简化信息
 */
func (TableKeyLocalBase) Info() *model.TableInfo {
	return &model.TableInfo{
		GsDbName:          TableKeyLocalBase_dbName,
		GsTableName:       TableKeyLocalBase_tableName,
		GsKeyName:         "iId",
		GiKeyLen:          20,
		GbAutoKey:         false,
		GbHasPid:          false,
		GbHasPath:         false,
		GbHasRecordKey:    false,
		GbHasMemo:         false,
		GbHasCreator:      false,
		GbHasCreateDate:   false,
		GbHasModifieder:   false,
		GbHasModifiedDate: false,
		GbHasState:        false,
		GbHasIndex:        true,
		GbHasVersion:      true,
		GbHasPassword:     false,
		GbHasSign:         false,
		GbHasOnlyign:      false,
		GbHasDelSign:      false,
	}
}
