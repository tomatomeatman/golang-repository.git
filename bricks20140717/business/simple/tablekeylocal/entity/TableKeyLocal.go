package entity

import (
	"gitee.com/tomatomeatman/golang-repository/bricks/model"
)

/**
 * 记录编号序列管理表'TableKeyLocal'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type TableKeyLocal struct {
	TableKeyLocalBase
}

/**
 * 结构体映射表名,处理结构体名称与表名不一致的情况
 */
func (o TableKeyLocal) TableName() string {
	return o.TableKeyLocalBase.TableName()
}

/**
 * 结构体映射表简化信息
 */
func (o TableKeyLocal) Info() *model.TableInfo {
	return o.TableKeyLocalBase.Info()
}

/**
 * 创建结构实体,并赋予默认值
 */
func (TableKeyLocal) New() TableKeyLocal {
	result := TableKeyLocal{}
	result.TableKeyLocalBase = TableKeyLocalBase{}.New()

	return result
}

/**
 * 设置默认值
 */
func (o TableKeyLocal) SetDefault() {
	o.TableKeyLocalBase.SetDefault()
}
