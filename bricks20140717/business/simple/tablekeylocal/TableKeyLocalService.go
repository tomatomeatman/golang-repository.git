package tablekeylocal

import (
	. "gitee.com/tomatomeatman/golang-repository/bricks/model"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/app"
)

/**
 * 记录编号序列管理表TableKeyLocal表基本业务操作结构体
 */
type TableKeyLocalService struct {
	CommonService
}

/**
 * 初始化
 */
func init() {
	go GlobalVariable{}.RegisterVariable("TableKeyLocalService", TableKeyLocalService{})
}

/**
 * 取各表(或序列)的新Id
 * @param formatLong 格式化长度(不足长度+0)
 * @param seriesName 表名或序列名
 * @return
 */
func (TableKeyLocalService) GetNewId(formatLong int, seriesName string) string {
	return TableKeyLocalDao{}.GetNewId(formatLong, seriesName)
}

/**
 * 取各表的一批新Id
 * @param formatLong 格式化长度(不足长度+0)
 * @param seriesName 表名或序列名
 * @param size 数量
 * @return
 */
func (TableKeyLocalService) GetNewIds(formatLong int, seriesName string, size int) []string {
	return TableKeyLocalDao{}.GetNewIds(formatLong, seriesName, size)
}

/**
 * 重置
 * @param seriesName 表名或序列名
 * @return
 */
func (TableKeyLocalService) Reset(seriesName string) string {
	return TableKeyLocalDao{}.Reset(seriesName)
}
