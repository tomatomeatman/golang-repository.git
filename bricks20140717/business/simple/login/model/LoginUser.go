package model

/**
 * 登录缓存对象
 * 非数据库实体
 */
type LoginUser struct {
	GsId       string            `json:"sId" gorm:"column:sId; type:varchar"`       //用户编号
	GsName     string            `json:"sName" gorm:"column:sName; type:varchar"`   //姓名
	GsNo       string            `json:"sNo" gorm:"column:sNo; type:varchar"`       //工号
	GsPass     string            `json:"-" gorm:"column:sPass; type:varchar"`       //密码
	GsSignPass string            `json:"-" gorm:"column:sSignPass; type:varchar"`   //手势密码
	GsType     string            `json:"sType" gorm:"column:sType; type:varchar"`   //用户类型编号
	GsOwner    string            `json:"sOwner" gorm:"column:sOwner; type:varchar"` //用户来源
	GiState    int               `json:"iState" gorm:"column:iState; type:int"`     //状态(-1,人员表人员被删除,1表示正常状态,2人员停用,)
	GdLastDate int64             `json:"dLastDate"`                                 //最后访问时间
	GsCookie   string            `json:"sCookie"`                                   //分配的Cookie
	GiDevice   int               `json:"iDevice"`                                   //设备类型,1:PC,2:手机,3:平板,4.....
	Gattached  map[string]string `json:"attached" gorm:"-"`                         //与登录相关的附属信息
}

/**
 * 复制
 * @return
 */
func (lu *LoginUser) Clone() LoginUser {
	result := LoginUser{}
	result.GsId = lu.GsId
	result.GsName = lu.GsName
	result.GsNo = lu.GsNo
	result.GsPass = lu.GsPass
	result.GsSignPass = lu.GsSignPass
	result.GsType = lu.GsType
	result.GsOwner = lu.GsOwner
	result.GiState = lu.GiState
	result.GdLastDate = lu.GdLastDate
	result.GsCookie = lu.GsCookie

	if len(lu.Gattached) > 0 {
		result.Gattached = map[string]string{}
		for key, val := range lu.Gattached {
			result.Gattached[key] = val
		}
	}

	return result
}
