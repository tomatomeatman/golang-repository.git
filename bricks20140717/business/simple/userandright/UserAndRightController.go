package userandright

import (
	. "gitee.com/tomatomeatman/golang-repository/bricks/business/simple/userandright/entity"

	. "gitee.com/tomatomeatman/golang-repository/bricks/model"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/app"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/url"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/gin"
	"github.com/gin-gonic/gin"
)

// @Controller 用户权限控制器
type UserAndRightController struct {
	CommonController                     //通用控制层
	ModuleEntity     UserAndRight        //对应模块数据实体
	ModuleService    UserAndRightService //对应模块业务实体
}

/**
 * 初始化
 */
func init() {

	//-- 控制操作,首位9不参与用7个数代表开关,0为不限制,1为限制 --//
	//-- 7个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆、物理删除 --//
	//-- 其中前5个为'是否仅创建者可操作'后续为其它控制开关 --//
	go GlobalVariable{}.RegisterVariable("UserAndRight_ModuleEnable", 9000001)

	//-- 检查待新增内容是否存在重复数据(多字段组合重复即重复)集合 --//
	go GlobalVariable{}.RegisterVariable("UserAndRight_CheckRepeatCombination", []string{"sUserId", "sRightId"})

	//-- 检查待新增内容是否存在重复数据(单独字段重复即重复)集合,注意:int必须是1、10、100、1000 --//
	//GlobalVariable{}.RegisterVariable("IgnoreURL_CheckRepeatAlone", map[string]int{})

	//-- 记录编号值前缀,此属性用于给id字段添加前缀,以便于在分布式系统中进行数据合并 --//
	//-- 注意:必须规划好各个模块的前缀,如果没有数据合并的需要则不需要设置,默认没有前缀 --//
	//-- 前缀只有1个字符 --//
	//GlobalVariable{}.RegisterVariable("IgnoreURL_IdValuePrefix", "")

	//-- 接口注册 --//
	go GinUtil{}.ControllerRegister("/user/and/right/add", UserAndRightController{}.Add, POST)
	go GinUtil{}.ControllerRegister("/user/and/right/del", UserAndRightController{}.Del, POST)
	go GinUtil{}.ControllerRegister("/user/and/right/edit", UserAndRightController{}.Edit, POST)
	go GinUtil{}.ControllerRegister("/user/and/right/find/id", UserAndRightController{}.FindById, POST)
	go GinUtil{}.ControllerRegister("/user/and/right/find/key", UserAndRightController{}.FindByKey, POST)
	go GinUtil{}.ControllerRegister("/user/and/right/find/page/row", UserAndRightController{}.FindByRow, POST)
	go GinUtil{}.ControllerRegister("/user/and/right/find/page", UserAndRightController{}.FindByPage, POST)
	go GinUtil{}.ControllerRegister("/user/and/right/check/right", UserAndRightController{}.CheckRight, POST)
	go GinUtil{}.ControllerRegister("/user/and/right/lasttime", UserAndRightController{}.LastTime, POST)
	go GinUtil{}.ControllerRegister("/user/and/right/find/rightid", UserAndRightController{}.FindRightId, POST)
	go GinUtil{}.ControllerRegister("/user/and/right/find/english", UserAndRightController{}.FindEnglishByUserId, POST)
}

// #region @Api {title=新增}
// @param {name=data dataType=json paramType=body explain=UserAndRight结构数据 explainType=UserAndRight required=true}
// @return {type=json explainType=MsgEmity<UserAndRight> explain=返回对象}
// @RequestMapping {name=Add type=POST value=/user/and/right/add}
// #endregion
func (control UserAndRightController) Add(ctx *gin.Context) interface{} {
	return ControllerUtil{}.Add(ctx, &control)
}

// #region @Api {title=删除数据}
// @param {name=iId dataType=int64 paramType=query explain=记录编号 required=true}
// @param {name=iVersion dataType=int paramType=query explain=版本号 required=true}
// @return {type=json explainType=MsgEmity<int> explain=返回影响数}
// @RequestMapping {name=Del type=POST value=/user/and/right/del}
// #endregion
func (control UserAndRightController) Del(ctx *gin.Context) interface{} {
	return ControllerUtil{}.Del(ctx, &control)
}

// #region @Api {title=修改数据}
// @param {name=data dataType=json paramType=body explain=UserAndRight结构数据 explainType=UserAndRight required=true}
// @return {type=json explainType=MsgEmity<int> explain=返回码值}
// @RequestMapping {name=Edit type=POST value=/user/and/right/edit}
// #endregion
func (control UserAndRightController) Edit(ctx *gin.Context) interface{} {
	return ControllerUtil{}.Edit(ctx, &control)
}

// #region @Api {title=查询全部}
// @param {name=data dataType=json paramType=body explain=map[string]interface结构数据 explainType=UserAndRight required=true}
// @return {type=json explainType=MsgEmity<UserAndRight> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindAll type=POST value=/user/and/right/find/all}
// #endregion
func (control UserAndRightController) FindAll(ctx *gin.Context) interface{} {
	return ControllerUtil{}.FindAll(ctx, &control)
}

// #region @Api {title=查询指定时间内数据}
// @param {name=sDateSt dataType=string paramType=query explain=格式:YYYY-MM-DD HH:mm:ss required=true}
// @param {name=sDateEd dataType=string paramType=query explain=格式:YYYY-MM-DD HH:mm:ss required=true}
// @return {type=json explainType=MsgEmity<UserAndRight> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByDate type=POST value=/user/and/right/find/date}
// #endregion
func (control UserAndRightController) FindByDate(ctx *gin.Context) interface{} {
	return ControllerUtil{}.FindByDate(ctx, &control)
}

// #region @Api {title=根据记录编号取对象}
// @param {name=iId dataType=int64 paramType=query explain=记录编号 required=true}
// @return {type=json explainType=MsgEmity<UserAndRight> explain=返回对象}
// @RequestMapping {name=FindById type=POST value=/user/and/right/find/id}
// #endregion
func (control UserAndRightController) FindById(ctx *gin.Context) interface{} {
	return ControllerUtil{}.FindById(ctx, &control)
}

// #region @Api {title=根据关键值取对象集合}
// @param {name=data dataType=json paramType=body explain=UserAndRight结构数据 explainType=UserAndRight required=true}
// @return {type=json explainType=MsgEmity<UserAndRight> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByKey type=POST value=/user/and/right/find/key}
// #endregion
func (control UserAndRightController) FindByKey(ctx *gin.Context) interface{} {
	return ControllerUtil{}.FindByKey(ctx, &control)
}

// #region @Api {title=根据记录编号查询符合分页数据的某条记录}
// @param {name=iId dataType=int64 paramType=query explain=记录编号 required=true}
// @return {type=json explainType=MsgEmity<UserAndRight> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByRow type=POST value=/user/and/right/find/page/row}
// #endregion
func (control UserAndRightController) FindByRow(ctx *gin.Context) interface{} {
	return ControllerUtil{}.FindByRow(ctx, &control)
}

// #region @Api {title=查询分页数据}
// @param {name=data dataType=json paramType=body explain=FindByPageParam结构数据 explainType=FindByPageParam<UserAndRight> required=true}
// @return {type=json explainType=MsgEmity<Page<UserAndRight>> explain=返回分页数据}
// @RequestMapping {name=FindByPage type=POST value=/user/and/right/find/page}
// #endregion
func (control UserAndRightController) FindByPage(ctx *gin.Context) interface{} {
	return ControllerUtil{}.FindByPage(ctx, &control)
}

// #region @Api {title=权限验证 explain=验证指定用户是否有访问指定url的权限(内部拦截器用)}
// @param {name=sUserId dataType=string paramType=query explain=用户编号 required=false}
// @param {name=url dataType=string paramType=query explain=权限值 required=true}
// @RequestMapping {name=FindByPage type=POST value=/user/and/right/check/right}
// #endregion
func (control UserAndRightController) CheckRight(ctx *gin.Context) interface{} {
	sUserId := UrlUtil{}.GetParam(ctx, "sUserId", "").(string)
	url := UrlUtil{}.GetParam(ctx, "url", "").(string)

	return UserAndRightService{}.CheckRight(ctx, sUserId, url)
}

// #region @Api {title=缓存权限时间 explain=根据用户查询用户所拥有的权限的最后更新时间}
// @param {name=sUserId dataType=string paramType=query explain=用户编号 required=true}
// @RequestMapping {name=FindByPage type=POST value=/user/and/right/lasttime}
// #endregion
func (control UserAndRightController) LastTime(ctx *gin.Context) interface{} {
	sUserId := UrlUtil{}.GetParam(ctx, "sUserId", "").(string)

	return UserAndRightService{}.LastTime(ctx, sUserId)
}

// #region @Api {title=查询用户权限 explain=根据用户查询用户所拥有的权限编号集合}
// @param {name=sUserId dataType=string paramType=query explain=用户编号 required=true}
// @RequestMapping {name=FindByPage type=POST value=/user/and/right/find/rightid}
// #endregion
func (control UserAndRightController) FindRightId(ctx *gin.Context) interface{} {
	sUserId := UrlUtil{}.GetParam(ctx, "sUserId", "").(string)

	return UserAndRightService{}.FindRightId(sUserId)
}

// #region @Api {title=查询权限标识字符串 explain=根据用户取权限标识字符串(一个权限标识代表了多个可访问的url路径)}
// @param {name=sUserId dataType=string paramType=query explain=用户编号 required=true}
// @RequestMapping {name=FindByPage type=POST value=/user/and/right/find/english}
// #endregion
func (control UserAndRightController) FindEnglishByUserId(ctx *gin.Context) interface{} {
	sUserId := UrlUtil{}.GetParam(ctx, "sUserId", "").(string)

	return UserAndRightService{}.FindEnglishByUserId(sUserId)
}
