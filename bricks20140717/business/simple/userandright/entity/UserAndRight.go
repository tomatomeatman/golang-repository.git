package entity

import (
	. "gitee.com/tomatomeatman/golang-repository/bricks/model"
)

var UserAndRight_tableName = ""

/**
 * 用户权限表'UserAndRight'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type UserAndRight struct {
	UserAndRightBase
}

/**
 * 结构体映射表名,处理结构体名称与表名不一致的情况
 */
func (o UserAndRight) TableName() string {
	return o.UserAndRightBase.TableName()
}

/**
 * 结构体映射表简化信息
 */
func (o UserAndRight) Info() *TableInfo {
	return o.UserAndRightBase.Info()
}

/**
 * 创建结构实体,并赋予默认值
 */
func (UserAndRight) New() UserAndRight {
	result := UserAndRight{}
	result.UserAndRightBase = UserAndRightBase{}.New()

	return result
}

/**
 * 设置默认值
 */
func (o *UserAndRight) SetDefault() {
	o.UserAndRightBase.SetDefault()
}
