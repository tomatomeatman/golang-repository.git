package entity

//简化的权限信息实体
type SimpleRight struct {
	GsRightId        string `json:"sRightId" gorm:"column:sRightId; type:varchar"`               //用户编号
	GsEnglish        string `json:"sEnglish" gorm:"column:sEnglish; type:varchar"`               //权限标识名称
	GsControllerPath string `json:"sControllerPath" gorm:"column:sControllerPath; type:varchar"` //URL路径值
}
