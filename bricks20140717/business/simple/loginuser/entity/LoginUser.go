package entity

import (
	"gitee.com/tomatomeatman/golang-repository/bricks/model"
)

/**
 * 用户表'LoginUser'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type LoginUser struct {
	LoginUserBase
	GsStateText string `json:"sStateText" gorm:"<-:false;column:sStateText"` //状态(枚举,1:启用;2:禁用)
}

/**
 * 结构体映射表名,处理结构体名称与表名不一致的情况
 */
func (lu LoginUser) TableName() string {
	return lu.LoginUserBase.TableName()
}

/**
 * 结构体映射表简化信息
 */
func (lu LoginUser) Info() *model.TableInfo {
	return lu.LoginUserBase.Info()
}

/**
 * 创建结构实体,并赋予默认值
 */
func (LoginUser) New() LoginUser {
	result := LoginUser{}
	result.LoginUserBase = LoginUserBase{}.New()
	result.GsStateText = ""

	return result
}

/**
 * 设置默认值
 */
func (lu LoginUser) SetDefault() {
	lu.LoginUserBase.SetDefault()
}
