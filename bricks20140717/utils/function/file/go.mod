module gitee.com/tomatomeatman/golang-repository/bricks/utils/function/file

go 1.21.6

require (
	gitee.com/tomatomeatman/golang-repository/bricks/model v0.0.0-20240612071459-29e8b6c7b371
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	github.com/mholt/archiver v3.1.1+incompatible
	github.com/nwaples/rardecode v1.1.3
	gopkg.in/ini.v1 v1.67.0
)

require (
	github.com/dsnet/compress v0.0.1 // indirect
	github.com/frankban/quicktest v1.14.6 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/pierrec/lz4 v2.6.1+incompatible // indirect
	github.com/stretchr/testify v1.9.0 // indirect
	github.com/ulikunitz/xz v0.5.12 // indirect
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8 // indirect
)
