package file

import (
	"fmt"
	"io"
	"os"
	"strings"

	"gitee.com/tomatomeatman/golang-repository/bricks/model"
	"github.com/mholt/archiver"
	"github.com/nwaples/rardecode"
)

type RarUtil struct {
}

/**
 * 读取指定(序号)文件
 * source 源文件
 * iIndex 序号
 * *model.MsgEmity (content []byte, fileName string)
 */
func (RarUtil) Read(source string, iIndex int) *model.MsgEmity {
	source = strings.TrimSpace(source)

	if _, err := os.Stat(source); err != nil {
		if os.IsNotExist(err) {
			return model.MsgEmity{}.Err(1001, "源文件不存在")
		}

		return model.MsgEmity{}.Err(1002, "访问源文件异常:", err)
	}

	r := archiver.NewRar()

	var result []byte
	fileName := ""
	index := -1
	err := r.Walk(source, func(f archiver.File) error {
		index++

		if index != iIndex {
			return nil
		}

		rh, ok := f.Header.(*rardecode.FileHeader)
		if !ok {
			return fmt.Errorf("读取文件头失败")
		}
		// fmt.Println("FileName:", rh.Name)

		content, err := io.ReadAll(f)
		if err != nil {
			return err
		}

		result = content
		fileName = rh.Name

		return fmt.Errorf("找到")
	})

	if err != nil {
		return model.MsgEmity{}.Err(1003, "读取文件失败:", err)
	}

	if fileName == "" {
		return model.MsgEmity{}.Err(1004, "未找到文件")
	}

	return model.MsgEmity{}.Success(result, fileName)
}

/**
 * 读取指定(序号)文件
 * source 源文件
 * iIndex 序号
 * *model.MsgEmity (content []byte, fileName string)
 */
func (ru RarUtil) ReadByIndex(source string, iIndex int) *model.MsgEmity {
	return ru.Read(source, iIndex)
}

/**
 * 读取指定(名称)文件
 * source 源文件
 * iIndex 序号
 * *model.MsgEmity (content []byte, fileName string)
 */
func (RarUtil) ReadByName(source, sName string) *model.MsgEmity {
	source = strings.TrimSpace(source)

	if _, err := os.Stat(source); err != nil {
		if os.IsNotExist(err) {
			return model.MsgEmity{}.Err(1001, "源文件不存在")
		}

		return model.MsgEmity{}.Err(1002, "访问源文件异常:", err)
	}

	sName = strings.TrimSpace(sName)

	r := archiver.NewRar()

	var result []byte
	fileName := ""
	err := r.Walk(source, func(f archiver.File) error {
		rh, ok := f.Header.(*rardecode.FileHeader)
		if !ok {
			return fmt.Errorf("读取文件头失败")
		}

		if sName != rh.Name {
			return nil
		}
		// fmt.Println("FileName:", rh.Name)

		content, err := io.ReadAll(f)
		if err != nil {
			return err
		}

		result = content
		fileName = rh.Name

		return fmt.Errorf("找到")
	})

	if err != nil {
		return model.MsgEmity{}.Err(1003, "读取文件失败:", err)
	}

	if fileName == "" {
		return model.MsgEmity{}.Err(1004, "未找到文件:", sName)
	}

	return model.MsgEmity{}.Success(result, fileName)
}

/**
 * 解压文件
 * source 源文件
 * targe 目标路径
 * *model.MsgEmity (int, msg)
 */
func (RarUtil) Unpack(source, targe string) *model.MsgEmity {
	source = strings.TrimSpace(source)

	if _, err := os.Stat(source); err != nil {
		if os.IsNotExist(err) {
			return model.MsgEmity{}.Err(1001, "源文件不存在")
		}

		return model.MsgEmity{}.Err(1002, "访问源文件异常:", err)
	}

	targe = strings.TrimSpace(targe)

	r := archiver.NewRar()

	err := r.Unarchive(source, targe)
	if err != nil {
		return model.MsgEmity{}.Err(1003, "解压文件异常:", err)
	}

	return model.MsgEmity{}.Success(9999, "解压完毕")
}
