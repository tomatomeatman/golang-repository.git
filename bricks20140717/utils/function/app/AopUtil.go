package app

import (
	"strings"

	"gitee.com/tomatomeatman/golang-repository/bricks/model"
	"github.com/gin-gonic/gin"
)

var (
	beforeAopInfo = make(map[string][]func(ctx *gin.Context, params ...interface{}) *model.MsgEmity)
	afterAopInfo  = make(map[string][]func(ctx *gin.Context, params ...interface{}) *model.MsgEmity)
	aroundAopInfo = make(map[string][]func(ctx *gin.Context, params ...interface{}) *model.MsgEmity)
)

type AopUtil struct{}

/**
 * 注册Aop-函数执行前调用函数
 * @param funcName 被监听函数
 * @param doFunc 被调用函数
 * @return
 */
func (au AopUtil) RegisterBeforeAop(funcName string, doFunc func(ctx *gin.Context, params ...interface{}) *model.MsgEmity) {
	funcName = strings.TrimSpace(funcName)
	if funcName == "" {
		return
	}

	funcArray, ok := beforeAopInfo[funcName]
	if ok {
		funcArray = append(funcArray, doFunc)
		beforeAopInfo[funcName] = funcArray
		return
	}

	funcArray = []func(ctx *gin.Context, params ...interface{}) *model.MsgEmity{doFunc}
	beforeAopInfo[funcName] = funcArray
}

/**
 * 注册Aop-函数执行后调用函数
 * @param funcName 被监听函数
 * @param doFunc 被调用函数
 * @return
 */
func (au AopUtil) RegisterAfterAop(funcName string, doFunc func(ctx *gin.Context, params ...interface{}) *model.MsgEmity) {
	funcName = strings.TrimSpace(funcName)
	if funcName == "" {
		return
	}

	funcArray, ok := afterAopInfo[funcName]
	if ok {
		funcArray = append(funcArray, doFunc)
		afterAopInfo[funcName] = funcArray
		return
	}

	funcArray = []func(ctx *gin.Context, params ...interface{}) *model.MsgEmity{doFunc}
	afterAopInfo[funcName] = funcArray
}

/**
 * 注册Aop-函数执行中调用函数
 * @param funcName 被监听函数 xxservice.xx
 * @param doFunc 被调用函数
 * @return
 */
func (au AopUtil) RegisterAroundAop(funcName string, doFunc func(ctx *gin.Context, params ...interface{}) *model.MsgEmity) {
	funcName = strings.TrimSpace(funcName)
	if funcName == "" {
		return
	}

	funcArray, ok := aroundAopInfo[funcName]
	if ok {
		funcArray = append(funcArray, doFunc)
		aroundAopInfo[funcName] = funcArray
		return
	}

	funcArray = []func(ctx *gin.Context, params ...interface{}) *model.MsgEmity{doFunc}
	aroundAopInfo[funcName] = funcArray
}

/**
 * 调用Aop-函数执行前调用函数
 * @param funcName 被监听函数 xxservice.xx
 * @param doFunc 被调用函数
 * @return
 */
func (au AopUtil) CallBeforeFunc(funcName string, ctx *gin.Context, params ...interface{}) *model.MsgEmity {
	return au.callFunc(beforeAopInfo, funcName, ctx, params)
}

/**
 * 调用Aop-函数执行后调用函数
 * @param funcName 被监听函数 xxservice.xx
 * @param doFunc 被调用函数
 * @return
 */
func (au AopUtil) CallAfterFunc(funcName string, ctx *gin.Context, params ...interface{}) *model.MsgEmity {
	return au.callFunc(afterAopInfo, funcName, ctx, params)
}

/**
 * 调用Aop-函数执行中调用函数
 * @param funcName 被监听函数 xxservice.xx
 * @param doFunc 被调用函数
 * @return
 */
func (au AopUtil) CallAroundFunc(funcName string, ctx *gin.Context, params ...interface{}) *model.MsgEmity {
	return au.callFunc(aroundAopInfo, funcName, ctx, params)
}

/**
 * 调用Aop-函数执行中调用函数
 * @param aopInfo aop信息
 * @param funcName 被监听函数 xxservice.xx
 * @param doFunc 被调用函数
 * @return
 */
func (au AopUtil) callFunc(aopInfo map[string][]func(ctx *gin.Context, params ...interface{}) *model.MsgEmity,
	funcName string, ctx *gin.Context, params []interface{}) *model.MsgEmity {
	if funcName == "" {
		return model.MsgEmity{}.Success(1000, "函数名为空,不处理")
	}

	funcArray, ok := aopInfo[funcName]
	if !ok {
		return model.MsgEmity{}.Success(1001, "没有函数,不处理")
	}

	if len(funcArray) < 1 {
		return model.MsgEmity{}.Success(1002, "没有调用函数,结束AOP处理")
	}

	for _, fun := range funcArray {
		me := fun(ctx, params...)
		if !me.Gsuccess {
			return me
		}
	}

	return model.MsgEmity{}.Success(1003, "调用函数没有错误,结束AOP处理")
}
