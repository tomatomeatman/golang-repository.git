package data

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"strings"
)

type Md5 struct{}

//取MD5,大写
func (md Md5) Upper(str ...interface{}) string {
	var build strings.Builder

	for _, value := range str {
		build.WriteString(fmt.Sprintf("%v", value))
	}

	data := []byte(build.String())
	md5Ctx := md5.New()
	md5Ctx.Write(data)
	cipherStr := md5Ctx.Sum(nil)
	result := hex.EncodeToString(cipherStr)

	return strings.ToUpper(result)
}

//取MD5小写
func (md Md5) Lower(str ...interface{}) string {
	var build strings.Builder

	for _, value := range str {
		build.WriteString(fmt.Sprintf("%v", value))
	}

	data := []byte(build.String())
	md5Ctx := md5.New()
	md5Ctx.Write(data)
	cipherStr := md5Ctx.Sum(nil)
	result := hex.EncodeToString(cipherStr)

	return result
}
