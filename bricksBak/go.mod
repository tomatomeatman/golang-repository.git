module gitee.com/tomatomeatman/golang-repository/bricks

go 1.16

require (
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	github.com/gin-contrib/static v0.0.1 // indirect
	github.com/gin-gonic/gin v1.9.0 // indirect
	github.com/go-ego/riot v0.0.0-20201013133145-f4c30acb3704 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	gopkg.in/ini.v1 v1.67.0
	gorm.io/driver/mysql v1.3.6
	gorm.io/gorm v1.23.8
)
