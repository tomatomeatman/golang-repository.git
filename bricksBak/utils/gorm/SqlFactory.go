package gorm

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"os"
	"reflect"
	"strconv"
	"time"
	"unsafe"

	"net/http"

	. "gitee.com/tomatomeatman/golang-repository/bricks/model"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data"
	Log "github.com/cihub/seelog"
	"gopkg.in/ini.v1"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var (
	isInitEd       = 0                                                                     //是否已经进行初始化
	DB             *gorm.DB                                                                //数据库操作对象
	httpHandleFunc = make(map[string]func(http.ResponseWriter, *http.Request) interface{}) //http控制层注册
	dbVariables    map[string]string                                                       //数据库名称映射集合
)

type SqlFactory struct{}

//初始化
func init() {
	if isInitEd == 1 {
		return
	}

	SetupLogger()

	configFilePath := "./config/app.ini"

	_, err := os.Stat(configFilePath) //os.Stat获取文件信息
	if err != nil {
		if !os.IsExist(err) {
			Log.Error("配置文件不存在", err)
			os.Exit(1)
			return
		}
	}

	cfg, err := ini.Load(configFilePath)
	if err != nil {
		Log.Error("配置文件读取错误", err)
		os.Exit(1)
	}

	if !cfg.HasSection("DataSource") {
		Log.Debug("配置文件没有DataSource,不进行数据库连接")
		isInitEd = 1
		return
	}

	dbVariables = make(map[string]string)

	DbVariables := cfg.Section("DbVariables")
	for i := 0; i < len(DbVariables.Keys()); i++ {
		key := DbVariables.Keys()[i].Name()
		value, _ := DbVariables.GetKey(key)
		if value != nil {
			dbVariables[key] = value.String()
		}
	}

	DataSource := cfg.Section("DataSource")

	user := "root"
	value, _ := DataSource.GetKey("User")
	if value != nil {
		user = value.String()
	}

	pass := "view"
	value, _ = DataSource.GetKey("Pass")
	if value != nil {
		pass = value.String()
	}

	ip := "127.0.0.1"
	value, _ = DataSource.GetKey("Ip")
	if value != nil {
		ip = value.String()
	}

	port := "3306"
	value, _ = DataSource.GetKey("Port")
	if value != nil {
		port = value.String()
	}

	dbName := "mysql"
	value, _ = DataSource.GetKey("MainDb")
	if value != nil {
		dbName = value.String()
	}

	PrintSql := false
	value, _ = DataSource.GetKey("PrintSql")
	if value != nil {
		tmp, _ := value.Int()
		PrintSql = 1 == tmp
	}

	AutoMigrate := false //是否开启AutoMigrate功能(会创建表、缺失的外键、约束、列和索引)
	value, _ = DataSource.GetKey("AutoMigrate")
	if value != nil {
		tmp, _ := value.Int()
		AutoMigrate = 1 == tmp
	}

	//dsn := "root:view@tcp(127.0.0.1:3306)/Dev_BricksBaseSystem?charset=utf8mb4&parseTime=True&loc=Local"
	//dsn := user + ":" + pass + "@tcp(" + ip + ":" + port + ")/" + dbName + "?charset=utf8mb4&serverTimezone=GMT%2B8&parseTime=True&loc=Local"
	dsn := user + ":" + pass + "@tcp(" + ip + ":" + port + ")/" + dbName + "?charset=utf8mb4&parseTime=True&loc=Local"
	//dsn := user + ":" + pass + "@tcp(" + ip + ":" + port + ")/" + dbName + "?charset=utf8mb4&loc=Local"

	dbConfig := &gorm.Config{
		DisableForeignKeyConstraintWhenMigrating: !AutoMigrate,
	}

	if PrintSql {
		dbConfig.Logger = logger.Default.LogMode(logger.Info) //开启sql打印
	}

	DB, err = gorm.Open(mysql.Open(dsn), dbConfig)
	if err != nil {
		Log.Error("连接数据库错误", err)
		return
	}

	isInitEd = 1
}

//取gorm操作对象
func (sf SqlFactory) GetDB() *gorm.DB {
	return DB
}

//注册web接口
func (sf SqlFactory) RegisterController(url string, handler func(http.ResponseWriter, *http.Request) interface{}) {
	_, ok := httpHandleFunc[url]
	if ok {
		Log.Error("链接重复:" + url)
		return
	}

	httpHandleFunc[url] = handler
}

//取注册的web接口
func (sf SqlFactory) GetController() map[string]func(http.ResponseWriter, *http.Request) interface{} {
	return httpHandleFunc
}

//取注册的web接口
func (sf SqlFactory) GetHandleFunc(urlKey string, InterceptorFunc func(w http.ResponseWriter, r *http.Request) bool,
	controllerMap map[string]func(http.ResponseWriter, *http.Request) interface{}) func(w http.ResponseWriter, r *http.Request) {
	result := func(w http.ResponseWriter, r *http.Request) {
		//允许跨域访问
		(w).Header().Set("Access-Control-Allow-Origin", "*")
		(w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		(w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

		urlStr := r.URL.Path

		if nil != InterceptorFunc {
			if !InterceptorFunc(w, r) {
				return
			}
		}

		handler := controllerMap[urlKey] //给个默认
		vb := false
		for runKey := range controllerMap {
			if runKey != urlStr {
				continue
			}

			handler = controllerMap[runKey]
			vb = true
			break
		}

		if !vb {
			return
		}

		obj := handler(w, r)
		switch obj.(type) {
		case *MsgEmity, MsgEmity:
			w.Header().Set("content-type", "text/json")
			jsonStr, err := json.Marshal(obj)
			if err != nil {
				w.Write([]byte("{\"success\":false,\"msg\":\"执行结果转换发生错误\",\"data\":1008001}"))
				return
			}

			w.Write(jsonStr)
		default:
			fmt.Fprint(w, fmt.Sprintf("%v", obj))
		}
	}

	return result
}

//将查询结果转换成map数组,常用于原生sql查询
func (sf SqlFactory) ScanRows2map(rows *sql.Rows) []map[string]string {
	if nil == rows {
		return nil
	}

	res := make([]map[string]string, 0)               //  定义结果 map
	colTypes, _ := rows.ColumnTypes()                 // 列信息
	var rowParam = make([]interface{}, len(colTypes)) // 传入到 rows.Scan 的参数 数组
	var rowValue = make([]interface{}, len(colTypes)) // 接收数据一行列的数组

	for i, colType := range colTypes {
		rowValue[i] = reflect.New(colType.ScanType())           // 跟据数据库参数类型，创建默认值 和类型
		rowParam[i] = reflect.ValueOf(&rowValue[i]).Interface() // 跟据接收的数据的类型反射出值的地址
	}

	// 遍历
	for rows.Next() {
		rows.Scan(rowParam...) // 赋值到 rowValue 中
		record := make(map[string]string)
		for i, colType := range colTypes {
			if rowValue[i] == nil {
				record[colType.Name()] = ""
				continue
			}

			//如果字段类型为int,则需要进一步判断
			//并且1.如果获得值类型为int64,则需要按int64处理,常用于类似以下的查询:
			//rows, _ := SqlFactory{}.GetDB().Raw("select * from table where uId=@GuId and sName=@GsName", &where).Rows()
			//并且2.如果数据库类型虽然为INT,但获取的值被以string进行接收, 则要按字符串的方式进行,常用于类似以下的查询:
			//rows, _ := SqlFactory{}.GetDB().Raw("select * from Student").Rows()
			if colType.DatabaseTypeName() == "INT" { //
				switch rowValue[i].(type) {
				case int64: //
					record[colType.Name()] = strconv.FormatInt(int64(rowValue[i].(int64)), 10)
					continue
				}
			}

			if colType.DatabaseTypeName() == "DATETIME" {
				record[colType.Name()] = rowValue[i].(time.Time).Format("2006-01-02 15:04:05")
				continue
			}

			record[colType.Name()] = byte2Str(rowValue[i].([]byte))
		}
		res = append(res, record)
	}

	return res
}

//将查询结果转换成map数组,常用于原生sql查询
func (sf SqlFactory) ScanRows2mapI(rows *sql.Rows) []map[string]interface{} {
	if nil == rows {
		return nil
	}

	res := make([]map[string]interface{}, 0)          //  定义结果 map
	colTypes, _ := rows.ColumnTypes()                 // 列信息
	var rowParam = make([]interface{}, len(colTypes)) // 传入到 rows.Scan 的参数 数组
	var rowValue = make([]interface{}, len(colTypes)) // 接收数据一行列的数组

	for i, colType := range colTypes {
		rowValue[i] = reflect.New(colType.ScanType())           // 跟据数据库参数类型，创建默认值 和类型
		rowParam[i] = reflect.ValueOf(&rowValue[i]).Interface() // 跟据接收的数据的类型反射出值的地址
	}

	// 遍历
	for rows.Next() {
		rows.Scan(rowParam...) // 赋值到 rowValue 中
		record := make(map[string]interface{})
		for i, colType := range colTypes {
			if rowValue[i] == nil {
				record[colType.Name()] = ""
				continue
			}

			//如果字段类型为int,则需要进一步判断
			//并且1.如果获得值类型为int64,则需要按int64处理,常用于类似以下的查询:
			//rows, _ := SqlFactory{}.GetDB().Raw("select * from table where uId=@GuId and sName=@GsName", &where).Rows()
			//并且2.如果数据库类型虽然为INT,但获取的值被以string进行接收, 则要按字符串的方式进行,常用于类似以下的查询:
			//rows, _ := SqlFactory{}.GetDB().Raw("select * from Student").Rows()
			if colType.DatabaseTypeName() == "INT" { //
				switch value := rowValue[i].(type) {
				case int64:
					record[colType.Name()] = rowValue[i]
					//record[colType.Name()] = strconv.FormatInt(int64(rowValue[i].(int64)), 10)
					continue
				case string:
					record[colType.Name()] = IntegerUtil{}.ToInt64(rowValue[i], -99999)
					continue
				case []uint8:
					record[colType.Name()] = IntegerUtil{}.ToInt64(rowValue[i], -99999)
					continue
				default:
					fmt.Println(value)
				}
			}

			if colType.DatabaseTypeName() == "BIGINT" {
				switch value := rowValue[i].(type) {
				case int64:
					record[colType.Name()] = rowValue[i]
					//record[colType.Name()] = strconv.FormatInt(int64(rowValue[i].(int64)), 10)
					continue
				case string:
					record[colType.Name()] = IntegerUtil{}.ToInt64(rowValue[i], -99999)
					continue
				case []uint8:
					record[colType.Name()] = IntegerUtil{}.ToInt64(rowValue[i], -99999)
					continue
				default:
					fmt.Println(value)
				}
			}

			if colType.DatabaseTypeName() == "DATETIME" {
				record[colType.Name()] = rowValue[i]
				continue
			}

			record[colType.Name()] = byte2Str(rowValue[i].([]byte))
		}
		res = append(res, record)
	}

	return res
}

// Byte转Str
func byte2Str(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}

//取数据库名称
func (sf SqlFactory) GetDbName(name string) string {
	return sf.GetVariable(name)
}

//取数据库全局变量
func (sf SqlFactory) GetVariable(name string) string {
	if name == "" {
		return ""
	}

	for key := range dbVariables {
		if name == key {
			return dbVariables[key]
		}
	}

	return ""
}

//调用数据查询
func (sf SqlFactory) Raw(sql string, params ...interface{}) (tx *gorm.DB) {
	return sf.doDb(sql, params, DB.Raw)
}

//调用数据更新
func (sf SqlFactory) Exec(sql string, params ...interface{}) (tx *gorm.DB) {
	return sf.doDb(sql, params, DB.Exec)
}

//调用数据库操作
func (sf SqlFactory) doDb(sql string, param []interface{}, dbFunc func(sql string, values ...interface{}) (tx *gorm.DB)) (tx *gorm.DB) {
	if (nil == param) || (len(param) < 1) {
		return dbFunc(sql)
	}

	iCount := len(param)
	if iCount > 1 {
		return dbFunc(sql, param)
	}

	rtk := reflect.TypeOf(param[0]).Kind()
	if rtk == reflect.Map {
		s := reflect.ValueOf(param[0])
		if s.Len() < 1 {
			return dbFunc(sql)
		}

		return dbFunc(sql, param[0])
	}

	if (rtk != reflect.Slice) && (rtk != reflect.Array) {
		return dbFunc(sql, param[0])
	}

	params := []interface{}{}
	s := reflect.ValueOf(param[0])
	for i := 0; i < s.Len(); i++ {
		params = append(params, s.Index(i).Interface())
	}

	switch len(params) {
	case 0:
		return dbFunc(sql)
	case 1:
		return dbFunc(sql, params[0])
	case 2:
		return dbFunc(sql, params[0], params[1])
	case 3:
		return dbFunc(sql, params[0], params[1], params[2])
	case 4:
		return dbFunc(sql, params[0], params[1], params[2], params[3])
	case 5:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4])
	case 6:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5])
	case 7:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6])
	case 8:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7])
	case 9:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8])
	case 10:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9])
	case 11:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10])
	case 12:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11])
	case 13:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12])
	case 14:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13])
	case 15:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14])
	case 16:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15])
	case 17:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16])
	case 18:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17])
	case 19:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18])
	case 20:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19])
	case 21:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20])
	case 22:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21])
	case 23:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22])
	case 24:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23])
	case 25:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24])
	case 26:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25])
	case 27:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26])
	case 28:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27])
	case 29:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28])
	case 30:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29])
	case 31:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30])
	case 32:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31])
	case 33:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32])
	case 34:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33])
	case 35:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34])
	case 36:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35])
	case 37:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36])
	case 38:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37])
	case 39:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38])
	case 40:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39])
	case 41:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40])
	case 42:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41])
	case 43:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42])
	case 44:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43])
	case 45:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44])
	case 46:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45])
	case 47:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46])
	case 48:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47])
	case 49:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48])
	case 50:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49])
	case 51:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50])
	case 52:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51])
	case 53:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52])
	case 54:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53])
	case 55:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54])
	case 56:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55])
	case 57:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56])
	case 58:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57])
	case 59:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58])
	case 60:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59])
	case 61:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60])
	case 62:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61])
	case 63:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62])
	case 64:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63])
	case 65:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64])
	case 66:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65])
	case 67:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66])
	case 68:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67])
	case 69:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68])
	case 70:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69])
	case 71:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70])
	case 72:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71])
	case 73:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72])
	case 74:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73])
	case 75:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74])
	case 76:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75])
	case 77:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76])
	case 78:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77])
	case 79:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78])
	case 80:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79])
	case 81:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80])
	case 82:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80], params[81])
	case 83:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80], params[81], params[82])
	case 84:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80], params[81], params[82], params[83])
	case 85:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80], params[81], params[82], params[83], params[84])
	case 86:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80], params[81], params[82], params[83], params[84], params[85])
	case 87:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80], params[81], params[82], params[83], params[84], params[85], params[86])
	case 88:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80], params[81], params[82], params[83], params[84], params[85], params[86], params[87])
	case 89:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80], params[81], params[82], params[83], params[84], params[85], params[86], params[87], params[88])
	case 90:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80], params[81], params[82], params[83], params[84], params[85], params[86], params[87], params[88], params[89])
	case 91:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80], params[81], params[82], params[83], params[84], params[85], params[86], params[87], params[88], params[89], params[90])
	case 92:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80], params[81], params[82], params[83], params[84], params[85], params[86], params[87], params[88], params[89], params[90], params[91])
	case 93:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80], params[81], params[82], params[83], params[84], params[85], params[86], params[87], params[88], params[89], params[90], params[91], params[92])
	case 94:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80], params[81], params[82], params[83], params[84], params[85], params[86], params[87], params[88], params[89], params[90], params[91], params[92], params[93])
	case 95:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80], params[81], params[82], params[83], params[84], params[85], params[86], params[87], params[88], params[89], params[90], params[91], params[92], params[93], params[94])
	case 96:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80], params[81], params[82], params[83], params[84], params[85], params[86], params[87], params[88], params[89], params[90], params[91], params[92], params[93], params[94], params[95])
	case 97:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80], params[81], params[82], params[83], params[84], params[85], params[86], params[87], params[88], params[89], params[90], params[91], params[92], params[93], params[94], params[95], params[96])
	case 98:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80], params[81], params[82], params[83], params[84], params[85], params[86], params[87], params[88], params[89], params[90], params[91], params[92], params[93], params[94], params[95], params[96], params[97])
	case 99:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80], params[81], params[82], params[83], params[84], params[85], params[86], params[87], params[88], params[89], params[90], params[91], params[92], params[93], params[94], params[95], params[96], params[97], params[98])
	case 100:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80], params[81], params[82], params[83], params[84], params[85], params[86], params[87], params[88], params[89], params[90], params[91], params[92], params[93], params[94], params[95], params[96], params[97], params[98], params[99])
	case 101:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80], params[81], params[82], params[83], params[84], params[85], params[86], params[87], params[88], params[89], params[90], params[91], params[92], params[93], params[94], params[95], params[96], params[97], params[98], params[99], params[100])
	case 102:
		return dbFunc(sql, params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9], params[10], params[11], params[12], params[13], params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22], params[23], params[24], params[25], params[26], params[27], params[28], params[29], params[30], params[31], params[32], params[33], params[34], params[35], params[36], params[37], params[38], params[39], params[40], params[41], params[42], params[43], params[44], params[45], params[46], params[47], params[48], params[49], params[50], params[51], params[52], params[53], params[54], params[55], params[56], params[57], params[58], params[59], params[60], params[61], params[62], params[63], params[64], params[65], params[66], params[67], params[68], params[69], params[70], params[71], params[72], params[73], params[74], params[75], params[76], params[77], params[78], params[79], params[80], params[81], params[82], params[83], params[84], params[85], params[86], params[87], params[88], params[89], params[90], params[91], params[92], params[93], params[94], params[95], params[96], params[97], params[98], params[99], params[100], params[101])
	default:
		return dbFunc(sql, params)
	}
}
