package gin

import (
	"mime"
	"net/http"

	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
)

type GormUtil struct{}

//设置跨域
func (this GormUtil) SetCors(r *gin.Engine) {
	r.Use(this.Cors())
}

//跨域函数
func (this GormUtil) Cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		method := c.Request.Method
		origin := c.Request.Header.Get("Origin")
		if origin != "" {
			c.Header("Access-Control-Allow-Origin", "*") // 可将将 * 替换为指定的域名
			c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, UPDATE")
			//c.Header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
			//c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Cache-Control, Content-Language, Content-Type")
			c.Header("Access-Control-Allow-Headers", "*")
			c.Header("Access-Control-Expose-Headers", "*")
			c.Header("Access-Control-Allow-Credentials", "true")
		}
		if method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
		}
		c.Next()
	}
}

//设置静态路径
func (this GormUtil) SetStatic(r *gin.Engine, webroot string) {
	_ = mime.AddExtensionType(".js", "application/javascript") // 特殊处理JS文件的Content-Type
	r.Use(static.Serve("/", static.LocalFile(webroot, true)))  // 前端项目静态资源
}
