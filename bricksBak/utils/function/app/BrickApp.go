package app

import (
	"fmt"
	"net/http"

	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/gorm"
)

type BrickApp struct{}

/**
 * 程序运行
 * @param InterceptorFunc 拦截函数
 */
func (this BrickApp) Run(InterceptorFunc func(w http.ResponseWriter, r *http.Request) bool) {
	Root := AppUtil{}.ReadConfigKey("System", "Root", "/webroot/").(string)
	Port := AppUtil{}.ReadConfigKey("System", "Port", "8080").(string)
	Title := AppUtil{}.ReadConfigKey("System", "Title", "").(string)
	Name := AppUtil{}.ReadConfigKey("System", "Name", "").(string)

	go Cloud{}.RegistraCloud() //注册服务到注册中心

	urlMap := SqlFactory{}.GetController()
	http.Handle("/", http.FileServer(http.Dir("."+Root)))

	for key := range urlMap {
		http.HandleFunc(key, SqlFactory{}.GetHandleFunc(key, InterceptorFunc, urlMap))
	}

	fmt.Println(StringUtil{}.Append("================ ", Title, Name, "启动完毕,使用端口:", Port, " ================"))

	http.ListenAndServe(":"+Port, nil)
}
