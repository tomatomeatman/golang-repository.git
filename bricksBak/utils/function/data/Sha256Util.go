package data

import (
	"crypto/sha256"
	"fmt"
	"strings"
)

type Sha256Util struct{}

//加密
func (this Sha256Util) Encode(params ...interface{}) (Byte [32]byte, Str string) {
	if len(params) < 1 {
		return [32]byte{}, ""
	}

	var build strings.Builder

	for _, value := range params {
		build.WriteString(fmt.Sprintf("%v", value))
	}

	Byte = sha256.Sum256([]byte(build.String()))
	Str = fmt.Sprintf("%x", Byte)

	return
}
