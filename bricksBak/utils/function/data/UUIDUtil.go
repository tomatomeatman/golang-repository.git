package data

import (
	"strings"

	uuid "github.com/satori/go.uuid"
)

type UUIDUtil struct{}

func (ul UUIDUtil) Get() string {
	result := strings.Replace(uuid.NewV4().String(), "-", "", -1)
	return strings.ToUpper(result)
}

func (ul UUIDUtil) GetLowerCase() string {
	result := strings.Replace(uuid.NewV4().String(), "-", "", -1)
	return result
}
