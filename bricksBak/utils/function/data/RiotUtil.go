package data

import (
	"strings"

	. "gitee.com/tomatomeatman/golang-repository/bricks/model"
	//Log "github.com/cihub/seelog"
	"github.com/go-ego/riot"
	"github.com/go-ego/riot/types"
)

type RiotUtil struct{}

//加载动作
func init() {
	SetupLogger()

	//err := os.MkdirAll("./temp/riot/", 0766)
	//if err != nil {
	//	Log.Error("创建临时文件异常:", err)
	//}

	////以读写方式打开文件，如果不存在，则创建
	//file2, err := os.OpenFile("./temp/riot/testdict.txt", os.O_RDWR|os.O_CREATE, 0766)
	//if err != nil {
	//	Log.Error("创建临时文件'./temp/riot/testdict.txt'异常:", err)
	//}
	//file2.Close()

	////以读写方式打开文件，如果不存在，则创建
	//file3, err := os.OpenFile("./temp/riot/stop_tokens.txt", os.O_RDWR|os.O_CREATE, 0766)
	//if err != nil {
	//	Log.Error("创建临时文件'./temp/riot/stop_tokens.txt'异常:", err)
	//}
	//file3.Close()
}

////初始化
//func (this RiotUtil) Init(dbName string) {
//	if RiotInit {
//		return
//	}

//	searcher.Init(types.EngineOpts{
//		Using:       3,
//		StoreFolder: "./temp/riot/" + dbName, //存储库
//		UseStore:    true,
//		//GseDict: "zh",
//		//GseDict:       "./temp/riot/test_dict.txt",
//		//StopTokenFile: "./temp/riot/stop_tokens.txt",
//	})

//	RiotInit = true
//}

//添加
func (this RiotUtil) Add(dbName, key, data string) *MsgEmity {
	if "" == strings.TrimSpace(dbName) {
		return MsgEmity{}.Err(1001, "未指定搜索库")
	}

	if "" == strings.TrimSpace(key) {
		return MsgEmity{}.Err(1002, "关键字为空")
	}

	dbName = strings.TrimSpace(dbName)
	key = strings.TrimSpace(key)

	searcher := riot.Engine{}
	searcher.Init(types.EngineOpts{
		Using:    3,
		GseDict:  "zh",
		UseStore: true,
		//GseDict: "./dictionary.txt",
		//GseDict:       "./temp/test_dict.txt",
		//StopTokenFile: "./temp/stop_tokens.txt",
		StoreFolder: "./temp/riot/" + dbName, //存储库
	})

	defer searcher.Close()

	searcher.Index(key, types.DocData{Content: data}, true)

	searcher.Flush() //等待索引刷新完毕

	return MsgEmity{}.Success(key, "添加内容成功")
}

//添加
func (this RiotUtil) Adds(dbName string, data map[string]string) *MsgEmity {
	if "" == strings.TrimSpace(dbName) {
		return MsgEmity{}.Err(1001, "未指定搜索库")
	}

	if len(data) < 1 {
		return MsgEmity{}.Err(1002, "内容为空")
	}

	dbName = strings.TrimSpace(dbName)

	searcher := riot.Engine{}
	searcher.Init(types.EngineOpts{
		Using:    3,
		GseDict:  "zh",
		UseStore: true,
		//GseDict: "./dictionary.txt",
		//GseDict:       "./temp/test_dict.txt",
		//StopTokenFile: "./temp/stop_tokens.txt",
		StoreFolder: "./temp/riot/" + dbName, //存储库
	})

	defer searcher.Close()

	for key, val := range data {
		key = strings.TrimSpace(key)
		if key == "" {
			continue
		}

		searcher.Index(key, types.DocData{Content: val}, true)
	}

	searcher.Flush() //等待索引刷新完毕

	return MsgEmity{}.Success(data, "添加内容成功")
}

//删除
func (this RiotUtil) Del(dbName, key string) *MsgEmity {
	if "" == strings.TrimSpace(dbName) {
		return MsgEmity{}.Err(1001, "未指定搜索库")
	}

	if "" == strings.TrimSpace(key) {
		return MsgEmity{}.Err(1002, "关键字为空")
	}

	dbName = strings.TrimSpace(dbName)
	key = strings.TrimSpace(key)

	searcher := riot.Engine{}
	searcher.Init(types.EngineOpts{
		Using:    3,
		GseDict:  "zh",
		UseStore: true,
		//GseDict: "./dictionary.txt",
		//GseDict:       "./temp/test_dict.txt",
		//StopTokenFile: "./temp/stop_tokens.txt",
		StoreFolder: "./temp/riot/" + dbName, //存储库
	})

	defer searcher.Close()

	searcher.RemoveDoc(key, true)

	searcher.Flush() //等待索引刷新完毕

	return MsgEmity{}.Success(key, "删除内容成功")
}

//搜索
func (this RiotUtil) Find(dbName, key string, entity interface{}) *MsgEmity {
	if "" == strings.TrimSpace(dbName) {
		return MsgEmity{}.Err(1001, "未指定搜索库")
	}

	if "" == strings.TrimSpace(key) {
		return MsgEmity{}.Err(1002, "关键字为空")
	}

	dbName = strings.TrimSpace(dbName)
	key = strings.TrimSpace(key)

	searcher := riot.Engine{}
	searcher.Init(types.EngineOpts{
		Using:    3,
		GseDict:  "zh",
		UseStore: true,
		//GseDict: "./dictionary.txt",
		//GseDict:       "./temp/test_dict.txt",
		//StopTokenFile: "./temp/stop_tokens.txt",
		StoreFolder: "./temp/riot/" + dbName, //存储库
	})

	defer searcher.Close()
	searcher.Flush() //等待索引刷新完毕

	res := searcher.Search(types.SearchReq{Text: key})

	if res.NumDocs < 1 {
		return MsgEmity{}.Err(1003, "搜索结束,没有发现数据")
	}

	if nil == entity {
		result := []string{}
		for _, val := range res.Docs.(types.ScoredDocs) {
			result = append(result, val.Content)
		}

		return MsgEmity{}.Success(result, "搜索结束")
	}

	result := []interface{}{}
	for _, val := range res.Docs.(types.ScoredDocs) {
		result = append(result, JsonUtil{}.ToObj(val.Content, entity))
	}

	return MsgEmity{}.Success(result, "搜索结束")
}
