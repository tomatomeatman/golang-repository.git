package data

import (
	"fmt"
	"strings"
	"sync"
	"time"

	. "gitee.com/tomatomeatman/golang-repository/bricks/model"
)

var (
	cachMap    = make(map[string]CacheInfo) //存储缓存的集合
	checkWrite sync.Mutex                   //保存锁
)

//自定义缓存池
type CachePool struct{}

/**
 * 清理缓存
 * @return
 */
func (this CachePool) Clear() *MsgEmity {
	if len(cachMap) < 1 {
		return MsgEmity{}.Success("原本就是空的", "清理完毕")
	}

	cachMap = make(map[string]CacheInfo)
	return MsgEmity{}.Success(6999, "清理完毕")
}

/**
* 释放缓存
* @param sId
* @return
 */
func (this CachePool) Free(sId string) *MsgEmity {
	sId = strings.TrimSpace(sId)

	if (len(cachMap) < 1) || ("" == sId) {
		return MsgEmity{}.Success("本身没有这个缓存", "释放成功")
	}

	delete(cachMap, sId)

	return MsgEmity{}.Success(6999, "释放成功")
}

/**
* 释放缓存
* @param sId
* @return
 */
func (this CachePool) FreeById(sId string) *MsgEmity {
	return this.Free(sId)
}

/**
* 释放缓存
* @param sId
* @return
 */
func (this CachePool) FreeByData(data interface{}) *MsgEmity {
	iCount := 0
	for key, val := range cachMap {
		if val != data {
			continue
		}

		delete(cachMap, key)
		iCount++
	}

	if iCount < 1 {
		return MsgEmity{}.Success("本身没有这个缓存", "释放成功")
	}

	return MsgEmity{}.Success(iCount, "释放成功")
}

/**
* 释放指定分钟之前的缓存
* @param iMinute
* @return
 */
func (this CachePool) FreeByTime(iMinute int) *MsgEmity {
	if len(cachMap) < 1 {
		return MsgEmity{}.Success("本身没有这个缓存", "释放成功")
	}

	//--取过期时间点--//
	now := time.Now()
	iCount := 0
	for key, val := range cachMap {
		if int(now.Sub(val.GdLast).Minutes()) < iMinute { //两个时间相减
			continue
		}

		delete(cachMap, key)
		iCount++
	}

	return MsgEmity{}.Success("释放数:"+fmt.Sprintf("%v", iCount), "释放成功")
}

/**
* 添加缓存
* @param cacheInfo
* @return
 */
func (this CachePool) Add(cacheInfo CacheInfo) *MsgEmity {
	if nil == cacheInfo.Gdata {
		return MsgEmity{}.Err(6001, "添加失败,缓存信息为null")
	}

	sId := strings.TrimSpace(cacheInfo.GsId)
	if "" == sId {
		return MsgEmity{}.Err(6002, "添加失败,缓存信息关键字为空")
	}

	cachMap[sId] = cacheInfo

	return MsgEmity{}.Success(cacheInfo, "添加成功")
}

/**
* 添加缓存
* @param sId
* @param oData
* @return
 */
func (this CachePool) AddData(sId string, data interface{}) *MsgEmity {
	sId = strings.TrimSpace(sId)
	if "" == sId {
		return MsgEmity{}.Err(6001, "添加失败,缓存信息关键字为空")
	}

	if nil == data {
		return MsgEmity{}.Err(6002, "添加失败,缓存信息为null")
	}

	cacheInfo := CacheInfo{}.New(sId, data)

	cachMap[sId] = cacheInfo

	return MsgEmity{}.Success(cacheInfo, "添加成功")
}

/**
* 判断是否包含缓存,包含返回true
* @param sId
* @return
 */
func (this CachePool) Contains(sId string) bool {
	sId = strings.TrimSpace(sId)
	if "" == sId {
		return false
	}

	if len(cachMap) < 1 {
		return false
	}

	_, ok := cachMap[sId]

	return ok
}

/**
* 查找缓存信息
* @param sId
* @return
 */
func (this CachePool) Find(sId string) *CacheInfo {
	if "" == sId {
		return nil
	}

	if len(cachMap) < 1 {
		return nil
	}

	cacheInfo, ok := cachMap[sId]
	if !ok {
		return nil //缓存中还没有存在
	}

	cacheInfo.GdLast = time.Now() //修改访问时间

	return &cacheInfo
}

/**
* 查找缓存对象
* @param sId
* @return
 */
func (this CachePool) FindData(sId string) interface{} {
	sId = strings.TrimSpace(sId)
	cacheInfo := this.Find(sId)
	if nil == cacheInfo {
		return nil
	}

	return cacheInfo.Gdata
}
