package data

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"fmt"
	"strings"

	Log "github.com/cihub/seelog"
)

type AesUtil struct{}

//加密过程：
//  1、处理数据，对数据进行填充，采用PKCS7（当密钥长度不够时，缺几位补几个几）的方式。
//  2、对数据进行加密，采用AES加密方法中CBC加密模式
//  3、对得到的加密数据，进行base64加密，得到字符串
// 解密过程相反

//16,24,32位字符串的话，分别对应AES-128，AES-192，AES-256 加密方法

//pkcs7Padding 填充
func (this AesUtil) pkcs7Padding(data []byte, blockSize int) []byte {
	//判断缺少几位长度。最少1，最多 blockSize
	padding := blockSize - len(data)%blockSize
	//补足位数。把切片[]byte{byte(padding)}复制padding个
	padText := bytes.Repeat([]byte{byte(padding)}, padding)

	return append(data, padText...)
}

//pkcs7UnPadding 填充的反向操作
func (this AesUtil) pkcs7UnPadding(data []byte) ([]byte, error) {
	length := len(data)
	if length == 0 {
		return nil, errors.New("加密字符串错误！")
	}

	//获取填充的个数
	unPadding := int(data[length-1])

	return data[:(length - unPadding)], nil
}

//AesEncrypt 加密
func (this AesUtil) AesEncrypt(data []byte, key string) ([]byte, error) {
	bkey := this.getKey(key)

	//创建加密实例
	block, err := aes.NewCipher(bkey)
	if err != nil {
		Log.Error("AES加密错误:", err)
		return nil, err
	}

	//判断加密快的大小
	blockSize := block.BlockSize()
	//填充
	//encryptBytes := this.pkcs7Padding(data, blockSize)
	encryptBytes := this.pkcs7Padding(data, blockSize)
	//初始化加密数据接收切片
	crypted := make([]byte, len(encryptBytes))
	//使用cbc加密模式
	blockMode := cipher.NewCBCEncrypter(block, bkey[:blockSize])
	//执行加密
	blockMode.CryptBlocks(crypted, encryptBytes)

	return crypted, nil
}

//AesDecrypt 解密
func (this AesUtil) AesDecrypt(data []byte, key string) ([]byte, error) {
	bkey := this.getKey(key)

	//创建实例
	block, err := aes.NewCipher(bkey)
	if err != nil {
		Log.Error("AES解密错误:", err)
		return nil, err
	}

	//获取块的大小
	blockSize := block.BlockSize()
	//使用cbc
	blockMode := cipher.NewCBCDecrypter(block, bkey[:blockSize])
	//初始化解密数据接收切片
	crypted := make([]byte, len(data))
	//执行解密
	blockMode.CryptBlocks(crypted, data)
	//去除填充
	crypted, err = this.pkcs7UnPadding(crypted)
	if err != nil {
		return nil, err
	}

	return crypted, nil
}

//EncryptByAes Aes加密 后 base64 再加转字符串
func (this AesUtil) EncryptBase64(data string, key string) string {
	res, err := this.AesEncrypt([]byte(data), key)
	if err != nil {
		Log.Error("AES加密错误:", err)
		return ""
	}

	return base64.StdEncoding.EncodeToString(res)
}

//DecryptByAes Aes 解密
func (this AesUtil) DecryptBase64(data string, key string) string {
	dataByte, err := base64.StdEncoding.DecodeString(data)
	if err != nil {
		Log.Error("base64解密错误:", err)
		return ""
	}

	result, err := this.AesDecrypt(dataByte, key)
	if err != nil {
		Log.Error("AES解密错误:", err)
		return ""
	}

	return string(result)
}

//EncryptByAes Aes加密 后 返回16进制
func (this AesUtil) Encrypt(data string, key string) string {
	res, err := this.AesEncrypt([]byte(data), key)
	if err != nil {
		Log.Error("AES加密错误:", err)
		return ""
	}

	return strings.ToUpper(hex.EncodeToString(res))
}

//DecryptByAes Aes 解密16进制
func (this AesUtil) Decrypt(data string, key string) string {
	dataByte, err := hex.DecodeString(data)
	if err != nil {
		Log.Error("16进制转byte[]错误:", err)
		return ""
	}

	result, err := this.AesDecrypt(dataByte, key)
	if err != nil {
		Log.Error("AES解密错误:", err)
		return ""
	}

	return string(result)
}

//取正确的加密密钥
func (this AesUtil) getKey(key string) []byte {
	iLength := len(key)

	if iLength < 16 {
		key = fmt.Sprint(key, "0000000000000000")
		//return []byte(fmt.Sprintf("%016s", key))//前面补0
	}

	return []byte(key[0:16])
}
