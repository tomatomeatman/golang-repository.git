package utils

import (
	"os"
	"path/filepath"
)

type SystemUtil struct{}

// 获取当前执行程序所在的绝对路径
func (this SystemUtil) AppPath() string {
	exePath, err := os.Executable()
	if err != nil {
		return "./"
	}

	res, _ := filepath.EvalSymlinks(filepath.Dir(exePath))

	return res
}
