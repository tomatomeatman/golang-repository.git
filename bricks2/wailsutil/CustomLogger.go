package wailsutil

import (
	"sync"

	"github.com/cihub/seelog"
)

// CustomLogger 实现github.com/wailsapp/wails/v2/pkg/logger里logger.Logger接口的自定义日志器的结构体
type CustomLogger struct{}

var (
	seelogInit sync.Once
	clogger    = GetLog()
)

// Init 初始化日志文件
func InitLog() {
	txt := `<seelog levels="trace,debug,info,warn,error,critical"><outputs formatid="main"><filter levels="trace,debug"><console formatid="colored-default"/><rollingfile formatid="main" type="size" filename="./logs/debug.log" maxsize="10485760" maxrolls="99" /></filter><filter levels="trace,info"><console formatid="colored-default"/><rollingfile formatid="main" type="size" filename="./logs/info.log" maxsize="10485760" maxrolls="99" /></filter><filter levels="warn"><console formatid="colored-warn"/><rollingfile formatid="main" type="size" filename="./logs/warn.log" maxsize="10485760" maxrolls="99" /></filter><filter levels="error,critical"><console formatid="colored-error"/><rollingfile formatid="main" type="size" filename="./logs/error.log" maxsize="10485760" maxrolls="99" /></filter></outputs><formats><format id="colored-default" format="%EscM(38)%Date %Time [%LEV] %File:%Line | %Msg%n%EscM(0)"/><format id="colored-warn" format="%EscM(33)%Date %Time [%LEV] %File:%Line | %Msg%n%EscM(0)"/><format id="colored-error" format="%EscM(31)%Date %Time [%LEV] %File:%Line | %Msg%n%EscM(0)"/><format id="main" format="%Date %Time [%LEV] %File:%Line | %Msg%n"/></formats></seelog>`
	logger, err := seelog.LoggerFromConfigAsBytes([]byte(txt))
	if err != nil {
		return
	}

	seelog.ReplaceLogger(logger)
}

func GetLog() *CustomLogger {
	seelogInit.Do(InitLog)
	return &CustomLogger{}
}

// Debug 实现了 logger.Logger 接口的 Debug 方法
func (*CustomLogger) Debug(message string) {
	seelogInit.Do(InitLog)
	seelog.Debug(message)
}

// Info 实现了 logger.Logger 接口的 Info 方法
func (*CustomLogger) Info(message string) {
	seelogInit.Do(InitLog)
	seelog.Info(message)
}

// Warning 实现了 logger.Logger 接口的 Warning 方法
func (*CustomLogger) Warning(message string) {
	seelogInit.Do(InitLog)
	seelog.Warn(message)
}

// Error 实现了 logger.Logger 接口的 Error 方法
func (*CustomLogger) Error(message string) {
	seelogInit.Do(InitLog)
	seelog.Error(message)
}

// Critical 实现了 logger.Logger 接口的 Critical 方法
func (*CustomLogger) Critical(message string) {
	seelogInit.Do(InitLog)
	seelog.Critical(message)
}

// Fatal 实现了 logger.Logger 接口的 Fatal 方法
func (*CustomLogger) Fatal(message string) {
	seelogInit.Do(InitLog)
	seelog.Error(message)
}

// Panic 实现了 logger.Logger 接口的 Panic 方法
func (*CustomLogger) Print(message string) {
	seelogInit.Do(InitLog)
	seelog.Debug(message)
}

// Panic 实现了 logger.Logger 接口的 Panic 方法
func (*CustomLogger) Trace(message string) {
	seelogInit.Do(InitLog)
	seelog.Trace(message)
}
