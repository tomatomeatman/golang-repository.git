module gitee.com/tomatomeatman/golang-repository/bricks2/wailsutil

go 1.23.4

require (
	github.com/MakeNowJust/hotkey v0.0.0-20231028172355-3f1d3592581e
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	github.com/energye/systray v1.0.2
	github.com/juju/fslock v0.0.0-20160525022230-4d5c94c67b4b
	github.com/wailsapp/wails/v2 v2.10.1
	gopkg.in/ini.v1 v1.67.0
)

require (
	github.com/godbus/dbus/v5 v5.1.0 // indirect
	github.com/leaanthony/slicer v1.6.0 // indirect
	github.com/leaanthony/u v1.1.1 // indirect
	github.com/lxn/win v0.0.0-20210218163916-a377121e959e // indirect
	github.com/tevino/abool v0.0.0-20220530134649-2bfc934cb23c // indirect
	golang.org/x/sys v0.31.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
