package interceptor

/**
 * 登录缓存对象
 * 非数据库实体
 */
type LoginUser struct {
	Id       string            `json:"id" gorm:"column:Id; type:varchar"`       //用户编号
	Name     string            `json:"name" gorm:"column:Name; type:varchar"`   //姓名
	No       string            `json:"no" gorm:"column:No; type:varchar"`       //工号
	Pass     string            `json:"-" gorm:"column:Pass; type:varchar"`      //密码
	SignPass string            `json:"-" gorm:"column:SignPass; type:varchar"`  //手势密码
	Type     string            `json:"type" gorm:"column:Type; type:varchar"`   //用户类型编号
	Owner    string            `json:"owner" gorm:"column:Owner; type:varchar"` //用户来源
	State    int               `json:"state" gorm:"column:State; type:int"`     //状态(-1,人员表人员被删除,1表示正常状态,2人员停用,)
	LastDate int64             `json:"lastDate"`                                //最后访问时间
	Token    string            `json:"token"`                                   //分配的Token
	Device   int               `json:"device"`                                  //设备类型,1:PC,2:手机,3:平板,4.....
	Attached map[string]string `json:"attached"`                                //与登录相关的附属信息
}

/**
 * 复制
 * @return
 */
func (o *LoginUser) Clone() LoginUser {
	result := LoginUser{}
	result.Id = o.Id
	result.Name = o.Name
	result.No = o.No
	result.Pass = o.Pass
	result.SignPass = o.SignPass
	result.Type = o.Type
	result.Owner = o.Owner
	result.State = o.State
	result.LastDate = o.LastDate
	result.Token = o.Token

	if len(o.Attached) > 0 {
		result.Attached = map[string]string{}
		for key, val := range o.Attached {
			result.Attached[key] = val
		}
	}

	return result
}
