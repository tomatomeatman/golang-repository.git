package bridge

import (
	"gitee.com/tomatomeatman/golang-repository/bricks2/function/urlutil"
	"gitee.com/tomatomeatman/golang-repository/bricks2/model/msgentity"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/ginutil"
)

/**
 * 模块管理ModuleManage表基本业务操作结构体
 */
type ModuleManageService struct {
}

var (
	moduleManageServiceName = "" //桥接到登录服务的服务名,默认GBaseSystemServer
	//moduleManageServiceName  = "" //桥接到登录服务的服务密钥
)

// 初始化
func init() {
	if app.IsNotCloudSystem() { //禁用桥接
		return
	}

	moduleManageServiceName = app.ReadConfigKey("CloudServer", "BaseServerName", "GBaseSystemServer").(string)
	//moduleManageServiceName = app.ReadConfigKey("CloudServer", "BaseServerKey", "").(string)
}

/**
 * 读取树形结构数据
 * ctx Http请求对象
 */
func (service ModuleManageService) FindByTree(ctx ginutil.Context) *msgentity.MsgEntity {
	groupColumn := urlutil.GetParam(ctx.Request, "groupColumn", "").(string)
	groupName := urlutil.GetParam(ctx.Request, "groupName", "").(string)
	m := map[string]interface{}{
		"groupColumn": groupColumn,
		"groupName":   groupName,
	}

	me := BridgeDao{}.Post(ctx, moduleManageServiceName, "/module/manage/find/tree", m, &msgentity.MsgEntity{})
	if !me.Success {
		return me
	}

	return me.Data.(*msgentity.MsgEntity)
}
