package bridge

import (
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/ginutil"
)

// @Controller 模块管理控制器
type ModuleManageController struct {
	app.ControllerBaseFunc                     //通用控制层接口方法
	ModuleService          ModuleManageService //对应模块业务实体
}

/**
 * 初始化
 */
func init() {
	if app.IsNotCloudSystem() { //禁用桥接
		return
	}

	app.RegisterController(&ModuleManageController{})
}

// 接口注册
func (control ModuleManageController) RegisterUrl() {
	go ginutil.ControllerRegister("/module/manage/find/tree", control.FindByTree, ginutil.POST)
}

// #region @Api {title=查询树形结构数据}
// @param {name=groupName dataType=string paramType=query explain=指定节点名 required=false}
// @return {type=json explain=返回树型数据}
// @RequestMapping {name=FindByTree type=POST value=/module/manage/find/tree}
// #endregion
func (control ModuleManageController) FindByTree(ctx ginutil.Context) interface{} {
	return ModuleManageService{}.FindByTree(ctx)
}
