package bridge

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strings"

	"gitee.com/tomatomeatman/golang-repository/bricks2/function/httputil"
	"gitee.com/tomatomeatman/golang-repository/bricks2/function/urlutil"
	"gitee.com/tomatomeatman/golang-repository/bricks2/model/msgentity"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/ginutil"
	Log "github.com/cihub/seelog"
)

/**
 * 桥接数据操作结构体
 */
type BridgeDao struct {
	app.DaoBaseFunc
}

var (
	cloudCenterUrl = "" //Cloud注册中心地址(分布式时使用)
	// cloudCenterUser = "" //Cloud注册中心用户名称(分布式时使用)
	cloudCenterPass = "" //Cloud注册中心用户密码(分布式时使用)
	domainName      = "" //项目所在域名
)

// 初始化
func init() {
	cloudCenterUrl = app.ReadConfigKey("CloudCenter", "Site", "").(string)
	// cloudCenterUser = app.ReadConfigKey("CloudCenter", "User", "").(string)
	cloudCenterPass = app.ReadConfigKey("CloudCenter", "Password", "").(string)
	domainName = app.ReadConfigKey("CloudCenter", "DomainName", "").(string)
}

/**
 * post请求
 * @param serverName 服务名
 * @param url 请求的相对路径
 * @param parameters 请求参数
 * @param resultType 返回类型
 * @param isResultList 返沪类型是否list
 * @return
 */
func (dao BridgeDao) Post(ctx ginutil.Context, serverName, url string, parameters map[string]interface{}, resultType interface{}) *msgentity.MsgEntity {
	hearMap := urlutil.GetHeader(ctx.Request) //取请求头参数
	return dao.post(serverName, url, parameters, resultType, hearMap)
}

/**
 * 代理请求post
 * 注意:要求请求方返回的必须是*msgentity.MsgEntity类型
 * @return
 */
func (dao BridgeDao) ProxyPost(ctx ginutil.Context) interface{} {
	domainName := urlutil.GetParam(ctx.Request, "domainName", "GBaseSystemServer").(string)
	url := string([]rune(ctx.Request.URL.String())[6:])

	params := urlutil.GetParamsAll(ctx.Request, false)
	hearMap := urlutil.GetHeader(ctx.Request) //取请求头参数
	m := dao.post(domainName, url, params, &msgentity.MsgEntity{}, hearMap)
	if !m.Success {
		return m
	}

	return m.Data
}

/**
 * 代理请求get
 * 注意:要求请求方返回的必须是*msgentity.MsgEntity类型
 * @return
 */
func (dao BridgeDao) ProxyGet(ctx ginutil.Context) interface{} {
	domainName := urlutil.GetParam(ctx.Request, "domainName", "GBaseSystemServer").(string)
	url := string([]rune(ctx.Request.URL.String())[6:])

	params := urlutil.GetParamsAll(ctx.Request, false)
	hearMap := urlutil.GetHeader(ctx.Request) //取请求头参数
	m := dao.get(domainName, url, params, &msgentity.MsgEntity{}, hearMap)
	if !m.Success {
		return m
	}

	return m.Data
}

/**
 * GET请求
 * @param serverName 服务名
 * @param url 请求的相对路径
 * @param resultType 返回类型
 * @param isResultList 返沪类型是否list
 * @return
 */
func (dao BridgeDao) Get(ctx ginutil.Context, serverName, url string, parameters map[string]interface{}, resultType interface{}) *msgentity.MsgEntity {
	hearMap := urlutil.GetHeader(ctx.Request) //取请求头参数
	return dao.get(serverName, url, parameters, resultType, hearMap)
}

/**
 * post请求
 * @param serverName 服务名
 * @param url 请求的相对路径
 * @param parameters 请求参数
 * @param resultType 返回类型
 * @param hearMap 自定义请求头
 * @return
 */
func (dao BridgeDao) post(serverName, url string, parameters map[string]interface{},
	resultType interface{}, hearMap map[string]string) *msgentity.MsgEntity {
	me := dao.findServerSite(serverName)
	if !me.Success {
		return me
	}

	params := map[string]interface{}{}
	if (parameters != nil) && (len(parameters) > 0) {
		for key, val := range parameters {
			params[key] = val
		}
	}

	url = me.Data.(string) + url
	bl, msg, data := httputil.DoPost(url, params, hearMap)
	if !bl {
		return msgentity.Create(bl, msg, data)
	}

	if me.Data == nil {
		return msgentity.Err(8001, "调用POST未能获取指定结果")
	}

	if resultType == nil {
		return me
	}

	if reflect.TypeOf(resultType).String() == "string" {
		return me
	}

	str := string(me.Data.([]uint8))
	err := json.Unmarshal([]byte(str), resultType)
	if err != nil {
		Log.Error("Json字符串转换异常: ", err)
		return msgentity.Err(8003, "内容转换发生异常")
	}

	return me.SetData(resultType)
}

/**
 * GET请求
 * @param serverName 服务名
 * @param url 请求的相对路径
 * @param resultType 返回类型
 * @param hearMap 自定义请求头
 * @return
 */
func (dao BridgeDao) get(serverName, url string, parameters map[string]interface{},
	resultType interface{}, hearMap map[string]string) *msgentity.MsgEntity {
	me := dao.findServerSite(serverName)
	if !me.Success {
		return me
	}

	//url = strings.ReplaceAll(me.Data.(string)+url, "//", "/")
	url = me.Data.(string) + url
	bl, msg, data := httputil.DoGet(url, parameters, hearMap)
	if !bl {
		return msgentity.Create(bl, msg, data)
	}

	if me.Data == nil {
		return msgentity.Err(8001, "调用POST未能获取指定结果")
	}

	if resultType == nil {
		return me
	}

	if reflect.TypeOf(resultType).String() == "string" {
		return me
	}

	err := json.Unmarshal([]byte(me.Data.(string)), &resultType)
	if err != nil {
		Log.Error("Json字符串转换异常: %+v\n", err)
		return msgentity.Err(8002, "内容转换发生异常")
	}

	return me.SetData(resultType)
}

/**
 * 访问注册中心,获取服务地址
 * @param serverName
 * @param cloudCenterUrl
 * @return
 */
func (dao BridgeDao) findServerSite(serverName string) *msgentity.MsgEntity {
	if cloudCenterUrl == "" {
		return msgentity.Err(7101, "项目未配置注册中心地址,配置项'cloud.center.site'")
	}

	url := strings.ReplaceAll(fmt.Sprintf("http://%s/find/name?Name=%s&key=%s", cloudCenterUrl, serverName, cloudCenterPass), "http://http://", "http://")
	bl, msg, data := httputil.DoGet(url, nil, nil)
	if !bl {
		return msgentity.Create(bl, msg, data)
	}

	result := msgentity.MsgEntity{}
	err := json.Unmarshal([]byte(data.(string)), &result)
	if err != nil {
		return msgentity.Err(7102, "访问注册中心失败,未能获取预期信息")
	}

	if result.Data == nil {
		return msgentity.Err(7102, "访问注册中心失败,未能获取预期信息")
	}

	if !result.Success {
		if reflect.TypeOf(result.Data).String() == "int" {
			result.Data = result.Data.(int) + 200
			return &result
		}

		return &result
	}

	m := result.Data.(map[string]interface{})

	if (m["domainName"] == nil) || (m["domainName"] == "") { //没有启用域名
		result.Data = fmt.Sprintf("http://%s:%s", m["serverIp"], m["serverPort"])
		return &result
	}

	if (m["domainName"] != "") && (domainName == m["domainName"]) { //启用域名,但域名与本程序一致,则属于同一个服务器或同一个局域网内
		result.Data = fmt.Sprintf("http://%s:%s", m["serverIp"], m["serverPort"]) //通过ip和端口访问
		return &result
	}

	if (m["domainPort"] == nil) || (m["domainPort"] == "") { //启用域名但没有使用端口,则端口为80
		result.Data = fmt.Sprintf("http://%s", m["domainName"])
		return &result
	}

	//启用域名且使用指定端口

	return result.SetData(fmt.Sprintf("http://%s:%s", m["domainName"], m["domainPort"]))
}
