package bridge

import (
	"gitee.com/tomatomeatman/golang-repository/bricks2/model/msgentity"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/ginutil"
)

type UserAndRightService struct {
	app.ServiceBaseFunc
}

var (
	userAndRightServiceName = "" //桥接到登录服务的服务名,默认GBaseSystemServer
	//userAndRightServiceKey  = "" //桥接到登录服务的服务密钥
)

// 初始化
func init() {
	if app.IsNotCloudSystem() { //禁用桥接
		return
	}

	userAndRightServiceName = app.ReadConfigKey("CloudServer", "BaseServerName", "GBaseSystemServer").(string)
	//userAndRightServiceKey = app.ReadConfigKey("CloudServer", "BaseServerKey", "").(string)
}

/**
 * 清理指定用户的缓存
 * @param user 用户编号
 * @return
 */
func (service UserAndRightService) ClearCache(ctx ginutil.Context, cacheName, user string) *msgentity.MsgEntity {
	m := map[string]interface{}{"sLoginId": user}

	me := BridgeDao{}.Post(ctx, userAndRightServiceName, "/user/and/right/clear/cache", m, &msgentity.MsgEntity{})
	if !me.Success {
		return me
	}

	return me.Data.(*msgentity.MsgEntity)
}

/**
 * 根据用户取权限标识字符串(一个权限标识代表了多个可访问的url路径)
 * 不用判断请求用户是谁,因为其它人获取信息后没用,权限校验会在每次进行具体操作时进行再次判断
 * @param userId
 * @return
 */
func (service UserAndRightService) FindEnglishByUserId(ctx ginutil.Context, userId string) *msgentity.MsgEntity {
	m := map[string]interface{}{"userId": userId}

	me := BridgeDao{}.Post(ctx, userAndRightServiceName, "/user/and/right/find/english", m, &msgentity.MsgEntity{})
	if !me.Success {
		return me
	}

	return me.Data.(*msgentity.MsgEntity)
}

/**
 * 验证指定用户是否有访问指定url的权限(给内部拦截器用,直接返回Boolean)
 * @param userId
 * @param url
 * @return
 */
func (service UserAndRightService) CheckUrlRight(ctx ginutil.Context, userId, url string) bool {
	m := map[string]interface{}{"userId": userId, "url": url}

	me := BridgeDao{}.Post(ctx, userAndRightServiceName, "/user/and/right/check", m, &msgentity.MsgEntity{})
	if !me.Success {
		return false
	}

	return me.Data.(*msgentity.MsgEntity).Success
}

/**
 * 验证指定用户是否有访问指定url的权限
 * @param userId 验证的用户
 * @param url 请求验证的权限(URL地址)
 * @return
 */
func (service UserAndRightService) CheckRight(ctx ginutil.Context, userId, url string) *msgentity.MsgEntity {
	m := map[string]interface{}{"userId": userId, "url": url}

	me := BridgeDao{}.Post(ctx, userAndRightServiceName, "/user/and/right/check/right", m, &msgentity.MsgEntity{})
	if !me.Success {
		return me
	}

	return me.Data.(*msgentity.MsgEntity)
}

/**
 * 根据用户查询用户所拥有的权限编号集合
 * @param userId
 * @return
 */
func (service UserAndRightService) FindRightId(ctx ginutil.Context, userId string) *msgentity.MsgEntity {
	m := map[string]interface{}{"userId": userId}

	me := BridgeDao{}.Post(ctx, userAndRightServiceName, "/user/and/right/find/rightid", m, &msgentity.MsgEntity{})
	if !me.Success {
		return me
	}

	return me.Data.(*msgentity.MsgEntity)
}

/**
 * 根据用户查询用户所拥有的权限的最后更新时间
 * @param userId
 * @return
 */
func (service UserAndRightService) LastTime(ctx ginutil.Context, userId string) *msgentity.MsgEntity {
	m := map[string]interface{}{"userId": userId}

	me := BridgeDao{}.Post(ctx, userAndRightServiceName, "/user/and/right/lasttime", m, &msgentity.MsgEntity{})
	if !me.Success {
		return me
	}

	return me.Data.(*msgentity.MsgEntity)
}
