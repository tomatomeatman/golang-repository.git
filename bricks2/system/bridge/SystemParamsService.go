package bridge

import (
	"gitee.com/tomatomeatman/golang-repository/bricks2/model/msgentity"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/ginutil"
)

type SystemParamsService struct {
	app.ServiceBaseFunc
}

var (
	systemParamsServiceName = "" //桥接到登录服务的服务名,默认GBaseSystemServer
	//systemParamsServiceName  = "" //桥接到登录服务的服务密钥
)

// 初始化
func init() {
	if app.IsNotCloudSystem() { //禁用桥接
		return
	}

	systemParamsServiceName = app.ReadConfigKey("CloudServer", "BaseServerName", "GBaseSystemServer").(string)
	//systemParamsServiceName = app.ReadConfigKey("CloudServer", "BaseServerKey", "").(string)
}

/**
 * 取所有免拦截系统参数对象集合
 * @return
 */
func (service SystemParamsService) FindByNoIntercept(ctx ginutil.Context) *msgentity.MsgEntity {
	me := BridgeDao{}.Post(ctx, systemParamsServiceName, "/system/params/insidevisit/find/not/intercept", nil, &msgentity.MsgEntity{})
	if !me.Success {
		return me
	}

	return me.Data.(*msgentity.MsgEntity)
}
