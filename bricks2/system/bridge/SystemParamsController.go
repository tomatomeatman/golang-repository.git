package bridge

import (
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/ginutil"
)

// @Controller 桥接服务-系统参数桥接操作接口
type SystemParamsController struct {
	app.ControllerBaseFunc                     //通用控制层接口方法
	ModuleService          SystemParamsService //对应模块业务实体
}

/**
 * 初始化
 */
func init() {
	if app.IsNotCloudSystem() { //禁用桥接
		return
	}

	app.RegisterController(&SystemParamsController{})
}

// 接口注册
func (control SystemParamsController) RegisterUrl() {
	go ginutil.ControllerRegister("/system/params/insidevisit/find/not/intercept", control.FindByNoIntercept, ginutil.POST)
}

// #region @Api {title=取所有免拦截系统参数对象集合}
// @return {type=json explain=返回分页数据}
// @RequestMapping {name=FindByPage type=POST value=/system/params/insidevisit/find/not/intercept}
// #endregion
func (control SystemParamsController) FindByNoIntercept(ctx ginutil.Context) interface{} {
	return SystemParamsService{}.FindByNoIntercept(ctx)
}
