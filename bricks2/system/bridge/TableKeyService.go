package bridge

import (
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/app"
)

/**
 * 记录编号序列管理表TableKey表基本业务操作结构体
 */
type TableKeyService struct {
	app.ServiceBaseFunc
}

/**
 * 初始化
 */
func init() {
	if app.IsNotCloudSystem() { //禁用桥接
		return
	}

	app.RegisterService("TableKey", &TableKeyService{})
}

/**
 * 取各表(或序列)的新Id
 * @param formatLong 格式化长度(不足长度+0)
 * @param serieName 表名或序列名
 * @return
 */
func (ts TableKeyService) GetNewId(formatLong int, serieName string) string {
	return TableKeyDao{}.GetNewId(formatLong, serieName)
}

/**
 * 取各表的一批新Id
 * @param formatLong 格式化长度(不足长度+0)
 * @param serieName 表名或序列名
 * @param size 数量
 * @return
 */
func (ts TableKeyService) GetNewIds(formatLong int, serieName string, size int) []string {
	return TableKeyDao{}.GetNewIds(formatLong, serieName, size)
}

/**
 * 重置
 * @param serieName 表名或序列名
 * @return
 */
func (ts TableKeyService) Reset(serieName string) string {
	return TableKeyDao{}.Reset(serieName)
}
