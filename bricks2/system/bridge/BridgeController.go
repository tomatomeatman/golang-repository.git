package bridge

import (
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/ginutil"
)

/**
 * 桥接代理请求控制器
 */
type BridgeController struct {
	app.ControllerBaseFunc //通用控制层接口方法
}

/**
 * 初始化
 */
func init() {
	if app.IsNotCloudSystem() { //禁用桥接
		return
	}

	//-- 接口注册 --//
	go ginutil.ControllerRegister("/proxy/*proxy", BridgeController{}.ProxyGet, ginutil.GET)
	go ginutil.ControllerRegister("/proxy/*proxy", BridgeController{}.ProxyPost, ginutil.POST)
}

func (control BridgeController) ProxyGet(ctx ginutil.Context) interface{} {
	return BridgeDao{}.ProxyGet(ctx)
}

func (control BridgeController) ProxyPost(ctx ginutil.Context) interface{} {
	return BridgeDao{}.ProxyPost(ctx)
}
