package bridge

import (
	"gitee.com/tomatomeatman/golang-repository/bricks2/model/msgentity"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/ginutil"
)

type LoginService struct {
	app.ServiceBaseFunc
}

var (
	loginServiceName = "" //桥接到登录服务的服务名,默认GBaseSystemServer
	// loginServiceKey  = "" //桥接到登录服务的服务密钥
)

// 初始化
func init() {
	if app.IsNotCloudSystem() { //禁用桥接
		return
	}

	loginServiceName = app.ReadConfigKey("CloudServer", "BaseServerName", "GBaseSystemServer").(string)
	// loginServiceKey = app.ReadConfigKey("CloudServer", "BaseServerKey", "").(string)
}

/**
 * 登录,如果用户和密码正确则返回同行令牌
 * @param nameOrNo 名称或工号
 * @param Pass 密码
 * @param owner 用户来源表
 * @param device 设备类型,1:PC,2:手机,3:平板,4.....
 * @return
 */
func (service LoginService) In(ctx ginutil.Context, nameOrNo, Pass, owner string, device int) *msgentity.MsgEntity {
	m := map[string]interface{}{
		"nameOrNo": nameOrNo,
		"pass":     Pass,
		"owner":    owner,
		"device":   device,
	}

	me := BridgeDao{}.Post(ctx, loginServiceName, "/login/in", m, &msgentity.MsgEntity{})
	if !me.Success {
		return me
	}

	return me.Data.(*msgentity.MsgEntity)
}

/**
 * 用户登出
 * @param token
 * @return
 */
func (service LoginService) Out(ctx ginutil.Context, token string) *msgentity.MsgEntity {
	m := map[string]interface{}{"token": token}

	me := BridgeDao{}.Post(ctx, loginServiceName, "/login/out", m, &msgentity.MsgEntity{})
	if !me.Success {
		return me
	}

	return me.Data.(*msgentity.MsgEntity)
}

/**
 * 判断Token是否已经登录
 * @param token
 * @return
 */
func (service LoginService) Check(ctx ginutil.Context, token string) *msgentity.MsgEntity {
	m := map[string]interface{}{"token": token}

	me := BridgeDao{}.Post(ctx, loginServiceName, "/login/check", m, &msgentity.MsgEntity{})
	if !me.Success {
		return me
	}

	return me.Data.(*msgentity.MsgEntity)
}

/**
 * 登录心跳操作,Token存在则更新并返回true,没有则返回false
 * @param token
 * @return
 */
func (service LoginService) Heartbeat(ctx ginutil.Context, token string) *msgentity.MsgEntity {
	m := map[string]interface{}{"token": token}

	me := BridgeDao{}.Post(ctx, loginServiceName, "/login/heartbeat", m, &msgentity.MsgEntity{})
	if !me.Success {
		return me
	}

	return me.Data.(*msgentity.MsgEntity)
}

/**
 * 取登录用户信息,注意:限制为内部系统访问
 * @param key
 * @param token
 * @return
 */
func (service LoginService) GetLogin(ctx ginutil.Context, key, token string) *msgentity.MsgEntity {
	m := map[string]interface{}{"key": key, "token": token}

	me := BridgeDao{}.Post(ctx, loginServiceName, "/login/info", m, &msgentity.MsgEntity{})
	if !me.Success {
		return me
	}

	return me.Data.(*msgentity.MsgEntity)
}

/**
 * 取当前登录用户简洁信息
 * @param request
 * @return
 */
func (service LoginService) GetCurrentLogin(ctx ginutil.Context) *msgentity.MsgEntity {
	me := BridgeDao{}.Post(ctx, loginServiceName, "/login/info/current", nil, &msgentity.MsgEntity{})
	if !me.Success {
		return me
	}

	return me.Data.(*msgentity.MsgEntity)
}

/**
 * 根据用户和密码取对应的用户编号
 * @param nameOrNo
 * @param Pass
 * @param owner 用户来源表
 * @return
 */
func (service LoginService) GetUserId(ctx ginutil.Context, nameOrNo, Pass, owner string) *msgentity.MsgEntity {
	m := map[string]interface{}{"nameOrNo": nameOrNo, "pass": Pass, "owner": owner}

	me := BridgeDao{}.Post(ctx, loginServiceName, "/login/getid", m, &msgentity.MsgEntity{})
	if !me.Success {
		return me
	}

	return me.Data.(*msgentity.MsgEntity)
}
