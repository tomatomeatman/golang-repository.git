package tablekeylocal

import (
	"gitee.com/tomatomeatman/golang-repository/bricks2/function/urlutil"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/ginutil"
)

// @Controller 记录编号序列管理表控制器
type TableKeyLocalController struct {
	app.ControllerBaseFunc                      //通用控制层接口方法
	ModuleEntity           TableKeyLocal        //对应模块数据实体
	ModuleService          TableKeyLocalService //对应模块业务实体
}

/**
 * 初始化
 */
func init() {
	app.SetTableKeyServiceName("TableKeyLocal")
	app.RegisterController(&TableKeyLocalController{})
}

// -- 检查待新增内容是否存在重复数据(单独字段重复即重复)集合,注意:int必须是1、10、100、1000 --//
func (control TableKeyLocalController) CheckRepeatAlone() map[string]int {
	return map[string]int{"Type": 1}
}

// 接口注册
func (control TableKeyLocalController) RegisterUrl() {
	go ginutil.ControllerRegister("/table/key/add", control.Add, ginutil.POST)
	go ginutil.ControllerRegister("/table/key/del", control.Del, ginutil.POST)
	go ginutil.ControllerRegister("/table/key/edit", control.Edit, ginutil.POST)
	go ginutil.ControllerRegister("/table/key/find/id", control.FindById, ginutil.POST)
	go ginutil.ControllerRegister("/table/key/find/key", control.FindByKey, ginutil.POST)
	go ginutil.ControllerRegister("/table/key/find/all", control.FindByKey, ginutil.POST)
	go ginutil.ControllerRegister("/table/key/find/page/row", control.FindByRow, ginutil.POST)
	go ginutil.ControllerRegister("/table/key/find/page", control.FindByPage, ginutil.POST)
	go ginutil.ControllerRegister("/table/key/new", control.GetNewId, ginutil.POST)
	go ginutil.ControllerRegister("/table/key/news", control.GetNewIds, ginutil.POST)
	go ginutil.ControllerRegister("/table/key/reset", control.Reset, ginutil.POST)
}

// #region @Api {title=新增}
// @param {name=data dataType=json paramType=body explain=TableKeyLocal结构数据 explainType=TableKeyLocal required=true}
// @return {type=json explainType=MsgEntity<TableKeyLocal> explain=返回对象}
// @RequestMapping {name=Add type=POST value=/table/key/add}
// #endregion
func (control TableKeyLocalController) Add(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Add(ctx, &control)
}

// #region @Api {title=删除数据}
// @param {name=id dataType=int64 paramType=query explain=记录编号 required=true}
// @param {name=edition dataType=int paramType=query explain=版本号 required=true}
// @return {type=json explainType=MsgEntity<int> explain=返回影响数}
// @RequestMapping {name=Del type=POST value=/table/key/del}
// #endregion
func (control TableKeyLocalController) Del(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Del(ctx, &control)
}

// #region @Api {title=修改数据}
// @param {name=data dataType=json paramType=body explain=TableKeyLocal结构数据 explainType=TableKeyLocal required=true}
// @return {type=json explainType=MsgEntity<int> explain=返回码值}
// @RequestMapping {name=Edit type=POST value=/table/key/edit}
// #endregion
func (control TableKeyLocalController) Edit(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Edit(ctx, &control)
}

// #region @Api {title=查询全部}
// @param {name=data dataType=json paramType=body explain=map[string]interface结构数据 explainType=TableKeyLocal required=true}
// @return {type=json explainType=MsgEntity<TableKeyLocal> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindAll type=POST value=/table/key/find/all}
// #endregion
func (control TableKeyLocalController) FindAll(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindAll(ctx, &control)
}

// #region @Api {title=查询指定时间内数据}
// @param {name=dateSt dataType=string paramType=query explain=格式:YYYY-MM-DD HH:mm:ss required=true}
// @param {name=dateEd dataType=string paramType=query explain=格式:YYYY-MM-DD HH:mm:ss required=true}
// @return {type=json explainType=MsgEntity<TableKeyLocal> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByDate type=POST value=/table/key/find/date}
// #endregion
func (control TableKeyLocalController) FindByDate(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByDate(ctx, &control)
}

// #region @Api {title=根据记录编号取对象}
// @param {name=id dataType=int64 paramType=query explain=记录编号 required=true}
// @return {type=json explainType=MsgEntity<TableKeyLocal> explain=返回对象}
// @RequestMapping {name=FindById type=POST value=/table/key/find/id}
// #endregion
func (control TableKeyLocalController) FindById(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindById(ctx, &control)
}

// #region @Api {title=根据关键值取对象集合}
// @param {name=data dataType=json paramType=body explain=TableKeyLocal结构数据 explainType=TableKeyLocal required=true}
// @return {type=json explainType=MsgEntity<TableKeyLocal> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByKey type=POST value=/table/key/find/key}
// #endregion
func (control TableKeyLocalController) FindByKey(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByKey(ctx, &control)
}

// #region @Api {title=根据记录编号查询符合分页数据的某条记录}
// @param {name=id dataType=int64 paramType=query explain=记录编号 required=true}
// @return {type=json explainType=MsgEntity<TableKeyLocal> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByRow type=POST value=/table/key/find/page/row}
// #endregion
func (control TableKeyLocalController) FindByRow(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByRow(ctx, &control)
}

// #region @Api {title=查询分页数据}
// @param {name=data dataType=json paramType=body explain=FindByPageParam结构数据 explainType=FindByPageParam<TableKeyLocal> required=true}
// @return {type=json explainType=MsgEntity<Page<TableKeyLocal>> explain=返回分页数据}
// @RequestMapping {name=FindByPage type=POST value=/table/key/find/page}
// #endregion
func (control TableKeyLocalController) FindByPage(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByPage(ctx, &control)
}

// #region @Api {title=取指定表(或序列)的新Id}
// @param {name=formatLong dataType=int paramType=query explain=格式化长度(不足长度+0) required=true}
// @param {name=serieName dataType=string paramType=query explain=表名或序列名 required=true}
// @return {type=json explainType=string explain=返回新序列值}
// @RequestMapping {name=GetNewId type=POST value=/table/key/new}
// #endregion
func (control TableKeyLocalController) GetNewId(ctx ginutil.Context) interface{} {
	formatLong := urlutil.GetParam(ctx.Request, "formatLong", 0).(int)
	serieName := urlutil.GetParam(ctx.Request, "serieName", "").(string)

	return control.ModuleService.GetNewId(formatLong, serieName)
}

// #region @Api {title=取指定表的一批新Id}
// @param {name=formatLong dataType=int paramType=query explain=格式化长度(不足长度+0) required=true}
// @param {name=serieName dataType=string paramType=query explain=表名或序列名 required=true}
// @param {name=size dataType=int paramType=query explain=数量 required=true}
// @return {type=json explainType=string explain=返回新序列值}
// @RequestMapping {name=GetNewIds type=POST value=/table/key/news}
// #endregion
func (control TableKeyLocalController) GetNewIds(ctx ginutil.Context) interface{} {
	formatLong := urlutil.GetParam(ctx.Request, "formatLong", 0).(int)
	serieName := urlutil.GetParam(ctx.Request, "serieName", "").(string)
	size := urlutil.GetParam(ctx.Request, "size", 0).(int)
	return control.ModuleService.GetNewIds(formatLong, serieName, size)
}

// #region @Api {title=重置指定表序号}
// @param {name=serieName dataType=string paramType=query explain=表名或序列名 required=true}
// @return {type=json explainType=MsgEntity<int> explain=返回码值}
// @RequestMapping {name=Reset type=POST value=/table/key/reset}
// #endregion
func (control TableKeyLocalController) Reset(ctx ginutil.Context) interface{} {
	serieName := urlutil.GetParam(ctx.Request, "serieName", "").(string)
	return control.ModuleService.Reset(serieName)
}
