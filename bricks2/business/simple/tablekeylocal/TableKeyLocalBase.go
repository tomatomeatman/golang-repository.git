package tablekeylocal

import (
	"strings"

	"gitee.com/tomatomeatman/golang-repository/bricks2/model/dbinfo"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/gorm"
)

var (
	tableName = ""
	dbName    = ""
)

/**
 * 记录编号序列管理表'TableKeyLocal'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type TableKeyLocalBase struct {
	Id      int64  `json:"id" gorm:"column:id; type:bigint; NOT NULL; DEFAULT '0'; primary_key" defaultData:"0" comment:"记录编号(非自增长)"`
	Type    string `json:"type" gorm:"column:type; type:varchar; NOT NULL" defaultData:"''" comment:"类型"`
	Value   string `json:"value" gorm:"column:value; type:varchar; NOT NULL" defaultData:"''" comment:"值"`
	Rank    int    `json:"rank" gorm:"column:index; type:int; DEFAULT '0'" defaultData:"0" comment:"序号"`
	Edition int    `json:"edition" gorm:"column:edition; type:int; NOT NULL; DEFAULT '1'" defaultData:"1" comment:"版本号"`
}

/**
 * 初始化
 */
func init() {
	// tableName = app.ReadConfigKey("DbVariables", "TableKeyName", "TableKeyLocal").(string)
	// dbName = app.ReadConfigKey("DbVariables", "MainDb", "BaseSystem").(string)
	// dbinfo.TableInfo{}.RegisterEntity(tableName+"Base", &TableKeyLocalBase{}) //注册注册数据库实体类结构体
	// dbinfo.TableInfo{}.RegisterTableInfo(tableName+"Base", TableKeyLocalBase{}.Info()) //注册数据库表信息
}

/**
 * 创建结构实体,并赋予默认值
 */
func (TableKeyLocalBase) New() dbinfo.Entity {
	return &TableKeyLocalBase{}
}

/**
 * 结构体映射表名,处理结构体名称与表名不一致的情况
 */
func (TableKeyLocalBase) TableName() string {
	if tableName != "" {
		return tableName
	}

	tableName = gorm.GetDbName(dbName) + "." + tableName
	tableName = strings.Replace(tableName, "..", ".", -1)

	return tableName
}

/**
 * 结构体映射表的字段名串
 */
func (TableKeyLocalBase) BaseColumnNames() string {
	return "Id,Type,Value,Rank,Edition"
}

/**
 * 取基础实体,用于在子类(嵌套结构体)时同样获得基类
 */
func (TableKeyLocalBase) BaseEntity() dbinfo.Entity {
	return &TableKeyLocalBase{}
}

/**
 * 结构体映射库名,去除'Dev_'等前缀
 * @return
 */
func (TableKeyLocalBase) OwnerName() string {
	return dbName
}

/**
 * 结构体映射表名,无库名
 * @return
 */
func (TableKeyLocalBase) OwnerTable() string {
	return tableName
}

/**
 * 取数据结构信息
 * name 字段名
 */
func (TableKeyLocalBase) GetDataInfo(name string) *dbinfo.DataInfo {
	switch name {
	case "Id":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "table_key", Name: "id", Comment: "记录编号(非自增长)", IsDbField: true, DbFileType: "bigint", DefaultData: "0", GoDefaultData: 0, IsExtra: false, IsKey: true, IsNull: false, MaxLength: 20, IsDecimal: false, IntLength: 20, DecimalLength: 0, Index: 0, IsBigTxt: false}
	case "Type":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "table_key", Name: "type", Comment: "类型", IsDbField: true, DbFileType: "varchar", DefaultData: "", GoDefaultData: "", IsExtra: false, IsKey: false, IsNull: false, MaxLength: 30, IsDecimal: false, IntLength: 30, DecimalLength: 0, Index: 1, IsBigTxt: false}
	case "Value":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "table_key", Name: "value", Comment: "值", IsDbField: true, DbFileType: "varchar", DefaultData: "", GoDefaultData: "", IsExtra: false, IsKey: false, IsNull: false, MaxLength: 30, IsDecimal: false, IntLength: 30, DecimalLength: 0, Index: 2, IsBigTxt: false}
	case "Rank":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "table_key", Name: "rank", Comment: "序号", IsDbField: true, DbFileType: "int", DefaultData: "0", GoDefaultData: 0, IsExtra: false, IsKey: false, IsNull: true, MaxLength: 11, IsDecimal: false, IntLength: 11, DecimalLength: 0, Index: 3, IsBigTxt: false}
	case "Edition":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "table_key", Name: "edition", Comment: "版本号", IsDbField: true, DbFileType: "int", DefaultData: "1", GoDefaultData: 1, IsExtra: false, IsKey: false, IsNull: false, MaxLength: 11, IsDecimal: false, IntLength: 11, DecimalLength: 0, Index: 4, IsBigTxt: false}
	default:
		return nil
	}
}
