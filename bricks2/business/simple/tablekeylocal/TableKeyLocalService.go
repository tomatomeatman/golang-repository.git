package tablekeylocal

import (
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/app"
)

/**
 * 记录编号序列管理表TableKeyLocal表基本业务操作结构体
 */
type TableKeyLocalService struct {
	app.ServiceBaseFunc
}

/**
 * 初始化
 */
func init() {
}

/**
 * 取各表(或序列)的新Id
 * @param formatLong 格式化长度(不足长度+0)
 * @param serieName 表名或序列名
 * @return
 */
func (TableKeyLocalService) GetNewId(formatLong int, serieName string) string {
	return TableKeyLocalDao{}.GetNewId(formatLong, serieName)
}

/**
 * 取各表的一批新Id
 * @param formatLong 格式化长度(不足长度+0)
 * @param serieName 表名或序列名
 * @param size 数量
 * @return
 */
func (TableKeyLocalService) GetNewIds(formatLong int, serieName string, size int) []string {
	return TableKeyLocalDao{}.GetNewIds(formatLong, serieName, size)
}

/**
 * 重置
 * @param serieName 表名或序列名
 * @return
 */
func (TableKeyLocalService) Reset(serieName string) string {
	return TableKeyLocalDao{}.Reset(serieName)
}
