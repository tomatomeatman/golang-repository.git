module gitee.com/tomatomeatman/golang-repository/bricks2/business/simple/tablekeylocal

go 1.23.4

require (
	gitee.com/tomatomeatman/golang-repository/bricks2/function/data/integerutil v0.0.0-20250317072830-519b6760ee3b
	gitee.com/tomatomeatman/golang-repository/bricks2/function/data/stringutil v0.0.0-20250317072830-519b6760ee3b
	gitee.com/tomatomeatman/golang-repository/bricks2/function/urlutil v0.0.0-20250317072830-519b6760ee3b
	gitee.com/tomatomeatman/golang-repository/bricks2/model/dbinfo v0.0.0-20250317072830-519b6760ee3b
	gitee.com/tomatomeatman/golang-repository/bricks2/utils/app v0.0.0-20250317072830-519b6760ee3b
	gitee.com/tomatomeatman/golang-repository/bricks2/utils/ginutil v0.0.0-20250317072830-519b6760ee3b
	gitee.com/tomatomeatman/golang-repository/bricks2/utils/gorm v0.0.0-20250317072830-519b6760ee3b
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
)

require (
	filippo.io/edwards25519 v1.1.0 // indirect
	gitee.com/tomatomeatman/golang-repository/bricks2/function/data/aesutil v0.0.0-20250317072830-519b6760ee3b // indirect
	gitee.com/tomatomeatman/golang-repository/bricks2/function/data/jsonutil v0.0.0-20250317072830-519b6760ee3b // indirect
	gitee.com/tomatomeatman/golang-repository/bricks2/function/data/maputil v0.0.0-20250317072830-519b6760ee3b // indirect
	gitee.com/tomatomeatman/golang-repository/bricks2/function/data/timeutil v0.0.0-20250317072830-519b6760ee3b // indirect
	gitee.com/tomatomeatman/golang-repository/bricks2/function/data/u64util v0.0.0-20250317072830-519b6760ee3b // indirect
	gitee.com/tomatomeatman/golang-repository/bricks2/function/data/uuidutil v0.0.0-20250317072830-519b6760ee3b // indirect
	gitee.com/tomatomeatman/golang-repository/bricks2/function/httputil v0.0.0-20250317072830-519b6760ee3b // indirect
	gitee.com/tomatomeatman/golang-repository/bricks2/function/reflectutil v0.0.0-20250317072830-519b6760ee3b // indirect
	gitee.com/tomatomeatman/golang-repository/bricks2/function/system v0.0.0-20250317072830-519b6760ee3b // indirect
	gitee.com/tomatomeatman/golang-repository/bricks2/model/fileback v0.0.0-20250317072830-519b6760ee3b // indirect
	gitee.com/tomatomeatman/golang-repository/bricks2/model/msgentity v0.0.0-20250317072830-519b6760ee3b // indirect
	gitee.com/tomatomeatman/golang-repository/bricks2/model/set v0.0.0-20250317072830-519b6760ee3b // indirect
	github.com/bytedance/sonic v1.13.1 // indirect
	github.com/bytedance/sonic/loader v0.2.4 // indirect
	github.com/cloudwego/base64x v0.1.5 // indirect
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/fsnotify/fsnotify v1.8.0 // indirect
	github.com/gabriel-vasile/mimetype v1.4.8 // indirect
	github.com/gin-contrib/sse v1.0.0 // indirect
	github.com/gin-contrib/static v1.1.3 // indirect
	github.com/gin-gonic/gin v1.10.0 // indirect
	github.com/glebarez/go-sqlite v1.22.0 // indirect
	github.com/glebarez/sqlite v1.11.0 // indirect
	github.com/go-ole/go-ole v1.3.0 // indirect
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/go-playground/universal-translator v0.18.1 // indirect
	github.com/go-playground/validator/v10 v10.25.0 // indirect
	github.com/go-sql-driver/mysql v1.9.0 // indirect
	github.com/goccy/go-json v0.10.5 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/gorilla/websocket v1.5.3 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/klauspost/cpuid/v2 v2.2.10 // indirect
	github.com/leodido/go-urn v1.4.0 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/ncruces/go-strftime v0.1.9 // indirect
	github.com/pelletier/go-toml/v2 v2.2.3 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/shirou/gopsutil v3.21.11+incompatible // indirect
	github.com/shopspring/decimal v1.4.0 // indirect
	github.com/tklauser/go-sysconf v0.3.15 // indirect
	github.com/tklauser/numcpus v0.10.0 // indirect
	github.com/twitchyliquid64/golang-asm v0.15.1 // indirect
	github.com/ugorji/go/codec v1.2.12 // indirect
	github.com/yusufpapurcu/wmi v1.2.4 // indirect
	golang.org/x/arch v0.15.0 // indirect
	golang.org/x/crypto v0.36.0 // indirect
	golang.org/x/exp v0.0.0-20250305212735-054e65f0b394 // indirect
	golang.org/x/net v0.37.0 // indirect
	golang.org/x/sys v0.31.0 // indirect
	golang.org/x/text v0.23.0 // indirect
	google.golang.org/protobuf v1.36.5 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	gorm.io/driver/mysql v1.5.7 // indirect
	gorm.io/gorm v1.25.12 // indirect
	modernc.org/libc v1.61.13 // indirect
	modernc.org/mathutil v1.7.1 // indirect
	modernc.org/memory v1.8.2 // indirect
	modernc.org/sqlite v1.36.1 // indirect
)
