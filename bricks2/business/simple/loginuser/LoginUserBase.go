package loginuser

import (
	"time"

	"gitee.com/tomatomeatman/golang-repository/bricks2/model/dbinfo"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/gorm"
)

var tableInfo = []string{"", "base_system", "login_user"}

/**
 * 初始化
 */
func init() {
	dbinfo.RegisterEntity("LoginUserBase", &LoginUserBase{}) //注册注册数据库实体类结构体
}

/**
 * 用户表'login_user'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type LoginUserBase struct {
	Id           string    `json:"id" gorm:"column:id; type:varchar; NOT NULL; primary_key" defaultData:"''" comment:"表编号"`
	Name         string    `json:"name" gorm:"column:name; type:varchar; NOT NULL" defaultData:"''" comment:"名称"`
	No           string    `json:"no" gorm:"column:no; type:varchar; NOT NULL" defaultData:"''" comment:"标识(组合唯一)"`
	Type         string    `json:"type" gorm:"column:type; type:varchar; NOT NULL; DEFAULT '管理员'" defaultData:"'管理员'" comment:"类型"`
	Pass         string    `json:"pass" gorm:"column:pass; type:varchar; NOT NULL" defaultData:"''" comment:"密码"`
	PassWork     string    `json:"passWork" gorm:"column:pass_work; type:varchar" defaultData:"''" comment:"业务密码"`
	PassDate     time.Time `json:"passDate" gorm:"column:pass_date; type:datetime" defaultData:"'2025-02-06 12:05:49'" comment:"密码修改时间"`
	PassWorkDate time.Time `json:"passWorkDate" gorm:"column:pass_work_date; type:datetime" defaultData:"'2025-02-06 12:05:49'" comment:"业务密码修改时间"`
	Owner        string    `json:"owner" gorm:"column:owner; type:varchar; DEFAULT 'BaseSystem.LoginUser'" defaultData:"'BaseSystem.LoginUser'" comment:"来源"`
	Attrib       string    `json:"attrib" gorm:"column:attrib; type:longtext" defaultData:"''" comment:"附属信息"`
	Memo         string    `json:"memo" gorm:"column:memo; type:varchar" defaultData:"''" comment:"备注"`
	Creator      string    `json:"creator" gorm:"column:creator; type:varchar; NOT NULL; DEFAULT '00000000'" defaultData:"'00000000'" comment:"创建人员"`
	CreatorDate  time.Time `json:"creatorDate" gorm:"column:creator_date; type:datetime; NOT NULL" defaultData:"'2025-02-06 12:05:49'" comment:"创建时间"`
	Modifieder   string    `json:"modifieder" gorm:"column:modifieder; type:varchar; NOT NULL; DEFAULT '00000000'" defaultData:"'00000000'" comment:"修改人员"`
	ModifiedDate time.Time `json:"modifiedDate" gorm:"column:modified_date; type:datetime; NOT NULL" defaultData:"'2025-02-06 12:05:49'" comment:"修改时间"`
	State        int       `json:"state" gorm:"column:state; type:int; NOT NULL; DEFAULT '1'" defaultData:"1" comment:"状态(枚举,1:启用;2:禁用)"`
	Rank         int       `json:"rank" gorm:"column:index; type:int; NOT NULL; DEFAULT '1'" defaultData:"1" comment:"序号"`
	Edition      int       `json:"edition" gorm:"column:edition; type:int; NOT NULL; DEFAULT '1'" defaultData:"1" comment:"版本号"`
}

/**
 * 创建结构实体
 * @return
 */
func (LoginUserBase) New() dbinfo.Entity {
	return &LoginUserBase{}
}

/**
 * 取基础实体,用于在子类(嵌套结构体)时同样获得基类
 */
func (LoginUserBase) BaseEntity() dbinfo.Entity {
	return &LoginUserBase{}
}

/**
 * 结构体映射表名,处理结构体名称与表名不一致的情况
 * @return
 */
func (LoginUserBase) TableName() string {
	if tableInfo[0] != "" {
		return tableInfo[0]
	}

	tableInfo[0] = gorm.GetDbName(tableInfo[1]) + tableInfo[2]

	return tableInfo[0]
}

/**
 * 结构体映射库名,去除'Dev_'等前缀
 * @return
 */
func (LoginUserBase) OwnerName() string {
	return tableInfo[1]
}

/**
 * 结构体映射表名,无库名
 * @return
 */
func (LoginUserBase) OwnerTable() string {
	return tableInfo[2]
}

/**
 * 结构体映射表的字段名串
 * @return
 */
func (LoginUserBase) BaseColumnNames() string {
	return "Id,Name,No,Type,Pass,PassWork,PassDate,PassWorkDate,Owner,Attrib,Memo,Creator,CreatorDate,Modifieder,ModifiedDate,State,Rank,Edition"
}

/**
 * 取数据结构信息
 * @param name 字段名
 * @return
 */
func (LoginUserBase) GetDataInfo(name string) *dbinfo.DataInfo {
	switch name {
	case "Id":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_user", Name: "id", Comment: "表编号", IsDbField: true, DbFileType: "varchar", DefaultData: "", GoDefaultData: "", IsExtra: false, IsKey: true, IsNull: false, MaxLength: 8, IsDecimal: false, IntLength: 8, DecimalLength: 0, Index: 0, IsBigTxt: false}
	case "Name":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_user", Name: "name", Comment: "名称", IsDbField: true, DbFileType: "varchar", DefaultData: "", GoDefaultData: "", IsExtra: false, IsKey: false, IsNull: false, MaxLength: 32, IsDecimal: false, IntLength: 32, DecimalLength: 0, Index: 1, IsBigTxt: false}
	case "No":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_user", Name: "no", Comment: "标识(组合唯一)", IsDbField: true, DbFileType: "varchar", DefaultData: "", GoDefaultData: "", IsExtra: false, IsKey: false, IsNull: false, MaxLength: 64, IsDecimal: false, IntLength: 64, DecimalLength: 0, Index: 2, IsBigTxt: false}
	case "Type":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_user", Name: "type", Comment: "类型", IsDbField: true, DbFileType: "varchar", DefaultData: "管理员", GoDefaultData: "管理员", IsExtra: false, IsKey: false, IsNull: false, MaxLength: 50, IsDecimal: false, IntLength: 50, DecimalLength: 0, Index: 3, IsBigTxt: false}
	case "Pass":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_user", Name: "pass", Comment: "密码", IsDbField: true, DbFileType: "varchar", DefaultData: "", GoDefaultData: "", IsExtra: false, IsKey: false, IsNull: false, MaxLength: 64, IsDecimal: false, IntLength: 64, DecimalLength: 0, Index: 4, IsBigTxt: false}
	case "PassWork":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_user", Name: "pass_work", Comment: "业务密码", IsDbField: true, DbFileType: "varchar", DefaultData: "", GoDefaultData: "", IsExtra: false, IsKey: false, IsNull: true, MaxLength: 64, IsDecimal: false, IntLength: 64, DecimalLength: 0, Index: 5, IsBigTxt: false}
	case "PassDate":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_user", Name: "pass_date", Comment: "密码修改时间", IsDbField: true, DbFileType: "datetime", DefaultData: "", GoDefaultData: time.Now().Format("2006-01-02 15:04:05"), IsExtra: false, IsKey: false, IsNull: true, MaxLength: 23, IsDecimal: false, IntLength: 23, DecimalLength: 0, Index: 6, IsBigTxt: false}
	case "PassWorkDate":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_user", Name: "pass_work_date", Comment: "业务密码修改时间", IsDbField: true, DbFileType: "datetime", DefaultData: "", GoDefaultData: time.Now().Format("2006-01-02 15:04:05"), IsExtra: false, IsKey: false, IsNull: true, MaxLength: 23, IsDecimal: false, IntLength: 23, DecimalLength: 0, Index: 7, IsBigTxt: false}
	case "Owner":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_user", Name: "owner", Comment: "来源", IsDbField: true, DbFileType: "varchar", DefaultData: "BaseSystem.LoginUser", GoDefaultData: "BaseSystem.LoginUser", IsExtra: false, IsKey: false, IsNull: true, MaxLength: 100, IsDecimal: false, IntLength: 100, DecimalLength: 0, Index: 8, IsBigTxt: false}
	case "Attrib":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_user", Name: "attrib", Comment: "附属信息", IsDbField: true, DbFileType: "longtext", DefaultData: "", GoDefaultData: "", IsExtra: false, IsKey: false, IsNull: true, MaxLength: 100, IsDecimal: false, IntLength: 100, DecimalLength: 0, Index: 9, IsBigTxt: true}
	case "Memo":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_user", Name: "memo", Comment: "备注", IsDbField: true, DbFileType: "varchar", DefaultData: "", GoDefaultData: "", IsExtra: false, IsKey: false, IsNull: true, MaxLength: 200, IsDecimal: false, IntLength: 200, DecimalLength: 0, Index: 10, IsBigTxt: false}
	case "Creator":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_user", Name: "creator", Comment: "创建人员", IsDbField: true, DbFileType: "varchar", DefaultData: "00000000", GoDefaultData: "00000000", IsExtra: false, IsKey: false, IsNull: false, MaxLength: 8, IsDecimal: false, IntLength: 8, DecimalLength: 0, Index: 11, IsBigTxt: false}
	case "CreatorDate":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_user", Name: "creator_date", Comment: "创建时间", IsDbField: true, DbFileType: "datetime", DefaultData: "", GoDefaultData: time.Now().Format("2006-01-02 15:04:05"), IsExtra: false, IsKey: false, IsNull: false, MaxLength: 23, IsDecimal: false, IntLength: 23, DecimalLength: 0, Index: 12, IsBigTxt: false}
	case "Modifieder":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_user", Name: "modifieder", Comment: "修改人员", IsDbField: true, DbFileType: "varchar", DefaultData: "00000000", GoDefaultData: "00000000", IsExtra: false, IsKey: false, IsNull: false, MaxLength: 8, IsDecimal: false, IntLength: 8, DecimalLength: 0, Index: 13, IsBigTxt: false}
	case "ModifiedDate":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_user", Name: "modified_date", Comment: "修改时间", IsDbField: true, DbFileType: "datetime", DefaultData: "", GoDefaultData: time.Now().Format("2006-01-02 15:04:05"), IsExtra: false, IsKey: false, IsNull: false, MaxLength: 23, IsDecimal: false, IntLength: 23, DecimalLength: 0, Index: 14, IsBigTxt: false}
	case "State":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_user", Name: "state", Comment: "状态(枚举,1:启用;2:禁用)", IsDbField: true, DbFileType: "int", DefaultData: "1", GoDefaultData: 1, IsExtra: false, IsKey: false, IsNull: false, MaxLength: 8, IsDecimal: false, IntLength: 8, DecimalLength: 0, Index: 15, IsBigTxt: false}
	case "Rank":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_user", Name: "rank", Comment: "序号", IsDbField: true, DbFileType: "int", DefaultData: "1", GoDefaultData: 1, IsExtra: false, IsKey: false, IsNull: false, MaxLength: 8, IsDecimal: false, IntLength: 8, DecimalLength: 0, Index: 16, IsBigTxt: false}
	case "Edition":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_user", Name: "edition", Comment: "版本号", IsDbField: true, DbFileType: "int", DefaultData: "1", GoDefaultData: 1, IsExtra: false, IsKey: false, IsNull: false, MaxLength: 8, IsDecimal: false, IntLength: 8, DecimalLength: 0, Index: 17, IsBigTxt: false}
	default:
		return nil
	}
}
