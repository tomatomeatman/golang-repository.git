package loginuser

import (
	"gitee.com/tomatomeatman/golang-repository/bricks2/function/urlutil"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/ginutil"
)

// @Controller 用户表控制器
type LoginUserController struct {
	app.ControllerBaseFunc                  //通用控制层接口方法
	ModuleEntity           LoginUser        //对应模块数据实体
	ModuleService          LoginUserService //对应模块业务实体
	ModuleDao              LoginUserDao     //对应的数据处理实体
}

/**
 * 初始化
 */
func init() {
	app.RegisterController(&LoginUserController{})
}

// -- 检查待新增内容是否存在重复数据(单独字段重复即重复)集合,注意:int必须是1、10、100、1000 --//
func (control LoginUserController) CheckRepeatAlone() map[string]int {
	return map[string]int{"Name": 1, "No": 2}
}

// 接口注册
func (control LoginUserController) RegisterUrl() {
	go ginutil.ControllerRegister("/login/user/add", control.Add, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/del", control.Del, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/edit", control.Edit, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/find/id", control.FindById, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/find/key", control.FindByKey, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/find/all", control.FindAll, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/find/page/row", control.FindByRow, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/find/page", control.FindByPage, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/password/edit", control.EditPass, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/password/reset", control.ResetPass, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/edit/type", control.EditUserType, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/edit/head", control.EditUserHead, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/head", control.LookHead, ginutil.GET)
}

// #region @Api {title=新增}
// @param {name=data dataType=json paramType=body explain=LoginUser结构数据 required=true}
// @return {type=MsgEntity explain=返回对象}
// @RequestMapping {name=Add type=POST value=/login/user/add}
// #endregion
func (control LoginUserController) Add(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Add(ctx, &control)
}

// #region @Api {title=删除数据}
// @param {name=id dataType=string paramType=query explain=记录编号 required=true}
// @param {name=edition dataType=int paramType=query explain=版本号 required=true}
// @return {type=MsgEntity explain=返回影响数}
// @RequestMapping {name=Del type=POST value=/login/user/del}
// #endregion
func (control LoginUserController) Del(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Del(ctx, &control)
}

// #region @Api {title=修改数据}
// @param {name=data dataType=json paramType=body explain=LoginUser结构数据 required=true}
// @return {type=MsgEntity explain=返回码值}
// @RequestMapping {name=Edit type=POST value=/login/user/edit}
// #endregion
func (control LoginUserController) Edit(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Edit(ctx, &control)
}

// #region @Api {title=查询全部}
// @param {name=data dataType=json paramType=body explain=map[string]interface结构数据 required=true}
// @return {type=MsgEntity explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindAll type=POST value=/login/user/find/all}
// #endregion
func (control LoginUserController) FindAll(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindAll(ctx, &control)
}

// #region @Api {title=查询指定时间内数据}
// @param {name=DateSt dataType=string paramType=query explain=格式:YYYY-MM-DD HH:mm:ss required=true}
// @param {name=DateEd dataType=string paramType=query explain=格式:YYYY-MM-DD HH:mm:ss required=true}
// @return {type=MsgEntity explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByDate type=POST value=/login/user/find/date}
// #endregion
func (control LoginUserController) FindByDate(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByDate(ctx, &control)
}

// #region @Api {title=根据记录编号取对象}
// @param {name=id dataType=string paramType=query explain=记录编号 required=true}
// @return {type=MsgEntity explain=返回对象}
// @RequestMapping {name=FindById type=POST value=/login/user/find/id}
// #endregion
func (control LoginUserController) FindById(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindById(ctx, &control)
}

// #region @Api {title=根据关键值取对象集合}
// @param {name=data dataType=json paramType=body explain=LoginUser结构数据 required=true}
// @return {type=MsgEntity explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByKey type=POST value=/login/user/find/key}
// #endregion
func (control LoginUserController) FindByKey(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByKey(ctx, &control)
}

// #region @Api {title=根据记录编号查询符合分页数据的某条记录}
// @param {name=id dataType=string paramType=query explain=记录编号 required=true}
// @return {type=MsgEntity explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByRow type=POST value=/login/user/find/page/row}
// #endregion
func (control LoginUserController) FindByRow(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByRow(ctx, &control)
}

// #region @Api {title=查询分页数据}
// @param {name=data dataType=json paramType=body explain=findByPageParam结构数据 required=true}
// @return {type=MsgEntity explain=返回分页数据}
// @RequestMapping {name=FindByPage type=POST value=/login/user/find/page}
// #endregion
func (control LoginUserController) FindByPage(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByPage(ctx, &control)
}

// #region @Api {title=修改当前用户密码}
// @param {name=oldPass dataType=string paramType=query explain=旧密码 required=true}
// @param {name=newPass dataType=string paramType=query explain=新密码 required=true}
// @param {name=owner dataType=string paramType=query explain=归属 required=true}
// @return {type=json explainType=MsgEntity<int> explain=返回码值}
// @RequestMapping {name=EditPass type=POST value=/login/user/password/edit}
// #endregion
func (control LoginUserController) EditPass(ctx ginutil.Context) interface{} {
	loginUserId := app.CurrentLoginUserId(ctx.Request) //取当前登录用户编号
	sOldPass := urlutil.GetParam(ctx.Request, "oldPass", "").(string)
	sNewPass := urlutil.GetParam(ctx.Request, "newPass", "").(string)
	owner := urlutil.GetParam(ctx.Request, "owner", "").(string)

	return control.ModuleService.EditPass(ctx, &LoginUser{}, loginUserId, sOldPass, sNewPass, owner)
}

// 只有当前用户和管理员(admin)才能重置密码
// #region @Api {title=重置密码}
// @param {name=id dataType=string paramType=query explain=表编号 required=true}
// @param {name=pass dataType=int paramType=query explain=新密码 required=true}
// @return {type=json explainType=MsgEntity<int> explain=返回码值}
// @RequestMapping {name=ResetPass type=POST value=/login/user/password/reset}
// #endregion
func (control LoginUserController) ResetPass(ctx ginutil.Context) interface{} {
	loginUserId := app.CurrentLoginUserId(ctx.Request) //取当前登录用户编号
	id := urlutil.GetParam(ctx.Request, "id", "").(string)
	pass := urlutil.GetParam(ctx.Request, "pass", "").(string)
	owner := urlutil.GetParam(ctx.Request, "owner", "").(string)

	return control.ModuleService.ResetPass(loginUserId, id, pass, owner)
}

// #region @Api {title=根据用户编号设置用户类型}
// @param {name=id dataType=string paramType=query explain=用户编号 required=true}
// @param {name=type dataType=string paramType=query explain=用户类型 required=true}
// @return {type=json explainType=MsgEntity<int> explain=返回码值}
// @RequestMapping {name=EditUserType type=POST value=/login/user/edit/type}
// #endregion
func (control LoginUserController) EditUserType(ctx ginutil.Context) interface{} {
	loginUserId := app.CurrentLoginUserId(ctx.Request) //取当前登录用户编号
	id := urlutil.GetParam(ctx.Request, "id", "").(string)
	Type := urlutil.GetParam(ctx.Request, "type", "").(string)
	owner := urlutil.GetParam(ctx.Request, "owner", "").(string)

	return control.ModuleService.EditUserType(loginUserId, id, Type, owner)
}

// #region @Api {title=设置用户头像}
// @param {name=head dataType=string paramType=query explain=Base64图片 required=true}
// @return {type=json explainType=MsgEntity<int> explain=返回码值}
// @RequestMapping {name=EditUserHead type=POST value=/login/user/edit/head}
// #endregion
func (control LoginUserController) EditUserHead(ctx ginutil.Context) interface{} {
	loginUserId := app.CurrentLoginUserId(ctx.Request) //取当前登录用户编号
	head := urlutil.GetParam(ctx.Request, "head", "").(string)

	return control.ModuleService.EditUserHead(loginUserId, head)
}

// #region @Api {title=取用户头像}
// @return {type=bit explainType=image/png explain=返回图片流}
// @RequestMapping {name=LookHead type=GET value=/login/user/head}
// #endregion
func (control LoginUserController) LookHead(ctx ginutil.Context) interface{} {
	loginUserId := app.CurrentLoginUserId(ctx.Request) //取当前登录用户编号

	me := control.ModuleService.LookHead(loginUserId)
	if !me.Success {
		return me
	}

	// 设置HTTP响应头Content-Type为图片类型（这里是png）
	ctx.Header("Content-Type", "image/png")

	// 返回图片数据
	ctx.Data(200, "image/png", me.Data.([]byte))

	return nil
}
