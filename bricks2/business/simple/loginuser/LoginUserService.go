package loginuser

import (
	"encoding/json"
	"os"
	"strings"

	"gitee.com/tomatomeatman/golang-repository/bricks2/function/data/base64util"
	"gitee.com/tomatomeatman/golang-repository/bricks2/function/data/md5util"
	"gitee.com/tomatomeatman/golang-repository/bricks2/model/dbinfo"
	"gitee.com/tomatomeatman/golang-repository/bricks2/model/msgentity"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/ginutil"
)

/**
 * 用户表LoginUser表基本业务操作结构体
 */
type LoginUserService struct {
	app.ServiceBaseFunc
}

/**
 * 新增
 * @param ctx Http请求对象
 * @param dbEntity 实体数据结构
 * @param params 数据
 * @return *msgentity.MsgEntity 返回执行结果
 */
func (service LoginUserService) Add(ctx ginutil.Context, dbEntity dbinfo.Entity, params map[string]interface{}) *msgentity.MsgEntity {
	No, ok := params["No"]
	if !ok || (strings.TrimSpace(No.(string)) == "") {
		return msgentity.Err(8401, "工号作为唯一标识不能为空!") //在同一根部门下,工号必须唯一
	}

	Name, ok := params["Name"]
	if !ok || (strings.TrimSpace(Name.(string)) == "") {
		return msgentity.Err(8402, "用户名称不能为空!") //在同一根部门下,工号必须唯一
	}

	temp := strings.ToLower(strings.TrimSpace(No.(string)))
	if (temp == "admin") || (temp == "superadmin") {
		return msgentity.Err(8403, "特殊标识,禁止使用!")
	}

	temp = strings.ToLower(strings.TrimSpace(Name.(string)))
	if (temp == "admin") || (temp == "superadmin") {
		return msgentity.Err(8404, "特殊名称,禁止使用!")
	}

	Pass, ok := params["pass"]
	if !ok || (strings.TrimSpace(Pass.(string)) == "") {
		return msgentity.Err(8405, "密码不能为空!")
	}

	appMd5Key := app.ReadConfigKey("App", "Md5Key", "555").(string)
	params["pass"] = md5util.Lower(Pass, appMd5Key)

	Owner, ok := params["owner"]
	if !ok || (strings.TrimSpace(Owner.(string)) == "") {
		params["owner"] = "BaseSystem.LoginUser"
	}

	return service.ServiceBaseFunc.Add(ctx, dbEntity, params)
}

/**
 * 修改
 * @param entity 对象类型
 * @param id 记录编号值
 * @param edition 记录版本号
 * @param data 待更新的字段和值
 * @return *msgentity.MsgEntity 返回执行结果
 */
func (service LoginUserService) Edit(ctx ginutil.Context, entity dbinfo.Entity, id interface{}, edition int, data map[string]interface{}) *msgentity.MsgEntity {
	No, ok := data["No"]
	if ok && (strings.TrimSpace(No.(string)) == "") {
		return msgentity.Err(8401, "工号作为唯一标识不能为空!") //在同一根部门下,工号必须唯一
	}

	if ok {
		temp := strings.ToLower(strings.TrimSpace(No.(string)))
		if (temp == "admin") || (temp == "superadmin") {
			return msgentity.Err(8402, "特殊标识,禁止修改!")
		}
	}

	Name, ok := data["Name"]
	if ok && (strings.TrimSpace(Name.(string)) == "") {
		return msgentity.Err(8403, "用户名称不能为空!") //在同一根部门下,工号必须唯一
	}

	if ok {
		temp := strings.ToLower(strings.TrimSpace(Name.(string)))
		if (temp == "admin") || (temp == "superadmin") {
			return msgentity.Err(8404, "特殊名称,禁止使用!")
		}
	}

	delete(data, "pass") //不允许修改密码

	Owner, ok := data["owner"]
	if ok && (strings.TrimSpace(Owner.(string)) == "") {
		data["owner"] = "BaseSystem.LoginUser"
	}

	return service.ServiceBaseFunc.Edit(ctx, entity, data)
}

/**
 * 删除
 * @param entity 对象类型
 * @param id 记录编号值
 * @param edition 记录版本号
 * @return MsgEntity
 */
func (service LoginUserService) Del(ctx ginutil.Context, entity dbinfo.Entity, id interface{}, edition int) *msgentity.MsgEntity {
	if strings.Contains(",00000000,00000001,", id.(string)) {
		return msgentity.Err(8101, "特殊用户，禁止删除！")
	}

	return service.ServiceBaseFunc.Del(ctx, entity, id, edition)
}

/**
 * 修改密码
 * @param ctx
 * @param entity
 * @param Id
 * @param sOldPass
 * @param sNewPass
 * @return MsgEntity
 */
func (service LoginUserService) EditPass(ctx ginutil.Context, entity dbinfo.Entity, Id, sOldPass, sNewPass, Owner string) *msgentity.MsgEntity {
	if Id == "" {
		return msgentity.Err(8001, "用户编号不能为空！")
	}

	if sOldPass == "" {
		return msgentity.Err(8002, "旧密码不能为空！")
	}

	if sNewPass == "" {
		return msgentity.Err(8003, "新密码不能为空！")
	}

	me := app.CommonDao.HasById(entity, Id)
	if !me.Success {
		return msgentity.Err(8004, "没有查找到用户！")
	}

	me = app.CommonDao.GetValueByFieldName(entity, Id, []string{dbinfo.TableState[0], dbinfo.TablePassword[0], dbinfo.TableEdition[0]}, "", false)
	if !me.Success {
		return msgentity.Err(8005, "没有查找到对应用户！")
	}

	m := me.Data.(map[string]interface{})
	if m[dbinfo.TableState[0]].(int64) != 1 {
		return msgentity.Err(8007, "用户被限制！")
	}

	appMd5Key := app.ReadConfigKey("App", "Md5Key", "555").(string)
	sOldPassMd5 := md5util.Lower(sOldPass, appMd5Key)

	if sOldPassMd5 != m[dbinfo.TablePassword[0]].(string) {
		return msgentity.Err(8008, "提供的旧密码错误！")
	}

	sNewPassMd5 := md5util.Lower(sNewPass, appMd5Key)
	if sNewPassMd5 == sOldPassMd5 {
		return msgentity.Success(8999, "密码无需修改！")
	}

	return LoginUserDao{}.EditPass(dbinfo.EntityDbName(entity), Id, sNewPassMd5, sOldPassMd5, Id, Owner, m[dbinfo.TableEdition[0]].(int64))
}

func (service LoginUserService) MapToJson(m interface{}, v any) error {
	str, err := json.Marshal(m)
	if err != nil {
		return err
	}

	err = json.Unmarshal(str, v)
	if err != nil {
		return err
	}

	return nil
}

// 重置密码
func (service LoginUserService) ResetPass(currentUserId, Id, Pass, Owner string) *msgentity.MsgEntity {
	if Id == "" {
		return msgentity.Err(8001, "用户编号不能为空！")
	}

	if strings.TrimSpace(currentUserId) == "" {
		return msgentity.Err(8002, "未能获取操作人员信息！")
	}

	if ((currentUserId == "00000000") || (currentUserId == "00000001")) && (Id == currentUserId) { //超级管理员登录/管理员或当前用户
		return msgentity.Err(8003, "仅允许管理员或当前用户重置密码")
	}

	if strings.TrimSpace(Owner) == "" {
		return msgentity.Err(8004, "用户归属不能为空！")
	}

	entity := &LoginUser{}

	me := app.CommonDao.HasById(entity, Id)
	if !me.Success {
		return msgentity.Err(8005, "没有查找到用户！")
	}

	me = app.CommonDao.GetValueByFieldName(entity, Id, []string{dbinfo.TableState[0], "pass", dbinfo.TableEdition[0]}, "", false)
	if !me.Success {
		return msgentity.Err(8006, "没有查找到对应用户！")
	}

	m := me.Data.(map[string]interface{})
	if m[dbinfo.TableState[0]].(int64) != 1 {
		return msgentity.Err(8007, "用户被限制！")
	}

	appMd5Key := app.ReadConfigKey("App", "Md5Key", "555").(string)
	PassMd5 := md5util.Lower(Pass, appMd5Key)

	if m["pass"].(string) != PassMd5 {
		return msgentity.Err(8008, "提供的操作密码错误!")
	}

	NewPass := md5util.Lower("123456", appMd5Key)
	if NewPass == PassMd5 {
		return msgentity.Success(8999, "密码无需修改重置！")
	}

	return LoginUserDao{}.ResetPass(dbinfo.EntityDbName(entity), Id, NewPass, Id, Owner, m[dbinfo.TableEdition[0]].(int64))
}

// 根据用户编号设置用户类型
func (service LoginUserService) EditUserType(currentUserId, Id, Type, Owner string) *msgentity.MsgEntity {
	if Id == "" {
		return msgentity.Err(8001, "用户编号不能为空！")
	}

	if strings.TrimSpace(Type) == "" {
		return msgentity.Err(8002, "用户类型不能为空！")
	}

	if strings.TrimSpace(Owner) == "" {
		return msgentity.Err(8003, "用户归属不能为空！")
	}

	return LoginUserDao{}.EditUserType(dbinfo.EntityDbName(&LoginUser{}), Id, Type, Owner)
}

// 设置用户头像
func (service LoginUserService) EditUserHead(currentUserId, sHead string) *msgentity.MsgEntity {
	if currentUserId == "" {
		return msgentity.Err(8001, "用户编号不能为空！")
	}

	if strings.TrimSpace(sHead) == "" {
		return msgentity.Err(8002, "图片信息为空！")
	}

	//把base64图片转换为文件
	savePath := "./data/img/head/" + currentUserId + "/head.png"
	iCode, err := base64util.Base64ToPng(sHead, savePath)
	if err != nil {
		return msgentity.Err(iCode+7100, "图片转换失败！")
	}

	return msgentity.Success(8999, "设置成功！")
}

// 取用户头像
func (service LoginUserService) LookHead(currentUserId string) *msgentity.MsgEntity {
	if currentUserId == "" {
		return msgentity.Err(8001, "用户编号不能为空！")
	}

	//把base64图片转换为文件
	headPath := "./data/img/head/" + currentUserId + "/head.png"
	// 读取图片文件
	data, err := os.ReadFile(headPath)
	if err != nil {
		return msgentity.Err(8002, "读取图片文件失败！")
	}

	return msgentity.Success(data, "设置成功！")
}
