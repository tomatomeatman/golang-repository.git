package datacache

import (
	"gitee.com/tomatomeatman/golang-repository/bricks2/function/urlutil"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/ginutil"
)

// @Controller 登录服务控制器
type DataCacheController struct {
	// app.ControllerBaseFunc
	// ModuleEntity  LoginUser   //对应模块数据实体
	// ModuleService LoginServer //对应模块业务实体
}

/**
 * 初始化
 */
func init() {
	//app.RegisterController(&LoginController{})
	DataCacheController{}.RegisterUrl()
}

// 接口注册
func (control DataCacheController) RegisterUrl() {
	go ginutil.ControllerRegister("/data/cache/del", control.Del, ginutil.POST, ginutil.GET)
	go ginutil.ControllerRegister("/data/cache/find/page", control.FindAll, ginutil.POST)
}

// #region @Api {title=删除数据}
// @param {name=dbName dataType=string paramType=query explain=缓存名 required=true}
// @param {name=key dataType=string paramType=query explain=键 required=true}
// @return {type=MsgEntity explain=返回布尔值}
// @RequestMapping {name=Del type=POST value=/data/cache/del}
// #endregion
func (control DataCacheController) Del(ctx ginutil.Context) interface{} {
	dbName := urlutil.GetParam(ctx.Request, "dbName", "").(string)
	key := urlutil.GetParam(ctx.Request, "key", "").(string)

	return DataCacheServer{}.Del(dbName, key)
}

// #region @Api {title=查询所有数据}
// @param {name=dbName dataType=string paramType=query explain=缓存名 required=true}
// @return {type=MsgEntity explain=返回Map数据}
// @RequestMapping {name=FindAll type=POST value=/data/cache/find/all}
// #endregion
func (control DataCacheController) FindAll(ctx ginutil.Context) interface{} {
	dbName := urlutil.GetParam(ctx.Request, "dbName", "").(string)
	return DataCacheServer{}.FindByAll(dbName)
}
