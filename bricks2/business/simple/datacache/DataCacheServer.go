package datacache

import (
	"gitee.com/tomatomeatman/golang-repository/bricks2/model/msgentity"

	Log "github.com/cihub/seelog"

	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/leveldb"
)

type DataCacheServer struct {
	app.ServiceBaseFunc
}

/**
 * 删除数据
 * @param dbName 缓存名
 * @param key 键
 * @return *msgentity.MsgEntity
 */
func (serve DataCacheServer) Del(dbName, key string) *msgentity.MsgEntity {
	if dbName == "" {
		return msgentity.Err(8001, "缓存名不能为空！")
	}

	if key == "" {
		return msgentity.Err(8002, "键不能为空！")
	}

	err := leveldb.Del(dbName, key)
	if err != nil {
		Log.Error("删除数据发生异常:", err)
		return msgentity.Err(8003, "删除数据发生异常")
	}

	return msgentity.Success(8999, "删除成功")
}

/**
 * 查询所有数据
 * @param dbName 缓存名
 * @return *msgentity.MsgEntity
 */
func (serve DataCacheServer) FindByAll(dbName string) *msgentity.MsgEntity {
	if dbName == "" {
		return msgentity.Err(8001, "缓存名不能为空！")
	}

	result, err := leveldb.FindAll(dbName)
	if err != nil {
		Log.Error("查询数据发生异常:", err)
		return msgentity.Err(8003, "查询数据发生异常")
	}

	return msgentity.Success(result, "查询成功")
}
