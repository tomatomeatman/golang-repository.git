package login

import (
	"strings"

	"gitee.com/tomatomeatman/golang-repository/bricks2/model/msgentity"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/gorm"
	Log "github.com/cihub/seelog"
)

type LoginDao struct {
	app.DaoBaseFunc
}

/**
 * 取用户名或工号对应的用户集合
 * @param nameOrNo
 * @param owner
 * @return
 */
func (LoginDao) FindBynameOrNo(nameOrNo, owner string) *msgentity.MsgEntity {
	txt := `SELECT id,name,no,pass,pass_work,type,owner,state,attrib
	FROM ${BaseSystem}login_user
	WHERE no = @nameOrNo AND owner = @owner
	UNION 
	SELECT id,name,no,pass,pass_work,type,owner,state,attrib
	FROM ${BaseSystem}login_user
	WHERE name = @nameOrNo AND owner = @owner`

	tableName := owner
	if tableName == "" {
		tableName = "login_user"
	}

	txt = strings.Replace(txt, "${BaseSystem}login_user", tableName, -1)

	where := map[string]interface{}{
		"nameOrNo": nameOrNo,
		"owner":    owner,
	}

	list := []LoginUser{}
	dbResult := gorm.Raw(txt, where).Find(&list)
	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return msgentity.Err(1002, "查询发生异常:", dbResult.Error)
	}

	return msgentity.Success(list, "查询成功")
}
