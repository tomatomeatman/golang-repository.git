package login

/**
 * 登录缓存对象
 * 非数据库实体
 */
type LoginUser struct {
	Id       string            `json:"id" gorm:"column:id; type:varchar; NOT NULL; primary_key" defaultData:"''" comment:"表编号"`
	Name     string            `json:"name" gorm:"column:name; type:varchar; NOT NULL" defaultData:"''" comment:"名称"`
	No       string            `json:"no" gorm:"column:no; type:varchar; NOT NULL" defaultData:"''" comment:"标识(组合唯一)"`
	Type     string            `json:"type" gorm:"column:type; type:varchar; NOT NULL; DEFAULT '管理员'" defaultData:"'管理员'" comment:"类型"`
	Pass     string            `json:"pass" gorm:"column:pass; type:varchar; NOT NULL" defaultData:"''" comment:"密码"`
	PassWork string            `json:"passWork" gorm:"column:pass_work; type:varchar" defaultData:"''" comment:"业务密码"`
	Owner    string            `json:"owner" gorm:"column:owner; type:varchar; DEFAULT 'BaseSystem.LoginUser'" defaultData:"'BaseSystem.LoginUser'" comment:"来源"`
	Attrib   string            `json:"attrib" gorm:"column:attrib; type:longtext" defaultData:"''" comment:"附属信息"`
	State    int               `json:"state" gorm:"column:state; type:int; NOT NULL; DEFAULT '1'" defaultData:"1" comment:"状态(枚举,1:启用;2:禁用)"`
	LastDate int64             `json:"dLastDate"`         //最后访问时间
	Token    string            `json:"token"`             //分配的Token
	Device   int               `json:"device"`            //设备类型,1:PC,2:手机,3:平板,4.....
	Attached map[string]string `json:"attached" gorm:"-"` //与登录相关的附属信息
}

/**
 * 复制
 * @return
 */
func (lu *LoginUser) Clone() *LoginUser {
	result := LoginUser{}
	result.Id = lu.Id
	result.Name = lu.Name
	result.No = lu.No
	result.Type = lu.Type
	result.Pass = lu.Pass
	result.PassWork = lu.PassWork
	result.Owner = lu.Owner
	result.Attrib = lu.Attrib
	result.State = lu.State
	result.LastDate = lu.LastDate
	result.Token = lu.Token
	result.Device = lu.Device
	result.Attached = lu.Attached

	if len(lu.Attached) > 0 {
		result.Attached = map[string]string{}
		for key, val := range lu.Attached {
			result.Attached[key] = val
		}
	}

	return &result
}

/**
 * 复制
 * @return
 */
func (lu *LoginUser) CloneToMap() map[string]interface{} {
	result := map[string]interface{}{}
	result["id"] = lu.Id
	result["name"] = lu.Name
	result["no"] = lu.No
	result["pass"] = lu.Pass
	result["passWork"] = lu.PassWork
	result["type"] = lu.Type
	result["owner"] = lu.Owner
	result["state"] = lu.State
	result["lastDate"] = lu.LastDate
	result["token"] = lu.Token

	if len(lu.Attached) > 0 {
		obj := map[string]string{}
		for key, val := range lu.Attached {
			obj[key] = val
		}

		result["attached"] = obj
	}

	return result
}
