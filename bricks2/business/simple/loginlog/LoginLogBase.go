package loginlog

import (
	"time"

	"gitee.com/tomatomeatman/golang-repository/bricks2/model/dbinfo"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/gorm"
)

var tableInfo = []string{"", "base_system", "login_log"}

/**
 * 初始化
 */
func init() {
	dbinfo.RegisterEntity("LoginLogBase", &LoginLogBase{}) //注册注册数据库实体类结构体
}

/**
 * 登录日志'login_log'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type LoginLogBase struct {
	Id           int64     `json:"id" gorm:"column:id; type:bigintAUTO_INCREMENT; NOT NULL; primary_key" defaultData:"0" comment:"记录编号"`
	Name         string    `json:"name" gorm:"column:name; type:varchar" defaultData:"''" comment:"日志类型(冗余)"`
	Type         int       `json:"type" gorm:"column:type; type:int; NOT NULL; DEFAULT '1'" defaultData:"1" comment:"日志类型"`
	UserId       string    `json:"userId" gorm:"column:user_id; type:varchar" defaultData:"''" comment:"用户编号"`
	User         string    `json:"user" gorm:"column:user; type:varchar" defaultData:"''" comment:"用户名(冗余)"`
	Ip           string    `json:"ip" gorm:"column:ip; type:varchar" defaultData:"''" comment:"IP地址"`
	MacAddress   string    `json:"macAddress" gorm:"column:mac_address; type:varchar" defaultData:"''" comment:"MAC地址"`
	LogDate      time.Time `json:"logDate" gorm:"column:log_date; type:datetime" defaultData:"'2025-02-06 12:05:49'" comment:"发生时间"`
	LastCall     time.Time `json:"lastCall" gorm:"column:last_call; type:datetime" defaultData:"'2025-02-06 12:05:49'" comment:"最后访问时间"`
	Memo         string    `json:"memo" gorm:"column:memo; type:varchar" defaultData:"''" comment:"备注"`
	Creator      string    `json:"creator" gorm:"column:creator; type:varchar; NOT NULL; DEFAULT '00000000'" defaultData:"'00000000'" comment:"创建者"`
	CreatorDate  time.Time `json:"creatorDate" gorm:"column:creator_date; type:datetime; NOT NULL" defaultData:"'2025-02-06 12:05:49'" comment:"创建时间"`
	Modifieder   string    `json:"modifieder" gorm:"column:modifieder; type:varchar; NOT NULL; DEFAULT '00000000'" defaultData:"'00000000'" comment:"修改人"`
	ModifiedDate time.Time `json:"modifiedDate" gorm:"column:modified_date; type:datetime; NOT NULL" defaultData:"'2025-02-06 12:05:49'" comment:"修改时间"`
	State        int       `json:"state" gorm:"column:state; type:int; NOT NULL; DEFAULT '1'" defaultData:"1" comment:"状态"`
	Rank         int       `json:"rank" gorm:"column:index; type:int; NOT NULL; DEFAULT '1'" defaultData:"1" comment:"序号"`
	Edition      int       `json:"edition" gorm:"column:edition; type:int; NOT NULL; DEFAULT '1'" defaultData:"1" comment:"版本号"`
}

/**
 * 创建结构实体
 * @return
 */
func (LoginLogBase) New() dbinfo.Entity {
	return &LoginLogBase{}
}

/**
 * 取基础实体,用于在子类(嵌套结构体)时同样获得基类
 */
func (LoginLogBase) BaseEntity() dbinfo.Entity {
	return &LoginLogBase{}
}

/**
 * 结构体映射表名,处理结构体名称与表名不一致的情况
 * @return
 */
func (LoginLogBase) TableName() string {
	if tableInfo[0] != "" {
		return tableInfo[0]
	}

	tableInfo[0] = gorm.GetDbName(tableInfo[1]) + tableInfo[2]

	return tableInfo[0]
}

/**
 * 结构体映射库名,去除'Dev_'等前缀
 * @return
 */
func (LoginLogBase) OwnerName() string {
	return tableInfo[1]
}

/**
 * 结构体映射表名,无库名
 * @return
 */
func (LoginLogBase) OwnerTable() string {
	return tableInfo[2]
}

/**
 * 结构体映射表的字段名串
 * @return
 */
func (LoginLogBase) BaseColumnNames() string {
	return "Id,Name,Type,UserId,User,Ip,MacAddress,LogDate,LastCall,Memo,Creator,CreatorDate,Modifieder,ModifiedDate,State,Rank,Edition"
}

/**
 * 取数据结构信息
 * @param name 字段名
 * @return
 */
func (LoginLogBase) GetDataInfo(name string) *dbinfo.DataInfo {
	switch name {
	case "Id":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_log", Name: "id", Comment: "记录编号", IsDbField: true, DbFileType: "bigint", DefaultData: "", GoDefaultData: 0, IsExtra: true, IsKey: true, IsNull: false, MaxLength: 20, IsDecimal: false, IntLength: 20, DecimalLength: 0, Index: 0, IsBigTxt: false}
	case "Name":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_log", Name: "name", Comment: "日志类型(冗余)", IsDbField: true, DbFileType: "varchar", DefaultData: "", GoDefaultData: "", IsExtra: false, IsKey: false, IsNull: true, MaxLength: 32, IsDecimal: false, IntLength: 32, DecimalLength: 0, Index: 1, IsBigTxt: false}
	case "Type":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_log", Name: "type", Comment: "日志类型", IsDbField: true, DbFileType: "int", DefaultData: "1", GoDefaultData: 1, IsExtra: false, IsKey: false, IsNull: false, MaxLength: 11, IsDecimal: false, IntLength: 11, DecimalLength: 0, Index: 2, IsBigTxt: false}
	case "UserId":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_log", Name: "user_id", Comment: "用户编号", IsDbField: true, DbFileType: "varchar", DefaultData: "", GoDefaultData: "", IsExtra: false, IsKey: false, IsNull: true, MaxLength: 8, IsDecimal: false, IntLength: 8, DecimalLength: 0, Index: 3, IsBigTxt: false}
	case "User":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_log", Name: "user", Comment: "用户名(冗余)", IsDbField: true, DbFileType: "varchar", DefaultData: "", GoDefaultData: "", IsExtra: false, IsKey: false, IsNull: true, MaxLength: 32, IsDecimal: false, IntLength: 32, DecimalLength: 0, Index: 4, IsBigTxt: false}
	case "Ip":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_log", Name: "ip", Comment: "IP地址", IsDbField: true, DbFileType: "varchar", DefaultData: "", GoDefaultData: "", IsExtra: false, IsKey: false, IsNull: true, MaxLength: 32, IsDecimal: false, IntLength: 32, DecimalLength: 0, Index: 5, IsBigTxt: false}
	case "MacAddress":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_log", Name: "mac_address", Comment: "MAC地址", IsDbField: true, DbFileType: "varchar", DefaultData: "", GoDefaultData: "", IsExtra: false, IsKey: false, IsNull: true, MaxLength: 64, IsDecimal: false, IntLength: 64, DecimalLength: 0, Index: 6, IsBigTxt: false}
	case "LogDate":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_log", Name: "log_date", Comment: "发生时间", IsDbField: true, DbFileType: "datetime", DefaultData: "", GoDefaultData: time.Now().Format("2006-01-02 15:04:05"), IsExtra: false, IsKey: false, IsNull: true, MaxLength: 23, IsDecimal: false, IntLength: 23, DecimalLength: 0, Index: 7, IsBigTxt: false}
	case "LastCall":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_log", Name: "last_call", Comment: "最后访问时间", IsDbField: true, DbFileType: "datetime", DefaultData: "", GoDefaultData: time.Now().Format("2006-01-02 15:04:05"), IsExtra: false, IsKey: false, IsNull: true, MaxLength: 23, IsDecimal: false, IntLength: 23, DecimalLength: 0, Index: 8, IsBigTxt: false}
	case "Memo":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_log", Name: "memo", Comment: "备注", IsDbField: true, DbFileType: "varchar", DefaultData: "", GoDefaultData: "", IsExtra: false, IsKey: false, IsNull: true, MaxLength: 200, IsDecimal: false, IntLength: 200, DecimalLength: 0, Index: 9, IsBigTxt: false}
	case "Creator":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_log", Name: "creator", Comment: "创建者", IsDbField: true, DbFileType: "varchar", DefaultData: "00000000", GoDefaultData: "00000000", IsExtra: false, IsKey: false, IsNull: false, MaxLength: 8, IsDecimal: false, IntLength: 8, DecimalLength: 0, Index: 10, IsBigTxt: false}
	case "CreatorDate":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_log", Name: "creator_date", Comment: "创建时间", IsDbField: true, DbFileType: "datetime", DefaultData: "", GoDefaultData: time.Now().Format("2006-01-02 15:04:05"), IsExtra: false, IsKey: false, IsNull: false, MaxLength: 23, IsDecimal: false, IntLength: 23, DecimalLength: 0, Index: 11, IsBigTxt: false}
	case "Modifieder":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_log", Name: "modifieder", Comment: "修改人", IsDbField: true, DbFileType: "varchar", DefaultData: "00000000", GoDefaultData: "00000000", IsExtra: false, IsKey: false, IsNull: false, MaxLength: 8, IsDecimal: false, IntLength: 8, DecimalLength: 0, Index: 12, IsBigTxt: false}
	case "ModifiedDate":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_log", Name: "modified_date", Comment: "修改时间", IsDbField: true, DbFileType: "datetime", DefaultData: "", GoDefaultData: time.Now().Format("2006-01-02 15:04:05"), IsExtra: false, IsKey: false, IsNull: false, MaxLength: 23, IsDecimal: false, IntLength: 23, DecimalLength: 0, Index: 13, IsBigTxt: false}
	case "State":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_log", Name: "state", Comment: "状态", IsDbField: true, DbFileType: "int", DefaultData: "1", GoDefaultData: 1, IsExtra: false, IsKey: false, IsNull: false, MaxLength: 11, IsDecimal: false, IntLength: 11, DecimalLength: 0, Index: 14, IsBigTxt: false}
	case "Rank":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_log", Name: "rank", Comment: "序号", IsDbField: true, DbFileType: "int", DefaultData: "1", GoDefaultData: 1, IsExtra: false, IsKey: false, IsNull: false, MaxLength: 11, IsDecimal: false, IntLength: 11, DecimalLength: 0, Index: 15, IsBigTxt: false}
	case "Edition":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "login_log", Name: "edition", Comment: "版本号", IsDbField: true, DbFileType: "int", DefaultData: "1", GoDefaultData: 1, IsExtra: false, IsKey: false, IsNull: false, MaxLength: 11, IsDecimal: false, IntLength: 11, DecimalLength: 0, Index: 16, IsBigTxt: false}
	default:
		return nil
	}
}
