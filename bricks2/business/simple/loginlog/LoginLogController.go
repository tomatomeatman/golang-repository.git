package loginlog

import (
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/ginutil"
)

// @Controller 登录日志控制器
type LoginLogController struct {
	app.ControllerBaseFunc                 //通用控制层接口方法
	ModuleEntity           LoginLog        //对应模块数据实体
	ModuleService          LoginLogService //对应模块业务实体
}

/**
 * 初始化
 */
func init() {
	app.RegisterController(&LoginLogController{})
}

// 接口注册
func (control LoginLogController) RegisterUrl() {
	go ginutil.ControllerRegister("/login/log/del", control.Del, ginutil.POST)
	go ginutil.ControllerRegister("/login/log/find/page", control.FindByPage, ginutil.POST)
}

// #region @Api {title=删除数据}
// @param {name=id dataType=int64 paramType=query explain=记录编号 required=true}
// @param {name=edition dataType=int paramType=query explain=版本号 required=true}
// @return {type=MsgEntity explain=返回影响数}
// @RequestMapping {name=Del type=POST value=/login/log/del}
// #endregion
func (control LoginLogController) Del(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Del(ctx, &control)
}

// #region @Api {title=查询分页数据}
// @param {name=data dataType=json paramType=body explain=FindByPageParam结构数据 explainType=FindByPageParam<LoginLog> required=true}
// @return {type=MsgEntity explain=返回分页数据}
// @RequestMapping {name=FindByPage type=POST value=/login/log/find/page}
// #endregion
func (control LoginLogController) FindByPage(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByPage(ctx, &control)
}
