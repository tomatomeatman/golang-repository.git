package userandright

//简化的权限信息实体
type SimpleRight struct {
	RightId        string `json:"rightId" gorm:"column:right_id; type:varchar; NOT NULL" defaultData:"''" comment:"权限编号"`
	English        string `json:"english" gorm:"column:english; type:varchar" defaultData:"''" comment:"权限标识名称"`
	ControllerPath string `json:"controllerPath" gorm:"column:controller_path; type:varchar" comment:"URL路径值"`
}
