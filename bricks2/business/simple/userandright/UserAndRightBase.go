package userandright

import (
	"time"

	"gitee.com/tomatomeatman/golang-repository/bricks2/model/dbinfo"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/gorm"
)

var tableInfo = []string{"", "base_system", "user_and_right"}

/**
 * 初始化
 */
func init() {
	dbinfo.RegisterEntity("UserAndRightBase", &UserAndRightBase{}) //注册注册数据库实体类结构体
}

/**
 * 用户权限表'user_and_right'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type UserAndRightBase struct {
	Id           int64     `json:"id" gorm:"column:id; type:bigintAUTO_INCREMENT; NOT NULL; primary_key" defaultData:"0" comment:"记录编号"`
	UserId       string    `json:"userId" gorm:"column:user_id; type:varchar; NOT NULL" defaultData:"''" comment:"用户编号"`
	RightId      string    `json:"rightId" gorm:"column:right_id; type:varchar; NOT NULL" defaultData:"''" comment:"权限编号"`
	Type         int       `json:"type" gorm:"column:type; type:int; DEFAULT '1'" defaultData:"1" comment:"权限类型"`
	Memo         string    `json:"memo" gorm:"column:memo; type:varchar" defaultData:"''" comment:"备注"`
	Creator      string    `json:"creator" gorm:"column:creator; type:varchar; NOT NULL; DEFAULT '00000000'" defaultData:"'00000000'" comment:"创建者"`
	CreatorDate  time.Time `json:"creatorDate" gorm:"column:creator_date; type:datetime; NOT NULL" defaultData:"'2025-02-06 12:05:49'" comment:"创建时间"`
	Modifieder   string    `json:"modifieder" gorm:"column:modifieder; type:varchar; NOT NULL; DEFAULT '00000000'" defaultData:"'00000000'" comment:"修改人"`
	ModifiedDate time.Time `json:"modifiedDate" gorm:"column:modified_date; type:datetime; NOT NULL" defaultData:"'2025-02-06 12:05:49'" comment:"修改时间"`
	State        int       `json:"state" gorm:"column:state; type:int; NOT NULL; DEFAULT '1'" defaultData:"1" comment:"状态"`
	Rank         int       `json:"rank" gorm:"column:index; type:int; NOT NULL; DEFAULT '1'" defaultData:"1" comment:"序号"`
	Edition      int       `json:"edition" gorm:"column:edition; type:int; NOT NULL; DEFAULT '1'" defaultData:"1" comment:"版本号"`
}

/**
 * 创建结构实体
 * @return
 */
func (UserAndRightBase) New() dbinfo.Entity {
	return &UserAndRightBase{}
}

/**
 * 取基础实体,用于在子类(嵌套结构体)时同样获得基类
 */
func (UserAndRightBase) BaseEntity() dbinfo.Entity {
	return &UserAndRightBase{}
}

/**
 * 结构体映射表名,处理结构体名称与表名不一致的情况
 * @return
 */
func (UserAndRightBase) TableName() string {
	if tableInfo[0] != "" {
		return tableInfo[0]
	}

	tableInfo[0] = gorm.GetDbName(tableInfo[1]) + tableInfo[2]

	return tableInfo[0]
}

/**
 * 结构体映射库名,去除'Dev_'等前缀
 * @return
 */
func (UserAndRightBase) OwnerName() string {
	return tableInfo[1]
}

/**
 * 结构体映射表名,无库名
 * @return
 */
func (UserAndRightBase) OwnerTable() string {
	return tableInfo[2]
}

/**
 * 结构体映射表的字段名串
 * @return
 */
func (UserAndRightBase) BaseColumnNames() string {
	return "Id,UserId,RightId,Type,Memo,Creator,CreatorDate,Modifieder,ModifiedDate,State,Rank,Edition"
}

/**
 * 取数据结构信息
 * @param name 字段名
 * @return
 */
func (UserAndRightBase) GetDataInfo(name string) *dbinfo.DataInfo {
	switch name {
	case "Id":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "user_and_right", Name: "id", Comment: "记录编号", IsDbField: true, DbFileType: "bigint", DefaultData: "", GoDefaultData: 0, IsExtra: true, IsKey: true, IsNull: false, MaxLength: 20, IsDecimal: false, IntLength: 20, DecimalLength: 0, Index: 0, IsBigTxt: false}
	case "UserId":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "user_and_right", Name: "user_id", Comment: "用户编号", IsDbField: true, DbFileType: "varchar", DefaultData: "", GoDefaultData: "", IsExtra: false, IsKey: false, IsNull: false, MaxLength: 8, IsDecimal: false, IntLength: 8, DecimalLength: 0, Index: 1, IsBigTxt: false}
	case "RightId":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "user_and_right", Name: "right_id", Comment: "权限编号", IsDbField: true, DbFileType: "varchar", DefaultData: "", GoDefaultData: "", IsExtra: false, IsKey: false, IsNull: false, MaxLength: 20, IsDecimal: false, IntLength: 20, DecimalLength: 0, Index: 2, IsBigTxt: false}
	case "Type":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "user_and_right", Name: "type", Comment: "权限类型", IsDbField: true, DbFileType: "int", DefaultData: "1", GoDefaultData: 1, IsExtra: false, IsKey: false, IsNull: true, MaxLength: 11, IsDecimal: false, IntLength: 11, DecimalLength: 0, Index: 3, IsBigTxt: false}
	case "Memo":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "user_and_right", Name: "memo", Comment: "备注", IsDbField: true, DbFileType: "varchar", DefaultData: "", GoDefaultData: "", IsExtra: false, IsKey: false, IsNull: true, MaxLength: 200, IsDecimal: false, IntLength: 200, DecimalLength: 0, Index: 4, IsBigTxt: false}
	case "Creator":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "user_and_right", Name: "creator", Comment: "创建者", IsDbField: true, DbFileType: "varchar", DefaultData: "00000000", GoDefaultData: "00000000", IsExtra: false, IsKey: false, IsNull: false, MaxLength: 8, IsDecimal: false, IntLength: 8, DecimalLength: 0, Index: 5, IsBigTxt: false}
	case "CreatorDate":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "user_and_right", Name: "creator_date", Comment: "创建时间", IsDbField: true, DbFileType: "datetime", DefaultData: "", GoDefaultData: time.Now().Format("2006-01-02 15:04:05"), IsExtra: false, IsKey: false, IsNull: false, MaxLength: 23, IsDecimal: false, IntLength: 23, DecimalLength: 0, Index: 6, IsBigTxt: false}
	case "Modifieder":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "user_and_right", Name: "modifieder", Comment: "修改人", IsDbField: true, DbFileType: "varchar", DefaultData: "00000000", GoDefaultData: "00000000", IsExtra: false, IsKey: false, IsNull: false, MaxLength: 8, IsDecimal: false, IntLength: 8, DecimalLength: 0, Index: 7, IsBigTxt: false}
	case "ModifiedDate":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "user_and_right", Name: "modified_date", Comment: "修改时间", IsDbField: true, DbFileType: "datetime", DefaultData: "", GoDefaultData: time.Now().Format("2006-01-02 15:04:05"), IsExtra: false, IsKey: false, IsNull: false, MaxLength: 23, IsDecimal: false, IntLength: 23, DecimalLength: 0, Index: 8, IsBigTxt: false}
	case "State":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "user_and_right", Name: "state", Comment: "状态", IsDbField: true, DbFileType: "int", DefaultData: "1", GoDefaultData: 1, IsExtra: false, IsKey: false, IsNull: false, MaxLength: 11, IsDecimal: false, IntLength: 11, DecimalLength: 0, Index: 9, IsBigTxt: false}
	case "Rank":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "user_and_right", Name: "rank", Comment: "序号", IsDbField: true, DbFileType: "int", DefaultData: "1", GoDefaultData: 1, IsExtra: false, IsKey: false, IsNull: false, MaxLength: 11, IsDecimal: false, IntLength: 11, DecimalLength: 0, Index: 10, IsBigTxt: false}
	case "Edition":
		return &dbinfo.DataInfo{DbObj: "BaseSystem", DbName: "base_system", TableName: "user_and_right", Name: "edition", Comment: "版本号", IsDbField: true, DbFileType: "int", DefaultData: "1", GoDefaultData: 1, IsExtra: false, IsKey: false, IsNull: false, MaxLength: 11, IsDecimal: false, IntLength: 11, DecimalLength: 0, Index: 11, IsBigTxt: false}
	default:
		return nil
	}
}
