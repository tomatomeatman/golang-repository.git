package userandright

import "gitee.com/tomatomeatman/golang-repository/bricks2/utils/app"

/**
 * 用户权限表UserAndRight表基本业务操作结构体
 */
type UserAndRightDao struct {
	app.DaoBaseFunc
}
