package userandright

import "gitee.com/tomatomeatman/golang-repository/bricks2/model/dbinfo"

/**
 * 用户权限表'UserAndRight'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type UserAndRight struct {
	UserAndRightBase
}

/**
 * 创建结构实体,并赋予默认值
 */
func (UserAndRight) New() dbinfo.Entity {
	return &UserAndRight{}
}
