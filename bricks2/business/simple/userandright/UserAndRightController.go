package userandright

import (
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/app"

	"gitee.com/tomatomeatman/golang-repository/bricks2/function/urlutil"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/ginutil"
)

// @Controller 用户权限控制器
type UserAndRightController struct {
	app.ControllerBaseFunc                     //通用控制层接口方法
	ModuleEntity           UserAndRight        //对应模块数据实体
	ModuleService          UserAndRightService //对应模块业务实体
	ModuleDao              UserAndRightDao     //对应的数据处理实体
}

/**
 * 初始化
 */
func init() {
	app.RegisterController(&UserAndRightController{})
}

// -- 检查待新增内容是否存在重复数据(单独字段重复即重复)集合,注意:int必须是1、10、100、1000 --//
func (control UserAndRightController) CheckRepeatCombination() []string {
	return []string{"user_id", "right_id"}
}

// 接口注册
func (control UserAndRightController) RegisterUrl() {
	go ginutil.ControllerRegister("/user/and/right/add", control.Add, ginutil.POST)
	go ginutil.ControllerRegister("/user/and/right/del", control.Del, ginutil.POST)
	go ginutil.ControllerRegister("/user/and/right/edit", control.Edit, ginutil.POST)
	go ginutil.ControllerRegister("/user/and/right/find/id", control.FindById, ginutil.POST)
	go ginutil.ControllerRegister("/user/and/right/find/key", control.FindByKey, ginutil.POST)
	go ginutil.ControllerRegister("/user/and/right/find/page/row", control.FindByRow, ginutil.POST)
	go ginutil.ControllerRegister("/user/and/right/find/page", control.FindByPage, ginutil.POST)
	go ginutil.ControllerRegister("/user/and/right/check/right", control.CheckRight, ginutil.POST)
	go ginutil.ControllerRegister("/user/and/right/lasttime", control.LastTime, ginutil.POST)
	go ginutil.ControllerRegister("/user/and/right/find/rightid", control.FindRightId, ginutil.POST)
	go ginutil.ControllerRegister("/user/and/right/find/english", control.FindEnglishByUserId, ginutil.POST)
}

// #region @Api {title=新增}
// @param {name=data dataType=json paramType=body explain=UserAndRight结构数据 explainType=UserAndRight required=true}
// @return {type=json explainType=MsgEntity<UserAndRight> explain=返回对象}
// @RequestMapping {name=Add type=POST value=/user/and/right/add}
// #endregion
func (control UserAndRightController) Add(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Add(ctx, &control)
}

// #region @Api {title=删除数据}
// @param {name=id dataType=int64 paramType=query explain=记录编号 required=true}
// @param {name=edition dataType=int paramType=query explain=版本号 required=true}
// @return {type=json explainType=MsgEntity<int> explain=返回影响数}
// @RequestMapping {name=Del type=POST value=/user/and/right/del}
// #endregion
func (control UserAndRightController) Del(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Del(ctx, &control)
}

// #region @Api {title=修改数据}
// @param {name=data dataType=json paramType=body explain=UserAndRight结构数据 explainType=UserAndRight required=true}
// @return {type=json explainType=MsgEntity<int> explain=返回码值}
// @RequestMapping {name=Edit type=POST value=/user/and/right/edit}
// #endregion
func (control UserAndRightController) Edit(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Edit(ctx, &control)
}

// #region @Api {title=查询全部}
// @param {name=data dataType=json paramType=body explain=map[string]interface结构数据 explainType=UserAndRight required=true}
// @return {type=json explainType=MsgEntity<UserAndRight> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindAll type=POST value=/user/and/right/find/all}
// #endregion
func (control UserAndRightController) FindAll(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindAll(ctx, &control)
}

// #region @Api {title=查询指定时间内数据}
// @param {name=dateSt dataType=string paramType=query explain=格式:YYYY-MM-DD HH:mm:ss required=true}
// @param {name=dateEd dataType=string paramType=query explain=格式:YYYY-MM-DD HH:mm:ss required=true}
// @return {type=json explainType=MsgEntity<UserAndRight> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByDate type=POST value=/user/and/right/find/date}
// #endregion
func (control UserAndRightController) FindByDate(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByDate(ctx, &control)
}

// #region @Api {title=根据记录编号取对象}
// @param {name=id dataType=int64 paramType=query explain=记录编号 required=true}
// @return {type=json explainType=MsgEntity<UserAndRight> explain=返回对象}
// @RequestMapping {name=FindById type=POST value=/user/and/right/find/id}
// #endregion
func (control UserAndRightController) FindById(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindById(ctx, &control)
}

// #region @Api {title=根据关键值取对象集合}
// @param {name=data dataType=json paramType=body explain=UserAndRight结构数据 explainType=UserAndRight required=true}
// @return {type=json explainType=MsgEntity<UserAndRight> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByKey type=POST value=/user/and/right/find/key}
// #endregion
func (control UserAndRightController) FindByKey(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByKey(ctx, &control)
}

// #region @Api {title=根据记录编号查询符合分页数据的某条记录}
// @param {name=id dataType=int64 paramType=query explain=记录编号 required=true}
// @return {type=json explainType=MsgEntity<UserAndRight> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByRow type=POST value=/user/and/right/find/page/row}
// #endregion
func (control UserAndRightController) FindByRow(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByRow(ctx, &control)
}

// #region @Api {title=查询分页数据}
// @param {name=data dataType=json paramType=body explain=FindByPageParam结构数据 explainType=FindByPageParam<UserAndRight> required=true}
// @return {type=json explainType=MsgEntity<Page<UserAndRight>> explain=返回分页数据}
// @RequestMapping {name=FindByPage type=POST value=/user/and/right/find/page}
// #endregion
func (control UserAndRightController) FindByPage(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByPage(ctx, &control)
}

// #region @Api {title=权限验证 explain=验证指定用户是否有访问指定url的权限(内部拦截器用)}
// @param {name=UserId dataType=string paramType=query explain=用户编号 required=false}
// @param {name=url dataType=string paramType=query explain=权限值 required=true}
// @RequestMapping {name=FindByPage type=POST value=/user/and/right/check/right}
// #endregion
func (control UserAndRightController) CheckRight(ctx ginutil.Context) interface{} {
	UserId := urlutil.GetParam(ctx.Request, "userId", "").(string)
	url := urlutil.GetParam(ctx.Request, "url", "").(string)

	return UserAndRightService{}.CheckRight(ctx, UserId, url)
}

// #region @Api {title=缓存权限时间 explain=根据用户查询用户所拥有的权限的最后更新时间}
// @param {name=UserId dataType=string paramType=query explain=用户编号 required=true}
// @RequestMapping {name=FindByPage type=POST value=/user/and/right/lasttime}
// #endregion
func (control UserAndRightController) LastTime(ctx ginutil.Context) interface{} {
	UserId := urlutil.GetParam(ctx.Request, "userId", "").(string)

	return UserAndRightService{}.LastTime(ctx, UserId)
}

// #region @Api {title=查询用户权限 explain=根据用户查询用户所拥有的权限编号集合}
// @param {name=UserId dataType=string paramType=query explain=用户编号 required=true}
// @RequestMapping {name=FindByPage type=POST value=/user/and/right/find/rightid}
// #endregion
func (control UserAndRightController) FindRightId(ctx ginutil.Context) interface{} {
	UserId := urlutil.GetParam(ctx.Request, "userId", "").(string)

	return UserAndRightService{}.FindRightId(UserId)
}

// #region @Api {title=查询权限标识字符串 explain=根据用户取权限标识字符串(一个权限标识代表了多个可访问的url路径)}
// @param {name=UserId dataType=string paramType=query explain=用户编号 required=true}
// @RequestMapping {name=FindByPage type=POST value=/user/and/right/find/english}
// #endregion
func (control UserAndRightController) FindEnglishByUserId(ctx ginutil.Context) interface{} {
	UserId := urlutil.GetParam(ctx.Request, "userId", "").(string)

	return UserAndRightService{}.FindEnglishByUserId(UserId)
}
