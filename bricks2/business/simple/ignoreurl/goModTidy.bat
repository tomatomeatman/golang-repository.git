cd /d %~dp0


SET GOPROXY=https://goproxy.cn
SET GONOSUMDB=gitee.com
SET GONOPROXY=gitee.com

SET "currentPath=%cd:\=/%"
SET "prefixToRemove=/bricks2/"


for /f "delims=" %%i in ('powershell -Command "[string]$path='%currentPath%'; $path -replace '^.*%prefixToRemove%', ''"') do set "relativePath=%%i"

del go.mod

del go.sum

go mod init gitee.com/tomatomeatman/golang-repository/bricks2/%relativePath%

:: go mod tidy -go=1.21.6
go mod tidy

go get -u ./...

pause