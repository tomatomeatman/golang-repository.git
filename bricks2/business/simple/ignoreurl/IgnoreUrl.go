package ignoreurl

import "gitee.com/tomatomeatman/golang-repository/bricks2/model/dbinfo"

var IgnoreUrl_tableName = ""

/**
 * 拦截器忽略路径'IgnoreUrl'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type IgnoreUrl struct {
	IgnoreUrlBase
	MustLoginText string `json:"mustLoginText" gorm:"<-:false;column:mustLoginText" defaultData:"1" comment:"必须登录(布尔值,1:是;2:否)"`
}

/**
 * 创建结构实体,并赋予默认值
 */
func (IgnoreUrl) New() dbinfo.Entity {
	return &IgnoreUrl{}
}
