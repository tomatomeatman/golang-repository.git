package ignoreurl

import (
	"gitee.com/tomatomeatman/golang-repository/bricks2/function/urlutil"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/ginutil"
)

// @Controller 拦截器忽略路径控制器
type IgnoreUrlController struct {
	app.ControllerBaseFunc
	ModuleEntity  IgnoreUrl
	ModuleService IgnoreUrlService
}

/**
 * 初始化
 */
func init() {
	app.RegisterController(&IgnoreUrlController{})
}

// -- 检查待新增内容是否存在重复数据(单独字段重复即重复)集合,注意:int必须是1、10、100、1000 --//
func (control IgnoreUrlController) CheckRepeatAlone() map[string]int {
	return map[string]int{"UrlName": 1}
}

// 接口注册
func (control IgnoreUrlController) RegisterUrl() {
	go ginutil.ControllerRegister("/ignore/url/add", control.Add, ginutil.POST)
	go ginutil.ControllerRegister("/ignore/url/del", control.Del, ginutil.POST)
	go ginutil.ControllerRegister("/ignore/url/edit", control.Edit, ginutil.POST)
	go ginutil.ControllerRegister("/ignore/url/find/id", control.FindById, ginutil.POST)
	go ginutil.ControllerRegister("/ignore/url/find/key", control.FindByKey, ginutil.POST)
	go ginutil.ControllerRegister("/ignore/url/find/all", control.FindAll, ginutil.POST)
	go ginutil.ControllerRegister("/ignore/url/find/page/row", control.FindByRow, ginutil.POST)
	go ginutil.ControllerRegister("/ignore/url/find/page", control.FindByPage, ginutil.POST)
	go ginutil.ControllerRegister("/ignore/url/check", control.CheckIgnoreUrl, ginutil.POST)
	go ginutil.ControllerRegister("/ignore/url/clear/cache", control.ClearCache, ginutil.POST)
}

// #region @Api {title=新增}
// @param {name=data dataType=json paramType=body explain=IgnoreUrl结构数据 explainType=IgnoreUrl required=true}
// @return {type=json explainType=MsgEntity<IgnoreUrl> explain=返回对象}
// @RequestMapping {name=Add type=POST value=/ignore/url/add}
// #endregion
func (control IgnoreUrlController) Add(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Add(ctx, &control)
}

// #region @Api {title=删除数据}
// @param {name=id dataType=string paramType=query explain=记录编号 required=true}
// @param {name=edition dataType=int paramType=query explain=版本号 required=true}
// @return {type=json explainType=MsgEntity<int> explain=返回影响数}
// @RequestMapping {name=Del type=POST value=/ignore/url/del}
// #endregion
func (control IgnoreUrlController) Del(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Del(ctx, &control)
}

// #region @Api {title=修改数据}
// @param {name=data dataType=json paramType=body explain=IgnoreUrl结构数据 explainType=IgnoreUrl required=true}
// @return {type=json explainType=MsgEntity<int> explain=返回码值}
// @RequestMapping {name=Edit type=POST value=/ignore/url/edit}
// #endregion
func (control IgnoreUrlController) Edit(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Edit(ctx, &control)
}

// #region @Api {title=查询全部}
// @param {name=data dataType=json paramType=body explain=map[string]interface结构数据 explainType=IgnoreUrl required=true}
// @return {type=json explainType=MsgEntity<IgnoreUrl> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindAll type=POST value=/ignore/url/find/all}
// #endregion
func (control IgnoreUrlController) FindAll(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindAll(ctx, &control)
}

// #region @Api {title=查询指定时间内数据}
// @param {name=dateSt dataType=string paramType=query explain=格式:YYYY-MM-DD HH:mm:ss required=true}
// @param {name=dateEd dataType=string paramType=query explain=格式:YYYY-MM-DD HH:mm:ss required=true}
// @return {type=json explainType=MsgEntity<IgnoreUrl> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByDate type=POST value=/ignore/url/find/date}
// #endregion
func (control IgnoreUrlController) FindByDate(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByDate(ctx, &control)
}

// #region @Api {title=根据记录编号取对象}
// @param {name=id dataType=string paramType=query explain=记录编号 required=true}
// @return {type=json explainType=MsgEntity<IgnoreUrl> explain=返回对象}
// @RequestMapping {name=FindById type=POST value=/ignore/url/find/id}
// #endregion
func (control IgnoreUrlController) FindById(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindById(ctx, &control)
}

// #region @Api {title=根据关键值取对象集合}
// @param {name=data dataType=json paramType=body explain=IgnoreUrl结构数据 explainType=IgnoreUrl required=true}
// @return {type=json explainType=MsgEntity<IgnoreUrl> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByKey type=POST value=/ignore/url/find/key}
// #endregion
func (control IgnoreUrlController) FindByKey(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByKey(ctx, &control)
}

// #region @Api {title=根据记录编号查询符合分页数据的某条记录}
// @param {name=id dataType=string paramType=query explain=记录编号 required=true}
// @return {type=json explainType=MsgEntity<IgnoreUrl> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByRow type=POST value=/ignore/url/find/page/row}
// #endregion
func (control IgnoreUrlController) FindByRow(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByRow(ctx, &control)
}

// #region @Api {title=查询分页数据}
// @param {name=data dataType=json paramType=body explain=FindByPageParam结构数据 explainType=FindByPageParam<IgnoreUrl> required=true}
// @return {type=json explainType=MsgEntity<Page<IgnoreUrl>> explain=返回分页数据}
// @RequestMapping {name=FindByPage type=POST value=/ignore/url/find/page}
// #endregion
func (control IgnoreUrlController) FindByPage(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByPage(ctx, &control)
}

// #region @Api {title=验证指定url是否在可忽略的访问路径中(给内部拦截器用,直接返回Boolean)}
// @param {name=url dataType=string paramType=query explain=待检验的url required=true}
// @param {name=userType dataType=string paramType=query explain=待检验的用户类型 required=false}
// @param {name=mustLogin dataType=int paramType=query explain=是否必须登录,1:是;2:否 required=false}
// @return {type=bool explain=返回是否在可忽略的访问路径中}
// @RequestMapping {name=FindByPage type=POST value=/ignore/url/check}
// #endregion
func (control IgnoreUrlController) CheckIgnoreUrl(ctx ginutil.Context) interface{} {
	url := urlutil.GetParam(ctx.Request, "url", "").(string)           //待检验的url
	userType := urlutil.GetParam(ctx.Request, "userType", "").(string) //待检验的用户类型
	mustLogin := urlutil.GetParam(ctx.Request, "mustLogin", 2).(int)   //是否必须登录,1:是;2:否

	return control.ModuleService.CheckIgnoreUrl(url, mustLogin == 1, userType)
}

// #region @Api {title=清理缓存 explain=直接操作数据库的情况下需要手工更新缓存}
// @return {type=MsgEntity explain=返回分页数据}
// @RequestMapping {name=FindByPage type=POST value=/ignore/url/clear/cache}
// #endregion
func (control IgnoreUrlController) ClearCache(ctx ginutil.Context) interface{} {
	return control.ModuleService.ClearCache(ctx, "", "")
}
