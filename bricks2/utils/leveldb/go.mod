module gitee.com/tomatomeatman/golang-repository/bricks2/utils/leveldb

go 1.23.4

require (
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	github.com/syndtr/goleveldb v1.0.0
)

require github.com/golang/snappy v1.0.0 // indirect
