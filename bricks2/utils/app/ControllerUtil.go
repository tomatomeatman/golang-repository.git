package app

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	uu "net/url"
	"reflect"
	"strings"
	"time"

	"gitee.com/tomatomeatman/golang-repository/bricks2/function/data/timeutil"
	"gitee.com/tomatomeatman/golang-repository/bricks2/function/urlutil"
	"gitee.com/tomatomeatman/golang-repository/bricks2/model/dbinfo"
	"gitee.com/tomatomeatman/golang-repository/bricks2/model/msgentity"
	"gitee.com/tomatomeatman/golang-repository/bricks2/model/set"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/ginutil"
	Log "github.com/cihub/seelog"
)

type ControllerUtil struct{}

/**
 * 新增
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) Add(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	bl, msg, data := urlutil.GetParams(ctx.Request, entity)
	if !bl {
		return msgentity.Err(data.(int), msg)
	}

	params := data.(map[string]interface{})
	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".Add"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.Add(ctx, entity, params)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 批量新增
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) Adds(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	objStr := urlutil.GetParam(ctx.Request, "objs", "").(string)
	if objStr == "" {
		return msgentity.Err(9001, "未能获取'objs'参数")
	}

	var objs []map[string]interface{}
	err := json.Unmarshal([]byte(objStr), &objs)
	if err != nil {
		Log.Error("参数'objs'转换出错：", err)
		return msgentity.Err(9002, "参数'objs'转换出错")
	}

	if len(objs) < 1 {
		return msgentity.Err(9003, "参数'objs'转换后没有数据")
	}

	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	entitys := make([]dbinfo.Entity, len(objs))
	for i := 0; i < len(objs); i++ {
		entitys = append(entitys, entity.New())
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".Adds"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.Adds(ctx, entitys, objs)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 新增树节点
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) AddNode(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	bl, msg, data := urlutil.GetParams(ctx.Request, entity)
	if !bl {
		return msgentity.Err(data.(int), msg)
	}

	params := data.(map[string]interface{})
	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".AddNode"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.AddNode(ctx, entity, params)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 删除
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) Del(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	id := urlutil.GetParamToId(ctx.Request, dbinfo.EntityKeyName(entity)) //取请求参数中的记录编号

	if id == nil || (fmt.Sprintf("%v", id) == "") {
		id = urlutil.GetParamToId(ctx.Request, "id")

		if id == nil || (fmt.Sprintf("%v", id) == "") {
			return msgentity.Err(9012, "未能获取'", dbinfo.EntityKeyName(entity), "'参数")
		}
	}

	Edition := urlutil.GetParamToEdition(ctx.Request, dbinfo.EntityHasEdition(entity), dbinfo.TableEdition[1]) //取请求参数中的版本号

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".Del"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.Del(ctx, entity, id, Edition)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 修改
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) Edit(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	id := urlutil.GetParamToId(ctx.Request, dbinfo.EntityKeyName(entity)) //取请求参数中的记录编号
	// Edition := urlutil.GetParamToEdition(ctx.Request, dbinfo.EntityHasEdition(entity), dbinfo.TableEdition) //取请求参数中的版本号

	bl, msg, da := urlutil.GetParams(ctx.Request, entity)
	if !bl {
		return msgentity.Err(da.(int), msg)
	}

	data := da.(map[string]interface{})
	if id == nil || (fmt.Sprintf("%v", id) == "") {
		id = data[dbinfo.EntityKeyName(entity)]

		if id == nil || (fmt.Sprintf("%v", id) == "") {
			return msgentity.Err(1001, "未能获取'", dbinfo.EntityKeyName(entity), "'参数")
		}
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".Edit"

	me := AopUtil{}.CallBeforeFunc(name, ctx)
	if !me.Success {
		return me
	}

	me = service.Edit(ctx, entity, data)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 保存(有ID则更新,否则新增)
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) Save(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	bl, msg, da := urlutil.GetParams(ctx.Request, entity)
	if !bl {
		return msgentity.Err(da.(int), msg)
	}

	data := da.(map[string]interface{})

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".Save"

	me := AopUtil{}.CallBeforeFunc(name, ctx)
	if !me.Success {
		return me
	}

	me = service.Save(ctx, entity, data)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 批量修改
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @param transactional 启用事务
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) Edits(ctx ginutil.Context, control Controller, transactional bool) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	bl, msg, data := urlutil.GetParams(ctx.Request, entity)
	if !bl {
		return msgentity.Err(data.(int), msg)
	}

	datas := data.([]map[string]interface{})

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".Edits"

	me := AopUtil{}.CallBeforeFunc(name, ctx)
	if !me.Success {
		return me
	}

	me = service.Edits(ctx, entity, datas, transactional)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 批量保存
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @param transactional 启用事务
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) Saves(ctx ginutil.Context, control Controller, transactional bool) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	bl, msg, data := urlutil.GetParams(ctx.Request, entity)
	if !bl {
		return msgentity.Err(data.(int), msg)
	}

	datas := data.([]map[string]interface{})

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".Saves"

	me := AopUtil{}.CallBeforeFunc(name, ctx)
	if !me.Success {
		return me
	}

	me = service.Saves(ctx, entity, datas, transactional)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 批量操作
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @param transactional 启用事务
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) SaveList(ctx ginutil.Context, control Controller, transactional bool) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	bl, msg, da := urlutil.GetParams(ctx.Request, entity)
	if !bl {
		return msgentity.Err(da.(int), msg)
	}

	data := da.(map[string]interface{})
	addList := []map[string]interface{}{}
	editList := []map[string]interface{}{}
	delList := []map[string]interface{}{}

	if temp, ok := data["addList"]; ok {
		addList = temp.([]map[string]interface{})
	}

	if temp, ok := data["editList"]; ok {
		editList = temp.([]map[string]interface{})
	}

	if temp, ok := data["delList"]; ok {
		delList = temp.([]map[string]interface{})
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".SaveList"

	me := AopUtil{}.CallBeforeFunc(name, ctx)
	if !me.Success {
		return me
	}

	me = service.SaveList(ctx, entity, addList, editList, delList, transactional)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 根据主键查询数据
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 */
func (controlUtil ControllerUtil) FindById(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	id := urlutil.GetParamToId(ctx.Request, dbinfo.EntityKeyName(entity)) //取请求参数中的记录编号

	if (id == nil) || (fmt.Sprintf("%v", id) == "") {
		id = urlutil.GetParamToId(ctx.Request, "id")
	}

	if id == nil || (fmt.Sprintf("%v", id) == "") {
		return msgentity.Err(1001, "记录编号为空")
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindById"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.FindById(ctx, entity, id)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 查询所有数据
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) FindAll(ctx ginutil.Context, control Controller, attachWhere ...set.Set) *msgentity.MsgEntity {
	whereInfo := []dbinfo.WhereInfo{}
	if len(attachWhere) > 0 {
		for i := 0; i < len(attachWhere); i++ {
			whereInfo = append(whereInfo, dbinfo.WhereInfo{Name: attachWhere[i].Key, Value: attachWhere[i].Value})
		}
	}

	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindAll"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.FindAll(ctx, entity, whereInfo)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 查询时间范围内数据
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) FindByDate(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	dateSt := urlutil.GetParam(ctx.Request, "dateSt", "").(string) //记录开始时间
	dateEd := urlutil.GetParam(ctx.Request, "dateEd", "").(string) //记录结束时间

	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindByDate"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.FindByDate(ctx, entity, dateSt, dateEd)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 查找指定行数据
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) FindByRow(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	id := urlutil.GetParamToId(ctx.Request, dbinfo.EntityKeyName(entity)) //取请求参数中的记录编号

	if id == nil || (fmt.Sprintf("%v", id) == "") {
		id = urlutil.GetParamToId(ctx.Request, "id")

		if id == nil || (fmt.Sprintf("%v", id) == "") {
			return msgentity.Err(1001, "未能获取'", dbinfo.EntityKeyName(entity), "'参数")
		}
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindByRow"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.FindByRow(ctx, entity, id)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 查询分页数据
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @param injectionCondition 注入条件
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) FindByPage(ctx ginutil.Context, control Controller, injectionCondition ...set.Set) *msgentity.MsgEntity {
	var findByPageParam dbinfo.FindByPageParam
	if err := ctx.ShouldBind(&findByPageParam); err != nil {
		return msgentity.Err(9101, "请求参数错误", err.Error())
	}

	if findByPageParam.Page.Size < 1 {
		findByPageParam.Page.Size = 10
	}

	if findByPageParam.Page.Current < 1 {
		findByPageParam.Page.Current = 1
	}

	if (injectionCondition != nil) || (len(injectionCondition) > 0) { //将注入条件覆盖到查询条件
		if findByPageParam.Condition == nil {
			findByPageParam.Condition = map[string]interface{}{}
		}

		for _, set := range injectionCondition {
			findByPageParam.Condition.(map[string]interface{})[set.Key] = set.Value
		}
	}

	LikeStr := strings.TrimSpace(findByPageParam.LikeStr) //全文检索条件
	if LikeStr != "" {                                    //存在全文检索条件则需要考虑时间范围的问题
		sLikeDateSt := strings.TrimSpace(findByPageParam.LikeDateSt) //模糊查询记录修改时间范围条件-开始
		sLikeDateEd := strings.TrimSpace(findByPageParam.LikeDateEd) //模糊查询记录修改时间范围条件-结束

		var dLikeDateEd time.Time
		if sLikeDateEd == "" { //如果结束时间为空,则当前时间就是结束时间
			dLikeDateEd = time.Now()
			sLikeDateEd = dLikeDateEd.Format("2006-01-02 15:04:05")
		} else {
			dLikeDateEd = timeutil.ToDate(sLikeDateEd)
		}

		if sLikeDateSt == "" { //如果开始时间为空,则用结束时间-时间限制
			iLikeTimeLimit := control.LikeTimeLimit()
			sLikeDateSt = timeutil.AddDay(dLikeDateEd, -iLikeTimeLimit).Format("2006-01-02 15:04:05")
		}

		findByPageParam.LikeDateSt = sLikeDateSt
		findByPageParam.LikeDateEd = sLikeDateEd
	}

	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindByPage"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.FindByPage(ctx, entity, findByPageParam)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 取所有参数,并转换成对应实体属性类型(map[string]interface{})
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) GetParams(ctx ginutil.Context, entity interface{}) *msgentity.MsgEntity {
	if reflect.TypeOf(entity).Kind() == reflect.Struct {
		br, _ := io.ReadAll(ctx.Request.Body)
		ctx.Request.Body.Close()
		ctx.Request.Body = io.NopCloser(bytes.NewBuffer(br))
		json.NewDecoder(bytes.NewBuffer(br)).Decode(entity)

		// fmt.Println(reflect.TypeOf(entity).String()) // map[string]interface {}

		return msgentity.Success(entity, "转换结束")
	}

	bl, msg, data := urlutil.GetParams(ctx.Request, entity)

	return msgentity.Create(bl, msg, data)
}

/**
 * 读取树形结构数据
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) FindByTree(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	groupColumn := urlutil.GetParam(ctx.Request, "groupColumn", "").(string)
	groupName := urlutil.GetParam(ctx.Request, "groupName", "").(string)
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindByTree"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.FindByTree(ctx, entity, groupColumn, groupName)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 根据字段名取指定记录编号的数据库表中对应字段的值
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) GetValueByFieldName(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	temp := urlutil.GetParam(ctx.Request, "fieldNames", "").(string)
	fieldNames := strings.Split(temp, ",")

	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	id := urlutil.GetParamToId(ctx.Request, dbinfo.EntityKeyName(entity)) //取请求参数中的记录编号
	if id == nil || (fmt.Sprintf("%v", id) == "") {
		id = urlutil.GetParamToId(ctx.Request, "id")
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".GetValueByFieldName"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.GetValueByFieldName(ctx, entity, id, fieldNames)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 根据字段名取指定记录编号的数据库表中对应字段的值
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) GetValueByField(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	id := urlutil.GetParamToId(ctx.Request, dbinfo.EntityKeyName(entity)) //取请求参数中的记录编号
	if id == nil || (fmt.Sprintf("%v", id) == "") {
		id = urlutil.GetParamToId(ctx.Request, "id")

		if id == nil || (fmt.Sprintf("%v", id) == "") {
			return msgentity.Err(9012, "未能获取'", dbinfo.EntityKeyName(entity), "'参数")
		}
	}

	fieldName := urlutil.GetParam(ctx.Request, "fieldName", "").(string)

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".GetValueByField"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.GetValueByField(ctx, entity, id, fieldName)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 根据关键值取对象集合
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) FindByKey(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	bl, msg, da := urlutil.GetParams(ctx.Request, entity)
	if !bl {
		return msgentity.Err(da.(int), msg)
	}

	params := da.(map[string]interface{})
	whereInfo := []dbinfo.WhereInfo{}
	for k, v := range params {
		info := dbinfo.WhereInfo{Name: k, Value: v}
		whereInfo = append(whereInfo, info)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindByKey"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.FindByKey(ctx, entity, whereInfo)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 根据关键值取对象集合中的第一个
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) FindOneByKey(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	temp := urlutil.GetParam(ctx.Request, "fields", "").(string)
	fields := strings.Split(temp, ",")

	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	bl, msg, da := urlutil.GetParams(ctx.Request, entity)
	if !bl {
		return msgentity.Err(da.(int), msg)
	}

	params := da.(map[string]interface{})
	whereInfo := []dbinfo.WhereInfo{}
	for k, v := range params {
		info := dbinfo.WhereInfo{Name: k, Value: v}
		whereInfo = append(whereInfo, info)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindOneByKey"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.FindOneByKey(ctx, entity, whereInfo, fields...)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 根据关键值取对象集合中的符合条件的第一条记录的指定字段
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) FindValueByKey(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	bl, msg, da := urlutil.GetParams(ctx.Request, entity)
	if !bl {
		return msgentity.Err(da.(int), msg)
	}

	params := da.(map[string]interface{})
	whereInfo := []dbinfo.WhereInfo{}
	for k, v := range params {
		info := dbinfo.WhereInfo{Name: k, Value: v}
		whereInfo = append(whereInfo, info)
	}

	fieldName := urlutil.GetParam(ctx.Request, "fieldName", "").(string)

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindValueByKey"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.FindValueByKey(ctx, entity, whereInfo, fieldName)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 根据关键值查数量
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) FindCountByKey(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	bl, msg, da := urlutil.GetParams(ctx.Request, entity)
	if !bl {
		return msgentity.Err(da.(int), msg)
	}

	params := da.(map[string]interface{})
	whereInfo := []dbinfo.WhereInfo{}
	for k, v := range params {
		info := dbinfo.WhereInfo{Name: k, Value: v}
		whereInfo = append(whereInfo, info)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindCountByKey"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.FindCountByKey(ctx, entity, whereInfo)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 根据字段名取分组数据
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @param fields 分组字段[字段名,字段别名]
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) Group(ctx ginutil.Context, control Controller, fields map[string]string) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".GroupByField"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.GroupByField(ctx, entity, "", fields)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 根据字段名取分组数据且取数量
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @param fields 分组字段[字段名,字段别名]
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) GroupAndCount(ctx ginutil.Context, control Controller, fields map[string]string) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".GroupByFieldAndCount"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.GroupByFieldAndCount(ctx, entity, "", fields)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 根据字段名取分组数据
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) GroupByField(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	bl, msg, da := urlutil.GetParams(ctx.Request, entity)
	if !bl {
		return msgentity.Err(da.(int), msg)
	}

	fieldsTemp := da.(map[string]interface{})
	fields := make(map[string]string)
	for k, v := range fieldsTemp {
		fields[k] = fmt.Sprintf("%v", v)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".GroupByField"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.GroupByField(ctx, entity, "", fields)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 取表中指定字段的最大值
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) MaxByField(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	bl, msg, da := urlutil.GetParams(ctx.Request, entity)
	if !bl {
		return msgentity.Err(da.(int), msg)
	}

	field := urlutil.GetParam(ctx.Request, "field", "").(string)
	if field == "" {
		return msgentity.Err(9012, "field参数为空")
	}

	params := da.(map[string]interface{})
	whereInfo := []dbinfo.WhereInfo{}
	for k, v := range params {
		info := dbinfo.WhereInfo{Name: k, Value: v}
		whereInfo = append(whereInfo, info)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".MaxByField"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.MaxByField(ctx, entity, "", field, whereInfo)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 取表中指定字段的最小值
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) MinByField(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	field := urlutil.GetParam(ctx.Request, "field", "").(string)
	if field == "" {
		return msgentity.Err(9012, "field参数为空")
	}

	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	bl, msg, da := urlutil.GetParams(ctx.Request, entity)
	if !bl {
		return msgentity.Err(da.(int), msg)
	}

	params := da.(map[string]interface{})
	whereInfo := []dbinfo.WhereInfo{}
	for k, v := range params {
		info := dbinfo.WhereInfo{Name: k, Value: v}
		whereInfo = append(whereInfo, info)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".MinByField"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.MinByField(ctx, entity, "", field, whereInfo)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 取表中指定字段的最小值
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) HasById(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	// tableInfo := dbinfo. entity..TableInfo()
	id := urlutil.GetParamToId(ctx.Request, dbinfo.EntityKeyName(entity)) //取请求参数中的记录编号
	if id == nil || (fmt.Sprintf("%v", id) == "") {
		id = urlutil.GetParamToId(ctx.Request, "id")

		if id == nil || (fmt.Sprintf("%v", id) == "") {
			return msgentity.Err(1001, "未能获取'", dbinfo.EntityKeyName(entity), "'参数")
		}
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".HasById"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.HasById(ctx, entity, id)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 检查关键值记录是否存在(返回1:存在;0:不存在)
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) HasByKey(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	keyName := urlutil.GetParam(ctx.Request, "keyName", "")   //取请求参数中的字段名
	keyValue := urlutil.GetParam(ctx.Request, "keyValue", "") //取请求参数中的字段值

	if keyName == "" {
		return msgentity.Err(9001, "keyName参数为空")
	}

	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".HasByKey"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.HasByKey(ctx, entity, keyName.(string), keyValue)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 清理指定用户的缓存
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) ClearCache(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	user := urlutil.GetParam(ctx.Request, "user", "").(string)           //取请求参数中的用户名
	cacheName := urlutil.GetParam(ctx.Request, "cacheName", "").(string) //取请求参数中的缓存名

	if cacheName == "" {
		return msgentity.Err(8001, "指定'缓存库名称'参数为空！")
	}

	if user == "" {
		cacheName = cacheName + user
	}

	Log.Error("清理缓存库:" + cacheName)
	// if globalvariable.Del(cacheName) {//这种方式不合适
	// 	return msgentity.Success(8999, "清理成功！")
	// }

	return msgentity.Err(8002, "清理失败！")
}

/**
 * 查询组结构数据
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) FindByGroup(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	groupColumn := urlutil.GetParam(ctx.Request, "groupColumn", "").(string) //分组名(树节点)所在字段名
	groupName := urlutil.GetParam(ctx.Request, "groupName", "").(string)     //分组名(树节点)

	if groupName == "" {
		return controlUtil.FindByTree(ctx, control)
	}

	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindByGroup"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.FindByGroup(ctx, entity, groupColumn, groupName)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 添加数据到指定组下
 * 警告:对象必须符合树形结构要求,如:Id、Pid
 * @param ctx GinHttp上下文对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) AddToGroup(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	groupName := urlutil.GetParam(ctx.Request, "groupName", "").(string)   //分组字段名称(树节点)
	groupValue := urlutil.GetParam(ctx.Request, "groupValue", "").(string) //分组字段值(树节点)

	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9001, "控制层未设置ModuleEntit:", err)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".AddToGroup"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.AddToGroup(ctx, entity, groupName, groupValue)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 根据关键值翻转值(限布尔值类型,1转2,2转1)
 *	警告:此方法只支持布尔值类型,且只支持翻转1和2
 * @Param ctx http请求对象
 * @param control 控制层接口
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) ReversalByKey(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	reversalColumn := urlutil.GetParam(ctx.Request, "sReversalColumn", "").(string) //翻转的字段名
	entity, err := control.GetModuleEntity(control)                                 //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9001, "控制层未设置ModuleEntit:", err)
	}

	bl, msg, da := urlutil.GetParams(ctx.Request, entity)
	if !bl {
		return msgentity.Err(da.(int), msg)
	}

	params := da.(map[string]interface{})
	whereInfo := []dbinfo.WhereInfo{}
	for k, v := range params {
		info := dbinfo.WhereInfo{Name: k, Value: v}
		whereInfo = append(whereInfo, info)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".ReversalByKey"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Success {
		return meBefore
	}

	me := service.ReversalByKey(ctx, entity, whereInfo, reversalColumn)
	if !me.Success {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Success {
		return meAfter
	}

	return me
}

/**
 * 增加请求属性
 * 注:函数应用于模块控制器时修改或传递到通用方法时,切勿滥用
 * @Param ctx http请求对象
 * @Param attribs 参数键值对数组,[name,value],如果value不存在则为''
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) AddRequestAttrib(ctx ginutil.Context, attribs ...[]string) {
	if (attribs == nil) || (len(attribs) < 1) {
		return
	}

	ctx.Request.ParseForm() //警告:必须先 解析所有请求数据

	form := ctx.Request.Form
	if form == nil {
		form = make(uu.Values)
		ctx.Request.Form = form
	}

	for _, attrib := range attribs {
		if (attrib == nil) || (len(attrib) < 1) {
			continue
		}

		name := attrib[0]
		value := ""
		if (len(attrib) > 1) && (attrib[1] != "") {
			value = attrib[1] //fmt.Sprintf("%v", attrib[1])
		}

		form.Set(name, value)
	}
}

/**
 * 上传文件
 * @param ctx http请求对象
 * @param control 控制层接口
 * @param modelName 模块名称
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) UpFile(ctx ginutil.Context, control Controller, modelName string) *msgentity.MsgEntity {
	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	return service.UpFile(ctx, modelName)
}

/**
 * 获取图片
 * @param ctx http请求对象
 * @param control 控制层接口
 * @param modelName 模块名称
 * @param filename 文件名
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (controlUtil ControllerUtil) LookImg(ctx ginutil.Context, control Controller, modelName, filename string) {
	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	service.LookImg(ctx, modelName, filename)
}

//-----------------------------------------------//

/**
 * 取对应模块名称
 * object 对象
 */
func getSimplName(object interface{}) string {
	name := reflect.TypeOf(object).String()

	iSt := strings.Index(name, ".")
	if iSt > -1 {
		name = name[iSt+1:]
	}

	return name
}
