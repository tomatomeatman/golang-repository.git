package app

import (
	"database/sql"
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitee.com/tomatomeatman/golang-repository/bricks2/function/data/integerutil"
	"gitee.com/tomatomeatman/golang-repository/bricks2/function/data/stringutil"
	"gitee.com/tomatomeatman/golang-repository/bricks2/function/data/timeutil"
	"gitee.com/tomatomeatman/golang-repository/bricks2/function/reflectutil"
	"gitee.com/tomatomeatman/golang-repository/bricks2/model/dbinfo"
	"gitee.com/tomatomeatman/golang-repository/bricks2/model/msgentity"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/gorm"
	Log "github.com/cihub/seelog"
)

// we数据层接口定义,用于规范控制层结构体
type Dao interface {

	/**
	 * 新增
	 * @param entity 待保存实体
	 * @return msgentity.MsgEntity 返回执行情况
	 */
	Add(entity dbinfo.Entity) *msgentity.MsgEntity

	/**
	 * 通用新增数据方法
	 * @param entity 待保存实体
	 * @return msgentity.MsgEntity 返回执行情况
	 */
	AddCommon(entity dbinfo.Entity) *msgentity.MsgEntity

	/**
	 * 新增树节点
	 * @param entity 检查用数据结构
	 * @return msgentity.MsgEntity 返回执行情况
	 */
	AddNode(entity dbinfo.Entity) *msgentity.MsgEntity

	/**
	 * 批量新增
	 * @param entity 检查用数据结构
	 * @return msgentity.MsgEntity 返回执行情况
	 */
	Adds(entitys []dbinfo.Entity) *msgentity.MsgEntity

	/**
	 * 修改状态
	 * @param entity 实体类
	 * @param id 编号
	 * @param state 状态值
	 * @param edition 记录版本号
	 * @param Memo 备注
	 * @param unidirectional 是否单向 设置为单向则新值必须大于旧值才能执行
	 * @return msgentity.MsgEntity 返回执行情况
	 */
	ChangeState(entity dbinfo.Entity, id interface{}, state int, edition int, Memo string, unidirectional bool) *msgentity.MsgEntity

	/**
	 * 修改步骤值(如果设置为单向则新值必须大于旧值)
	 * @param id 编号
	 * @param setp 步骤值
	 * @param edition 记录版本号
	 * @param Memo 备注
	 * @param unidirectional 是否单向 设置为单向则新值必须大于旧值才能执行
	 * @param entity 实体类
	 * @return msgentity.MsgEntity 返回执行情况
	 */
	ChangeSetp(entity dbinfo.Entity, id interface{}, setp int, edition int, Memo string, unidirectional bool) *msgentity.MsgEntity

	/**
	 * 逻辑删除(非物理删除)
	 * @param entity 实体类
	 * @param id 记录编号值
	 * @param edition 记录版本号
	 * @param currentUser 当前用户
	 * @param onlyCreator 仅创建用户
	 * @return msgentity.MsgEntity
	 */
	DelSign(entity dbinfo.Entity, id interface{}, edition int, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 逻辑删除(非物理删除)
	 * @param entity 实体类
	 * @param whereInfo 条件
	 * @param currentUser 当前用户
	 * @param onlyCreator 仅创建用户
	 * @return msgentity.MsgEntity
	 */
	DelSignByMap(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 删除
	 * @param entity 对象类型
	 * @param id 记录编号值
	 * @param edition 记录版本号
	 * @param currentUser 当前用户
	 * @param onlyCreator 仅创建用户
	 * @return msgentity.MsgEntity
	 */
	Del(entity dbinfo.Entity, id interface{}, edition int, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 删除
	 * @param entity 对象类型
	 * @param whereInfo 条件
	 * @param currentUser 当前用户
	 * @param onlyCreator 仅创建用户
	 * @return msgentity.MsgEntity
	 */
	DelByMap(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 修改
	 * @param entity 对象类型
	 * @param id 记录编号值
	 * @param edition 记录版本号
	 * @param data 待更新的字段和值
	 * @param currentUser 当前用户
	 * @param onlyCreator 仅创建用户
	 * @return msgentity.MsgEntity
	 */
	Edit(entity dbinfo.Entity, id interface{}, edition int, data map[string]interface{}, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 批量修改(事务)
	 * @param entity 对象类型
	 * @param datas 待更新的字段和值集合
	 * @param currentUser 当前用户
	 * @param onlyCreator 仅创建用户
	 * @return msgentity.MsgEntity
	 */
	Edits(entity dbinfo.Entity, datas []map[string]interface{}, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 批量保存(事务)
	 * @param entity 对象类型
	 * @param addList 待新增集合
	 * @param editList 待更新集合
	 * @param delList 待删除集合
	 * @param currentUser 当前用户
	 * @param onlyCreator 仅创建用户
	 * @param transactional 是否启用事务
	 * @return msgentity.MsgEntity
	 */
	SaveList(entity dbinfo.Entity, addList, editList, delList []map[string]interface{}, currentUser string, onlyCreator, transactional bool) *msgentity.MsgEntity

	/**
	 * 根据主键查询数据
	 * @param entity 检查用数据结构
	 * @param id 主键
	 * @param currentUser 当前用户
	 * @param onlyCreator 仅创建用户
	 * @return msgentity.MsgEntity
	 */
	FindById(entity dbinfo.Entity, id interface{}, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 根据主键查询数据(返回结构体数据)
	 * @param entity 检查用数据结构
	 * @param id 主键
	 * @param currentUser 当前用户
	 * @param onlyCreator 仅创建用户
	 * @return msgentity.MsgEntity
	 */
	EntityById(entity dbinfo.Entity, id interface{}, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 根据sql查询数据(返回结构体数据)
	 * @param entityList 结构体集合
	 * @param sql sql语句
	 * @param where 查询条件
	 * @return msgentity.MsgEntity
	 */
	EntityBySql(entityList interface{}, sql string, where map[string]interface{}) *msgentity.MsgEntity

	/**
	 * 查询所有数据
	 * @param entity 检查用数据结构
	 * @param where 查询条件
	 * @param currentUser 当前用户
	 * @param onlyCreator 仅创建用户
	 * @return msgentity.MsgEntity
	 */
	FindAll(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 查询指定时间内数据
	 * @param entity 检查用数据结构
	 * @param dateSt 开始时间
	 * @param dateEd 结束时间
	 * @param currentUser 当前用户
	 * @param onlyCreator 仅创建用户
	 * @return msgentity.MsgEntity
	 */
	FindByDate(entity dbinfo.Entity, dateSt string, dateEd string, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 查询指定行数据
	 * @param entity 检查用数据结构
	 * @param id 记录编号
	 * @param currentUser 当前用户
	 * @param onlyCreator 仅创建用户
	 * @return msgentity.MsgEntity
	 */
	FindByRow(entity dbinfo.Entity, id interface{}, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 查询分页数据
	 * @param entity 检查用数据结构
	 * @param findByPageParam 分页参数
	 * @param currentUser 当前用户
	 * @param onlyCreator 仅创建用户
	 * @return msgentity.MsgEntity
	 */
	FindByPage(entity dbinfo.Entity, findByPageParam dbinfo.FindByPageParam, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 取路径字段
	 * @param Id 主键
	 * @param dbName 数据库名
	 * @param tableName 表名
	 * @return string
	 */
	GetPath(Id, dbName, tableName string) string

	/**
	 * 取指定节点下的子节点编号
	 * @param tableInfo 表信息
	 * @param Pid 父节点
	 * @return
	 */
	NewChildId(tableInfo dbinfo.Entity, Pid string) *msgentity.MsgEntity

	/**
	 * 验证新增数据是否存在重复
	 * @param entity 数据实体
	 * @param currentUser 当前用户
	 * @return msgentity.MsgEntity
	 */
	ValidEntityRepeatByAdd(entity dbinfo.Entity, currentUser string) *msgentity.MsgEntity

	/**
	 * 验证更新数据是否存在重复
	 * @param entity 数据实体
	 * @param id 主键
	 * @param data 数据
	 * @param currentUser 当前用户
	 * @return msgentity.MsgEntity
	 */
	ValidEntityRepeatByEdit(entity dbinfo.Entity, id interface{}, data map[string]interface{}, currentUser string) *msgentity.MsgEntity

	/**
	 * 通用树型结构表添加数据时重复检查方法
	 * @param entity 数据实体
	 * @param currentUser 当前用户
	 * @return msgentity.MsgEntity
	 */
	CommonCheckRepeatByAddAndTree(entity dbinfo.Entity, currentUser string) *msgentity.MsgEntity

	/**
	 * 通用树型结构表添加数据时重复检查方法
	 * @param entity 数据实体
	 * @param id 主键
	 * @param Name 展现名
	 * @param currentUser 当前用户
	 * @return msgentity.MsgEntity
	 */
	CommonCheckRepeatByEditAndTree(entity dbinfo.Entity, id interface{}, Name interface{}, currentUser string) *msgentity.MsgEntity

	/**
	 * 通用添加数据时重复检查方法
	 * @param entity 数据实体
	 * @param currentUser 当前用户
	 * @return msgentity.MsgEntity
	 */
	CommonCheckRepeatByAdd(entity dbinfo.Entity, currentUser string) *msgentity.MsgEntity

	/**
	 * 通用更新数据时重复检查方法
	 * @param entity 数据实体
	 * @param id 数据主键
	 * @param currentUser 当前用户
	 * @return msgentity.MsgEntity
	 */
	CommonCheckRepeatByEdit(entity dbinfo.Entity, id interface{}, currentUser string) *msgentity.MsgEntity

	/**
	 * 读取树形结构数据
	 * @param entity 数据实体
	 * @param groupColumn 分组字段
	 * @param groupName 分组名称
	 * @param currentUser 当前用户
	 * @return msgentity.MsgEntity
	 */
	FindByTree(entity dbinfo.Entity, groupmn, groupName string, currentUser string) *msgentity.MsgEntity

	/**
	 * 根据字段名取指定记录编号的数据库表中对应字段的值
	 * @param entity 实体类
	 * @param id 记录编号
	 * @param fieldNames 待取数据的字段名称集合
	 * @param currentUser 当前用户
	 * @Param onlyCreator 是否只取当前用户创建的数据
	 * @return msgentity.MsgEntity 返回内容data中存放的是Map
	 */
	GetValueByFieldName(entity dbinfo.Entity, id interface{}, fieldNames []string, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 根据字段名取指定记录编号的数据库表中对应字段的值
	 * @param entity 实体类
	 * @param id 记录编号
	 * @param fieldName 待取数据的字段名称集合
	 * @param currentUser 当前用户
	 * @Param onlyCreator 是否只取当前用户创建的数据
	 * @return msgentity.MsgEntity 返回内容data中存放的是Map
	 */
	GetValueByField(entity dbinfo.Entity, id interface{}, fieldName, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 根据字段名取指定条件的数据库表中对应字段的值
	 * @param entity 实体类
	 * @param whereInfo 条件
	 * @param fieldName 待取数据的字段名称集合
	 * @param currentUser 当前用户
	 * @Param onlyCreator 是否只取当前用户创建的数据
	 * @return msgentity.MsgEntity 返回内容data中存放的是Map
	 */
	GetValueByWhere(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, fieldName, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 取记录对应的版本号
	 * @param entity 实体类
	 * @param idName 编号
	 * @return
	 */
	GetEdition(entity dbinfo.Entity, id interface{}) *msgentity.MsgEntity

	/**
	 * 取记录对应的状态值
	 * @param entity 实体类
	 * @param id 编号
	 * @return
	 */
	GetState(entity dbinfo.Entity, id interface{}) *msgentity.MsgEntity

	/**
	 * 根据关键值取对象集合
	 * @Param entity 实体类
	 * @Param where 存放查询条件
	 * @Param currentUser 当前用户
	 * @Param onlyCreator 是否只取当前用户创建的数据
	 * @return msgentity.MsgEntity
	 */
	FindByKey(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 根据关键值取对象集合中的第一个
	 * @Param entity 实体类
	 * @Param where 存放查询条件
	 * @Param currentUser 当前用户
	 * @param onlyCreator 是否只取当前用户创建的数据
	 * @param fields 指定要查询的字段集合
	 * @return msgentity.MsgEntity
	 */
	FindOneByKey(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, currentUser string, onlyCreator bool, fields ...string) *msgentity.MsgEntity

	/**
	 * 根据关键值取对象集合中的符合条件的第一条记录的指定字段
	 * @Param entity 实体类
	 * @Param where 存放查询条件
	 * @param fieldName 指定要查询的字段
	 * @param currentUser 当前用户
	 * @param onlyCreator 仅查询创建者
	 * @return msgentity.MsgEntity
	 */
	FindValueByKey(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, fieldName string, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 根据关键值取对象集合中的符合条件的第一条记录的指定字段
	 * @Param entity 实体类
	 * @Param where 存放查询条件
	 * @param fieldMap 指定要查询的字段集合(原字段, 别名)
	 * @param currentUser 当前用户
	 * @param onlyCreator 仅查询创建者
	 * @return msgentity.MsgEntity
	 */
	FindByFields(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, fieldMap map[string]string, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 根据关键值查数量
	 * @Param entity 实体类
	 * @Param where 存放查询条件
	 * @param currentUser 当前用户
	 * @param onlyCreator 仅查询创建者
	 * @return msgentity.MsgEntity
	 */
	FindCountByKey(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 以事务方式执行多个sql
	 * 注意:Mapper必须存在才能执行
	 * @param sqls [sql, params]
	 * @return msgentity.MsgEntity 一个执行失败则回滚
	 */
	TransactionSql(sqls map[string]map[string]interface{}) *msgentity.MsgEntity

	/**
	 * 根据字段名取分组数据
	 * @param entity 实体类
	 * @param Creator 指定用户
	 * @param fields 字段名与别名对象
	 * @param currentUser 当前用户
	 * @param onlyCreator 用户查询限制(仅创建者可见)
	 * @return
	 */
	GroupByField(entity dbinfo.Entity, Creator string, fields map[string]string, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 根据字段名取分组数据,并返回数量
	 * @param entity 实体类
	 * @param Creator 指定用户
	 * @param fields 字段名与别名对象
	 * @param currentUser 当前用户
	 * @param onlyCreator 用户查询限制(仅创建者可见)
	 * @return
	 */
	GroupByFieldAndCount(entity dbinfo.Entity, Creator string, fields map[string]string, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 根据字段名取分组数据
	 * @param entity 实体类
	 * @param Creator 指定用户
	 * @param fields 字段名与别名对象
	 * @param currentUser 当前用户
	 * @param onlyCreator 用户查询限制(仅创建者可见)
	 * @param isGroupCount 添加分组后是否添加'数量列'
	 * @return
	 */
	GroupByFieldBase(entity dbinfo.Entity, Creator string, fields map[string]string, currentUser string, onlyCreator, isGroupCount bool) *msgentity.MsgEntity

	/**
	 * 取表中指定字段的最大值
	 * @param entity 实体类
	 * @param Creator 指定用户
	 * @param field 字段名
	 * @param where 查询条件
	 * @param currentUser 当前用户
	 * @param onlyCreator 用户查询限制(仅创建者可见)
	 * @return msgentity.MsgEntity
	 */
	MaxByField(entity dbinfo.Entity, Creator string, field string, whereInfo []dbinfo.WhereInfo, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 取表中指定字段的最小值
	 * @param entity 实体类
	 * @param Creator 指定用户
	 * @param field 字段名
	 * @param where 查询条件
	 * @param currentUser 当前用户
	 * @param onlyCreator 用户查询限制(仅创建者可见)
	 * @param whereStr 查询条件字符串
	 * @return
	 */
	MinByField(entity dbinfo.Entity, Creator string, field string, whereInfo []dbinfo.WhereInfo, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 检查关键值记录是否存在(返回1:存在;0:不存在)
	 * @param entity 实体类
	 * @param id 记录编号
	 * @return msgentity.MsgEntity
	 */
	HasById(entity dbinfo.Entity, id interface{}) *msgentity.MsgEntity

	/**
	 * 检查关键值记录是否存在(返回1:存在;0:不存在)
	 * @param entity 实体类
	 * @Param keyName 字段名
	 * @Param keyValue 字段值
	 * @return msgentity.MsgEntity
	 */
	HasByKey(entity dbinfo.Entity, keyName string, keyValue interface{}) *msgentity.MsgEntity

	/**
	 * 执行SQL脚本获取单行单列数据
	 * 注意:库名必须用${}进行包装,此脚本应只存在一条记录且为单列
	 * @param sql 待执行的SQL脚本
	 * @param where 存放查询条件
	 * @return msgentity.MsgEntity
	 */
	DoSql(sql string, where ...interface{}) *msgentity.MsgEntity

	/**
	 * 执行SQL脚本获取单行单列数据
	 * 注意:库名必须用${}进行包装,此脚本应只存在一条记录且为单列
	 * @param sql 待执行的SQL脚本
	 * @param where 存放查询条件
	 * @return msgentity.MsgEntity
	 */
	ExecSql(sql string, where ...interface{}) *msgentity.MsgEntity

	/**
	 * 执行SQL脚本获取单行单列数据
	 * 注意:库名必须用${}进行包装,此脚本应只存在一条记录且为单列
	 * @param sql 待执行的SQL脚本
	 * @param where 存放查询条件
	 * @return msgentity.MsgEntity
	 */
	GetValue(sql string, where ...interface{}) *msgentity.MsgEntity

	/**
	 * 执行SQL脚本获取一行数据(多列)
	 * 注意:库名必须用${}进行包装,此脚本应只存在一条记录
	 * @param sql 待执行的SQL脚本
	 * @param where 存放查询条件
	 * @return msgentity.MsgEntity
	 */
	GetRow(sql string, where ...interface{}) *msgentity.MsgEntity

	/**
	 * 执行SQL脚本获取多行数据(多列)
	 * 注意:库名必须用${}进行包装,此脚本可返回多条记录
	 * @param sql SQL脚本
	 * @param where 存放查询条件
	 * @return msgentity.MsgEntity
	 */
	GetRows(sql string, where ...interface{}) *msgentity.MsgEntity

	/**
	 * 根据关键值翻转值(限布尔值类型,1转2,2转1)
	 * @Param entity 实体类
	 * @Param whereInfo 存放查询条件
	 * @Param reversalColumn 翻转的字段名
	 * @Param currentUser 当前用户
	 * @Param onlyCreator 是否只翻转创建人创建的数据
	 * @return msgentity.MsgEntity
	 */
	ReversalByKey(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, reversalColumn, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	* 根据条件仅查询指定字段名数据
	* @param entity 实体类
	* @param whereInfo
	* @param fieldNames 待取数据的字段名称集合
	* @Param currentUser 当前用户
	* @Param onlyCreator 是否只翻转创建人创建的数据
	* @return msgentity.MsgEntity 返回内容data中存放的是Map
	 */
	FindField(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, fieldNames []string, currentUser string, onlyCreator bool) *msgentity.MsgEntity

	/**
	 * 设置父对象
	 * @param owner 父对象
	 */
	SetOwner(owner Dao)

	/**
	 * 对gorm.First操作进行封装
	 * @param entity 实体类
	 * @param conds
	 * @return
	 */
	First(entity dbinfo.Entity, conds ...interface{}) (dbinfo.Entity, error)

	// 对gorm.Create操作进行封装
	Create(entity dbinfo.Entity) (dbinfo.Entity, error)

	// 对gorm.Save操作进行封装,
	// 警告:Save是一个组合函数。 如果保存值不包含主键，它将执行 Create，否则它将执行 Update (包含所有字段)。
	Save(entity dbinfo.Entity) (dbinfo.Entity, error)

	// 对gorm.Delete操作进行封装
	Delete(entity dbinfo.Entity, conds ...interface{}) (int, error)
}

var CommonDao = &DaoBaseFunc{}
var containsMapLike = map[string]string{}
var containsMapInitSync sync.Mutex

type DaoBaseFunc struct {
	owner Dao //指定上级
}

/**
 * 设置父对象
 * @param owner
 */
func (dao *DaoBaseFunc) SetOwner(owner Dao) {
	dao.owner = owner
}

/**
 * 新增
 * @param entity 待保存实体
 */
func (dao DaoBaseFunc) Add(entity dbinfo.Entity) *msgentity.MsgEntity {
	if dbinfo.EntityHasPid(entity) {
		return dao.AddNode(entity)
	}

	return dao.AddCommon(entity)
}

/**
 * 通用新增数据方法
 * @param entity 待保存实体
 */
func (dao DaoBaseFunc) AddCommon(entity dbinfo.Entity) *msgentity.MsgEntity {
	dbResult := gorm.GetDB().Table(entity.TableName()).Create(entity)
	if dbResult.Error != nil {
		Log.Error("新增发生异常:", dbResult.Error)
		return msgentity.Err(8041, "新增发生异常:", dbResult.Error)
	}

	if dbResult.RowsAffected < 1 {
		return msgentity.Err(8042, "新增失败,影响数为0")
	}

	return msgentity.Success(entity, "新增成功")
}

/**
 * 新增树节点
 * @param entity 检查用数据结构
 */
func (dao DaoBaseFunc) AddNode(entity dbinfo.Entity) *msgentity.MsgEntity {
	// var dataEntity interface{}
	// typeOf := reflect.TypeOf(entity)  //通过反射获取type定义
	// if typeOf.Kind() == reflect.Ptr { //是否指针类型
	// 	dataEntity = entity
	// } else if "reflect.Value" == typeOf.String() {
	// 	dataEntity = entity.(reflect.Value).Interface()
	// } else {
	// 	dataEntity = reflect.ValueOf(entity)
	// }

	var build strings.Builder
	build.WriteString("INSERT INTO ")
	build.WriteString(entity.TableName())
	build.WriteString(" (@@@@@") //要替换'@@@@,'

	//未完
	// columns := dbinfo.TableInfo{}.GetColumnNames(entity)
	// for _, v := range columns {
	// 	if "/Path/"
	// 	build.WriteString(v)

	// 	"CONCAT(IFNULL((select a.Path from ${BaseSystem}Department a where a.Id=IFNULL(#{Pid}, '00') ), '/00/'), #{Id}, '/') as Path,"
	// }

	build.WriteString(" )")

	// params = append(params, sql.Named("state", state))

	// Memo = strings.TrimSpace(Memo)
	// if Memo != "" {
	// 	build.WriteString(" ,Memo=@Memo")
	// 	params = append(params, sql.Named("Memo", Memo))
	// }

	// if 0 != edition {
	// 	build.WriteString(" ,edition=edition+1")
	// }

	dbResult := gorm.GetDB().Table(entity.TableName()).Create(entity)
	if dbResult.Error != nil {
		Log.Error("新增发生异常:", dbResult.Error)
		return msgentity.Err(8041, "新增发生异常:", dbResult.Error)
	}

	if dbResult.RowsAffected < 1 {
		return msgentity.Err(8042, "新增失败,影响数为0")
	}

	return msgentity.Success(entity, "新增成功")
}

// 批量新增
func (dao DaoBaseFunc) Adds(entitys []dbinfo.Entity) *msgentity.MsgEntity {
	if len(entitys) < 1 {
		return msgentity.Err(8041, "待批量新增的数据为空")
	}

	// dbResult := gorm.GetDB().Table(detail.OrdersDetailBase{}.TableName()).CreateInBatches(&entitys, 100)
	// if dbResult.Error != nil {
	// 	Log.Error("新增订单详情异常:", dbResult.Error)
	// 	return msgentity.Err(8002, "订单详情创建失败")
	// }

	//下面是事务方式批量添加
	var me *msgentity.MsgEntity
	gorm.GetDB().Transaction(func(tx gorm.GormDB) error {
		for _, entity := range entitys {
			// var dataEntity interface{}
			// typeOf := reflect.TypeOf(entity)  //通过反射获取type定义
			// if typeOf.Kind() == reflect.Ptr { //是否指针类型
			// 	dataEntity = entity
			// } else if "reflect.Value" == typeOf.String() {
			// 	dataEntity = entity.(reflect.Value).Interface()
			// } else {
			// 	dataEntity = reflect.ValueOf(entity)
			// }

			if err := tx.Table(entity.TableName()).Create(entity).Error; err != nil {
				Log.Error("批量新增发生异常:", err)
				me = msgentity.Err(8041, "批量新增发生异常:", err)
				return err
			}
		}

		me = msgentity.Success(entitys, "批量新增成功")
		return nil
	})

	return me
}

/**
 * 修改状态
 * @param entity 实体类
 * @param id 编号
 * @param state 状态值
 * @param edition 记录版本号
 * @param Memo 备注
 * @param unidirectional 是否单向 设置为单向则新值必须大于旧值才能执行
 * @return msgentity.MsgEntity 返回执行情况
 */
func (dao DaoBaseFunc) ChangeState(entity dbinfo.Entity, id interface{}, state, edition int, Memo string, unidirectional bool) *msgentity.MsgEntity {
	if fmt.Sprintf("%v", id) == "" {
		return msgentity.Err(7001, "记录编号参数为空！")
	}

	params := []interface{}{}
	params = append(params, sql.Named(dbinfo.EntityKeyName(entity)+"Where", id))

	var build strings.Builder
	build.WriteString("UPDATE ")
	build.WriteString(entity.TableName())
	build.WriteString(" SET ")
	build.WriteString(dbinfo.TableState[0])
	build.WriteString("=@")
	build.WriteString(dbinfo.TableState[1])
	params = append(params, sql.Named(dbinfo.TableState[1], state))

	Memo = strings.TrimSpace(Memo)
	if Memo != "" {
		build.WriteString(" ,")
		build.WriteString(dbinfo.TableMemo[0])
		build.WriteString("=@")
		build.WriteString(dbinfo.TableMemo[1])
		params = append(params, sql.Named(dbinfo.TableMemo[1], Memo))
	}

	if edition != 0 {
		build.WriteString(" ,")
		build.WriteString(dbinfo.TableEdition[0])
		build.WriteString("= 1 + ")
		build.WriteString(dbinfo.TableEdition[0])
	}

	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString(" =@")
	build.WriteString(dbinfo.EntityKeyName(entity))
	build.WriteString("Where")
	if edition != 0 {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableEdition[0])
		build.WriteString("=@editionWhere")
		params = append(params, sql.Named("editionWhere", edition))
	}

	if unidirectional {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableState[0])
		build.WriteString(" < @StateWhere")
		params = append(params, sql.Named("StateWhere", state))
	}

	dbResult := gorm.Exec(build.String(), params)
	if dbResult.Error != nil {
		Log.Error("更新状态值发生异常:", dbResult.Error)
		return msgentity.Err(7005, "更新状态值发生异常:", dbResult.Error)
	}

	if dbResult.RowsAffected > 0 {
		return msgentity.Success(7999, "更新状态值成功！")
	}

	build.Reset()
	build.WriteString("SELECT ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString(", ")
	build.WriteString(dbinfo.TableState[0])

	if edition != 0 {
		build.WriteString(", ")
		build.WriteString(dbinfo.TableEdition[0])
	}

	build.WriteString(" FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString(" =@")
	build.WriteString(dbinfo.EntityKeyName(entity))

	var StateOld, editionOld int
	row := gorm.Raw(build.String(), sql.Named(dbinfo.EntityKeyName(entity), id)).Row()
	row.Scan(&StateOld, &editionOld)

	if StateOld == 0 {
		return msgentity.Err(7001, "没有找到对应数据！")
	}

	if (edition != 0) && (edition != editionOld) {
		return msgentity.Err(7002, "更新状态值失败，系统中的数据可能已经被更新！")
	}

	if (state >= 0) && (state < StateOld) {
		return msgentity.Err(7003, "更新状态值失败，状态禁止逆操作！")
	}

	Log.Error("更新状态值未能得到预期影响值(预期1):", dbResult.RowsAffected)
	return msgentity.Err(7004, "更新状态值未能得到预期影响值(预期1)", dbResult.RowsAffected)
}

/**
 * 修改步骤值(如果设置为单向则新值必须大于旧值)
 * @param id 编号
 * @param setp 步骤值
 * @param edition 记录版本号
 * @param Memo 备注
 * @param unidirectional 是否单向 设置为单向则新值必须大于旧值才能执行
 * @param entity 实体类
 * @return msgentity.MsgEntity 返回执行情况
 */
func (dao DaoBaseFunc) ChangeSetp(entity dbinfo.Entity, id interface{}, setp, edition int, Memo string, unidirectional bool) *msgentity.MsgEntity {
	if fmt.Sprintf("%v", id) == "" {
		return msgentity.Err(7001, "记录编号参数为空！")
	}

	params := []interface{}{}
	params = append(params, sql.Named(dbinfo.EntityKeyName(entity)+"Where", id))

	var build strings.Builder
	build.WriteString("UPDATE ")
	build.WriteString(entity.TableName())
	build.WriteString(" SET ")
	build.WriteString(dbinfo.TableSetp[0])
	build.WriteString(" =@")
	build.WriteString(dbinfo.TableSetp[1])
	params = append(params, sql.Named(dbinfo.TableSetp[1], setp))

	Memo = strings.TrimSpace(Memo)
	if Memo != "" {
		build.WriteString(" ,")
		build.WriteString(dbinfo.TableMemo[0])
		build.WriteString("=@")
		build.WriteString(dbinfo.TableMemo[1])
		params = append(params, sql.Named(dbinfo.TableMemo[1], Memo))
	}

	if edition != 0 {
		build.WriteString(" ,")
		build.WriteString(dbinfo.TableEdition[0])
		build.WriteString(" = 1 + ")
		build.WriteString(dbinfo.TableEdition[0])
	}

	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString(" =@")
	build.WriteString(dbinfo.EntityKeyName(entity))
	if edition != 0 {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableEdition[0])
		build.WriteString(" =@editionWhere")
		params = append(params, sql.Named("editionWhere", edition))
	}

	if unidirectional {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableSetp[0])
		build.WriteString(" < @SetpWhere")
		params = append(params, sql.Named("SetpWhere", setp))
	}

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	dbResult := gorm.Exec(build.String(), params)
	if dbResult.Error != nil {
		Log.Error("更新步骤值发生异常:", dbResult.Error)
		return msgentity.Err(7005, "更新步骤值发生异常:", dbResult.Error)
	}

	if dbResult.RowsAffected > 0 {
		return msgentity.Success(7999, "更新步骤值成功！")
	}

	build.Reset()
	build.WriteString("SELECT ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString(", ")
	build.WriteString(dbinfo.TableSetp[0])

	if edition != 0 {
		build.WriteString(", ")
		build.WriteString(dbinfo.TableEdition[0])
	}

	build.WriteString(" FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString(" =@")
	build.WriteString(dbinfo.EntityKeyName(entity))

	var SetpOld, editionOld int
	row := gorm.Raw(build.String(), sql.Named(dbinfo.EntityKeyName(entity), id)).Row()
	row.Scan(&SetpOld, &editionOld)

	if SetpOld == 0 {
		return msgentity.Err(7001, "没有找到对应数据！")
	}

	if (edition != 0) && (edition != editionOld) {
		return msgentity.Err(7002, "更新步骤值失败，系统中的数据可能已经被更新！")
	}

	if (setp >= 0) && (setp < SetpOld) {
		return msgentity.Err(7003, "更新步骤值失败，状态禁止逆操作！")
	}

	Log.Error("更新步骤值未能得到预期影响值(预期1):", dbResult.RowsAffected)
	return msgentity.Err(7004, "更新步骤值未能得到预期影响值(预期1)", dbResult.RowsAffected)
}

/**
 * 根据ID逻辑删除(非物理删除)
 * @param entity 实体信息
 * @param id 记录编号
 * @param edition 记录版本号
 * @param currentUser 当前用户
 * @param onlyCreator 仅创建者能删除
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) DelSign(entity dbinfo.Entity, id interface{}, edition int, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	if id == nil {
		return msgentity.Err(7001, "记录编号为空")
	}

	if !dbinfo.EntityHasDelSign(entity) { //有逻辑删除字段标识才能进行逻辑删除
		return msgentity.Err(1007, "没有逻辑删除字段,试图逻辑删除失败")
	}

	if dbinfo.EntityHasEdition(entity) && (edition < 1) && (edition != integerutil.MaxInt()) {
		return msgentity.Err(7002, "记录版本号不正确")
	}

	where := map[string]interface{}{}
	where[dbinfo.EntityKeyName(entity)] = id

	var build strings.Builder
	build.WriteString("UPDATE ")
	build.WriteString(entity.TableName())
	build.WriteString(" SET ")
	build.WriteString(dbinfo.TableDelSign[0])
	build.WriteString(" = 1 WHERE ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString(" =@")
	build.WriteString(dbinfo.EntityKeyName(entity))

	if dbinfo.EntityHasEdition(entity) && (edition != integerutil.MaxInt()) {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableEdition[0])
		build.WriteString("=@")
		build.WriteString(dbinfo.TableEdition[1])
		where[dbinfo.TableEdition[1]] = edition
	}

	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
		where["Creator"] = currentUser
	}

	dbResult := gorm.Exec(build.String(), where)

	if dbResult.Error != nil {
		Log.Error("逻辑删除发生异常:", dbResult.Error)
		return msgentity.Err(1002, "逻辑删除发生异常:", dbResult.Error)
	}

	if dbResult.RowsAffected > 0 {
		return msgentity.Success(1999, "(逻辑)删除成功")
	}

	//--影响数为0,需要进一步判断--//
	build.Reset()
	build.WriteString("SELECT ")
	build.WriteString(dbinfo.TableDelSign[0])
	build.WriteString(" FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString(" = ? ")

	var iCount int
	gorm.Raw(build.String(), id).Scan(&iCount)
	if iCount == 0 {
		return msgentity.Success(1999, "没有发现此数据")
	}

	if !dbinfo.EntityHasEdition(entity) {
		return msgentity.Err(1003, "删除失败,数据还存在")
	}

	var oldEdition int
	var iDelSign int
	build.Reset()
	build.WriteString("SELECT ")
	build.WriteString(dbinfo.TableEdition[0])
	build.WriteString(", ")
	build.WriteString(dbinfo.TableDelSign[0])
	build.WriteString(" FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString(" = ?")
	gorm.Raw(build.String(), id).Row().Scan(&oldEdition, &iDelSign)

	if iDelSign == 1 {
		return msgentity.Err(1004, "数据已经被(逻辑)删除")
	}

	if oldEdition == edition {
		return msgentity.Err(1005, "删除失败,数据还存在")
	}

	return msgentity.Err(1006, "(逻辑)删除失败,提供的版本号不正确,可能数据已经被改变")
}

/**
 * 根据条件逻辑删除(非物理删除)
 * @param entity 实体信息
 * @param whereInfo 条件信息
 * @param currentUser 当前用户
 * @param onlyCreator 仅创建者能删除
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) DelSignByMap(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	if !dbinfo.EntityHasDelSign(entity) { //有逻辑删除字段标识才能进行逻辑删除
		return msgentity.Err(1007, "没有逻辑删除字段,试图逻辑删除失败")
	}

	var build strings.Builder
	build.WriteString("UPDATE ")
	build.WriteString(entity.TableName())
	build.WriteString(" SET ")
	build.WriteString(dbinfo.TableDelSign[0])
	build.WriteString(" = 1 WHERE 1=1")

	whereInfo = dao.holdByEntityToWhereInfo(entity, whereInfo) //按实体保留条件
	where := map[string]interface{}{}
	for _, value := range whereInfo {
		condition := value.Condition
		if condition == "" {
			condition = "="
		}

		build.WriteString(" AND ")
		build.WriteString(value.Name)
		build.WriteString(" ")
		build.WriteString(condition)

		if value.ValueName != "" {
			build.WriteString(" ")
			build.WriteString(value.ValueName)
			continue
		}

		build.WriteString(" @")
		build.WriteString(value.Name)
		where[value.Name] = value.Value
	}

	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
		where["Creator"] = currentUser
	}

	dbResult := gorm.Exec(build.String(), where)

	if dbResult.Error != nil {
		Log.Error("逻辑删除发生异常:", dbResult.Error)
		return msgentity.Err(1002, "逻辑删除发生异常:", dbResult.Error)
	}

	if dbResult.RowsAffected > 0 {
		return msgentity.Success(1999, "(逻辑)删除成功")
	}

	return msgentity.Err(1003, "(逻辑)删除失败")
}

/**
 * 根据ID删除记录
 * @param entity 实体信息
 * @param id 记录编号
 * @param edition 记录版本号
 * @param currentUser 当前用户
 * @param onlyCreator 仅创建者能删除
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) Del(entity dbinfo.Entity, id interface{}, edition int, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	if id == nil {
		return msgentity.Err(7001, "记录编号为空")
	}

	if dbinfo.EntityHasEdition(entity) && (edition < 1) && (edition != integerutil.MaxInt()) {
		return msgentity.Err(7002, "记录版本号不正确")
	}

	where := map[string]interface{}{}
	where[dbinfo.EntityKeyName(entity)] = id

	var build strings.Builder
	build.WriteString("DELETE FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString(" =@")
	build.WriteString(dbinfo.EntityKeyName(entity))

	if dbinfo.EntityHasEdition(entity) && (edition != integerutil.MaxInt()) {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableEdition[0])
		build.WriteString("=@")
		build.WriteString(dbinfo.TableEdition[0])
		where[dbinfo.TableEdition[1]] = edition
	}

	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
		where["Creator"] = currentUser
	}

	dbResult := gorm.Exec(build.String(), where)

	if dbResult.Error != nil {
		Log.Error("删除发生异常:", dbResult.Error)
		return msgentity.Err(1002, "删除发生异常:", dbResult.Error)
	}

	if dbResult.RowsAffected > 0 {
		return msgentity.Success(1999, "删除成功")
	}

	//--影响数为0,需要进一步判断--//
	build.Reset()
	build.WriteString("SELECT 1 FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString(" = ?")

	var iCount int
	gorm.Raw(build.String(), id).Scan(&iCount)
	if iCount == 0 {
		return msgentity.Success(1999, "没有发现此数据")
	}

	if !dbinfo.EntityHasEdition(entity) {
		return msgentity.Err(1003, "删除失败,数据还存在")
	}

	var oldEdition int
	build.Reset()
	build.WriteString("SELECT ")
	build.WriteString(dbinfo.TableEdition[0])
	build.WriteString(" FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString(" = ?")
	gorm.Raw(build.String(), id).Scan(&oldEdition)

	if oldEdition == edition {
		return msgentity.Err(1004, "删除失败,数据还存在")
	}

	return msgentity.Err(1005, "删除失败,提供的版本号不正确,可能数据已经被改变")
}

/**
 * 根据条件删除
 * @param entity
 * @param whereInfo
 * @param currentUser 当前用户
 * @param onlyCreator 只创建人能删除
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) DelByMap(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	if entity == nil {
		return msgentity.Err(7001, "结构体参数为nil")
	}

	var build strings.Builder
	build.WriteString("DELETE FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE 1=1")

	whereInfo = dao.holdByEntityToWhereInfo(entity, whereInfo) //按实体保留条件

	where := map[string]interface{}{}
	for _, value := range whereInfo {
		condition := value.Condition
		if condition == "" {
			condition = "="
		}

		build.WriteString(" AND ")
		build.WriteString(value.Name)
		build.WriteString(" ")
		build.WriteString(condition)

		if value.ValueName != "" {
			build.WriteString(" ")
			build.WriteString(value.ValueName)
			continue
		}

		build.WriteString(" @")
		build.WriteString(value.Name)
		where[value.Name] = value.Value
	}

	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
		where["Creator"] = currentUser
	}

	dbResult := gorm.Exec(build.String(), where)

	if dbResult.Error != nil {
		Log.Error("删除发生异常:", dbResult.Error)
		return msgentity.Err(1002, "删除发生异常:", dbResult.Error)
	}

	return msgentity.Success(1999, "删除成功")
}

/**
 * 修改
 * @param entity 对象类型
 * @param id 记录编号值
 * @param edition 记录版本号
 * @param data 待更新的字段和值
 * @param currentUser 当前用户
 * @param onlyCreator 仅创建用户
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) Edit(entity dbinfo.Entity, id interface{}, edition int, data map[string]interface{}, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	if fmt.Sprintf("%v", id) == "" {
		return msgentity.Err(7001, "记录编号参数为空！")
	}

	params := []interface{}{} //[]sql.NamedArg{}
	params = append(params, sql.Named(dbinfo.EntityKeyName(entity), id))

	var build strings.Builder
	build.WriteString("UPDATE ")
	build.WriteString(entity.TableName())
	build.WriteString(" SET @@")

	for key, value := range data {
		build.WriteString(", ")
		build.WriteString(key)

		build.WriteString("=@")
		build.WriteString(key)
		params = append(params, sql.Named(key, value))
	}

	if dbinfo.EntityHasEdition(entity) {
		build.WriteString(", ")
		build.WriteString(dbinfo.TableEdition[0])
		build.WriteString("= 1 + ")
		build.WriteString(dbinfo.TableEdition[0])
	}

	if dbinfo.EntityHasModifieder(entity) {
		build.WriteString(",")
		build.WriteString(dbinfo.TableModifieder[0])
		build.WriteString("=@")
		build.WriteString(dbinfo.TableModifieder[1])
		params = append(params, sql.Named(dbinfo.TableModifieder[1], currentUser))
	}

	if dbinfo.EntityHasModifiedDate(entity) {
		build.WriteString(", ")
		build.WriteString(dbinfo.TableModifiedDate[0])
		build.WriteString("=NOW()")
	}

	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString(" =@")
	build.WriteString(dbinfo.EntityKeyName(entity))

	if (edition != 0) && dbinfo.EntityHasEdition(entity) {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableEdition[0])
		build.WriteString("=@")
		build.WriteString(dbinfo.TableEdition[1])
		params = append(params, sql.Named(dbinfo.TableEdition[1], edition))
	}

	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@")
		build.WriteString(dbinfo.TableCreator[1])
		params = append(params, sql.Named(dbinfo.TableCreator[1], currentUser))
	}

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	txt := strings.Replace(build.String(), " @@, ", " ", -1)
	dbResult := gorm.Exec(txt, params)
	if dbResult.Error != nil {
		Log.Error("更新数据发生异常:", dbResult.Error)
		return msgentity.Err(7005, "更新数据发生异常:", dbResult.Error)
	}

	if 0 < dbResult.RowsAffected {
		return msgentity.Success(7999, "更新数据成功！")
	}

	build.Reset()
	build.WriteString("SELECT ")
	build.WriteString(dbinfo.EntityKeyField(entity))

	if dbinfo.EntityHasEdition(entity) {
		build.WriteString(", ")
		build.WriteString(dbinfo.TableEdition[0])
	}

	build.WriteString(" FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString(" =@")
	build.WriteString(dbinfo.EntityKeyName(entity))

	var vId, editionOld int
	row := gorm.Raw(build.String(), sql.Named(dbinfo.EntityKeyName(entity), id)).Row()
	row.Scan(&vId, &editionOld)

	if (fmt.Sprintf("%v", vId) == "") || (fmt.Sprintf("%v", vId) == "<nil>") {
		return msgentity.Err(7001, "没有找到对应数据！")
	}

	if dbinfo.EntityHasEdition(entity) && (edition != editionOld) {
		return msgentity.Err(7002, "更新数据失败，系统中的数据可能已经被更新！")
	}

	Log.Error("更新数据未能得到预期影响值(预期1):", dbResult.RowsAffected)
	return msgentity.Err(7004, "更新数据未能得到预期影响值(预期1)", dbResult.RowsAffected)
}

/**
 * 批量修改(事务)
 * @param entity 对象类型
 * @param datas 待更新的字段和值集合
 * @param currentUser 当前用户
 * @param onlyCreator 仅创建用户
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) Edits(entity dbinfo.Entity, datas []map[string]interface{}, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	result := 0

	tableInfo := dbinfo.GetTableInfo(entity)

	tx := gorm.GetDB().Begin() // 开始事务

	for _, data := range datas {
		id := data[tableInfo.KeyName]
		edition := -1

		temp, ok := data[dbinfo.TableEdition[1]]
		if ok {
			edition = temp.(int)
		}

		params := []interface{}{} //[]sql.NamedArg{}
		params = append(params, sql.Named(dbinfo.EntityKeyName(entity), id))

		var build strings.Builder
		build.WriteString("UPDATE ")
		build.WriteString(entity.TableName())
		build.WriteString(" SET @@")

		for key, value := range data {
			build.WriteString(", ")
			build.WriteString(key)

			build.WriteString("=@")
			build.WriteString(key)
			params = append(params, sql.Named(key, value))
		}

		if dbinfo.EntityHasEdition(entity) {
			build.WriteString(", ")
			build.WriteString(dbinfo.TableEdition[0])
			build.WriteString("= 1 + ")
			build.WriteString(dbinfo.TableEdition[0])
		}

		if dbinfo.EntityHasModifieder(entity) {
			build.WriteString(",")
			build.WriteString(dbinfo.TableModifieder[0])
			build.WriteString("=@")
			build.WriteString(dbinfo.TableModifieder[1])
			params = append(params, sql.Named(dbinfo.TableModifieder[1], currentUser))
		}

		if dbinfo.EntityHasModifiedDate(entity) {
			build.WriteString(", ")
			build.WriteString(dbinfo.TableModifiedDate[0])
			build.WriteString("=NOW()")
		}

		build.WriteString(" WHERE ")
		build.WriteString(dbinfo.EntityKeyField(entity))
		build.WriteString(" =@")
		build.WriteString(dbinfo.EntityKeyName(entity))

		if (edition != 0) && dbinfo.EntityHasEdition(entity) {
			build.WriteString(" AND ")
			build.WriteString(dbinfo.TableEdition[0])
			build.WriteString("=@")
			build.WriteString(dbinfo.TableEdition[1])
			params = append(params, sql.Named(dbinfo.TableEdition[1], edition))
		}

		if onlyCreator && (currentUser != "") {
			build.WriteString(" AND ")
			build.WriteString(dbinfo.TableCreator[0])
			build.WriteString("=@")
			build.WriteString(dbinfo.TableCreator[1])
			params = append(params, sql.Named(dbinfo.TableCreator[1], currentUser))
		}

		if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
			build.WriteString(" AND ")
			build.WriteString(dbinfo.TableDelSign[0])
			build.WriteString(" != 1")
		}

		txt := strings.Replace(build.String(), " @@, ", " ", -1)

		err := tx.Exec(txt, params).Error
		if err == nil {
			result++
			continue
		}

		Log.Error("更新语句> ", txt, ";发生异常:", err.Error())
		tx.Rollback() //回滚事务

		return msgentity.Err(7005, "更新数据发生异常:", err.Error)
	}

	tx.Commit() //提交事务

	return msgentity.Success(result, "更新数据成功！")
}

/**
 * 批量保存(事务)
 * @param entity 对象类型
 * @param addList 待新增集合
 * @param editList 待更新集合
 * @param delList 待删除集合
 * @param currentUser 当前用户
 * @param onlyCreator 仅创建用户
 * @param transactional 是否启用事务
 * @return msgentity.MsgEntity
 */
func (dao DaoBaseFunc) SaveList(entity dbinfo.Entity, addList, editList, delList []map[string]interface{}, currentUser string, onlyCreator, transactional bool) *msgentity.MsgEntity {
	result := []int{0, 0, 0}
	tableInfo := dbinfo.GetTableInfo(entity)

	var tx gorm.GormDB
	if transactional {
		tx = gorm.GetDB().Begin() // 开始事务
	}

	for _, data := range addList {
		if err := tx.Table(entity.TableName()).Create(data).Error; err != nil {
			if transactional {
				tx.Rollback() //回滚事务
				return msgentity.Err(8041, "批量新增发生异常:", err)
			}
		}

		result[0] = result[0] + 1
	}

	for _, data := range editList {
		id := data[tableInfo.KeyName]
		edition := -1

		temp, ok := data[dbinfo.TableEdition[1]]
		if ok {
			edition = temp.(int)
		}

		me := dao.Edit(entity, id, edition, data, currentUser, onlyCreator)
		if !me.Success && transactional {
			tx.Rollback() //回滚事务
			return msgentity.Err(8041, "批量更新失败")
		}

		result[1] = result[1] + 1
	}

	for _, data := range delList {
		id := data[tableInfo.KeyName]
		edition := -1

		temp, ok := data[dbinfo.TableEdition[1]]
		if ok {
			edition = temp.(int)
		}

		me := dao.Del(entity, id, edition, currentUser, onlyCreator)
		if !me.Success && transactional {
			tx.Rollback() //回滚事务
			return msgentity.Err(8041, "批量删除失败")
		}

		result[2] = result[2] + 1
	}

	if transactional {
		return msgentity.Success(result, "批量操作成功！")
	}

	if transactional {
		tx.Commit() //提交事务
	}

	return msgentity.Success(result, "批量操作结束")
}

/**
 * 根据主键查询数据
 * @param entity 检查用数据结构
 * @param id 主键
 * @param currentUser 当前用户
 * @param onlyCreator 仅创建用户
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) FindById(entity dbinfo.Entity, id interface{}, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	if id == nil {
		return msgentity.Err(7001, "记录编号为空")
	}

	where := map[string]interface{}{"id": id}

	var build strings.Builder
	build.WriteString("SELECT * FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString("=@id")

	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
		where["Creator"] = currentUser
	}

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	rows, err := gorm.Raw(build.String(), where).Rows()
	if err != nil {
		Log.Error("查询发生异常:", err)
		return msgentity.Err(7002, "查询发生异常:", err)
	}
	defer rows.Close()

	res := gorm.ScanRows2mapI(rows)
	if res == nil {
		Log.Error("查询后数据转换发生异常:", err)
		return msgentity.Err(7003, "查询后数据转换发生异常:", err)
	}

	if len(res) < 1 {
		return msgentity.Err(7005, "查询成功,但没有数据")
	}

	return msgentity.Success(res[0], "查询成功")
}

/**
 * 根据主键查询数据(返回结构体数据)
 * @param entity 检查用数据结构
 * @param id 主键
 * @param currentUser 当前用户
 * @param onlyCreator 仅创建用户
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) EntityById(entity dbinfo.Entity, id interface{}, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	if id == nil {
		return msgentity.Err(7001, "记录编号为空")
	}

	where := map[string]interface{}{"id": id}

	var build strings.Builder
	build.WriteString("SELECT * FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString("=@id")

	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
		where["Creator"] = currentUser
	}

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	dbResult := gorm.Raw(build.String(), where).Scan(entity)
	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return msgentity.Err(7002, "查询发生异常")
	}

	if dbResult.RowsAffected != 1 {
		return msgentity.Err(7003, "查询数据不符合")
	}

	return msgentity.Success(entity, "查询成功")
}

func (dao DaoBaseFunc) EntityBySql(entityList interface{}, sql string, where map[string]interface{}) *msgentity.MsgEntity {
	sql = strings.TrimSpace(sql)
	if sql == "" {
		return msgentity.Err(1001, "待执行语句为空")
	}

	if entityList == nil {
		return msgentity.Err(1002, "结构体为空")
	}

	result, err := gorm.Query(sql, entityList, where)
	if err != nil {
		Log.Error("查询发生异常:", err)
		return msgentity.Err(1003, "查询发生异常:", err)
	}

	return msgentity.Success(result, "查询成功")
}

/**
 * 查询所有数据
 * @param entity 检查用数据结构
 * @param where 查询条件
 * @param currentUser 当前用户
 * @param onlyCreator 仅创建用户
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) FindAll(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, currentUser string, onlyCreator bool) *msgentity.MsgEntity {

	var build strings.Builder
	build.WriteString("SELECT * FROM ")
	build.WriteString(entity.TableName())

	whereInfo = dao.holdByEntityToWhereInfo(entity, whereInfo) //按实体保留
	where := map[string]interface{}{}
	if len(whereInfo) > 0 {
		build.WriteString(" WHERE 1=1")
		for _, value := range whereInfo {
			condition := value.Condition
			if condition == "" {
				condition = "="
			}

			build.WriteString(" AND ")
			build.WriteString(value.Name)
			build.WriteString(" ")
			build.WriteString(condition)

			if value.ValueName != "" {
				build.WriteString(" ")
				build.WriteString(value.ValueName)
				continue
			}

			build.WriteString(" @")
			build.WriteString(value.Name)
			where[value.Name] = value.Value
		}
	}

	if onlyCreator && (currentUser != "") {
		where[dbinfo.TableCreator[1]] = currentUser
	}

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	hasOrderby := false

	if dbinfo.EntityHasRank(entity) {
		hasOrderby = true
		build.WriteString(" ORDER BY ")
		build.WriteString(dbinfo.TableRrank[0])
		build.WriteString(" ASC")
	}

	if dbinfo.EntityHasRank(entity) {
		if !hasOrderby {
			build.WriteString(" ORDER BY ")
		}

		build.WriteString(dbinfo.TableRrank[0])
		build.WriteString(" DESC")
	}

	text := strings.Replace(build.String(), "1=1 AND ", "", -1)

	rows, err := gorm.Raw(text, where).Rows()
	if err != nil {
		Log.Error("查询发生异常:", err)
		return msgentity.Err(1005, "查询发生异常:", err)
	}
	defer rows.Close()

	res := gorm.ScanRows2mapI(rows)
	if res == nil {
		return msgentity.Err(1006, "查询后数据转换发生异常")
	}

	return msgentity.Success(res, "查询成功")
}

/**
 * 查询指定时间内数据
 * @param entity 检查用数据结构
 * @param dateSt 开始时间
 * @param dateEd 结束时间
 * @param currentUser 当前用户
 * @param onlyCreator 仅创建用户
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) FindByDate(entity dbinfo.Entity, dateSt string, dateEd string, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	var dDateSt time.Time
	if dateSt == "" {
		d, _ := time.ParseDuration("-168h") //7天前
		dDateSt = time.Now().Add(d)
	} else {
		temp, err := time.ParseInLocation("2006-01-02 15:04:05", dateSt, time.Local)
		if err != nil {
			return msgentity.Err(7001, "'开始时间'格式不符合要求,推荐使用格式:'YYYY-MM-DD HH:mm:ss'")
		}

		dDateSt = temp
	}

	var dDateEd time.Time
	if dateEd == "" {
		dDateEd = time.Now()
	} else {
		temp, err := time.ParseInLocation("2006-01-02 15:04:05", dateSt, time.Local)
		if err != nil {
			return msgentity.Err(7002, "'结束时间'格式不符合要求,推荐使用格式:'YYYY-MM-DD HH:mm:ss'")
		}

		dDateEd = temp
	}

	var build strings.Builder
	build.WriteString("SELECT * FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.TableCreateDate[0])
	build.WriteString(" BETWEEN @dDateSt AND @dDateEd")

	where := map[string]interface{}{
		"dDateSt": dDateSt,
		"dDateEd": dDateEd,
	}

	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
		where["Creator"] = currentUser
	}

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	rows, err := gorm.Raw(build.String(), where).Rows()
	if err != nil {
		Log.Error("查询发生异常:", err)
		return msgentity.Err(7003, "查询发生异常:", err)
	}
	defer rows.Close()

	res := gorm.ScanRows2mapI(rows)
	if res == nil {
		return msgentity.Err(7004, "查询后数据转换发生异常")
	}

	return msgentity.Success(res, "查询成功")
}

/**
 * 查询指定行数据
 * @param entity 检查用数据结构
 * @param id 记录编号
 * @param currentUser 当前用户
 * @param onlyCreator 仅创建用户
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) FindByRow(entity dbinfo.Entity, id interface{}, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	if id == nil {
		return msgentity.Err(7001, "记录编号为空")
	}

	Id := fmt.Sprintf("%v", id)
	if Id == "" {
		return msgentity.Err(7002, "记录编号为空")
	}

	// var s reflect.Type
	// typeOf := reflect.TypeOf(entity)  //通过反射获取type定义
	// if typeOf.Kind() == reflect.Ptr { //是否指针类型
	// 	s = reflect.TypeOf(entity).Elem() //通过反射获取type定义
	// } else {
	// 	s = typeOf //通过反射获取type定义
	// }

	var appendFieldSql strings.Builder
	var dictionarySql strings.Builder
	var oTherJoin = map[string]string{}

	var build strings.Builder
	build.WriteString("SELECT ")
	build.WriteString(dbinfo.SimpleTableName(entity))
	build.WriteString(".*,")

	dataInfoList := dbinfo.AllDataInfo(entity)
	for _, dataInfo := range dataInfoList {
		f, d := dao.GetFindByPageSelectSqlByField(dataInfo, dbinfo.SimpleTableName(entity), dataInfo.Name, oTherJoin)
		if f != "" {
			appendFieldSql.WriteString(f)
		}

		if d != "" {
			dictionarySql.WriteString(d)
		}
	}

	build.WriteString(appendFieldSql.String())
	build.WriteString(" FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" AS ")
	build.WriteString(dbinfo.SimpleTableName(entity))

	if dictionarySql.String() != "" {
		build.WriteString(dictionarySql.String())
	}

	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString(" =@")
	build.WriteString(dbinfo.EntityKeyName(entity))

	where := map[string]interface{}{dbinfo.EntityKeyName(entity): id}
	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
		where["Creator"] = currentUser
	}

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	text := strings.Replace(build.String(), ", FROM ", " FROM ", -1)

	rows, err := gorm.Raw(text, where).Rows()
	if err != nil {
		Log.Error("查询发生异常:", err)
		return msgentity.Err(1005, "查询发生异常:", err)
	}
	defer rows.Close()

	res := gorm.ScanRows2mapI(rows)
	if res == nil {
		return msgentity.Err(1006, "查询后数据转换发生异常")
	}

	if len(res) < 1 {
		return msgentity.Success(res, "查询成功,但没有数据")
	}

	return msgentity.Success(res[0], "查询成功")
}

/**
 * 查询分页数据
 * @param entity 检查用数据结构
 * @param findByPageParam 分页参数
 * @param currentUser 当前用户
 * @param onlyCreator 仅创建用户
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) FindByPage(entity dbinfo.Entity, findByPageParam dbinfo.FindByPageParam, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	findByPageCountSql, findByPageSql, params := dao.GetFindByPageSelectSql(currentUser, entity, findByPageParam, onlyCreator)

	var iCount int
	dbResult := gorm.Raw(findByPageCountSql, params).Find(&iCount)

	// var dbResult *ggorm.DB
	// if len(params) < 1 {
	// 	dbResult = gorm.Raw(findByPageCountSql).Find(&iCount)
	// } else {
	// 	dbResult = gorm.Raw(findByPageCountSql, params).Find(&iCount)
	// }

	if dbResult.Error != nil {
		Log.Error("查询分页数量发生异常:", dbResult.Error)
		return msgentity.Err(7001, "查询分页数量发生异常:", dbResult.Error)
	}

	if iCount < 1 {
		res := make([]map[string]interface{}, 0)
		page := findByPageParam.Page
		page.CountRow = 0
		page.SetRecord(res)

		return msgentity.Success(page, "查询分页数量成功但没有数据")
	}

	rows, err := gorm.Raw(findByPageSql, params).Rows()
	if err != nil {
		Log.Error("查询分页数量发生异常:", err)
		return msgentity.Err(7002, "查询分页数量发生异常")
	}
	defer rows.Close()

	page := findByPageParam.Page
	page.CountRow = iCount

	iCountPage := (iCount / page.Size)
	if (iCount % page.Size) > 0 {
		iCountPage = iCountPage + 1
	}
	page.CountPage = iCountPage

	res := gorm.ScanRows2mapI(rows)

	if res == nil {
		return msgentity.Err(7003, "查询分页数量成功但解析数据发生异常")
	}

	page.SetRecord(res)

	return msgentity.Success(page, "查询分页数量成功")
}

// func (dao DaoBaseFunc) FindData(OrderInfoList orders, String LikeStr, Object condition,
// 			onlyCreator bool, entity dbinfo.Entity) {

// 			}

// 取路径字段
func (dao DaoBaseFunc) GetPath(Id, dbName, tableName string) string {
	var build strings.Builder
	build.WriteString("SELECT Path FROM ")
	build.WriteString(dbName)
	build.WriteString(tableName)
	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.TableMajorKeyString[0])
	build.WriteString("=?")

	var Path string
	dbResult := gorm.Raw(build.String(), Id).Scan(&Path)
	if dbResult.Error != nil {
		Log.Error("查询路径信息发生异常:", dbResult.Error)
		return "/00/"
	}

	return Path
}

/**
 * 取指定节点下的子节点编号
 * @param tableInfo 表信息
 * @param Pid 父节点
 * @return
 */
func (dao DaoBaseFunc) NewChildId(entity dbinfo.Entity, Pid string) *msgentity.MsgEntity {
	var build strings.Builder
	build.WriteString("SELECT (cur + 1) AS newId FROM (")
	build.WriteString(" 		select cast(A.${MajorKeyString} AS SIGNED) AS cur, IFNULL(")
	build.WriteString(" 			(select MIN( CAST(B.${MajorKeyString} as signed integer)) from ${DbTableName} AS B")
	build.WriteString(" 				where cast(B.${MajorKeyString} AS SIGNED) > cast(A.${MajorKeyString} AS SIGNED)")
	build.WriteString(" 				and B.${PidKey} = @Pid")
	build.WriteString(" 				and CAST(B.${MajorKeyString} as signed integer) != 0")                                             // 过滤字段中含非数字的记录
	build.WriteString(" 				and CAST(B.${MajorKeyString} as signed integer) = CAST(B.${MajorKeyString} as signed integer) +0") // 过滤字段中含非数字的记录
	build.WriteString(" 			), 99999999) AS nxt")
	build.WriteString(" 		from ${DbTableName} AS A")
	build.WriteString(" 		where A.${PidKey} = @Pid")
	build.WriteString(" 		and CAST(A.${MajorKeyString} as signed integer) != 0")                                             // 过滤字段中含非数字的记录
	build.WriteString(" 		and CAST(A.${MajorKeyString} as signed integer) = CAST(A.${MajorKeyString} as signed integer) +0") // 过滤字段中含非数字的记录
	build.WriteString(" 	) AS D")
	build.WriteString(" WHERE (nxt - cur > 1) or (nxt = 99999999)")
	build.WriteString(" ORDER BY cur")
	build.WriteString(" LIMIT 0, 1")

	txt := build.String()
	txt = strings.Replace(txt, "${DbTableName}", entity.TableName(), -1)
	txt = strings.Replace(txt, "${TableName}", entity.OwnerTable(), -1)
	txt = strings.Replace(txt, "${PidKey}", dbinfo.TablePidKey[0], -1)
	txt = strings.Replace(txt, "${MajorKeyString}", dbinfo.TableMajorKeyString[0], -1)

	newId := ""
	dbResult := gorm.Raw(txt, sql.Named("Pid", Pid)).Find(&newId)
	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return msgentity.Err(1001, "查询数据发生异常:", dbResult.Error)
	}

	return msgentity.Success(newId, "获取子节点编号成功")
}

/**
 * 验证新增数据是否存在重复
 * @param entity 数据实体
 * @param currentUser 当前用户
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) ValidEntityRepeatByAdd(entity dbinfo.Entity, currentUser string) *msgentity.MsgEntity {
	customService := serviceMap[dbinfo.SimpleTableName(entity)]

	//-- 树形结构 --//
	if dbinfo.EntityHasPid(entity) {
		if customService == nil { //如果没有自定义业务层
			return dao.CommonCheckRepeatByAddAndTree(entity, currentUser) //通用树型结构表添加数据时重复检查方法
		}

		method := reflectutil.GetMethod(customService, "CheckRepeatByAddAndTree")
		if !method.IsValid() { //如果自定义业务层定义了自检方法
			return dao.CommonCheckRepeatByAddAndTree(entity, currentUser) //通用树型结构表添加数据时重复检查方法
		}

		result := reflectutil.DoMethod(customService, "CheckRepeatByAddAndTree", entity)
		me := result[0].Interface()
		return me.(*msgentity.MsgEntity)
	}

	//--不是树形数据则使用普通方法检查--//
	if customService == nil { //如果没有自定义业务层
		return dao.CommonCheckRepeatByAdd(entity, currentUser) //通用添加数据时重复检查方法
	}

	method := reflectutil.GetMethod(customService, "CheckRepeatByAdd")
	if !method.IsValid() { //如果自定义业务层定义了自检方法
		return dao.CommonCheckRepeatByAdd(entity, currentUser) //通用添加数据时重复检查方法
	}

	result := reflectutil.DoMethod(customService, "CheckRepeatByAdd", entity)
	me := result[0].Interface()
	return me.(*msgentity.MsgEntity)
}

/**
 * 验证更新数据是否存在重复
 * @param entity 数据实体
 * @param id 主键
 * @param data 数据
 * @param currentUser 当前用户
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) ValidEntityRepeatByEdit(entity dbinfo.Entity, id interface{}, data map[string]interface{}, currentUser string) *msgentity.MsgEntity {
	customService := serviceMap[dbinfo.SimpleTableName(entity)]

	//-- 树形结构 --//
	if dbinfo.EntityHasPid(entity) {
		if customService == nil { //如果没有自定义业务层
			return dao.CommonCheckRepeatByEditAndTree(entity, id, data["Name"], currentUser) //通用树型结构表添加数据时重复检查方法
		}

		method := reflectutil.GetMethod(customService, "CheckRepeatByEditAndTree")
		if !method.IsValid() { //如果自定义业务层定义了自检方法
			return dao.CommonCheckRepeatByEditAndTree(entity, id, data["Name"], currentUser) //通用树型结构表添加数据时重复检查方法
		}

		result := reflectutil.DoMethod(customService, "CheckRepeatByEditAndTree", id, data["Name"])
		me := result[0].Interface()
		return me.(*msgentity.MsgEntity)
	}

	//--不是树形数据则使用普通方法检查--//
	if customService == nil { //如果没有自定义业务层
		return dao.CommonCheckRepeatByEdit(entity, id, currentUser) //通用添加数据时重复检查方法
	}

	method := reflectutil.GetMethod(customService, "CheckRepeatByEdit")
	if !method.IsValid() { //如果自定义业务层定义了自检方法
		return dao.CommonCheckRepeatByEdit(entity, id, currentUser) //通用添加数据时重复检查方法
	}

	result := reflectutil.DoMethod(customService, "CheckRepeatByEdit", data, id)
	me := result[0].Interface()
	return me.(*msgentity.MsgEntity)
}

/**
 * 通用树型结构表添加数据时重复检查方法
 * @param entity 数据实体
 * @param currentUser 当前用户
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) CommonCheckRepeatByAddAndTree(entity dbinfo.Entity, currentUser string) *msgentity.MsgEntity {
	vName := dbinfo.GetVal(entity, dbinfo.TableTreeNode[0])
	if vName == nil {
		return msgentity.Err(1001, "节点名称为空")
	}

	Name := vName.(string)

	var Pid string
	vPid := dbinfo.GetVal(entity, dbinfo.TablePidKey[0])
	if vPid != nil {
		Pid = vPid.(string)
	} else {
		Pid = dbinfo.TableTreeRootValue[0]
	}

	if Pid == "" {
		Pid = dbinfo.TableTreeRootValue[0]
	}

	//同一层节点下,展现名不能相同//
	var build strings.Builder
	build.WriteString("SELECT SUM(iCount) AS iCount FROM (")
	build.WriteString(" 	select count(1) as iCount from ${DbTableName}")
	build.WriteString(" 	where ${PidKey} = @Pid and ${TreeNodeName} = @Name")
	build.WriteString(") TMP")

	txt := build.String()
	txt = strings.Replace(txt, "${DbTableName}", entity.TableName(), -1)
	txt = strings.Replace(txt, "${TableName}", entity.OwnerTable(), -1)
	txt = strings.Replace(txt, "${PidKey}", dbinfo.TablePidKey[0], -1)
	txt = strings.Replace(txt, "${TreeNodeName}", dbinfo.TableTreeNode[0], -1)

	var iCount int
	dbResult := gorm.Raw(txt, sql.Named(dbinfo.TablePidKey[1], Pid), sql.Named("Name", Name)).Find(&iCount)
	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return msgentity.Err(1002, "查询数据发生异常:", dbResult.Error)
	}

	if iCount != 0 {
		return msgentity.Err(1003, "节点重复")
	}

	return msgentity.Success(1999, "节点未重复")
}

/**
 * 通用树型结构表添加数据时重复检查方法
 * @param entity 数据实体
 * @param id 主键
 * @param Name 展现名
 * @param currentUser 当前用户
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) CommonCheckRepeatByEditAndTree(entity dbinfo.Entity, id interface{}, Name interface{}, currentUser string) *msgentity.MsgEntity {
	if (Name == nil) || (Name == "") || (Name == "<nil>") {
		return msgentity.Err(1001, "节点名称为空")
	}

	//同一层节点下,展现名不能相同//
	var build strings.Builder
	build.WriteString("SELECT SUM(iCount) AS iCount FROM (")
	build.WriteString(" 	select count(1) as iCount from ${DbTableName}")
	build.WriteString(" 	where ${Id} <> @Id")
	build.WriteString(" 	and ${PidKey} = (select a.${PidKey} from ${DbTableName} a where a.${Id} = @Id)")
	build.WriteString(" 	and ${TreeNodeName} = @Name")
	build.WriteString(") TMP")

	txt := build.String()
	txt = strings.Replace(txt, "${DbTableName}", entity.TableName(), -1)
	txt = strings.Replace(txt, "${TableName}", entity.OwnerTable(), -1)
	txt = strings.Replace(txt, "${Id}", dbinfo.EntityKeyName(entity), -1)
	txt = strings.Replace(txt, "${PidKey}", dbinfo.TablePidKey[0], -1)
	txt = strings.Replace(txt, "${TreeNodeName}", dbinfo.TableTreeNode[0], -1)

	var iCount int
	dbResult := gorm.Raw(txt, sql.Named("Id", id), sql.Named("Name", Name)).Find(&iCount)
	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return msgentity.Err(1002, "查询数据发生异常:", dbResult.Error)
	}

	if iCount != 0 {
		return msgentity.Err(1003, "节点重复")
	}

	return msgentity.Success(1999, "节点未重复")
}

/**
 * 通用添加数据时重复检查方法
 * @param entity 数据实体
 * @param currentUser 当前用户
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) CommonCheckRepeatByAdd(entity dbinfo.Entity, currentUser string) *msgentity.MsgEntity {

	vCheckRepeatCombination := checkRepeatCombinationMap[dbinfo.SimpleTableName(entity)]
	vCheckRepeatAlone := checkRepeatAloneMap[dbinfo.SimpleTableName(entity)]

	k := 0

	//检查待新增内容是否存在重复数据(多字段组合重复即重复)集合
	if (vCheckRepeatCombination != nil) && (len(vCheckRepeatCombination) > 0) {
		checkRepeatCombination := vCheckRepeatCombination

		var build strings.Builder
		build.WriteString("SELECT COUNT(1) AS iCount FROM ${DbTableName} WHERE 1=1")

		var temp strings.Builder
		temp.WriteString("[")

		where := make(map[string]interface{})
		for _, value := range checkRepeatCombination {
			dataInfo := entity.GetDataInfo(value)
			build.WriteString(" AND ")
			build.WriteString(dataInfo.Field)
			build.WriteString(" =@")
			build.WriteString(value)

			where[value] = dbinfo.GetVal(entity, value)
			temp.WriteString("、'")
			temp.WriteString(dataInfo.Comment)
			temp.WriteString("'")
		}

		if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
			build.WriteString(" AND ")
			build.WriteString(dbinfo.TableDelSign[0])
			build.WriteString(" != 1")
		}

		txt := strings.Replace(build.String(), "1=1 AND ", "", -1)
		txt = strings.Replace(txt, "${DbTableName}", entity.TableName(), -1)
		txt = strings.Replace(txt, "${TableName}", entity.OwnerTable(), -1)

		iCount := 0
		dbResult := gorm.Raw(txt, where).Find(&iCount)
		if dbResult.Error != nil {
			Log.Error("验证数据是否重复发生异常:", dbResult.Error)
			return msgentity.Err(1001, "验证数据是否重复发生异常:", dbResult.Error)
		}

		if iCount != 0 {
			temp.WriteString("]组合发现数据重复")
			return msgentity.Err(1002, strings.Replace(temp.String(), "、", "", 1))
		}

		k++
	}

	//检查待新增内容是否存在重复数据(单独字段重复即重复)集合
	if (vCheckRepeatAlone != nil) && (len(vCheckRepeatAlone) > 0) {
		checkRepeatAlone := vCheckRepeatAlone

		var build strings.Builder
		build.WriteString("SELECT SUM(iCount) FROM (")

		where := make(map[string]interface{})
		for key, value := range checkRepeatAlone {
			dataInfo := entity.GetDataInfo(key)
			build.WriteString(" union all select (SIGN(COUNT(1)) * ")
			build.WriteString(strconv.Itoa(value))
			build.WriteString(") as iCount ")
			build.WriteString(" from ${DbTableName} ")
			build.WriteString(" where ")
			build.WriteString(dataInfo.Field)
			build.WriteString("= @")
			build.WriteString(key)

			if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
				build.WriteString(" and ")
				build.WriteString(dbinfo.TableDelSign[0])
				build.WriteString(" != 1")
			}

			where[key] = dbinfo.GetVal(entity, key)
		}

		build.WriteString(") TMP")

		txt := strings.Replace(build.String(), " union all ", " ", 1)
		txt = strings.Replace(txt, "${DbTableName}", entity.TableName(), -1)
		txt = strings.Replace(txt, "${TableName}", entity.OwnerTable(), -1)

		iCount := 0
		dbResult := gorm.Raw(txt, where).Find(&iCount)
		if dbResult.Error != nil {
			Log.Error("验证数据是否重复发生异常:", dbResult.Error)
			return msgentity.Err(1003, "验证数据是否重复发生异常:", dbResult.Error)
		}

		if iCount != 0 {
			var temp strings.Builder
			str := fmt.Sprintf("%0*d", len(checkRepeatAlone), iCount)
			array := []rune(str)

			for key, value := range checkRepeatAlone {
				i := len(strconv.Itoa(value))
				if array[i] != 0 {
					continue
				}

				dataInfo := entity.GetDataInfo(key)
				temp.WriteString("、'")
				temp.WriteString(dataInfo.Comment)
				temp.WriteString("'")
			}

			temp.WriteString("存在重复")

			return msgentity.Err(1004, strings.Replace(temp.String(), "、", "", 1))
		}

		k++
	}

	if k == 0 {
		return msgentity.Success("没有设定验证函数,通过")
	}

	return msgentity.Success("经验证,通过")
}

/**
 * 通用更新数据时重复检查方法
 * @param entity 数据实体
 * @param id 数据主键
 * @param currentUser 当前用户
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) CommonCheckRepeatByEdit(entity dbinfo.Entity, id interface{}, currentUser string) *msgentity.MsgEntity {

	vCheckRepeatCombination := checkRepeatCombinationMap[dbinfo.SimpleTableName(entity)]
	vCheckRepeatAlone := checkRepeatAloneMap[dbinfo.SimpleTableName(entity)]

	k := 0

	//检查待新增内容是否存在重复数据(多字段组合重复即重复)集合
	if (vCheckRepeatCombination != nil) && (len(vCheckRepeatCombination) > 0) {
		checkRepeatCombination := vCheckRepeatCombination

		keyField := dbinfo.EntityKeyField(entity)
		var build strings.Builder
		build.WriteString("SELECT COUNT(1) AS iCount FROM ${DbTableName} WHERE 1=1")

		var temp strings.Builder
		temp.WriteString("[")

		where := make(map[string]interface{})
		where[keyField] = id
		build.WriteString(" AND ")
		build.WriteString(keyField)
		build.WriteString(" != @")
		build.WriteString(keyField)

		for _, value := range checkRepeatCombination {
			dataInfo := entity.GetDataInfo(value)
			build.WriteString(" AND ")
			build.WriteString(dataInfo.Field)
			build.WriteString(" =@")
			build.WriteString(value)

			where[value] = dbinfo.GetVal(entity, value) //利用反射取属性值

			temp.WriteString("、'")
			temp.WriteString(dataInfo.Comment)
			temp.WriteString("'")
		}

		if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
			build.WriteString(" AND ")
			build.WriteString(dbinfo.TableDelSign[0])
			build.WriteString(" != 1")
		}

		txt := strings.Replace(build.String(), "1=1 AND ", "", -1)
		txt = strings.Replace(txt, "${DbTableName}", entity.TableName(), -1)
		txt = strings.Replace(txt, "${TableName}", entity.OwnerTable(), -1)

		iCount := 0
		dbResult := gorm.Raw(txt, where).Find(&iCount)
		if dbResult.Error != nil {
			Log.Error("验证数据是否重复发生异常:", dbResult.Error)
			return msgentity.Err(1001, "验证数据是否重复发生异常:", dbResult.Error)
		}

		if iCount != 0 {
			temp.WriteString("]组合发现数据重复")
			return msgentity.Err(1002, strings.Replace(temp.String(), "、", "", 1))
		}

		k++
	}

	//检查待新增内容是否存在重复数据(单独字段重复即重复)集合
	if (vCheckRepeatAlone != nil) && (len(vCheckRepeatAlone) > 0) {
		checkRepeatAlone := vCheckRepeatAlone

		keyField := dbinfo.EntityKeyField(entity)

		var build strings.Builder
		build.WriteString("SELECT SUM(iCount) FROM (")

		where := make(map[string]interface{})
		where[keyField] = id

		for key, value := range checkRepeatAlone {
			dataInfo := entity.GetDataInfo(key)
			build.WriteString(" union all select (SIGN(COUNT(1)) * ")
			build.WriteString(strconv.Itoa(value))
			build.WriteString(") as iCount ")
			build.WriteString(" from ${DbTableName} ")
			build.WriteString(" where ")
			build.WriteString(dataInfo.Field)
			build.WriteString("= @")
			build.WriteString(key)
			build.WriteString(" and ")
			build.WriteString(keyField)
			build.WriteString(" != @")
			build.WriteString(keyField)

			if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
				build.WriteString(" and ")
				build.WriteString(dbinfo.TableDelSign[0])
				build.WriteString(" != 1")
			}

			where[key] = dbinfo.GetVal(entity, key)
		}

		build.WriteString(") TMP")

		txt := strings.Replace(build.String(), " union all ", " ", 1)
		txt = strings.Replace(txt, "${DbTableName}", entity.TableName(), -1)
		txt = strings.Replace(txt, "${TableName}", entity.OwnerTable(), -1)

		iCount := 0
		dbResult := gorm.Raw(txt, where).Find(&iCount)
		if dbResult.Error != nil {
			Log.Error("验证数据是否重复发生异常:", dbResult.Error)
			return msgentity.Err(1003, "验证数据是否重复发生异常:", dbResult.Error)
		}

		if iCount != 0 {
			var temp strings.Builder
			str := fmt.Sprintf("%0*d", len(checkRepeatAlone), iCount)
			array := []rune(str)

			for key, value := range checkRepeatAlone {
				i := len(strconv.Itoa(value))
				if array[i] != 0 {
					continue
				}

				dataInfo := entity.GetDataInfo(key)
				temp.WriteString("、'")
				temp.WriteString(dataInfo.Comment)
				temp.WriteString("'")
			}

			temp.WriteString("存在重复")

			return msgentity.Err(1004, strings.Replace(temp.String(), "、", "", 1))
		}

		k++
	}

	if k == 0 {
		return msgentity.Success("没有设定验证函数,通过")
	}

	return msgentity.Success("经验证,通过")
}

/**
 * 读取树形结构数据
 * @param entity 数据实体
 * @param groupColumn 分组字段
 * @param groupName 分组名称
 * @param currentUser 当前用户
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) FindByTree(entity dbinfo.Entity, groupColumn, groupName, currentUser string) *msgentity.MsgEntity {
	if !dbinfo.HasColumnName(entity, groupName) {
		return msgentity.Err(1001, "指定分组字段不存在！")
	}

	var build strings.Builder
	build.WriteString("SELECT * FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.TableMajorKeyString[0])
	build.WriteString(" > 0")

	where := make(map[string]interface{})
	if groupName != "" {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TablePathKey[0])
		build.WriteString(" LIKE (")
		build.WriteString(" 	select CONCAT(a.")
		build.WriteString(dbinfo.TablePathKey[0])
		build.WriteString(", '%')")
		build.WriteString(" 	from ${DbTableName} a")

		if groupColumn != "" {
			build.WriteString(" 	where a.")
			build.WriteString(groupColumn) //指定字段作为分组标识
			build.WriteString(" = ?")
		} else if dbinfo.EntityHasOnlyign(entity) {
			build.WriteString(" 	where a.")           //启用唯一标识作为关键字
			build.WriteString(dbinfo.TableOnlyign[0]) //启用唯一标识作为关键字
			build.WriteString(" = @groupName")        //启用唯一标识作为关键字
		} else {
			build.WriteString(" 	where a.")
			build.WriteString(dbinfo.TableTreeNode[0])
			build.WriteString(" = @groupName")
		}

		build.WriteString(" )")
		where["groupName"] = groupName
	}

	build.WriteString(" ORDER BY ")
	build.WriteString(dbinfo.TablePathKey[0])

	rows, err := gorm.Raw(build.String(), where).Rows()
	if err != nil {
		Log.Error("查询发生异常:", err)
		return msgentity.Err(1002, "查询发生异常:", err)
	}
	defer rows.Close()

	res := gorm.ScanRows2mapI(rows)
	if res == nil {
		return msgentity.Err(1003, "查询后数据转换发生异常")
	}

	return msgentity.Success(res, "查询成功")
}

/**
 * 根据字段名取指定记录编号的数据库表中对应字段的值
 * @param entity 实体类
 * @param id 记录编号
 * @param fieldNames 待取数据的字段名称集合
 * @param currentUser 当前用户
 * @Param onlyCreator 是否只取当前用户创建的数据
 * @return msgentity.MsgEntity 返回内容data中存放的是Map
 */
func (dao DaoBaseFunc) GetValueByFieldName(entity dbinfo.Entity, id interface{}, fieldNames []string, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	fieldNames = dao.holdByEntityToArray(entity, fieldNames) //按实体保留数组中的数据
	if len(fieldNames) < 1 {
		return msgentity.Err(7001, "没有对应的数据可查询！")
	}

	var build strings.Builder
	build.WriteString("SELECT ")

	for _, val := range fieldNames {
		build.WriteString(",")
		build.WriteString(val)
	}

	build.WriteString(" FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString("=@")
	build.WriteString(dbinfo.EntityKeyName(entity))

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	where := make(map[string]interface{})
	where[dbinfo.EntityKeyName(entity)] = id

	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
		where["Creator"] = currentUser
	}

	sql := strings.Replace(build.String(), ",", "", 1)
	rows, err := gorm.Raw(sql, where).Rows()
	if err != nil {
		Log.Error("查询发生异常:", err)
		return msgentity.Err(7002, "查询发生异常:", err)
	}
	defer rows.Close()

	res := gorm.ScanRows2mapI(rows)
	if res == nil {
		Log.Error("查询后数据转换发生异常:", err)
		return msgentity.Err(7003, "查询后数据转换发生异常:", err)
	}

	if len(res) < 1 {
		return msgentity.Err(7004, "数据不存在！")
	}

	return msgentity.Success(res[0], "查询成功")
}

/**
 * 根据字段名取指定记录编号的数据库表中对应字段的值
 * @param entity 实体类
 * @param id 记录编号
 * @param fieldName 待取数据的字段名称集合
 * @param currentUser 当前用户
 * @Param onlyCreator 是否只取当前用户创建的数据
 * @return msgentity.MsgEntity 返回内容data中存放的是Map
 */
func (dao DaoBaseFunc) GetValueByField(entity dbinfo.Entity, id interface{}, fieldName, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	fieldName = strings.TrimSpace(fieldName)
	if fieldName == "" {
		return msgentity.Err(7001, "没有对应的数据可查询！")
	}

	columns := strings.Split(fieldName, ",")
	columns = dao.holdByEntityToArray(entity, columns)
	if len(columns) < 1 {
		return msgentity.Err(7002, "没有对应的字段数据可查询！")
	}

	fieldName = ""
	for _, val := range columns {
		temp := strings.TrimSpace(val)
		if temp == "" {
			continue
		}

		fieldName = fieldName + "," + temp
	}

	if fieldName == "" {
		return msgentity.Err(7003, "没有对应的字段数据可查询！")
	}

	fieldName = fieldName[1:]

	var build strings.Builder
	build.WriteString("SELECT ")
	build.WriteString(fieldName)
	build.WriteString(" FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString(" =@")
	build.WriteString(dbinfo.EntityKeyName(entity))

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	where := make(map[string]interface{})
	where[dbinfo.EntityKeyName(entity)] = id

	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
		where["Creator"] = currentUser
	}

	sql := strings.Replace(build.String(), ",", "", 1)
	rows, err := gorm.Raw(sql, where).Rows()
	if err != nil {
		Log.Error("查询发生异常:", err)
		return msgentity.Err(7003, "查询发生异常:", err)
	}
	defer rows.Close()

	res := gorm.ScanRows2mapI(rows)
	if res == nil {
		Log.Error("查询后数据转换发生异常:", err)
		return msgentity.Err(7004, "查询后数据转换发生异常:", err)
	}

	if len(res) < 1 {
		return msgentity.Err(7005, "数据不存在！")
	}

	return msgentity.Success(res[0], "查询成功")
}

/**
 * 根据字段名取指定条件的数据库表中对应字段的值
 * @param entity 实体类
 * @param whereInfo 条件
 * @param fieldName 待取数据的字段名称集合
 * @param currentUser 当前用户
 * @Param onlyCreator 是否只取当前用户创建的数据
 * @return msgentity.MsgEntity 返回内容data中存放的是Map
 */
func (dao DaoBaseFunc) GetValueByWhere(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, fieldName, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	fieldName = strings.TrimSpace(fieldName)
	if fieldName == "" {
		return msgentity.Err(7001, "没有对应的数据可查询！")
	}

	if !dbinfo.HasColumnName(entity, fieldName) {
		return msgentity.Err(7002, "指定字段不存在！")
	}

	var build strings.Builder
	build.WriteString("SELECT ")
	build.WriteString(fieldName)
	build.WriteString(" FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE 1=1")

	where := make(map[string]interface{})
	for _, value := range whereInfo {
		condition := value.Condition
		if condition == "" {
			condition = "="
		}

		build.WriteString(" AND ")
		build.WriteString(value.Name)
		build.WriteString(" ")
		build.WriteString(condition)

		if value.ValueName != "" {
			build.WriteString(" ")
			build.WriteString(value.ValueName)
			continue
		}

		build.WriteString(" @")
		build.WriteString(value.Name)
		where[value.Name] = value.Value
	}

	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
		where["Creator"] = currentUser
	}

	sql := strings.Replace(build.String(), ",", "", 1)
	sql = strings.Replace(sql, " 1=1 AND ", " ", -1)
	sql = strings.Replace(sql, " WHERE 1=1", " ", -1)
	rows, err := gorm.Raw(sql, where).Rows()
	if err != nil {
		Log.Error("查询发生异常:", err)
		return msgentity.Err(7003, "查询发生异常:", err)
	}
	defer rows.Close()

	res := gorm.ScanRows2mapI(rows)
	if res == nil {
		Log.Error("查询发生异常:", err)
		return msgentity.Err(7004, "查询发生异常:", err)
	}

	if len(res) < 1 {
		return msgentity.Err(7005, "数据不存在！")
	}

	return msgentity.Success(res, "查询成功")
}

/**
 * 取记录对应的版本号
 * @param entity 实体类
 * @param idName 编号
 * @return
 */
func (dao DaoBaseFunc) GetEdition(entity dbinfo.Entity, id interface{}) *msgentity.MsgEntity {
	if fmt.Sprintf("%v", id) == "" {
		return msgentity.Err(7001, "记录编号参数为空！")
	}

	var build strings.Builder
	build.WriteString("SELECT ")
	build.WriteString(dbinfo.TableEdition[0])
	build.WriteString(" FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString("=@")
	build.WriteString(dbinfo.EntityKeyName(entity))

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	where := make(map[string]interface{})
	where[dbinfo.EntityKeyName(entity)] = id

	edition := 0
	dbResult := gorm.Raw(build.String(), where).Find(&edition)
	if dbResult.Error != nil {
		Log.Error("验证数据是否重复发生异常:", dbResult.Error)
		return msgentity.Err(7002, "验证数据是否重复发生异常:", dbResult.Error)
	}

	if edition == 0 {
		return msgentity.Err(7003, "没有发现此数据")
	}

	return msgentity.Success(edition, "查询成功")
}

/**
 * 取记录对应的状态值
 * @param entity 实体类
 * @param id 编号
 * @return
 */
func (dao DaoBaseFunc) GetState(entity dbinfo.Entity, id interface{}) *msgentity.MsgEntity {
	if fmt.Sprintf("%v", id) == "" {
		return msgentity.Err(7001, "记录编号参数为空！")
	}

	var build strings.Builder
	build.WriteString("SELECT ")
	build.WriteString(dbinfo.TableState[0])
	build.WriteString(" FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString("=@")
	build.WriteString(dbinfo.EntityKeyName(entity))

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	where := make(map[string]interface{})
	where[dbinfo.EntityKeyName(entity)] = id

	state := 0
	dbResult := gorm.Raw(build.String(), where).Find(&state)
	if dbResult.Error != nil {
		Log.Error("验证数据是否重复发生异常:", dbResult.Error)
		return msgentity.Err(7002, "验证数据是否重复发生异常:", dbResult.Error)
	}

	if state == 0 {
		return msgentity.Err(7003, "没有发现此数据")
	}

	return msgentity.Success(state, "查询成功")
}

/**
 * 根据关键值取对象集合
 * @Param entity 实体类
 * @Param where 存放查询条件
 * @Param currentUser 当前用户
 * @Param onlyCreator 是否只取当前用户创建的数据
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) FindByKey(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	whereInfo = dao.holdByEntityToWhereInfo(entity, whereInfo) //按实体保留条件
	if len(whereInfo) < 1 {
		return msgentity.Err(7002, "没有对应的查询条件！")
	}

	var build strings.Builder
	build.WriteString("SELECT * FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE 1=1")

	where := map[string]interface{}{}
	for _, value := range whereInfo {
		condition := value.Condition
		if condition == "" {
			condition = "="
		}

		build.WriteString(" AND ")
		build.WriteString(value.Name)
		build.WriteString(" ")
		build.WriteString(condition)

		if value.ValueName != "" {
			build.WriteString(" ")
			build.WriteString(value.ValueName)
			continue
		}

		build.WriteString(" @")
		build.WriteString(value.Name)
		where[value.Name] = value.Value
	}

	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
		where["Creator"] = currentUser
	}

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	hasOrderby := false

	if dbinfo.EntityHasRank(entity) {
		hasOrderby = true
		build.WriteString(" ORDER BY ")
		build.WriteString(dbinfo.TableRrank[0])
		build.WriteString(" ASC")
	}

	if dbinfo.EntityHasRank(entity) {
		if !hasOrderby {
			build.WriteString(" ORDER BY ")
		}

		build.WriteString(dbinfo.TableRrank[0])
		build.WriteString(" DESC")
	}

	sql := strings.Replace(build.String(), " 1=1 AND ", " ", 1)
	sql = strings.Replace(sql, " WHERE 1=1", " ", -1)
	rows, iCode, err := gorm.FindToMap(sql, where)
	if err != nil {
		return msgentity.Err(iCode, err.Error())
	}

	return msgentity.Success(rows, "查询成功")
}

/**
 * 根据关键值取对象集合中的第一个
 * @Param entity 实体类
 * @Param whereInfo 存放查询条件
 * @Param currentUser 当前用户
 * @param onlyCreator 是否只取当前用户创建的数据
 * @param fields 指定要查询的字段集合
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) FindOneByKey(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, currentUser string, onlyCreator bool, fields ...string) *msgentity.MsgEntity {
	columns := "," + entity.BaseColumnNames() + ","
	whereInfoTemp := []dbinfo.WhereInfo{}
	for _, val := range whereInfo {
		if !strings.Contains(columns, ","+val.Name+",") {
			continue
		}

		whereInfoTemp = append(whereInfoTemp, val)
	}

	if len(whereInfoTemp) < 1 {
		return msgentity.Err(7001, "没有对应的查询条件！")
	}

	var build strings.Builder
	build.WriteString("SELECT ")

	fields = dao.holdByEntityToArray(entity, fields) //按实体保留数组中的数据
	if len(fields) < 1 {
		return msgentity.Err(7002, "没有对应的字段数据可查询！")
	}

	for _, val := range fields {
		build.WriteString(",")
		build.WriteString(val)
	}

	build.WriteString(" FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE 1=1")

	where := map[string]interface{}{}
	for _, value := range whereInfoTemp {
		condition := value.Condition
		if condition == "" {
			condition = "="
		}

		build.WriteString(" AND ")
		build.WriteString(value.Name)
		build.WriteString(" ")
		build.WriteString(condition)

		if value.ValueName != "" {
			build.WriteString(" ")
			build.WriteString(value.ValueName)
			continue
		}

		build.WriteString(" @")
		build.WriteString(value.Name)
		where[value.Name] = value.Value
	}

	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
		where["Creator"] = currentUser
	}

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	text := strings.Replace(build.String(), "SELECT ,", "SELECT ", -1)
	text = strings.Replace(text, " 1=1 AND ", " ", -1)
	text = strings.Replace(text, " WHERE 1=1", " ", -1)
	data, iCode, err := gorm.FindToMap(text, where)
	if err != nil {
		return msgentity.Err(iCode, err)
	}

	if iCode < 1 {
		return msgentity.Err(7004, "数据不存在！")
	}

	return msgentity.Success(data[0], "查询成功")
}

/**
 * 根据关键值取对象集合中的符合条件的第一条记录的指定字段
 * @Param entity 实体类
 * @Param where 存放查询条件
 * @param fieldName 指定要查询的字段
 * @param currentUser 当前用户
 * @param onlyCreator 仅查询创建者
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) FindValueByKey(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, fieldName, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	whereInfo = dao.holdByEntityToWhereInfo(entity, whereInfo) //按实体保留条件
	if len(whereInfo) < 1 {
		return msgentity.Err(7002, "没有对应的查询条件！")
	}

	fieldName = strings.TrimSpace(fieldName)
	if fieldName == "" {
		return msgentity.Err(7002, "没有待查字段！")
	}

	if !dbinfo.HasColumnName(entity, fieldName) {
		return msgentity.Err(7003, "指定字段不存在！")
	}

	var build strings.Builder
	build.WriteString("SELECT ")
	build.WriteString(fieldName)
	build.WriteString(" FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE 1=1")

	where := map[string]interface{}{}
	for _, value := range whereInfo {
		condition := value.Condition
		if condition == "" {
			condition = "="
		}

		build.WriteString(" AND ")
		build.WriteString(value.Name)
		build.WriteString(" ")
		build.WriteString(condition)

		if value.ValueName != "" {
			build.WriteString(" ")
			build.WriteString(value.ValueName)
			continue
		}

		build.WriteString(" @")
		build.WriteString(value.Name)
		where[value.Name] = value.Value
	}

	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
		where["Creator"] = currentUser
	}

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	hasOrderby := false

	if dbinfo.EntityHasRank(entity) {
		hasOrderby = true
		build.WriteString(" ORDER BY ")
		build.WriteString(dbinfo.TableRrank[0])
		build.WriteString(" ASC")
	}

	if dbinfo.EntityHasRank(entity) {
		if !hasOrderby {
			build.WriteString(" ORDER BY ")
		}

		build.WriteString(dbinfo.TableRrank[0])
		build.WriteString(" DESC")
	}

	text := strings.Replace(build.String(), " 1=1 AND ", " ", -1)
	text = strings.Replace(text, " WHERE 1=1", " ", -1) //没有条件
	rows, iCount, err := gorm.FindToMap(text, where)
	if err != nil {
		return msgentity.Err(iCount, err.Error())
	}

	if iCount < 1 {
		return msgentity.Err(7006, "数据不存在！")
	}

	val := rows[0][fieldName]

	return msgentity.Success(val, "查询成功")
}

/**
 * 根据关键值取对象集合中的符合条件的第一条记录的指定字段
 * @Param entity 实体类
 * @Param where 存放查询条件
 * @param fieldMap 指定要查询的字段集合(原字段, 别名)
 * @param currentUser 当前用户
 * @param onlyCreator 仅查询创建者
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) FindByFields(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, fieldMap map[string]string, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	whereInfo = dao.holdByEntityToWhereInfo(entity, whereInfo) //按实体保留条件
	if len(whereInfo) < 1 {
		return msgentity.Err(7001, "没有对应的查询条件！")
	}

	var columns strings.Builder
	columns.WriteString("@@@")
	var orderStr strings.Builder
	orderStr.WriteString("@@@")

	iCount := 0
	for k, v := range fieldMap {
		if !dbinfo.HasColumnName(entity, k) {
			continue
		}

		iCount++

		columns.WriteString(",")
		columns.WriteString(k)

		orderStr.WriteString(",")
		orderStr.WriteString(k)

		if v != "" {
			columns.WriteString(" AS ")
			columns.WriteString(v)
		}
	}

	if iCount < 1 {
		return msgentity.Err(7003, "没有符合的字段！")
	}

	var build strings.Builder
	build.WriteString("SELECT ")
	build.WriteString(strings.Replace(columns.String(), "@@@,", "", -1))
	build.WriteString(" FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE 1=1")

	where := map[string]interface{}{}
	for _, value := range whereInfo {
		condition := value.Condition
		if condition == "" {
			condition = "="
		}

		build.WriteString(" AND ")
		build.WriteString(value.Name)
		build.WriteString(" ")
		build.WriteString(condition)

		if value.ValueName != "" {
			build.WriteString(" ")
			build.WriteString(value.ValueName)
			continue
		}

		build.WriteString(" @")
		build.WriteString(value.Name)
		where[value.Name] = value.Value
	}

	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
		where["Creator"] = currentUser
	}

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	build.WriteString(" ORDER BY ")
	build.WriteString(strings.Replace(orderStr.String(), "@@@,", "", -1))

	text := strings.Replace(build.String(), " WHERE 1=1 AND ", " WHERE ", -1)
	text = strings.Replace(text, " WHERE 1=1", " ", -1)

	rows, err := gorm.Raw(text, where).Rows()
	if err != nil {
		Log.Error("查询发生异常:", err)
		return msgentity.Err(7004, "查询发生异常:", err)
	}
	defer rows.Close()

	res := gorm.ScanRows2mapI(rows)
	if res == nil {
		Log.Error("查询后数据转换发生异常:", err)
		return msgentity.Err(7005, "查询后数据转换发生异常:", err)
	}

	if len(res) < 1 {
		return msgentity.Err(7006, "数据不存在！")
	}

	return msgentity.Success(res, "查询成功")
}

/**
 * 根据关键值查数量
 * @Param entity 实体类
 * @Param where 存放查询条件
 * @param currentUser 当前用户
 * @param onlyCreator 仅查询创建者
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) FindCountByKey(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	whereInfo = dao.holdByEntityToWhereInfo(entity, whereInfo) //按实体保留条件
	if len(whereInfo) < 1 {
		return msgentity.Err(7001, "没有对应的查询条件！")
	}

	var build strings.Builder
	build.WriteString("SELECT COUNT(1) AS iCount FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE 1=1")

	where := map[string]interface{}{}
	for _, value := range whereInfo {
		condition := value.Condition
		if condition == "" {
			condition = "="
		}

		build.WriteString(" AND ")
		build.WriteString(value.Name)
		build.WriteString(" ")
		build.WriteString(condition)

		if value.ValueName != "" {
			build.WriteString(" ")
			build.WriteString(value.ValueName)
			continue
		}

		build.WriteString(" @")
		build.WriteString(value.Name)
		where[value.Name] = value.Value
	}

	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
		where["Creator"] = currentUser
	}

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	sql := strings.Replace(build.String(), " 1=1 AND ", " ", 1)
	sql = strings.Replace(sql, " WHERE 1=1", " ", -1)

	rows, err := gorm.Raw(sql, where).Rows()
	if err != nil {
		Log.Error("查询发生异常:", err)
		return msgentity.Err(7002, "查询发生异常:", err)
	}
	defer rows.Close()

	res := gorm.ScanRows2mapI(rows)
	if res == nil {
		Log.Error("查询后数据转换发生异常:", err)
		return msgentity.Err(7003, "查询后数据转换发生异常:", err)
	}

	if len(res) < 1 {
		return msgentity.Err(7004, "数据不存在！")
	}

	return msgentity.Success(res[0]["iCount"], "查询成功")
}

/**
 * 以事务方式执行多个sql
 * 注意:Mapper必须存在才能执行
 * @param sqls [sql, params]
 * @return msgentity.MsgEntity 一个执行失败则回滚
 */
func (dao DaoBaseFunc) TransactionSql(sqls map[string]map[string]interface{}) *msgentity.MsgEntity {
	if len(sqls) < 1 {
		return msgentity.Err(7001, "没有需要执行的sql")
	}

	tx := gorm.GetDB().Begin() // 开始事务

	for sql, params := range sqls {
		err := tx.Exec(sql, params).Error

		if err == nil {
			continue
		}

		Log.Error("sql执行失败:", sql)
		tx.Rollback() //回滚事务
		return msgentity.Err(7002, "执行的sql发生错误", err)
	}

	tx.Commit() //提交事务

	return msgentity.Success(7999, "事务执行成功")
}

/**
 * 根据字段名取分组数据
 * @param entity 实体类
 * @param Creator 指定用户
 * @param fields 字段名与别名对象
 * @param currentUser 当前用户
 * @param onlyCreator 用户查询限制(仅创建者可见)
 * @return
 */
func (dao DaoBaseFunc) GroupByField(entity dbinfo.Entity, Creator string, fields map[string]string, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	return dao.GroupByFieldBase(entity, Creator, fields, currentUser, onlyCreator, false)
}

/**
 * 根据字段名取分组数据,并返回数量
 * @param entity 实体类
 * @param Creator 指定用户
 * @param fields 字段名与别名对象
 * @param currentUser 当前用户
 * @param onlyCreator 用户查询限制(仅创建者可见)
 * @return
 */
func (dao DaoBaseFunc) GroupByFieldAndCount(entity dbinfo.Entity, Creator string, fields map[string]string, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	return dao.GroupByFieldBase(entity, Creator, fields, currentUser, onlyCreator, true)
}

/**
 * 根据字段名取分组数据
 * @param entity 实体类
 * @param Creator 指定用户
 * @param fields 字段名与别名对象
 * @param currentUser 当前用户
 * @param onlyCreator 用户查询限制(仅创建者可见)
 * @param isGroupCount 添加分组后是否添加'数量列'
 * @return
 */
func (dao DaoBaseFunc) GroupByFieldBase(entity dbinfo.Entity, Creator string, fields map[string]string, currentUser string, onlyCreator, isGroupCount bool) *msgentity.MsgEntity {
	if len(fields) < 1 {
		return msgentity.Err(7001, "没有对应的待查字段！")
	}

	array := map[string]interface{}{}
	for key, value := range fields {
		array[key] = value
	}

	array = reflectutil.HoldByEntity(entity, array, "", "G") //按实体保留map中的数据

	var build strings.Builder
	build.WriteString("SELECT @@")

	for key, val := range array {
		build.WriteString(",")
		build.WriteString(key)

		if val.(string) != "" {
			build.WriteString(" AS ")
			build.WriteString(val.(string))
		}
	}

	if isGroupCount {
		build.WriteString(",")
		build.WriteString(" COUNT(1) AS iCount")
	}

	build.WriteString(" FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE 1=1")

	if Creator != "" {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
	}

	where := map[string]interface{}{}
	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
		where["Creator"] = currentUser
	}

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	build.WriteString(" GROUP BY @@")

	for key := range fields {
		build.WriteString(",")
		build.WriteString(key)
	}

	sql := build.String()
	sql = strings.Replace(sql, " WHERE 1=1 GROUP BY ", " GROUP BY ", -1)
	sql = strings.Replace(sql, " @@,", " ", -1)
	sql = strings.Replace(sql, " 1=1 AND ", " ", -1)

	rows, err := gorm.Raw(sql, where).Rows()
	if err != nil {
		Log.Error("查询发生异常:", err)
		return msgentity.Err(7003, "查询发生异常:", err)
	}
	defer rows.Close()

	res := gorm.ScanRows2mapI(rows)
	if res == nil {
		Log.Error("查询后数据转换发生异常:", err)
		return msgentity.Err(7004, "查询后数据转换发生异常:", err)
	}

	if len(res) < 1 {
		return msgentity.Err(7005, "数据不存在！")
	}

	return msgentity.Success(res, "查询成功")
}

/**
 * 取表中指定字段的最大值
 * @param entity 实体类
 * @param Creator 指定用户
 * @param field 字段名
 * @param where 查询条件
 * @param currentUser 当前用户
 * @param onlyCreator 用户查询限制(仅创建者可见)
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) MaxByField(entity dbinfo.Entity, Creator string, field string, whereInfo []dbinfo.WhereInfo, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	field = strings.TrimSpace(field)
	if field == "" {
		return msgentity.Err(7001, "没有待查字段！")
	}

	whereInfo = dao.holdByEntityToWhereInfo(entity, whereInfo) //按实体保留条件
	if len(whereInfo) < 1 {
		return msgentity.Err(7002, "没有对应的查询条件！")
	}

	if !dbinfo.HasColumnName(entity, field) {
		return msgentity.Err(7003, "指定字段不存在！")
	}

	var build strings.Builder
	build.WriteString("SELECT MAX(")
	build.WriteString(field)
	build.WriteString(") AS data FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE 1=1")

	where := map[string]interface{}{}
	for _, value := range whereInfo {
		condition := value.Condition
		if condition == "" {
			condition = "="
		}

		build.WriteString(" AND ")
		build.WriteString(value.Name)
		build.WriteString(" ")
		build.WriteString(condition)

		if value.ValueName != "" {
			build.WriteString(" ")
			build.WriteString(value.ValueName)
			continue
		}

		build.WriteString(" @")
		build.WriteString(value.Name)
		where[value.Name] = value.Value
	}

	if Creator == "" {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
	}

	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
		where["Creator"] = currentUser
	}

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	sql := strings.Replace(build.String(), " 1=1 AND ", " ", 1)

	rows, err := gorm.Raw(sql, where).Rows()
	if err != nil {
		Log.Error("查询发生异常:", err)
		return msgentity.Err(7004, "查询发生异常:", err)
	}
	defer rows.Close()

	res := gorm.ScanRows2mapI(rows)
	if res == nil {
		Log.Error("查询后数据转换发生异常:", err)
		return msgentity.Err(7005, "查询后数据转换发生异常:", err)
	}

	if len(res) < 1 {
		return msgentity.Err(7006, "数据不存在！")
	}

	return msgentity.Success(res[0]["data"], "查询成功")
}

/**
 * 取表中指定字段的最小值
 * @param entity 实体类
 * @param Creator 指定用户
 * @param field 字段名
 * @param where 查询条件
 * @param currentUser 当前用户
 * @param onlyCreator 用户查询限制(仅创建者可见)
 * @param whereStr 查询条件字符串
 * @return
 */
func (dao DaoBaseFunc) MinByField(entity dbinfo.Entity, Creator string, field string, whereInfo []dbinfo.WhereInfo, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	field = strings.TrimSpace(field)
	if field == "" {
		return msgentity.Err(7001, "没有待查字段！")
	}

	whereInfo = dao.holdByEntityToWhereInfo(entity, whereInfo) //按实体保留条件
	if len(whereInfo) < 1 {
		return msgentity.Err(7002, "没有对应的查询条件！")
	}

	if !dbinfo.HasColumnName(entity, field) {
		return msgentity.Err(7003, "指定字段不存在！")
	}

	var build strings.Builder
	build.WriteString("SELECT MIN(")
	build.WriteString(field)
	build.WriteString(") AS data FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE 1=1")

	where := map[string]interface{}{}
	for _, value := range whereInfo {
		condition := value.Condition
		if condition == "" {
			condition = "="
		}

		build.WriteString(" AND ")
		build.WriteString(value.Name)
		build.WriteString(" ")
		build.WriteString(condition)

		if value.ValueName != "" {
			build.WriteString(" ")
			build.WriteString(value.ValueName)
			continue
		}

		build.WriteString(" @")
		build.WriteString(value.Name)
		where[value.Name] = value.Value
	}

	if Creator == "" {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
	}

	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
		where["Creator"] = currentUser
	}

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	sql := strings.Replace(build.String(), " 1=1 AND ", " ", 1)

	rows, err := gorm.Raw(sql, where).Rows()
	if err != nil {
		Log.Error("查询发生异常:", err)
		return msgentity.Err(7004, "查询发生异常:", err)
	}
	defer rows.Close()

	res := gorm.ScanRows2mapI(rows)
	if res == nil {
		Log.Error("查询后数据转换发生异常:", err)
		return msgentity.Err(7005, "查询后数据转换发生异常:", err)
	}

	if len(res) < 1 {
		return msgentity.Err(7006, "数据不存在！")
	}

	return msgentity.Success(res[0]["data"], "查询成功")
}

/**
 * 检查关键值记录是否存在(返回1:存在;0:不存在)
 * @param entity 实体类
 * @param id 记录编号
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) HasById(entity dbinfo.Entity, id interface{}) *msgentity.MsgEntity {
	if fmt.Sprintf("%v", id) == "" {
		return msgentity.Err(7001, "记录编号参数为空！")
	}

	var build strings.Builder
	build.WriteString("SELECT COUNT(1) AS iCount FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE ")
	build.WriteString(dbinfo.EntityKeyField(entity))
	build.WriteString(" =@")
	build.WriteString(dbinfo.EntityKeyName(entity))

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	where := map[string]interface{}{dbinfo.EntityKeyName(entity): id}

	var iCount int
	dbResult := gorm.Raw(build.String(), where).Find(&iCount)
	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return msgentity.Err(7003, "查询数据发生异常:", dbResult.Error)
	}

	if iCount == 0 {
		return msgentity.Err(0, "数据不存在！")
	}

	return msgentity.Success(1, "数据存在！")
}

/**
 * 检查关键值记录是否存在(返回1:存在;0:不存在)
 * @param entity 实体类
 * @Param keyName 字段名
 * @Param keyValue 字段值
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) HasByKey(entity dbinfo.Entity, keyName string, keyValue interface{}) *msgentity.MsgEntity {
	keyName = strings.TrimSpace(keyName)
	if keyName == "" {
		return msgentity.Err(7001, "字段名参数为空！")
	}

	if (keyValue == nil) || (fmt.Sprintf("%v", keyValue) == "") || (fmt.Sprintf("%v", keyValue) == "<nil>") {
		return msgentity.Err(7002, "字段值参数为空！")
	}

	if !dbinfo.HasColumnName(entity, keyName) {
		return msgentity.Err(7003, "指定字段不存在！")
	}

	dataInfo := entity.GetDataInfo(keyName)
	if dataInfo == nil {
		return msgentity.Err(7004, "字段备注信息缺失")
	}

	switch dataInfo.DbFileType {
	case "int":
		temp, err := strconv.Atoi(keyValue.(string))
		if err != nil {
			return msgentity.Err(7005, "字段值参数为不符合规范！")
		}
		keyValue = temp
	case "bigint":
		temp, err := strconv.ParseInt(keyValue.(string), 10, 64)
		if err != nil {
			return msgentity.Err(7006, "字段值参数为不符合规范！")
		}
		keyValue = temp
	}

	var build strings.Builder
	build.WriteString("SELECT COUNT(1) AS iCount FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE ")
	build.WriteString(keyName)
	build.WriteString(" =@")
	build.WriteString(keyName)

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	where := map[string]interface{}{keyName: keyValue}

	var iCount int
	dbResult := gorm.Raw(build.String(), where).Find(&iCount)
	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return msgentity.Err(7007, "查询数据发生异常:", dbResult.Error)
	}

	if iCount == 0 {
		return msgentity.Err(0, "数据不存在！")
	}

	return msgentity.Success(1, "数据存在！")
}

/**
 * 执行SQL脚本获取单行单列数据
 * 注意:库名必须用${}进行包装,此脚本应只存在一条记录且为单列
 * @param sql 待执行的SQL脚本
 * @param where 存放查询条件
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) DoSql(sql string, where ...interface{}) *msgentity.MsgEntity {
	txt := gorm.ReplaceVariable(sql)

	var list []map[string]interface{}
	dbResult := gorm.Raw(txt, where...).Scan(&list)
	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return msgentity.Err(1002, "查询发生异常:", dbResult.Error)
	}

	if len(list) < 1 {
		return msgentity.Err(1003, "查询成功但没有数据")
	}

	return msgentity.Success(list, "查询成功")
}

/**
 * 执行SQL脚本获取单行单列数据
 * 注意:库名必须用${}进行包装,此脚本应只存在一条记录且为单列
 * @param sql 待执行的SQL脚本
 * @param where 存放查询条件
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) ExecSql(sql string, where ...interface{}) *msgentity.MsgEntity {
	txt := gorm.ReplaceVariable(sql)

	dbResult := gorm.Exec(txt, where...)
	if dbResult.Error != nil {
		Log.Error("执行发生异常:", dbResult.Error)
		return msgentity.Err(1001, "执行发生异常:", dbResult.Error)
	}

	if dbResult.RowsAffected < 1 {
		return msgentity.Err(1002, "执行成功但没有影响数")
	}

	return msgentity.Success(dbResult.RowsAffected, "执行成功")
}

/**
 * 执行SQL脚本获取单行单列数据
 * 注意:库名必须用${}进行包装,此脚本应只存在一条记录且为单列
 * @param sql 待执行的SQL脚本
 * @param where 存放查询条件
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) GetValue(sql string, where ...interface{}) *msgentity.MsgEntity {
	txt := gorm.ReplaceVariable(sql)

	var list []map[string]interface{}
	dbResult := gorm.Raw(txt, where...).Scan(&list)
	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return msgentity.Err(1002, "查询发生异常:", dbResult.Error)
	}

	if len(list) < 1 {
		return msgentity.Err(1003, "查询成功但没有数据")
	}

	var result interface{}
	row := list[0]
	for _, v := range row {
		result = v
	}

	return msgentity.Success(result, "查询成功")
}

/**
 * 执行SQL脚本获取一行数据(多列)
 * 注意:库名必须用${}进行包装,此脚本应只存在一条记录
 * @param sql 待执行的SQL脚本
 * @param where 存放查询条件
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) GetRow(sql string, where ...interface{}) *msgentity.MsgEntity {
	txt := gorm.ReplaceVariable(sql)

	var list []map[string]interface{}
	dbResult := gorm.Raw(txt, where...).Scan(&list)
	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return msgentity.Err(1001, "查询发生异常:", dbResult.Error)
	}

	if len(list) < 1 {
		return msgentity.Err(1002, "查询成功但没有数据")
	}

	return msgentity.Success(list[0], "查询成功")
}

/**
 * 执行SQL脚本获取多行数据(多列)
 * 注意:库名必须用${}进行包装,此脚本可返回多条记录
 * @param sql SQL脚本
 * @param where 存放查询条件
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) GetRows(sql string, where ...interface{}) *msgentity.MsgEntity {
	txt := gorm.ReplaceVariable(sql)

	var list []map[string]interface{}
	dbResult := gorm.Raw(txt, where...).Scan(&list)
	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return msgentity.Err(1001, "查询发生异常:", dbResult.Error)
	}

	if len(list) < 1 {
		return msgentity.Err(1002, "查询成功但没有数据")
	}

	return msgentity.Success(list, "查询成功")
}

/**
 * 根据关键值翻转值(限布尔值类型,1转2,2转1)
 * @Param entity 实体类
 * @Param whereInfo 存放查询条件
 * @Param reversalColumn 翻转的字段名
 * @Param currentUser 当前用户
 * @Param onlyCreator 是否只翻转创建人创建的数据
 * @return *msgentity.MsgEntity 返回验证结果
 */
func (dao DaoBaseFunc) ReversalByKey(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, reversalColumn, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	whereInfo = dao.holdByEntityToWhereInfo(entity, whereInfo) //按实体保留条件
	if len(whereInfo) < 1 {
		return msgentity.Err(7001, "没有对应的查询条件！")
	}

	var build strings.Builder
	build.WriteString("UPDATE ")
	build.WriteString(entity.TableName())
	build.WriteString(" SET ")
	build.WriteString(reversalColumn)
	build.WriteString("= IF(")
	build.WriteString(reversalColumn)
	build.WriteString("=1,2,1)")
	build.WriteString(" WHERE 1=1")

	where := map[string]interface{}{}
	for _, value := range whereInfo {
		condition := value.Condition
		if condition == "" {
			condition = "="
		}

		build.WriteString(" AND ")
		build.WriteString(value.Name)
		build.WriteString(" ")
		build.WriteString(condition)

		if value.ValueName != "" {
			build.WriteString(" ")
			build.WriteString(value.ValueName)
			continue
		}

		build.WriteString(" @")
		build.WriteString(value.Name)
		where[value.Name] = value.Value
	}

	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
		where["Creator"] = currentUser
	}

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableDelSign[0])
		build.WriteString(" != 1")
	}

	sql := strings.Replace(build.String(), " 1=1 AND ", " ", 1)

	dbResult := gorm.Exec(sql, where)
	if dbResult.Error != nil {
		Log.Error("执行发生异常:", dbResult.Error)
		return msgentity.Err(1001, "执行发生异常:", dbResult.Error)
	}

	if dbResult.RowsAffected < 1 {
		return msgentity.Err(1002, "执行成功但没有影响数")
	}

	return msgentity.Success(dbResult.RowsAffected, "执行成功")
}

/**
 * 根据条件仅查询指定字段名数据
 * @param entity 实体类
 * @param whereInfo
 * @param fieldNames 待取数据的字段名称集合
 * @Param currentUser 当前用户
 * @Param onlyCreator 是否只翻转创建人创建的数据
 * @return msgentity.MsgEntity 返回内容data中存放的是Map
 */
func (dao DaoBaseFunc) FindField(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo, fieldNames []string, currentUser string, onlyCreator bool) *msgentity.MsgEntity {
	fieldNames = dao.holdByEntityToArray(entity, fieldNames) //按实体保留数组中的数据
	if len(fieldNames) < 1 {
		return msgentity.Err(7001, "没有对应的数据可查询！")
	}

	whereInfo = dao.holdByEntityToWhereInfo(entity, whereInfo) //按实体保留条件

	var build strings.Builder
	build.WriteString("SELECT ")

	for _, val := range fieldNames {
		build.WriteString(",")
		build.WriteString(val)
	}

	build.WriteString(" FROM ")
	build.WriteString(entity.TableName())
	build.WriteString(" WHERE 1=1")

	where := map[string]interface{}{}
	for _, value := range whereInfo {
		condition := value.Condition
		if condition == "" {
			condition = "="
		}

		build.WriteString(" AND ")
		build.WriteString(value.Name)
		build.WriteString(" ")
		build.WriteString(condition)

		if value.ValueName != "" {
			build.WriteString(" ")
			build.WriteString(value.ValueName)
			continue
		}

		build.WriteString(" @")
		build.WriteString(value.Name)
		where[value.Name] = value.Value
	}

	if onlyCreator && (currentUser != "") {
		build.WriteString(" AND ")
		build.WriteString(dbinfo.TableCreator[0])
		build.WriteString("=@Creator")
		where["Creator"] = currentUser
	}

	hasOrderby := false

	if dbinfo.EntityHasRank(entity) {
		hasOrderby = true
		build.WriteString(" ORDER BY ")
		build.WriteString(dbinfo.TableRrank[0])
		build.WriteString(" ASC")
	}

	if dbinfo.EntityHasRank(entity) {
		if !hasOrderby {
			build.WriteString(" ORDER BY ")
		}

		build.WriteString(dbinfo.TableRrank[0])
		build.WriteString(" DESC")
	}

	sql := strings.Replace(build.String(), ",", "", 1)
	sql = strings.Replace(sql, " WHERE 1=1 AND ", " WHERE ", -1)
	sql = strings.Replace(sql, " WHERE 1=1", " ", -1)

	rows, err := gorm.Raw(sql, where).Rows()
	if err != nil {
		Log.Error("查询发生异常:", err)
		return msgentity.Err(7002, "查询发生异常:", err)
	}
	defer rows.Close()

	res := gorm.ScanRows2mapI(rows)
	if res == nil {
		Log.Error("查询发生异常:", err)
		return msgentity.Err(7003, "查询发生异常:", err)
	}

	if len(res) < 1 {
		return msgentity.Err(7004, "数据不存在！")
	}

	return msgentity.Success(res, "查询成功")
}

/**
 * 按实体保留切片中的数据
 * object 待检查对象
 * whereInfo 数据
 */
func (DaoBaseFunc) holdByEntityToWhereInfo(entity dbinfo.Entity, whereInfo []dbinfo.WhereInfo) []dbinfo.WhereInfo {
	columns := "," + entity.BaseColumnNames() + ","
	result := []dbinfo.WhereInfo{}
	for _, val := range whereInfo {
		if !strings.Contains(columns, ","+val.Name+",") {
			continue
		}

		result = append(result, val)
	}

	return result
}

/**
 * 按实体保留切片中的数据
 * object 待检查对象
 * data 数据
 * fieldPrefix 字段前缀(可不传)
 */
func (DaoBaseFunc) holdByEntityToArray(entity dbinfo.Entity, data []string) []string {
	columns := "," + entity.BaseColumnNames() + ","
	result := []string{}
	for _, val := range data {
		if !strings.Contains(columns, ","+val+",") {
			continue
		}

		result = append(result, val)
	}

	return result
}

// 分页数据查询部分语句,在分页查询数据及 数据搜索时使用
// 返回 findByPageCountSql, findByPageSql, params
func (dao DaoBaseFunc) GetFindByPageSelectSql(currentUser string, entity dbinfo.Entity,
	findByPageParam dbinfo.FindByPageParam, onlyCreator bool) (string, string, map[string]interface{}) {
	conditionMap := map[string]interface{}{}                     //普通条件
	conditionRangeStMap := map[string]interface{}{}              //范围条件
	conditionRangeEdMap := map[string]interface{}{}              //范围条件
	conditionArrayMap := map[string]string{}                     //数组条件
	conditionLikeMap := []string{}                               //模糊查询字段
	LikeStr := strings.TrimSpace(findByPageParam.LikeStr)        //全文检索条件
	sLikeDateSt := strings.TrimSpace(findByPageParam.LikeDateSt) //模糊查询记录修改时间范围条件-开始
	sLikeDateEd := strings.TrimSpace(findByPageParam.LikeDateEd) //模糊查询记录修改时间范围条件-结束

	params := map[string]interface{}{}

	var appendFieldSql strings.Builder
	var dictionarySql strings.Builder
	var oTherJoin map[string]string

	// var s reflect.Type
	// typeOf := reflect.TypeOf(entity)  //通过反射获取type定义
	// if typeOf.Kind() == reflect.Ptr { //是否指针类型
	// 	s = reflect.TypeOf(entity).Elem() //通过反射获取type定义
	// } else if "reflect.Value" == typeOf.String() {
	// 	if entity.(reflect.Value).Kind() == reflect.Ptr {
	// 		s = entity.(reflect.Value).Elem().Type()
	// 	} else if entity.(reflect.Value).Kind() == reflect.Struct {
	// 		s = entity.(reflect.Value).Type()
	// 	} else {
	// 		s = entity.(reflect.Value).Type()
	// 	}
	// } else if typeOf.Kind() == reflect.Struct {
	// 	s = typeOf
	// } else {
	// 	s = entity.(reflect.Value).Type() //通过反射获取type定义
	// }

	// sDbName := dbinfo.GetDbName(entity)
	// TableName := dbinfo.SimpleTableName(entity)
	//sMainTableKeyName := dbinfo.EntityKeyName(entity)

	if (findByPageParam.Condition != nil) && (len(findByPageParam.Condition.(map[string]interface{})) > 0) {
		condition := findByPageParam.Condition.(map[string]interface{})
		for key, val := range condition {
			if val == nil {
				continue
			}

			keyFieldName := key
			// if mu.isLowerStart(keyFieldName) {
			// 	keyFieldName = "G" + key
			// }

			//_, ok := s.FieldByName(keyFieldName) //直接以字段名作为条件的(即不管是否以St或Ed结尾,都不属于范围条件)
			if dbinfo.HasColumnName(entity, keyFieldName) {
				conditionMap[key] = val //明确值的字符串/日期/数字等都直接加入查询条件
				params[key] = val
				continue
			}

			if strings.HasSuffix(key, "St") { //以St结尾
				temp := key[0 : len(key)-2] //去掉结尾
				if dbinfo.HasColumnName(entity, temp) {
					conditionRangeStMap[temp] = val //明确值的字符串/日期/数字等都直接加入查询条件
					params[key] = val
				}

				// keyFieldName := temp
				// if mu.isLowerStart(keyFieldName) {
				// 	keyFieldName = "G" + temp
				// }

				// _, ok := s.FieldByName(keyFieldName) //直接以字段名作为条件的(即不管是否以St或Ed结尾,都不属于范围条件)
				// if ok {
				// 	conditionRangeStMap[temp] = val //明确值的字符串/日期/数字等都直接加入查询条件
				// 	params[key] = val
				// }

				continue
			}

			if strings.HasSuffix(key, "Ed") { //以Ed结尾
				temp := key[0 : len(key)-2] //去掉结尾
				if dbinfo.HasColumnName(entity, temp) {
					conditionRangeStMap[temp] = val //明确值的字符串/日期/数字等都直接加入查询条件
					params[key] = val
				}

				// keyFieldName := temp
				// if mu.isLowerStart(keyFieldName) {
				// 	keyFieldName = "G" + temp
				// }

				// _, ok := s.FieldByName(keyFieldName) //直接以字段名作为条件的(即不管是否以St或Ed结尾,都不属于范围条件)
				// if ok {
				// 	conditionRangeEdMap[temp] = val //明确值的字符串/日期/数字等都直接加入查询条件
				// 	params[key] = val
				// }
				continue
			}

			valType := reflect.TypeOf(val)
			if (strings.HasSuffix(key, "Array")) && (strings.HasPrefix(valType.String(), "[]")) { //以Array结尾,并且属于数组类型
				temp := key[0 : len(key)-len("Array")] //去掉结尾
				if !dbinfo.HasColumnName(entity, temp) {
					continue //字段名不存在
				}

				// keyFieldName := temp
				// if mu.isLowerStart(keyFieldName) {
				// 	keyFieldName = "G" + temp
				// }

				// _, ok := s.FieldByName(keyFieldName) //直接以字段名作为条件的(即不管是否以St或Ed结尾,都不属于范围条件)
				// if !ok {
				// 	continue //字段名不存在
				// }

				var sb strings.Builder
				sb.WriteString("(@@@@_@@@@")

				valArray := val.([]interface{})
				if strings.Contains("/int/int64/long/decimal/float64/", valType.String()) {
					for _, v := range valArray {
						sb.WriteString(",")
						sb.WriteString(fmt.Sprintf("%v", v))
					}
				} else {
					for _, v := range valArray {
						sb.WriteString(",'")
						sb.WriteString(fmt.Sprintf("%v", v))
						sb.WriteString("'")
					}
				}

				sb.WriteString(")")
				conditionArrayMap[temp] = strings.Replace(sb.String(), "@@@@_@@@@,", "", 1)

				continue
			}
		}
	}

	if LikeStr != "" {
		var likeDateSt time.Time
		var likeDateEd time.Time

		if sLikeDateSt != "" {
			likeDateSt = timeutil.ToDate(sLikeDateSt)
		} else {
			likeDateSt = timeutil.AddDay(time.Now(), -7)
		}

		if sLikeDateEd != "" {
			likeDateEd = timeutil.ToDate(sLikeDateEd)
		} else {
			likeDateEd = time.Now()
		}

		params["likeDateSt"] = likeDateSt
		params["likeDateEd"] = likeDateEd
	}

	dataInfoList := dbinfo.AllDataInfo(entity)
	for _, dataInfo := range dataInfoList {
		if findByPageParam.LikeStr != "" {
			containsMapLikeInit()
			_, ok := containsMapLike[dataInfo.Field]
			//if !strings.Contains("/id/Id/Id/Id/Creator/CreateDate/Modifieder/ModifiedDate/state/setp/Rank/edition/", dataInfo.Name) &&
			if !ok && ((dataInfo.DbFileType == "varchar") || (dataInfo.DbFileType == "char")) {
				conditionLikeMap = append(conditionLikeMap, dataInfo.TableName+"."+dataInfo.Name)
			}
		}

		if findByPageParam.HideBigText && dataInfo.IsBigTxt { //如果开启了过滤大文本字段,则跳过
			continue
		}

		if findByPageParam.HideFields != "" { //如果是过滤字段则跳过
			if strings.Contains(findByPageParam.HideFields, ";"+dataInfo.Name+";") {
				continue
			}
		}

		f, d := dao.GetFindByPageSelectSqlByField(dataInfo, entity.OwnerTable(), dataInfo.Name, oTherJoin)
		if f != "" {
			appendFieldSql.WriteString(f)
		}

		if d != "" {
			dictionarySql.WriteString(d)
		}
	}

	var defaultOrderSql strings.Builder
	defaultOrderSql.WriteString(" ORDER BY @@@@_@@@@")

	if dbinfo.EntityHasRank(entity) {
		defaultOrderSql.WriteString(",")
		defaultOrderSql.WriteString("${TableName}.")
		defaultOrderSql.WriteString(dbinfo.TableRrank[0])
	}

	if dbinfo.EntityHasModifiedDate(entity) {
		defaultOrderSql.WriteString(",")
		defaultOrderSql.WriteString("${TableName}.")
		defaultOrderSql.WriteString(dbinfo.TableModifiedDate[0])
		defaultOrderSql.WriteString(" DESC")
	}

	var vFindByPageWhere strings.Builder
	if onlyCreator && (dbinfo.EntityHasCreator(entity)) {
		currentLoginUserId := currentUser
		if !strings.Contains("/00000000/00000001/", "/"+currentLoginUserId+"/") {
			vFindByPageWhere.WriteString("AND ${TableName}.")
			vFindByPageWhere.WriteString(dbinfo.TableCreator[0])
			vFindByPageWhere.WriteString(" = '")
			vFindByPageWhere.WriteString(currentLoginUserId)
			vFindByPageWhere.WriteString("'")
		}
	}

	if len(conditionMap) > 0 {
		for key, val := range conditionMap {
			if strings.Contains(fmt.Sprintf("%v", val), "%") {
				vFindByPageWhere.WriteString(" AND ${TableName}.")
				vFindByPageWhere.WriteString(key)
				vFindByPageWhere.WriteString(" LIKE @")
				vFindByPageWhere.WriteString(key)
			} else {
				vFindByPageWhere.WriteString(" AND ${TableName}.")
				vFindByPageWhere.WriteString(key)
				vFindByPageWhere.WriteString(" =@")
				vFindByPageWhere.WriteString(key)
			}

			params[key] = val
		}
	}

	if len(conditionRangeStMap) > 0 {
		for key, val := range conditionRangeStMap {
			if fmt.Sprintf("%v", val) != "" {
				vFindByPageWhere.WriteString(" AND ${TableName}.")
				vFindByPageWhere.WriteString(key)
				vFindByPageWhere.WriteString(" >= @")
				vFindByPageWhere.WriteString(key)
				vFindByPageWhere.WriteString("St")
			}
		}
	}

	if len(conditionRangeEdMap) > 0 {
		for key, val := range conditionRangeEdMap {
			if fmt.Sprintf("%v", val) != "" {
				vFindByPageWhere.WriteString(" AND ${TableName}.")
				vFindByPageWhere.WriteString(key)
				vFindByPageWhere.WriteString(" <= @")
				vFindByPageWhere.WriteString(key)
				vFindByPageWhere.WriteString("Ed")
			}
		}
	}

	if len(conditionArrayMap) > 0 {
		for key, val := range conditionArrayMap {
			if fmt.Sprintf("%v", val) == "" {
				continue
			}

			vFindByPageWhere.WriteString(" AND ${TableName}.")
			vFindByPageWhere.WriteString(key)
			vFindByPageWhere.WriteString(" IN (")
			vFindByPageWhere.WriteString(val)
			vFindByPageWhere.WriteString(")")
		}
	}

	if (LikeStr != "") && (len(conditionLikeMap) > 0) {
		if dbinfo.EntityHasModifiedDate(entity) {
			vFindByPageWhere.WriteString(" AND ${TableName}.")
			vFindByPageWhere.WriteString(dbinfo.TableModifiedDate[0])
			vFindByPageWhere.WriteString(" >= @likeDateSt")
			vFindByPageWhere.WriteString(" AND ${TableName}.")
			vFindByPageWhere.WriteString(dbinfo.TableModifiedDate[0])
			vFindByPageWhere.WriteString(" <= @likeDateEd")
		}

		vFindByPageWhere.WriteString(" AND (@@@@_@@@@")

		for _, key := range conditionLikeMap {
			vFindByPageWhere.WriteString(" OR (")
			vFindByPageWhere.WriteString(key)
			vFindByPageWhere.WriteString(" LIKE CONCAT('%', @LikeStr, '%'))")
		}

		vFindByPageWhere.WriteString(")")

		params["LikeStr"] = LikeStr
	}

	if dbinfo.EntityHasDelSign(entity) { //存在逻辑删除字段
		vFindByPageWhere.WriteString(" AND ")
		vFindByPageWhere.WriteString(entity.TableName())
		vFindByPageWhere.WriteString(dbinfo.TableDelSign[0])
		vFindByPageWhere.WriteString(" != 1")
	}

	var findByPageCountSql strings.Builder
	findByPageCountSql.WriteString("SELECT COUNT(1) AS iCount FROM ")
	findByPageCountSql.WriteString(entity.TableName())

	if vFindByPageWhere.Len() != 0 {
		findByPageCountSql.WriteString(" WHERE ")
		findByPageCountSql.WriteString(vFindByPageWhere.String())
	}

	result0 := strings.Replace(findByPageCountSql.String(), "${TableName}", entity.OwnerTable(), -1)
	result0 = strings.Replace(result0, "(@@@@_@@@@)", "", -1)
	result0 = strings.Replace(result0, "@@@@_@@@@ OR ", "", -1)
	result0 = strings.Replace(result0, " AND AND ", " AND ", -1)
	result0 = strings.Replace(result0, " WHERE    AND ", " WHERE ", -1)
	result0 = strings.Replace(result0, " WHERE   AND ", " WHERE ", -1)
	result0 = strings.Replace(result0, " WHERE  AND ", " WHERE ", -1)
	result0 = strings.Replace(result0, " WHERE AND ", " WHERE ", -1)

	var findByPageSql strings.Builder
	findByPageSql.WriteString("SELECT ")
	findByPageSql.WriteString(appendFieldSql.String())
	findByPageSql.WriteString(" ${TableName}.* ")
	findByPageSql.WriteString(" FROM ${DbTableName} AS ${TableName} ")

	if dictionarySql.String() != "" {
		findByPageSql.WriteString(dictionarySql.String())
	}

	if vFindByPageWhere.Len() != 0 {
		findByPageSql.WriteString(" WHERE ")
		findByPageSql.WriteString(vFindByPageWhere.String())
	}

	if len(findByPageParam.Orders) > 0 {
		findByPageSql.WriteString(" ORDER BY ")
		findByPageSql.WriteString("@@@@_@@@@")
		for _, val := range findByPageParam.Orders {
			findByPageSql.WriteString(",")
			temp := val.String()
			for k, v := range gorm.GetVariables() {
				temp = strings.Replace(temp, k+".", v, -1)
			}

			findByPageSql.WriteString(temp)
		}
	} else {
		if !strings.Contains(defaultOrderSql.String(), " ORDER BY ") {
			findByPageSql.WriteString(" ORDER BY ")
		}

		findByPageSql.WriteString(defaultOrderSql.String())
	}

	if findByPageParam.Page.Size < 1 {
		findByPageParam.Page.Size = 10
	}

	if findByPageParam.Page.Current < 1 {
		findByPageParam.Page.Current = 1
	}

	findByPageSql.WriteString(" LIMIT ")
	findByPageSql.WriteString(strconv.Itoa((findByPageParam.Page.Current - 1) * findByPageParam.Page.Size))
	findByPageSql.WriteString(" , ")
	findByPageSql.WriteString(strconv.Itoa(findByPageParam.Page.Size))

	result1 := findByPageSql.String()

	result1 = strings.Replace(result1, "${DbTableName}", entity.TableName(), -1)
	result1 = strings.Replace(result1, "${TableName}", entity.OwnerTable(), -1)
	result1 = strings.Replace(result1, "@@@@_@@@@ OR ", "", -1)
	result1 = strings.Replace(result1, "@@@@_@@@@, ", "", -1)
	result1 = strings.Replace(result1, "ORDER BY @@@@_@@@@,", "ORDER BY ", -1)
	result1 = strings.Replace(result1, " ORDER BY @@@@_@@@@ LIMIT ", " LIMIT ", -1)
	result1 = strings.Replace(result1, " WHERE  AND ", " WHERE ", -1)
	result1 = strings.Replace(result1, " WHERE AND ", " WHERE ", -1)

	return result0, result1, params
}

// 取分页查询的字段信息
func (DaoBaseFunc) GetFindByPageSelectSqlByField(dataInfo *dbinfo.DataInfo, TableName, sFieldName string, oTherJoin map[string]string) (string, string) {
	var appendFieldSql strings.Builder
	var dictionarySql strings.Builder

	//--发现关联表--//
	if (TableName != dataInfo.TableName) && (dataInfo.RelName != "") && (dataInfo.RelMainName != "") {
		title := strings.TrimSpace(dataInfo.RelTitle)
		if title == "" {
			title = sFieldName
		}

		appendFieldSql.WriteString(" ")
		appendFieldSql.WriteString(dataInfo.TableName)
		appendFieldSql.WriteString("_")
		appendFieldSql.WriteString(dataInfo.RelName)
		appendFieldSql.WriteString(".")
		appendFieldSql.WriteString(dataInfo.Name)
		appendFieldSql.WriteString(" AS ")
		appendFieldSql.WriteString(title)
		appendFieldSql.WriteString(",")

		var leftJoinName strings.Builder
		leftJoinName.WriteString(dataInfo.TableName)
		leftJoinName.WriteString("_")
		leftJoinName.WriteString(dataInfo.RelName)

		_, ok := oTherJoin[leftJoinName.String()]
		if ok { //已经存在
			return appendFieldSql.String(), dictionarySql.String()
		}

		var leftJoin strings.Builder
		leftJoin.WriteString(" LEFT JOIN ")
		leftJoin.WriteString(gorm.GetDbName(dataInfo.DbName))
		leftJoin.WriteString(dataInfo.TableName)
		leftJoin.WriteString(" AS ")
		leftJoin.WriteString(leftJoinName.String())
		leftJoin.WriteString(" ON (")
		leftJoin.WriteString(dataInfo.TableName)
		leftJoin.WriteString("_")
		leftJoin.WriteString(dataInfo.RelName)
		leftJoin.WriteString(".")
		leftJoin.WriteString(dataInfo.RelName)
		leftJoin.WriteString(" = ")
		leftJoin.WriteString(gorm.GetDbName(dataInfo.DbName))
		leftJoin.WriteString(TableName)
		leftJoin.WriteString(".")
		leftJoin.WriteString(dataInfo.RelMainName)
		leftJoin.WriteString(") ")

		oTherJoin[leftJoinName.String()] = leftJoin.String() //防止重复累加相同的关联

		dictionarySql.WriteString(leftJoin.String())

		return appendFieldSql.String(), dictionarySql.String()
	}

	//--检查备注中的通用关联--//
	sComment := stringutil.SubBetween(dataInfo.Comment, "(", ")", true)
	if sComment == "" {
		return appendFieldSql.String(), dictionarySql.String()
	}

	if strings.HasPrefix(sComment, "枚举") { //枚举类型
		sComment = sComment[len("枚举")+1:]
		comment := strings.Split(sComment, ";")
		if len(comment) < 1 {
			return appendFieldSql.String(), dictionarySql.String()
		}

		appendFieldSql.WriteString(" CASE ")
		appendFieldSql.WriteString(dataInfo.TableName)
		appendFieldSql.WriteString(".")
		appendFieldSql.WriteString(dataInfo.Name)

		for _, val := range comment {
			array := strings.Split(val, ":")
			if len(array) < 1 {
				return appendFieldSql.String(), dictionarySql.String()
			}

			if len(array) < 2 {
				array = append(array, "缺失")
			}

			appendFieldSql.WriteString(" WHEN ")
			appendFieldSql.WriteString(array[0])
			appendFieldSql.WriteString(" THEN '")
			appendFieldSql.WriteString(array[1])
			appendFieldSql.WriteString("' ")
		}

		appendFieldSql.WriteString(" ELSE '未知' END AS ")
		appendFieldSql.WriteString(dataInfo.Name[1:])
		appendFieldSql.WriteString("Text,")

		return appendFieldSql.String(), dictionarySql.String()
	}

	if strings.HasPrefix(sComment, "布尔值,") { //布尔值
		sComment = sComment[len("布尔值,"):]
		comment := strings.Split(sComment, ";")
		if len(comment) <= 0 {
			return appendFieldSql.String(), dictionarySql.String()
		}

		appendFieldSql.WriteString(" CASE ")
		appendFieldSql.WriteString(TableName)
		appendFieldSql.WriteString(".")
		appendFieldSql.WriteString(dataInfo.Name)

		for _, val := range comment {
			val = strings.TrimSpace(val)
			if val == "" {
				return appendFieldSql.String(), dictionarySql.String()
			}

			array := strings.Split(val, ":")
			if len(array) < 1 {
				return appendFieldSql.String(), dictionarySql.String()
			}

			if len(array) < 2 {
				array = append(array, "缺失")
			}

			appendFieldSql.WriteString(" WHEN ")
			appendFieldSql.WriteString(array[0])
			appendFieldSql.WriteString(" THEN '")
			appendFieldSql.WriteString(array[1])
			appendFieldSql.WriteString("' ")
		}

		appendFieldSql.WriteString(" ELSE '否' END AS ")
		appendFieldSql.WriteString(dataInfo.Name[1:])
		appendFieldSql.WriteString("Text,")

		return appendFieldSql.String(), dictionarySql.String()
	}

	if strings.HasPrefix(sComment, "字典") { //字典
		appendFieldSql.WriteString(" Dictionary_")
		appendFieldSql.WriteString(dataInfo.Name)
		appendFieldSql.WriteString(".")
		appendFieldSql.WriteString(dbinfo.TableTreeNode[0])
		appendFieldSql.WriteString(" AS ")
		appendFieldSql.WriteString(dataInfo.Name[1:])
		appendFieldSql.WriteString("Text,")

		dictionarySql.WriteString(" LEFT JOIN ")
		dictionarySql.WriteString(gorm.GetDbName(dbinfo.BaseSystemDb[0]))
		dictionarySql.WriteString(dbinfo.TableNameDictionary[0])
		dictionarySql.WriteString(" AS Dictionary_")
		dictionarySql.WriteString(dataInfo.Name)
		dictionarySql.WriteString(" ON (Dictionary_")
		dictionarySql.WriteString(dataInfo.Name)
		dictionarySql.WriteString(".")
		dictionarySql.WriteString(dbinfo.TableOnlyign[0])
		dictionarySql.WriteString(" LIKE '")
		dictionarySql.WriteString(dataInfo.Name)
		dictionarySql.WriteString("%' AND Dictionary_")
		dictionarySql.WriteString(dataInfo.Name)
		dictionarySql.WriteString(".")
		dictionarySql.WriteString(dbinfo.TableDictionaryValue[0])
		dictionarySql.WriteString(" = CAST(")
		dictionarySql.WriteString(TableName)
		dictionarySql.WriteString(".")
		dictionarySql.WriteString(dataInfo.Name)
		dictionarySql.WriteString(" AS CHAR)) ")

		return appendFieldSql.String(), dictionarySql.String()
	}

	return appendFieldSql.String(), dictionarySql.String()
}

// 对gorm.First操作进行封装
func (DaoBaseFunc) First(entity dbinfo.Entity, conds ...interface{}) (dbinfo.Entity, error) {
	dbResult := gorm.GetDB().Table(entity.TableName()).First(entity, conds...)
	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return nil, dbResult.Error
	}

	if dbResult.RowsAffected < 1 {
		return nil, errors.New("没有查询到数据")
	}

	return entity, nil
}

// 对gorm.Create操作进行封装
func (DaoBaseFunc) Create(entity dbinfo.Entity) (dbinfo.Entity, error) {
	dbResult := gorm.GetDB().Table(entity.TableName()).Create(entity)
	if dbResult.Error != nil {
		Log.Error("新增发生异常:", dbResult.Error)
		return nil, dbResult.Error
	}

	if dbResult.RowsAffected < 1 {
		return nil, errors.New("新增失败,影响数为0")
	}

	return entity, nil
}

// 对gorm.Save操作进行封装,
// 警告:Save是一个组合函数。 如果保存值不包含主键，它将执行 Create，否则它将执行 Update (包含所有字段)。
func (DaoBaseFunc) Save(entity dbinfo.Entity) (dbinfo.Entity, error) {
	dbResult := gorm.GetDB().Table(entity.TableName()).Save(entity)
	if dbResult.Error != nil {
		Log.Error("更新发生异常:", dbResult.Error)
		return nil, dbResult.Error
	}

	if dbResult.RowsAffected < 1 {
		return nil, errors.New("更新失败,影响数为0")
	}

	return entity, nil
}

// 对gorm.Delete操作进行封装
func (DaoBaseFunc) Delete(entity dbinfo.Entity, conds ...interface{}) (int, error) {
	dbResult := gorm.GetDB().Table(entity.TableName()).Delete(entity, conds...)
	if dbResult.Error != nil {
		Log.Error("删除发生异常:", dbResult.Error)
		return 1001, dbResult.Error
	}

	iCode := dbResult.RowsAffected
	if iCode < 1 {
		return 1002, errors.New("删除失败,影响数为0")
	}

	return int(iCode), nil
}

// like查询map字段信息初始化
func containsMapLikeInit() {
	if len(containsMapLike) > 0 {
		return
	}

	containsMapInitSync.Lock()         //加锁
	defer containsMapInitSync.Unlock() //解锁

	containsMapLike[dbinfo.TableMajorKeyString[0]] = dbinfo.TableMajorKeyString[1]   //数据库表主键名称(字符串形式)
	containsMapLike[dbinfo.TableMajorKeyAutoInt[0]] = dbinfo.TableMajorKeyAutoInt[1] //数据库表主键名称(自增长形式)
	containsMapLike[dbinfo.TableMajorKeyUuId[0]] = dbinfo.TableMajorKeyUuId[1]       //数据库表主键名称(UUID形式)
	containsMapLike[dbinfo.TableCreator[0]] = dbinfo.TableCreator[1]                 //数据库表字段名称-创建人
	containsMapLike[dbinfo.TableCreateDate[0]] = dbinfo.TableCreateDate[1]           //数据库表字段名称-创建时间
	containsMapLike[dbinfo.TableModifieder[0]] = dbinfo.TableModifieder[1]           //数据库表字段名称-修改人
	containsMapLike[dbinfo.TableModifiedDate[0]] = dbinfo.TableModifiedDate[1]       //数据库表字段名称-修改时间
	containsMapLike[dbinfo.TableState[0]] = dbinfo.TableState[1]                     //数据库表字段名称-状态值
	containsMapLike[dbinfo.TableRrank[0]] = dbinfo.TableRrank[1]                     //数据库表字段名称-排序值
	containsMapLike[dbinfo.TableEdition[0]] = dbinfo.TableEdition[1]                 //数据库表字段名称-版本号
	containsMapLike[dbinfo.TablePassword[0]] = dbinfo.TablePassword[1]               //数据库表字段名称-密码
	containsMapLike[dbinfo.TableDelSign[0]] = dbinfo.TableDelSign[1]                 //数据库表字段名称-逻辑删除标识
	containsMapLike[dbinfo.TableSetp[0]] = dbinfo.TableSetp[1]                       //数据库表字段名称-步骤值
	containsMapLike[dbinfo.TableRecordKey[0]] = dbinfo.TableRecordKey[1]             //记录验证串字段名
}
