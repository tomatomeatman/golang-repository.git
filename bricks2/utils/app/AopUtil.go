package app

import (
	"strings"
	"sync"

	"gitee.com/tomatomeatman/golang-repository/bricks2/model/msgentity"
	"gitee.com/tomatomeatman/golang-repository/bricks2/utils/ginutil"
)

var (
	registerLock  sync.Mutex //注册锁
	beforeAopInfo = make(map[string][]func(ctx ginutil.Context, params ...interface{}) *msgentity.MsgEntity)
	afterAopInfo  = make(map[string][]func(ctx ginutil.Context, params ...interface{}) *msgentity.MsgEntity)
	aroundAopInfo = make(map[string][]func(ctx ginutil.Context, params ...interface{}) *msgentity.MsgEntity)
)

type AopUtil struct{}

/**
 * 注册Aop-函数执行前调用函数
 * @param funcName 被监听函数
 * @param doFunc 被调用函数
 * @return
 */
func (au AopUtil) RegisterBefore(funcName string, doFunc func(ctx ginutil.Context, params ...interface{}) *msgentity.MsgEntity) {
	funcName = strings.TrimSpace(funcName)
	if funcName == "" {
		return
	}

	registerLock.Lock()
	defer registerLock.Unlock()

	funcArray, ok := beforeAopInfo[funcName]
	if ok {
		funcArray = append(funcArray, doFunc)
		beforeAopInfo[funcName] = funcArray
		return
	}

	funcArray = []func(ctx ginutil.Context, params ...interface{}) *msgentity.MsgEntity{doFunc}
	beforeAopInfo[funcName] = funcArray
}

/**
 * 注册Aop-函数执行后调用函数
 * @param funcName 被监听函数
 * @param doFunc 被调用函数
 * @return
 */
func (au AopUtil) RegisterAfter(funcName string, doFunc func(ctx ginutil.Context, params ...interface{}) *msgentity.MsgEntity) {
	funcName = strings.TrimSpace(funcName)
	if funcName == "" {
		return
	}

	registerLock.Lock()
	defer registerLock.Unlock()

	funcArray, ok := afterAopInfo[funcName]
	if ok {
		funcArray = append(funcArray, doFunc)
		afterAopInfo[funcName] = funcArray
		return
	}

	funcArray = []func(ctx ginutil.Context, params ...interface{}) *msgentity.MsgEntity{doFunc}
	afterAopInfo[funcName] = funcArray
}

/**
 * 注册Aop-函数执行中调用函数
 * @param funcName 被监听函数 xxservice.xx
 * @param doFunc 被调用函数
 * @return
 */
func (au AopUtil) RegisterAround(funcName string, doFunc func(ctx ginutil.Context, params ...interface{}) *msgentity.MsgEntity) {
	funcName = strings.TrimSpace(funcName)
	if funcName == "" {
		return
	}

	registerLock.Lock()
	defer registerLock.Unlock()

	funcArray, ok := aroundAopInfo[funcName]
	if ok {
		funcArray = append(funcArray, doFunc)
		aroundAopInfo[funcName] = funcArray
		return
	}

	funcArray = []func(ctx ginutil.Context, params ...interface{}) *msgentity.MsgEntity{doFunc}
	aroundAopInfo[funcName] = funcArray
}

/**
 * 调用Aop-函数执行前调用函数
 * @param funcName 被监听函数 xxservice.xx
 * @param doFunc 被调用函数
 * @return
 */
func (au AopUtil) CallBeforeFunc(funcName string, ctx ginutil.Context, params ...interface{}) *msgentity.MsgEntity {
	return au.callFunc(beforeAopInfo, funcName, ctx, params)
}

/**
 * 调用Aop-函数执行后调用函数
 * @param funcName 被监听函数 xxservice.xx
 * @param doFunc 被调用函数
 * @return
 */
func (au AopUtil) CallAfterFunc(funcName string, ctx ginutil.Context, params ...interface{}) *msgentity.MsgEntity {
	return au.callFunc(afterAopInfo, funcName, ctx, params)
}

/**
 * 调用Aop-函数执行中调用函数
 * @param funcName 被监听函数 xxservice.xx
 * @param doFunc 被调用函数
 * @return
 */
func (au AopUtil) CallAroundFunc(funcName string, ctx ginutil.Context, params ...interface{}) *msgentity.MsgEntity {
	return au.callFunc(aroundAopInfo, funcName, ctx, params)
}

/**
 * 调用Aop-函数执行中调用函数
 * @param aopInfo aop信息
 * @param funcName 被监听函数 xxservice.xx
 * @param doFunc 被调用函数
 * @return
 */
func (au AopUtil) callFunc(aopInfo map[string][]func(ctx ginutil.Context, params ...interface{}) *msgentity.MsgEntity,
	funcName string, ctx ginutil.Context, params []interface{}) *msgentity.MsgEntity {
	if funcName == "" {
		return msgentity.Success(1000, "函数名为空,不处理")
	}

	funcArray, ok := aopInfo[funcName]
	if !ok {
		return msgentity.Success(1001, "没有函数,不处理")
	}

	if len(funcArray) < 1 {
		return msgentity.Success(1002, "没有调用函数,结束AOP处理")
	}

	for _, fun := range funcArray {
		me := fun(ctx, params...)
		if !me.Success {
			return me
		}
	}

	return msgentity.Success(1003, "调用函数没有错误,结束AOP处理")
}
