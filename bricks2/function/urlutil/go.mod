module gitee.com/tomatomeatman/golang-repository/bricks2/function/urlutil

go 1.23.4

require (
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	github.com/shopspring/decimal v1.4.0
)
