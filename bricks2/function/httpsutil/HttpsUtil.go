package httpsutil

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"github.com/cihub/seelog"
)

// GET请求
func Get(urlStr string, params map[string]interface{}, hears map[string]string) (bool, string, interface{}) {
	if params != nil {
		var temp strings.Builder
		temp.WriteString(urlStr)
		temp.WriteString("?lt=12")

		for key, value := range params {
			temp.WriteString("&")
			temp.WriteString(key)
			temp.WriteString("=")
			temp.WriteString(fmt.Sprintf("%v", value))
		}

		urlStr = strings.Replace(temp.String(), "lt=12&", "", 1)
	}

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	client := &http.Client{Transport: tr, Timeout: 10 * time.Second}

	req, _ := http.NewRequest("GET", urlStr, nil)
	if hears != nil {
		for key, value := range hears {
			req.Header.Add(key, value)
		}
	}

	resp, err := client.Do(req)
	if err != nil {
		return false, "请求错误:" + err.Error(), 1001
	}

	defer resp.Body.Close() // 释放对象

	// 把获取到的页面作为返回值返回
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return false, "请求错误:" + err.Error(), 1002
	}

	defer client.CloseIdleConnections() // 释放对象

	return true, "请求成功", string(body)
}

//// GET请求
//func (this HttpsUtil) Get(urlStr string, jsonParam string) string {
//	m, err := JsonUtil{}.JsonToMap(jsonParam)
//	if err != nil {
//		seelog.Error("Convert json to map failed with error: %+v\n", err)
//	}

//	urlStr = urlStr + "?lt=12"
//	for key, value := range m {
//		urlStr = urlStr + "&" + key + "=" + value
//	}

//	tr := &http.Transport{
//		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
//	}

//	client := &http.Client{Transport: tr, Timeout: 10 * time.Second}

//	req, _ := http.NewRequest("GET", urlStr, nil)
//	resp, err := client.Do(req)
//	if err != nil {
//		return msgentity.ErrString(9004, "请求错误:"+err.Error())
//	}

//	defer resp.Body.Close() // 释放对象

//	// 把获取到的页面作为返回值返回
//	body, err := io.ReadAll(resp.Body)
//	if err != nil {
//		return msgentity.ErrString(9006, "请求错误:"+err.Error())
//	}

//	defer client.CloseIdleConnections() // 释放对象

//	return msgentity.SuccessString(string(body), "请求成功")
//}

// POST请求
func Post(urlStr string, jsonParam string, isJsonParams bool) string {
	contentType := "charset=utf-8"
	if isJsonParams {
		contentType = "application/json;charset=utf-8"
	}

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	client := &http.Client{Transport: tr, Timeout: 10 * time.Second}

	req, _ := http.NewRequest("POST", urlStr, bytes.NewBuffer([]byte(jsonParam)))
	req.Header.Set("Content-Type", contentType)

	resp, err := client.Do(req)
	if err != nil {
		m := map[string]interface{}{
			"success": false,
			"msg":     "请求错误:" + err.Error(),
			"data":    9006,
		}

		ret_json, _ := json.Marshal(m)

		return string(ret_json)
	}

	defer resp.Body.Close()
	content, err := io.ReadAll(resp.Body)
	if err != nil {
		seelog.Error("Fatal error", err.Error())
	}

	defer client.CloseIdleConnections() // 释放对象

	m := map[string]interface{}{
		"success": true,
		"msg":     "请求成功",
		"data":    string(content),
	}

	ret_json, _ := json.Marshal(m)

	return string(ret_json)
}
