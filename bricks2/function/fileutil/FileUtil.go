package fileutil

import (
	"io"
	"os"
	"path/filepath"
	"strings"

	Log "github.com/cihub/seelog"
)

// 读取文件
func ReadFromFile(path string) (bool, string, interface{}) {
	if !IsExist(path) {
		return false, "文件不存在", 1001
	}

	data, err := os.ReadFile(path)
	if err != nil {
		return false, "读取失败", 1002
	}

	return true, "读取成功", string(data)
}

// 读取文件,返回[]byte
func Read(path string) (bool, string, interface{}) {
	if !IsExist(path) {
		return false, "文件不存在", 1001
	}

	data, err := os.ReadFile(path)
	if err != nil {
		return false, "读取失败", 1002
	}

	return true, path, data
}

// 调用os.MkdirAll递归创建文件夹
func CreateFile(filePath string) (bool, error) {
	if !IsExist(filePath) {
		err := os.MkdirAll(filePath, os.ModePerm)
		return false, err
	}

	return true, nil
}

// 判断所给路径文件/文件夹是否存在(返回true是存在)
func IsExist(path string) bool {
	_, err := os.Stat(path) //os.Stat获取文件信息
	if err != nil {
		return os.IsExist(err)
	}

	return true
}

// 保存文件内容
func Save(text string, path string) (bool, error) {
	// ok, err := f.CreateFile(path)
	// if err != nil {
	// 	return false, err
	// }

	// if !ok {
	// 	return false, err
	// }

	iEd := strings.LastIndex(path, "/")
	dir := path[:iEd]
	if dir != "." {
		os.MkdirAll(dir, os.ModePerm)
	}

	//使用 os.WriteFile写文件，在写入文件之前，不需要判断文件是否存在，如果文件不存在，会自动创建文件，如果文件存在，则会覆盖原来的内容
	err := os.WriteFile(path, []byte(text), 0666) // 保存到文件
	if err != nil {
		return false, err
	}

	return true, nil
}

// 删除文件
func Del(path string) (bool, error) {
	err := os.Remove(path)
	if err == nil { // 删除成功
		return true, err
	}

	// 删除失败
	ok := IsExist(path)
	if ok {
		return true, nil
	}

	Log.Error("删除文件失败:", err)

	return false, err
}

// 获取指定目录下的所有文件和目录
func GetFilesAndDirs(dirPth string) (files []string, dirs []string, err error) {
	dir, err := os.ReadDir(dirPth)
	if err != nil {
		return nil, nil, err
	}

	PthSep := string(os.PathSeparator)
	//suffix = strings.ToUpper(suffix) //忽略后缀匹配的大小写

	for _, fi := range dir {
		if fi.IsDir() { // 目录, 递归遍历
			dirs = append(dirs, dirPth+PthSep+fi.Name())
			GetFilesAndDirs(dirPth + PthSep + fi.Name())
		} else {
			// 过滤指定格式
			//if strings.HasSuffix(fi.Name(), ".go") {
			files = append(files, dirPth+PthSep+fi.Name())
			//}
		}
	}

	return files, dirs, nil
}

// 获取指定目录下的所有文件,包含子目录下的文件
func GetAllFiles(dirPth string) (files []string, err error) {
	var dirs []string
	dir, err := os.ReadDir(dirPth)
	if err != nil {
		return nil, err
	}

	PthSep := string(os.PathSeparator)
	//suffix = strings.ToUpper(suffix) //忽略后缀匹配的大小写

	for _, fi := range dir {
		if fi.IsDir() { // 目录, 递归遍历
			dirs = append(dirs, dirPth+PthSep+fi.Name())
			GetAllFiles(dirPth + PthSep + fi.Name())
		} else {
			// 过滤指定格式
			//if strings.HasSuffix(fi.Name(), ".go") {
			files = append(files, dirPth+PthSep+fi.Name())
			//}
		}
	}

	// 读取子目录下文件
	for _, table := range dirs {
		temp, _ := GetAllFiles(table)
		// for _, temp1 := range temp {
		// 	files = append(files, temp1)
		// }
		files = append(files, temp...)
	}

	return files, nil
}

/**
 * 文件信息信息
 * @author HuangXinBian
 */
type FileInfo struct {
	Name     string      `json:"name"`     //名称
	Path     string      `json:"path"`     //路径
	Dir      int         `json:"dir"`      //是否目录
	Size     int64       `json:"size"`     //大小
	Time     string      `json:"time"`     //修改时间
	Children []*FileInfo `json:"children"` //子文件
}

/**
 * 取文件夹信息树
 * rootName 根节点
 * delRoot 路径中隐藏根路径
 */
func GetFilesTree(rootName string, delRoot bool) (bool, string, interface{}) {
	rootName = strings.TrimSpace(rootName)
	root, err := os.Stat(rootName)
	if err != nil {
		return false, "没有数据!", 1001
	}

	Path := changePath(rootName)
	if delRoot {
		Path = strings.Replace(Path, rootName, "", 1) //0表示不替换任何符合数据
	}

	fileInfo := FileInfo{
		Name: root.Name(),
		Path: Path,
		Size: root.Size(),
		Time: root.ModTime().Format("2006-01-02 15:04:05"),
		Dir:  1,
	}

	if delRoot {
		buildTree(&fileInfo, rootName, rootName)
	} else {
		buildTree(&fileInfo, rootName, "")
	}

	printTree(&fileInfo)

	return true, "构造成功!", fileInfo
}

// 递归遍历文件夹及其子文件夹，构建节点树
func buildTree(node *FileInfo, vPath, replaceRoot string) {
	files, err := os.ReadDir(vPath)
	if err != nil {
		Log.Error("构建节点树异常:", err)
		return
	}

	for _, file := range files {
		if file.Name() == ".keep" {
			continue
		}

		info, _ := file.Info()
		fileInfo := FileInfo{}
		fileInfo.Name = file.Name()
		fileInfo.Size = info.Size()
		fileInfo.Time = info.ModTime().Format("2006-01-02 15:04:05")

		if !file.IsDir() {
			Path := changePath(vPath + "/" + fileInfo.Name)
			if replaceRoot != "" {
				Path = strings.Replace(Path, replaceRoot, "", 1) //0表示不替换任何符合数据
			}

			fileInfo.Path = Path
			fileInfo.Size = info.Size()
			fileInfo.Dir = 0
			node.Children = append(node.Children, &fileInfo)
			continue
		}

		childPath := filepath.Join(vPath, file.Name())
		Path := changePath(childPath)
		if replaceRoot != "" {
			Path = strings.Replace(Path, replaceRoot, "", 1) //0表示不替换任何符合数据
		}

		fileInfo.Path = Path
		fileInfo.Dir = 1

		node.Children = append(node.Children, &fileInfo)
		buildTree(&fileInfo, childPath, replaceRoot)
	}
}

// 递归节点树
func printTree(node *FileInfo) {
	for _, child := range node.Children {
		printTree(child)
	}
}

// 格式化路径
func changePath(vPath string) string {
	vPath = strings.Replace(vPath, "\\", "/", -1)

	// 检查路径是否有效
	if _, err := os.Stat(vPath); err != nil {
		if !strings.HasPrefix(vPath, "./") { //无效路径就加当前路径
			vPath = "./" + vPath
		}
	}

	return vPath
}

// 判断所给路径是否为文件夹
func IsDir(path string) bool {
	s, err := os.Stat(path)
	if err != nil {
		return false
	}

	return s.IsDir()
}

// 判断所给路径是否为文件
func IsFile(path string) bool {
	s, err := os.Stat(path)
	if err != nil {
		return false
	}

	return !s.IsDir()
}

// 删除文件(文件夹)
func DelFiles(dir string, delDir bool) error {
	d, err := os.Open(dir)
	if err != nil {
		return err
	}
	defer d.Close()

	names, err := d.Readdirnames(-1)
	if err != nil {
		return err
	}

	for _, name := range names {
		err = os.RemoveAll(dir + "/" + name)
		if err != nil {
			Log.Error("删除失败:", err)
			return err
		}
	}

	if !delDir {
		return nil
	}

	d.Close()

	err = os.Remove(dir)
	if err != nil {
		Log.Error("删除失败:", err)
		return err
	}

	return nil
}

/**
 * 创建文件所在文件夹
 * 注意:必须是文件,否则只能创建出上级路径
 * @param filePath 文件路径
 * @return bool:是否成功(如果路径已经存在则不会创建,但返回true); error:错误信息
 */
func CreateFileDir(filePath string) (bool, error) {
	path := filepath.Dir(filePath) //获取上级目录
	_, err := os.Stat(path)        //os.Stat获取文件信息
	if err == nil {                //获取成功,说明文件存在
		return true, nil
	}

	if os.IsExist(err) { //再次判断路径是否存在,也可以使用os.IsNotExist(err)来判断是否不存在
		return true, nil
	}

	err = os.MkdirAll(path, os.ModePerm)
	if err != nil {
		return false, err
	}

	return true, nil
}

/**
 * 判断文件夹是否存在
 * @param filePath 文件路径
 * @return bool, error:是否存在(存在返回true); error:错误信息
 */
func Exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}

	if os.IsNotExist(err) {
		return false, nil
	}

	return false, err
}

/**
 * 获取当前执行程序目录
 * @return string 路径
 */
func CurrentDir() string {
	//返回绝对路径  filepath.Dir(os.Args[0])去除最后一个元素的路径
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		Log.Error("获取程序目录异常:", err)
		return "."
	}

	//将\替换成/
	return strings.Replace(dir, "\\", "/", -1)
}

// 复制文件夹
func CopyDir(sourceDirPath, targetDirPath string) (bool, error) {
	// 检查源文件夹是否存在
	if _, err := os.Stat(sourceDirPath); os.IsNotExist(err) {
		return false, err
	}

	if strings.HasPrefix(sourceDirPath, "./") { //以"./"开头必须处理,否则会在"srcPath[len(sourceDirPath):]"获取错误
		appRoot, err := os.Getwd()
		if err != nil {
			return false, err
		}

		sourceDirPath = appRoot + sourceDirPath[1:] //去除"./",加入应用根目录
	}

	// 遍历源文件夹中的所有文件和子目录
	err := filepath.Walk(sourceDirPath, func(srcPath string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		// 计算目标路径
		temp := srcPath[len(sourceDirPath):]
		targetPath := filepath.Join(targetDirPath, temp)

		// 如果是目录，则创建目标目录
		if info.IsDir() {
			return os.MkdirAll(targetPath, info.Mode())
		}

		// 如果是文件，则复制文件
		srcFile, err := os.Open(srcPath)
		if err != nil {
			return err
		}
		defer srcFile.Close()

		targetFile, err := os.Create(targetPath)
		if err != nil {
			return err
		}
		defer targetFile.Close()

		_, err = io.Copy(targetFile, srcFile)
		if err != nil {
			return err
		}

		return os.Chmod(targetPath, info.Mode())
	})

	if err != nil {
		return false, err
	}

	return true, nil
}
