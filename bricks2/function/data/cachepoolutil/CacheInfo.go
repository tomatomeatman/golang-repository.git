package cachepoolutil

import "time"

//用于定义缓存信息的类
type CacheInfo struct {
	Id         string      `json:"id"`         //编号,作为关键字存在
	Data       interface{} `json:"data"`       //缓存的数据对象
	CreateDate time.Time   `json:"createDate"` //创建时间
	LastDate   time.Time   `json:"lastDate"`   //最后访问时间
}

/**
 * 新建缓存信息结构体
 * @param Id 编号,作为关键字存在
 * @param Data 缓存的数据对象
 * @return 返回新创建的结构体
 */
func New(Id string, Data interface{}) *CacheInfo {
	result := &CacheInfo{
		Id,
		Data,
		time.Now(),
		time.Now(),
	}

	return result
}
