package cachepoolutil

import (
	"fmt"
	"strings"
	"time"
)

var (
	cachMap = make(map[string]*CacheInfo) //存储缓存的集合
	//checkWrite sync.Mutex                             //保存锁
)

// 自定义缓存池
type CachePool struct{}

/**
 * 清理缓存
 * @return
 */
func (cp CachePool) Clear() (bool, string, interface{}) {
	if len(cachMap) < 1 {
		return true, "清理完毕", "原本就是空的"
	}

	cachMap = make(map[string]*CacheInfo)
	return true, "清理完毕", 6999
}

/**
* 释放缓存
* @param Id
* @return
 */
func (cp CachePool) Free(Id string) (bool, string, interface{}) {
	Id = strings.TrimSpace(Id)

	if (len(cachMap) < 1) || (Id == "") {
		return true, "释放成功", "本身没有这个缓存"
	}

	delete(cachMap, Id)

	return true, "释放成功", 6999
}

/**
* 释放缓存
* @param Id
* @return
 */
func (cp CachePool) FreeById(Id string) (bool, string, interface{}) {
	return cp.Free(Id)
}

/**
* 释放缓存
* @param Id
* @return
 */
func (cp CachePool) FreeByData(data interface{}) (bool, string, interface{}) {
	iCount := 0
	for key, val := range cachMap {
		if val != data {
			continue
		}

		delete(cachMap, key)
		iCount++
	}

	if iCount < 1 {
		return true, "释放成功", "本身没有这个缓存"
	}

	return true, "释放成功", iCount
}

/**
* 释放指定分钟之前的缓存
* @param iMinute
* @return
 */
func (cp CachePool) FreeByTime(iMinute int) (bool, string, interface{}) {
	if len(cachMap) < 1 {
		return true, "释放成功", "本身没有这个缓存"
	}

	//--取过期时间点--//
	now := time.Now()
	iCount := 0
	for key, val := range cachMap {
		if int(now.Sub(val.LastDate).Minutes()) < iMinute { //两个时间相减
			continue
		}

		delete(cachMap, key)
		iCount++
	}

	return true, "释放成功", "释放数:" + fmt.Sprintf("%v", iCount)
}

/**
* 添加缓存
* @param cacheInfo
* @return
 */
func (cp CachePool) Add(cacheInfo *CacheInfo) (bool, string, interface{}) {
	if cacheInfo.Data == nil {
		return false, "添加失败,缓存信息为null", 6001
	}

	Id := strings.TrimSpace(cacheInfo.Id)
	if Id == "" {
		return false, "添加失败,缓存信息关键字为空", 6002
	}

	cachMap[Id] = cacheInfo

	return true, "添加成功", cacheInfo
}

/**
* 添加缓存
* @param Id
* @param oData
* @return
 */
func (cp CachePool) AddData(Id string, data interface{}) (bool, string, interface{}) {
	Id = strings.TrimSpace(Id)
	if Id == "" {
		return false, "添加失败,缓存信息关键字为空", 6001
	}

	if data == nil {
		return false, "添加失败,缓存信息为null", 6002
	}

	cacheInfo := New(Id, data)

	cachMap[Id] = cacheInfo

	return true, "添加成功", cacheInfo
}

/**
* 判断是否包含缓存,包含返回true
* @param Id
* @return
 */
func (cp CachePool) Contains(Id string) bool {
	Id = strings.TrimSpace(Id)
	if Id == "" {
		return false
	}

	if len(cachMap) < 1 {
		return false
	}

	_, ok := cachMap[Id]

	return ok
}

/**
* 查找缓存信息
* @param Id
* @return
 */
func (cp CachePool) Find(Id string) *CacheInfo {
	if Id == "" {
		return nil
	}

	if len(cachMap) < 1 {
		return nil
	}

	cacheInfo, ok := cachMap[Id]
	if !ok {
		return nil //缓存中还没有存在
	}

	cacheInfo.LastDate = time.Now() //修改访问时间

	return cacheInfo
}

/**
* 查找缓存对象
* @param Id
* @return
 */
func (cp CachePool) FindData(Id string) interface{} {
	Id = strings.TrimSpace(Id)
	cacheInfo := cp.Find(Id)
	if cacheInfo == nil {
		return nil
	}

	return cacheInfo.Data
}
