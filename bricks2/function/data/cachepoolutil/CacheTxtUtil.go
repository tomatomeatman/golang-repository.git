package cachepoolutil

import (
	"encoding/json"
	"os"
	"reflect"
	"strings"
	"sync"

	Log "github.com/cihub/seelog"
)

type CacheTxtUtil struct{}

var (
	cacheTxtUtilLock sync.Mutex //同步锁
)

/**
 * 创建缓存文件
 * @param object 待存储数据对象
 * @param sCacheFile 缓存文件路径及文件名
 * @return
 */
func (ctu CacheTxtUtil) CreateCacheFile(object interface{}, sCacheFile string) (bool, string, interface{}) {
	if strings.HasPrefix(reflect.TypeOf(object).String(), "[]") {
		return ctu.CreateCacheFileByList(object.([]interface{}), sCacheFile)
	}

	list := []interface{}{object}
	return ctu.CreateCacheFileByList(list, sCacheFile)
}

/**
 * 创建缓存文件
 * @param list 数据集合
 * @param sCacheFile 缓存文件路径及文件名
 * @return
 */
func (ctu CacheTxtUtil) CreateCacheFileByList(list []interface{}, sCacheFile string) (bool, string, interface{}) {
	if len(list) < 1 {
		return false, "没有数据需要缓存！", 1001
	}

	ret_json, _ := json.Marshal(list)
	txt := string(ret_json)
	path := sCacheFile + ".tmp." + SequenceGet() //临时文件

	cacheTxtUtilLock.Lock() //加锁
	save(txt, path)
	cacheTxtUtilLock.Unlock() //解锁

	return true, "转换成功", 1999
}

// /**
//  * 读取缓存文件
//  * @param clazz 缓存内数据对象的格式
//  * @param sCacheFile 缓存文件路径及文件名
//  * @return
//  */
// func (ctu CacheTxtUtil) ReadCacheFile(clazz interface{}, sCacheFile ...string) []interface{} {
// 	if len(sCacheFile) < 1 {
// 		return make([]interface{}, 0)
// 	}

// 	path := sCacheFile[0]
// 	if (!FileUtil{}.IsExist(path)) {
// 		return make([]interface{}, 0)
// 	}

// 	text, err := os.ReadFile(path)
// 	if err != nil {
// 		Log.Error("读取文件异常:", err)
// 		return make([]interface{}, 0)
// 	}

// 	result := []interface{}{}

// 	err = json.Unmarshal([]byte(text), &result)
// 	if err != nil {
// 		Log.Error("Json字符串转换异常: %+v\n", err)
// 		return result
// 	}

// 	return result
// }

/**
 * 读取缓存文件
 * @param sCacheFile 缓存文件路径及文件名
 * @return
 */
func (ctu CacheTxtUtil) ReadCacheFile(sCacheFile ...string) []interface{} {
	if len(sCacheFile) < 1 {
		return make([]interface{}, 0)
	}

	path := sCacheFile[0]
	if !isExist(path) {
		return make([]interface{}, 0)
	}

	text, err := os.ReadFile(path)
	if err != nil {
		Log.Error("读取文件异常:", err)
		return make([]interface{}, 0)
	}

	result := []interface{}{}

	err = json.Unmarshal([]byte(text), &result)
	if err != nil {
		Log.Error("Json字符串转换异常: %+v\n", err)
		return result
	}

	return result
}

/**
 * 删除缓存文件
 * @param sCacheFile
 */
func (ctu CacheTxtUtil) DelCacheFile(sCacheFile string) (bool, string, interface{}) {
	ok, _ := del(sCacheFile)
	if !ok {
		return false, "删除失败！", 1000
	}

	return true, "删除成功！", 1999
}

// 判断所给路径文件/文件夹是否存在(返回true是存在)
func isExist(path string) bool {
	_, err := os.Stat(path) //os.Stat获取文件信息
	if err != nil {
		return os.IsExist(err)
	}

	return true
}

// 删除文件
func del(path string) (bool, error) {
	err := os.Remove(path)
	if err == nil { // 删除成功
		return true, err
	}

	// 删除失败
	ok := isExist(path)
	if ok {
		return true, nil
	}

	Log.Error("删除文件失败:", err)

	return false, err
}

// 保存文件内容
func save(text, path string) (bool, error) {
	// ok, err := f.CreateFile(path)
	// if err != nil {
	// 	return false, err
	// }

	// if !ok {
	// 	return false, err
	// }

	iEd := strings.LastIndex(path, "/")
	dir := path[:iEd]
	if dir != "." {
		os.MkdirAll(dir, os.ModePerm)
	}

	//使用 os.WriteFile写文件，在写入文件之前，不需要判断文件是否存在，如果文件不存在，会自动创建文件，如果文件存在，则会覆盖原来的内容
	err := os.WriteFile(path, []byte(text), 0666) // 保存到文件
	if err != nil {
		return false, err
	}

	return true, nil
}
