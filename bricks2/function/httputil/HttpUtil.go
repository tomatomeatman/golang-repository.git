package httputil

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	netUrl "net/url"

	Log "github.com/cihub/seelog"
)

func DoGet(url string, params map[string]interface{}, hears map[string]string) (bool, string, interface{}) {
	if params != nil {
		var temp strings.Builder
		temp.WriteString(url)
		temp.WriteString("?lt=12")

		for key, value := range params {
			temp.WriteString("&")
			temp.WriteString(key)
			temp.WriteString("=")
			temp.WriteString(fmt.Sprintf("%v", value))
		}

		url = strings.Replace(temp.String(), "lt=12&", "", 1)
	}

	client := &http.Client{}
	reqest, _ := http.NewRequest("GET", url, nil)

	if hears != nil {
		for key, value := range hears {
			reqest.Header.Add(key, value)
		}
	}

	response, err := client.Do(reqest)
	if err != nil {
		return false, "请求错误:" + err.Error(), 1001
	}

	defer response.Body.Close()

	body, err := io.ReadAll(response.Body)
	if err != nil {
		return false, "解析页面内容错误:" + err.Error(), 1002
	}

	return true, "请求成功", string(body)
}

// 发送POST请求
func DoPost(url string, params map[string]interface{}, hears map[string]string, isJson ...bool) (bool, string, interface{}) {
	var ioReader *strings.Reader
	vParams := netUrl.Values{}
	for key, val := range params {
		vParams.Set(key, fmt.Sprintf("%v", val))
	}

	// if len(params) > 0 {
	// 	ioReader = strings.NewReader(vParams.Encode())
	// } else {
	// 	ioReader = nil //必须设置,否则会错误
	// }
	ioReader = strings.NewReader(vParams.Encode()) //必须设置,否则会异常

	client := &http.Client{}
	reqest, _ := http.NewRequest("POST", url, ioReader)

	if (len(isJson) > 0) && isJson[0] {
		reqest.Header.Add("Content-Type", "application/json")
	} else {
		reqest.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	}

	if hears != nil {
		for key, value := range hears {
			reqest.Header.Add(key, value)
		}
	}

	response, err := client.Do(reqest)
	if err != nil {
		Log.Error("请求访问地址发生异常:", err)
		return false, "请求访问地址发生异常", 1001
	}

	defer response.Body.Close()

	body, err := io.ReadAll(response.Body)
	if err != nil {
		Log.Error("获取请求内容发生异常:", err)
		return false, "请求访问地址内容发生异常", 1002
	}

	return true, "Post请求成功", body
}

// func DoPost(url string, params map[string]interface{}, hears map[string]string) (bool, string, interface{}) {
// 	vParams := netUrl.Values{}
// 	for key, val := range params {
// 		vParams.Set(key, fmt.Sprintf("%v", val))
// 	}

// 	var ioReader *bytes.Buffer
// 	if params != nil {
// 		bodyBytes, err := json.Marshal(params)
// 		if err != nil {
// 			return false, 1001, "请求参数错误")
// 		}

// 		ioReader = bytes.NewBuffer(bodyBytes)
// 	}

// 	client := &http.Client{}
// 	reqest, _ := http.NewRequest("POST", url, ioReader)

// 	if hears != nil {
// 		for key, value := range hears {
// 			reqest.Header.Add(key, value)
// 		}
// 	}

// 	//response, err := http.PostForm(url, vParams)
// 	response, err := client.Do(reqest)
// 	if err != nil {
// 		Log.Error("请求访问地址发生异常:", err)
// 		return false, 1001, "请求访问地址发生异常")
// 	}

// 	defer response.Body.Close()

// 	body, err := io.ReadAll(response.Body)
// 	if err != nil {
// 		Log.Error("获取请求内容发生异常:", err)
// 		return false, 1002, "请求访问地址内容发生异常")
// 	}

// 	return true, body, "Post请求成功")
// }

func Get(url string, jsonParams string) string {
	client := http.Client{Timeout: 10 * time.Second}

	if jsonParams != "" {
		params, err := jsonToMap(jsonParams)
		if err != nil {
			fmt.Printf("Convert json to map failed with error: %+v\n", err)
		}

		var temp strings.Builder
		temp.WriteString(url)
		temp.WriteString("?lt=12")

		for key, value := range params {
			temp.WriteString("&")
			temp.WriteString(key)
			temp.WriteString("=")
			temp.WriteString(fmt.Sprintf("%v", value))
		}

		url = strings.Replace(temp.String(), "lt=12&", "", 1)
	}

	resp, err := client.Get(url)
	if err != nil {
		m := map[string]interface{}{
			"success": false,
			"msg":     "请求错误:" + err.Error(),
			"data":    9006,
		}

		ret_json, _ := json.Marshal(m)

		return string(ret_json)
	}

	defer resp.Body.Close() // 释放对象

	// 把获取到的页面作为返回值返回
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		m := map[string]interface{}{
			"success": false,
			"msg":     "请求错误:" + err.Error(),
			"data":    9007,
		}

		ret_json, _ := json.Marshal(m)

		return string(ret_json)
	}

	defer client.CloseIdleConnections() // 释放对象

	m := map[string]interface{}{
		"success": true,
		"msg":     "请求成功",
		"data":    string(body),
	}

	ret_json, _ := json.Marshal(m)

	return string(ret_json)
}

func Post(url string, sParams string, isJsonParams bool) string {
	contentType := "charset=utf-8"
	if isJsonParams {
		contentType = "application/json;charset=utf-8"
	}

	response, err := http.Post(url, contentType, bytes.NewBuffer([]byte(sParams)))
	if err != nil {
		m := map[string]interface{}{
			"success": false,
			"msg":     "请求错误:" + err.Error(),
			"data":    9006,
		}

		ret_json, _ := json.Marshal(m)

		return string(ret_json)
	}

	defer response.Body.Close()
	content, err := io.ReadAll(response.Body)
	if err != nil {
		m := map[string]interface{}{
			"success": false,
			"msg":     "请求错误:" + err.Error(),
			"data":    9007,
		}

		ret_json, _ := json.Marshal(m)

		return string(ret_json)
	}

	m := map[string]interface{}{
		"success": true,
		"msg":     "请求成功",
		"data":    string(content),
	}

	ret_json, _ := json.Marshal(m)

	return string(ret_json)
}

// json字符串转map
func jsonToMap(jsonStr string) (map[string]string, error) {
	if jsonStr == "" {
		return make(map[string]string), nil
	}

	m := make(map[string]string)
	err := json.Unmarshal([]byte(jsonStr), &m)
	if err != nil {
		Log.Error("Json字符串转换异常: %+v\n", err)
		return nil, err
	}

	return m, nil
}
