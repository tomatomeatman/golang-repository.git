package globalconst

const (

// /**
//  * 数据库表主键名称(字符串形式)
//  */
// TableMajorKeyString = "id" //"Id"

// /**
//  * 数据库表主键名称(自增长形式)
//  */
// TableMajorKeyAutoInt = "id" //"Id"

// /**
//  * 数据库表主键名称(UUID形式)
//  */
// TableMajorKeyUId = "id" //"Id"

// /**
//  * 数据库表字段名称-上级字段名
//  */
// TablePidKey = "pid" //"Pid"

// /**
//  * 数据库表字段名称-主键路径名
//  */
// TablePathKey = "path" //"Path"

// /**
//  * 数据库表字段名称-树形节点名
//  */
// TableTreeNodeName = "name" //"Name"

// /**
//  * 数据库表字段名称-创建人
//  */
// TableCreatorName = "creator" //"Creator"

// /**
//  * 数据库表字段名称-创建时间
//  */
// TableCreateDateName = "creator_date" //"CreateDate"

// /**
//  * 数据库表字段名称-修改人
//  */
// TableModifiederName = "modifieder" //"Modifieder"

// /**
//  * 数据库表字段名称-修改时间
//  */
// TableModifiedDateName = "modifieder_data" //"ModifiedDate"

// /**
//  * 数据库表字段名称-状态值
//  */
// TableState = "state" //"State"

// /**
//  * 数据库表字段名称-排序值
//  */
// TableRrankName = "rank" //"Rank"

// /**
//  * 数据库表字段名称-版本号
//  */
// TableEdition = "edition" //"Edition"

// /**
//  * 数据库表字段名称-逻辑删除标识
//  */
// TableDelSignName = "deleted" //"iDeleted"

// /**
//  * 数据库表字段名称-步骤值
//  */
// TableSetpName = "setp" //"Setp"

// /**
//  * 数据库表字段名称-唯一标识
//  */
// TableOnlyignName = "onlyign" //"sOnlyign"

// /**
//  * 基本数据库库名
//  */
// BaseSystemName = "base_system" //"BaseSystem"

// /**
//  * 字典表表名
//  */
// TableNameDictionary = "dictionary" //"Dictionary"

// /**
//  * 字典表值字段名
//  */
// TableDictionaryValueName = "value" //"Value"

// /**
//  * 记录验证串字段名
//  */
// TableRecordKeyName = "record_key" //"sRecordKey"

// /**
//  * 标识字段名
//  */
// TableSign = "sign" //"Sign"

// /**
// * 备注字段名
//  */
// TableMemo = "memo" //"Memo"

// /**
//  * 数据库树型表根节点默认值
//  */
// TableTreeRootValue = "00"

// //-------------------------------------------//

// /**
//  * 属性对应数据库表主键名称(字符串形式)
//  */
// GtableMajorKeyString = "id" //"Id"

// /**
//  * 属性对应数据库表主键名称(自增长形式)
//  */
// GtableMajorKeyAutoInt = "id" //"Id"

// /**
//  * 属性对应数据库表主键名称(UUID形式)
//  */
// GtableMajorKeyUId = "id" //"Id"

// /**
//  * 属性对应数据库表字段名称-上级字段名
//  */
// GtablePidKey = "" //"Pid"

// /**
//  * 属性对应数据库表字段名称-主键路径名
//  */
// GtablePathKey = "path" //"Path"

// /**
//  * 属性对应数据库表字段名称-树形节点名
//  */
// GtableTreeNodeName = "name" //"Name"

// /**
//  * 属性对应数据库表字段名称-创建人
//  */
// GtableCreatorName = "creator" //"Creator"

// /**
//  * 属性对应数据库表字段名称-创建时间
//  */
// GtableCreateDateName = "create_date" //"CreateDate"

// /**
//  * 属性对应数据库表字段名称-修改人
//  */
// GtableModifiederName = "modified" //"Modifieder"

// /**
//  * 属性对应数据库表字段名称-修改时间
//  */
// GtableModifiedDateName = "modified_date" //"ModifiedDate"

// /**
//  * 属性对应数据库表字段名称-状态值
//  */
// GtableStateName = "state" //"State"

// /**
//  * 属性对应数据库表字段名称-排序值
//  */
// GtableRankName = "index" //"GRank"

// /**
//  * 属性对应数据库表字段名称-版本号
//  */
// GtableVersionName = "edition" //"GVersion"

// /**
//  * 属性对应数据库表字段名称-逻辑删除标识
//  */
// GtableDelSignName = "deleted" //"GiDeleted"

// /**
//  * 属性对应数据库表字段名称-步骤值
//  */
// GtableSetpName = "setp" //"GSetp"

// /**
//  * 属性对应数据库表字段名称-唯一标识
//  */
// GtableOnlyignName = "onlyign" //"GsOnlyign"

// /**
//  * 属性对应数据库树型表根节点默认值
//  */
// GtableTreeRootValue = "00"

// /**
//  * 属性对应基本数据库库名
//  */
// GBaseSystemName = "base_system" //"BaseSystem"

// /**
//  * 属性对应字典表表名
//  */
// GtableNameDictionary = "dictionary" //"Dictionary"

// /**
//  * 属性对应字典表值字段名
//  */
// GtableDictionaryValueName = "value" //"GValue"

// /**
//  * 属性对应记录验证串字段名
//  */
// GtableRecordKeyName = "record_key" //"RecordKey"

// /**
//  * 属性对应标识字段名
//  */
// GtableSign = "sign" //"GSign"

// /**
//   - 属性对应备注字段名
//     */
//
// GtableMemo = "memo" //"GMemo"
)

// package model

// type GlobalConstant struct{}

// var (
// 	DbNamedRules = "common" //common 通用数据库命名格式定义;bricks 自定义的格式

// 	/**
// 	 * 数据库表主键名称(字符串形式)
// 	 */
// 	TableMajorKeyString = ifelse(DbNamedRules == "bricks", "Id", "s_id")

// 	/**
// 	 * 数据库表主键名称(自增长形式)
// 	 */
// 	TableMajorKeyAutoInt = ifelse(DbNamedRules == "bricks", "Id", "i_id")

// 	/**
// 	 * 数据库表主键名称(UUID形式)
// 	 */
// 	TableMajorKeyUId = ifelse(DbNamedRules == "bricks", "Id", "u_id")

// 	/**
// 	 * 数据库表字段名称-上级字段名
// 	 */
// 	TablePidKey = ifelse(DbNamedRules == "bricks", "Pid", "s_pid")

// 	/**
// 	 * 数据库表字段名称-主键路径名
// 	 */
// 	TablePathKey = ifelse(DbNamedRules == "bricks", "Path", "s_path")

// 	/**
// 	 * 数据库表字段名称-树形节点名
// 	 */
// 	TableTreeNodeName = ifelse(DbNamedRules == "bricks", "Name", "s_name")

// 	/**
// 	 * 数据库表字段名称-创建人
// 	 */
// 	TableCreatorName = ifelse(DbNamedRules == "bricks", "Creator", "s_creator")

// 	/**
// 	 * 数据库表字段名称-创建时间
// 	 */
// 	TableCreateDateName = ifelse(DbNamedRules == "bricks", "CreateDate", "d_creator_date")

// 	/**
// 	 * 数据库表字段名称-修改人
// 	 */
// 	TableModifiederName = ifelse(DbNamedRules == "bricks", "Modifieder", "s_modifieder")

// 	/**
// 	 * 数据库表字段名称-修改时间
// 	 */
// 	TableModifiedDateName = ifelse(DbNamedRules == "bricks", "ModifiedDate", "d_modified_date")

// 	/**
// 	 * 数据库表字段名称-状态值
// 	 */
// 	TableState = ifelse(DbNamedRules == "bricks", "State", "i_state")

// 	/**
// 	 * 数据库表字段名称-排序值
// 	 */
// 	TableRrankName = ifelse(DbNamedRules == "bricks", "Rank", "i_index")

// 	/**
// 	 * 数据库表字段名称-版本号
// 	 */
// 	TableEdition = ifelse(DbNamedRules == "bricks", "Version", "i_edition")

// 	/**
// 	 * 数据库表字段名称-步骤值
// 	 */
// 	TableSetpName = ifelse(DbNamedRules == "bricks", "Setp", "i_setp")

// 	/**
// 	 * 数据库表字段名称-唯一标识
// 	 */
// 	TableOnlyignName = ifelse(DbNamedRules == "bricks", "sOnlyign", "s_onlyign")

// 	/**
// 	 * 字典表表名
// 	 */
// 	TableNameDictionary = ifelse(DbNamedRules == "bricks", "Dictionary", "dictionary")

// 	/**
// 	 * 字典表值字段名
// 	 */
// 	TableDictionaryValueName = ifelse(DbNamedRules == "bricks", "Value", "s_value")

// 	/**
// 	 * 记录验证串字段名
// 	 */
// 	TableRecordKeyName = ifelse(DbNamedRules == "bricks", "sRecordKey", "s_record_key")

// 	/**
// 	 * 标识字段名
// 	 */
// 	TableSign = ifelse(DbNamedRules == "bricks", "Sign", "s_sign")

// 	/**
// 	 * 备注字段名
// 	 */
// 	TableMemo = ifelse(DbNamedRules == "bricks", "Memo", "s_memo")
// )

// const (

// 	/**
// 	 * 数据库树型表根节点默认值
// 	 */
// 	TableTreeRootValue = "00"

// 	/**
// 	 * 基本数据库库名
// 	 */
// 	BaseSystemName = "BaseSystem"

// 	/**
// 	 * 属性对应数据库表主键名称(字符串形式)
// 	 */
// 	GtableMajorKeyString = "Id"

// 	/**
// 	 * 属性对应数据库表主键名称(自增长形式)
// 	 */
// 	GtableMajorKeyAutoInt = "Id"

// 	/**
// 	 * 属性对应数据库表主键名称(UUID形式)
// 	 */
// 	GtableMajorKeyUId = "Id"

// 	/**
// 	 * 属性对应数据库表字段名称-上级字段名
// 	 */
// 	GtablePidKey = "Pid"

// 	/**
// 	 * 属性对应数据库表字段名称-主键路径名
// 	 */
// 	GtablePathKey = "Path"

// 	/**
// 	 * 属性对应数据库表字段名称-树形节点名
// 	 */
// 	GtableTreeNodeName = "Name"

// 	/**
// 	 * 属性对应数据库表字段名称-创建人
// 	 */
// 	GtableCreatorName = "Creator"

// 	/**
// 	 * 属性对应数据库表字段名称-创建时间
// 	 */
// 	GtableCreateDateName = "CreateDate"

// 	/**
// 	 * 属性对应数据库表字段名称-修改人
// 	 */
// 	GtableModifiederName = "Modifieder"

// 	/**
// 	 * 属性对应数据库表字段名称-修改时间
// 	 */
// 	GtableModifiedDateName = "ModifiedDate"

// 	/**
// 	 * 属性对应数据库表字段名称-状态值
// 	 */
// 	GtableStateName = "State"

// 	/**
// 	 * 属性对应数据库表字段名称-排序值
// 	 */
// 	GtableRankName = "GRank"

// 	/**
// 	 * 属性对应数据库表字段名称-版本号
// 	 */
// 	GtableVersionName = "GVersion"

// 	/**
// 	 * 属性对应数据库表字段名称-步骤值
// 	 */
// 	GtableSetpName = "GSetp"

// 	/**
// 	 * 属性对应数据库表字段名称-唯一标识
// 	 */
// 	GtableOnlyignName = "GsOnlyign"

// 	/**
// 	 * 属性对应数据库树型表根节点默认值
// 	 */
// 	GtableTreeRootValue = "00"

// 	/**
// 	 * 属性对应基本数据库库名
// 	 */
// 	GBaseSystemName = "BaseSystem"

// 	/**
// 	 * 属性对应字典表表名
// 	 */
// 	GtableNameDictionary = "Dictionary"

// 	/**
// 	 * 属性对应字典表值字段名
// 	 */
// 	GtableDictionaryValueName = "GValue"

// 	/**
// 	 * 属性对应记录验证串字段名
// 	 */
// 	GtableRecordKeyName = "RecordKey"

// 	/**
// 	 * 属性对应标识字段名
// 	 */
// 	GtableSign = "GSign"

//	/**
//	* 属性对应备注字段名
//	 */
//	GtableMemo = "GMemo"
// )

// // 三目运算代替品
// func ifelse(bl bool, a, b string) string {
// 	if bl {
// 		return a
// 	}

// 	return b
// }
