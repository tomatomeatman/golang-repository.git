package set

//键值对结构体
type Set struct {
	Key   string      `json:"key"`
	Value interface{} `json:"value"`
}
