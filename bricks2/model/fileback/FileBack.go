package fileback

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
)

// 文件结构体,用于http返回传递消息
type FileBack struct {
	Success     bool        `json:"success"`
	Msg         string      `json:"msg"`
	Data        interface{} `json:"data"` //[]byte
	Disposition string      `json:"disposition"`
	Type        string      `json:"type"`
	Length      int         `json:"length"`
	LengthStr   string      `json:"lengthStr"`
}

/**
 * 构造消息结构体,返回包含'错误信息'的结构体
 * @param data 码值
 * @param msg 描述信息
 * @return 返回新创建的结构体
 */
func Err(data int, msg ...interface{}) *FileBack {
	if len(msg) == 0 {
		return &FileBack{false, "", data, "", "", 0, "0"}
	}

	var build strings.Builder
	for _, v := range msg {
		build.WriteString(fmt.Sprintf("%v", v))
	}

	return &FileBack{false, build.String(), data, "", "", 0, "0"}
}

/**
 * 返回包含'错误信息'的结构体Json字符串
 * @param data 码值
 * @param msg 描述信息
 * @return 返回Json字符串
 */
func ErrString(data int, msg ...interface{}) string {
	var build strings.Builder
	for _, v := range msg {
		build.WriteString(fmt.Sprintf("%v", v))
	}

	return Err(data, build.String()).ToStr()
}

/**
 * 构造消息结构体,返回包含'正确信息'的结构体
 * @param data 码值|数据
 * @param msg 描述信息
 * @return 返回新创建的结构体
 */
func Success(data interface{}, disposition, sType string, msg ...interface{}) *FileBack {
	if len(msg) == 0 {
		iL := len(data.([]byte))
		return &FileBack{true, "", data, disposition, sType, iL, strconv.Itoa(iL)}
	}

	var build strings.Builder
	for _, v := range msg {
		build.WriteString(fmt.Sprintf("%v", v))
	}

	iL := len(data.([]byte))
	return &FileBack{true, build.String(), data, disposition, sType, iL, strconv.Itoa(iL)}
}

/**
 * 重设'返回数据'
 * data 码值|数据
 * @return 返回原结构体 m
 */
func (fm *FileBack) SetData(data interface{}) *FileBack {
	fm.Data = data

	return fm
}

/**
 * 重设'描述信息'
 * msg 描述信息
 * @return 返回原结构体 m
 */
func (fm *FileBack) SetMsg(msg ...interface{}) *FileBack {
	result := fm
	if len(msg) == 0 {
		return result
	}

	var build strings.Builder
	for _, v := range msg {
		build.WriteString(fmt.Sprintf("%v", v))
	}

	result.Msg = build.String()

	return result
}

/**
 * 累加'码值'
 * iCode 被累加值
 * @return 返回原结构体 m
 */
func (fm *FileBack) IncCode(iCode int) *FileBack {
	result := fm

	if result.Success { //成功情况下不允许累加值
		return fm
	}

	iData, err := strconv.Atoi(fmt.Sprintf("%v", result.Data)) //防止返回的类型未float64
	if err != nil {
		iData = 0
	}

	result.Data = iData + iCode

	return result
}

/**
 * 累加'码值'
 * iCode 被累加值
 * @return 返回原结构体 m
 */
func (fm *FileBack) IncData(iCode int) int {
	result := fm

	if result.Success { //成功情况下不允许累加值
		return 0
	}

	if iCode == 0 {
		return fm.Data.(int)
	}

	iData, err := strconv.Atoi(fmt.Sprintf("%v", fm.Data)) //防止返回的类型未float64
	if err != nil {
		iData = 0
	}

	return iData + iCode
}

/**
 * 在描述信息后面累加'描述信息'
 * msg 描述信息
 * @return 返回原结构体 FileBack
 */
func (fm *FileBack) AppendMsg(msg ...interface{}) *FileBack {
	result := fm

	var build strings.Builder
	build.WriteString(result.Msg)

	for _, v := range msg {
		build.WriteString(fmt.Sprintf("%v", v))
	}

	result.Msg = build.String()

	return result
}

/**
 * 在描述信息前面插入'描述信息'
 * msg 描述信息
 * @return 返回原结构体 FileBack
 */
func (fm *FileBack) InsertMsg(msg ...interface{}) *FileBack {
	result := fm
	if len(msg) == 0 {
		return result
	}

	var build strings.Builder

	for _, v := range msg {
		build.WriteString(fmt.Sprintf("%v", v))
	}

	build.WriteString(result.Msg)

	result.Msg = build.String()

	return result
}

/**
 * 将结构体转换成json字符串输出
 * @return 返回json结构字符串
 */
func (fm *FileBack) ToStr() string {
	ret_json, _ := json.Marshal(fm)

	return string(ret_json)
}
