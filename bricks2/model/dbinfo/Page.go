package dbinfo

//分页数据信息对象,常用于前端与后台列表数据交互
type Page struct {
	Current   int                      `json:"current"`   //当前页
	Size      int                      `json:"size"`      //每页显示记录
	CountRow  int                      `json:"countRow"`  //总记录数
	CountPage int                      `json:"countPage"` //总页数
	Record    []map[string]interface{} `json:"record"`    //本页的数据列表
}

//创建分页信息
func (Page) New() *Page {
	result := Page{}

	result.Current = 1
	result.Size = 10
	result.CountRow = 0
	result.CountPage = 0

	return &result
}

//设置分页信息
func (p *Page) Set(start, size, countRow int, record []map[string]interface{}) *Page {
	if start < 1 {
		start = 0
	}

	if size < 1 {
		size = 10
	}

	if countRow < 0 {
		countRow = 0
	}

	current := start / size

	countPage := 1
	if (countRow > 0) && (size > 0) {
		if countRow%size == 0 {
			countPage = countRow / size
		} else {
			countPage = countRow/size + 1
		}
	}

	p.Current = current
	p.Size = size
	p.CountRow = countRow
	p.CountPage = countPage
	p.Record = record

	return p
}

//设置数据列表
func (p *Page) SetRecord(record []map[string]interface{}) *Page {
	p.Record = record
	return p
}
