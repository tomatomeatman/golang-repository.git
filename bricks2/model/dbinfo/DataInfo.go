package dbinfo

// 数据信息注解
type DataInfo struct {
	Name          string      `json:"name"`          //对象名
	IsDbField     bool        `json:"isDbField"`     //是否数据库字段
	Field         string      `json:"field"`         //字段名称
	DbName        string      `json:"dbName"`        //所在数据库名称
	DbObj         string      `json:"dbObj"`         //数据库名称别名,对应数据库全局配置名
	TableName     string      `json:"tableName"`     //所在数据库表表名称
	KeyName       string      `json:"keyName"`       //表主键名称
	Index         int         `json:"index"`         //序号
	MaxLength     int         `json:"maxLength"`     //最大长度
	IsNull        bool        `json:"isNull"`        //是否允许为空
	IsKey         bool        `json:"isKey"`         //是否主键
	IsExtra       bool        `json:"isExtra"`       //是否自增
	IsBigTxt      bool        `json:"isBigTxt"`      //是否大文本字段
	IsDecimal     bool        `json:"isDecimal"`     //是否浮点数字段
	DefaultData   string      `json:"defaultData"`   //默认值
	GoDefaultData interface{} `json:"goDefaultData"` //默认值(Go语言用)
	Comment       string      `json:"comment"`       //字段备注
	HasDecimal    bool        `json:"hasDecimal"`    //类型是否有小数
	IntLength     int         `json:"intLength"`     //整数位的长度
	DecimalLength int         `json:"decimalLength"` //小数位的长度
	DbFileType    string      `json:"dbFileType"`    //字段在数据库中的类型
	RelTitle      string      `json:"relTitle"`      //关联后显示的名称
	RelName       string      `json:"relName"`       //关联关系中被动关联的字段名,如 (LEFT JOIN RelTable ON RelTable.A = MainTable.B )中的A
	RelMainName   string      `json:"relMainName"`   //关联关系中被关联的字段名,如 (LEFT JOIN RelTable ON RelTable.A = MainTable.B )中的B
}

// 创建结构体,并设置默认值
func (DataInfo) New() *DataInfo {
	result := DataInfo{}

	result.Name = ""           //对象名
	result.IsDbField = false   //是否数据库字段
	result.Field = ""          //字段名称
	result.DbName = ""         //所在数据库名称
	result.DbObj = ""          //数据库名称别名,对应数据库全局配置名
	result.TableName = ""      //所在数据库表表名称
	result.KeyName = ""        //表主键名称
	result.Index = 0           //序号
	result.MaxLength = 1       //最大长度
	result.IsNull = true       //是否允许为空
	result.IsKey = false       //是否主键
	result.IsExtra = false     //是否自增
	result.IsBigTxt = false    //是否大文本字段
	result.IsDecimal = false   //是否浮点数字段
	result.DefaultData = ""    //默认值
	result.GoDefaultData = nil //默认值
	result.Comment = ""        //字段备注
	result.HasDecimal = false  //类型是否有小数
	result.IntLength = 0       //整数位的长度
	result.DecimalLength = 0   //小数位的长度
	result.DbFileType = ""     //字段在数据库中的类型
	result.RelTitle = ""       //关联后显示的名称
	result.RelName = ""        //关联关系中被动关联的字段名,如 (LEFT JOIN RelTable ON RelTable.A = MainTable.B )中的A
	result.RelMainName = ""    //关联关系中被关联的字段名,如 (LEFT JOIN RelTable ON RelTable.A = MainTable.B )中的B

	return &result
}
