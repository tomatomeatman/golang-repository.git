package dbinfo

import "strings"

// 排序信息
type OrderInfo struct {
	OrderDbName string `json:"orderDbName"` //排序字段名所属库名
	OrderTable  string `json:"orderTable"`  //排序字段名所属表
	OrderField  string `json:"orderField"`  //排序字段名
	Direction   string `json:"direction"`   //排序方式(升序ASC 降序DESC)
	Rank        int    `json:"rank"`        //排序顺序
}

// 要被过滤的特殊字符串,用'/'分隔
const SPECIAL_STR = "/INTEGER/STRING/INT/NULL/LONG/FLOAT/DOUBLE/FUNCTION/" // /Integer/string/int/null/long/float/double/function/

/**
 * 设置 排序字段名所属库名
 * @param OrderDbName 排序字段名所属库名
 */
func (o *OrderInfo) SetOrderDbName(OrderDbName string) {
	OrderDbName = strings.TrimSpace(OrderDbName)
	if OrderDbName == "" {
		return
	}

	if strings.Contains(SPECIAL_STR, "/"+strings.ToUpper(OrderDbName)+"/") { //排除一些不可能使用的字符串，如在使用swagger操作时，默认赋值string
		return
	}

	o.OrderDbName = OrderDbName
}

/**
 * 设置 排序字段名所属表
 * @param OrderTable 排序字段名所属表
 */
func (o *OrderInfo) SetOrderTable(OrderTable string) {
	OrderTable = strings.TrimSpace(OrderTable)
	if OrderTable == "" {
		return
	}

	if strings.Contains(SPECIAL_STR, "/"+strings.ToUpper(OrderTable)+"/") { //排除一些不可能使用的字符串，如在使用swagger操作时，默认赋值string
		return
	}

	o.OrderTable = OrderTable
}

/**
 * 设置 排序字段名
 * @param OrderField 排序字段名
 */
func (o *OrderInfo) SetOrderField(OrderField string) {
	OrderField = strings.TrimSpace(OrderField)
	if OrderField == "" {
		return
	}

	if strings.Contains(SPECIAL_STR, "/"+strings.ToUpper(OrderField)+"/") { //排除一些不可能使用的字符串，如在使用swagger操作时，默认赋值string
		return
	}

	o.OrderField = OrderField
}

/**
 * 设置 排序方式(升序ASC 降序DESC)
 * @param Direction 排序方式(升序ASC 降序DESC)
 */
func (o *OrderInfo) SetDirection(Direction string) {
	Direction = strings.TrimSpace(Direction)
	if Direction == "" {
		return
	}

	if (strings.ToUpper(Direction) != "ASC") && (strings.ToUpper(Direction) != "DESC") { //排除一些不可能使用的字符串，如在使用swagger操作时，默认赋值string
		return
	}

	o.Direction = Direction
}

/**
 * 输出排序字符串
 */
func (o *OrderInfo) String() string {
	if o.OrderField == "" {
		return "" //如果字段为空则直接为空
	}

	var build strings.Builder

	if (o.OrderDbName != "") && (o.OrderTable != "") {
		build.WriteString(o.OrderDbName)
		build.WriteString(".")
	}

	if o.OrderTable != "" {
		build.WriteString(o.OrderTable)
		build.WriteString(".")
	}

	build.WriteString(o.OrderField)

	if o.Direction != "" {
		build.WriteString(" ")
		build.WriteString(o.Direction)
	} else {
		build.WriteString(" ASC")
	}

	return build.String()
}
