package dbinfo

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"sync"
	"time"
	"unicode/utf8"

	"github.com/shopspring/decimal"
	"gopkg.in/ini.v1"
)

//const timeFormat = "2006-01-02 15:04:05"

var (
	entityWriteLock sync.Mutex                //保存锁
	entityMap       = make(map[string]Entity) //注册实体结构体集合
	// dbMap           = make(map[string]string) //注册数据库别名集合
	// tableNameMap    = make(map[string]string) //实体的TableName缓存,TableName用于在进行gorm查询时自定义表名需要

	// --  数据库字段名常量 -- //

	tableKeyNameMap      = map[string][]string{}
	BaseSystemDb         []string //基本数据库库名
	TableMajorKeyString  []string //数据库表主键名称(字符串形式)
	TableMajorKeyAutoInt []string //数据库表主键名称(自增长形式)
	TableMajorKeyUuId    []string //数据库表主键名称(UUID形式)
	TablePidKey          []string //数据库表字段名称-上级字段名
	TablePathKey         []string //数据库表字段名称-主键路径名
	TableTreeNode        []string //数据库表字段名称-树形节点名
	TableCreator         []string //数据库表字段名称-创建人
	TableCreateDate      []string //数据库表字段名称-创建时间
	TableModifieder      []string //数据库表字段名称-修改人
	TableModifiedDate    []string //数据库表字段名称-修改时间
	TableState           []string //数据库表字段名称-状态值
	TableRrank           []string //数据库表字段名称-排序值
	TableEdition         []string //数据库表字段名称-版本号
	TablePassword        []string //数据库表字段名称-密码
	TableDelSign         []string //数据库表字段名称-逻辑删除标识
	TableSetp            []string //数据库表字段名称-步骤值
	TableOnlyign         []string //数据库表字段名称-唯一标识
	TableNameDictionary  []string //字典表表名
	TableDictionaryValue []string //字典表值字段名
	TableRecordKey       []string //记录验证串字段名
	TableSign            []string //标识字段名
	TableMemo            []string //备注字段名

	TableTreeRootValue = []string{"00", "00"} //数据库树型表根节点默认值
)

// 实体接口定义,用于规范实体结构体
type Entity interface {
	New() Entity                       //创建结构实体
	BaseColumnNames() string           //结构体的属性名串
	BaseEntity() Entity                //取基础实体,用于在子类(嵌套结构体)时同样获得基类
	GetDataInfo(name string) *DataInfo //取数据结构信息
	TableName() string                 //结构体映射表名,当gorm进行查询时要明确与gorm规则不同的表名时使用
	OwnerName() string                 //结构体映射库名,去除'Dev_'等前缀
	OwnerTable() string                //结构体映射表名,无库名
}

func init() {
	initTableKeyName()
}

// --------- 注册实体结构体集合 开始 ---------//

/**
 * 注册实体结构体
 * @param name
 * @param entity
 */
func RegisterEntity(name string, entity Entity) Entity {
	entityWriteLock.Lock()         //加锁
	defer entityWriteLock.Unlock() //解锁

	entityMap[name] = entity

	return entity
}

/**
 * 取实体结构体
 * @param name
 * @return
 */
func GetEntity(name string) Entity {
	entityWriteLock.Lock()         //加锁
	defer entityWriteLock.Unlock() //解锁

	return entityMap[name]
}

// --------- 注册实体结构体集合 结束 ---------//

/**
 * 取结构体映射表的字段名串
 * @param entity
 * @return
 */
func BaseFieldNames(entity Entity) []string {
	array := strings.Split(entity.BaseColumnNames(), ",")

	result := []string{}
	for _, key := range array {
		result = append(result, entity.GetDataInfo(key).Field)
	}

	return result
}

/**
 * 取基础实体,用于在子类(嵌套结构体)时同样获得基类
 * @param entity
 * @return
 */
func BaseEntity(entity Entity) Entity {
	return entity.BaseEntity() //调用子类的实现方法
}

/**
 * 按名称取数据结构信息
 * @param entity
 * @param name
 * @return
 */
func GetDataInfo(entity Entity, name string) *DataInfo {
	return entity.GetDataInfo(name) //子类实现
}

/**
 * 按字段顺序取数据结构信息
 * @param entity
 * @param name
 * @return
 */
func GetDataInfoByIndex(entity Entity, index int) *DataInfo {
	if index < 0 {
		index = 0
	}

	array := strings.Split(entity.BaseColumnNames(), ",")
	if len(array) <= index {
		return nil
	}

	return entity.GetDataInfo(array[index]) //子类实现
}

/**
 * 判断是否存在字段名
 * @param entity
 * @param name
 * @return
 */
func HasColumnName(entity Entity, name string) bool {
	array := strings.Split(entity.BaseColumnNames(), ",")
	for _, v := range array {
		if v == name {
			return true
		}
	}

	return false
}

/**
 * 取所有数据结构信息
 * @param entity
 * @return
 */
func AllDataInfo(entity Entity) []*DataInfo {
	array := strings.Split(entity.BaseColumnNames(), ",")

	result := []*DataInfo{}
	for _, key := range array {
		result = append(result, entity.GetDataInfo(key))
	}

	return result
}

/**
 * 结构体映射表的字段名集合
 * @param entity
 * @return
 */
func BaseColumnNameList(entity Entity) []string {
	return strings.Split(entity.BaseColumnNames(), ",")
}

/**
 * 创建指定长度的结构实体集合
 * @param entity
 * @param size
 * @return
 */
func NewList(entity Entity, size int) []Entity {
	result := make([]Entity, size)
	for i := 0; i < size; i++ {
		result[i] = entity.New()
	}

	return result
}

/**
 * 设置默认值
 * @param entity
 * @param cover 是否覆盖
 */
func SetDefault(entity Entity, cover bool) {
	array := strings.Split(entity.BaseColumnNames(), ",") //结构体映射表的字段名串
	data := make(map[string]interface{})
	for _, name := range array {
		dataInfo := entity.GetDataInfo(name)
		if dataInfo.GoDefaultData == nil {
			continue
		}

		data[name] = dataInfo.GoDefaultData
	}

	pValue := reflect.ValueOf(entity) // 获取指针指向的值

	setFieldsVal(pValue, data, cover)
}

/**
 * 取主键数据结构信息
 * @param entity
 * @return
 */
func GetKeyDataInfo(entity Entity) []*DataInfo {
	array := strings.Split(entity.BaseColumnNames(), ",")

	result := []*DataInfo{}
	for _, key := range array {
		dataInfo := entity.GetDataInfo(key)
		if dataInfo.IsKey {
			result = append(result, dataInfo)
		}
	}

	return result
}

/**
 * 取所属数据库名
 * @param entity
 * @return
 */
func EntityDbName(entity Entity) string {
	array := strings.Split(entity.BaseColumnNames(), ",")

	var dataInfo *DataInfo
	for _, key := range array {
		dataInfo = entity.GetDataInfo(key)
		if dataInfo.IsKey {
			return dataInfo.DbName
		}
	}

	if dataInfo == nil {
		return ""
	}

	return dataInfo.DbName
}

/**
 * 取所属数据库表名
 * @param entity
 * @return
 */
func EntityTableName(entity Entity) string {
	array := strings.Split(entity.BaseColumnNames(), ",")

	var dataInfo *DataInfo
	for _, key := range array {
		dataInfo = entity.GetDataInfo(key)
		if dataInfo.IsKey {
			return dataInfo.TableName
		}
	}

	if dataInfo == nil {
		return ""
	}

	return dataInfo.TableName
}

/**
 * 取所属数据库表主键名
 * 注意:如果有多个主键,则取第一个主键
 * @param entity
 * @return
 */
func EntityKeyField(entity Entity) string {
	array := strings.Split(entity.BaseColumnNames(), ",")

	for _, key := range array {
		dataInfo := entity.GetDataInfo(key)
		if dataInfo.IsKey {
			return dataInfo.Field
		}
	}

	return ""
}

/**
 * 取所属数据库表主键名
 * 注意:如果有多个主键,则全取,并以','分隔
 * @param entity
 * @return
 */
func EntityKeyFields(entity Entity) string {
	array := strings.Split(entity.BaseColumnNames(), ",")

	result := ""
	for _, key := range array {
		dataInfo := entity.GetDataInfo(key)
		if dataInfo.IsKey {
			result += "," + dataInfo.Field
		}
	}

	return strings.TrimPrefix(result, ",") //去掉前缀
}

/**
 * 取所属数据库表主键对象名
 * 注意:如果有多个主键,则取第一个主键
 * @param entity
 * @return
 */
func EntityKeyName(entity Entity) string {
	array := strings.Split(entity.BaseColumnNames(), ",")

	for _, key := range array {
		dataInfo := entity.GetDataInfo(key)
		if dataInfo.IsKey {
			return dataInfo.Name
		}
	}

	return ""
}

/**
 * 取所属数据库表主键对象名
 * 注意:如果有多个主键,则全取,并以','分隔
 * @param entity
 * @return
 */
func EntityKeyNames(entity Entity) string {
	array := strings.Split(entity.BaseColumnNames(), ",")

	result := ""
	for _, key := range array {
		dataInfo := entity.GetDataInfo(key)
		if dataInfo.IsKey {
			result += "," + dataInfo.Name
		}
	}

	return strings.TrimPrefix(result, ",") //去掉前缀
}

/**
 * 取所属数据库表主键长度
 * 注意:如果有多个主键,则取第一个主键
 * @param entity
 * @return
 */
func EntityKeyLength(entity Entity) int {
	array := strings.Split(entity.BaseColumnNames(), ",")

	for _, key := range array {
		dataInfo := entity.GetDataInfo(key)
		if dataInfo.IsKey {
			return dataInfo.MaxLength
		}
	}

	return 8
}

/**
 * 取所属数据库表主键长度
 * 注意:如果有多个主键,则全取
 * @param entity
 * @return
 */
func EntityKeysLength(entity Entity) map[string]int {
	array := strings.Split(entity.BaseColumnNames(), ",")

	result := make(map[string]int, 0)
	for _, key := range array {
		dataInfo := entity.GetDataInfo(key)
		if dataInfo.IsKey {
			result[dataInfo.Name] = dataInfo.MaxLength
		}
	}

	return result
}

// ----------- 特殊字段是否存在判断 开始 ----------- //

/**
 * 是否存在自增主键
 * @param entity
 * @return
 */
func EntityAutoKey(entity Entity) bool {
	array := strings.Split(entity.BaseColumnNames(), ",")

	for _, key := range array {
		dataInfo := entity.GetDataInfo(key)
		if dataInfo.IsExtra && dataInfo.IsKey {
			return true
		}
	}

	return false
}

/**
 * 是否存在Pid
 * @param entity
 * @return
 */
func EntityHasPid(entity Entity) bool {
	names := "," + entity.BaseColumnNames() + ","

	return strings.Contains(names, ","+TablePidKey[1]+",")
}

/**
 * 是否存在Path
 * @param entity
 * @return
 */
func EntityHasPath(entity Entity) bool {
	names := "," + entity.BaseColumnNames() + ","

	return strings.Contains(names, ","+TablePathKey[1]+",")
}

/**
 * 是否存在sRecordKey
 * @param entity
 * @return
 */
func EntityHasRecordKey(entity Entity) bool {
	names := "," + entity.BaseColumnNames() + ","

	return strings.Contains(names, ","+TableRecordKey[1]+",")
}

/**
 * 是否存在Memo
 * @param entity
 * @return
 */
func EntityHasMemo(entity Entity) bool {
	names := "," + entity.BaseColumnNames() + ","

	return strings.Contains(names, ","+TableMemo[1]+",")
}

/**
 * 是否存在Creator
 * @param entity
 * @return
 */
func EntityHasCreator(entity Entity) bool {
	names := "," + entity.BaseColumnNames() + ","

	return strings.Contains(names, ","+TableCreator[1]+",")
}

/**
 * 是否存在CreateDate
 * @param entity
 * @return
 */
func EntityHasCreateDate(entity Entity) bool {
	names := "," + entity.BaseColumnNames() + ","

	return strings.Contains(names, ","+TableCreateDate[1]+",")
}

/**
 * 是否存在Modifieder
 * @param entity
 * @return
 */
func EntityHasModifieder(entity Entity) bool {
	names := "," + entity.BaseColumnNames() + ","

	return strings.Contains(names, ","+TableModifieder[1]+",")
}

/**
 * 是否存在ModifiedDate
 * @param entity
 * @return
 */
func EntityHasModifiedDate(entity Entity) bool {
	names := "," + entity.BaseColumnNames() + ","

	return strings.Contains(names, ","+TableModifiedDate[1]+",")
}

/**
 * 是否存在State
 * @param entity
 * @return
 */
func EntityHasState(entity Entity) bool {
	names := "," + entity.BaseColumnNames() + ","

	return strings.Contains(names, ","+TableState[1]+",")
}

/**
 * 是否存在Rank
 * @param entity
 * @return
 */
func EntityHasRank(entity Entity) bool {
	names := "," + entity.BaseColumnNames() + ","

	return strings.Contains(names, ","+TableRrank[1]+",")
}

/**
 * 是否存在Edition
 * @param entity
 * @return
 */
func EntityHasEdition(entity Entity) bool {
	names := "," + entity.BaseColumnNames() + ","

	return strings.Contains(names, ","+TableEdition[1]+",")
}

/**
 * 是否存在password
 * @param entity
 * @return
 */
func EntityHasPassword(entity Entity) bool {
	names := "," + entity.BaseColumnNames() + ","

	return strings.Contains(names, ","+TablePassword[1]+",")
}

/**
 * 是否存在sign
 * @param entity
 * @return
 */
func EntityHasSign(entity Entity) bool {
	names := "," + entity.BaseColumnNames() + ","

	return strings.Contains(names, ","+TableSign[1]+",")
}

/**
 * 是否存在sOnlyign
 * @param entity
 * @return
 */
func EntityHasOnlyign(entity Entity) bool {
	names := "," + entity.BaseColumnNames() + ","

	return strings.Contains(names, ","+TableOnlyign[1]+",")
}

/**
 * 是否存在iDelSign
 * @param entity
 * @return
 */
func EntityHasDelSign(entity Entity) bool {
	names := "," + entity.BaseColumnNames() + ","

	return strings.Contains(names, ","+TableDelSign[1]+",")
}

/**
 * 大文本字段名称集合
 * @param entity
 * @return
 */
func BigTextFields(entity Entity) []string {
	array := strings.Split(entity.BaseColumnNames(), ",")

	result := make([]string, 0)
	for _, key := range array {
		dataInfo := entity.GetDataInfo(key)
		if dataInfo.IsBigTxt {
			result = append(result, dataInfo.Name)
		}
	}

	return result
}

// ----------- 特殊字段是否存在判断 结束 ----------- //

// ---------- 去除字符类型属性前后空格 开始 -------- //

/**
 * 去除字符类型属性前后空格
 */
func TrimFields(entity Entity) {
	v := reflect.ValueOf(entity).Elem()

	trimStringFields(v) // 调用函数清除字符串字段的前后空格
}

/**
 * 通用函数，用于递归处理结构体及其匿名字段中的字符串
 * @param v 结构体或匿名结构体
 */
func trimStringFields(v reflect.Value) {
	for i := 0; i < v.NumField(); i++ {
		field := v.Field(i)
		fieldType := v.Type().Field(i)

		// 检查字段是否为匿名结构体
		if fieldType.Anonymous {
			trimStringFields(field)
			continue
		}

		if field.Kind() == reflect.String {
			field.SetString(strings.TrimSpace(field.String())) // 清除前后空格
			continue
		}

		if field.Kind() == reflect.Struct {
			trimStringFields(field) // 如果字段是结构体，则递归调用trimStringFields
			continue
		}
	}
}

// ---------- 去除字符类型属性前后空格 结束 -------- //

/*
 * 创建记录验证值
 * @param entity
 * @return
 */
func CreateRecordKey(entity Entity) string {
	pValue := reflect.ValueOf(entity) // 获取指针指向的值

	array := strings.Split(entity.BaseColumnNames(), ",")
	valMap := getFieldsVal(pValue) // 递归遍历结构体的所有字段

	temp := ""
	for _, k := range array { //只有数组才能固定顺序
		if k == TableRecordKey[1] { //recordKey字段不能参与,并且一旦轮询到recordKey就结束
			break
		}

		v, ok := valMap[k]
		if !ok {
			continue
		}

		if v == nil {
			temp = temp + ";"
			continue
		}

		switch reflect.TypeOf(v).String() {
		case "time.Time":
			temp = temp + v.(time.Time).Format(timeFormat) + ";"
		default:
			temp = temp + fmt.Sprintf("%v", v) + ";"
		}
	}

	data := []byte(temp)
	md5Ctx := md5.New()
	md5Ctx.Write(data)
	cipherStr := md5Ctx.Sum(nil)
	result := hex.EncodeToString(cipherStr)

	return strings.ToUpper(result)
}

/*
 * 根据结构体创建map的记录验证值
 * @param entity 参照结构体,仅用于取结构
 * @param data 数据
 * @return
 */
func CreateRecordKeyByMap(entity Entity, data map[string]interface{}) string {
	array := strings.Split(entity.BaseColumnNames(), ",") //关键就是按顺序进行

	temp := ""
	for _, k := range array { //只有数组才能固定顺序
		if k == TableRecordKey[1] { //recordKey字段不能参与,并且一旦轮询到recordKey就结束
			break
		}

		v, ok := data[k]
		if !ok {
			temp = temp + ";"
			continue
		}

		if v == nil {
			temp = temp + ";"
			continue
		}

		switch reflect.TypeOf(v).String() {
		case "time.Time":
			temp = temp + v.(time.Time).Format(timeFormat) + ";"
		default:
			temp = temp + fmt.Sprintf("%v", v) + ";"
		}
	}

	b := []byte(temp)
	md5Ctx := md5.New()
	md5Ctx.Write(b)
	cipherStr := md5Ctx.Sum(nil)
	result := hex.EncodeToString(cipherStr)

	return strings.ToUpper(result)
}

/**
 * 验证'记录验证串'是否正确
 * @param entity
 * @return
 */
func CheckRecordKey(entity Entity) bool {
	nowKey := CreateRecordKey(entity)

	v := reflect.ValueOf(entity)
	oldKey := v.FieldByName(TableRecordKey[1]).Interface().(string)

	return oldKey == nowKey
}

/**
 * 设置当前实体的'记录验证串'值
 * @param entity
 * @return 返回新值
 */
func SetRecordKey(entity Entity) string {
	nowKey := CreateRecordKey(entity)

	v := reflect.ValueOf(entity).Elem()
	v.FieldByName(TableRecordKey[1]).SetString(nowKey)

	return nowKey
}

// /* 相对于每个表中定义一个变量来保存数据库表名来说性能太差
//  * 结构体映射表名,处理结构体名称与表名不一致的情况
//  * @return
//  */
// func TableName(entity Entity) string {
// 	array := strings.Split(entity.BaseColumnNames(), ",")

// 	var dbName, tableName string
// 	var dataInfo *DataInfo
// 	for _, key := range array {
// 		dataInfo = entity.GetDataInfo(key)
// 		if dataInfo.IsKey {
// 			dbName = dataInfo.DbName
// 			tableName = dataInfo.TableName
// 			break
// 		}
// 	}

// 	if dataInfo != nil && tableName == "" {
// 		dbName = dataInfo.DbName
// 		tableName = dataInfo.TableName
// 	}

// 	id := dbName + "." + tableName

// 	name := GetTableNameById(id)
// 	if name != "" {
// 		return name
// 	}

// 	if dbName == "" {
// 		return tableName
// 	}

// 	dbName = dbMap[dbName] //从注册数据库别名集合中获取到实际的库名
// 	if dbName == "" {
// 		return id //如果没有别名,则直接返回id
// 	}

// 	return dbName + tableName //别名中存在'.'
// }

/*
 * 结构体简略表名(无库名)
 * @return
 */
func SimpleTableName(entity Entity) string {
	return EntityTableName(entity)
}

/**
 * 取数据值
 * @param entity
 * @param name 字段名
 * @return
 */
func GetVal(entity Entity, name string) interface{} {
	// // 使用反射获取结构体类型和值
	// entityValue := reflect.ValueOf(entity).Elem() // 获取指针指向的值
	// entityType := entityValue.Type()

	// // 遍历结构体的所有字段
	// for i := 0; i < entityType.NumField(); i++ {
	// 	fieldType := entityType.Field(i)
	// 	fieldValue := entityValue.Field(i)

	// 	jsonTag := fieldType.Tag.Get("json") // 获取json标签

	// 	// 检查是否有特定的json标签
	// 	if jsonTag == name {
	// 		return fieldValue.Interface()
	// 	}
	// }

	// return nil

	pValue := reflect.ValueOf(entity)
	return getFieldVal(pValue, name)
}

/**
 * 设置数据值(此函数用于未知结构体的情况下调用)
 * @param name 字段名
 * @param val 值
 * @return
 */
func SetVal(entity Entity, name string, val interface{}) {
	pValue := reflect.ValueOf(entity)
	setFieldVal(pValue, name, val)
}

/**
 * 将map转成实体
 * data 数据
 * entity 数据结构
 */
func ToEntity(data map[string]interface{}, entity Entity) (Entity, int, error) {
	if data == nil {
		return nil, 1001, errors.New("map为nil")
	}

	if entity == nil {
		return nil, 1002, errors.New("数据结构为nil")
	}

	SetDefault(entity, true) //补齐默认值

	if !EntityHasRecordKey(entity) { //如果没有sRecordKey字段则直接结束
		MapSetEntity(data, entity) //Map设置结构体
		return entity, 1999, nil
	}

	MapSetEntity(data, entity) //Map设置结构体

	sRecordKey := CreateRecordKey(entity)
	SetVal(entity, TableRecordKey[1], sRecordKey)

	return entity, 1999, nil
}

// Map设置结构体
func MapSetEntity(data map[string]interface{}, entity Entity) Entity {
	pValue := reflect.ValueOf(entity) // 获取指针指向的值
	setFieldsVal(pValue, data, true)  //设置值

	return entity
}

// ----------- Map转结构体 开始 ----------- //

// 将map数据转换为实体
func MapToEntity(data map[string]interface{}, entity Entity) Entity {
	result := mapToEntity(data, entity) // mapToStruct 将map数据转换为指定的结构体实例

	return result.(Entity)
}

// mapToStruct 将map数据转换为指定的结构体实例
func mapToEntity(data map[string]interface{}, target interface{}) interface{} {
	targetValue := reflect.ValueOf(target)
	if targetValue.Kind() == reflect.Ptr { //传入的是指针如:&User{}
		//v = v.Elem() // 获取指针指向的值
	} else { //传入的是实体,如User{}
		// 获取目标结构体的类型
		targetType := reflect.TypeOf(target)
		targetPtrType := reflect.PtrTo(targetType)

		// 创建一个新的指针类型
		newTarget := reflect.New(targetPtrType.Elem()).Interface()

		// 将原始值复制到新指针中
		reflect.ValueOf(newTarget).Elem().Set(reflect.ValueOf(target))

		// 重新设置目标为指针类型
		target = newTarget
		targetValue = reflect.ValueOf(target)
	}

	v := targetValue.Elem() // 获取目标结构体的元素值

	for key, val := range data {
		setField(v, key, val)
	}

	if targetValue.Kind() == reflect.Ptr { //如果参数本身就是指针,则参数已经被修改,直接返回参数即可
		return target
	}

	result := v.Interface() //参数不是指针,属于内部结构体,需要返回指针

	return &result
}

// setField 遍历结构体并设置字段值，如果字段名与key相同，则设置为val
func setField(v reflect.Value, key string, val interface{}) {
	// 检查是否为结构体
	if v.Kind() != reflect.Struct {
		return
	}

	for i := 0; i < v.NumField(); i++ {
		field := v.Field(i)

		fieldType := v.Type().Field(i)

		// 获取字段的json标签
		jsonTag := fieldType.Tag.Get("json")
		if jsonTag == "" {
			continue
		}

		// 解析json标签
		parts := strings.Split(jsonTag, ",")
		if len(parts) > 0 && parts[0] == key {
			// 尝试将 val 转换为字段类型
			canSetValue(field, val)
			// if _, ok := canSetValue(field, val); ok { //有可能其它嵌套结构体也有同名字段
			// field.Set(reflect.ValueOf(cVal))
			// }
		}

		// 递归处理嵌套结构体或指针
		if field.Kind() == reflect.Ptr && !field.IsNil() {
			elem := field.Elem()
			if elem.Kind() == reflect.Struct {
				setField(elem, key, val)
			}

			continue
		}

		if field.Kind() == reflect.Struct {
			setField(field, key, val)
			continue
		}

		if field.Kind() == reflect.Interface { // 处理 interface{} 类型
			if field.IsNil() && (jsonTag == key) { // 如果类型为interface{}的字段当前值为nil,且json键值与map键值相符,则赋值,否则无法赋值
				field.Set(reflect.ValueOf(val))
				continue
			}

			handleInterface(field, key, val)
			continue
		}
	}
}

// handleInterface 处理 interface{} 类型的字段
func handleInterface(v reflect.Value, key string, val interface{}) {
	if v.IsNil() {
		return
	}

	elem := v.Elem() // 获取实际类型

	switch elem.Kind() {
	case reflect.Map:
		handleMap(elem, key, val) // 处理 map 类型
		return
	case reflect.Struct:
		setField(elem, key, val) // 处理结构体类型
		return
	}
}

// handleMap 处理 map 类型的字段
func handleMap(v reflect.Value, key string, val interface{}) {
	for _, k := range v.MapKeys() {
		val0 := v.MapIndex(k)
		if val0.Kind() == reflect.Struct {
			setField(val0, key, val)
		} else if val0.Kind() == reflect.Map {
			handleMap(val0, key, val)
		} else {
			// 尝试将 val 转换为字段类型
			if cVal, ok := canSetValue(val0, val); ok {
				val0.Set(reflect.ValueOf(cVal))
			}
		}
	}
}

// canSetValue 判断是否可以将 val 转换为字段类型
func canSetValue(field reflect.Value, val interface{}) (interface{}, bool) {
	fieldValue := reflect.ValueOf(val)
	fieldType := field.Type()

	switch fieldType.Kind() {
	case reflect.String:
		if fieldValue.Type().ConvertibleTo(fieldType) {
			field.Set(fieldValue.Convert(fieldType))
			return val, true
		}
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		if fieldValue.Type().ConvertibleTo(fieldType) {
			field.Set(fieldValue.Convert(fieldType))
			return val, true
		}
	case reflect.Float32, reflect.Float64:
		if fieldValue.Type().ConvertibleTo(fieldType) {
			field.Set(fieldValue.Convert(fieldType))
			return val, true
		}
	case reflect.Bool:
		if fieldValue.Type().ConvertibleTo(fieldType) {
			field.Set(fieldValue.Convert(fieldType))
			return val, true
		}

		temp := strings.ToUpper(fmt.Sprintf("%v", fieldValue))
		bl := (temp == "1") || (temp == "TRUE")
		field.Set(reflect.ValueOf(bl))
		return bl, bl
	case reflect.Struct:
		switch fieldType.String() {
		case "time.Time":
			if fieldValue.Type().Kind() == reflect.String {
				t := toDate(fieldValue.String())
				field.Set(reflect.ValueOf(t))
				return t, true
			}

			field.Set(fieldValue)
			return val, true
		case "decimal.Decimal":
			t, err := decimal.NewFromString(fmt.Sprintf("%v", fieldValue))
			if err != nil {
				field.Set(reflect.ValueOf(decimal.Zero))
				return decimal.Zero, true
			}
			field.Set(reflect.ValueOf(t))
			return t, true
		}
	}

	return val, false
}

// 字符串转时间
func toDate(str string) time.Time {
	str = strings.TrimSpace(str)
	if str == "" {
		return time.Now()
	}

	str = strings.Replace(str, "/", "-", -1)
	str = strings.Replace(str, "T", " ", -1)
	str = strings.Replace(str, "Z", "", -1)
	str = strings.Replace(str, "z", "", -1)

	iEd := strings.LastIndex(str, ".")
	if iEd > -1 {
		str = str[:iEd]
	}

	local, _ := time.LoadLocation("Asia/Shanghai")
	if len(str) == 19 {
		result, _ := time.ParseInLocation(timeFormat, str, local)
		return result
	}

	if len(str) == 16 {
		result, _ := time.ParseInLocation("2006-01-02 15:04", str, local)
		return result
	}

	result, _ := time.ParseInLocation("2006-01-02", str, local)

	return result
}

// ----------- Map转结构体 结束 ----------- //

// ----------- 结构体转Map 开始 ----------- //

// 将结构体的所有属性及其值存入 map[string]interface{}
func ToMap(entity interface{}) map[string]interface{} {
	return structToMap(entity)
}

// 将结构体的所有属性及其值存入 map[string]interface{}
func structToMap(source interface{}) map[string]interface{} {
	entityValue := reflect.ValueOf(source)

	if entityValue.Kind() == reflect.Ptr { //传入的是指针如:&User{}
		entityValue = entityValue.Elem() // 获取指针指向的值
	}

	entityType := entityValue.Type()

	result := make(map[string]interface{})

	// 遍历结构体的所有字段
	for i := 0; i < entityType.NumField(); i++ {
		fieldType := entityType.Field(i)
		fieldValue := entityValue.Field(i)

		jsonTag := fieldType.Tag.Get("json")

		// 跳过忽略字段
		if jsonTag == "-" {
			continue
		}

		// 获取当前字段的键名
		key := jsonTag

		// 处理匿名结构体
		if fieldType.Anonymous {
			if fieldValue.Kind() == reflect.Struct {
				subMap := structToMap(fieldValue.Interface()) //递归
				for k, v := range subMap {
					//result["@@@Anonymous_" + k] = v//如果发现匿名字段覆盖了上级字段,则必须进行特殊处理,在此保留代码
					result[k] = v
				}
			}

			continue
		}

		if fieldValue.Kind() == reflect.Ptr && !fieldValue.IsNil() {
			// 处理结构体指针
			subMap := structToMap(fieldValue.Elem().Interface()) //递归
			result[key] = subMap
			continue
		}

		// 直接处理普通字段
		if fieldType.Type == reflect.TypeOf(time.Time{}) {
			// 格式化 time.Time 类型
			if fieldValue.CanInterface() {
				t := fieldValue.Interface().(time.Time)
				result[key] = t.Format(timeFormat)
			}
			continue
		}

		result[key] = fieldValue.Interface()
	}

	return result
}

// ----------- 结构体转Map 结束 ----------- //

/**
 * 结构体映射表简化信息
 */
func GetTableInfo(entity Entity) *TableInfo {
	result := &TableInfo{}

	array := strings.Split(entity.BaseColumnNames(), ",")
	for _, key := range array {
		dataInfo := entity.GetDataInfo(key)
		result.DbName = dataInfo.DbName
		result.TableName = dataInfo.TableName

		if dataInfo.IsKey && result.KeyName == "" {
			result.KeyName = dataInfo.Name
			result.AutoKey = dataInfo.IsExtra
			result.KeyLen = dataInfo.MaxLength
			continue
		}

		if dataInfo.IsBigTxt {
			result.BigTextFields += dataInfo.Name + ";"
		}

		switch dataInfo.Name {
		case TablePidKey[1]:
			result.HasPid = true
		case TablePathKey[1]:
			result.HasPath = true
		case TableRecordKey[1]:
			result.HasRecordKey = true
		case TableMemo[1]:
			result.HasMemo = true
		case TableCreator[1]:
			result.HasCreator = true
		case TableCreateDate[1]:
			result.HasCreateDate = true
		case TableModifieder[1]:
			result.HasModifieder = true
		case TableModifiedDate[1]:
			result.HasModifiedDate = true
		case TableState[1]:
			result.HasState = true
		case TableRrank[1]:
			result.HasRank = true
		case TableEdition[1]:
			result.HasEdition = true
		case TablePassword[1]:
			result.HasPassword = true
		case TableSign[1]:
			result.HasSign = true
		case TableOnlyign[1]:
			result.HasOnlyign = true
		case TableDelSign[1]:
			result.HasDelSign = true
		}
	}

	return result
}

// ----------- 实体验证 开始 ----------- //

/**
 * 对对象中添加了DataInfo注解的不为null的属性检查限制
 * data 数据
 * entity 检查用数据结构
 * ignoreNames 待忽略的字段
 */
func ValidAttr(data map[string]interface{}, entity Entity, ignoreNames []string) (int, error) {
	if data == nil {
		return 1001, createError("数据为nil")
	}

	if entity == nil {
		return 1002, createError("数据结构为nil")
	}

	ignoreNamesMap := make(map[string]struct{}, len(ignoreNames))
	for _, v := range ignoreNames {
		ignoreNamesMap[v] = struct{}{}
	}

	array := strings.Split(entity.BaseColumnNames(), ",")
	for _, key := range array {
		if _, ok := ignoreNamesMap[key]; ok { //忽略字段
			continue
		}

		dataInfo := entity.GetDataInfo(key)
		if dataInfo == nil { //这个字段没有数据信息(一般不太可能)
			return 1003, createError("发现不符合框架规则的字段:", key)
		}

		if !dataInfo.IsNull { //字段是否允许为空
			temp := data[key]
			if temp == nil { //数据如果为空
				return 1004, createError(dataInfo.Comment, "[", dataInfo.Name, "]不允许为空！")
			} else if fmt.Sprintf("%v", temp) == "" {
				return 1005, createError(dataInfo.Comment, "[", dataInfo.Name, "]不允许为空！")
			}
		}

		oldValue := fmt.Sprintf("%v", data[key])
		strType := dataInfo.DbFileType

		if strType == "int" { // 此时oldValue的值为:"<int Value>"
			oldValue = fmt.Sprintf("%v", data[key])
		}

		iL := dataInfo.MaxLength //真正存储字段的最大长度要求不可能小于1,否则无意义,如果定义iMaxLength为0,或-1则说明是要忽略检查的
		if !strings.Contains("/datetime/date/time/", strType) && iL > 0 && iL < utf8.RuneCountInString(oldValue) {
			return 1006, createError(dataInfo.Comment, "[", dataInfo.Name, "]长度超出，最大长度限制为:", iL)
		}
	}

	return 1999, nil
}

/**
 * 对'编辑'对象中添加了DataInfo注解的不为null的属性检查限制
 * data 数据
 * entity 检查用数据结构
 * ignoreNames 待忽略的字段
 */
func ValidAttrByEdit(data map[string]interface{}, entity Entity, ignoreNames []string) (int, error) {
	if data == nil {
		return 1001, errors.New("数据为nil")
	}

	if entity == nil {
		return 1002, errors.New("数据结构为nil")
	}

	ignoreNamesMap := make(map[string]string, len(ignoreNames))
	for _, v := range ignoreNames {
		ignoreNamesMap[v] = v
	}

	allDataInfo := AllDataInfo(entity)
	for _, dataInfo := range allDataInfo {
		value, ok := data[dataInfo.Name]
		if !ok {
			continue //此字段不参与更新则不进行检查
		}

		if _, ok = ignoreNamesMap[dataInfo.Name]; ok {
			continue
		}

		oldValue := strings.TrimSpace(fmt.Sprintf("%v", value))
		strType := dataInfo.DbFileType

		if strType == "int" { // 此时oldValue的值为:"<int Value>"
			oldValue = fmt.Sprintf("%v", value)
		}

		iL := dataInfo.MaxLength //真正存储字段的最大长度要求不可能小于1,否则无意义,如果定义iMaxLength为0,或-1则说明是要忽略检查的
		if (!strings.Contains("/datetime/date/time/time.Time/decimal.Decimal", strType)) && (iL > 0) && (iL < utf8.RuneCountInString(oldValue)) {
			return 1003, createError(dataInfo.Comment, "[", dataInfo.Name, "]长度超出，最大长度限制为:", iL)
		}

		if !dataInfo.IsNull && (oldValue == "<nil>" || oldValue == "") {
			return 1004, createError(dataInfo.Comment, "[", dataInfo.Name, "]不允许为空！")
		}

		if strings.Contains("/decimal.Decimal/*big.Float", strType) && (iL > 0) {
			str := fmt.Sprintf("%v", data[dataInfo.Name])
			array := strings.Split(str, ".")

			if dataInfo.IntLength < len(array[0]) {
				return 1005, createError(dataInfo.Comment, "[", dataInfo.Name, "]整数超出，最大长度限制为:", dataInfo.IntLength)
			}

			if (len(array) > 1) && (dataInfo.DecimalLength < len(array[1])) {
				return 1006, createError(dataInfo.Comment, "[", dataInfo.Name, "]小数超过，最大长度限制为:", dataInfo.IntLength)
			}
		}

		if strings.Contains("/float64/float32/", strType) && (iL > 0) {
			str := fmt.Sprintf("%f", data[dataInfo.Name])
			array := strings.Split(str, ".")

			if dataInfo.IntLength < len(array[0]) {
				return 1005, createError(dataInfo.Comment, "[", dataInfo.Name, "]整数超出，最大长度限制为:", dataInfo.IntLength)
			}

			if (len(array) > 1) && (dataInfo.DecimalLength < len(array[1])) {
				return 1006, createError(dataInfo.Comment, "[", dataInfo.Name, "]小数超过，最大长度限制为:", dataInfo.IntLength)
			}
		}
	}

	return 1999, nil
}

/**
 * 对'新增'对象中添加了DataInfo注解的属性检查限制
 * entity 检查用数据结构
 * ignoreNames 待忽略的字段
 */
func ValidAttrByAdd(entity Entity, ignoreNames []string) (int, error) {
	if entity == nil {
		return 1001, createError("数据结构为nil")
	}

	ignoreNamesMap := make(map[string]struct{}, len(ignoreNames))
	for _, v := range ignoreNames {
		ignoreNamesMap[v] = struct{}{}
	}

	pType := reflect.TypeOf(entity).Elem()   // 获取指针指向的类型
	pValue := reflect.ValueOf(entity).Elem() // 获取指针指向的值

	for i := 0; i < pValue.NumField(); i++ {
		field := pValue.Field(i)
		jsonTag := pType.Field(i).Tag.Get("json") // 获取json标签

		if jsonTag == "-" {
			continue
		}

		dataInfo := GetDataInfo(entity, jsonTag)
		if dataInfo == nil {
			continue
		}

		if _, ok := ignoreNamesMap[dataInfo.Name]; ok { //排除忽略字段
			continue
		}

		oldValue := fmt.Sprintf("%v", reflect.ValueOf(field))
		strType := fmt.Sprintf("%v", field.Type()) //类型

		if strType == "int" { // 此时oldValue的值为:"<int Value>"
			oldValue = fmt.Sprintf("%v", field)
		}

		iL := dataInfo.MaxLength //真正存储字段的最大长度要求不可能小于1,否则无意义,如果定义iMaxLength为0,或-1则说明是要忽略检查的
		if !strings.Contains("/datetime/date/time/time.Time/decimal.Decimal/", strType) && iL > 0 && iL < utf8.RuneCountInString(oldValue) {
			return 1002, createError(dataInfo.Comment, "[", dataInfo.Name, "]长度超出，最大长度限制为:", iL)
		}

		if !dataInfo.IsNull && ((oldValue == "<nil>") || (oldValue == "")) {
			return 1003, createError(dataInfo.Comment, "[", dataInfo.Name, "]不允许为空！")
		}

		if strings.Contains("/datetime/date/time/time.Time/", strType) && !dataInfo.IsNull && (oldValue == "" || oldValue == "0001-01-01 00:00:00 +0000 UTC" || oldValue == "<nil>") {
			return 1004, createError(dataInfo.Comment, "[", dataInfo.Name, "]不允许为空！")
		}

		if strings.Contains("/decimal.Decimal/*big.Float", strType) && (iL > 0) {
			str := fmt.Sprintf("%v", field)
			array := strings.Split(str, ".")

			if dataInfo.IntLength < len(array[0]) {
				return 1005, createError(dataInfo.Comment, "[", dataInfo.Name, "]整数超出，最大长度限制为:", dataInfo.IntLength)
			}

			if (len(array) > 1) && (dataInfo.DecimalLength < len(array[1])) {
				return 1006, createError(dataInfo.Comment, "[", dataInfo.Name, "]小数超过，最大长度限制为:", dataInfo.IntLength)
			}
		}

		if strings.Contains("/float64/float32/", strType) && (iL > 0) {
			str := fmt.Sprintf("%v", field)
			array := strings.Split(str, ".")

			if dataInfo.IntLength < len(array[0]) {
				return 1005, createError(dataInfo.Comment, "[", dataInfo.Name, "]整数超出，最大长度限制为:", dataInfo.IntLength)
			}

			if (len(array) > 1) && (dataInfo.DecimalLength < len(array[1])) {
				return 1006, createError(dataInfo.Comment, "[", dataInfo.Name, "]小数超过，最大长度限制为:", dataInfo.IntLength)
			}
		}
	}

	return 1999, nil
}

/**
 * 按实体保留map中的数据
 * object 待检查对象
 * data 数据
 * fieldPrefix 字段前缀(可不传)
 */
func HoldByEntity(entity Entity, data map[string]interface{}) map[string]interface{} {
	result := make(map[string]interface{})

	if entity == nil {
		return result
	}

	if data == nil {
		return result
	}

	array := strings.Split(entity.BaseColumnNames(), ",")
	for _, key := range array {
		if v, ok := data[key]; ok {
			result[key] = v
		}
	}

	return result
}

/**
 * 创建错误信息
 * @param msg 错误信息
 * 返回错误信息
 */
func createError(msg ...interface{}) error {
	var build strings.Builder
	for _, v := range msg {
		build.WriteString(fmt.Sprintf("%v", v))
	}

	return errors.New(build.String())
}

// ----------- 实体验证 结束 ----------- //

// ----------- 反射属性操作 开始 ----------- //

// 递归访问所有字段
func getFieldsVal(val reflect.Value) map[string]interface{} {
	result := make(map[string]interface{})

	var t reflect.Type
	if val.Kind() == reflect.Ptr { // 判断是否是指针类型
		val = val.Elem()
		t = val.Type()
	} else {
		t = val.Type()
	}

	// 遍历结构体字段,遍历优先级: 具名结构体>匿名结构体>字段; 按此遍历将实现后面遍历到的同名字段将覆盖前面被遍历到的

	// 处理具名结构体
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)

		if !field.Anonymous && field.Type.Kind() == reflect.Struct {
			anonVal := val.Field(i)
			if anonVal.Kind() == reflect.Ptr && !anonVal.IsNil() {
				anonVal = anonVal.Elem()
			}

			temp := getFieldsVal(anonVal)
			for k, v := range temp {
				result[k] = v
			}
		}
	}

	// 处理匿名结构体
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)

		if field.Anonymous && field.Type.Kind() == reflect.Struct {
			anonVal := val.Field(i)
			if anonVal.Kind() == reflect.Ptr && !anonVal.IsNil() {
				anonVal = anonVal.Elem()
			}

			temp := getFieldsVal(anonVal)
			for k, v := range temp {
				result[k] = v
			}
		}
	}

	// 处理最外层普通属性
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		jsonTag := field.Tag.Get("json")
		if jsonTag != "" {
			fieldValue := val.Field(i).Interface()
			result[jsonTag] = fieldValue
		}
	}

	return result
}

// 取指定字段值,从外往内找,找到就结束
func getFieldVal(val reflect.Value, jsonName string) interface{} {
	var t reflect.Type
	if val.Kind() == reflect.Ptr { // 判断是否是指针类型
		val = val.Elem()
		t = val.Type()
	} else {
		t = val.Type()
	}

	//在外层找,再向内层(匿名结构体)查找

	// 最外层普通属性
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		jsonTag := field.Tag.Get("json")
		if jsonTag == jsonName {
			fieldValue := val.Field(i).Interface()
			return fieldValue
		}
	}

	// 处理匿名结构体
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)

		if !field.Anonymous {
			continue
		}

		if field.Type.Kind() != reflect.Struct {
			continue
		}

		anonVal := val.Field(i)
		if anonVal.Kind() == reflect.Ptr && !anonVal.IsNil() {
			anonVal = anonVal.Elem()
		}

		temp := getFieldVal(anonVal, jsonName)
		if temp != nil {
			return temp
		}
	}

	// 处理具名结构体
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)

		if field.Anonymous || field.Type.Kind() != reflect.Struct {
			continue
		}

		anonVal := val.Field(i)
		if anonVal.Kind() == reflect.Ptr && !anonVal.IsNil() {
			anonVal = anonVal.Elem()
		}

		temp := getFieldVal(anonVal, jsonName)
		if temp != nil {
			return temp
		}
	}

	return nil
}

// 设置指定字段的值,从外往内找,找到就结束
func setFieldVal(val reflect.Value, jsonName string, data interface{}) bool {
	var t reflect.Type
	if val.Kind() == reflect.Ptr { // 判断是否是指针类型
		val = val.Elem()
		t = val.Type()
	} else {
		t = val.Type()
	}

	//在外层找,再向内层(匿名结构体)查找

	// 最外层普通属性
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		jsonTag := field.Tag.Get("json")
		if jsonTag == jsonName {
			fieldV := val.Field(i)
			canSetValue(fieldV, data)
			// if _, ok := canSetValue(fieldV, data); ok { //有可能其它嵌套结构体也有同名字段
			//fieldV.Set(reflect.ValueOf(cData))
			// }
			return true
		}
	}

	// 处理匿名结构体
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)

		if !field.Anonymous {
			continue
		}

		if field.Type.Kind() != reflect.Struct {
			continue
		}

		anonVal := val.Field(i)
		if anonVal.Kind() == reflect.Ptr && !anonVal.IsNil() {
			anonVal = anonVal.Elem()
		}

		if setFieldVal(anonVal, jsonName, data) {
			return true
		}
	}

	// 处理具名结构体
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)

		if field.Anonymous || field.Type.Kind() != reflect.Struct {
			continue
		}

		anonVal := val.Field(i)
		if anonVal.Kind() == reflect.Ptr && !anonVal.IsNil() {
			anonVal = anonVal.Elem()
		}

		if setFieldVal(anonVal, jsonName, data) {
			return true
		}
	}

	return false
}

// 按map设置指定字段的值,从外往内找,找到就结束
func setFieldsVal(val reflect.Value, data map[string]interface{}, cover bool) {
	var t reflect.Type
	if val.Kind() == reflect.Ptr { // 判断是否是指针类型
		val = val.Elem()
		t = val.Type()
	} else {
		t = val.Type()
	}

	//在外层找,再向内层(匿名结构体)查找

	// 最外层普通属性
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		jsonTag := field.Tag.Get("json")
		if v, ok := data[jsonTag]; ok {
			fieldV := val.Field(i)

			oldValue := reflect.ValueOf(field)
			if (fmt.Sprintf("%v", oldValue) != "") && !cover {
				continue //如果值不为空,且不允许覆盖,则不要赋值
			}

			canSetValue(fieldV, v)
			//fieldV.Set(reflect.ValueOf(v))
		}
	}

	// 处理匿名结构体
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)

		if !field.Anonymous {
			continue
		}

		if field.Type.Kind() != reflect.Struct {
			continue
		}

		anonVal := val.Field(i)
		if anonVal.Kind() == reflect.Ptr && !anonVal.IsNil() {
			anonVal = anonVal.Elem()
		}

		setFieldsVal(anonVal, data, cover)
	}

	// 处理具名结构体
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)

		if field.Anonymous || field.Type.Kind() != reflect.Struct {
			continue
		}

		anonVal := val.Field(i)
		if anonVal.Kind() == reflect.Ptr && !anonVal.IsNil() {
			anonVal = anonVal.Elem()
		}

		setFieldsVal(anonVal, data, cover)
	}
}

// ----------- 反射属性操作 结束 ----------- //

// ----------- 数据库字段名常量 开始 ----------- //

// 初始化数据库字段名常量
func initTableKeyName() {
	root := ""
	exePath, err := os.Executable()
	if err != nil {
		root = "."
	}

	root, _ = filepath.EvalSymlinks(filepath.Dir(exePath))

	filePath := root + "/config/entity.ini"

	// 尝试加载配置文件
	cfg, err := ini.Load(filePath)
	if err != nil {
		// 文件不存在或打开失败，返回渼沐方式
		initTableKeyNamePopular() //配置文件不存在,取默认
		go saveEntityConfig(filePath)
		return
	}

	// 获取 "mode" 部分
	modeSection, err := cfg.GetSection("mode")
	if err != nil {
		// "mode" 部分不存在，返回渼沐方式
		initTableKeyNamePopular() //配置文件不存在,取默认
		go saveEntityConfig(filePath)
		return
	}

	// 获取 "type" 键的值
	modeType, err := modeSection.GetKey("type")
	if err != nil {
		// "type" 键不存在，返回渼沐方式
		initTableKeyNamePopular() //配置文件不存在,取默认
		go saveEntityConfig(filePath)
		return
	}

	sType := strings.ToLower(modeType.String())
	if sType == "default" {
		initTableKeyNamePopular() //取通俗方式
	} else if sType == "meimu" {
		initTableKeyNameMeiMu() //取渼沐方式
	} else if sType == "popular" {
		initTableKeyNamePopular() //取通俗方式
	} else if sType == "custom" {
		customSection, err := cfg.GetSection("custom")
		if err != nil {
			// "mode" 部分不存在，返回通俗方式
			initTableKeyNamePopular() //自定义配置不存在,取默认
			go saveEntityConfig(filePath)
			return
		}

		initTableKeyNameCustom(customSection) //取自定义方式
		go saveEntityConfig(filePath)
	} else {
		initTableKeyNamePopular() //取通俗方式
		go saveEntityConfig(filePath)
	}
}

// 初始化数据库字段名常量-渼沐方式
func initTableKeyNameMeiMu() {
	tableKeyNameMap = map[string][]string{
		"BaseSystem":    {"BaseSystem", "BaseSystem"},        //基本数据库库名
		"sId":           {"sId", "GsId"},                     //数据库表主键名称(字符串形式)
		"iId":           {"iId", "GiId"},                     //数据库表主键名称(自增长形式)
		"uId":           {"uId", "GuId"},                     //数据库表主键名称(UUID形式)
		"sPid":          {"sPid", "GsPid"},                   //数据库表字段名称-上级字段名
		"sPath":         {"sPath", "GsPath"},                 //数据库表字段名称-主键路径名
		"sName":         {"sName", "GsName"},                 //数据库表字段名称-树形节点名
		"sCreator":      {"sCreator", "GsCreator"},           //数据库表字段名称-创建人
		"dCreateDate":   {"dCreateDate", "GdCreateDate"},     //数据库表字段名称-创建时间
		"sModifieder":   {"sModifieder", "GsModifieder"},     //数据库表字段名称-修改人
		"dModifiedDate": {"dModifiedDate", "GdModifiedDate"}, //数据库表字段名称-修改时间
		"iState":        {"iState", "GiState"},               //数据库表字段名称-状态值
		"iRank":         {"iIndex", "GiIndex"},               //数据库表字段名称-排序值
		"iEdition":      {"iVersion", "GiVersion"},           //数据库表字段名称-版本号
		"sPassword":     {"password", "Gpassword"},           //数据库表字段名称-密码
		"iDelSign":      {"iDelSign", "GiDelSign"},           //数据库表字段名称-逻辑删除标识
		"iSetp":         {"iSetp", "GiSetp"},                 //数据库表字段名称-步骤值
		"sOnlyign":      {"sOnlyign", "GsOnlyign"},           //数据库表字段名称-唯一标识
		"sValue":        {"sValue", "GsValue"},               //字典表值字段名
		"sRecordKey":    {"sRecordKey", "GsRecordKey"},       //记录验证串字段名
		"sSign":         {"sSign", "GsSign"},                 //标识字段名
		"sMemo":         {"sMemo", "GsMemo"},                 //备注字段名
		"Dictionary":    {"Dictionary", "Dictionary"},        //字典表表名
	}

	initTableKeyNameValue()
}

// 初始化数据库字段名常量-通俗方式
func initTableKeyNamePopular() {
	tableKeyNameMap = map[string][]string{
		"BaseSystem":    {"base_system", "BaseSystem"},     //基本数据库库名
		"sId":           {"id", "Id"},                      //数据库表主键名称(字符串形式)
		"iId":           {"id", "Id"},                      //数据库表主键名称(自增长形式)
		"uId":           {"id", "Id"},                      //数据库表主键名称(UUID形式)
		"sPid":          {"pid", "Pid"},                    //数据库表字段名称-上级字段名
		"sPath":         {"path", "Path"},                  //数据库表字段名称-主键路径名
		"sName":         {"name", "Name"},                  //数据库表字段名称-树形节点名
		"sCreator":      {"creator", "Creator"},            //数据库表字段名称-创建人
		"dCreateDate":   {"create_date", "CreateDate"},     //数据库表字段名称-创建时间
		"sModifieder":   {"modifieder", "Modifieder"},      //数据库表字段名称-修改人
		"dModifiedDate": {"modified_date", "ModifiedDate"}, //数据库表字段名称-修改时间
		"iState":        {"state", "State"},                //数据库表字段名称-状态值
		"iRank":         {"rank", "Rank"},                  //数据库表字段名称-排序值
		"iEdition":      {"edition", "Edition"},            //数据库表字段名称-版本号
		"sPassword":     {"password", "Password"},          //数据库表字段名称-密码
		"iDelSign":      {"deleted", "Deleted"},            //数据库表字段名称-逻辑删除标识
		"iSetp":         {"setp", "Setp"},                  //数据库表字段名称-步骤值
		"sOnlyign":      {"onlyign", "Onlyign"},            //数据库表字段名称-唯一标识
		"sValue":        {"value", "Value"},                //字典表值字段名
		"sRecordKey":    {"record_key", "RecordKey"},       //记录验证串字段名
		"sSign":         {"sign", "Sign"},                  //标识字段名
		"sMemo":         {"memo", "Memo"},                  //备注字段名
		"Dictionary":    {"dictionary", "Dictionary"},      //字典表表名
	}

	initTableKeyNameValue()
}

// 初始化数据库字段名常量-自定义方式
func initTableKeyNameCustom(section *ini.Section) {
	for _, key := range section.Keys() {
		temp := strings.Split(key.String(), ",")
		field := temp[0]
		name := temp[0]
		if len(temp) > 1 {
			name = temp[1]
		}

		tableKeyNameMap[key.Name()] = []string{field, name}
	}

	initTableKeyNameValue()
}

func initTableKeyNameValue() {
	BaseSystemDb = tableKeyNameMap["BaseSystem"]         //基本数据库库名
	TableMajorKeyString = tableKeyNameMap["sId"]         //数据库表主键名称(字符串形式)
	TableMajorKeyAutoInt = tableKeyNameMap["iId"]        //数据库表主键名称(自增长形式)
	TableMajorKeyUuId = tableKeyNameMap["uId"]           //数据库表主键名称(UUID形式)
	TablePidKey = tableKeyNameMap["sPid"]                //数据库表字段名称-上级字段名
	TablePathKey = tableKeyNameMap["sPath"]              //数据库表字段名称-主键路径名
	TableTreeNode = tableKeyNameMap["sName"]             //数据库表字段名称-树形节点名
	TableCreator = tableKeyNameMap["sCreator"]           //数据库表字段名称-创建人
	TableCreateDate = tableKeyNameMap["dCreateDate"]     //数据库表字段名称-创建时间
	TableModifieder = tableKeyNameMap["sModifieder"]     //数据库表字段名称-修改人
	TableModifiedDate = tableKeyNameMap["dModifiedDate"] //数据库表字段名称-修改时间
	TableState = tableKeyNameMap["iState"]               //数据库表字段名称-状态值
	TableRrank = tableKeyNameMap["iRank"]                //数据库表字段名称-排序值
	TableEdition = tableKeyNameMap["iEdition"]           //数据库表字段名称-版本号
	TablePassword = tableKeyNameMap["sPassword"]         //数据库表字段名称-密码
	TableDelSign = tableKeyNameMap["iDelSign"]           //数据库表字段名称-逻辑删除标识
	TableSetp = tableKeyNameMap["iSetp"]                 //数据库表字段名称-步骤值
	TableOnlyign = tableKeyNameMap["sOnlyign"]           //数据库表字段名称-唯一标识
	TableDictionaryValue = tableKeyNameMap["sValue"]     //字典表值字段名
	TableRecordKey = tableKeyNameMap["sRecordKey"]       //记录验证串字段名
	TableSign = tableKeyNameMap["sSign"]                 //标识字段名
	TableMemo = tableKeyNameMap["sMemo"]                 //备注字段名
	TableNameDictionary = tableKeyNameMap["Dictionary"]  //字典表表名

	TableTreeRootValue = []string{"00", "00"} //数据库树型表根节点默认值
}

/**
 * 保存配置文件
 * @param filePath string
 */
func saveEntityConfig(filePath string) {
	cfg, err := ini.Load(filePath)
	if err != nil {
		cfg = ini.Empty()
	}

	cfg.Section("mode").Key("type").SetValue("default")
	section := cfg.Section("custom")

	for key, val := range tableKeyNameMap {
		temp := val[0] + "," + val[1]
		section.Key(key).SetValue(temp)
	}

	cfg.SaveTo(filePath)
}

// ----------- 数据库字段名常量 结束 ----------- //
