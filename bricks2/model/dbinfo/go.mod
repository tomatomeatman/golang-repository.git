module gitee.com/tomatomeatman/golang-repository/bricks2/model/dbinfo

go 1.23.4

require (
	github.com/shopspring/decimal v1.4.0
	gopkg.in/ini.v1 v1.67.0
)

require github.com/stretchr/testify v1.10.0 // indirect
