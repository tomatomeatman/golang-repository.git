package dbinfo

// 数据信息注解
type TableInfo struct {
	DbName          string `json:"dbName"`          //所在数据库名称
	TableName       string `json:"tableName"`       //所在数据库表表名称
	KeyName         string `json:"keyName"`         //表主键名称
	KeyLen          int    `json:"keyLen"`          //主键长度
	AutoKey         bool   `json:"autoKey"`         //是否自增主键
	HasPid          bool   `json:"hasPid"`          //是否存在Pid
	HasPath         bool   `json:"hasPath"`         //是否存在Path
	HasRecordKey    bool   `json:"hasRecordKey"`    //是否存在sRecordKey
	HasMemo         bool   `json:"hasMemo"`         //是否存在Memo
	HasCreator      bool   `json:"hasCreator"`      //是否存在Creator
	HasCreateDate   bool   `json:"hasCreateDate"`   //是否存在CreateDate
	HasModifieder   bool   `json:"hasModifieder"`   //是否存在Modifieder
	HasModifiedDate bool   `json:"hasModifiedDate"` //是否存在ModifiedDate
	HasState        bool   `json:"hasState"`        //是否存在State
	HasRank         bool   `json:"hasRank"`         //是否存在Rank
	HasEdition      bool   `json:"hasEdition"`      //是否存在Edition
	HasPassword     bool   `json:"hasPassword"`     //是否存在password
	HasSign         bool   `json:"hasSign"`         //是否存在sign
	HasOnlyign      bool   `json:"hasOnlyign"`      //是否存在sOnlyign
	HasDelSign      bool   `json:"hasDelSign"`      //是否存在iDelSign
	BigTextFields   string `json:"bigTextFields"`   //大文本字段名称集合,';'分隔
}
