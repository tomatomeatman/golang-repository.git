package dbinfo

import (
	"encoding/json"
	"time"
)

// 自定义时间类型
type Time time.Time

const (
	timeFormat = "2006-01-02 15:04:05"
)

func (t *Time) UnmarshalJSON(data []byte) (err error) {
	now, err := time.ParseInLocation(`"`+timeFormat+`"`, string(data), time.Local)
	*t = Time(now)
	return
}

// 重写MarshalJSON方法来实现gorm查询时间字段事数据解析
func (tu *Time) MarshalJSON() ([]byte, error) {
	b := make([]byte, 0, len(timeFormat)+2)
	b = append(b, '"')
	b = time.Time(*tu).AppendFormat(b, timeFormat)
	b = append(b, '"')
	return b, nil
}

func (tu Time) String() string {
	return time.Time(tu).Format(timeFormat)
}

// 取当前时间
func (tu Time) Now() Time {
	var result Time

	ti := time.Now().Format(timeFormat)
	str, err := json.Marshal(ti)
	if err != nil {
		return result
	}

	json.Unmarshal([]byte(str), &result)

	return result
}
