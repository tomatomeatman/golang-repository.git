package dbinfo

//分页查询条件组合类
type FindByPageParam struct {
	Page        Page        `json:"page"`        //分页信息对象
	Orders      []OrderInfo `json:"orders"`      //排序信息对象集合
	LikeStr     string      `json:"likeStr"`     //全文检索条件
	LikeDateSt  string      `json:"likeDateSt"`  //全文检索查询记录修改时间范围条件-开始
	LikeDateEd  string      `json:"likeDateEd"`  //全文检索查询记录修改时间范围条件-结束
	Condition   interface{} `json:"condition"`   //常规查询条件对象(根据定义的类型转换)
	HideBigText bool        `json:"hideBigText"` //是否隐藏大文本字段
	HideFields  string      `json:"hideFields"`  //隐藏字段集合';'分隔
}
