package tablekeylocal

import (
	"strings"
	"sync"

	"gitee.com/tomatomeatman/golang-repository/bricks/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data/integerutil"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data/stringutil"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/gorm"
	Log "github.com/cihub/seelog"
)

var (
	syncTablekeyMap        = map[string]*sync.Mutex{} //序列锁集合
	syncCountTablekeyMap   = map[string]int{}         //序列锁引用计数值集合,当值为0时同时清理'序列锁集合'
	sDbName                string                     //数据库名
	tableKeyLocalTableName string                     //表名
)

func init() {
	tableKeyLocalTableName = app.AppUtil{}.ReadConfigKey("DbVariables", "TableKeyName", "TableKeyLocal").(string)

	sDbName = app.AppUtil{}.ReadConfigKey("DbVariables", "MainDb", "BaseSystem.").(string)
	if !strings.HasSuffix(sDbName, ".") {
		sDbName = sDbName + "."
	}
}

/**
 * 记录编号序列管理表TableKeyLocal表基本业务操作结构体
 */
type TableKeyLocalDao struct {
}

/**
 * 验证新增数据是否存在重复 (一旦使用则覆盖通用方法)
 * @param r Http请求对象
 * @param dbEntity 验证数据
 * @return *emity.MsgEmity 返回验证结果
 */
//func (o *TableKeyLocalService) CheckRepeatByAdd(ctx ginutil.Context, dbEntity interface{}) *emity.MsgEmity {
//	return emity.MsgEmity{}.Success(res, "验证通过")
//}

/**
 * 验证更新数据是否存在重复 (一旦使用则覆盖通用方法)
 * @param r Http请求对象
 * @param data 待验证数据
 * @param id 记录编号
 * @return *emity.MsgEmity 返回验证结果
 */
//func (service *TableKeyLocalService) CheckRepeatByEdit(ctx ginutil.Context, data map[string]interface{}, id interface{}) *emity.MsgEmity {
//	return emity.MsgEmity{}.Success(res, "验证通过")
//}

/**
 * 取各表(或序列)的新Id
 * @param formatLong 格式化长度(不足长度+0)
 * @param seriesName 表名或序列名
 * @return
 */
func (o TableKeyLocalDao) GetNewId(formatLong int, seriesName string) string {
	lock, ok := syncTablekeyMap[seriesName]
	if !ok {
		syncTablekeyMap[seriesName] = &sync.Mutex{}
		syncCountTablekeyMap[seriesName] = 1 //计数值
		lock = syncTablekeyMap[seriesName]
	} else {
		syncCountTablekeyMap[seriesName] = syncCountTablekeyMap[seriesName] + 1 //计数值
	}

	lock.Lock() //加锁

	iCount := o.newValue(seriesName)
	if iCount != 1 {
		iCount = o.addTable(seriesName)
		if iCount != 0 {
			syncCountTablekeyMap[seriesName] = syncCountTablekeyMap[seriesName] - 1 //计数值-1
			if syncCountTablekeyMap[seriesName] < 1 {                               //计数值为0,剔除
				delete(syncCountTablekeyMap, seriesName)
				delete(syncTablekeyMap, seriesName)
			}

			lock.Unlock() //解锁
			return stringutil.SupplyZero(formatLong, 1)
		}

		syncCountTablekeyMap[seriesName] = syncCountTablekeyMap[seriesName] - 1 //计数值-1
		if syncCountTablekeyMap[seriesName] < 1 {                               //计数值为0,剔除
			delete(syncCountTablekeyMap, seriesName)
			delete(syncTablekeyMap, seriesName)
		}

		lock.Unlock() //解锁
		return "0"    //说明是取值失败
	}

	sId := o.findLastId(seriesName)
	if sId == "0" {
		syncCountTablekeyMap[seriesName] = syncCountTablekeyMap[seriesName] - 1 //计数值-1
		if syncCountTablekeyMap[seriesName] < 1 {                               //计数值为0,剔除
			delete(syncCountTablekeyMap, seriesName)
			delete(syncTablekeyMap, seriesName)
		}

		lock.Unlock() //解锁
		return "0"    //说明是取值失败
	}

	syncCountTablekeyMap[seriesName] = syncCountTablekeyMap[seriesName] - 1 //计数值-1
	if syncCountTablekeyMap[seriesName] < 1 {                               //计数值为0,剔除
		delete(syncCountTablekeyMap, seriesName)
		delete(syncTablekeyMap, seriesName)
	}

	lock.Unlock() //解锁

	result := stringutil.SupplyZero(formatLong, sId)

	return result
}

/**
 * 取各表的一批新Id
 * @param formatLong 格式化长度(不足长度+0)
 * @param seriesName 表名或序列名
 * @param size 数量
 * @return
 */
func (o TableKeyLocalDao) GetNewIds(formatLong int, seriesName string, size int) []string {
	lock, ok := syncTablekeyMap[seriesName]
	if !ok {
		syncTablekeyMap[seriesName] = &sync.Mutex{}
		syncCountTablekeyMap[seriesName] = 1 //计数值
		lock = syncTablekeyMap[seriesName]
	} else {
		syncCountTablekeyMap[seriesName] = syncCountTablekeyMap[seriesName] + 1 //计数值
	}

	lock.Lock() //加锁

	iCount := o.newValues(seriesName, size)
	if iCount != 1 {
		iCount = o.addTableValues(seriesName, size)
	}

	if iCount != 1 {
		syncCountTablekeyMap[seriesName] = syncCountTablekeyMap[seriesName] - 1 //计数值-1
		if syncCountTablekeyMap[seriesName] < 1 {                               //计数值为0,剔除
			delete(syncCountTablekeyMap, seriesName)
			delete(syncTablekeyMap, seriesName)
		}

		lock.Unlock() //解锁
		return nil    //说明是取值失败
	}

	sLastId := o.findLastId(seriesName)

	syncCountTablekeyMap[seriesName] = syncCountTablekeyMap[seriesName] - 1 //计数值-1
	if syncCountTablekeyMap[seriesName] < 1 {                               //计数值为0,剔除
		delete(syncCountTablekeyMap, seriesName)
		delete(syncTablekeyMap, seriesName)
	}

	lock.Unlock() //解锁

	iLastId := integerutil.ToInt(sLastId)

	result := []string{}
	for i := 0; i < size; i++ {
		vNewId := stringutil.SupplyZero(formatLong, iLastId+i+1)
		result = append(result, vNewId)
	}

	return result
}

/**
 * 重置
 * @param seriesName 表名或序列名
 * @return
 */
func (o TableKeyLocalDao) Reset(seriesName string) string {
	iCount := o.reset(seriesName)

	if iCount < 1 {
		return "0"
	}

	return "1"
}

/**
 * 更新到新sValue值
 * @param sType
 * @return
 */
func (o TableKeyLocalDao) newValue(seriesName string) int {
	sql := "UPDATE ${BaseSystem}TableKeyLocal SET sValue = (sValue +1) ,iVersion = (iVersion +1) WHERE sType = '#{sType}'"
	sql = strings.Replace(sql, "${BaseSystem}", sDbName, -1)
	sql = strings.Replace(sql, "TableKeyLocal", tableKeyLocalTableName, -1)
	sql = strings.Replace(sql, "#{sType}", seriesName, -1)

	dbResult := gorm.Exec(sql)
	if dbResult.Error != nil {
		Log.Error("更新数据发生异常:", dbResult.Error)
		return 0
	}

	return integerutil.ToInt(dbResult.RowsAffected, 0)
}

/**
 * 取最后的id值
 * @param sValue
 * @return
 */
func (o TableKeyLocalDao) findLastId(seriesName string) string {
	sql := "SELECT sValue FROM ${BaseSystem}TableKeyLocal WHERE sType = '#{sType}'"
	sql = strings.Replace(sql, "${BaseSystem}", sDbName, -1)
	sql = strings.Replace(sql, "TableKeyLocal", tableKeyLocalTableName, -1)
	sql = strings.Replace(sql, "#{sType}", seriesName, -1)

	sValue := "0"
	dbResult := gorm.Raw(sql).Find(&sValue)
	if dbResult.Error != nil {
		Log.Error("更新数据发生异常:", dbResult.Error)
		return sValue
	}

	return sValue
}

/**
 * 添加表字段信息
 * @param seriesName
 * @return
 */
func (o TableKeyLocalDao) addTable(seriesName string) int {
	sql := `
	INSERT INTO ${BaseSystem}TableKeyLocal (
		iId
		,sType
		,sValue
		,iIndex
		,iVersion
	)
	SELECT
		IFNULL(MAX(iId), 0) +1
		,'#{sType}'
		,'1'
		,1
		,1
	FROM ${BaseSystem}TableKeyLocal`
	sql = strings.Replace(sql, "${BaseSystem}", sDbName, -1)
	sql = strings.Replace(sql, "TableKeyLocal", tableKeyLocalTableName, -1)
	sql = strings.Replace(sql, "#{sType}", seriesName, -1)

	dbResult := gorm.Exec(sql)
	if dbResult.Error != nil {
		Log.Error("更新数据发生异常:", dbResult.Error)
		return 0
	}

	return integerutil.ToInt(dbResult.RowsAffected, 0)
}

/**
 * 更新到新的一批sValue值
 * @param seriesName
 * @param size
 * @return
 */
func (o TableKeyLocalDao) newValues(seriesName string, size int) int {
	sql := `UPDATE ${BaseSystem}TableKeyLocal SET
				sValue = (sValue +1 + ${size})
				,iVersion = (iVersion +1 + ${size})
			WHERE sType = #{sType}`
	sql = strings.Replace(sql, "${BaseSystem}", sDbName, -1)
	sql = strings.Replace(sql, "TableKeyLocal", tableKeyLocalTableName, -1)
	sql = strings.Replace(sql, "${size}", stringutil.ToStr(size), -1)
	sql = strings.Replace(sql, "#{sType}", seriesName, -1)

	dbResult := gorm.Exec(sql)
	if dbResult.Error != nil {
		Log.Error("更新数据发生异常:", dbResult.Error)
		return 0
	}

	return integerutil.ToInt(dbResult.RowsAffected, 0)
}

/**
 * 添加表字段信息,并设置初始值
 * @param seriesName
 * @param size
 * @return
 */
func (o TableKeyLocalDao) addTableValues(seriesName string, size int) int {
	sql := `
	INSERT INTO ${BaseSystem}TableKeyLocal (
		iId
		,sType
		,sValue
		,iIndex
		,iVersion
	)
	SELECT
		IFNULL(MAX(iId), 0) +1
		,'#{sType}'
		,#{size}
		,1
		,#{size}
	FROM ${BaseSystem}TableKeyLocal`
	sql = strings.Replace(sql, "${BaseSystem}", sDbName, -1)
	sql = strings.Replace(sql, "TableKeyLocal", tableKeyLocalTableName, -1)
	sql = strings.Replace(sql, "${size}", stringutil.ToStr(size), -1)
	sql = strings.Replace(sql, "#{sType}", seriesName, -1)

	dbResult := gorm.Exec(sql)
	if dbResult.Error != nil {
		Log.Error("更新数据发生异常:", dbResult.Error)
		return 0
	}

	return integerutil.ToInt(dbResult.RowsAffected, 0)
}

/**
 * 重置
 * @param seriesName
 * @return
 */
func (o TableKeyLocalDao) reset(seriesName string) int {
	sql := `UPDATE ${BaseSystem}TableKeyLocal SET
			sValue = 1
			,iVersion = (iVersion +1)
		WHERE sType = '#{sType}'`
	sql = strings.Replace(sql, "${BaseSystem}", sDbName, -1)
	sql = strings.Replace(sql, "TableKeyLocal", tableKeyLocalTableName, -1)
	sql = strings.Replace(sql, "#{sType}", seriesName, -1)

	dbResult := gorm.Exec(sql)
	if dbResult.Error != nil {
		Log.Error("更新数据发生异常:", dbResult.Error)
		return 0
	}

	return integerutil.ToInt(dbResult.RowsAffected, 0)
}
