package login

import (
	"gitee.com/tomatomeatman/golang-repository/bricks/model/emity"
	"gitee.com/tomatomeatman/golang-repository/bricks/model/globalvariable"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/urlutil"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/ginutil"
)

// @Controller 登录服务控制器
type LoginController struct {
	ModuleEntity LoginUser //对应模块数据实体
}

/**
 * 初始化
 */
func init() {

	//-- 控制操作,首位9不参与用6个数代表开关,0为不限制,1为限制 --//
	//-- 6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆 --//
	//-- 其中前5个为'是否仅创建者可操作'后续为其它控制开关 --//
	go globalvariable.RegisterVariable("Login_ModuleEnable", 9000001)

	//-- 接口注册 --//
	go ginutil.ControllerRegister("/login/in", LoginController{}.In, ginutil.POST, ginutil.GET)
	go ginutil.ControllerRegister("/login/out", LoginController{}.Out, ginutil.POST)
	go ginutil.ControllerRegister("/login/check", LoginController{}.Check, ginutil.POST)
	go ginutil.ControllerRegister("/login/heartbeat", LoginController{}.Heartbeat, ginutil.POST)
	go ginutil.ControllerRegister("/login/info", LoginController{}.GetLogin, ginutil.POST)
	go ginutil.ControllerRegister("/login/info/current", LoginController{}.GetCurrentLogin, ginutil.POST)
	go ginutil.ControllerRegister("/login/getid", LoginController{}.GetUserId, ginutil.POST)
	go ginutil.ControllerRegister("/login/heartbeat/websoket", LoginController{}.HeartbeatWebsoket, ginutil.WEBSOCKET)
}

// #region @Api {title=用户登录 explain=如果用户和密码正确则返回同行令牌}
// @param {name=sNameOrNo dataType=string paramType=query explain=名称或工号 required=true}
// @param {name=sPass dataType=string paramType=query explain=密码 required=true}
// @param {name=sOwner dataType=string paramType=query explain=用户来源表 required=true}
// @param {name=iDevice dataType=int paramType=query explain=设备类型,1:PC,2:手机,3:平板,4.....}
// @param {name=iResultInfo dataType=int paramType=query explain=是否返回用户信息}
// @return {type=MsgEmity explain=返回令牌}
// @RequestMapping {name=login type=(POST, GET) value=/login/in}
// #endregion
func (LoginController) In(ctx ginutil.Context) interface{} {
	sNameOrNo := urlutil.GetParam(ctx.Request, "sNameOrNo", "").(string)
	sPass := urlutil.GetParam(ctx.Request, "sPass", "").(string)
	sOwner := urlutil.GetParam(ctx.Request, "sOwner", "").(string)
	iDevice := urlutil.GetParam(ctx.Request, "iDevice", 1).(int)
	iResultInfo := urlutil.GetParam(ctx.Request, "iResultInfo", 2).(int)

	return LoginServer{}.In(sNameOrNo, sPass, sOwner, iDevice, iResultInfo)
}

// #region @Api {title=用户登出 explain=使提交的令牌失效}
// @param {name=sCookie dataType=string paramType=query explain=令牌 required=true}
// @return {type=MsgEmity}
// @RequestMapping {name=login type=(POST,GET) value=/login/out}
// #endregion
func (LoginController) Out(ctx ginutil.Context) interface{} {
	sCookie := urlutil.GetParam(ctx.Request, "sCookie", "").(string)
	return LoginServer{}.Out(sCookie)
}

// #region @Api {title=验证令牌 explain=判断sCookie是否已经登录}
// @param {name=sCookie dataType=string paramType=query explain=令牌 required=true}
// @return {type=MsgEmity}
// @RequestMapping {name=login type=POST value=/login/check}
// #endregion
func (LoginController) Check(ctx ginutil.Context) interface{} {
	sCookie := urlutil.GetParam(ctx.Request, "sCookie", "").(string)
	return LoginServer{}.Check(sCookie)
}

// #region @Api {title=取登录用户信息 explain=注意:限制为内部系统访问}
// @param {name=key}
// @param {name=sCookie dataType=string paramType=query explain=令牌 required=true}
// @return {type=MsgEmity}
// @RequestMapping {name=login type=POST value=/login/info}
// #endregion
func (LoginController) GetLogin(ctx ginutil.Context) interface{} {
	appKey := app.AppUtil{}.ReadConfigKey("App", "InsideKey", "12345").(string)

	key := urlutil.GetParam(ctx.Request, "key", "").(string)
	if (key == "") || (key != appKey) {
		return emity.MsgEmity{}.Err(9000, "验证密钥错误")
	}

	sCookie := urlutil.GetParam(ctx.Request, "sCookie", "").(string)
	return LoginServer{}.GetLogin(sCookie)
}

// #region @Api {title=登录心跳操作 explain=sCookie存在则更新并返回true,没有则返回false}
// @param {name=sCookie dataType=string paramType=query explain=令牌 required=true}
// @return {type=MsgEmity}
// @RequestMapping {name=login type=(POST,GET) value=/login/heartbeat}
// #endregion
func (LoginController) Heartbeat(ctx ginutil.Context) interface{} {
	sCookie := urlutil.GetParam(ctx.Request, "sCookie", "").(string)
	return LoginServer{}.Heartbeat(sCookie)
}

// #region @Api {title=取当前登录用户简洁信息}
// @return {type=MsgEmity}
// @RequestMapping {name=login type=POST value=/login/info/current}
// #endregion
func (LoginController) GetCurrentLogin(ctx ginutil.Context) interface{} {
	sCookie := urlutil.GetParam(ctx.Request, "sCookie", "").(string) //获取request对象中的参数,优先: 头信息->参数-->属性
	if sCookie == "" {
		return emity.MsgEmity{}.Err(8001, "令牌已无效,限制获取登录信息")
	}

	return LoginServer{}.GetLogin(sCookie)
}

// #region @Api {title=根据用户和密码取对应的用户编号}
// @param {name=sNameOrNo dataType=string paramType=query explain=名称或工号 required=true}
// @param {name=sPass dataType=string paramType=query explain=密码 required=true}
// @param {name=sOwner dataType=string paramType=query explain=用户来源表 required=true}
// @return {type=MsgEmity}
// @RequestMapping {name=login type=POST value=/login/getid}
// #endregion
func (LoginController) GetUserId(ctx ginutil.Context) interface{} {
	sNameOrNo := urlutil.GetParam(ctx.Request, "sNameOrNo", "").(string)
	sPass := urlutil.GetParam(ctx.Request, "sPass", "").(string)
	sOwner := urlutil.GetParam(ctx.Request, "sOwner", "").(string)
	return LoginServer{}.GetUserId(sNameOrNo, sPass, sOwner)
}

// #region @Api {title=登录心跳操作Websoket explain=msg存在则更新并返回true,没有则返回false}
// @param {name=msg dataType=string paramType=query explain=令牌 required=true}
// @return {type=MsgEmity}
// @RequestMapping {name=login type=(POST,GET) value=/login/heartbeat/websoket}
// #endregion
func (LoginController) HeartbeatWebsoket(ctx ginutil.Context) interface{} {
	sCookie := urlutil.GetParam(ctx.Request, "sCookie", "").(string)
	return LoginServer{}.Heartbeat(sCookie)
}
