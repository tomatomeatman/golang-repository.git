package loginuser

import (
	"time"

	"gitee.com/tomatomeatman/golang-repository/bricks/model/dbinfo"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/gorm"
)

var tableName = ""

/**
 * 用户表'LoginUser'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type LoginUserBase struct {
	dbinfo.BaseFunc
	GsId           string    `json:"sId" gorm:"column:sId; type:varchar; NOT NULL; primary_key"`                        //表编号
	GsName         string    `json:"sName" gorm:"column:sName; type:varchar; NOT NULL"`                                 //名称
	GsNo           string    `json:"sNo" gorm:"column:sNo; type:varchar; NOT NULL"`                                     //标识(组合唯一)
	GsPass         string    `json:"sPass" gorm:"column:sPass; type:varchar; NOT NULL"`                                 //密码
	GsType         string    `json:"sType" gorm:"column:sType; type:varchar; NOT NULL; DEFAULT '管理员'"`                  //类型
	GsOwner        string    `json:"sOwner" gorm:"column:sOwner; type:varchar; DEFAULT 'BaseSystem.LoginUser'"`         //来源
	GsMemo         string    `json:"sMemo" gorm:"column:sMemo; type:varchar"`                                           //备注
	GsCreator      string    `json:"sCreator" gorm:"column:sCreator; type:varchar; NOT NULL; DEFAULT '00000000'"`       //创建人员
	GdCreateDate   time.Time `json:"dCreateDate" gorm:"column:dCreateDate; type:datetime; NOT NULL"`                    //创建时间
	GsModifieder   string    `json:"sModifieder" gorm:"column:sModifieder; type:varchar; NOT NULL; DEFAULT '00000000'"` //修改人员
	GdModifiedDate time.Time `json:"dModifiedDate" gorm:"column:dModifiedDate; type:datetime; NOT NULL"`                //修改时间
	GiState        int       `json:"iState" gorm:"column:iState; type:int; NOT NULL; DEFAULT '1'"`                      //状态(枚举,1:启用;2:禁用)
	GiIndex        int       `json:"iIndex" gorm:"column:iIndex; type:int; NOT NULL; DEFAULT '1'"`                      //序号
	GiVersion      int       `json:"iVersion" gorm:"column:iVersion; type:int; NOT NULL; DEFAULT '1'"`                  //版本号
}

/**
 * 初始化
 */
func init() {
	// dbinfo.TableInfo{}.RegisterEntity("LoginUserBase", &LoginUserBase{})           //注册注册数据库实体类结构体
	// dbinfo.TableInfo{}.RegisterTableInfo("LoginUserBase", LoginUserBase{}.Info()) //注册数据库表信息
}

/**
 * 创建结构实体,并赋予默认值
 */
func (LoginUserBase) New() dbinfo.Entity {
	result := &LoginUserBase{}
	result.SetDefault()

	return result
}

/**
 * 创建结构实体集合
 */
func (LoginUserBase) NewList() interface{} {
	return &[]LoginUserBase{}
}

/**
 * 取基础实体
 */
func (o LoginUserBase) BaseEntity() dbinfo.Entity {
	return o.New()
}

/**
 * 设置默认值
 */
func (lu *LoginUserBase) SetDefault() {
	lu.GsId = ""
	lu.GsName = ""
	lu.GsNo = ""
	lu.GsPass = ""
	lu.GsType = "管理员"
	lu.GsOwner = "BaseSystem.LoginUser"
	lu.GsMemo = ""
	lu.GsCreator = "00000000"
	lu.GdCreateDate = time.Now()
	lu.GsModifieder = "00000000"
	lu.GdModifiedDate = time.Now()
	lu.GiState = 1
	lu.GiIndex = 1
	lu.GiVersion = 1
}

/**
 * 结构体映射表名,处理结构体名称与表名不一致的情况
 */
func (LoginUserBase) TableName() string {
	if tableName != "" {
		return tableName
	}

	tableName = gorm.GetDbName("BaseSystem") + "LoginUser" //通过配置文件的数据库全局变量'DbVariables'进行调整即可

	return tableName
}

/**
 * 结构体映射表的字段名串
 */
func (LoginUserBase) BaseColumnNames() string {
	return "sId,sName,sNo,sPass,sType,sOwner,sMemo,sCreator,dCreateDate,sModifieder,dModifiedDate,iState,iIndex,iVersion"
}

/**
 * 结构体映射表的字段名集合
 */
func (LoginUserBase) BaseColumnNameList() []string {
	return []string{"sId", "sName", "sNo", "sPass", "sType", "sOwner", "sMemo", "sCreator", "dCreateDate", "sModifieder", "dModifiedDate", "iState", "iIndex", "iVersion"}
}

/**
 * 结构体映射表简化信息
 */
func (LoginUserBase) TableInfo() *dbinfo.TableInfo {
	return &dbinfo.TableInfo{
		GsDbName:          "",
		GsTableName:       "LoginUser",
		GsKeyName:         "sId",
		GiKeyLen:          8,
		GbAutoKey:         false,
		GbHasPid:          false,
		GbHasPath:         false,
		GbHasRecordKey:    false,
		GbHasMemo:         true,
		GbHasCreator:      true,
		GbHasCreateDate:   true,
		GbHasModifieder:   true,
		GbHasModifiedDate: true,
		GbHasState:        true,
		GbHasIndex:        true,
		GbHasVersion:      true,
		GbHasPassword:     true,
		GbHasSign:         false,
		GbHasOnlyign:      false,
	}
}
