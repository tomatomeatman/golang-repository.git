package loginuser

import "gitee.com/tomatomeatman/golang-repository/bricks/model/dbinfo"

/**
 * 用户表'LoginUser'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type LoginUser struct {
	LoginUserBase
	GsStateText string `json:"sStateText" gorm:"<-:false;column:sStateText"` //状态(枚举,1:启用;2:禁用)
}

/**
 * 创建结构实体,并赋予默认值
 */
func (LoginUser) New() dbinfo.Entity {
	result := LoginUser{}
	result.LoginUserBase = LoginUserBase{}
	result.LoginUserBase.SetDefault()
	result.GsStateText = ""

	return &result
}
