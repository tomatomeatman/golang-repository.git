package loginlog

import (
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/app"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/ginutil"
)

// @Controller 登录日志控制器
type LoginLogController struct {
	ControllerBaseFunc                 //通用控制层接口方法
	ModuleEntity       LoginLog        //对应模块数据实体
	ModuleService      LoginLogService //对应模块业务实体
}

/**
 * 初始化
 */
func init() {
	ModuleUtil{}.ControllerInit(&LoginLogController{})
}

// 接口注册
func (control LoginLogController) RegisterUrl() {
	go RegisterController("/login/log/del", POST, control.Del)
	go RegisterController("/login/log/find/page", POST, control.FindByPage)
}

// #region @Api {title=删除数据}
// @param {name=iId dataType=int64 paramType=query explain=记录编号 required=true}
// @param {name=iVersion dataType=int paramType=query explain=版本号 required=true}
// @return {type=MsgEmity explain=返回影响数}
// @RequestMapping {name=Del type=POST value=/login/log/del}
// #endregion
func (control LoginLogController) Del(ctx Context) interface{} {
	return ControllerUtil{}.Del(ctx, &control)
}

// #region @Api {title=查询分页数据}
// @param {name=data dataType=json paramType=body explain=FindByPageParam结构数据 explainType=FindByPageParam<LoginLog> required=true}
// @return {type=MsgEmity explain=返回分页数据}
// @RequestMapping {name=FindByPage type=POST value=/login/log/find/page}
// #endregion
func (control LoginLogController) FindByPage(ctx Context) interface{} {
	return ControllerUtil{}.FindByPage(ctx, &control)
}
