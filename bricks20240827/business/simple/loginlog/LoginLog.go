package loginlog

import "gitee.com/tomatomeatman/golang-repository/bricks/model/dbinfo"

/**
 * 登录日志'LoginLog'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type LoginLog struct {
	LoginLogBase
}

/**
 * 创建结构实体,并赋予默认值
 */
func (loginlog LoginLog) New() dbinfo.Entity {
	result := LoginLog{}
	result.LoginLogBase = LoginLogBase{}
	result.LoginLogBase.SetDefault()

	return &result
}

/**
 * 创建结构实体集合
 */
func (LoginLog) NewList() interface{} {
	return &[]LoginLog{}
}
