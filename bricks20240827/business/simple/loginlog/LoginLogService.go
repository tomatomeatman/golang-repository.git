package loginlog

import (
	"strings"

	Log "github.com/cihub/seelog"

	"gitee.com/tomatomeatman/golang-repository/bricks/model/emity"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data/iputil"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/urlutil"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/ginutil"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/gorm"
)

/**
 * 登录日志LoginLog表基本业务操作结构体
 */
type LoginLogService struct {
	app.ServiceBaseFunc
}

var (
	sDbName string //数据库名
)

/**
 * 初始化
 */
func init() {
	sDbName = app.AppUtil{}.ReadConfigKey("DbVariables", "BaseSystem", "BaseSystem").(string)
	ginutil.RegisterAfter("/login/in", LoginLogService{}.AopAddLog) //当触发"LoginController.In"函数时调用LoginLogService{}.AopAddLog
}

/**
 * 验证更新数据是否存在重复 (一旦使用则覆盖通用方法)
 * @param r Http请求对象
 * @param sIp 登录者IP
 * @param sNameOrNo 登录者使用的用户名或工号
 * @return *emity.MsgEmity 返回验证结果
 */
func (service LoginLogService) AddLog(ctx ginutil.Context, sNameOrNo string) *emity.MsgEmity {
	sIp := iputil.RemoteIp(ctx.Request)
	sMACAddress := "" //MacAddress.getMac(sIp);
	var build strings.Builder
	build.WriteString("INSERT INTO ${BricksBaseSystem}LoginLog (")
	build.WriteString("sName, iType, sUserId, sUser, sIp, sMACAddress, dLogDate, dLastCall, sMemo,")
	build.WriteString("sCreator, dCreateDate, sModifieder, dModifiedDate, iState, iIndex, iVersion")
	build.WriteString(") SELECT * FROM (")
	build.WriteString(" select")
	build.WriteString(" '登录日志' as sName,")
	build.WriteString(" 1 as iType,")
	build.WriteString(" sId as sUserId,")
	build.WriteString(" ? as sUser,")
	build.WriteString(" ? as sIp,")
	build.WriteString(" ? as sMACAddress,")
	build.WriteString(" NOW() as dLogDate,")
	build.WriteString(" NOW() as dLastCall,")
	build.WriteString(" null as sMemo,")
	build.WriteString(" '00000000' as sCreator,")
	build.WriteString(" NOW() as dCreateDate,")
	build.WriteString(" '00000000' as sModifieder,")
	build.WriteString(" NOW() as dModifiedDate,")
	build.WriteString(" 1 as iState,")
	build.WriteString(" 1 as iIndex,")
	build.WriteString(" 1 as iVersion")
	build.WriteString(" from ${BricksBaseSystem}LoginUser")
	build.WriteString(" where sId = (")
	build.WriteString(" 	select sId from ${BricksBaseSystem}LoginUser")
	build.WriteString(" 	where sNo = ?")
	build.WriteString(" 	union ")
	build.WriteString(" 	select sId from ${BricksBaseSystem}LoginUser")
	build.WriteString(" 	where sName = ?")
	build.WriteString(" )")
	build.WriteString(" ) TMP")

	txt := build.String()
	txt = strings.Replace(txt, "${BricksBaseSystem}", gorm.GetDbName(sDbName), -1)

	//where := map[string]interface{}{
	//	"sUser":       sNameOrNo,
	//	"sNo":         sNameOrNo,
	//	"sName":       sNameOrNo,
	//	"sIp":         sIp,
	//	"sMACAddress": sMACAddress,
	//}
	//dbResult := gorm.Exec(txt, where)

	dbResult := gorm.Exec(txt, sNameOrNo, sIp, sMACAddress, sNameOrNo, sNameOrNo)
	if dbResult.Error != nil {
		Log.Error("新增发生异常:", dbResult.Error)
		return emity.MsgEmity{}.Err(1002, "新增发生异常:", dbResult.Error)
	}

	if dbResult.RowsAffected < 1 {
		Log.Error("新增失败,影响值小于1")
		return emity.MsgEmity{}.Err(1002, "新增失败")
	}

	return emity.MsgEmity{}.Success(8999, "新增成功")
}

/**
 * Aop方式添加日志
 * @param sDateSt 开始时间范围
 * @param sDateEd 结束时间范围
 * @return
 */
func (service LoginLogService) AopAddLog(ctx ginutil.Context, params ...interface{}) *emity.MsgEmity {
	if len(params) < 1 {
		return emity.MsgEmity{}.Success(1000, "参数错误,但不拦截")
	}

	me := params[0].(*emity.MsgEmity)
	if !me.Gsuccess {
		return emity.MsgEmity{}.Success(1001, "上层函数未正确执行结束所以不进行记录,但不拦截")
	}

	sNameOrNo := urlutil.GetParam(ctx.Request, "sNameOrNo", "").(string)
	if "" == sNameOrNo {
		return emity.MsgEmity{}.Success(8001, "用户名称为空") //虽然不添加,但不能影响切面
	}

	return service.AddLog(ctx, sNameOrNo)
}
