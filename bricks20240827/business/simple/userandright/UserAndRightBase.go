package userandright

import (
	"time"

	"gitee.com/tomatomeatman/golang-repository/bricks/model/dbinfo"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/gorm"
)

var tableName = ""

/**
 * 用户权限表'UserAndRight'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type UserAndRightBase struct {
	dbinfo.BaseFunc
	GiId           int64     `json:"iId" gorm:"column:iId; type:bigintAUTO_INCREMENT; NOT NULL; primary_key"`           //记录编号
	GsUserId       string    `json:"sUserId" gorm:"column:sUserId; type:varchar; NOT NULL"`                             //用户编号
	GsRightId      string    `json:"sRightId" gorm:"column:sRightId; type:varchar; NOT NULL"`                           //权限编号
	GiType         int       `json:"iType" gorm:"column:iType; type:int; DEFAULT '1'"`                                  //权限类型
	GsMemo         string    `json:"sMemo" gorm:"column:sMemo; type:varchar"`                                           //备注
	GsCreator      string    `json:"sCreator" gorm:"column:sCreator; type:varchar; NOT NULL; DEFAULT '00000000'"`       //创建者
	GdCreateDate   time.Time `json:"dCreateDate" gorm:"column:dCreateDate; type:datetime; NOT NULL"`                    //创建时间
	GsModifieder   string    `json:"sModifieder" gorm:"column:sModifieder; type:varchar; NOT NULL; DEFAULT '00000000'"` //修改人
	GdModifiedDate time.Time `json:"dModifiedDate" gorm:"column:dModifiedDate; type:datetime; NOT NULL"`                //修改时间
	GiState        int       `json:"iState" gorm:"column:iState; type:int; NOT NULL; DEFAULT '1'"`                      //状态
	GiIndex        int       `json:"iIndex" gorm:"column:iIndex; type:int; NOT NULL; DEFAULT '1'"`                      //序号
	GiVersion      int       `json:"iVersion" gorm:"column:iVersion; type:int; NOT NULL; DEFAULT '1'"`                  //版本号
}

/**
 * 初始化
 */
func init() {
	// dbinfo.TableInfo{}.RegisterEntity("UserAndRightBase", UserAndRightBase{})           //注册注册数据库实体类结构体
	// dbinfo.TableInfo{}.RegisterTableInfo("UserAndRightBase", UserAndRightBase{}.Info()) //注册数据库表信息
}

/**
 * 创建结构实体,并赋予默认值
 */
func (UserAndRightBase) New() dbinfo.Entity {
	result := &UserAndRightBase{}
	result.SetDefault()

	return result
}

/**
 * 创建结构实体集合
 */
func (UserAndRightBase) NewList() interface{} {
	return &[]UserAndRightBase{}
}

/**
 * 设置默认值
 */
func (o *UserAndRightBase) SetDefault() {
	o.GiId = 0
	o.GsUserId = ""
	o.GsRightId = ""
	o.GiType = 1
	o.GsMemo = ""
	o.GsCreator = "00000000"
	o.GdCreateDate = time.Now()
	o.GsModifieder = "00000000"
	o.GdModifiedDate = time.Now()
	o.GiState = 1
	o.GiIndex = 1
	o.GiVersion = 1
}

/**
 * 结构体映射表名,处理结构体名称与表名不一致的情况
 */
func (userAndRightBase UserAndRightBase) TableName() string {
	if tableName != "" {
		return tableName
	}

	tableName = gorm.GetDbName("BaseSystem") + "UserAndRight"

	return tableName
}

/**
 * 结构体映射表的字段名串
 */
func (UserAndRightBase) BaseColumnNames() string {
	return "iId,sUserId,sRightId,iType,sMemo,sCreator,dCreateDate,sModifieder,dModifiedDate,iState,iIndex,iVersion"
}

/**
 * 结构体映射表的字段名集合
 */
func (UserAndRightBase) BaseColumnNameList() []string {
	return []string{"iId", "sUserId", "sRightId", "iType", "sMemo", "sCreator", "dCreateDate", "sModifieder", "dModifiedDate", "iState", "iIndex", "iVersion"}
}

/**
 * 取基础实体,用于在子类(嵌套结构体)时同样获得基类
 */
func (UserAndRightBase) BaseEntity() dbinfo.Entity {
	return &UserAndRightBase{}
}

/**
 * 结构体映射表简化信息
 */
func (UserAndRightBase) TableInfo() *dbinfo.TableInfo {
	return &dbinfo.TableInfo{
		GsDbName:          "",
		GsTableName:       "UserAndRight",
		GsKeyName:         "iId",
		GiKeyLen:          20,
		GbAutoKey:         true,
		GbHasPid:          false,
		GbHasPath:         false,
		GbHasRecordKey:    false,
		GbHasMemo:         true,
		GbHasCreator:      true,
		GbHasCreateDate:   true,
		GbHasModifieder:   true,
		GbHasModifiedDate: true,
		GbHasState:        true,
		GbHasIndex:        true,
		GbHasVersion:      true,
		GbHasPassword:     false,
		GbHasSign:         false,
		GbHasOnlyign:      false,
		GbHasDelSign:      false,
	}
}
