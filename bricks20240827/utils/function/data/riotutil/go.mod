module gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data/riotutil

go 1.21.6

require (
	gitee.com/tomatomeatman/golang-repository/bricks/model/emity v0.0.0-20240809004902-3c7c04818bc0
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	github.com/go-ego/riot v0.0.0-20201013133145-f4c30acb3704
)

require (
	github.com/AndreasBriese/bbloom v0.0.0-20190825152654-46b345b51c96 // indirect
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/StackExchange/wmi v0.0.0-20190523213315-cbe66965904d // indirect
	github.com/cespare/xxhash v1.1.0 // indirect
	github.com/dgraph-io/badger v1.6.1 // indirect
	github.com/dgraph-io/ristretto v0.0.2 // indirect
	github.com/dustin/go-humanize v1.0.0 // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/go-ego/cedar v0.10.0 // indirect
	github.com/go-ego/gpy v0.31.0 // indirect
	github.com/go-ego/gse v0.50.3 // indirect
	github.com/go-ego/murmur v0.10.0 // indirect
	github.com/go-ole/go-ole v1.2.4 // indirect
	github.com/go-vgo/gt/conf v0.0.0-20200606140533-a397c46789df // indirect
	github.com/go-vgo/gt/info v0.0.0-20200606140533-a397c46789df // indirect
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/pelletier/go-toml v1.8.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/shirou/gopsutil v2.20.5+incompatible // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
	go.etcd.io/bbolt v1.3.4 // indirect
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9 // indirect
	golang.org/x/sys v0.0.0-20200610111108-226ff32320da // indirect
	google.golang.org/protobuf v1.24.0 // indirect
)
