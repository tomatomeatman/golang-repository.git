package maputil

// 移除指定键
func RemoveData(data map[string]interface{}, filter []string) {
	for _, value := range filter {
		delete(data, value)
	}
}

// 判断是否存在指定键
func HasName(data map[string]interface{}, name string) bool {
	_, ok := data[name]
	return ok
}
