package uuidutil

import (
	"strings"

	uuid "github.com/satori/go.uuid"
)

// 获取UUID大写
func Get() string {
	result := strings.Replace(uuid.NewV4().String(), "-", "", -1)
	return strings.ToUpper(result)
}

// 获取UUID小写
func GetLowerCase() string {
	result := strings.Replace(uuid.NewV4().String(), "-", "", -1)
	return result
}
