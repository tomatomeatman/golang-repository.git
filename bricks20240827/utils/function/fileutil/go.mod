module gitee.com/tomatomeatman/golang-repository/bricks/utils/function/fileutil

go 1.21.6

require (
	gitee.com/tomatomeatman/golang-repository/bricks/model/emity v0.0.0-20240813025147-3922cfd1991b
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
)
