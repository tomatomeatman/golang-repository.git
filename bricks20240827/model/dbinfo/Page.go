package dbinfo

//分页数据信息对象,常用于前端与后台列表数据交互
type Page struct {
	GiCurrent   int                      `json:"iCurrent"`   //当前页
	GiSize      int                      `json:"iSize"`      //每页显示记录
	GiCountRow  int                      `json:"iCountRow"`  //总记录数
	GiCountPage int                      `json:"iCountPage"` //总页数
	Record      []map[string]interface{} `json:"Record"`     //本页的数据列表
}

//创建分页信息
func (p *Page) New() *Page {
	p.GiCurrent = 1
	p.GiSize = 10
	p.GiCountRow = 0
	p.GiCountPage = 0

	return p
}

//设置分页信息
func (p *Page) Set(start, size, countRow int, record []map[string]interface{}) *Page {
	if start < 1 {
		start = 0
	}

	if size < 1 {
		size = 10
	}

	if countRow < 0 {
		countRow = 0
	}

	current := start / size

	countPage := 1
	if (countRow > 0) && (size > 0) {
		if countRow%size == 0 {
			countPage = countRow / size
		} else {
			countPage = countRow/size + 1
		}
	}

	p.GiCurrent = current
	p.GiSize = size
	p.GiCountRow = countRow
	p.GiCountPage = countPage
	p.Record = record

	return p

	// result := Page{
	// 	1,
	// 	10,
	// 	0,
	// 	0,
	// 	[]interface{}{},
	// }

	// return result
}

//设置数据列表
func (p *Page) SetRecord(record []map[string]interface{}) *Page {
	p.Record = record
	return p
}
