module gitee.com/tomatomeatman/golang-repository/bricks/model/dbinfo

go 1.21.6

require github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575

require gitee.com/tomatomeatman/golang-repository/bricks/model/emity v0.0.0-20240813021511-fa5e1cf12bb4
