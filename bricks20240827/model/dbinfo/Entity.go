package dbinfo

import (
	"reflect"
	"strings"
)

// 实体接口定义,默认方法
type EntityDefaultFunc interface {
	HasColumnNames(entiy Entity, name string) bool //判断是否存在字段名
	AllDataInfo(entiy Entity) []*DataInfo          //取所有数据结构信息
	BaseColumnNames() string                       //结构体映射表的字段名串
	BaseColumnNameList() []string                  //结构体映射表的字段名集合
	GetDataInfo(name string) *DataInfo             //取数据结构信息
	NewList() interface{}                          //创建类型未知的结构实体集合
	SetDefault()                                   //设置默认值
	BaseEntity() Entity                            //取基础实体,用于在子类(嵌套结构体)时同样获得基类
	New() Entity                                   //创建结构实体,并赋予默认值
	TrimFields()                                   //去除字符类型属性前后空格
}

// 实体接口定义,用于规范实体结构体
type Entity interface {
	EntityDefaultFunc
	TableName() string     //结构体映射表名
	TableInfo() *TableInfo //结构体映射表简化信息
}

type BaseFunc struct {
	owner Entity //指定上级
}

/**
 * 判断是否存在字段名
 */
func (bf *BaseFunc) HasColumnNames(entiy Entity, name string) bool {
	return strings.Contains(entiy.BaseColumnNames(), name)
}

/**
 * 取所有数据结构信息
 */
func (bf *BaseFunc) AllDataInfo(entiy Entity) []*DataInfo {
	array := entiy.BaseColumnNameList()

	result := []*DataInfo{}
	for _, key := range array {
		result = append(result, entiy.GetDataInfo(key))
	}

	return result
}

// 取基础实体,用于在子类(嵌套结构体)时同样获得基类
func (bf *BaseFunc) BaseEntity() Entity {
	return nil //调用子类的实现方法
}

/**
 * 结构体映射表的字段名串
 */
func (bf *BaseFunc) BaseColumnNames() string {
	return "" //子类实现
}

/**
 * 结构体映射表的字段名集合
 */
func (bf *BaseFunc) BaseColumnNameList() []string {
	return nil //子类实现
}

/**
 * 取数据结构信息
 */
func (bf *BaseFunc) GetDataInfo(name string) *DataInfo {
	return nil //子类实现
}

/**
 * 创建结构实体集合
 */
func (bf *BaseFunc) NewList() interface{} {
	return nil //子类实现
}

/**
 * 设置默认值
 */
func (bf *BaseFunc) SetDefault() {
	//子类实现
}

// 创建结构实体,并赋予默认值
func (bf *BaseFunc) New() Entity {
	return nil //子类实现
}

/**
 * 去除字符类型属性前后空格
 */
func (bf *BaseFunc) TrimFields() {
	v := reflect.ValueOf(bf).Elem()

	trimStringFields(v) // 调用函数清除字符串字段的前后空格
}

// 通用函数，用于递归处理结构体及其匿名字段中的字符串
func trimStringFields(v reflect.Value) {
	for i := 0; i < v.NumField(); i++ {
		field := v.Field(i)
		fieldType := v.Type().Field(i)

		// 检查字段是否为匿名结构体
		if fieldType.Anonymous {
			trimStringFields(field)
			continue
		}

		if field.Kind() == reflect.String {
			field.SetString(strings.TrimSpace(field.String())) // 清除前后空格
			continue
		}

		if field.Kind() == reflect.Struct {
			trimStringFields(field) // 如果字段是结构体，则递归调用trimStringFields
			continue
		}
	}
}
