package wailsutil

import (
	"os"
	"path/filepath"
	"strings"

	"github.com/MakeNowJust/hotkey"
	"github.com/energye/systray"
)

var iconInfos map[string][]byte

type SystemTray struct {
	appFun *AppFun
	sTitle string
}

func (st SystemTray) Run(appFun *AppFun, IconInfos map[string][]byte) {
	iconInfos = IconInfos

	dir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	appConfigFile := dir + "/config/app.ini"

	st.sTitle = ReadConfig(appConfigFile, "System", "Title")
	FormHotkey := ReadConfig(appConfigFile, "App", "FormHotkey")

	st.appFun = appFun
	st.appFun.ClearTrayFun = st.onExit

	st.SetHotkey(FormHotkey) //设置热键

	systray.Run(st.onReady, st.onExit) //运行托盘菜单
}

func (st *SystemTray) onExit() {
	st.appFun.CloseAppNoQuest()
}

func (st *SystemTray) onReady() {
	systray.SetTemplateIcon(iconInfos["IconData"], iconInfos["IconData"])
	systray.SetTitle("GNotepadWails")
	systray.SetTooltip(st.sTitle)

	showWindow := systray.AddMenuItem("显示主界面", "显示主界面")
	showWindow.SetIcon(iconInfos["IconShow"])
	hideWindow := systray.AddMenuItem("隐藏主界面", "隐藏主界面")
	hideWindow.SetIcon(iconInfos["IconHide"])

	if st.appFun.WindwIsShow() {
		hideWindow.Show()
		showWindow.Hide()
	} else {
		showWindow.Show()
		hideWindow.Hide()
	}

	showWindow.Click(func() {
		st.appFun.WindowShow()
		hideWindow.Show()
		showWindow.Hide()
	})

	hideWindow.Click(func() {
		st.appFun.WindowHide()
		showWindow.Show()
		hideWindow.Hide()
	})

	systray.AddSeparator()

	topWindow := systray.AddMenuItem("窗口置顶", "窗口置顶状态")
	//topWindow.SetIcon(IconWait)
	topWindow.Click(func() {
		bl := !topWindow.Checked()
		if bl {
			topWindow.Check()
		} else {
			topWindow.Uncheck()
		}

		st.appFun.WindowOnTop(bl)
		if bl {
			topWindow.SetTitle("窗口置顶")
		} else {
			topWindow.SetTitle("窗口不置顶")
		}
	})

	systray.AddSeparator()

	mQuit := systray.AddMenuItem("退出程序", "彻底关闭程序")
	mQuit.SetIcon(iconInfos["IconExit"])
	mQuit.Click(func() {
		systray.Quit()
	})

	// 点击事件
	systray.SetOnClick(func(menu systray.IMenu) {
		if st.appFun.WindwIsShow() {
			st.appFun.WindowHide()
			showWindow.Show()
			hideWindow.Hide()
		} else {
			st.appFun.WindowShow()
			hideWindow.Show()
			showWindow.Hide()
		}
	})

	// 右击事件
	systray.SetOnRClick(func(menu systray.IMenu) {
		if st.appFun.WindwIsShow() {
			hideWindow.Show()
			showWindow.Hide()
		} else {
			showWindow.Show()
			hideWindow.Hide()
		}

		menu.ShowMenu()
	})

	// 双击事件
	// systray.SetOnDClick(func(menu systray.IMenu) {
	// 	fmt.Println("SetOnDClick")
	// })

}

// 设置热键
func (st *SystemTray) SetHotkey(FormHotkey string) {
	if FormHotkey == "" {
		return
	}

	array := strings.Split(strings.ToUpper(FormHotkey), "+")
	if len(array) < 2 {
		return
	}

	//启动快捷键监听
	go func() {
		hkey := hotkey.New()

		var mods hotkey.Modifier
		switch array[0] {
		case "CTRL":
			mods = hotkey.Ctrl
		case "ALT":
			mods = hotkey.Alt
		case "SHIFT":
			mods = hotkey.Shift
		case "WIN":
			mods = hotkey.Win
		default:
			mods = hotkey.None
		}

		var vk uint32
		switch array[1] {
		case "SPACE":
			vk = hotkey.SPACE
		case "BACK":
			vk = hotkey.BACK
		default:
			vk = uint32(rune(array[1][0]))
		}

		hkey.Register(mods, vk, func() {
			if st.appFun.WindwIsShow() {
				st.appFun.WindowHide()
			} else {
				st.appFun.WindowShow()
			}
		})
	}()
}
