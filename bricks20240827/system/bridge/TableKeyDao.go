package bridge

import (
	"strings"
	"sync"

	"gitee.com/tomatomeatman/golang-repository/bricks/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data/integerutil"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data/stringutil"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/gorm"
	Log "github.com/cihub/seelog"
)

var (
	syncTablekeyMap      = map[string]*sync.Mutex{} //序列锁集合
	syncCountTablekeyMap = map[string]int{}         //序列锁引用计数值集合,当值为0时同时清理'序列锁集合'
	sDbName              string                     //数据库名
)

func init() {
	if (app.AppUtil{}.IsNotCloudSystem()) { //禁用桥接
		return
	}

	sDbName = app.AppUtil{}.ReadConfigKey("DbVariables", "BaseSystem", "BaseSystem").(string)
}

/**
 * 记录编号序列管理表TableKey表基本业务操作结构体
 */
type TableKeyDao struct {
}

/**
 * 取各表(或序列)的新Id
 * @param formatLong 格式化长度(不足长度+0)
 * @param seriesName 表名或序列名
 * @return
 */
func (dao TableKeyDao) GetNewId(formatLong int, seriesName string) string {
	lock, ok := syncTablekeyMap[seriesName]
	if !ok {
		syncTablekeyMap[seriesName] = &sync.Mutex{}
		syncCountTablekeyMap[seriesName] = 1 //计数值
		lock = syncTablekeyMap[seriesName]
	} else {
		syncCountTablekeyMap[seriesName] = syncCountTablekeyMap[seriesName] + 1 //计数值
	}

	lock.Lock() //加锁

	iCount := dao.newValue(seriesName)
	if iCount != 1 {
		iCount = dao.addTable(seriesName)
		if iCount != 0 {
			syncCountTablekeyMap[seriesName] = syncCountTablekeyMap[seriesName] - 1 //计数值-1
			if syncCountTablekeyMap[seriesName] < 1 {                               //计数值为0,剔除
				delete(syncCountTablekeyMap, seriesName)
				delete(syncTablekeyMap, seriesName)
			}

			lock.Unlock() //解锁
			return stringutil.SupplyZero(formatLong, 1)
		}

		syncCountTablekeyMap[seriesName] = syncCountTablekeyMap[seriesName] - 1 //计数值-1
		if syncCountTablekeyMap[seriesName] < 1 {                               //计数值为0,剔除
			delete(syncCountTablekeyMap, seriesName)
			delete(syncTablekeyMap, seriesName)
		}

		lock.Unlock() //解锁
		return "0"    //说明是取值失败
	}

	sId := dao.findLastId(seriesName)
	if "0" == sId {
		syncCountTablekeyMap[seriesName] = syncCountTablekeyMap[seriesName] - 1 //计数值-1
		if syncCountTablekeyMap[seriesName] < 1 {                               //计数值为0,剔除
			delete(syncCountTablekeyMap, seriesName)
			delete(syncTablekeyMap, seriesName)
		}

		lock.Unlock() //解锁
		return "0"    //说明是取值失败
	}

	syncCountTablekeyMap[seriesName] = syncCountTablekeyMap[seriesName] - 1 //计数值-1
	if syncCountTablekeyMap[seriesName] < 1 {                               //计数值为0,剔除
		delete(syncCountTablekeyMap, seriesName)
		delete(syncTablekeyMap, seriesName)
	}

	lock.Unlock() //解锁

	result := stringutil.SupplyZero(formatLong, sId)

	return result
}

/**
 * 取各表的一批新Id
 * @param formatLong 格式化长度(不足长度+0)
 * @param seriesName 表名或序列名
 * @param size 数量
 * @return
 */
func (dao TableKeyDao) GetNewIds(formatLong int, seriesName string, size int) []string {
	lock, ok := syncTablekeyMap[seriesName]
	if !ok {
		syncTablekeyMap[seriesName] = &sync.Mutex{}
		syncCountTablekeyMap[seriesName] = 1 //计数值
		lock = syncTablekeyMap[seriesName]
	} else {
		syncCountTablekeyMap[seriesName] = syncCountTablekeyMap[seriesName] + 1 //计数值
	}

	lock.Lock() //加锁

	iCount := dao.newValues(seriesName, size)
	if iCount != 1 {
		iCount = dao.addTableValues(seriesName, size)
	}

	if iCount != 1 {
		syncCountTablekeyMap[seriesName] = syncCountTablekeyMap[seriesName] - 1 //计数值-1
		if syncCountTablekeyMap[seriesName] < 1 {                               //计数值为0,剔除
			delete(syncCountTablekeyMap, seriesName)
			delete(syncTablekeyMap, seriesName)
		}

		lock.Unlock() //解锁
		return nil    //说明是取值失败
	}

	sLastId := dao.findLastId(seriesName)

	syncCountTablekeyMap[seriesName] = syncCountTablekeyMap[seriesName] - 1 //计数值-1
	if syncCountTablekeyMap[seriesName] < 1 {                               //计数值为0,剔除
		delete(syncCountTablekeyMap, seriesName)
		delete(syncTablekeyMap, seriesName)
	}

	lock.Unlock() //解锁

	iLastId := integerutil.ToInt(sLastId)

	result := []string{}
	for i := 0; i < size; i++ {
		vNewId := stringutil.SupplyZero(formatLong, iLastId+i+1)
		result = append(result, vNewId)
	}

	return result
}

/**
 * 重置
 * @param seriesName 表名或序列名
 * @return
 */
func (dao TableKeyDao) Reset(seriesName string) string {
	iCount := dao.reset(seriesName)

	if iCount < 1 {
		return "0"
	}

	return "1"
}

/**
 * 更新到新sValue值
 * @param sType
 * @return
 */
func (dao TableKeyDao) newValue(seriesName string) int {
	sql := "UPDATE ${BaseSystem}TableKey SET sValue = (sValue +1) ,iVersion = (iVersion +1) WHERE sType = '#{sType}'"
	sql = strings.Replace(sql, "${BaseSystem}", sDbName, -1)
	sql = strings.Replace(sql, "#{sType}", seriesName, -1)

	dbResult := gorm.Exec(sql)
	if dbResult.Error != nil {
		Log.Error("更新数据发生异常:", dbResult.Error)
		return 0
	}

	return integerutil.ToInt(dbResult.RowsAffected, 0)
}

/**
 * 取最后的id值
 * @param sValue
 * @return
 */
func (dao TableKeyDao) findLastId(seriesName string) string {
	sql := "SELECT sValue FROM ${BaseSystem}TableKey WHERE sType = '#{sType}'"
	sql = strings.Replace(sql, "${BaseSystem}", sDbName, -1)
	sql = strings.Replace(sql, "#{sType}", seriesName, -1)

	sValue := "0"
	dbResult := gorm.Raw(sql).Find(&sValue)
	if dbResult.Error != nil {
		Log.Error("更新数据发生异常:", dbResult.Error)
		return sValue
	}

	return sValue
}

/**
 * 添加表字段信息
 * @param seriesName
 * @return
 */
func (dao TableKeyDao) addTable(seriesName string) int {
	sql := `
	INSERT INTO ${BaseSystem}TableKey (
		iId
		,sType
		,sValue
		,iIndex
		,iVersion
	)
	SELECT
		IFNULL(MAX(iId), 0) +1
		,'#{sType}'
		,'1'
		,1
		,1
	FROM ${BaseSystem}TableKey`
	sql = strings.Replace(sql, "${BaseSystem}", sDbName, -1)
	sql = strings.Replace(sql, "#{sType}", seriesName, -1)

	dbResult := gorm.Exec(sql)
	if dbResult.Error != nil {
		Log.Error("更新数据发生异常:", dbResult.Error)
		return 0
	}

	return integerutil.ToInt(dbResult.RowsAffected, 0)
}

/**
 * 更新到新的一批sValue值
 * @param seriesName
 * @param size
 * @return
 */
func (dao TableKeyDao) newValues(seriesName string, size int) int {
	sql := `UPDATE ${BaseSystem}TableKey SET
				sValue = (sValue +1 + #{size})
				,iVersion = (iVersion +1 + #{size})
			WHERE sType = #{sType}`
	sql = strings.Replace(sql, "${BaseSystem}", sDbName, -1)
	sql = strings.Replace(sql, "#{size}", stringutil.ToStr(size), -1)
	sql = strings.Replace(sql, "#{sType}", seriesName, -1)

	dbResult := gorm.Exec(sql)
	if dbResult.Error != nil {
		Log.Error("更新数据发生异常:", dbResult.Error)
		return 0
	}

	return integerutil.ToInt(dbResult.RowsAffected, 0)
}

/**
 * 添加表字段信息,并设置初始值
 * @param seriesName
 * @param size
 * @return
 */
func (dao TableKeyDao) addTableValues(seriesName string, size int) int {
	sql := `
	INSERT INTO ${BaseSystem}TableKey (
		iId
		,sType
		,sValue
		,iIndex
		,iVersion
	)
	SELECT
		IFNULL(MAX(iId), 0) +1
		,'#{sType}'
		,#{size}
		,1
		,#{size}
	FROM ${BaseSystem}TableKey`
	sql = strings.Replace(sql, "${BaseSystem}", sDbName, -1)
	sql = strings.Replace(sql, "#{size}", stringutil.ToStr(size), -1)
	sql = strings.Replace(sql, "#{sType}", seriesName, -1)

	dbResult := gorm.Exec(sql)
	if dbResult.Error != nil {
		Log.Error("更新数据发生异常:", dbResult.Error)
		return 0
	}

	return integerutil.ToInt(dbResult.RowsAffected, 0)
}

/**
 * 重置
 * @param seriesName
 * @return
 */
func (dao TableKeyDao) reset(seriesName string) int {
	sql := `UPDATE ${BaseSystem}TableKey SET
			sValue = 1
			,iVersion = (iVersion +1)
		WHERE sType = '#{sType}'`
	sql = strings.Replace(sql, "${BaseSystem}", sDbName, -1)
	sql = strings.Replace(sql, "#{sType}", seriesName, -1)

	dbResult := gorm.Exec(sql)
	if dbResult.Error != nil {
		Log.Error("更新数据发生异常:", dbResult.Error)
		return 0
	}

	return integerutil.ToInt(dbResult.RowsAffected, 0)
}
