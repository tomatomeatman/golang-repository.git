package bridge

import (
	"gitee.com/tomatomeatman/golang-repository/bricks/model/globalvariable"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/app"
)

/**
 * 记录编号序列管理表TableKey表基本业务操作结构体
 */
type TableKeyService struct {
	app.ServiceBaseFunc
}

/**
 * 初始化
 */
func init() {
	if (app.AppUtil{}.IsNotCloudSystem()) { //禁用桥接
		return
	}

	go globalvariable.RegisterVariable("TableKeyService", TableKeyService{})
}

/**
 * 取各表(或序列)的新Id
 * @param formatLong 格式化长度(不足长度+0)
 * @param seriesName 表名或序列名
 * @return
 */
func (ts TableKeyService) GetNewId(formatLong int, seriesName string) string {
	return TableKeyDao{}.GetNewId(formatLong, seriesName)
}

/**
 * 取各表的一批新Id
 * @param formatLong 格式化长度(不足长度+0)
 * @param seriesName 表名或序列名
 * @param size 数量
 * @return
 */
func (ts TableKeyService) GetNewIds(formatLong int, seriesName string, size int) []string {
	return TableKeyDao{}.GetNewIds(formatLong, seriesName, size)
}

/**
 * 重置
 * @param seriesName 表名或序列名
 * @return
 */
func (ts TableKeyService) Reset(seriesName string) string {
	return TableKeyDao{}.Reset(seriesName)
}
