package bridge

import (
	"gitee.com/tomatomeatman/golang-repository/bricks/model/emity"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/ginutil"
)

type SystemParamsService struct {
}

var (
	systemParamsServiceName = "" //桥接到登录服务的服务名,默认GBaseSystemServer
	//systemParamsServiceName  = "" //桥接到登录服务的服务密钥
)

// 初始化
func init() {
	if (app.AppUtil{}.IsNotCloudSystem()) { //禁用桥接
		return
	}

	systemParamsServiceName = app.AppUtil{}.ReadConfigKey("CloudServer", "BaseServerName", "GBaseSystemServer").(string)
	//systemParamsServiceName = app.AppUtil{}.ReadConfigKey("CloudServer", "BaseServerKey", "").(string)
}

/**
 * 取所有免拦截系统参数对象集合
 * @return
 */
func (service SystemParamsService) FindByNoIntercept(ctx ginutil.Context) *emity.MsgEmity {
	me := BridgeDao{}.Post(ctx, systemParamsServiceName, "/system/params/insidevisit/find/not/intercept", nil, &emity.MsgEmity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*emity.MsgEmity)
}
