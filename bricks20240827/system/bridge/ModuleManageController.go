package bridge

import (
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/ginutil"
)

// @Controller 模块管理控制器
type ModuleManageController struct {
}

/**
 * 初始化
 */
func init() {
	if (app.AppUtil{}.IsNotCloudSystem()) { //禁用桥接
		return
	}

	//-- 接口注册 --//
	go ginutil.RegisterController("/module/manage/find/tree", ginutil.POST, ModuleManageController{}.FindByTree)
}

// #region @Api {title=查询树形结构数据}
// @param {name=sGroupName dataType=string paramType=query explain=指定节点名 required=false}
// @return {type=json explain=返回树型数据}
// @RequestMapping {name=FindByTree type=POST value=/module/manage/find/tree}
// #endregion
func (control ModuleManageController) FindByTree(ctx ginutil.Context) interface{} {
	return ModuleManageService{}.FindByTree(ctx)
}
