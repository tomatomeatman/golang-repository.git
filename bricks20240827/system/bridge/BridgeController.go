package bridge

import (
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/ginutil"
)

/**
 * 桥接代理请求控制器
 */
type BridgeController struct{}

/**
 * 初始化
 */
func init() {
	if (app.AppUtil{}.IsNotCloudSystem()) { //禁用桥接
		return
	}

	//-- 接口注册 --//
	go ginutil.RegisterController("/proxy/*proxy", ginutil.GET, BridgeController{}.ProxyGet)
	go ginutil.RegisterController("/proxy/*proxy", ginutil.POST, BridgeController{}.ProxyPost)
}

func (control BridgeController) ProxyGet(ctx ginutil.Context) interface{} {
	return BridgeDao{}.ProxyGet(ctx)
}

func (control BridgeController) ProxyPost(ctx ginutil.Context) interface{} {
	return BridgeDao{}.ProxyPost(ctx)
}
