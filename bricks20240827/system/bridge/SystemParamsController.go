package bridge

import (
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/ginutil"
)

// @Controller 桥接服务-系统参数桥接操作接口
type SystemParamsController struct {
}

/**
 * 初始化
 */
func init() {
	if (app.AppUtil{}.IsNotCloudSystem()) { //禁用桥接
		return
	}

	//-- 接口注册 --//
	go ginutil.RegisterController("/system/params/insidevisit/find/not/intercept", ginutil.POST, SystemParamsController{}.FindByNoIntercept)
}

// #region @Api {title=取所有免拦截系统参数对象集合}
// @return {type=json explain=返回分页数据}
// @RequestMapping {name=FindByPage type=POST value=/system/params/insidevisit/find/not/intercept}
// #endregion
func (control SystemParamsController) FindByNoIntercept(ctx ginutil.Context) interface{} {
	return SystemParamsService{}.FindByNoIntercept(ctx)
}
