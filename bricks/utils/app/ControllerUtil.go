package app

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	uu "net/url"
	"reflect"
	"strings"
	"time"

	"gitee.com/tomatomeatman/golang-repository/bricks/model/dbinfo"
	"gitee.com/tomatomeatman/golang-repository/bricks/model/msgentity"
	"gitee.com/tomatomeatman/golang-repository/bricks/model/set"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data/timeutil"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/urlutil"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/ginutil"
	Log "github.com/cihub/seelog"
)

type ControllerUtil struct{}

/**
 * 新增
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) Add(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	me := urlutil.GetParams(ctx.Request, entity)
	if !me.Gsuccess {
		return me
	}

	params := me.Gdata.(map[string]interface{})
	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".Add"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me = service.Add(ctx, entity, params)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 批量新增
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) Adds(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	objStr := urlutil.GetParam(ctx.Request, "objs", "").(string)
	if objStr == "" {
		return msgentity.Err(9001, "未能获取'objs'参数")
	}

	var objs []map[string]interface{}
	err := json.Unmarshal([]byte(objStr), &objs)
	if err != nil {
		Log.Error("参数'objs'转换出错：", err)
		return msgentity.Err(9002, "参数'objs'转换出错")
	}

	if len(objs) < 1 {
		return msgentity.Err(9003, "参数'objs'转换后没有数据")
	}

	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	entitys := make([]dbinfo.Entity, len(objs))
	for i := 0; i < len(objs); i++ {
		entitys = append(entitys, entity.New())
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".Adds"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me := service.Adds(ctx, entitys, objs)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 新增树节点
 * @param ctx http请求对象
 * @param entity 检查用数据结构
 * @return
 */
func (controlUtil ControllerUtil) AddNode(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	me := urlutil.GetParams(ctx.Request, entity)
	if !me.Gsuccess {
		return me
	}

	params := me.Gdata.(map[string]interface{})
	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".AddNode"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me = service.AddNode(ctx, entity, params)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 删除
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) Del(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	id := urlutil.GetParamToId(ctx.Request, dbinfo.EntityKeyName(entity)) //取请求参数中的记录编号

	if id == nil || (fmt.Sprintf("%v", id) == "") {
		id = urlutil.GetParamToId(ctx.Request, "id")

		if id == nil || (fmt.Sprintf("%v", id) == "") {
			return msgentity.Err(9012, "未能获取'", dbinfo.EntityKeyName(entity), "'参数")
		}
	}

	iVersion := urlutil.GetParamToVersion(ctx.Request, dbinfo.EntityHasVersion(entity), dbinfo.TableVersionName) //取请求参数中的版本号

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".Del"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me := service.Del(ctx, entity, id, iVersion)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 修改
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return msgentity.MsgEntity
 */
func (controlUtil ControllerUtil) Edit(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	id := urlutil.GetParamToId(ctx.Request, dbinfo.EntityKeyName(entity))                                        //取请求参数中的记录编号
	iVersion := urlutil.GetParamToVersion(ctx.Request, dbinfo.EntityHasVersion(entity), dbinfo.TableVersionName) //取请求参数中的版本号

	me := urlutil.GetParams(ctx.Request, entity)
	if !me.Gsuccess {
		return me
	}

	data := me.Gdata.(map[string]interface{})
	if id == nil || (fmt.Sprintf("%v", id) == "") {
		id = data[dbinfo.EntityKeyName(entity)]

		if id == nil || (fmt.Sprintf("%v", id) == "") {
			return msgentity.Err(1001, "未能获取'", dbinfo.EntityKeyName(entity), "'参数")
		}
	}

	if iVersion == -1 {
		if val, ok := data[dbinfo.TableVersionName]; ok {
			iVersion = val.(int)
		}
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".Edit"

	me = AopUtil{}.CallBeforeFunc(name, ctx)
	if !me.Gsuccess {
		return me
	}

	me = service.Edit(ctx, entity, id, iVersion, data)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 批量修改
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return msgentity.MsgEntity
 */
func (controlUtil ControllerUtil) Edits(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	me := urlutil.GetParams(ctx.Request, entity)
	if !me.Gsuccess {
		return me
	}

	datas := me.Gdata.([]map[string]interface{})

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".Edits"

	me = AopUtil{}.CallBeforeFunc(name, ctx)
	if !me.Gsuccess {
		return me
	}

	me = service.Edits(ctx, entity, datas)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 根据主键查询数据
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) FindById(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	id := urlutil.GetParamToId(ctx.Request, dbinfo.EntityKeyName(entity)) //取请求参数中的记录编号

	if (nil == id) || (fmt.Sprintf("%v", id) == "") {
		id = urlutil.GetParamToId(ctx.Request, "id")
	}

	if nil == id || (fmt.Sprintf("%v", id) == "") {
		return msgentity.Err(1001, "记录编号为空")
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindById"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me := service.FindById(ctx, entity, id)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 查询所有数据
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) FindAll(ctx ginutil.Context, control Controller, attachWhere ...set.Set) *msgentity.MsgEntity {
	whereInfo := []dbinfo.WhereInfo{}
	if nil != attachWhere && len(attachWhere) > 0 {
		for i := 0; i < len(attachWhere); i++ {
			whereInfo = append(whereInfo, dbinfo.WhereInfo{Name: attachWhere[i].Key, Value: attachWhere[i].Value})
		}
	}

	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindAll"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me := service.FindAll(ctx, entity, whereInfo)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 查询时间范围内数据
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) FindByDate(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	sDateSt := urlutil.GetParam(ctx.Request, "sDateSt", "").(string) //记录开始时间
	sDateEd := urlutil.GetParam(ctx.Request, "sDateEd", "").(string) //记录结束时间

	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindByDate"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me := service.FindByDate(ctx, entity, sDateSt, sDateEd)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 查找指定行数据
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) FindByRow(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	id := urlutil.GetParamToId(ctx.Request, dbinfo.EntityKeyName(entity)) //取请求参数中的记录编号

	if id == nil || (fmt.Sprintf("%v", id) == "") {
		id = urlutil.GetParamToId(ctx.Request, "id")

		if id == nil || (fmt.Sprintf("%v", id) == "") {
			return msgentity.Err(1001, "未能获取'", dbinfo.EntityKeyName(entity), "'参数")
		}
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindByRow"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me := service.FindByRow(ctx, entity, id)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 查询分页数据
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) FindByPage(ctx ginutil.Context, control Controller, injectionCondition ...set.Set) *msgentity.MsgEntity {
	me := urlutil.GetBody(ctx.Request, dbinfo.FindByPageParam{})
	if !me.Gsuccess {
		return me
	}

	findByPageParam := me.Gdata.(dbinfo.FindByPageParam)

	if findByPageParam.Gpage.GiSize < 1 {
		findByPageParam.Gpage.GiSize = 10
	}

	if findByPageParam.Gpage.GiCurrent < 1 {
		findByPageParam.Gpage.GiCurrent = 1
	}

	if (nil != injectionCondition) || (len(injectionCondition) > 0) { //将注入条件覆盖到查询条件
		if nil == findByPageParam.Gcondition {
			findByPageParam.Gcondition = map[string]interface{}{}
		}

		for _, set := range injectionCondition {
			findByPageParam.Gcondition.(map[string]interface{})[set.Key] = set.Value
		}
	}

	sLikeStr := strings.TrimSpace(findByPageParam.GsLikeStr) //全文检索条件
	if sLikeStr != "" {                                      //存在全文检索条件则需要考虑时间范围的问题
		sLikeDateSt := strings.TrimSpace(findByPageParam.GsLikeDateSt) //模糊查询记录修改时间范围条件-开始
		sLikeDateEd := strings.TrimSpace(findByPageParam.GsLikeDateEd) //模糊查询记录修改时间范围条件-结束

		var dLikeDateEd time.Time
		if sLikeDateEd == "" { //如果结束时间为空,则当前时间就是结束时间
			dLikeDateEd = time.Now()
			sLikeDateEd = dLikeDateEd.Format("2006-01-02 15:04:05")
		} else {
			dLikeDateEd = timeutil.ToDate(sLikeDateEd)
		}

		if sLikeDateSt == "" { //如果开始时间为空,则用结束时间-时间限制
			iLikeTimeLimit := control.LikeTimeLimit()
			sLikeDateSt = timeutil.AddDay(dLikeDateEd, -iLikeTimeLimit).Format("2006-01-02 15:04:05")
		}

		findByPageParam.GsLikeDateSt = sLikeDateSt
		findByPageParam.GsLikeDateEd = sLikeDateEd
	}

	findByPageParam.GsHideFields = strings.TrimSpace(findByPageParam.GsHideFields)

	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindByPage"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me = service.FindByPage(ctx, entity, findByPageParam)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 取所有参数,并转换成对应实体属性类型(map[string]interface{})
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) GetParams(ctx ginutil.Context, entity interface{}) *msgentity.MsgEntity {
	if reflect.TypeOf(entity).Kind() == reflect.Struct {
		br, _ := io.ReadAll(ctx.Request.Body)
		ctx.Request.Body.Close()
		ctx.Request.Body = io.NopCloser(bytes.NewBuffer(br))
		json.NewDecoder(bytes.NewBuffer(br)).Decode(entity)

		// fmt.Println(reflect.TypeOf(entity).String()) // map[string]interface {}

		return msgentity.Success(entity, "转换结束")
	}

	return urlutil.GetParams(ctx.Request, entity)
}

/**
 * 读取树形结构数据
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) FindByTree(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	sGroupColumn := urlutil.GetParam(ctx.Request, "sGroupColumn", "").(string)
	sGroupName := urlutil.GetParam(ctx.Request, "sGroupName", "").(string)
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindByTree"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me := service.FindByTree(ctx, entity, sGroupColumn, sGroupName)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 根据字段名取指定记录编号的数据库表中对应字段的值
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) GetValueByFieldName(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	temp := urlutil.GetParam(ctx.Request, "fieldNames", "").(string)
	fieldNames := strings.Split(temp, ",")

	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	id := urlutil.GetParamToId(ctx.Request, dbinfo.EntityKeyName(entity)) //取请求参数中的记录编号
	if id == nil || (fmt.Sprintf("%v", id) == "") {
		id = urlutil.GetParamToId(ctx.Request, "id")
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".GetValueByFieldName"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me := service.GetValueByFieldName(ctx, entity, id, fieldNames)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 根据字段名取指定记录编号的数据库表中对应字段的值
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) GetValueByField(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	id := urlutil.GetParamToId(ctx.Request, dbinfo.EntityKeyName(entity)) //取请求参数中的记录编号
	if id == nil || (fmt.Sprintf("%v", id) == "") {
		id = urlutil.GetParamToId(ctx.Request, "id")

		if id == nil || (fmt.Sprintf("%v", id) == "") {
			return msgentity.Err(9012, "未能获取'", dbinfo.EntityKeyName(entity), "'参数")
		}
	}

	fieldName := urlutil.GetParam(ctx.Request, "fieldName", "").(string)

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".GetValueByField"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me := service.GetValueByField(ctx, entity, id, fieldName)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 根据关键值取对象集合
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) FindByKey(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	me := urlutil.GetParams(ctx.Request, entity)
	if !me.Gsuccess {
		return me
	}

	params := me.Gdata.(map[string]interface{})
	whereInfo := []dbinfo.WhereInfo{}
	for k, v := range params {
		info := dbinfo.WhereInfo{Name: k, Value: v}
		whereInfo = append(whereInfo, info)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindByKey"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me = service.FindByKey(ctx, entity, whereInfo)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 根据关键值取对象集合中的第一个
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) FindOneByKey(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	temp := urlutil.GetParam(ctx.Request, "fields", "").(string)
	fields := strings.Split(temp, ",")

	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	me := urlutil.GetParams(ctx.Request, entity)
	if !me.Gsuccess {
		return me
	}

	params := me.Gdata.(map[string]interface{})
	whereInfo := []dbinfo.WhereInfo{}
	for k, v := range params {
		info := dbinfo.WhereInfo{Name: k, Value: v}
		whereInfo = append(whereInfo, info)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindOneByKey"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me = service.FindOneByKey(ctx, entity, whereInfo, fields...)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 根据关键值取对象集合中的符合条件的第一条记录的指定字段
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) FindValueByKey(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	me := urlutil.GetParams(ctx.Request, entity)
	if !me.Gsuccess {
		return me
	}

	params := me.Gdata.(map[string]interface{})
	whereInfo := []dbinfo.WhereInfo{}
	for k, v := range params {
		info := dbinfo.WhereInfo{Name: k, Value: v}
		whereInfo = append(whereInfo, info)
	}

	fieldName := urlutil.GetParam(ctx.Request, "fieldName", "").(string)

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindValueByKey"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me = service.FindValueByKey(ctx, entity, whereInfo, fieldName)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 根据关键值查数量
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) FindCountByKey(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	me := urlutil.GetParams(ctx.Request, entity)
	if !me.Gsuccess {
		return me
	}

	params := me.Gdata.(map[string]interface{})
	whereInfo := []dbinfo.WhereInfo{}
	for k, v := range params {
		info := dbinfo.WhereInfo{Name: k, Value: v}
		whereInfo = append(whereInfo, info)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindCountByKey"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me = service.FindCountByKey(ctx, entity, whereInfo)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 根据字段名取分组数据
 * @param ctx http请求对象
 * @param control 控制层接口
 * @param fields 分组字段[字段名,字段别名]
 */
func (controlUtil ControllerUtil) Group(ctx ginutil.Context, control Controller, fields map[string]string) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".GroupByField"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me := service.GroupByField(ctx, entity, "", fields)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 根据字段名取分组数据且取数量
 * @param ctx http请求对象
 * @param control 控制层接口
 * @param fields 分组字段[字段名,字段别名]
 */
func (controlUtil ControllerUtil) GroupAndCount(ctx ginutil.Context, control Controller, fields map[string]string) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".GroupByFieldAndCount"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me := service.GroupByFieldAndCount(ctx, entity, "", fields)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 根据字段名取分组数据
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) GroupByField(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	me := urlutil.GetParams(ctx.Request, entity)
	if !me.Gsuccess {
		return me
	}

	fieldsTemp := me.Gdata.(map[string]interface{})
	fields := make(map[string]string)
	for k, v := range fieldsTemp {
		fields[k] = fmt.Sprintf("%v", v)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".GroupByField"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me = service.GroupByField(ctx, entity, "", fields)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 取表中指定字段的最大值
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) MaxByField(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	me := urlutil.GetParams(ctx.Request, entity)
	if !me.Gsuccess {
		return me
	}

	field := urlutil.GetParam(ctx.Request, "field", "").(string)
	if field == "" {
		return msgentity.Err(9012, "field参数为空")
	}

	params := me.Gdata.(map[string]interface{})
	whereInfo := []dbinfo.WhereInfo{}
	for k, v := range params {
		info := dbinfo.WhereInfo{Name: k, Value: v}
		whereInfo = append(whereInfo, info)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".MaxByField"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me = service.MaxByField(ctx, entity, "", field, whereInfo)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 取表中指定字段的最小值
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) MinByField(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	me := urlutil.GetParams(ctx.Request, entity)
	if !me.Gsuccess {
		return me
	}

	field := urlutil.GetParam(ctx.Request, "field", "").(string)
	if field == "" {
		return msgentity.Err(9012, "field参数为空")
	}

	params := me.Gdata.(map[string]interface{})
	whereInfo := []dbinfo.WhereInfo{}
	for k, v := range params {
		info := dbinfo.WhereInfo{Name: k, Value: v}
		whereInfo = append(whereInfo, info)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".MinByField"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me = service.MinByField(ctx, entity, "", field, whereInfo)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 取表中指定字段的最小值
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) HasById(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	// tableInfo := dbinfo. entity..TableInfo()
	id := urlutil.GetParamToId(ctx.Request, dbinfo.EntityKeyName(entity)) //取请求参数中的记录编号
	if id == nil || (fmt.Sprintf("%v", id) == "") {
		id = urlutil.GetParamToId(ctx.Request, "id")

		if id == nil || (fmt.Sprintf("%v", id) == "") {
			return msgentity.Err(1001, "未能获取'", dbinfo.EntityKeyName(entity), "'参数")
		}
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".HasById"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me := service.HasById(ctx, entity, id)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 检查关键值记录是否存在(返回1:存在;0:不存在)
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) HasByKey(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	keyName := urlutil.GetParam(ctx.Request, "keyName", "")   //取请求参数中的字段名
	keyValue := urlutil.GetParam(ctx.Request, "keyValue", "") //取请求参数中的字段值

	if keyName == "" {
		return msgentity.Err(9001, "keyName参数为空")
	}

	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".HasByKey"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me := service.HasByKey(ctx, entity, keyName.(string), keyValue)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 清理指定用户的缓存
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) ClearCache(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	sUser := urlutil.GetParam(ctx.Request, "sUser", "").(string)         //取请求参数中的用户名
	cacheName := urlutil.GetParam(ctx.Request, "cacheName", "").(string) //取请求参数中的缓存名

	if cacheName == "" {
		return msgentity.Err(8001, "指定'缓存库名称'参数为空！")
	}

	if sUser == "" {
		cacheName = cacheName + sUser
	}

	Log.Error("清理缓存库:" + cacheName)
	// if globalvariable.Del(cacheName) {//这种方式不合适
	// 	return msgentity.Success(8999, "清理成功！")
	// }

	return msgentity.Err(8002, "清理失败！")
}

/**
 * 查询组结构数据
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return
 */
func (controlUtil ControllerUtil) FindByGroup(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	sGroupColumn := urlutil.GetParam(ctx.Request, "sGroupColumn", "").(string) //分组名(树节点)所在字段名
	sGroupName := urlutil.GetParam(ctx.Request, "sGroupName", "").(string)     //分组名(树节点)

	if sGroupName == "" {
		return controlUtil.FindByTree(ctx, control)
	}

	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9011, err)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".FindByGroup"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me := service.FindByGroup(ctx, entity, sGroupColumn, sGroupName)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 添加数据到指定组下
 * 警告:对象必须符合树形结构要求,如:sId、sPid
 * @param ctx http请求对象
 * @param control 控制层接口
 * @param sGroupName 分组字段名称(树节点)
 * @param sGroupValue 分组字段值(树节点)
 * @param entity 实体对象
 * @return
 */
func (controlUtil ControllerUtil) AddToGroup(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	sGroupName := urlutil.GetParam(ctx.Request, "sGroupName", "").(string)   //分组字段名称(树节点)
	sGroupValue := urlutil.GetParam(ctx.Request, "sGroupValue", "").(string) //分组字段值(树节点)

	entity, err := control.GetModuleEntity(control) //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9001, "控制层未设置ModuleEntit:", err)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".AddToGroup"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me := service.AddToGroup(ctx, entity, sGroupName, sGroupValue)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 根据关键值翻转值(限布尔值类型,1转2,2转1)
 * 警告:此方法只支持布尔值类型,且只支持翻转1和2
 * @param ctx http请求对象
 * @param control 控制层接口
 * @return msgentity.MsgEntity
 */
func (controlUtil ControllerUtil) ReversalByKey(ctx ginutil.Context, control Controller) *msgentity.MsgEntity {
	reversalColumn := urlutil.GetParam(ctx.Request, "sReversalColumn", "").(string) //翻转的字段名
	entity, err := control.GetModuleEntity(control)                                 //按模块数据实体创建新实例,并实例化
	if err != nil {
		return msgentity.Err(9001, "控制层未设置ModuleEntit:", err)
	}

	me := urlutil.GetParams(ctx.Request, entity)
	if !me.Gsuccess {
		return me
	}

	params := me.Gdata.(map[string]interface{})
	whereInfo := []dbinfo.WhereInfo{}
	for k, v := range params {
		info := dbinfo.WhereInfo{Name: k, Value: v}
		whereInfo = append(whereInfo, info)
	}

	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	name := getSimplName(control) + ".ReversalByKey"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	me = service.ReversalByKey(ctx, entity, whereInfo, reversalColumn)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 增加请求属性
 * 注:函数应用于模块控制器时修改或传递到通用方法时,切勿滥用
 * @param ctx http请求对象
 * @Param attribs 参数键值对数组,[name,value],如果value不存在则为''
 */
func (controlUtil ControllerUtil) AddRequestAttrib(ctx ginutil.Context, attribs ...[]string) {
	if (attribs == nil) || (len(attribs) < 1) {
		return
	}

	ctx.Request.ParseForm() //警告:必须先 解析所有请求数据

	form := ctx.Request.Form
	if nil == form {
		form = make(uu.Values)
		ctx.Request.Form = form
	}

	for _, attrib := range attribs {
		if (attrib == nil) || (len(attrib) < 1) {
			continue
		}

		name := attrib[0]
		value := ""
		if (len(attrib) > 1) && (attrib[1] != "") {
			value = attrib[1] //fmt.Sprintf("%v", attrib[1])
		}

		form.Set(name, value)
	}
}

/**
 * 上传文件
 * @param ctx http请求对象
 * @param control 控制层接口
 * @param modelName 模块名称
 * @return msgentity.MsgEntity
 */
func (controlUtil ControllerUtil) UpFile(ctx ginutil.Context, control Controller, modelName string) *msgentity.MsgEntity {
	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	return service.UpFile(ctx, modelName)
}

/**
 * 获取图片
 * @param ctx http请求对象
 * @param control 控制层接口
 * @param modelName 模块名称
 * @param filename 文件名
 */
func (controlUtil ControllerUtil) LookImg(ctx ginutil.Context, control Controller, modelName, filename string) {
	service, err := control.GetModuleService(control)
	if err != nil {
		service = &ServiceBaseFunc{}
	}

	service.LookImg(ctx, modelName, filename)
}

// func (controlUtil ControllerUtil) GetModuleEntity(controller Controller) (dbinfo.Entity, error) {
// 	moduleValue := reflect.ValueOf(controller).Elem().FieldByName("ModuleEntity")

// 	if moduleValue.Kind() != reflect.Ptr && moduleValue.Kind() != reflect.Struct {
// 		return nil, errors.New("未设置模块数据实体")
// 	}

// 	//直接调用Now
// 	nowMethod := moduleValue.MethodByName("New")
// 	entityElem := nowMethod.Call(nil)[0].Interface()
// 	result := entityElem.(dbinfo.Entity)

// 	// // 获取 Entity 的反射值,并建立新的实例(必须)
// 	// var entityElem reflect.Value
// 	// if moduleValue.Kind() == reflect.Ptr {
// 	// 	entityElem = reflect.New(moduleValue.Type().Elem())
// 	// } else {
// 	// 	entityElem = reflect.New(moduleValue.Type())
// 	// }

// 	// result := entityElem.Interface().(dbinfo.Entity).New()

// 	return result, nil
// }

// func (controlUtil ControllerUtil) GetModuleService(control Controller) (Service, error) {
// 	moduleValue := reflect.ValueOf(control).Elem().FieldByName("ModuleService")

// 	if moduleValue.Kind() != reflect.Ptr && moduleValue.Kind() != reflect.Struct {
// 		return &ServiceBaseFunc{}, errors.New("未设置模块业务实体")
// 	}

// 	// 获取 Service 的反射值
// 	var serviceElem reflect.Value
// 	if moduleValue.Kind() == reflect.Ptr {
// 		serviceElem = reflect.New(moduleValue.Type().Elem())
// 	} else {
// 		serviceElem = reflect.New(moduleValue.Type())
// 	}

// 	service := serviceElem.Interface().(Service)

// 	return service, nil
// }

//-----------------------------------------------//

/**
 * 取对应模块名称
 * object 对象
 */
func getSimplName(object interface{}) string {
	name := reflect.TypeOf(object).String()

	iSt := strings.Index(name, ".")
	if iSt > -1 {
		name = name[iSt+1:]
	}

	return name
}
