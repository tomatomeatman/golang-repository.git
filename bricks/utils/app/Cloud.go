package app

import (
	"net/url"
	"strings"
	"time"

	"gitee.com/tomatomeatman/golang-repository/bricks/model/msgentity"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/httputil"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/ginutil"
	"github.com/cihub/seelog"
)

type Cloud struct{}

/**
 * 初始化
 */
func init() {
	if !(IsCloudApp()) {
		return
	}

	go ginutil.ControllerRegister("/cloud/monitor", Cloud{}.CloudMonitor, ginutil.POST, ginutil.GET)
}

/**
 * 注册服务到注册中心
 */
func (Cloud) RegistraCloud() {
	if !(IsCloudApp()) {
		return
	}

	cloudCenterSite := ReadConfigKey("CloudCenter", "Site", "").(string)
	cloudCenterUser := ReadConfigKey("CloudCenter", "User", "").(string)
	cloudCenterPassword := ReadConfigKey("CloudCenter", "Password", "").(string)
	DomainName := ReadConfigKey("System", "DomainName", "").(string)
	DomainPort := ReadConfigKey("System", "DomainPort", "").(string)

	Port := ReadConfigKey("System", "Port", "8080").(string)
	Name := ReadConfigKey("System", "Name", "").(string)

	var json strings.Builder
	json.WriteString("{\"sSign\":\"")
	json.WriteString(Name)
	json.WriteString("Server\",\"serverPort\":\"")
	json.WriteString(Port)
	json.WriteString("\",\"serverIp\":\"unknown\",\"domainName\":\"")
	json.WriteString(DomainName)
	json.WriteString("\",\"domainPort\":\"")
	json.WriteString(DomainPort)
	json.WriteString("\"}")

	var host strings.Builder
	host.WriteString("http://")
	host.WriteString(cloudCenterSite)
	host.WriteString("/add")

	params := map[string]interface{}{
		"user":         cloudCenterUser,
		"key":          cloudCenterPassword,
		"sSign":        Name + "Server",
		"sRegisterTxt": url.QueryEscape(json.String()),
	}

	urlStr := strings.Replace(host.String(), "http://http://", "http://", 1)

	for {
		me := httputil.DoGet(urlStr, params, nil)
		if me.Gsuccess {
			//fmt.Println("注册服务成功")
			seelog.Info("注册服务成功")
			return
		}

		seelog.Error("注册服务失败,返回内容:", me)
		time.Sleep(time.Second * 5) //休眠5秒
	}
}

// #region @Api {title=中心监听接口}
// @return {type=json explainType=MsgEntity<string> explain=返回响应}
// @RequestMapping {name=ServerIp type=GET value=/cloud/monitor}
// #endregion
func (Cloud) CloudMonitor(ctx ginutil.Context) interface{} {
	return msgentity.Success(time.Now().Format("2006-01-02 15:04:05"), "访问成功")
}
