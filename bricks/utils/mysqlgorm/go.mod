module gitee.com/tomatomeatman/golang-repository/bricks/utils/mysqlgorm

go 1.21.6

toolchain go1.23.4

require (
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	github.com/shopspring/decimal v1.4.0
	gopkg.in/ini.v1 v1.67.0
	gorm.io/driver/mysql v1.5.7
	gorm.io/gorm v1.25.12
)

require (
	filippo.io/edwards25519 v1.1.0 // indirect
	github.com/go-sql-driver/mysql v1.8.1 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/stretchr/testify v1.10.0 // indirect
	golang.org/x/text v0.21.0 // indirect
)
