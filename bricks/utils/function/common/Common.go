package common

/**
 * 判断是否为空,val为nil时返回默认值
 * @param val
 * @param def
 * @return
 */
func Isnull(val, def interface{}) interface{} {
	if val == nil {
		return def
	}

	return val
}

/**
 * 三元运算
 * @param bl
 * @param trueVal
 * @param falseVal
 * @return
 */
func Ternary(bl bool, trueVal, falseVal interface{}) interface{} {
	if bl {
		return trueVal
	}

	return falseVal
}
