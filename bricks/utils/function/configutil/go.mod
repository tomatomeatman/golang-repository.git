module gitee.com/tomatomeatman/golang-repository/bricks/utils/function/configutil

go 1.21.6

toolchain go1.23.4

require (
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	gopkg.in/ini.v1 v1.67.0
)

require github.com/stretchr/testify v1.10.0 // indirect
