module gitee.com/tomatomeatman/golang-repository/bricks/utils/function/rarutil

go 1.21.6

toolchain go1.23.4

require (
	gitee.com/tomatomeatman/golang-repository/bricks/model/msgentity v0.0.0-20250106012713-7ae425b1f66a
	github.com/mholt/archiver v3.1.1+incompatible
	github.com/nwaples/rardecode v1.1.3
)

require (
	github.com/dsnet/compress v0.0.1 // indirect
	github.com/frankban/quicktest v1.14.6 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/pierrec/lz4 v2.6.1+incompatible // indirect
	github.com/ulikunitz/xz v0.5.12 // indirect
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8 // indirect
)
