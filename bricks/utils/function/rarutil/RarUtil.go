package rarutil

import (
	"fmt"
	"io"
	"os"
	"strings"

	"gitee.com/tomatomeatman/golang-repository/bricks/model/msgentity"
	"github.com/mholt/archiver"
	"github.com/nwaples/rardecode"
)

/**
 * 读取指定(序号)文件
 * source 源文件
 * iIndex 序号
 * *msgentity.MsgEntity (content []byte, fileName string)
 */
func Read(source string, iIndex int) *msgentity.MsgEntity {
	source = strings.TrimSpace(source)

	if _, err := os.Stat(source); err != nil {
		if os.IsNotExist(err) {
			return msgentity.Err(1001, "源文件不存在")
		}

		return msgentity.Err(1002, "访问源文件异常:", err)
	}

	r := archiver.NewRar()

	var result []byte
	fileName := ""
	index := -1
	err := r.Walk(source, func(f archiver.File) error {
		index++

		if index != iIndex {
			return nil
		}

		rh, ok := f.Header.(*rardecode.FileHeader)
		if !ok {
			return fmt.Errorf("读取文件头失败")
		}
		// fmt.Println("FileName:", rh.Name)

		content, err := io.ReadAll(f)
		if err != nil {
			return err
		}

		result = content
		fileName = rh.Name

		return fmt.Errorf("找到")
	})

	if err != nil {
		return msgentity.Err(1003, "读取文件失败:", err)
	}

	if fileName == "" {
		return msgentity.Err(1004, "未找到文件")
	}

	return msgentity.Success(result, fileName)
}

/**
 * 读取指定(序号)文件
 * source 源文件
 * iIndex 序号
 * *msgentity.MsgEntity (content []byte, fileName string)
 */
func ReadByIndex(source string, iIndex int) *msgentity.MsgEntity {
	return Read(source, iIndex)
}

/**
 * 读取指定(名称)文件
 * source 源文件
 * iIndex 序号
 * *msgentity.MsgEntity (content []byte, fileName string)
 */
func ReadByName(source, sName string) *msgentity.MsgEntity {
	source = strings.TrimSpace(source)

	if _, err := os.Stat(source); err != nil {
		if os.IsNotExist(err) {
			return msgentity.Err(1001, "源文件不存在")
		}

		return msgentity.Err(1002, "访问源文件异常:", err)
	}

	sName = strings.TrimSpace(sName)

	r := archiver.NewRar()

	var result []byte
	fileName := ""
	err := r.Walk(source, func(f archiver.File) error {
		rh, ok := f.Header.(*rardecode.FileHeader)
		if !ok {
			return fmt.Errorf("读取文件头失败")
		}

		if sName != rh.Name {
			return nil
		}
		// fmt.Println("FileName:", rh.Name)

		content, err := io.ReadAll(f)
		if err != nil {
			return err
		}

		result = content
		fileName = rh.Name

		return fmt.Errorf("找到")
	})

	if err != nil {
		return msgentity.Err(1003, "读取文件失败:", err)
	}

	if fileName == "" {
		return msgentity.Err(1004, "未找到文件:", sName)
	}

	return msgentity.Success(result, fileName)
}

/**
 * 解压文件
 * source 源文件
 * targe 目标路径
 * *msgentity.MsgEntity (int, msg)
 */
func Unpack(source, targe string) *msgentity.MsgEntity {
	source = strings.TrimSpace(source)

	if _, err := os.Stat(source); err != nil {
		if os.IsNotExist(err) {
			return msgentity.Err(1001, "源文件不存在")
		}

		return msgentity.Err(1002, "访问源文件异常:", err)
	}

	targe = strings.TrimSpace(targe)

	r := archiver.NewRar()

	err := r.Unarchive(source, targe)
	if err != nil {
		return msgentity.Err(1003, "解压文件异常:", err)
	}

	return msgentity.Success(1999, "解压完毕")
}
