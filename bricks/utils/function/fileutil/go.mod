module gitee.com/tomatomeatman/golang-repository/bricks/utils/function/fileutil

go 1.21.6

toolchain go1.23.4

require (
	gitee.com/tomatomeatman/golang-repository/bricks/model/msgentity v0.0.0-20250106012713-7ae425b1f66a
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
)
