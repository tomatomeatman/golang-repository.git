package base64util

import (
	"bytes"
	"encoding/base64"
	"errors"
	"fmt"
	"image"
	"image/png"
	"os"
	"path/filepath"
	"strings"
)

// Base64转换为PNG图片
func Base64ToPng(base64Data, savePath string) (int, error) {
	iSt := strings.Index(base64Data, ",")
	if iSt > 0 { //清除前缀
		base64Data = base64Data[iSt+1:]
	}

	// 解码Base64字符串为字节流
	decodedBytes, err := base64.StdEncoding.DecodeString(base64Data)
	if err != nil {
		return 1001, createError("无法解码base64:", err.Error())
	}

	// 将解码后的字节流转换为image.Image接口实例
	img, _, err := image.Decode(bytes.NewReader(decodedBytes))
	if err != nil {
		return 1002, createError("解码图像失败:", err.Error())
	}

	dirPath := filepath.Dir(savePath)

	// 检查并创建目录（如果不存在的话）
	if err := os.MkdirAll(dirPath, 0755); err != nil {
		return 1003, createError("创建输出文件夹失败:", err.Error())
	}

	outFile, err := os.Create(savePath) // 创建一个新的文件来保存图片
	if err != nil {
		return 1004, createError("创建输出文件失败:", err.Error())
	}
	defer outFile.Close()

	// 使用png格式编码器将图像写入文件
	err = png.Encode(outFile, img)
	if err != nil {
		return 1005, createError("未能将图像编码为PNG:", err.Error())
	}

	return 1999, nil //"图片保存成功！"
}

/**
 * 创建错误信息
 * msg 错误信息
 * 返回错误信息
 */
func createError(msg ...interface{}) error {
	var build strings.Builder
	for _, v := range msg {
		build.WriteString(fmt.Sprintf("%v", v))
	}

	return errors.New(build.String())
}
