package md5util

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"strings"
)

// 取MD5,大写
func Upper(str ...interface{}) string {
	result := Lower(str...)

	return strings.ToUpper(result)
}

// 取MD5小写
func Lower(str ...interface{}) string {
	var temp string

	if len(str) > 0 {
		var build strings.Builder

		for _, value := range str {
			build.WriteString(fmt.Sprintf("%v", value))
		}

		temp = build.String()
	} else {
		temp = ""
	}

	data := []byte(temp)
	md5Ctx := md5.New()
	md5Ctx.Write(data)
	cipherStr := md5Ctx.Sum(nil)
	result := hex.EncodeToString(cipherStr)

	return result
}
