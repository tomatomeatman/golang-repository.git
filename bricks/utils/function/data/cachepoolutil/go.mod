module gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data/cachepoolutil

go 1.21.6

toolchain go1.23.4

require (
	gitee.com/tomatomeatman/golang-repository/bricks/model/cacheInfo v0.0.0-20250106012415-8c7d71581876
	gitee.com/tomatomeatman/golang-repository/bricks/model/msgentity v0.0.0-20250106012415-8c7d71581876
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
)
