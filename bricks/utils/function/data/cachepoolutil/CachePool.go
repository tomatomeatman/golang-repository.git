package cachepoolutil

import (
	"fmt"
	"strings"
	"time"

	"gitee.com/tomatomeatman/golang-repository/bricks/model/cacheInfo"
	"gitee.com/tomatomeatman/golang-repository/bricks/model/msgentity"
)

var (
	cachMap = make(map[string]*cacheInfo.CacheInfo) //存储缓存的集合
	//checkWrite sync.Mutex                             //保存锁
)

// 自定义缓存池
type CachePool struct{}

/**
 * 清理缓存
 * @return
 */
func (cp CachePool) Clear() *msgentity.MsgEntity {
	if len(cachMap) < 1 {
		return msgentity.Success("原本就是空的", "清理完毕")
	}

	cachMap = make(map[string]*cacheInfo.CacheInfo)
	return msgentity.Success(6999, "清理完毕")
}

/**
* 释放缓存
* @param sId
* @return
 */
func (cp CachePool) Free(sId string) *msgentity.MsgEntity {
	sId = strings.TrimSpace(sId)

	if (len(cachMap) < 1) || (sId == "") {
		return msgentity.Success("本身没有这个缓存", "释放成功")
	}

	delete(cachMap, sId)

	return msgentity.Success(6999, "释放成功")
}

/**
* 释放缓存
* @param sId
* @return
 */
func (cp CachePool) FreeById(sId string) *msgentity.MsgEntity {
	return cp.Free(sId)
}

/**
* 释放缓存
* @param sId
* @return
 */
func (cp CachePool) FreeByData(data interface{}) *msgentity.MsgEntity {
	iCount := 0
	for key, val := range cachMap {
		if val != data {
			continue
		}

		delete(cachMap, key)
		iCount++
	}

	if iCount < 1 {
		return msgentity.Success("本身没有这个缓存", "释放成功")
	}

	return msgentity.Success(iCount, "释放成功")
}

/**
* 释放指定分钟之前的缓存
* @param iMinute
* @return
 */
func (cp CachePool) FreeByTime(iMinute int) *msgentity.MsgEntity {
	if len(cachMap) < 1 {
		return msgentity.Success("本身没有这个缓存", "释放成功")
	}

	//--取过期时间点--//
	now := time.Now()
	iCount := 0
	for key, val := range cachMap {
		if int(now.Sub(val.GdLast).Minutes()) < iMinute { //两个时间相减
			continue
		}

		delete(cachMap, key)
		iCount++
	}

	return msgentity.Success("释放数:"+fmt.Sprintf("%v", iCount), "释放成功")
}

/**
* 添加缓存
* @param cacheInfo
* @return
 */
func (cp CachePool) Add(cacheInfo *cacheInfo.CacheInfo) *msgentity.MsgEntity {
	if nil == cacheInfo.Gdata {
		return msgentity.Err(6001, "添加失败,缓存信息为null")
	}

	sId := strings.TrimSpace(cacheInfo.GsId)
	if sId == "" {
		return msgentity.Err(6002, "添加失败,缓存信息关键字为空")
	}

	cachMap[sId] = cacheInfo

	return msgentity.Success(cacheInfo, "添加成功")
}

/**
* 添加缓存
* @param sId
* @param oData
* @return
 */
func (cp CachePool) AddData(sId string, data interface{}) *msgentity.MsgEntity {
	sId = strings.TrimSpace(sId)
	if sId == "" {
		return msgentity.Err(6001, "添加失败,缓存信息关键字为空")
	}

	if data == nil {
		return msgentity.Err(6002, "添加失败,缓存信息为null")
	}

	cacheInfo := cacheInfo.New(sId, data)

	cachMap[sId] = cacheInfo

	return msgentity.Success(cacheInfo, "添加成功")
}

/**
* 判断是否包含缓存,包含返回true
* @param sId
* @return
 */
func (cp CachePool) Contains(sId string) bool {
	sId = strings.TrimSpace(sId)
	if sId == "" {
		return false
	}

	if len(cachMap) < 1 {
		return false
	}

	_, ok := cachMap[sId]

	return ok
}

/**
* 查找缓存信息
* @param sId
* @return
 */
func (cp CachePool) Find(sId string) *cacheInfo.CacheInfo {
	if sId == "" {
		return nil
	}

	if len(cachMap) < 1 {
		return nil
	}

	cacheInfo, ok := cachMap[sId]
	if !ok {
		return nil //缓存中还没有存在
	}

	cacheInfo.GdLast = time.Now() //修改访问时间

	return cacheInfo
}

/**
* 查找缓存对象
* @param sId
* @return
 */
func (cp CachePool) FindData(sId string) interface{} {
	sId = strings.TrimSpace(sId)
	cacheInfo := cp.Find(sId)
	if nil == cacheInfo {
		return nil
	}

	return cacheInfo.Gdata
}
