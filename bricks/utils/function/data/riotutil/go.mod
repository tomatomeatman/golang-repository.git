module gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data/riotutil

go 1.22

toolchain go1.23.4

require (
	gitee.com/tomatomeatman/golang-repository/bricks/model/msgentity v0.0.0-20250106012415-8c7d71581876
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	github.com/go-ego/riot v0.0.0-20201013133145-f4c30acb3704
)

require (
	github.com/AndreasBriese/bbloom v0.0.0-20190825152654-46b345b51c96 // indirect
	github.com/BurntSushi/toml v1.4.0 // indirect
	github.com/StackExchange/wmi v1.2.1 // indirect
	github.com/cespare/xxhash v1.1.0 // indirect
	github.com/cespare/xxhash/v2 v2.3.0 // indirect
	github.com/dgraph-io/badger v1.6.2 // indirect
	github.com/dgraph-io/ristretto v0.2.0 // indirect
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/ebitengine/purego v0.8.1 // indirect
	github.com/fsnotify/fsnotify v1.8.0 // indirect
	github.com/go-ego/cedar v0.10.2 // indirect
	github.com/go-ego/gpy v0.42.1 // indirect
	github.com/go-ego/gse v0.80.3 // indirect
	github.com/go-ego/murmur v0.10.1 // indirect
	github.com/go-ole/go-ole v1.3.0 // indirect
	github.com/go-vgo/gt/conf v0.0.0-20230822203217-77ef441996bf // indirect
	github.com/go-vgo/gt/info v0.0.0-20241007202518-5cad3e61bcfd // indirect
	github.com/golang/protobuf v1.5.4 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/lufia/plan9stats v0.0.0-20240909124753-873cd0166683 // indirect
	github.com/pelletier/go-toml v1.9.5 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/power-devops/perfstat v0.0.0-20240221224432-82ca36839d55 // indirect
	github.com/shirou/gopsutil v3.21.11+incompatible // indirect
	github.com/shirou/gopsutil/v4 v4.24.12 // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
	github.com/tklauser/go-sysconf v0.3.14 // indirect
	github.com/tklauser/numcpus v0.9.0 // indirect
	github.com/vcaesar/cedar v0.20.2 // indirect
	github.com/yusufpapurcu/wmi v1.2.4 // indirect
	go.etcd.io/bbolt v1.3.11 // indirect
	golang.org/x/net v0.33.0 // indirect
	golang.org/x/sys v0.29.0 // indirect
	google.golang.org/protobuf v1.36.1 // indirect
)
