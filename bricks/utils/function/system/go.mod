module gitee.com/tomatomeatman/golang-repository/bricks/utils/function/system

go 1.21.6

toolchain go1.23.4

require github.com/shirou/gopsutil v3.21.11+incompatible

require (
	github.com/go-ole/go-ole v1.3.0 // indirect
	github.com/stretchr/testify v1.10.0 // indirect
	github.com/tklauser/go-sysconf v0.3.14 // indirect
	github.com/tklauser/numcpus v0.9.0 // indirect
	github.com/yusufpapurcu/wmi v1.2.4 // indirect
	golang.org/x/sys v0.29.0 // indirect
)
