package wailsutil

import (
	"context"
	"encoding/json"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"gitee.com/tomatomeatman/golang-repository/bricks/model/msgentity"
	"github.com/wailsapp/wails/v2/pkg/runtime"
	"gopkg.in/ini.v1"
)

var (
	iniFilePath        string
	windowInfoFilePath string
)

// App struct
type AppFun struct {
	sysType        string //系统类型
	windwIsShow    bool   //窗体当前是否显示
	useBeforeClose bool   //是否显示关闭前提示
	isWindowOnTop  bool   //窗口是否置顶
	ctx            context.Context
	GiWidth        int
	GiHeight       int
	GiTop          int
	GiLeft         int
	GisMaxWindow   bool
	ClearTrayFun   func() //清理托盘函数
}

// NewApp creates a new App application struct
func NewApp(sIniFilePath, sysType string) *AppFun {
	result := &AppFun{useBeforeClose: true, windwIsShow: true}
	result.sysType = sysType
	result.createInitFile(sIniFilePath) //创建配置文件
	result.readInit()                   //读取配置文件

	return result
}

// 创建配置文件
func (a *AppFun) createInitFile(sPath string) {
	// 获取文件的完整路径
	var err error
	iniFilePath, err = filepath.Abs(sPath)
	if err != nil {
		clogger.Error("获取绝对路径失败:" + err.Error())
	}

	_, err = os.Stat(iniFilePath) // 尝试获取文件信息
	if err == nil {               // 如果文件存在并且没有错误发生，则说明存在配置文件
		return
	}

	// 检查文件夹是否存在，如果不存在则创建
	dir := filepath.Dir(iniFilePath)
	err = os.MkdirAll(dir, 0755)
	if err != nil {
		clogger.Error("创建目录失败:" + err.Error())
	}

	// 打开文件，如果文件不存在，os.OpenFile会创建它
	file, err := os.OpenFile(iniFilePath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		clogger.Error("打开或创建文件失败:" + err.Error())
		return
	}

	file.WriteString("")
	file.Close()

	cfg, err := ini.Load(iniFilePath)
	if err != nil {
		clogger.Error("读取配置文件异常:" + err.Error())
		return
	}

	cfg.Section("System").Key("Name").SetValue("GNotepad")
	cfg.Section("System").Key("Title").SetValue("笔记")
	cfg.Section("System").Key("OnlyRun").SetValue("1")

	cfg.Section("App").Key("iWidth").SetValue("960")
	cfg.Section("App").Key("iHeight").SetValue("600")
	cfg.Section("App").Key("iLeft").SetValue("-1024")
	cfg.Section("App").Key("iTop").SetValue("-1024")
	cfg.Section("App").Key("isMaxWindow").SetValue("0")
	cfg.SaveTo(iniFilePath)
}

func (a *AppFun) readInit() {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		clogger.Error("取程序路径异常:" + err.Error())
	}

	// 检查文件夹是否存在，如果不存在则创建
	err = os.MkdirAll(dir+"/temp", 0755)
	if err != nil {
		clogger.Error("创建目录失败:" + err.Error())
	}

	windowInfoFilePath = strings.Replace(dir, "\\", "/", -1) + "/temp/windowInfo.ini"

	// // 打开文件，如果文件不存在，os.OpenFile会创建它
	// file, err := os.OpenFile(windowInfoFilePath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	// if err != nil {
	// 	clogger.Error("打开或创建文件失败:" + err.Error())
	// 	return
	// }

	// // file.WriteString("")
	// file.Close()

	cfg, err := ini.Load(windowInfoFilePath)
	if err != nil {
		clogger.Error("读取配置文件异常:" + err.Error())
		a.GiWidth = 960
		a.GiHeight = 600
		a.GiTop = -1024
		a.GiLeft = -1024
		a.GisMaxWindow = false
		return
	}

	section := cfg.Section("Window")
	a.GiWidth, err = section.Key("iWidth").Int()
	if err != nil {
		a.GiWidth = 960
	}

	a.GiHeight, err = section.Key("iHeight").Int()
	if err != nil {
		a.GiHeight = 600
	}

	a.GiTop, err = section.Key("iTop").Int()
	if err != nil {
		a.GiTop = -1024
	}

	a.GiLeft, err = section.Key("iLeft").Int()
	if err != nil {
		a.GiLeft = -1024
	}

	temp, _ := section.Key("isMaxWindow").Int()
	a.GisMaxWindow = temp == 1
}

// 一旦 Wails 分配了它需要的资源，就会调用 startup 方法，它是创建资源、设置事件侦听器以及应用程序在启动时需要的任何其他东西的好地方
func (a *AppFun) Startup(ctx context.Context) {
	a.ctx = ctx

	if (a.GiTop <= -1024) || (a.GiLeft <= -1024) {
		runtime.WindowCenter(a.ctx) //居中
		return
	}

	runtime.WindowSetPosition(a.ctx, a.GiLeft, a.GiTop) //指定位置
}

// 在关闭过程结束时由 Wails 调用。 这是释放内存和执行关闭任务的好地方
func (a *AppFun) Shutdown(ctx context.Context) {
	// SaveWindowInfo(ctx)//无法获得window信息,不能在这里调用
}

func SaveWindowInfo(ctx context.Context) {
	// 打开文件，如果文件不存在，os.OpenFile会创建它
	file, err := os.OpenFile(windowInfoFilePath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		clogger.Error("打开或创建文件失败:" + err.Error())
		return
	}

	// file.WriteString("")
	file.Close()

	cfg, err := ini.Load(windowInfoFilePath)
	if err != nil {
		clogger.Error("读取配置文件异常:" + err.Error())
		return
	}

	if runtime.WindowIsMaximised(ctx) { //窗口最大化情况下不能保存窗口位置和大小,否则会都是最大化
		cfg.Section("Window").Key("isMaxWindow").SetValue("1")
		cfg.SaveTo(windowInfoFilePath)
		return
	}

	iWidth, iHeight := runtime.WindowGetSize(ctx)
	iLeft, iTop := runtime.WindowGetPosition(ctx)

	cfg.Section("Window").Key("iWidth").SetValue(strconv.Itoa(iWidth))
	cfg.Section("Window").Key("iHeight").SetValue(strconv.Itoa(iHeight))
	cfg.Section("Window").Key("iLeft").SetValue(strconv.Itoa(iLeft))
	cfg.Section("Window").Key("iTop").SetValue(strconv.Itoa(iTop))
	cfg.Section("Window").Key("isMaxWindow").SetValue("0")
	cfg.SaveTo(windowInfoFilePath)
}

func (a *AppFun) BeforeClose(ctx context.Context) (prevent bool) {
	if a.sysType == "windows" {
		a.windwIsShow = false
		// runtime.WindowHide(a.ctx)
		// a.windwIsShow = false
	}

	if !a.useBeforeClose { //不询问,直接关闭
		return false
	}

	dialog, err := runtime.MessageDialog(ctx, runtime.MessageDialogOptions{
		Type:    runtime.QuestionDialog,
		Title:   "关闭确认",
		Message: "确定要退出程序吗？",
	})

	if err != nil {
		return false
	}

	return dialog != "Yes"
}

func (a *AppFun) HasWindow() string {
	return "local"
}

func (a *AppFun) CloseApp() string {
	go SaveWindowInfo(a.ctx)

	if a.sysType == "windows" {
		a.windwIsShow = false
		runtime.WindowHide(a.ctx)
		return ""
	}

	//a.windwIsShow = false
	runtime.Quit(a.ctx) //请求关闭,如果有实现beforeClose旧会触发beforeClose

	//下面代码保留,这是没有实现beforeClose时的做法
	// dialog, err := runtime.MessageDialog(a.ctx, runtime.MessageDialogOptions{
	// 	Type:    runtime.QuestionDialog,
	// 	Title:   "关闭确认",
	// 	Message: "确定要退出程序吗？",
	// })

	// if err != nil {
	// 	return "发生错误：" + err.Error()
	// }

	// if dialog == "Yes" {
	// 	//os.Exit(0)
	// 	runtime.Quit(a.ctx)
	// 	return "确定关闭"
	// }

	// // a.beforeClose(a.ctx)

	return "收到"
}

func (a *AppFun) CloseAppNoQuest() {
	go SaveWindowInfo(a.ctx)
	a.useBeforeClose = false
	runtime.Quit(a.ctx) //请求关闭,如果有实现beforeClose旧会触发beforeClose
}

func (a *AppFun) SystemType() string {
	return a.sysType
}

// 隐藏窗口
func (a *AppFun) WindowHide() {
	go SaveWindowInfo(a.ctx)
	runtime.WindowHide(a.ctx)
	a.windwIsShow = false
}

// 显示窗口
func (a *AppFun) WindowShow() {
	runtime.WindowShow(a.ctx)
	a.windwIsShow = true
}

// 窗口是否显示
func (a *AppFun) WindwIsShow() bool {
	return a.windwIsShow
}

// 设置窗口显示状态值
func (a *AppFun) SetWindwIsShow(bl bool) {
	a.windwIsShow = bl
}

// 使窗口全屏
func (a *AppFun) WindowFullscreen() {
	runtime.WindowFullscreen(a.ctx)
}

// 最大化窗口以填满屏幕
func (a *AppFun) WindowMaximise() {
	runtime.WindowMaximise(a.ctx)
}

// 窗口是否最大化
func (a *AppFun) WindowIsMaximised() bool {
	return runtime.WindowIsMaximised(a.ctx)
}

// 在最大化和未最大化之间切换
func (a *AppFun) WindowToggleMaximise() {
	runtime.WindowToggleMaximise(a.ctx)
}

// 窗口最小化
func (a *AppFun) WindowMinimise() {
	runtime.WindowMinimise(a.ctx)
}

// 将窗口恢复到最小化之前的尺寸和位置
func (a *AppFun) WindowUnminimise() {
	runtime.WindowUnminimise(a.ctx)
}

// 如果窗口最小化，则返回 true。
func (a *AppFun) WindowIsMinimised() bool {
	return runtime.WindowIsMinimised(a.ctx)
}

// 设置窗口置顶或取消置顶
func (a *AppFun) WindowOnTop(top bool) {
	a.isWindowOnTop = top
	runtime.WindowSetAlwaysOnTop(a.ctx, top)
}

// 设置窗口置顶或取消置顶
func (a *AppFun) ChangeWindowOnTop() bool {
	a.isWindowOnTop = !a.isWindowOnTop
	runtime.WindowSetAlwaysOnTop(a.ctx, a.isWindowOnTop)
	return a.isWindowOnTop
}

// 设置窗口置顶或取消置顶
func (a *AppFun) IsWindowOnTop() bool {
	return a.isWindowOnTop
}

// 读取指定文件内容
func (a *AppFun) ReadFile(path string) *msgentity.MsgEntity {
	if path == "" {
		return msgentity.Err(8001, "路径参数缺失")
	}

	//读取文件内容
	file, err := os.Open(path)
	if err != nil {
		return msgentity.Err(8002, "打开文件异常:"+err.Error())
	}

	defer file.Close()

	//读取文件内容
	content, err := io.ReadAll(file)
	if err != nil {
		return msgentity.Err(8003, "读取文件异常:"+err.Error())
	}

	return msgentity.Success(string(content), path)
}

// 保存文件内容
func (a *AppFun) SaveFile(path string, text string) *msgentity.MsgEntity {
	if path == "" {
		return msgentity.Err(8001, "路径参数缺失")
	}

	file, err := os.OpenFile(path, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return msgentity.Err(8002, "打开文件异常:"+err.Error())
	}

	defer file.Close()
	_, err = file.WriteString(text)
	if err != nil {
		return msgentity.Err(8003, "保存文件异常:"+err.Error())
	}

	return msgentity.Success(path, "保存文件成功")
}

// 另存文件
func (a *AppFun) SaveAsFile(defaultPath string, text string) *msgentity.MsgEntity {
	dialogOptions := runtime.SaveDialogOptions{
		DefaultDirectory: defaultPath,
		Title:            "选择保存位置",
		Filters: []runtime.FileFilter{{
			DisplayName: "Markdown",
			Pattern:     "*.md",
		}},
		CanCreateDirectories: true,
	}

	sFilePath, err := runtime.SaveFileDialog(a.ctx, dialogOptions)
	if err != nil {
		return msgentity.Err(8001, "打开文件异常:"+err.Error())
	}

	file, err := os.OpenFile(sFilePath, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return msgentity.Err(8003, "打开文件异常:"+err.Error())
	}

	defer file.Close()
	_, err = file.WriteString(text)
	if err != nil {
		return msgentity.Err(8004, "保存文件异常:"+err.Error())
	}

	return msgentity.Success(sFilePath, "保存文件成功")
}

// 打开文件
func (a *AppFun) OpenFile(defaultPath string) *msgentity.MsgEntity {
	dialogOptions := runtime.OpenDialogOptions{
		DefaultDirectory: defaultPath,
		Title:            "选择文件",
		Filters: []runtime.FileFilter{{
			DisplayName: "Markdown",
			Pattern:     "*.md",
		}, {
			DisplayName: "所有文件",
			Pattern:     "*.*",
		}},
	}

	sFilePath, err := runtime.OpenFileDialog(a.ctx, dialogOptions)
	if err != nil {
		return msgentity.Err(8001, "打开文件异常:"+err.Error())
	}

	if sFilePath == "" {
		return msgentity.Err(8002, "文件路径为空")
	}

	//读取文件内容
	file, err := os.Open(sFilePath)
	if err != nil {
		return msgentity.Err(8003, "试图打开文件异常:"+err.Error())
	}

	defer file.Close()

	//读取文件内容
	content, err := io.ReadAll(file)
	if err != nil {
		return msgentity.Err(8001, "读取文件异常:"+err.Error())
	}

	return msgentity.Success(string(content), sFilePath)
}

// Info消息
func (a *AppFun) InfoMsg(sTitle, sMessage string) {
	dialogOptions := runtime.MessageDialogOptions{
		Type:          runtime.InfoDialog,
		Title:         sTitle,
		Message:       sMessage,
		DefaultButton: "确定",
		//CancelButton:  string
	}

	runtime.MessageDialog(a.ctx, dialogOptions)
}

// Warning消息
func (a *AppFun) WarningMsg(sTitle, sMessage string) {
	dialogOptions := runtime.MessageDialogOptions{
		Type:          runtime.WarningDialog,
		Title:         sTitle,
		Message:       sMessage,
		DefaultButton: "确定",
		//CancelButton:  string
	}

	runtime.MessageDialog(a.ctx, dialogOptions)
}

// Error消息
func (a *AppFun) ErrorMsg(sTitle, sMessage string) {
	dialogOptions := runtime.MessageDialogOptions{
		Type:          runtime.ErrorDialog,
		Title:         sTitle,
		Message:       sMessage,
		DefaultButton: "确定",
		//CancelButton:  string
	}

	runtime.MessageDialog(a.ctx, dialogOptions)
}

// Question消息
func (a *AppFun) QuestionMsg(sTitle, sMessage string) string {
	dialogOptions := runtime.MessageDialogOptions{
		Type:          runtime.QuestionDialog,
		Title:         sTitle,
		Message:       sMessage,
		DefaultButton: "确定",
		CancelButton:  "取消",
	}

	result, _ := runtime.MessageDialog(a.ctx, dialogOptions)

	return result
}

// Question消息
func (a *AppFun) WindowPrint() string {
	runtime.WindowPrint(a.ctx)

	return "响应"
}

// 打开选择目录对话框
func (a *AppFun) OpenDirectoryDialog(dialogOption string) *msgentity.MsgEntity {
	var dialogOptions runtime.OpenDialogOptions
	if strings.TrimSpace(dialogOption) != "" {
		err := json.Unmarshal([]byte(dialogOption), &dialogOptions)
		if err != nil {
			return msgentity.Err(1001, "选择文件发生异常")
		}
	} else {
		dialogOptions = runtime.OpenDialogOptions{Title: "选择目录", DefaultDirectory: "C:\\"}
	}

	str, err := runtime.OpenDirectoryDialog(a.ctx, dialogOptions)
	if err != nil {
		return msgentity.Err(1002, "选择目录发生异常")
	}

	return msgentity.Success(str, str)
}

// 打开选择文件对话框
func (a *AppFun) OpenFileDialog(dialogOption string) *msgentity.MsgEntity {
	var dialogOptions runtime.OpenDialogOptions
	if strings.TrimSpace(dialogOption) != "" {
		err := json.Unmarshal([]byte(dialogOption), &dialogOptions)
		if err != nil {
			return msgentity.Err(1001, "选择文件发生异常")
		}
	} else {
		dialogOptions = runtime.OpenDialogOptions{Title: "选择文件", DefaultDirectory: "C:\\"}
	}

	str, err := runtime.OpenFileDialog(a.ctx, dialogOptions)
	if err != nil {
		return msgentity.Err(1002, "选择文件发生异常")
	}

	return msgentity.Success(str, str)
}

// 打开保存文件对话框
func (a *AppFun) SaveFileDialog(dialogOption string) *msgentity.MsgEntity {
	var dialogOptions runtime.SaveDialogOptions
	if strings.TrimSpace(dialogOption) != "" {
		err := json.Unmarshal([]byte(dialogOption), &dialogOptions)
		if err != nil {
			return msgentity.Err(1001, "选择保存文件发生异常")
		}
	} else {
		dialogOptions = runtime.SaveDialogOptions{Title: "选择保存文件", DefaultDirectory: "C:\\"}
	}

	str, err := runtime.SaveFileDialog(a.ctx, dialogOptions)
	if err != nil {
		return msgentity.Err(1002, "选择保存文件发生异常")
	}

	return msgentity.Success(str, str)
}
