package wailsutil

import (
	"os"
	"path/filepath"
	"strings"

	"github.com/juju/fslock"
	"gopkg.in/ini.v1"
)

/**
 * 读取配置文件中指定块下的指定键
 */
func ReadConfig(file string, section string, key string) string {
	cfg, err := ini.Load(file)
	if err != nil {
		clogger.Error("文件读取错误:" + err.Error())
		os.Exit(1)
	}

	return string(cfg.Section(section).Key(key).Value())
}

/**
 * 文件加锁
 */
func LockFile() bool {
	CareteDir("./temp") // 创建文件夹

	dir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	fileName := dir + "/temp/App.lock"
	file, er := os.Open(fileName)
	defer func() { file.Close() }()
	if er != nil && os.IsNotExist(er) {
		file, _ = os.Create(fileName)
	}

	defer file.Close()

	lock := fslock.New(fileName)
	lockErr := lock.TryLock()
	if lockErr == nil {
		return true
	}

	if lockErr.Error() == "fslock is already locked" {
		clogger.Error("文件已枷锁,程序可能设置了单例运行模式且正在运行中")
		return false
	}

	clogger.Error("文件加锁失败异常:" + lockErr.Error())
	return false
}

/**
 * 创建文件夹
 */
func CareteDir(path string) {
	exist, err := PathExists(path)
	if err != nil {
		clogger.Error("获取路径错误:" + err.Error())
	}

	if exist {
		return
	}

	err = os.MkdirAll(path, os.ModePerm) // 创建文件夹
	if err != nil {
		clogger.Error("创建文件夹失败:" + err.Error())
		return
	}
}

/**
 * 判断文件夹是否存在
 */
func PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}

	if os.IsNotExist(err) {
		return false, nil
	}

	return false, err
}

// 获取当前执行程序目录
func GetCurrentDirectory() string {
	//返回绝对路径  filepath.Dir(os.Args[0])去除最后一个元素的路径
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		clogger.Error(err.Error())
	}

	//将\替换成/
	return strings.Replace(dir, "\\", "/", -1)
}
