module gitee.com/tomatomeatman/golang-repository/bricks/wailsutil

go 1.21.6

toolchain go1.23.4

require (
	gitee.com/tomatomeatman/golang-repository/bricks/model/msgentity v0.0.0-20250106013308-8701209ad601
	github.com/MakeNowJust/hotkey v0.0.0-20231028172355-3f1d3592581e
	github.com/energye/systray v1.0.2
	github.com/juju/fslock v0.0.0-20160525022230-4d5c94c67b4b
	github.com/wailsapp/wails/v2 v2.9.2
	gopkg.in/ini.v1 v1.67.0
)

require (
	github.com/godbus/dbus/v5 v5.1.0 // indirect
	github.com/leaanthony/slicer v1.6.0 // indirect
	github.com/leaanthony/u v1.1.1 // indirect
	github.com/lxn/win v0.0.0-20210218163916-a377121e959e // indirect
	github.com/tevino/abool v0.0.0-20220530134649-2bfc934cb23c // indirect
	golang.org/x/sys v0.29.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
