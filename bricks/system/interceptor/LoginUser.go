package interceptor

/**
 * 登录缓存对象
 * 非数据库实体
 */
type LoginUser struct {
	GsId       string            `json:"sId" gorm:"column:sId; type:varchar"`       //用户编号
	GsName     string            `json:"sName" gorm:"column:sName; type:varchar"`   //姓名
	GsNo       string            `json:"sNo" gorm:"column:sNo; type:varchar"`       //工号
	GsPass     string            `json:"-" gorm:"column:sPass; type:varchar"`       //密码
	GsSignPass string            `json:"-" gorm:"column:sSignPass; type:varchar"`   //手势密码
	GsType     string            `json:"sType" gorm:"column:sType; type:varchar"`   //用户类型编号
	GsOwner    string            `json:"sOwner" gorm:"column:sOwner; type:varchar"` //用户来源
	GiState    int               `json:"iState" gorm:"column:iState; type:int"`     //状态(-1,人员表人员被删除,1表示正常状态,2人员停用,)
	GdLastDate int64             `json:"dLastDate"`                                 //最后访问时间
	GsCookie   string            `json:"sCookie"`                                   //分配的Cookie
	GiDevice   int               `json:"iDevice"`                                   //设备类型,1:PC,2:手机,3:平板,4.....
	Gattached  map[string]string `json:"attached"`                                  //与登录相关的附属信息
}

/**
 * 复制
 * @return
 */
func (o *LoginUser) Clone() LoginUser {
	result := LoginUser{}
	result.GsId = o.GsId
	result.GsName = o.GsName
	result.GsNo = o.GsNo
	result.GsPass = o.GsPass
	result.GsSignPass = o.GsSignPass
	result.GsType = o.GsType
	result.GsOwner = o.GsOwner
	result.GiState = o.GiState
	result.GdLastDate = o.GdLastDate
	result.GsCookie = o.GsCookie

	if len(o.Gattached) > 0 {
		result.Gattached = map[string]string{}
		for key, val := range o.Gattached {
			result.Gattached[key] = val
		}
	}

	return result
}
