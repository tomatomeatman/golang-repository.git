package bridge

import (
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/urlutil"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/ginutil"
)

var baseSystemInsideKey string //桥接必须使用配置中的密钥

// @Controller 桥接服务-登录桥接操作接口
type LoginController struct {
	app.ControllerBaseFunc              //通用控制层接口方法
	ModuleService          LoginService //对应模块业务实体
}

/**
 * 初始化
 */
func init() {
	if app.IsNotCloudSystem() { //禁用桥接
		return
	}

	baseSystemInsideKey = app.ReadConfigKey("CloudSystem", "BaseSystemInsideKey", "123456").(string) //访问基本系统服务的密钥

	app.RegisterController(&LoginController{})
}

// 接口注册
func (control LoginController) RegisterUrl() {
	go ginutil.ControllerRegister("/login/in", LoginController{}.In, ginutil.POST)
	go ginutil.ControllerRegister("/login/out", LoginController{}.Out, ginutil.POST)
	go ginutil.ControllerRegister("/login/check", LoginController{}.Check, ginutil.POST)
	go ginutil.ControllerRegister("/login/heartbeat", LoginController{}.Heartbeat, ginutil.POST)
	go ginutil.ControllerRegister("/login/info", LoginController{}.GetLogin, ginutil.POST)
	go ginutil.ControllerRegister("/login/info/current", LoginController{}.GetCurrentLogin, ginutil.POST)
	go ginutil.ControllerRegister("/login/getid", LoginController{}.GetUserId, ginutil.POST)
}

// #region @Api {title=登录,如果用户和密码正确则返回通行令牌}
// @param {name=sNameOrNo dataType=string paramType=query explain=名称或工号 required=true}
// @param {name=sPass dataType=int paramType=query explain=密码 required=true}
// @param {name=sOwner dataType=string paramType=query explain=用户来源表 required=true}
// @param {name=iDevice dataType=int paramType=query explain=设备类型,1:PC,2:手机,3:平板,4..... required=false}
// @return {type=json explain=返回对象}
// @RequestMapping {name=In type=POST value=/login/in}
// #endregion
func (control LoginController) In(ctx ginutil.Context) interface{} {
	sNameOrNo := urlutil.GetParam(ctx.Request, "sNameOrNo", "").(string)
	sPass := urlutil.GetParam(ctx.Request, "sPass", "").(string)
	sOwner := urlutil.GetParam(ctx.Request, "sOwner", "").(string)
	iDevice := urlutil.GetParam(ctx.Request, "iDevice", 1).(int)

	return LoginService{}.In(ctx, sNameOrNo, sPass, sOwner, iDevice)
}

// #region @Api {title=用户登出}
// @param {name=sCookie dataType=string paramType=query explain=令牌 required=true}
// @return {type=json explain=返回对象}
// @RequestMapping {name=Out type=POST value=/login/out}
// #endregion
func (control LoginController) Out(ctx ginutil.Context) interface{} {
	sCookie := urlutil.GetParam(ctx.Request, "sCookie", "").(string)
	return LoginService{}.Out(ctx, sCookie)
}

// #region @Api {title=判断sCookie是否已经登录}
// @param {name=sCookie dataType=string paramType=query explain=令牌 required=true}
// @return {type=json explain=返回对象}
// @RequestMapping {name=Out type=POST value=/login/check}
// #endregion
func (control LoginController) Check(ctx ginutil.Context) interface{} {
	sCookie := urlutil.GetParam(ctx.Request, "sCookie", "").(string)
	return LoginService{}.Check(ctx, sCookie)
}

// #region @Api {title=维持登录的心跳操作}
// @param {name=sCookie dataType=string paramType=query explain=令牌 required=true}
// @return {type=json explain=返回对象}
// @RequestMapping {name=Out type=POST value=/login/heartbeat}
// #endregion
func (control LoginController) Heartbeat(ctx ginutil.Context) interface{} {
	sCookie := urlutil.GetParam(ctx.Request, "sCookie", "").(string)
	return LoginService{}.Heartbeat(ctx, sCookie)
}

// #region @Api {title=取登录用户信息,注意:限制为内部系统访问}
// @param {name=key dataType=string paramType=query explain=访问基本系统服务的密钥 required=true}
// @param {name=sCookie dataType=string paramType=query explain=令牌 required=true}
// @return {type=json explain=返回对象}
// @RequestMapping {name=Out type=POST value=/login/info}
// #endregion
func (control LoginController) GetLogin(ctx ginutil.Context) interface{} {
	sCookie := urlutil.GetParam(ctx.Request, "sCookie", "").(string)
	return LoginService{}.GetLogin(ctx, baseSystemInsideKey, sCookie)
}

// #region @Api {title=取当前登录用户简洁信息}
// @return {type=json explain=返回对象}
// @RequestMapping {name=Out type=POST value=/login/info/current}
// #endregion
func (control LoginController) GetCurrentLogin(ctx ginutil.Context) interface{} {
	return LoginService{}.GetCurrentLogin(ctx)
}

// #region @Api {title=根据用户和密码取对应的用户编号}
// @param {name=sNameOrNo dataType=string paramType=query explain=名称或工号 required=true}
// @param {name=sPass dataType=string paramType=query explain=密码 required=true}
// @param {name=sOwner dataType=string paramType=query explain=用户来源表 required=true}
// @return {type=json explain=返回对象}
// @RequestMapping {name=Out type=POST value=/login/getid}
// #endregion
func (control LoginController) GetUserId(ctx ginutil.Context) interface{} {
	sNameOrNo := urlutil.GetParam(ctx.Request, "sNameOrNo", "").(string)
	sPass := urlutil.GetParam(ctx.Request, "sPass", "").(string)
	sOwner := urlutil.GetParam(ctx.Request, "sOwner", "").(string)

	return LoginService{}.GetUserId(ctx, sNameOrNo, sPass, sOwner)
}
