package bridge

import (
	"gitee.com/tomatomeatman/golang-repository/bricks/model/msgentity"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/ginutil"
)

type UserAndRightService struct {
	app.ServiceBaseFunc
}

var (
	userAndRightServiceName = "" //桥接到登录服务的服务名,默认GBaseSystemServer
	//userAndRightServiceKey  = "" //桥接到登录服务的服务密钥
)

// 初始化
func init() {
	if app.IsNotCloudSystem() { //禁用桥接
		return
	}

	userAndRightServiceName = app.ReadConfigKey("CloudServer", "BaseServerName", "GBaseSystemServer").(string)
	//userAndRightServiceKey = app.ReadConfigKey("CloudServer", "BaseServerKey", "").(string)
}

/**
 * 清理指定用户的缓存
 * @param sUser 用户编号
 * @return
 */
func (service UserAndRightService) ClearCache(ctx ginutil.Context, cacheName, sUser string) *msgentity.MsgEntity {
	m := map[string]interface{}{"sLoginId": sUser}

	me := BridgeDao{}.Post(ctx, userAndRightServiceName, "/user/and/right/clear/cache", m, &msgentity.MsgEntity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*msgentity.MsgEntity)
}

/**
 * 根据用户取权限标识字符串(一个权限标识代表了多个可访问的url路径)
 * 不用判断请求用户是谁,因为其它人获取信息后没用,权限校验会在每次进行具体操作时进行再次判断
 * @param sUserId
 * @return
 */
func (service UserAndRightService) FindEnglishByUserId(ctx ginutil.Context, sUserId string) *msgentity.MsgEntity {
	m := map[string]interface{}{"sUserId": sUserId}

	me := BridgeDao{}.Post(ctx, userAndRightServiceName, "/user/and/right/find/english", m, &msgentity.MsgEntity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*msgentity.MsgEntity)
}

/**
 * 验证指定用户是否有访问指定url的权限(给内部拦截器用,直接返回Boolean)
 * @param sUserId
 * @param url
 * @return
 */
func (service UserAndRightService) CheckUrlRight(ctx ginutil.Context, sUserId, url string) bool {
	m := map[string]interface{}{"sUserId": sUserId, "url": url}

	me := BridgeDao{}.Post(ctx, userAndRightServiceName, "/user/and/right/check", m, &msgentity.MsgEntity{})
	if !me.Gsuccess {
		return false
	}

	return me.Gdata.(*msgentity.MsgEntity).Gsuccess
}

/**
 * 验证指定用户是否有访问指定url的权限
 * @param sUserId 验证的用户
 * @param url 请求验证的权限(URL地址)
 * @return
 */
func (service UserAndRightService) CheckRight(ctx ginutil.Context, sUserId, url string) *msgentity.MsgEntity {
	m := map[string]interface{}{"sUserId": sUserId, "url": url}

	me := BridgeDao{}.Post(ctx, userAndRightServiceName, "/user/and/right/check/right", m, &msgentity.MsgEntity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*msgentity.MsgEntity)
}

/**
 * 根据用户查询用户所拥有的权限编号集合
 * @param sUserId
 * @return
 */
func (service UserAndRightService) FindRightId(ctx ginutil.Context, sUserId string) *msgentity.MsgEntity {
	m := map[string]interface{}{"sUserId": sUserId}

	me := BridgeDao{}.Post(ctx, userAndRightServiceName, "/user/and/right/find/rightid", m, &msgentity.MsgEntity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*msgentity.MsgEntity)
}

/**
 * 根据用户查询用户所拥有的权限的最后更新时间
 * @param sUserId
 * @return
 */
func (service UserAndRightService) LastTime(ctx ginutil.Context, sUserId string) *msgentity.MsgEntity {
	m := map[string]interface{}{"sUserId": sUserId}

	me := BridgeDao{}.Post(ctx, userAndRightServiceName, "/user/and/right/lasttime", m, &msgentity.MsgEntity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*msgentity.MsgEntity)
}
