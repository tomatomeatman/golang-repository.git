package bridge

import (
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/urlutil"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/ginutil"
)

// @Controller 桥接服务-免拦截桥接操作接口
type IgnoreURLController struct {
	app.ControllerBaseFunc                  //通用控制层接口方法
	ModuleService          IgnoreURLService //对应模块业务实体
}

/**
 * 初始化
 */
func init() {
	if app.IsNotCloudSystem() { //禁用桥接
		return
	}

	app.RegisterController(&IgnoreURLController{})
}

// 接口注册
func (control IgnoreURLController) RegisterUrl() {
	go ginutil.ControllerRegister("/ignore/url/find/id", control.FindById, ginutil.POST)
	go ginutil.ControllerRegister("/ignore/url/check", control.CheckIgnoreUrl, ginutil.POST)
	go ginutil.ControllerRegister("/ignore/url/clear/cache", control.ClearCache, ginutil.POST)
}

// #region @Api {title=根据记录编号取对象}
// @param {name=sId dataType=string paramType=query explain=记录编号 required=true}
// @return {type=json explain=返回对象}
// @RequestMapping {name=FindById type=POST value=/ignore/url/find/id}
// #endregion
func (control IgnoreURLController) FindById(ctx ginutil.Context) interface{} {
	sId := urlutil.GetParam(ctx.Request, "sId", "").(string)
	return IgnoreURLService{}.FindById(ctx, nil, sId)
}

// #region @Api {title=内部方法:验证url是否可免于拦截,注意:限制为内部系统(生产者系统)访问}
// @param {name=sUrl dataType=string paramType=query explain=待检验的url required=true}
// @param {name=iMustLogin dataType=int paramType=query explain=是否必须登录,1:是;2:否 required=false}
// @param {name=sUserType dataType=string paramType=query explain=待检验的用户类型 required=false}
// @return {type=bool explain=返回对象}
// @RequestMapping {name=CheckIgnoreUrl type=POST value=/ignore/url/check}
// #endregion
func (control IgnoreURLController) CheckIgnoreUrl(ctx ginutil.Context) interface{} {
	sUrl := urlutil.GetParam(ctx.Request, "sUrl", "").(string)
	iMustLogin := urlutil.GetParam(ctx.Request, "iMustLogin", 2).(int)
	sUserType := urlutil.GetParam(ctx.Request, "sUserType", "").(string)
	return IgnoreURLService{}.CheckIgnoreUrl(ctx, sUrl, iMustLogin, sUserType)
}

// #region @Api {title=清理缓存}
// @return {type=json explain=返回对象}
// @RequestMapping {name=ClearCache type=POST value=/ignore/url/clear/cache}
// #endregion
func (control IgnoreURLController) ClearCache(ctx ginutil.Context) interface{} {
	return IgnoreURLService{}.ClearCache(ctx, "", "")
}
