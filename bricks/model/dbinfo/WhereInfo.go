package dbinfo

/**
 * @Description: 查询条件
 */
type WhereInfo struct {
	Name      string      //字段名
	Value     interface{} //字段值
	ValueName string      //值字段名,与Value互斥, 一般用于条件 Name < ValueNmae的情况,如 'where dStTime < dStopTime'
	Condition string      //查询条件符号
}

/**
 * @Description: 查询条件符号
 */
const (
	Eq         = "="
	Lt         = "<"
	Le         = "<="
	Gt         = ">"
	Ge         = ">="
	Ne         = "<>"
	Between    = "BETWEEN"
	NotBetween = "NOT BETWEEN"
	Like       = "LIKE"
	NoLike     = "NOT LIKE"
	IsNull     = "IS NULL"
	IsNotNull  = "IS NOT NULL"
	In         = "IN"
	NotIn      = "NOT IN"
)

func NewWhereInfo(name string, value interface{}) *WhereInfo {
	return &WhereInfo{
		Name:      name,
		Value:     value,
		ValueName: "",
		Condition: "=",
	}
}
