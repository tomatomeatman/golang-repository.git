package dbinfo

//分页查询条件组合类
type FindByPageParam struct {
	Gpage         Page        `json:"page"`         //分页信息对象
	Gorders       []OrderInfo `json:"orders"`       //排序信息对象集合
	GsLikeStr     string      `json:"sLikeStr"`     //全文检索条件
	GsLikeDateSt  string      `json:"sLikeDateSt"`  //全文检索查询记录修改时间范围条件-开始
	GsLikeDateEd  string      `json:"sLikeDateEd"`  //全文检索查询记录修改时间范围条件-结束
	Gcondition    interface{} `json:"condition"`    //常规查询条件对象(根据定义的类型转换)
	GsHideBigText bool        `json:"sHideBigText"` //是否隐藏大文本字段
	GsHideFields  string      `json:"sHideFields"`  //隐藏字段集合';'分隔
}
