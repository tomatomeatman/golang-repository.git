package tablekeylocal

import (
	"strings"

	"gitee.com/tomatomeatman/golang-repository/bricks/model/dbinfo"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/gorm"
)

var (
	tableName = ""
	dbName    = ""
)

/**
 * 记录编号序列管理表'TableKeyLocal'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type TableKeyLocalBase struct {
	GiId      int64  `json:"iId" gorm:"column:iId; type:bigint; NOT NULL; DEFAULT '0'; primary_key"` //记录编号(非自增长)
	GsType    string `json:"sType" gorm:"column:sType; type:varchar; NOT NULL"`                      //类型
	GsValue   string `json:"sValue" gorm:"column:sValue; type:varchar; NOT NULL"`                    //值
	GiIndex   int    `json:"iIndex" gorm:"column:iIndex; type:int; DEFAULT '0'"`                     //序号
	GiVersion int    `json:"iVersion" gorm:"column:iVersion; type:int; NOT NULL; DEFAULT '1'"`       //版本号
}

/**
 * 初始化
 */
func init() {
	// tableName = app.ReadConfigKey("DbVariables", "TableKeyName", "TableKeyLocal").(string)
	// dbName = app.ReadConfigKey("DbVariables", "MainDb", "BaseSystem").(string)
	// dbinfo.TableInfo{}.RegisterEntity(tableName+"Base", &TableKeyLocalBase{}) //注册注册数据库实体类结构体
	// dbinfo.TableInfo{}.RegisterTableInfo(tableName+"Base", TableKeyLocalBase{}.Info()) //注册数据库表信息
}

/**
 * 创建结构实体,并赋予默认值
 */
func (TableKeyLocalBase) New() dbinfo.Entity {
	return &TableKeyLocalBase{}
}

/**
 * 结构体映射表名,处理结构体名称与表名不一致的情况
 */
func (TableKeyLocalBase) TableName() string {
	if tableName != "" {
		return tableName
	}

	tableName = gorm.GetDbName(dbName) + "." + tableName
	tableName = strings.Replace(tableName, "..", ".", -1)

	return tableName
}

/**
 * 结构体映射表的字段名串
 */
func (TableKeyLocalBase) BaseColumnNames() string {
	return "iId,sType,sValue,iIndex,iVersion"
}

/**
 * 取基础实体,用于在子类(嵌套结构体)时同样获得基类
 */
func (TableKeyLocalBase) BaseEntity() dbinfo.Entity {
	return &TableKeyLocalBase{}
}

/**
 * 结构体映射库名,去除'Dev_'等前缀
 * @return
 */
func (TableKeyLocalBase) OwnerName() string {
	return dbName
}

/**
 * 结构体映射表名,无库名
 * @return
 */
func (TableKeyLocalBase) OwnerTable() string {
	return tableName
}

/**
 * 取数据结构信息
 * name 字段名
 */
func (TableKeyLocalBase) GetDataInfo(name string) *dbinfo.DataInfo {
	switch name {
	case "iId":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "TableKey", GsName: "iId", GsComment: "记录编号(非自增长)", GbDbField: true, GsDbFileType: "bigint", GsDefaultData: "0", GbExtra: false, Gbkey: true, GbNull: false, GiMaxLength: 20, GbDecimal: false, GiIntegralLength: 20, GiDecimalLength: 0, GiIndex: 0}
	case "sType":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "TableKey", GsName: "sType", GsComment: "类型", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "", GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 30, GbDecimal: false, GiIntegralLength: 30, GiDecimalLength: 0, GiIndex: 1}
	case "sValue":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "TableKey", GsName: "sValue", GsComment: "值", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "", GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 30, GbDecimal: false, GiIntegralLength: 30, GiDecimalLength: 0, GiIndex: 2}
	case "iIndex":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "TableKey", GsName: "iIndex", GsComment: "序号", GbDbField: true, GsDbFileType: "int", GsDefaultData: "0", GbExtra: false, Gbkey: false, GbNull: true, GiMaxLength: 11, GbDecimal: false, GiIntegralLength: 11, GiDecimalLength: 0, GiIndex: 3}
	case "iVersion":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "TableKey", GsName: "iVersion", GsComment: "版本号", GbDbField: true, GsDbFileType: "int", GsDefaultData: "1", GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 11, GbDecimal: false, GiIntegralLength: 11, GiDecimalLength: 0, GiIndex: 4}
	default:
		return nil
	}
}
