package tablekeylocal

import "gitee.com/tomatomeatman/golang-repository/bricks/model/dbinfo"

/**
 * 记录编号序列管理表'TableKeyLocal'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type TableKeyLocal struct {
	TableKeyLocalBase
}

/**
 * 创建结构实体,并赋予默认值
 */
func (TableKeyLocal) New() dbinfo.Entity {
	return &TableKeyLocal{}
}
