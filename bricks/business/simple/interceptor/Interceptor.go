package interceptor

import (
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"gitee.com/tomatomeatman/golang-repository/bricks/business/simple/ignoreurl"
	"gitee.com/tomatomeatman/golang-repository/bricks/business/simple/login"
	"gitee.com/tomatomeatman/golang-repository/bricks/business/simple/userandright"
	"gitee.com/tomatomeatman/golang-repository/bricks/model/log"
	"gitee.com/tomatomeatman/golang-repository/bricks/model/msgentity"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/urlutil"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/ginutil"
	"gopkg.in/ini.v1"
)

var (
	interceptorOnce                sync.Once             //初始化锁
	sysIgnoreUrlByNotLogin         = ""                  //当前系统设置的忽略拦截路径-未登录前 配置app.InterceptorIgnore
	sysIgnoreUrlByLogined          = ""                  //当前系统设置的忽略拦截路径-登录后 配置app.InterceptorIgnoreLogined
	sysIgnoreUrlByNotLoginList     = map[string]string{} //当前系统设置的忽略拦截路径-未登录前
	sysIgnoreUrlByNotLoginLikeList = []string{}          //当前系统设置的忽略拦截路径(模糊)-未登录前
	sysIgnoreUrlByLoginedList      = map[string]string{} //当前系统设置的忽略拦截路径-登录后
	sysIgnoreUrlByLoginedLikeList  = []string{}          //当前系统设置的忽略拦截路径(模糊)-登录后
	InterceptorLock                sync.Mutex            //保存锁
	appInsideKey                   string                //内部密钥
)

// --拦截器--//
type Interceptor struct{}

// 引入操作
func (ic Interceptor) Import() {
	//fmt.Println("无意义,只为了能将模块引入程序")
}

// 初始化
func init() {
	go interceptorOnce.Do(interceptor_initVar)
}

// 初始化参数
func interceptor_initVar() {
	// sysIgnoreUrlByNotLogin = app.ReadConfigKey("App", "InterceptorIgnore", "").(string)
	// sysIgnoreUrlByLogined = app.ReadConfigKey("App", "InterceptorIgnoreLogined", "").(string)
	// appInsideKey = app.ReadConfigKey("App", "InsideKey", "12345").(string)

	root := ""
	exePath, err := os.Executable()
	if err != nil {
		root = "."
	}

	root, _ = filepath.EvalSymlinks(filepath.Dir(exePath))

	configFilePath := strings.Replace(root+"/config/app.ini", "\\", "/", -1)

	_, err = os.Stat(configFilePath) //os.Stat获取文件信息
	if err != nil {
		if !os.IsExist(err) {
			log.Error("配置文件不存在", err)
			return
		}
	}

	appCfg, err := ini.Load(configFilePath)
	if err != nil {
		log.Error("配置文件读取错误", err)
		return
	}

	appSec := appCfg.Section("App")

	value, _ := appSec.GetKey("InterceptorIgnore")
	if value != nil {
		sysIgnoreUrlByNotLogin = value.String()
	}

	value, _ = appSec.GetKey("InterceptorIgnoreLogined")
	if value != nil {
		sysIgnoreUrlByLogined = value.String()
	}

	value, _ = appSec.GetKey("InsideKey")
	if value != nil {
		appInsideKey = value.String()
	}
}

/**
 * 拦截操作
 * @param w
 * @param r
 * @return
 */
func (inter Interceptor) Check(ctx ginutil.Context) bool {
	interceptorOnce.Do(interceptor_initVar) //防止未初始化结束

	sCookie := urlutil.GetParam(ctx.Request, "sCookie", "").(string) //必须在此获取,否则可能存在请求参数丢失的问题
	go login.LoginServer{}.Heartbeat(sCookie)                        //登录心跳操作,直接使用server,如果用controller则不能使用多线程,否则会存在并发读取ctx的问题
	// ginutil.CallFuncByUrl("/login/heartbeat", ginutil.POST, ctx)

	if inter.isIgnoresByNotLogin(ctx) { //属于配置文件中的未登录时免拦截路径,因程序可能属于无数据库的情况,所以需要配置文件检查
		return true
	}

	i := inter.checkIgnoreUrl(ctx, false, "") //验证是否可忽略路径模块,只能验证不用登录也能忽略的路径
	if i == 2 {                               //1:没有使用'忽略路径模块'2:路径为'可忽略路径'3:不是可忽略的路径
		inter.getUserInfoByCookie(ctx) //取当前用户信息
		return true                    //属于免登录的免拦截路径则通过
	}

	switch inter.isInside(ctx) { //判断是否属于内部请求的路径 0:不是内部请求, 1:是内部请求并通过, 2:是内部请求但不通过
	case 1: //1:是内部请求并通过
		return true
	case 2: //2:是内部请求但不通过
		return false
	default: //0:不是内部请求
		break
	}

	if !inter.validLogin(ctx) { // 用户登录验证
		return false
	}

	sUserType := urlutil.GetParam(ctx.Request, "sLoginUserType", "").(string)
	i = inter.checkIgnoreUrl(ctx, true, sUserType) //验证是否可忽略路径模块,一并验证登录后才能忽略的路径
	if i == 2 {                                    //1:没有使用'忽略路径模块'2:路径为'可忽略路径'3:不是可忽略的路径
		return true //属于登录后的免拦截路径则通过
	}

	//没有使用'忽略路径模块'或'不是可忽略的路径',必须交由权限进行控制访问
	if !inter.validRight(ctx) { //权限验证
		return false
	}

	return true
}

/**
 * 判断是否属于内部请求的路径; 0:不是内部请求, 1:是内部请求并通过, 2:是内部请求但不通过
 * 判断内部请求的依据:有内部请求密钥或请求地址为内部请求地址
 * @param w
 * @param r
 * @return
 */
func (inter Interceptor) isInside(ctx ginutil.Context) int {
	servletPath := ctx.Request.URL.Path

	key := urlutil.GetParam(ctx.Request, "sInsideKey", "").(string) //如果是内部请求,则内部密钥必须存在，从请求头获取内部密钥

	//--检查是否属于内部请求路径,如果是内部请求路径则检查访问key是否与当前系统一致,只要一致则可以通过--//
	if !strings.Contains(servletPath, "/inside/") { //包含'/inside/'的请求属于内部请求
		if key == "" { //不是内部请求路径,又不提供内部请求密钥,则返回"不是内部请求"
			return 0 //不是内部请求
		}

		if (appInsideKey == "") || (key != appInsideKey) { //有提供内部密钥,但是密钥错误,则返回"不是内部请求"
			return 0 //不是内部请求
		}

		//--对非内部请求的路径进行了内部请求操作,使用超管账号--//
		urlutil.AddAttrib(ctx.Request, "sLoginUserId", "00000000")     //将对应的id赋予请求属性,用于后续请求操作
		urlutil.AddAttrib(ctx.Request, "sLoginUserName", "superAdmin") //将对应的sName赋予请求属性,用于后续请求操作
		urlutil.AddAttrib(ctx.Request, "sLoginUserNo", "0000")         //将对应的sNo赋予请求属性,用于后续请求操作
		urlutil.AddAttrib(ctx.Request, "sLoginUserType", "admin")      //将对应的sType赋予请求属性,用于后续请求操作

		return 1 //内部请求操作
	}

	if (key == "") || (key != appInsideKey) {
		ctx.JSONP(http.StatusOK, msgentity.Err(1000001, "内部请求密钥错误"))
		return 2 //密钥不符
	}

	//--使用内部请求时,一旦没有输入登录密钥则视为超管操作--//
	sCookie := urlutil.GetParam(ctx.Request, "sCookie", "").(string) //获取request对象中的参数,优先: 头信息->参数-->属性

	if sCookie == "" { //确实没有登录
		urlutil.AddAttrib(ctx.Request, "sLoginUserId", "00000000")     //将对应的id赋予请求属性,用于后续请求操作
		urlutil.AddAttrib(ctx.Request, "sLoginUserName", "superAdmin") //将对应的sName赋予请求属性,用于后续请求操作
		urlutil.AddAttrib(ctx.Request, "sLoginUserNo", "0000")         //将对应的sNo赋予请求属性,用于后续请求操作
		urlutil.AddAttrib(ctx.Request, "sLoginUserType", "admin")      //将对应的sType赋予请求属性,用于后续请求操作
	} else {
		urlutil.AddAttrib(ctx.Request, "sCookie", sCookie) //放入传递的参数
	}

	return 1
}

/**
 * 判断是否属于未登录下免拦截的路径
 * @param request
 * @param response
 * @return
 */
func (inter Interceptor) isIgnoresByNotLogin(ctx ginutil.Context) bool {
	servletPath := ctx.Request.URL.Path

	if strings.Contains(sysIgnoreUrlByNotLogin, servletPath) { //访问'忽略路径的验证'方法必须跳过
		return true
	}

	if len(sysIgnoreUrlByNotLoginList) < 1 {
		array := strings.Split(sysIgnoreUrlByNotLogin, ",")

		InterceptorLock.Lock() //加锁

		for _, val := range array {
			if !strings.Contains(val, "*") {
				sysIgnoreUrlByNotLoginList[val] = ""
				continue
			}

			iEd := strings.Index(val, "*")
			val := val[:iEd-1]
			sysIgnoreUrlByNotLoginLikeList = append(sysIgnoreUrlByNotLoginLikeList, val)
			sysIgnoreUrlByNotLoginList[val] = ""
		}

		InterceptorLock.Unlock() //解锁
	}

	_, ok := sysIgnoreUrlByNotLoginList[servletPath] //明确的路径是否存在
	if ok {
		return true
	}

	if len(sysIgnoreUrlByNotLoginLikeList) > 0 {
		for _, val := range sysIgnoreUrlByNotLoginLikeList {
			if strings.Contains(servletPath, val) {
				return true
			}
		}
	}

	return false
}

/**
 * 用户登录验证
 * @param request
 * @param response
 * @return
 */
func (inter Interceptor) validLogin(ctx ginutil.Context) bool {
	sCookie := urlutil.GetParam(ctx.Request, "sCookie", "").(string) //获取request对象中的参数,优先: 头信息->参数-->属性

	if (sCookie == "") || (strings.ToUpper(sCookie) == "NULL") { //如果没有登录密钥,则返回错误
		ctx.JSONP(http.StatusOK, msgentity.Err(1000002, "您还没有登录或登录已超时，请重新登录！"))
		return false
	}

	//--检查登录密钥--//
	me := (login.LoginController{}.Check(ctx)).(*msgentity.MsgEntity)
	// me := ginutil.CallFuncByUrl("/login/check", ginutil.POST, ctx).(*msgentity.MsgEntity)
	if !me.Gsuccess {
		ctx.JSONP(http.StatusOK, msgentity.Err(1000000+me.Gdata.(int), me.Gmsg))
		return false
	}

	me = login.LoginServer{}.GetLogin(sCookie)
	// urlutil.AddAttrib(ctx.Request, "key", appInsideKey)
	// urlutil.AddAttrib(ctx.Request, "resultType", "map")
	// me = ginutil.CallFuncByUrl("/login/info", ginutil.POST, ctx).(*msgentity.MsgEntity)
	if !me.Gsuccess {
		ctx.JSONP(http.StatusOK, msgentity.Err(1100000+me.Gdata.(int), me.Gmsg))
		return false
	}

	// loginUser := me.Gdata.(map[string]interface{})

	// urlutil.AddAttrib(ctx.Request, "sLoginUserId", loginUser["sId"].(string))     //将对应的id赋予请求属性,用于后续请求操作
	// urlutil.AddAttrib(ctx.Request, "sLoginUserName", loginUser["sName"].(string)) //将对应的sName赋予请求属性,用于后续请求操作
	// urlutil.AddAttrib(ctx.Request, "sLoginUserNo", loginUser["sNo"].(string))     //将对应的sNo赋予请求属性,用于后续请求操作
	// urlutil.AddAttrib(ctx.Request, "sLoginUserType", loginUser["sType"].(string)) //将对应的sType赋予请求属性,用于后续请求操作
	// urlutil.AddAttrib(ctx.Request, "sCookie", sCookie)                            //放入传递的参数

	// //-- 含有附属信息则一并加入 --//
	// var attached map[string]interface{}
	// if loginUser["attached"] != nil {
	// 	attached = loginUser["attached"].(map[string]interface{})
	// }

	// if len(attached) > 0 {
	// 	for key, val := range attached {
	// 		urlutil.AddAttrib(ctx.Request, key, fmt.Sprintf("%v", val))
	// 	}
	// }

	// return true

	loginUser := me.Gdata.(*login.LoginUser)

	urlutil.AddAttrib(ctx.Request, "sLoginUserId", loginUser.GsId)     //将对应的id赋予请求属性,用于后续请求操作
	urlutil.AddAttrib(ctx.Request, "sLoginUserName", loginUser.GsName) //将对应的sName赋予请求属性,用于后续请求操作
	urlutil.AddAttrib(ctx.Request, "sLoginUserNo", loginUser.GsNo)     //将对应的sNo赋予请求属性,用于后续请求操作
	urlutil.AddAttrib(ctx.Request, "sLoginUserType", loginUser.GsType) //将对应的sType赋予请求属性,用于后续请求操作
	urlutil.AddAttrib(ctx.Request, "sCookie", sCookie)                 //放入传递的参数

	//-- 含有附属信息则一并加入 --//
	if len(loginUser.Gattached) > 0 {
		for key, val := range loginUser.Gattached {
			urlutil.AddAttrib(ctx.Request, key, val)
		}
	}

	return true
}

/**
 * 取用户信息
 */
func (inter Interceptor) getUserInfoByCookie(ctx ginutil.Context) {
	sCookie := urlutil.GetParam(ctx.Request, "sCookie", "").(string) //获取request对象中的参数,优先: 头信息->参数-->属性

	if (sCookie == "") || (strings.ToUpper(sCookie) == "NULL") { //如果没有登录密钥,则不能获取
		return
	}

	//--检查登录密钥--//
	me := (login.LoginController{}.Check(ctx)).(*msgentity.MsgEntity)
	// me := ginutil.CallFuncByUrl("/login/check", ginutil.POST, ctx).(*msgentity.MsgEntity)
	if !me.Gsuccess {
		return
	}

	me = login.LoginServer{}.GetLogin(sCookie)
	// urlutil.AddAttrib(ctx.Request, "key", appInsideKey)
	// urlutil.AddAttrib(ctx.Request, "resultType", "map")
	// me = ginutil.CallFuncByUrl("/login/info", ginutil.POST, ctx).(*msgentity.MsgEntity)
	if !me.Gsuccess {
		return
	}

	loginUser := me.Gdata.(*login.LoginUser)

	urlutil.AddAttrib(ctx.Request, "sLoginUserId", loginUser.GsId)     //将对应的id赋予请求属性,用于后续请求操作
	urlutil.AddAttrib(ctx.Request, "sLoginUserName", loginUser.GsName) //将对应的sName赋予请求属性,用于后续请求操作
	urlutil.AddAttrib(ctx.Request, "sLoginUserNo", loginUser.GsNo)     //将对应的sNo赋予请求属性,用于后续请求操作
	urlutil.AddAttrib(ctx.Request, "sLoginUserType", loginUser.GsType) //将对应的sType赋予请求属性,用于后续请求操作
	urlutil.AddAttrib(ctx.Request, "sCookie", sCookie)                 //放入传递的参数

	//-- 含有附属信息则一并加入 --//
	if len(loginUser.Gattached) > 0 {
		for key, val := range loginUser.Gattached {
			urlutil.AddAttrib(ctx.Request, key, val)
		}
	}

	// loginUser := me.Gdata.(map[string]interface{})

	// urlutil.AddAttrib(ctx.Request, "sLoginUserId", loginUser["sId"].(string))     //将对应的id赋予请求属性,用于后续请求操作
	// urlutil.AddAttrib(ctx.Request, "sLoginUserName", loginUser["sName"].(string)) //将对应的sName赋予请求属性,用于后续请求操作
	// urlutil.AddAttrib(ctx.Request, "sLoginUserNo", loginUser["sNo"].(string))     //将对应的sNo赋予请求属性,用于后续请求操作
	// urlutil.AddAttrib(ctx.Request, "sLoginUserType", loginUser["sType"].(string)) //将对应的sType赋予请求属性,用于后续请求操作
	// urlutil.AddAttrib(ctx.Request, "sCookie", sCookie)                            //放入传递的参数

	// //-- 含有附属信息则一并加入 --//
	// var attached map[string]interface{}
	// if loginUser["attached"] != nil {
	// 	attached = loginUser["attached"].(map[string]interface{})
	// }

	// if len(attached) > 0 {
	// 	for key, val := range attached {
	// 		urlutil.AddAttrib(ctx.Request, key, fmt.Sprintf("%v", val))
	// 	}
	// }
}

/**
 * 用户权限验证
 * @param request
 * @param response
 * @return
 * @throws IOException
 */
func (inter Interceptor) validRight(ctx ginutil.Context) bool {
	servletPath := ctx.Request.URL.Path

	sUserId := urlutil.GetAttrib(ctx.Request, "sLoginUserId") //验证'是否登录'时已经添加

	me := userandright.UserAndRightService{}.CheckRight(ctx, sUserId, servletPath)
	// urlutil.AddAttrib(ctx.Request, "sUserId", sUserId)
	// urlutil.AddAttrib(ctx.Request, "url", servletPath)
	// me := ginutil.CallFuncByUrl("/user/and/right/check/right", ginutil.POST, ctx).(*msgentity.MsgEntity)
	if me.Gsuccess {
		return true //有权限
	}

	ctx.JSONP(http.StatusOK, msgentity.Err(1000006, "当前用户没有访问'", servletPath, "'的权限",
		"请检查1:是否在权限中添加了权限信息2:是否分配权限到具体角色或用户;3:若权限分配到了角色则是否已经将角色分配到用户"))

	return false //没有权限,也不是可忽略的路径,禁止
}

/**
 * 验证是否可忽略路径模块
 * @param ctx 上下文信息, ctx.Request.URL.Path为要验证的路径
 * @param isMustLogin 是否必须登录
 * @param sUserType 待检验的用户类型
 * @return 1:没有使用'忽略路径模块';2:路径为'可忽略路径';3:不是可忽略的路径
 */
func (inter Interceptor) checkIgnoreUrl(ctx ginutil.Context, isMustLogin bool, sUserType string) int {
	url := ctx.Request.URL.Path

	if isMustLogin { //判断是否是在配置文件中要求登录后就能免拦截的路径
		if len(sysIgnoreUrlByLoginedList) < 1 {
			array := strings.Split(sysIgnoreUrlByLogined, ",")

			InterceptorLock.Lock() //加锁

			for _, val := range array {
				if !strings.Contains(val, "*") {
					sysIgnoreUrlByLoginedList[val] = ""
					continue
				}

				iEd := strings.Index(val, "*")
				val := val[:iEd-1]

				sysIgnoreUrlByLoginedLikeList = append(sysIgnoreUrlByLoginedLikeList, val)
				sysIgnoreUrlByLoginedList[val] = "" //明确的也属于
			}

			InterceptorLock.Unlock() //解锁
		}

		_, ok := sysIgnoreUrlByLoginedList[url] //明确的路径是否存在
		if ok {
			return 2
		}

		if len(sysIgnoreUrlByLoginedLikeList) > 0 {
			for _, val := range sysIgnoreUrlByLoginedLikeList {
				if strings.Contains(url, val) {
					return 2
				}
			}
		}
	}

	me := ignoreurl.IgnoreURLService{}.CheckIgnoreUrl(url, isMustLogin, sUserType)

	// iMustLogin := "2"
	// if isMustLogin {
	// 	iMustLogin = "1"
	// }
	// ctx.AddParam("iMustLogin", iMustLogin)
	// ctx.AddParam("sUserType", sUserType)
	// me := ginutil.CallFuncByUrl("/ignore/url/check", ginutil.POST, ctx).(*msgentity.MsgEntity)
	if me.Gsuccess && (me.Gdata.(bool)) {
		return 2 //路径是可忽略路径
	}

	return 3 //不是可忽略的路径,禁止
}
