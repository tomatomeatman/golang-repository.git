package loginuser

import (
	"time"

	"gitee.com/tomatomeatman/golang-repository/bricks/model/dbinfo"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/gorm"
)

var tableInfo = []string{"", "BaseSystem", "LoginUser"}

/**
 * 初始化
 */
func init() {
	dbinfo.RegisterEntity("LoginUserBase", &LoginUserBase{}) //注册注册数据库实体类结构体
}

/**
 * 用户表'LoginUser'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type LoginUserBase struct {
	GsId           string    `json:"sId" gorm:"column:sId; type:varchar; NOT NULL; primary_key"`                        //表编号
	GsName         string    `json:"sName" gorm:"column:sName; type:varchar; NOT NULL"`                                 //名称
	GsNo           string    `json:"sNo" gorm:"column:sNo; type:varchar; NOT NULL"`                                     //标识(组合唯一)
	GsPass         string    `json:"sPass" gorm:"column:sPass; type:varchar; NOT NULL"`                                 //密码
	GsType         string    `json:"sType" gorm:"column:sType; type:varchar; NOT NULL; DEFAULT '管理员'"`                  //类型
	GsOwner        string    `json:"sOwner" gorm:"column:sOwner; type:varchar; DEFAULT 'BaseSystem.LoginUser'"`         //来源
	GsMemo         string    `json:"sMemo" gorm:"column:sMemo; type:varchar"`                                           //备注
	GsCreator      string    `json:"sCreator" gorm:"column:sCreator; type:varchar; NOT NULL; DEFAULT '00000000'"`       //创建人员
	GdCreateDate   time.Time `json:"dCreateDate" gorm:"column:dCreateDate; type:datetime; NOT NULL"`                    //创建时间
	GsModifieder   string    `json:"sModifieder" gorm:"column:sModifieder; type:varchar; NOT NULL; DEFAULT '00000000'"` //修改人员
	GdModifiedDate time.Time `json:"dModifiedDate" gorm:"column:dModifiedDate; type:datetime; NOT NULL"`                //修改时间
	GiState        int       `json:"iState" gorm:"column:iState; type:int; NOT NULL; DEFAULT '1'"`                      //状态(枚举,1:启用;2:禁用)
	GiIndex        int       `json:"iIndex" gorm:"column:iIndex; type:int; NOT NULL; DEFAULT '1'"`                      //序号
	GiVersion      int       `json:"iVersion" gorm:"column:iVersion; type:int; NOT NULL; DEFAULT '1'"`                  //版本号
}

/**
 * 创建结构实体
 * @return
 */
func (LoginUserBase) New() dbinfo.Entity {
	return &LoginUserBase{}
}

/**
 * 取基础实体,用于在子类(嵌套结构体)时同样获得基类
 */
func (LoginUserBase) BaseEntity() dbinfo.Entity {
	return &LoginUserBase{}
}

/**
 * 结构体映射表名,处理结构体名称与表名不一致的情况
 * @return
 */
func (LoginUserBase) TableName() string {
	if tableInfo[0] != "" {
		return tableInfo[0]
	}

	tableInfo[0] = gorm.GetDbName(tableInfo[1]) + tableInfo[2]

	return tableInfo[0]
}

/**
 * 结构体映射库名,去除'Dev_'等前缀
 * @return
 */
func (LoginUserBase) OwnerName() string {
	return tableInfo[1]
}

/**
 * 结构体映射表名,无库名
 * @return
 */
func (LoginUserBase) OwnerTable() string {
	return tableInfo[2]
}

/**
 * 结构体映射表的字段名串
 * @return
 */
func (LoginUserBase) BaseColumnNames() string {
	return "sId,sName,sNo,sPass,sType,sOwner,sMemo,sCreator,dCreateDate,sModifieder,dModifiedDate,iState,iIndex,iVersion"
}

/**
 * 取数据结构信息
 * @param name 字段名
 * @return
 */
func (LoginUserBase) GetDataInfo(name string) *dbinfo.DataInfo {
	switch name {
	case "sId":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginUser", GsName: "sId", GsComment: "表编号", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "", GoDefaultData: "", GbExtra: false, Gbkey: true, GbNull: false, GiMaxLength: 8, GbDecimal: false, GiIntegralLength: 8, GiDecimalLength: 0, GiIndex: 0}
	case "sName":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginUser", GsName: "sName", GsComment: "名称", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "", GoDefaultData: "", GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 32, GbDecimal: false, GiIntegralLength: 32, GiDecimalLength: 0, GiIndex: 1}
	case "sNo":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginUser", GsName: "sNo", GsComment: "标识(组合唯一)", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "", GoDefaultData: "", GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 64, GbDecimal: false, GiIntegralLength: 64, GiDecimalLength: 0, GiIndex: 2}
	case "sPass":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginUser", GsName: "sPass", GsComment: "密码", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "", GoDefaultData: "", GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 64, GbDecimal: false, GiIntegralLength: 64, GiDecimalLength: 0, GiIndex: 3}
	case "sType":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginUser", GsName: "sType", GsComment: "类型", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "管理员", GoDefaultData: "管理员", GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 50, GbDecimal: false, GiIntegralLength: 50, GiDecimalLength: 0, GiIndex: 4}
	case "sOwner":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginUser", GsName: "sOwner", GsComment: "来源", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "BaseSystem.LoginUser", GoDefaultData: "BaseSystem.LoginUser", GbExtra: false, Gbkey: false, GbNull: true, GiMaxLength: 100, GbDecimal: false, GiIntegralLength: 100, GiDecimalLength: 0, GiIndex: 5}
	case "sMemo":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginUser", GsName: "sMemo", GsComment: "备注", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "", GoDefaultData: "", GbExtra: false, Gbkey: false, GbNull: true, GiMaxLength: 200, GbDecimal: false, GiIntegralLength: 200, GiDecimalLength: 0, GiIndex: 6}
	case "sCreator":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginUser", GsName: "sCreator", GsComment: "创建人员", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "00000000", GoDefaultData: "00000000", GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 8, GbDecimal: false, GiIntegralLength: 8, GiDecimalLength: 0, GiIndex: 7}
	case "dCreateDate":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginUser", GsName: "dCreateDate", GsComment: "创建时间", GbDbField: true, GsDbFileType: "datetime", GsDefaultData: "", GoDefaultData: time.Now().Format("2006-01-02 15:04:05"), GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 23, GbDecimal: false, GiIntegralLength: 23, GiDecimalLength: 0, GiIndex: 8}
	case "sModifieder":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginUser", GsName: "sModifieder", GsComment: "修改人员", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "00000000", GoDefaultData: "00000000", GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 8, GbDecimal: false, GiIntegralLength: 8, GiDecimalLength: 0, GiIndex: 9}
	case "dModifiedDate":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginUser", GsName: "dModifiedDate", GsComment: "修改时间", GbDbField: true, GsDbFileType: "datetime", GsDefaultData: "", GoDefaultData: time.Now().Format("2006-01-02 15:04:05"), GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 23, GbDecimal: false, GiIntegralLength: 23, GiDecimalLength: 0, GiIndex: 10}
	case "iState":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginUser", GsName: "iState", GsComment: "状态(枚举,1:启用;2:禁用)", GbDbField: true, GsDbFileType: "int", GsDefaultData: "1", GoDefaultData: 1, GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 8, GbDecimal: false, GiIntegralLength: 8, GiDecimalLength: 0, GiIndex: 11}
	case "iIndex":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginUser", GsName: "iIndex", GsComment: "序号", GbDbField: true, GsDbFileType: "int", GsDefaultData: "1", GoDefaultData: 1, GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 8, GbDecimal: false, GiIntegralLength: 8, GiDecimalLength: 0, GiIndex: 12}
	case "iVersion":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginUser", GsName: "iVersion", GsComment: "版本号", GbDbField: true, GsDbFileType: "int", GsDefaultData: "1", GoDefaultData: 1, GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 8, GbDecimal: false, GiIntegralLength: 8, GiDecimalLength: 0, GiIndex: 13}
	default:
		return nil
	}
}
