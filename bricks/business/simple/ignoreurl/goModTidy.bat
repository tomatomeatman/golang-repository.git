cd /d %~dp0

::设置代理
SET GOPROXY=https://goproxy.cn
SET GONOSUMDB=gitee.com
SET GONOPROXY=gitee.com

SET "currentPath=%cd:\=/%"
SET "prefixToRemove=/bricks/"

:: 使用 PowerShell 进行字符串替换
for /f "delims=" %%i in ('powershell -Command "[string]$path='%currentPath%'; $path -replace '^.*%prefixToRemove%', ''"') do set "relativePath=%%i"

del go.mod

del go.sum

go mod init gitee.com/tomatomeatman/golang-repository/bricks/%relativePath%

go mod tidy -go=1.21.6

go get -u ./...

pause
