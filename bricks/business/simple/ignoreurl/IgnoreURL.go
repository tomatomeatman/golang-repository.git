package ignoreurl

import "gitee.com/tomatomeatman/golang-repository/bricks/model/dbinfo"

var IgnoreURL_tableName = ""

/**
 * 拦截器忽略路径'IgnoreURL'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type IgnoreURL struct {
	IgnoreURLBase
	GsMustLoginText string `json:"sMustLoginText" gorm:"<-:false;column:sMustLoginText"` //必须登录(布尔值,1:是;2:否)
}

/**
 * 创建结构实体,并赋予默认值
 */
func (IgnoreURL) New() dbinfo.Entity {
	return &IgnoreURL{}
}
