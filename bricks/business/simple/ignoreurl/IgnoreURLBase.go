package ignoreurl

import (
	"time"

	"gitee.com/tomatomeatman/golang-repository/bricks/model/dbinfo"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/gorm"
)

var tableInfo = []string{"", "BaseSystem", "IgnoreURL"}

/**
 * 初始化
 */
func init() {
	dbinfo.RegisterEntity("IgnoreURLBase", &IgnoreURLBase{}) //注册注册数据库实体类结构体
}

/**
 * 拦截器忽略路径'IgnoreURL'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type IgnoreURLBase struct {
	GsId           string    `json:"sId" gorm:"column:sId; type:varchar; NOT NULL; primary_key"`                        //记录编号
	GsUrlName      string    `json:"sUrlName" gorm:"column:sUrlName; type:varchar; NOT NULL"`                           //路径名称类别
	GsIgnoreUrl    string    `json:"sIgnoreUrl" gorm:"column:sIgnoreUrl; type:varchar; NOT NULL"`                       //路径集合
	GiMustLogin    int       `json:"iMustLogin" gorm:"column:iMustLogin; type:int; NOT NULL; DEFAULT '1'"`              //必须登录(布尔值,1:是;2:否)
	GsOnlyUserType string    `json:"sOnlyUserType" gorm:"column:sOnlyUserType; type:varchar"`                           //限用户类型(分号分隔)
	GsMemo         string    `json:"sMemo" gorm:"column:sMemo; type:varchar"`                                           //备注
	GsCreator      string    `json:"sCreator" gorm:"column:sCreator; type:varchar; NOT NULL; DEFAULT '00000000'"`       //创建者
	GdCreateDate   time.Time `json:"dCreateDate" gorm:"column:dCreateDate; type:datetime; NOT NULL"`                    //创建时间
	GsModifieder   string    `json:"sModifieder" gorm:"column:sModifieder; type:varchar; NOT NULL; DEFAULT '00000000'"` //修改人
	GdModifiedDate time.Time `json:"dModifiedDate" gorm:"column:dModifiedDate; type:datetime; NOT NULL"`                //修改时间
	GiState        int       `json:"iState" gorm:"column:iState; type:int; NOT NULL; DEFAULT '1'"`                      //版本
	GiIndex        int       `json:"iIndex" gorm:"column:iIndex; type:int; NOT NULL; DEFAULT '1'"`                      //序号
	GiVersion      int       `json:"iVersion" gorm:"column:iVersion; type:int; NOT NULL; DEFAULT '1'"`                  //版本号
}

/**
 * 创建结构实体
 * @return
 */
func (IgnoreURLBase) New() dbinfo.Entity {
	return &IgnoreURLBase{}
}

/**
 * 取基础实体,用于在子类(嵌套结构体)时同样获得基类
 */
func (IgnoreURLBase) BaseEntity() dbinfo.Entity {
	return &IgnoreURLBase{}
}

/**
 * 结构体映射表名,处理结构体名称与表名不一致的情况
 * @return
 */
func (IgnoreURLBase) TableName() string {
	if tableInfo[0] != "" {
		return tableInfo[0]
	}

	tableInfo[0] = gorm.GetDbName(tableInfo[1]) + tableInfo[2]

	return tableInfo[0]
}

/**
 * 结构体映射库名,去除'Dev_'等前缀
 * @return
 */
func (IgnoreURLBase) OwnerName() string {
	return tableInfo[1]
}

/**
 * 结构体映射表名,无库名
 * @return
 */
func (IgnoreURLBase) OwnerTable() string {
	return tableInfo[2]
}

/**
 * 结构体映射表的字段名串
 * @return
 */
func (IgnoreURLBase) BaseColumnNames() string {
	return "sId,sUrlName,sIgnoreUrl,iMustLogin,sOnlyUserType,sMemo,sCreator,dCreateDate,sModifieder,dModifiedDate,iState,iIndex,iVersion"
}

/**
 * 取数据结构信息
 * @param name 字段名
 * @return
 */
func (IgnoreURLBase) GetDataInfo(name string) *dbinfo.DataInfo {
	switch name {
	case "sId":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "IgnoreURL", GsName: "sId", GsComment: "记录编号", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "", GoDefaultData: "", GbExtra: false, Gbkey: true, GbNull: false, GiMaxLength: 8, GbDecimal: false, GiIntegralLength: 8, GiDecimalLength: 0, GiIndex: 0}
	case "sUrlName":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "IgnoreURL", GsName: "sUrlName", GsComment: "路径名称类别", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "", GoDefaultData: "", GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 50, GbDecimal: false, GiIntegralLength: 50, GiDecimalLength: 0, GiIndex: 1}
	case "sIgnoreUrl":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "IgnoreURL", GsName: "sIgnoreUrl", GsComment: "路径集合", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "", GoDefaultData: "", GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 5000, GbDecimal: false, GiIntegralLength: 5000, GiDecimalLength: 0, GiIndex: 2}
	case "iMustLogin":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "IgnoreURL", GsName: "iMustLogin", GsComment: "必须登录(布尔值,1:是;2:否)", GbDbField: true, GsDbFileType: "int", GsDefaultData: "1", GoDefaultData: 1, GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 11, GbDecimal: false, GiIntegralLength: 11, GiDecimalLength: 0, GiIndex: 3}
	case "sOnlyUserType":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "IgnoreURL", GsName: "sOnlyUserType", GsComment: "限用户类型(分号分隔)", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "", GoDefaultData: "", GbExtra: false, Gbkey: false, GbNull: true, GiMaxLength: 500, GbDecimal: false, GiIntegralLength: 500, GiDecimalLength: 0, GiIndex: 4}
	case "sMemo":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "IgnoreURL", GsName: "sMemo", GsComment: "备注", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "", GoDefaultData: "", GbExtra: false, Gbkey: false, GbNull: true, GiMaxLength: 200, GbDecimal: false, GiIntegralLength: 200, GiDecimalLength: 0, GiIndex: 5}
	case "sCreator":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "IgnoreURL", GsName: "sCreator", GsComment: "创建者", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "00000000", GoDefaultData: "00000000", GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 8, GbDecimal: false, GiIntegralLength: 8, GiDecimalLength: 0, GiIndex: 6}
	case "dCreateDate":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "IgnoreURL", GsName: "dCreateDate", GsComment: "创建时间", GbDbField: true, GsDbFileType: "datetime", GsDefaultData: "", GoDefaultData: time.Now().Format("2006-01-02 15:04:05"), GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 23, GbDecimal: false, GiIntegralLength: 23, GiDecimalLength: 0, GiIndex: 7}
	case "sModifieder":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "IgnoreURL", GsName: "sModifieder", GsComment: "修改人", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "00000000", GoDefaultData: "00000000", GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 8, GbDecimal: false, GiIntegralLength: 8, GiDecimalLength: 0, GiIndex: 8}
	case "dModifiedDate":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "IgnoreURL", GsName: "dModifiedDate", GsComment: "修改时间", GbDbField: true, GsDbFileType: "datetime", GsDefaultData: "", GoDefaultData: time.Now().Format("2006-01-02 15:04:05"), GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 23, GbDecimal: false, GiIntegralLength: 23, GiDecimalLength: 0, GiIndex: 9}
	case "iState":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "IgnoreURL", GsName: "iState", GsComment: "版本", GbDbField: true, GsDbFileType: "int", GsDefaultData: "1", GoDefaultData: 1, GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 11, GbDecimal: false, GiIntegralLength: 11, GiDecimalLength: 0, GiIndex: 10}
	case "iIndex":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "IgnoreURL", GsName: "iIndex", GsComment: "序号", GbDbField: true, GsDbFileType: "int", GsDefaultData: "1", GoDefaultData: 1, GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 11, GbDecimal: false, GiIntegralLength: 11, GiDecimalLength: 0, GiIndex: 11}
	case "iVersion":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "IgnoreURL", GsName: "iVersion", GsComment: "版本号", GbDbField: true, GsDbFileType: "int", GsDefaultData: "1", GoDefaultData: 1, GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 11, GbDecimal: false, GiIntegralLength: 11, GiDecimalLength: 0, GiIndex: 12}
	default:
		return nil
	}
}
