package login

import (
	"strings"

	"gitee.com/tomatomeatman/golang-repository/bricks/model/msgentity"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/app"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/gorm"
	Log "github.com/cihub/seelog"
)

type LoginDao struct {
	app.DaoBaseFunc
}

/**
 * 取用户名或工号对应的用户集合
 * @param sNameOrNo
 * @param sOwner
 * @return
 */
func (LoginDao) FindByNameOrNo(sNameOrNo, sOwner string) *msgentity.MsgEntity {
	//txt := SqlFactory{}.ReplaceVariable(``)
	txt := `SELECT sId,sName,sNo,sPass,sType,sOwner,iState
	FROM ${BaseSystem}LoginUser
	WHERE sNo = @sNameOrNo AND sOwner = @sOwner
	UNION 
	SELECT sId,sName,sNo,sPass,sType,sOwner,iState
	FROM ${BaseSystem}LoginUser
	WHERE sName = @sNameOrNo AND sOwner = @sOwner`

	tableName := sOwner
	if tableName == "" {
		tableName = "LoginUser"
	}

	txt = strings.Replace(txt, "${BaseSystem}LoginUser", tableName, -1)

	where := map[string]interface{}{
		"sNameOrNo": sNameOrNo,
		"sOwner":    sOwner,
	}

	list := []LoginUser{}
	dbResult := gorm.Raw(txt, where).Find(&list)
	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return msgentity.Err(1002, "查询发生异常:", dbResult.Error)
	}

	return msgentity.Success(list, "查询成功")
	// rows, err := gorm.Raw(txt, where).Rows()
	// defer rows.Close()

	// if nil != err {
	// 	Log.Error("查询发生异常:", err)
	// 	return msgentity.Err(1002, "查询发生异常:", err)
	// }

	// res := SqlFactory{}.ScanRows2mapI(rows)
	// if res == nil {
	// 	return msgentity.Err(1003, "查询发生异常")
	// }

	// if len(res) < 1 {
	// 	return msgentity.Err(1004, "没有数据")
	// }

	// return msgentity.Success(res, "查询成功")
}
