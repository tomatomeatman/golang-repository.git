package userandright

import "gitee.com/tomatomeatman/golang-repository/bricks/utils/app"

/**
 * 用户权限表UserAndRight表基本业务操作结构体
 */
type UserAndRightDao struct {
	app.DaoBaseFunc
}
