package loginlog

import (
	"time"

	"gitee.com/tomatomeatman/golang-repository/bricks/model/dbinfo"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/gorm"
)

var tableInfo = []string{"", "BaseSystem", "LoginLog"}

/**
 * 初始化
 */
func init() {
	dbinfo.RegisterEntity("LoginLogBase", &LoginLogBase{}) //注册注册数据库实体类结构体
}

/**
 * 登录日志'LoginLog'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type LoginLogBase struct {
	GiId           int64     `json:"iId" gorm:"column:iId; type:bigintAUTO_INCREMENT; NOT NULL; primary_key"`           //记录编号
	GsName         string    `json:"sName" gorm:"column:sName; type:varchar"`                                           //日志类型(冗余)
	GiType         int       `json:"iType" gorm:"column:iType; type:int; NOT NULL; DEFAULT '1'"`                        //日志类型
	GsUserId       string    `json:"sUserId" gorm:"column:sUserId; type:varchar"`                                       //用户编号
	GsUser         string    `json:"sUser" gorm:"column:sUser; type:varchar"`                                           //用户名(冗余)
	GsIp           string    `json:"sIp" gorm:"column:sIp; type:varchar"`                                               //IP地址
	GsMACAddress   string    `json:"sMACAddress" gorm:"column:sMACAddress; type:varchar"`                               //MAC地址
	GdLogDate      time.Time `json:"dLogDate" gorm:"column:dLogDate; type:datetime"`                                    //发生时间
	GdLastCall     time.Time `json:"dLastCall" gorm:"column:dLastCall; type:datetime"`                                  //最后访问时间
	GsMemo         string    `json:"sMemo" gorm:"column:sMemo; type:varchar"`                                           //备注
	GsCreator      string    `json:"sCreator" gorm:"column:sCreator; type:varchar; NOT NULL; DEFAULT '00000000'"`       //创建者
	GdCreateDate   time.Time `json:"dCreateDate" gorm:"column:dCreateDate; type:datetime; NOT NULL"`                    //创建时间
	GsModifieder   string    `json:"sModifieder" gorm:"column:sModifieder; type:varchar; NOT NULL; DEFAULT '00000000'"` //修改人
	GdModifiedDate time.Time `json:"dModifiedDate" gorm:"column:dModifiedDate; type:datetime; NOT NULL"`                //修改时间
	GiState        int       `json:"iState" gorm:"column:iState; type:int; NOT NULL; DEFAULT '1'"`                      //状态
	GiIndex        int       `json:"iIndex" gorm:"column:iIndex; type:int; NOT NULL; DEFAULT '1'"`                      //序号
	GiVersion      int       `json:"iVersion" gorm:"column:iVersion; type:int; NOT NULL; DEFAULT '1'"`                  //版本号
}

/**
 * 创建结构实体
 * @return
 */
func (LoginLogBase) New() dbinfo.Entity {
	return &LoginLogBase{}
}

/**
 * 取基础实体,用于在子类(嵌套结构体)时同样获得基类
 */
func (LoginLogBase) BaseEntity() dbinfo.Entity {
	return &LoginLogBase{}
}

/**
 * 结构体映射表名,处理结构体名称与表名不一致的情况
 * @return
 */
func (LoginLogBase) TableName() string {
	if tableInfo[0] != "" {
		return tableInfo[0]
	}

	tableInfo[0] = gorm.GetDbName(tableInfo[1]) + tableInfo[2]

	return tableInfo[0]
}

/**
 * 结构体映射库名,去除'Dev_'等前缀
 * @return
 */
func (LoginLogBase) OwnerName() string {
	return tableInfo[1]
}

/**
 * 结构体映射表名,无库名
 * @return
 */
func (LoginLogBase) OwnerTable() string {
	return tableInfo[2]
}

/**
 * 结构体映射表的字段名串
 * @return
 */
func (LoginLogBase) BaseColumnNames() string {
	return "iId,sName,iType,sUserId,sUser,sIp,sMACAddress,dLogDate,dLastCall,sMemo,sCreator,dCreateDate,sModifieder,dModifiedDate,iState,iIndex,iVersion"
}

/**
 * 取数据结构信息
 * @param name 字段名
 * @return
 */
func (LoginLogBase) GetDataInfo(name string) *dbinfo.DataInfo {
	switch name {
	case "iId":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginLog", GsName: "iId", GsComment: "记录编号", GbDbField: true, GsDbFileType: "bigint", GsDefaultData: "", GoDefaultData: 0, GbExtra: true, Gbkey: true, GbNull: false, GiMaxLength: 20, GbDecimal: false, GiIntegralLength: 20, GiDecimalLength: 0, GiIndex: 0}
	case "sName":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginLog", GsName: "sName", GsComment: "日志类型(冗余)", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "", GoDefaultData: "", GbExtra: false, Gbkey: false, GbNull: true, GiMaxLength: 32, GbDecimal: false, GiIntegralLength: 32, GiDecimalLength: 0, GiIndex: 1}
	case "iType":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginLog", GsName: "iType", GsComment: "日志类型", GbDbField: true, GsDbFileType: "int", GsDefaultData: "1", GoDefaultData: 1, GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 11, GbDecimal: false, GiIntegralLength: 11, GiDecimalLength: 0, GiIndex: 2}
	case "sUserId":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginLog", GsName: "sUserId", GsComment: "用户编号", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "", GoDefaultData: "", GbExtra: false, Gbkey: false, GbNull: true, GiMaxLength: 8, GbDecimal: false, GiIntegralLength: 8, GiDecimalLength: 0, GiIndex: 3}
	case "sUser":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginLog", GsName: "sUser", GsComment: "用户名(冗余)", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "", GoDefaultData: "", GbExtra: false, Gbkey: false, GbNull: true, GiMaxLength: 32, GbDecimal: false, GiIntegralLength: 32, GiDecimalLength: 0, GiIndex: 4}
	case "sIp":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginLog", GsName: "sIp", GsComment: "IP地址", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "", GoDefaultData: "", GbExtra: false, Gbkey: false, GbNull: true, GiMaxLength: 32, GbDecimal: false, GiIntegralLength: 32, GiDecimalLength: 0, GiIndex: 5}
	case "sMACAddress":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginLog", GsName: "sMACAddress", GsComment: "MAC地址", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "", GoDefaultData: "", GbExtra: false, Gbkey: false, GbNull: true, GiMaxLength: 64, GbDecimal: false, GiIntegralLength: 64, GiDecimalLength: 0, GiIndex: 6}
	case "dLogDate":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginLog", GsName: "dLogDate", GsComment: "发生时间", GbDbField: true, GsDbFileType: "datetime", GsDefaultData: "", GoDefaultData: time.Now().Format("2006-01-02 15:04:05"), GbExtra: false, Gbkey: false, GbNull: true, GiMaxLength: 23, GbDecimal: false, GiIntegralLength: 23, GiDecimalLength: 0, GiIndex: 7}
	case "dLastCall":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginLog", GsName: "dLastCall", GsComment: "最后访问时间", GbDbField: true, GsDbFileType: "datetime", GsDefaultData: "", GoDefaultData: time.Now().Format("2006-01-02 15:04:05"), GbExtra: false, Gbkey: false, GbNull: true, GiMaxLength: 23, GbDecimal: false, GiIntegralLength: 23, GiDecimalLength: 0, GiIndex: 8}
	case "sMemo":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginLog", GsName: "sMemo", GsComment: "备注", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "", GoDefaultData: "", GbExtra: false, Gbkey: false, GbNull: true, GiMaxLength: 200, GbDecimal: false, GiIntegralLength: 200, GiDecimalLength: 0, GiIndex: 9}
	case "sCreator":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginLog", GsName: "sCreator", GsComment: "创建者", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "00000000", GoDefaultData: "00000000", GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 8, GbDecimal: false, GiIntegralLength: 8, GiDecimalLength: 0, GiIndex: 10}
	case "dCreateDate":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginLog", GsName: "dCreateDate", GsComment: "创建时间", GbDbField: true, GsDbFileType: "datetime", GsDefaultData: "", GoDefaultData: time.Now().Format("2006-01-02 15:04:05"), GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 23, GbDecimal: false, GiIntegralLength: 23, GiDecimalLength: 0, GiIndex: 11}
	case "sModifieder":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginLog", GsName: "sModifieder", GsComment: "修改人", GbDbField: true, GsDbFileType: "varchar", GsDefaultData: "00000000", GoDefaultData: "00000000", GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 8, GbDecimal: false, GiIntegralLength: 8, GiDecimalLength: 0, GiIndex: 12}
	case "dModifiedDate":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginLog", GsName: "dModifiedDate", GsComment: "修改时间", GbDbField: true, GsDbFileType: "datetime", GsDefaultData: "", GoDefaultData: time.Now().Format("2006-01-02 15:04:05"), GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 23, GbDecimal: false, GiIntegralLength: 23, GiDecimalLength: 0, GiIndex: 13}
	case "iState":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginLog", GsName: "iState", GsComment: "状态", GbDbField: true, GsDbFileType: "int", GsDefaultData: "1", GoDefaultData: 1, GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 11, GbDecimal: false, GiIntegralLength: 11, GiDecimalLength: 0, GiIndex: 14}
	case "iIndex":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginLog", GsName: "iIndex", GsComment: "序号", GbDbField: true, GsDbFileType: "int", GsDefaultData: "1", GoDefaultData: 1, GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 11, GbDecimal: false, GiIntegralLength: 11, GiDecimalLength: 0, GiIndex: 15}
	case "iVersion":
		return &dbinfo.DataInfo{GsDbName: "BaseSystem", GsTableName: "LoginLog", GsName: "iVersion", GsComment: "版本号", GbDbField: true, GsDbFileType: "int", GsDefaultData: "1", GoDefaultData: 1, GbExtra: false, Gbkey: false, GbNull: false, GiMaxLength: 11, GbDecimal: false, GiIntegralLength: 11, GiDecimalLength: 0, GiIndex: 16}
	default:
		return nil
	}
}
