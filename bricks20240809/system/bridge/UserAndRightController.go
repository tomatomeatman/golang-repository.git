package bridge

import (
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/app"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/urlutil"

	"gitee.com/tomatomeatman/golang-repository/bricks/utils/ginutil"
)

// @Controller 桥接服务-用户权限桥接操作接口
type UserAndRightController struct {
}

/**
 * 初始化
 */
func init() {
	if (app.AppUtil{}.IsNotCloudSystem()) { //禁用桥接
		return
	}

	//-- 接口注册 --//
	go ginutil.RegisterController("/user/and/right/clear/cache", ginutil.POST, UserAndRightController{}.ClearCache)
	go ginutil.RegisterController("/user/and/right/find/english", ginutil.POST, UserAndRightController{}.FindEnglishByUserId)
	go ginutil.RegisterController("/user/and/right/check", ginutil.POST, UserAndRightController{}.CheckUrlRight)
	go ginutil.RegisterController("/user/and/right/find/rightid", ginutil.POST, UserAndRightController{}.FindRightId)
	go ginutil.RegisterController("/user/and/right/lasttime", ginutil.POST, UserAndRightController{}.LastTime)
}

// #region @Api {title=清理指定用户的缓存}
// @param {name=sLoginId dataType=string paramType=query explain=用户编号 required=false}
// @return {type=json explain=返回对象}
// @RequestMapping {name=In type=POST value=/user/and/right/clear/cache}
// #endregion
func (control UserAndRightController) ClearCache(ctx ginutil.Context) interface{} {
	sLoginId := urlutil.UrlUtil{}.GetParam(ctx, "sLoginId", "").(string)
	return UserAndRightService{}.ClearCache(ctx, sLoginId)
}

// #region @Api {title=根据用户取权限标识字符串,一个权限标识代表了多个可访问的url路径 explain=不用判断请求用户是谁,因为其它人获取信息后没用,权限校验会在每次进行具体操作时进行再次判断}
// @param {name=sUserId dataType=string paramType=query explain=用户编号 required=true}
// @return {type=json explain=返回对象}
// @RequestMapping {name=In type=POST value=/user/and/right/find/english}
// #endregion
func (control UserAndRightController) FindEnglishByUserId(ctx ginutil.Context) interface{} {
	sUserId := urlutil.UrlUtil{}.GetParam(ctx, "sUserId", "").(string)
	return UserAndRightService{}.FindEnglishByUserId(ctx, sUserId)
}

// #region @Api {title=验证指定用户是否有访问指定url的权限 explain=给内部拦截器用,直接返回Boolean}
// @param {name=sUserId dataType=string paramType=query explain=用户编号 required=true}
// @param {name=url dataType=string paramType=query explain=检验地址 required=true}
// @return {type=bool explain=返回对象}
// @RequestMapping {name=In type=POST value=/user/and/right/check}
// #endregion
func (control UserAndRightController) CheckUrlRight(ctx ginutil.Context) interface{} {
	sUserId := urlutil.UrlUtil{}.GetParam(ctx, "sUserId", "").(string)
	url := urlutil.UrlUtil{}.GetParam(ctx, "url", "").(string)
	return UserAndRightService{}.CheckUrlRight(ctx, sUserId, url)
}

// #region @Api {title=根据用户查询用户所拥有的权限编号集合}
// @param {name=sUserId dataType=string paramType=query explain=用户编号 required=true}
// @return {type=bool explain=返回对象}
// @RequestMapping {name=In type=POST value=/user/and/right/find/rightid}
// #endregion
func (control UserAndRightController) FindRightId(ctx ginutil.Context) interface{} {
	sUserId := urlutil.UrlUtil{}.GetParam(ctx, "sUserId", "").(string)
	return UserAndRightService{}.FindRightId(ctx, sUserId)
}

// #region @Api {title=根据用户查询用户所拥有的权限的最后更新时间}
// @param {name=sUserId dataType=string paramType=query explain=用户编号 required=true}
// @return {type=bool explain=返回对象}
// @RequestMapping {name=In type=POST value=/user/and/right/lasttime}
// #endregion
func (control UserAndRightController) LastTime(ctx ginutil.Context) interface{} {
	sUserId := urlutil.UrlUtil{}.GetParam(ctx, "sUserId", "").(string)
	return UserAndRightService{}.LastTime(ctx, sUserId)
}
