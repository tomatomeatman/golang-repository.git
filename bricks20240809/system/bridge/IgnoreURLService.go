package bridge

import (
	"gitee.com/tomatomeatman/golang-repository/bricks/model/emity"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/app"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/ginutil"
)

/**
 * 拦截器忽略路径接口桥接业务操作结构体
 */
type IgnoreURLService struct{}

var (
	ignoreURLServerName = "" //桥接到登录服务的服务名,默认GBaseSystemServer
	//ignoreURLServerKey  = "" //桥接到登录服务的服务密钥
)

// 初始化
func init() {
	if (app.AppUtil{}.IsNotCloudSystem()) { //禁用桥接
		return
	}

	ignoreURLServerName = app.AppUtil{}.ReadConfigKey("CloudServer", "BaseServerName", "GBaseSystemServer").(string)
	//ignoreURLServerKey = app.AppUtil{}.ReadConfigKey("CloudServer", "BaseServerKey", "").(string)
}

/**
 * 根据记录编号取对象
 * @param sId 记录编号
 * @return
 */
func (service IgnoreURLService) FindById(ctx ginutil.Context, sId string) *emity.MsgEmity {
	m := map[string]interface{}{"sId": sId}

	me := BridgeDao{}.Post(ctx, ignoreURLServerName, "/ignore/url/find/id", m, &emity.MsgEmity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*emity.MsgEmity)
}

/**
 * 验证指定url是否在可忽略的访问路径中(给内部拦截器用,直接返回Boolean)
 * @param sUrl 待检验的url
 * @param isMustLogin 是否必须登录
 * @return
 */
func (service IgnoreURLService) CheckIgnoreUrl(ctx ginutil.Context, sUrl string, iMustLogin int, sUserType string) bool {
	m := map[string]interface{}{
		"sUrl":       sUrl,
		"iMustLogin": iMustLogin,
		"sUserType":  sUserType,
	}

	me := BridgeDao{}.Post(ctx, ignoreURLServerName, "/ignore/url/check", m, &emity.MsgEmity{})
	if !me.Gsuccess { //Post请求未成功
		return false
	}

	me = me.Gdata.(*emity.MsgEmity)
	if !me.Gsuccess { //"在指定项目中都没有找到可以免拦截的约定"
		return false
	}

	return me.Gdata.(bool)
}

/**
 * 清理缓存
 * @return
 */
func (service IgnoreURLService) ClearCache(ctx ginutil.Context) *emity.MsgEmity {
	me := BridgeDao{}.Post(ctx, ignoreURLServerName, "/ignore/url/clear/cache", nil, "")
	if !me.Gsuccess {
		return me
	}

	return me
}
