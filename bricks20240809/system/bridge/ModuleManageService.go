package bridge

import (
	"gitee.com/tomatomeatman/golang-repository/bricks/model/emity"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/app"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/urlutil"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/ginutil"
)

/**
 * 模块管理ModuleManage表基本业务操作结构体
 */
type ModuleManageService struct {
}

var (
	moduleManageServiceName = "" //桥接到登录服务的服务名,默认GBaseSystemServer
	//moduleManageServiceName  = "" //桥接到登录服务的服务密钥
)

// 初始化
func init() {
	if (app.AppUtil{}.IsNotCloudSystem()) { //禁用桥接
		return
	}

	moduleManageServiceName = app.AppUtil{}.ReadConfigKey("CloudServer", "BaseServerName", "GBaseSystemServer").(string)
	//moduleManageServiceName = app.AppUtil{}.ReadConfigKey("CloudServer", "BaseServerKey", "").(string)
}

/**
 * 读取树形结构数据
 * ctx Http请求对象
 */
func (service ModuleManageService) FindByTree(ctx ginutil.Context) *emity.MsgEmity {
	sGroupColumn := urlutil.UrlUtil{}.GetParam(ctx, "sGroupColumn", "").(string)
	sGroupName := urlutil.UrlUtil{}.GetParam(ctx, "sGroupName", "").(string)
	m := map[string]interface{}{
		"sGroupColumn": sGroupColumn,
		"sGroupName":   sGroupName,
	}

	me := BridgeDao{}.Post(ctx, moduleManageServiceName, "/module/manage/find/tree", m, &emity.MsgEmity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*emity.MsgEmity)
}
