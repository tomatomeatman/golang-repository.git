package uuidutil

import (
	"strings"

	uuid "github.com/satori/go.uuid"
)

type UUIDUtil struct{}

// 获取UUID大写
func (ul UUIDUtil) Get() string {
	result := strings.Replace(uuid.NewV4().String(), "-", "", -1)
	return strings.ToUpper(result)
}

// 获取UUID小写
func (ul UUIDUtil) GetLowerCase() string {
	result := strings.Replace(uuid.NewV4().String(), "-", "", -1)
	return result
}
