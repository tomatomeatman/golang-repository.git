package floatutil

import (
	"fmt"
	"strconv"
	"unsafe"
)

type FloatUtil struct{}

// 对象(字符串)转浮点数
func (fu FloatUtil) ToStr(obj float64) string {
	return fmt.Sprintf("%f", obj)
}

// 对象(字符串)转64整型
func (fu FloatUtil) ToFloat(obj interface{}, bitSize int, iDefault float64) float64 {
	var str string
	switch obj.(type) {
	case []uint8:
		str = fu.Byte2Str(obj.([]uint8))
	default:
		str = fmt.Sprintf("%v", obj)
	}

	if str == "" { //字符串不能判断nil
		return iDefault
	}

	result, err := strconv.ParseFloat(str, bitSize)
	if err != nil {
		return iDefault
	}

	return result
}

// Byte转Str
func (fu FloatUtil) Byte2Str(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}
