package app

import (
	"errors"
	"reflect"

	"gitee.com/tomatomeatman/golang-repository/bricks/model/dbinfo"
)

// web控制层接口定义,用于规范控制层结构体
type Controller interface {
	GetModuleEntity() (dbinfo.Entity, error) //对应模块数据实体
	GetModuleService() (interface{}, error)  //对应模块业务实体
	RegisterUrl()                            //接口注册
	Enable() string                          //控制操作,首位9不参与用7个数代表开关,0为不限制,1为限制
	CheckRepeatCombination() []string        //检查待新增内容是否存在重复数据(多字段组合重复即重复)集合
	CheckRepeatAlone() map[string]int        //检查待新增内容是否存在重复数据(单独字段重复即重复)集合,注意:int必须是1、10、100、1000
	IdValuePrefix() string                   //记录编号值前缀,此属性用于给id字段添加前缀,以便于在分布式系统中进行数据合并,注意:前缀只有1个字符
}

// we业务层接口定义,用于规范控制层结构体
type Service interface {
}

// we数据层接口定义,用于规范控制层结构体
type Dao interface {
}

type ControllerBaseFunc struct{}

// 接口注册
func (control ControllerBaseFunc) RegisterUrl() {
	//子类实现
}

// -- 控制操作,首位9不参与用7个数代表开关,0为不限制,1为限制 --//
// -- 7个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆、物理删除 --//
// -- 其中前5个为'是否仅创建者可操作'后续为其它控制开关 --//
func (control ControllerBaseFunc) Enable() string {
	return "90000001"
}

// 检查待新增内容是否存在重复数据(多字段组合重复即重复)集合
func (control ControllerBaseFunc) CheckRepeatCombination() []string {
	return []string{}
}

// -- 检查待新增内容是否存在重复数据(单独字段重复即重复)集合,注意:int必须是1、10、100、1000 --//
func (control ControllerBaseFunc) CheckRepeatAlone() map[string]int {
	return map[string]int{}
}

func (control ControllerBaseFunc) IdValuePrefix() string {
	return ""
}

func (control *ControllerBaseFunc) GetModuleEntity() (dbinfo.Entity, error) {
	moduleValue := reflect.ValueOf(control).Elem().FieldByName("ModuleEntity")

	if moduleValue.Kind() != reflect.Ptr || moduleValue.Type().Elem().Kind() != reflect.Struct {
		return nil, errors.New("未设置模块数据实体")
	}

	// 获取 Entity 的反射值
	entityElem := moduleValue.Elem()
	//entityValue := entityElem.Interface().(dbinfo.Entity).New()
	entityValue := entityElem.Interface().(dbinfo.Entity)

	return entityValue, nil
}

func (control *ControllerBaseFunc) GetModuleService() (interface{}, error) {
	moduleValue := reflect.ValueOf(control).Elem().FieldByName("ModuleService")

	if moduleValue.Kind() != reflect.Ptr || moduleValue.Type().Elem().Kind() != reflect.Struct {
		return nil, errors.New("未设置模块业务实体")
	}

	// 获取 Entity 的反射值
	serviceElem := moduleValue.Elem()
	service := serviceElem.Interface()

	return service, nil
}
