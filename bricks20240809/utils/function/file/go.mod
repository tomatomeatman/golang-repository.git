module gitee.com/tomatomeatman/golang-repository/bricks/utils/function/file

go 1.21.6

require (
	gitee.com/tomatomeatman/golang-repository/bricks/model/emity v0.0.0-20240717053259-28a2915b1dd3
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
)
