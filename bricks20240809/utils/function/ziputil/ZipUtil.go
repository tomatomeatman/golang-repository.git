package file

import (
	"archive/zip"
	"bytes"
	"io"
	"os"
	"path"
	"path/filepath"
	"strings"

	"gitee.com/tomatomeatman/golang-repository/bricks/model/emity"
	Log "github.com/cihub/seelog"
)

type ZipUtil struct {
}

/**
 * 打包成zip文件
 * src 源文件夹
 * target 目标文件名
 */
func (zu ZipUtil) Create(source string, target string) *emity.MsgEmity {
	source = strings.TrimSpace(source)

	if _, err := os.Stat(source); err != nil {
		if os.IsNotExist(err) {
			return emity.MsgEmity{}.Err(1001, "源文件不存在")
		}

		return emity.MsgEmity{}.Err(1002, "访问源文件异常:", err)
	}

	target = strings.TrimSpace(target)

	os.MkdirAll(path.Dir(target), os.ModePerm) //创建多级目录

	os.RemoveAll(target) // 预防：旧文件无法覆盖

	targetFile, err := os.Create(target) // 创建目标 zip 文件
	if err != nil {
		Log.Error("创建目标zip文件失败:", err)
		return emity.MsgEmity{}.Err(1003, "创建目标zip文件失败")
	}

	defer targetFile.Close()

	// 创建 zip.Writer
	zipWriter := zip.NewWriter(targetFile)
	defer zipWriter.Close()

	var me *emity.MsgEmity
	// 获取需要打包的所有文件
	filepath.Walk(source, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			Log.Error("打包zip文件失败:", err)
			me = emity.MsgEmity{}.Err(1004, "打包zip文件失败")
			return err
		}

		// 如果是源路径，提前进行下一个遍历
		if path == source {
			return nil
		}

		// 创建新的 zip 文件头信息
		header, err := zip.FileInfoHeader(info)
		if err != nil {
			Log.Error("创建新的zip文件头信息失败:", err)
			me = emity.MsgEmity{}.Err(1005, "创建新的zip文件头信息失败")
			return err
		}

		// 设置文件头信息中的 Name 字段
		header.Name, err = filepath.Rel(source, path)
		if err != nil {
			return err
		}

		// zip.Store | zip.Deflate 预定义压缩算法。
		// archive/zip包中预定义的有两种压缩方式。一个是仅把文件写入到zip中。不做压缩。一种是压缩文件然后写入到zip中。默认的Store模式。就是只保存不压缩的模式。
		// Store   unit16 = 0  //仅存储文件
		// Deflate unit16 = 8  //压缩文件

		// 判断当前路径是否为一个文件夹
		if info.IsDir() {
			// 如果是文件夹，创建文件夹信息
			header.Name += "/"
			header.Method = zip.Store //文件夹不压缩
		} else {
			// 如果是文件，打开文件准备读取内容,并使用压缩
			header.Method = zip.Deflate
		}

		header.Name = strings.Replace(header.Name, "\\", "/", -1)

		// 将新的文件头信息写入到 zip.Writer 中
		writer, err := zipWriter.CreateHeader(header)
		if err != nil {
			Log.Error("新的文件头信息写入失败:", err)
			me = emity.MsgEmity{}.Err(1006, "新的文件头信息写入失败")
			return err
		}

		// 如果当前路径是一个文件，读取文件内容并写入 zip.Writer 中
		if !info.IsDir() {
			file, err := os.Open(path)
			if err != nil {
				Log.Error("指定文件不存在:", err)
				me = emity.MsgEmity{}.Err(1007, "指定文件不存在")
				return err
			}
			defer file.Close()

			_, err = io.Copy(writer, file)
			if err != nil {
				Log.Error("复制文件失败:", err)
				me = emity.MsgEmity{}.Err(1008, "复制文件失败")
				return err
			}
		}

		return nil
	})

	if me != nil && !me.Gsuccess {
		return me
	}

	return emity.MsgEmity{}.Success(1999, "打包结束")
}

/**
 * 读取指定(序号)文件
 * source 源文件
 * iIndex 序号
 * *emity.MsgEmity (content []byte, fileName string)
 */
func (zu ZipUtil) ReadByIndex(source string, iIndex int) *emity.MsgEmity {
	if iIndex < 0 {
		return emity.MsgEmity{}.Err(1001, "下标越界,最小下标:0")
	}

	source = strings.TrimSpace(source)

	if _, err := os.Stat(source); err != nil {
		if os.IsNotExist(err) {
			return emity.MsgEmity{}.Err(1002, "源文件不存在")
		}

		return emity.MsgEmity{}.Err(1003, "访问源文件异常:", err)
	}

	zipFile, err := zip.OpenReader(source) // 打开ZIP文件
	if err != nil {
		return emity.MsgEmity{}.Err(1004, "打开zip文件异常:", err)
	}
	defer zipFile.Close()

	if iIndex >= len(zipFile.File) {
		return emity.MsgEmity{}.Err(1005, "下标越界,最大下标:", (len(zipFile.File) - 1))
	}

	file := zipFile.File[iIndex]
	f, err := file.Open() // 打开ZIP文件内的成员
	if err != nil {
		return emity.MsgEmity{}.Err(1006, "读取文件异常:", err)
	}
	defer f.Close()

	// 读取ZIP内文件的内容
	buf := new(bytes.Buffer)
	if _, err := io.Copy(buf, f); err != nil {
		return emity.MsgEmity{}.Err(1007, "读取文件内容异常:", err)
	}

	return emity.MsgEmity{}.Success(buf.Bytes(), file.Name)
}

/**
 * 读取压缩文件里的文件数
 * source 源文件
 * iIndex 序号
 * *emity.MsgEmity (content []byte, fileName string)
 */
func (zu ZipUtil) LoadCount(source string) *emity.MsgEmity {
	source = strings.TrimSpace(source)

	if _, err := os.Stat(source); err != nil {
		if os.IsNotExist(err) {
			return emity.MsgEmity{}.Err(1001, "源文件不存在")
		}

		return emity.MsgEmity{}.Err(1002, "访问源文件异常:", err)
	}

	zipFile, err := zip.OpenReader(source) // 打开ZIP文件
	if err != nil {
		return emity.MsgEmity{}.Err(1003, "打开zip文件异常:", err)
	}
	defer zipFile.Close()

	iCount := len(zipFile.File)

	return emity.MsgEmity{}.Success(iCount, "数量读取成功")
}

/**
 * 读取指定(名称)文件
 * source 源文件
 * sName 名称
 * *emity.MsgEmity (content []byte, fileName string)
 */
func (zu ZipUtil) ReadByName(source, sName string) *emity.MsgEmity {
	source = strings.TrimSpace(source)

	if _, err := os.Stat(source); err != nil {
		if os.IsNotExist(err) {
			return emity.MsgEmity{}.Err(1001, "源文件不存在")
		}

		return emity.MsgEmity{}.Err(1002, "访问源文件异常:", err)
	}

	zipFile, err := zip.OpenReader(source) // 打开ZIP文件
	if err != nil {
		return emity.MsgEmity{}.Err(1003, "打开zip文件异常:", err)
	}
	defer zipFile.Close()

	sName = strings.TrimSpace(sName)

	// 遍历ZIP文件内的各个文件
	var content []byte
	var fileName string
	for _, file := range zipFile.File {
		if file.Name == sName {
			continue
		}

		f, err := file.Open() // 打开ZIP文件内的成员
		if err != nil {
			return emity.MsgEmity{}.Err(1004, "读取文件异常:", err)
		}
		defer f.Close()

		// 读取ZIP内文件的内容
		buf := new(bytes.Buffer)
		if _, err := io.Copy(buf, f); err != nil {
			return emity.MsgEmity{}.Err(1005, "读取文件内容异常:", err)
		}

		fileName = file.Name
		content = buf.Bytes()

		break
	}

	if fileName == "" {
		return emity.MsgEmity{}.Err(1006, "未找到文件:", sName)
	}

	return emity.MsgEmity{}.Success(content, fileName)
}

/**
 * 解压zip文件
 * src 源文件夹
 * target 目标文件夹
 * *emity.MsgEmity (int, msg)
 */
func (zu ZipUtil) UnZip(source string, target string) *emity.MsgEmity {
	if _, err := os.Stat(source); err != nil {
		if os.IsNotExist(err) {
			return emity.MsgEmity{}.Err(1001, "源文件不存在")
		}

		return emity.MsgEmity{}.Err(1002, "访问源文件异常:", err)
	}

	// 打开Zip文件
	r, err := zip.OpenReader(source)
	if err != nil {
		return emity.MsgEmity{}.Err(1003, "打开zip文件异常:", err)
	}
	defer r.Close()

	//target = strings.TrimSpace(target)

	// 遍历Zip文件中的所有文件并解压缩
	iCount := 0
	for _, f := range r.File {
		rc, err := f.Open() // 打开文件
		if err != nil {
			return emity.MsgEmity{}.Err(1004, "读取文件异常:", err)
		}
		defer rc.Close()

		dst, err := os.Create(f.Name) // 创建目标文件
		if err != nil {
			return emity.MsgEmity{}.Err(1005, "创建目标文件异常:", err)
		}
		defer dst.Close()

		_, err = io.Copy(dst, rc) // 将文件内容复制到目标文件中
		if err != nil {
			return emity.MsgEmity{}.Err(1006, "取源文件内容到目标文件异常:", err)
		}

		iCount++
	}

	if iCount < len(r.File) {
		return emity.MsgEmity{}.Err(1007, "解压结束:", iCount, "/", len(r.File))
	}

	return emity.MsgEmity{}.Success(9999, "解压完毕")
}
