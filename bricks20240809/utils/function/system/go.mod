module gitee.com/tomatomeatman/golang-repository/bricks/utils/function/system

go 1.21.6

require (
	gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data/mdutil v0.0.0-20240717053259-28a2915b1dd3
	github.com/shirou/gopsutil v3.21.11+incompatible
)

require (
	github.com/go-ole/go-ole v1.3.0 // indirect
	github.com/stretchr/testify v1.9.0 // indirect
	github.com/tklauser/go-sysconf v0.3.14 // indirect
	github.com/tklauser/numcpus v0.8.0 // indirect
	github.com/yusufpapurcu/wmi v1.2.4 // indirect
	golang.org/x/sys v0.22.0 // indirect
)
