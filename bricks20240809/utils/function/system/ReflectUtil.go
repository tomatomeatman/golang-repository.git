package system

import (
	"reflect"
	"strings"
)

type ReflectUtils struct{}

// 取结构体各属性的tag信息
func (ru ReflectUtils) GetTagInfo(entry interface{}) map[string]reflect.StructTag {
	result := make(map[string]reflect.StructTag)

	s := reflect.TypeOf(entry).Elem() //通过反射获取type定义
	for i := 0; i < s.NumField(); i++ {
		if s.Field(i).Anonymous {
			temp := getAnonyTagInfo(s.Field(i))
			if nil == temp {
				continue
			}

			for key := range temp {
				result[key] = temp[key]
			}

			continue
		}

		tag := s.Field(i).Tag
		result[s.Field(i).Name] = tag
	}

	return result
}

// 取结构体匿名属性的tag信息
func getAnonyTagInfo(sf reflect.StructField) map[string]reflect.StructTag {
	if !sf.Anonymous {
		return nil
	}

	result := make(map[string]reflect.StructTag)
	t := sf.Type
	for k := 0; k < t.NumField(); k++ {
		if t.Field(k).Anonymous {
			temp := getAnonyTagInfo(t.Field(k))
			if nil == temp {
				continue
			}

			for key := range temp {
				result[key] = temp[key]
			}

			continue
		}

		tag := t.Field(k).Tag
		result[t.Field(k).Name] = tag
	}

	return result
}

// 取结构体指定属性的tag信息
func (ru ReflectUtils) GetTagInfoByName(entry interface{}, name string) reflect.StructTag {
	if name == "" {
		return ""
	}

	elem := reflect.TypeOf(entry).Elem() //通过反射获取type定义

	if sf, ok := elem.FieldByName(name); ok {
		return sf.Tag
	}

	for i := 0; i < elem.NumField(); i++ {
		if !elem.Field(i).Anonymous {
			continue
		}

		temp := ru.getAnonyTagInfoByName(elem.Field(i), name)
		if temp != "" {
			return temp
		}
	}

	return ""
}

// 取结构体匿名属性中指定名称的tag信息
func (ru ReflectUtils) getAnonyTagInfoByName(sf reflect.StructField, name string) reflect.StructTag {
	if !sf.Anonymous {
		return ""
	}

	t := sf.Type
	for k := 0; k < t.NumField(); k++ {
		if name == t.Field(k).Name {
			return t.Field(k).Tag
		}
	}

	for k := 0; k < t.NumField(); k++ {
		if !t.Field(k).Anonymous {
			continue
		}

		if t.Field(k).Anonymous {
			temp := ru.getAnonyTagInfoByName(t.Field(k), name)
			if temp == "" {
				continue
			}

			return temp
		}
	}

	return ""
}

// 取实体对应父类实体(对应数据库)
func (ru ReflectUtils) GetBaseEntity(entity interface{}) interface{} {
	var rte reflect.Type
	var rve reflect.Value

	typeOf := reflect.TypeOf(entity)  //通过反射获取type定义
	if typeOf.Kind() == reflect.Ptr { //是否指针类型
		rve = reflect.ValueOf(entity).Elem()
		rte = rve.Type() // 取得struct变量的指针
	} else if typeOf.String() == "reflect.Value" {
		if entity.(reflect.Value).Kind() == reflect.Ptr {
			rve = entity.(reflect.Value).Elem()
		} else if entity.(reflect.Value).Kind() == reflect.Struct {
			rve = entity.(reflect.Value)
		} else {
			rve = entity.(reflect.Value)
		}

		rte = rve.Type()
	} else {
		rve = reflect.ValueOf(entity)
		rte = rve.Type()
	}

	for k := 0; k < rte.NumField(); k++ {
		field := rte.Field(k)
		if field.Anonymous && strings.HasSuffix(field.Name, "Base") { //必须是匿名及'Base'结尾
			return rve.Field(k) //找到
		}
	}

	return entity //没找到
}

//-----------------------------------------------------------//

/**
 * 利用反射设置属性值
 * @param object 被修改属性的对象
 * @param name 属性名
 * @param val 值 不确定类型
 * @return
 */
func (ru ReflectUtils) SetFieldValue(object interface{}, name string, val interface{}) {
	var rve reflect.Value

	typeOf := reflect.TypeOf(object)  //通过反射获取type定义
	if typeOf.Kind() == reflect.Ptr { //是否指针类型
		rve = reflect.ValueOf(object).Elem() // 取得struct变量的指针
	} else if typeOf.String() == "reflect.Value" {
		if object.(reflect.Value).Kind() == reflect.Ptr {
			rve = object.(reflect.Value).Elem()
		} else if object.(reflect.Value).Kind() == reflect.Struct {
			rve = object.(reflect.Value)
		} else {
			rve = object.(reflect.Value)
		}
	} else {
		rve = reflect.ValueOf(object)
	}

	field := rve.FieldByName(name)
	if field.IsValid() {
		field.Set(reflect.ValueOf(val))
		return
	}

	c := name[0]
	if (!strings.HasPrefix(name, "G")) && ('a' <= c) && (c <= 'z') {
		field := rve.FieldByName("G" + name)
		if field.IsValid() {
			field.Set(reflect.ValueOf(val))
			return
		}
	}
}

/**
 * 利用反射设置属性值
 * @param object 被修改属性的对象
 * @param name 属性名
 * @param val 值 字符串
 * @return
 */
func (ru ReflectUtils) SetFieldString(object interface{}, name string, val string) {
	var rve reflect.Value

	typeOf := reflect.TypeOf(object)  //通过反射获取type定义
	if typeOf.Kind() == reflect.Ptr { //是否指针类型
		rve = reflect.ValueOf(object).Elem() // 取得struct变量的指针
	} else if typeOf.String() == "reflect.Value" {
		if object.(reflect.Value).Kind() == reflect.Ptr {
			rve = object.(reflect.Value).Elem()
		} else if object.(reflect.Value).Kind() == reflect.Struct {
			rve = object.(reflect.Value)
		} else {
			rve = object.(reflect.Value)
		}
	} else {
		rve = reflect.ValueOf(object)
	}

	field := rve.FieldByName(name)
	if field.IsValid() {
		field.Set(reflect.ValueOf(val))
		return
	}

	c := name[0]
	if (!strings.HasPrefix(name, "G")) && ('a' <= c && c <= 'z') {
		field := rve.FieldByName("G" + name)
		if field.IsValid() {
			field.Set(reflect.ValueOf(val))
			return
		}
	}
}

/**
 * 利用反射设置属性值
 * @param object 被修改属性的对象
 * @param name 属性名
 * @param val 值 字符串
 * @return
 */
func (ru ReflectUtils) SetFieldInt(object interface{}, name string, val int) {
	var rve reflect.Value

	typeOf := reflect.TypeOf(object)  //通过反射获取type定义
	if typeOf.Kind() == reflect.Ptr { //是否指针类型
		rve = reflect.ValueOf(object).Elem() // 取得struct变量的指针
	} else if typeOf.String() == "reflect.Value" {
		if object.(reflect.Value).Kind() == reflect.Ptr {
			rve = object.(reflect.Value).Elem()
		} else if object.(reflect.Value).Kind() == reflect.Struct {
			rve = object.(reflect.Value)
		} else {
			rve = object.(reflect.Value)
		}
	} else {
		rve = reflect.ValueOf(object)
	}

	field := rve.FieldByName(name)
	if field.IsValid() {
		field.Set(reflect.ValueOf(val))
		return
	}

	c := name[0]
	if (!strings.HasPrefix(name, "G")) && ('a' <= c && c <= 'z') {
		field := rve.FieldByName("G" + name)
		if field.IsValid() {
			field.Set(reflect.ValueOf(val))
			return
		}
	}
}

/**
 * 利用反射取实际类型名称
 * @param object 被修改属性的对象
 * @return
 */
func (ru ReflectUtils) GetTypeName(object interface{}) string {
	var rve reflect.Value

	typeOf := reflect.TypeOf(object)  //通过反射获取type定义
	if typeOf.Kind() == reflect.Ptr { //是否指针类型
		rve = reflect.ValueOf(object).Elem() // 取得struct变量的指针
	} else if typeOf.String() == "reflect.Value" {
		if object.(reflect.Value).Kind() == reflect.Ptr {
			rve = object.(reflect.Value).Elem()
		} else if object.(reflect.Value).Kind() == reflect.Struct {
			rve = object.(reflect.Value)
		} else {
			rve = object.(reflect.Value)
		}
	} else {
		rve = reflect.ValueOf(object)
	}

	return rve.Type().Name()
}

/**
 * 利用反射取属性值
 * @param object 被修改属性的对象
 * @param name 属性名
 * @return
 */
func (ru ReflectUtils) GetFieldValue(object interface{}, name string) interface{} {
	var rve reflect.Value

	typeOf := reflect.TypeOf(object)  //通过反射获取type定义
	if typeOf.Kind() == reflect.Ptr { //是否指针类型
		rve = reflect.ValueOf(object).Elem() // 取得struct变量的指针
	} else if typeOf.String() == "reflect.Value" {
		if object.(reflect.Value).Kind() == reflect.Ptr {
			rve = object.(reflect.Value).Elem()
		} else if object.(reflect.Value).Kind() == reflect.Struct {
			rve = object.(reflect.Value)
		} else {
			rve = object.(reflect.Value)
		}
	} else {
		rve = reflect.ValueOf(object)
	}

	field := rve.FieldByName(name)
	if !field.IsValid() {
		return nil
	}

	return field.Interface()
}

/**
 * 利用反射执行函数
 * @param object 被修改属性的对象
 * @param name 属性名
 * @return
 */
func (ru ReflectUtils) DoMethod(object interface{}, name string, paramList ...interface{}) []reflect.Value {
	var rve reflect.Value

	typeOf := reflect.TypeOf(object) //通过反射获取type定义

	if typeOf.Kind() == reflect.Ptr { //是否指针类型
		rve = reflect.ValueOf(object).Elem() // 取得struct变量的指针
	} else if typeOf.String() == "reflect.Value" {
		if object.(reflect.Value).Kind() == reflect.Ptr {
			rve = object.(reflect.Value).Elem()
		} else if object.(reflect.Value).Kind() == reflect.Struct {
			rve = object.(reflect.Value)
		} else {
			rve = object.(reflect.Value)
		}
		// } else if typeOf.Kind() == reflect.Struct {
		// 	rve = typeOf
	} else {
		rve = reflect.ValueOf(object)
	}

	method := rve.MethodByName(name)
	if !method.IsValid() {
		return []reflect.Value{}
	}

	if nil == paramList {
		params := make([]reflect.Value, 0)
		return method.Call(params)
	}

	params := make([]reflect.Value, len(paramList))
	for i := 0; i < len(paramList); i++ {
		params[i] = reflect.ValueOf(paramList[i])
	}

	return method.Call(params)
}

/**
 * 判断函数是否存在
 * object 待检查对象
 * name 函数名
 */
func (ru ReflectUtils) HasMethod(object interface{}, name string) bool {
	var rve reflect.Type
	typeOf := reflect.TypeOf(object)  //通过反射获取type定义
	if typeOf.Kind() == reflect.Ptr { //是否指针类型
		rve = reflect.ValueOf(object).Elem().Type() // 取得struct变量的指针
	} else if typeOf.String() == "reflect.Value" {
		if object.(reflect.Value).Kind() == reflect.Ptr {
			rve = object.(reflect.Value).Elem().Type()
		} else if object.(reflect.Value).Kind() == reflect.Struct {
			rve = object.(reflect.Value).Type()
		} else {
			rve = object.(reflect.Value).Type()
		}
	} else {
		rve = reflect.ValueOf(object).Type()
	}

	_, has := rve.MethodByName(name)

	return has

	// var rve reflect.Value

	// typeOf := reflect.TypeOf(object)  //通过反射获取type定义
	// if typeOf.Kind() == reflect.Ptr { //是否指针类型
	// 	rve = reflect.ValueOf(object).Elem() // 取得struct变量的指针
	// } else if typeOf.String() == "reflect.Value" {
	// 	rve = object.(reflect.Value)
	// } else {
	// 	rve = reflect.ValueOf(object)
	// }

	// method := rve.MethodByName(name)
	// if !method.IsValid() {
	// 	return false
	// }

	// return true
}

/**
 * 取函数
 * object 待检查对象
 * name 函数名
 */
func (ru ReflectUtils) GetMethod(object interface{}, name string) reflect.Value {
	var rve reflect.Value

	typeOf := reflect.TypeOf(object)  //通过反射获取type定义
	if typeOf.Kind() == reflect.Ptr { //是否指针类型
		rve = reflect.ValueOf(object).Elem() // 取得struct变量的指针
	} else if typeOf.String() == "reflect.Value" {
		if object.(reflect.Value).Kind() == reflect.Ptr {
			rve = object.(reflect.Value).Elem()
		} else if object.(reflect.Value).Kind() == reflect.Struct {
			rve = object.(reflect.Value)
		} else {
			rve = object.(reflect.Value)
		}
	} else {
		rve = reflect.ValueOf(object)
	}

	method := rve.MethodByName(name)
	if !method.IsValid() {
		return reflect.Value{}
	}

	return method
}

/**
 * 判断字段是否存在
 * object 待检查对象
 * name 成员名
 * fieldPrefix 字段前缀(可不传)
 */
func (ru ReflectUtils) HasField(object interface{}, name string, fieldPrefix ...string) bool {
	var rve reflect.Type
	typeOf := reflect.TypeOf(object)  //通过反射获取type定义
	if typeOf.Kind() == reflect.Ptr { //是否指针类型
		rve = reflect.ValueOf(object).Elem().Type() // 取得struct变量的指针
	} else if typeOf.String() == "reflect.Value" {
		if object.(reflect.Value).Kind() == reflect.Ptr {
			rve = object.(reflect.Value).Elem().Type()
		} else if object.(reflect.Value).Kind() == reflect.Struct {
			rve = object.(reflect.Value).Type()
		} else {
			rve = object.(reflect.Value).Type()
		}
	} else {
		rve = reflect.ValueOf(object).Type()
	}

	prefix := ""
	for _, key := range fieldPrefix {
		prefix = prefix + key
	}

	_, has := rve.FieldByName(prefix + name)

	return has

	//typeOf := reflect.TypeOf(object) //通过反射获取type定义

	//if typeOf.Kind() == reflect.Ptr { //是否指针类型
	//	typeOf = typeOf.Elem()
	//}

	//_, has := typeOf.FieldByName(name)

	//return has
}

/**
 * 按实体保留map中的数据
 * object 待检查对象
 * data 数据
 * fieldPrefix 字段前缀(可不传)
 */
func (ru ReflectUtils) HoldByEntity(entity interface{}, data map[string]interface{}, fieldPrefix ...string) map[string]interface{} {
	var rve reflect.Type
	typeOf := reflect.TypeOf(entity)  //通过反射获取type定义
	if typeOf.Kind() == reflect.Ptr { //是否指针类型
		rve = reflect.ValueOf(entity).Elem().Type() // 取得struct变量的指针
	} else if typeOf.String() == "reflect.Value" {
		if entity.(reflect.Value).Kind() == reflect.Ptr {
			rve = entity.(reflect.Value).Elem().Type()
		} else if entity.(reflect.Value).Kind() == reflect.Struct {
			rve = entity.(reflect.Value).Type()
		} else {
			rve = entity.(reflect.Value).Type()
		}
	} else if typeOf.Kind() == reflect.Struct {
		rve = typeOf
	} else {
		rve = reflect.ValueOf(entity).Type()
	}

	result := map[string]interface{}{}
	for _, prefix := range fieldPrefix {
		for key, value := range data {
			str := key
			if prefix != "" {
				str = prefix + key
			}

			_, b := rve.FieldByName(str)
			if b {
				result[key] = value
			}
		}
	}

	// prefix := ""
	// for _, key := range fieldPrefix {
	// 	prefix = prefix + key
	// }

	// for key := range data {
	// 	//--先检查没有前缀的情况下是否存在
	// 	_, b := rve.FieldByName(key)
	// 	if !b {
	// 		delete(data, key)
	// 	}

	// 	str := key
	// 	if prefix != "" {
	// 		str = prefix + key
	// 	}

	// 	_, b = rve.FieldByName(str)
	// 	if !b {
	// 		delete(data, key)
	// 	}
	// }

	return result

	//typeOf := reflect.TypeOf(entity) //通过反射获取type定义

	//if typeOf.Kind() == reflect.Ptr { //是否指针类型
	//	typeOf = typeOf.Elem()
	//}

	//prefix := ""
	//for _, key := range fieldPrefix {
	//	prefix = prefix + key
	//}

	//for key := range data {
	//	str := key
	//	if prefix != "" {
	//		str = prefix + key
	//	}

	//	_, b := typeOf.FieldByName(str)
	//	if !b {
	//		delete(data, key)
	//	}
	//}

	//return data
}

/**
 * 按实体剔除map中的数据
 * object 待检查对象
 * data 数据
 * fieldPrefix 字段前缀(可不传)
 */
func (ru ReflectUtils) RemoveByEntity(entity interface{}, data map[string]interface{}, fieldPrefix ...string) map[string]interface{} {
	var rve reflect.Type
	typeOf := reflect.TypeOf(entity)  //通过反射获取type定义
	if typeOf.Kind() == reflect.Ptr { //是否指针类型
		rve = reflect.ValueOf(entity).Elem().Type() // 取得struct变量的指针
	} else if typeOf.String() == "reflect.Value" {
		if entity.(reflect.Value).Kind() == reflect.Ptr {
			rve = entity.(reflect.Value).Elem().Type()
		} else if entity.(reflect.Value).Kind() == reflect.Struct {
			rve = entity.(reflect.Value).Type()
		} else {
			rve = entity.(reflect.Value).Type()
		}
	} else if typeOf.Kind() == reflect.Struct {
		rve = typeOf
	} else {
		rve = reflect.ValueOf(entity).Type()
	}

	prefix := ""
	for _, key := range fieldPrefix {
		prefix = prefix + key
	}

	for key := range data {
		str := key
		if prefix != "" {
			str = prefix + key
		}

		_, b := rve.FieldByName(str)
		if !b {
			delete(data, key)
		}
	}

	return data

	//typeOf := reflect.TypeOf(entity) //通过反射获取type定义

	//if typeOf.Kind() == reflect.Ptr { //是否指针类型
	//	typeOf = typeOf.Elem()
	//}

	//prefix := ""
	//for _, key := range fieldPrefix {
	//	prefix = prefix + key
	//}

	//for key := range data {
	//	str := key
	//	if prefix != "" {
	//		str = prefix + key
	//	}

	//	_, b := typeOf.FieldByName(str)
	//	if b {
	//		delete(data, key)
	//	}
	//}

	//return data
}

/**
 * 按实体保留切片中的数据
 * object 待检查对象
 * data 数据
 * fieldPrefix 字段前缀(可不传)
 */
func (ru ReflectUtils) HoldByEntityToArray(entity interface{}, data []string, fieldPrefix ...string) []string {
	var rve reflect.Type
	typeOf := reflect.TypeOf(entity)  //通过反射获取type定义
	if typeOf.Kind() == reflect.Ptr { //是否指针类型
		rve = reflect.ValueOf(entity).Elem().Type() // 取得struct变量的指针
	} else if typeOf.String() == "reflect.Value" {
		if entity.(reflect.Value).Kind() == reflect.Ptr {
			rve = entity.(reflect.Value).Elem().Type()
		} else if entity.(reflect.Value).Kind() == reflect.Struct {
			rve = entity.(reflect.Value).Type()
		} else {
			rve = entity.(reflect.Value).Type()
		}
	} else if typeOf.Kind() == reflect.Struct {
		rve = typeOf
	} else {
		rve = reflect.ValueOf(entity).Type()
	}

	result := []string{}
	for _, prefix := range fieldPrefix {
		for i := len(data) - 1; i >= 0; i-- {
			key := data[i]

			str := key
			if prefix != "" {
				str = prefix + key
			}

			_, b := rve.FieldByName(str)
			if b {
				result = append(result, data[:len(data)-1]...)
			}
		}
	}

	return result

	//typeOf := reflect.TypeOf(entity) //通过反射获取type定义

	//if typeOf.Kind() == reflect.Ptr { //是否指针类型
	//	typeOf = typeOf.Elem()
	//}

	//prefix := ""
	//for _, key := range fieldPrefix {
	//	prefix = prefix + key
	//}

	//for i := len(data) - 1; i >= 0; i-- {
	//	key := data[i]

	//	str := key
	//	if prefix != "" {
	//		str = prefix + key
	//	}

	//	_, b := typeOf.FieldByName(str)
	//	if !b {
	//		data = append(data[:len(data)-1])
	//	}
	//}

	//return data
}

/**
 * 按实体剔除切片中的数据
 * object 待检查对象
 * data 数据
 * fieldPrefix 字段前缀(可不传)
 */
func (ru ReflectUtils) RemoveByEntityToArray(entity interface{}, data []string, fieldPrefix ...string) []string {
	var rve reflect.Type
	typeOf := reflect.TypeOf(entity)  //通过反射获取type定义
	if typeOf.Kind() == reflect.Ptr { //是否指针类型
		rve = reflect.ValueOf(entity).Elem().Type() // 取得struct变量的指针
	} else if typeOf.String() == "reflect.Value" {
		if entity.(reflect.Value).Kind() == reflect.Ptr {
			rve = entity.(reflect.Value).Elem().Type()
		} else if entity.(reflect.Value).Kind() == reflect.Struct {
			rve = entity.(reflect.Value).Type()
		} else {
			rve = entity.(reflect.Value).Type()
		}
	} else if typeOf.Kind() == reflect.Struct {
		rve = typeOf
	} else {
		rve = reflect.ValueOf(entity).Type()
	}

	prefix := ""
	for _, key := range fieldPrefix {
		prefix = prefix + key
	}

	result := []string{}
	for i := len(data) - 1; i >= 0; i-- {
		key := data[i]

		str := key
		if prefix != "" {
			str = prefix + key
		}

		_, b := rve.FieldByName(str)
		if b {
			result = append(result, data[:len(data)-1]...)
		}
	}

	return result

	//typeOf := reflect.TypeOf(entity) //通过反射获取type定义

	//if typeOf.Kind() == reflect.Ptr { //是否指针类型
	//	typeOf = typeOf.Elem()
	//}

	//prefix := ""
	//for _, key := range fieldPrefix {
	//	prefix = prefix + key
	//}

	//for i := len(data) - 1; i >= 0; i-- {
	//	key := data[i]

	//	str := key
	//	if prefix != "" {
	//		str = prefix + key
	//	}

	//	_, b := typeOf.FieldByName(str)
	//	if b {
	//		data = append(data[:len(data)-1])
	//	}
	//}

	//return data
}

/**
 * 动态创建数组
 * entity 结构参照
 */
func (ru ReflectUtils) MakArray(entity interface{}) interface{} {
	if nil == entity {
		return nil
	}

	rt := reflect.TypeOf(entity)
	sliceType := reflect.SliceOf(rt)
	valueSlice := reflect.MakeSlice(sliceType, 0, 0)

	return valueSlice.Interface()
}

/**
 * 动态创建数组
 * keyEntity 键结构参照
 * valEntity 值结构参照
 */
func (ru ReflectUtils) MakMap(keyEntity interface{}, valEntity interface{}) interface{} {
	if (nil == keyEntity) || (nil == valEntity) {
		return nil
	}

	kType := reflect.TypeOf(keyEntity)
	vType := reflect.TypeOf(valEntity)
	mapType := reflect.MapOf(kType, vType)
	mapValue := reflect.MakeMap(mapType)

	return mapValue.Interface()
}

/**
 * 数组转指定结构数组
 * data 待转换数据,结构: []map[string]interface{}
 * entity 结构参照,结构: []entity
 */
func (ru ReflectUtils) ToEntity(data []interface{}, entity interface{}) (interface{}, bool) {
	if nil == entity {
		return data, false
	}

	rt := reflect.TypeOf(entity)
	sliceType := reflect.SliceOf(rt)
	result := reflect.MakeSlice(sliceType, 0, 0)

	for _, val := range data {
		obj := reflect.New(rt).Elem()
		for k, v := range val.(map[string]interface{}) {
			field := obj.FieldByName(k)
			if field.IsValid() {
				field.Set(reflect.ValueOf(v))
				continue
			}

			field = obj.FieldByName("G" + k)
			if field.IsValid() {
				field.Set(reflect.ValueOf(v))
			}
		}

		result = reflect.Append(result, obj)
	}

	return result.Interface(), true
}

/**
 * 通用Map转指定结构Map
 * data 待转换Map,结构:
 * entity 结构参照
 */
func (ru ReflectUtils) ToEntityMap(data map[string]interface{}, entity interface{}, tt reflect.Type) (interface{}, bool) {
	if nil == entity {
		return data, false
	}

	kType := reflect.TypeOf("")
	vType := reflect.TypeOf(entity)
	mapType := reflect.MapOf(kType, vType)
	result := reflect.MakeMap(mapType)

	num := vType.NumField()
	for key, val := range data {
		obj := reflect.New(vType).Elem()
		vv := reflect.ValueOf(val)
		for i := 0; i < num; i++ {
			field := obj.FieldByName(reflect.TypeOf(val).Name())
			if field.IsValid() {
				field.Set(vv.Field(i))
				continue
			}

			field = obj.FieldByName("G" + reflect.TypeOf(val).Name())
			if field.IsValid() {
				field.Set(vv.Field(i))
			}
		}

		result.SetMapIndex(reflect.ValueOf(key), obj)
	}

	return result.Interface(), true
}

// 根据类型创建新的实体(指针或值)
// isReturnVal 是否以值返回,调用方为匿名类调用时必须返回值,true:返回'值';false:返回'指针'
// 注意:入参数 t不是匿名函数时不能使用 isReturnVal=true
func (ru ReflectUtils) CreateNewInstance(t reflect.Type, isReturnVal bool) interface{} {
	if t.Kind() != reflect.Struct {
		return nil
	}

	ptr := reflect.New(t)
	val := ptr.Elem()

	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		if !f.Anonymous { //是否匿名类
			zero := reflect.Zero(f.Type)
			val.Field(i).Set(zero)
			continue
		}

		subVal := ru.CreateNewInstance(f.Type, f.Anonymous) //匿名类型必须以指针返回
		if subVal != nil {
			sub := reflect.ValueOf(subVal) //原始
			val.Field(i).Set(sub)
		}
	}

	if isReturnVal {
		return ptr.Elem().Interface() //以值返回
	}

	return ptr
}
