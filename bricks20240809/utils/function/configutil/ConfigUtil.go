package file

import (
	Log "github.com/cihub/seelog"
	"gopkg.in/ini.v1"
)

type ConfigUtil struct{}

/**
 * 读取配置文件中指定块下的指定键
 */
func (cu ConfigUtil) readConfig(filePath string, section string, key string) string {
	//set.SetupLogger()

	cfg, err := ini.Load(filePath)

	if err != nil {
		Log.Error("错误:", err.Error())
	}

	result, _ := cfg.Section(section).GetKey(key)
	if result == nil {
		return ""
	}

	return result.String()
}
