package urlutil

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"gitee.com/tomatomeatman/golang-repository/bricks/model/emity"
	"github.com/cihub/seelog"
)

type HttpsUtil struct{}

// GET请求
func (HttpsUtil) Get(urlStr string, params map[string]interface{}, hears map[string]string) *emity.MsgEmity {
	if nil != params {
		var temp strings.Builder
		temp.WriteString(urlStr)
		temp.WriteString("?lt=12")

		for key, value := range params {
			temp.WriteString("&")
			temp.WriteString(key)
			temp.WriteString("=")
			temp.WriteString(fmt.Sprintf("%v", value))
		}

		urlStr = strings.Replace(temp.String(), "lt=12&", "", 1)
	}

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	client := &http.Client{Transport: tr, Timeout: 10 * time.Second}

	req, _ := http.NewRequest("GET", urlStr, nil)
	if nil != hears {
		for key, value := range hears {
			req.Header.Add(key, value)
		}
	}

	resp, err := client.Do(req)
	if err != nil {
		return emity.MsgEmity{}.Err(1001, "请求错误:"+err.Error())
	}

	defer resp.Body.Close() // 释放对象

	// 把获取到的页面作为返回值返回
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return emity.MsgEmity{}.Err(1002, "请求错误:"+err.Error())
	}

	defer client.CloseIdleConnections() // 释放对象

	return emity.MsgEmity{}.Success(string(body), "请求成功")
}

//// GET请求
//func (this HttpsUtil) Get(urlStr string, jsonParam string) string {
//	m, err := jsonutil.JsonUtil{}.JsonToMap(jsonParam)
//	if err != nil {
//		seelog.Error("Convert json to map failed with error: %+v\n", err)
//	}

//	urlStr = urlStr + "?lt=12"
//	for key, value := range m {
//		urlStr = urlStr + "&" + key + "=" + value
//	}

//	tr := &http.Transport{
//		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
//	}

//	client := &http.Client{Transport: tr, Timeout: 10 * time.Second}

//	req, _ := http.NewRequest("GET", urlStr, nil)
//	resp, err := client.Do(req)
//	if err != nil {
//		return emity.MsgEmity{}.ErrString(9004, "请求错误:"+err.Error())
//	}

//	defer resp.Body.Close() // 释放对象

//	// 把获取到的页面作为返回值返回
//	body, err := io.ReadAll(resp.Body)
//	if err != nil {
//		return emity.MsgEmity{}.ErrString(9006, "请求错误:"+err.Error())
//	}

//	defer client.CloseIdleConnections() // 释放对象

//	return emity.MsgEmity{}.SuccessString(string(body), "请求成功")
//}

// POST请求
func (HttpsUtil) Post(urlStr string, jsonParam string, isJsonParams bool) string {
	contentType := "charset=utf-8"
	if isJsonParams {
		contentType = "application/json;charset=utf-8"
	}

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	client := &http.Client{Transport: tr, Timeout: 10 * time.Second}

	req, _ := http.NewRequest("POST", urlStr, bytes.NewBuffer([]byte(jsonParam)))
	req.Header.Set("Content-Type", contentType)

	resp, err := client.Do(req)
	if err != nil {
		return emity.MsgEmity{}.ErrString(9006, "请求错误:"+err.Error())
	}

	defer resp.Body.Close()
	content, err := io.ReadAll(resp.Body)
	if err != nil {
		seelog.Error("Fatal error", err.Error())
	}

	defer client.CloseIdleConnections() // 释放对象

	return emity.MsgEmity{}.SuccessString(string(content), "请求成功")
}
