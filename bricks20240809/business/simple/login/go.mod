module gitee.com/tomatomeatman/golang-repository/bricks/business/simple/login

go 1.21.6

require (
	gitee.com/tomatomeatman/golang-repository/bricks/model/emity v0.0.0-20240717082459-d112b3679d9b
	gitee.com/tomatomeatman/golang-repository/bricks/model/global v0.0.0-20240717082459-d112b3679d9b
	gitee.com/tomatomeatman/golang-repository/bricks/utils/function/app v0.0.0-20240717082459-d112b3679d9b
	gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data/mdutil v0.0.0-20240717082459-d112b3679d9b
	gitee.com/tomatomeatman/golang-repository/bricks/utils/function/file v0.0.0-20240717082459-d112b3679d9b
	gitee.com/tomatomeatman/golang-repository/bricks/utils/function/urlutil v0.0.0-20240717082459-d112b3679d9b
	gitee.com/tomatomeatman/golang-repository/bricks/utils/ginutil v0.0.0-20240717082459-d112b3679d9b
	gitee.com/tomatomeatman/golang-repository/bricks/utils/gorm v0.0.0-20240717082459-d112b3679d9b
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
)

require (
	filippo.io/edwards25519 v1.1.0 // indirect
	gitee.com/tomatomeatman/golang-repository/bricks/model/dbinfo v0.0.0-20240717071722-5484f2aa23e0 // indirect
	gitee.com/tomatomeatman/golang-repository/bricks/model/set v0.0.0-20240717071722-5484f2aa23e0 // indirect
	gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data/aesutil v0.0.0-20240717071722-5484f2aa23e0 // indirect
	gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data/integerutil v0.0.0-20240717071722-5484f2aa23e0 // indirect
	gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data/jsonutil v0.0.0-20240717071722-5484f2aa23e0 // indirect
	gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data/maputil v0.0.0-20240717071722-5484f2aa23e0 // indirect
	gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data/stringutil v0.0.0-20240717071722-5484f2aa23e0 // indirect
	gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data/timeutil v0.0.0-20240717071722-5484f2aa23e0 // indirect
	gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data/uuidutil v0.0.0-20240717071722-5484f2aa23e0 // indirect
	gitee.com/tomatomeatman/golang-repository/bricks/utils/function/system v0.0.0-20240717071722-5484f2aa23e0 // indirect
	github.com/bytedance/sonic v1.11.9 // indirect
	github.com/bytedance/sonic/loader v0.1.1 // indirect
	github.com/cloudwego/base64x v0.1.4 // indirect
	github.com/cloudwego/iasm v0.2.0 // indirect
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/gabriel-vasile/mimetype v1.4.4 // indirect
	github.com/gin-contrib/sse v0.1.0 // indirect
	github.com/gin-contrib/static v1.1.2 // indirect
	github.com/gin-gonic/gin v1.10.0 // indirect
	github.com/glebarez/go-sqlite v1.22.0 // indirect
	github.com/glebarez/sqlite v1.11.0 // indirect
	github.com/go-ole/go-ole v1.3.0 // indirect
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/go-playground/universal-translator v0.18.1 // indirect
	github.com/go-playground/validator/v10 v10.22.0 // indirect
	github.com/go-sql-driver/mysql v1.8.1 // indirect
	github.com/goccy/go-json v0.10.3 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/klauspost/cpuid/v2 v2.2.8 // indirect
	github.com/leodido/go-urn v1.4.0 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/ncruces/go-strftime v0.1.9 // indirect
	github.com/pelletier/go-toml/v2 v2.2.2 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/shirou/gopsutil v3.21.11+incompatible // indirect
	github.com/shopspring/decimal v1.4.0 // indirect
	github.com/tklauser/go-sysconf v0.3.14 // indirect
	github.com/tklauser/numcpus v0.8.0 // indirect
	github.com/twitchyliquid64/golang-asm v0.15.1 // indirect
	github.com/ugorji/go/codec v1.2.12 // indirect
	github.com/yusufpapurcu/wmi v1.2.4 // indirect
	golang.org/x/arch v0.8.0 // indirect
	golang.org/x/crypto v0.25.0 // indirect
	golang.org/x/net v0.27.0 // indirect
	golang.org/x/sys v0.22.0 // indirect
	golang.org/x/text v0.16.0 // indirect
	google.golang.org/protobuf v1.34.2 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	gorm.io/driver/mysql v1.5.7 // indirect
	gorm.io/gorm v1.25.11 // indirect
	modernc.org/libc v1.55.2 // indirect
	modernc.org/mathutil v1.6.0 // indirect
	modernc.org/memory v1.8.0 // indirect
	modernc.org/sqlite v1.30.2 // indirect
)
