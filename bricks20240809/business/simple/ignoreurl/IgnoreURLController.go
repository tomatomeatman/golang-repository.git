package ignoreurl

import (
	"gitee.com/tomatomeatman/golang-repository/bricks/model/global"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/app"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/urlutil"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/ginutil"
)

// @Controller 拦截器忽略路径控制器
type IgnoreURLController struct {
	app.ControllerBaseFunc                  //通用控制层
	ModuleEntity           IgnoreURL        //对应模块数据实体
	ModuleService          IgnoreURLService //对应模块业务实体
}

/**
 * 初始化
 */
func init() {

	//-- 控制操作,首位9不参与用7个数代表开关,0为不限制,1为限制 --//
	//-- 7个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆、物理删除 --//
	//-- 其中前5个为'是否仅创建者可操作'后续为其它控制开关 --//
	go global.GlobalVariable{}.RegisterVariable("IgnoreURL_ModuleEnable", 90000001)

	//-- 检查待新增内容是否存在重复数据(多字段组合重复即重复)集合 --//
	//GlobalVariable{}.RegisterVariable("IgnoreURL_CheckRepeatCombination", []string{"xxx"})

	//-- 检查待新增内容是否存在重复数据(单独字段重复即重复)集合,注意:int必须是1、10、100、1000 --//
	go global.GlobalVariable{}.RegisterVariable("IgnoreURL_CheckRepeatAlone", map[string]int{"sUrlName": 1})

	//-- 记录编号值前缀,此属性用于给id字段添加前缀,以便于在分布式系统中进行数据合并 --//
	//-- 注意:必须规划好各个模块的前缀,如果没有数据合并的需要则不需要设置,默认没有前缀 --//
	//-- 前缀只有1个字符 --//
	//GlobalVariable{}.RegisterVariable("IgnoreURL_IdValuePrefix", "")

	//-- 接口注册 --//
	go ginutil.ControllerRegister("/ignore/url/add", IgnoreURLController{}.Add, ginutil.POST)
	go ginutil.ControllerRegister("/ignore/url/del", IgnoreURLController{}.Del, ginutil.POST)
	go ginutil.ControllerRegister("/ignore/url/edit", IgnoreURLController{}.Edit, ginutil.POST)
	go ginutil.ControllerRegister("/ignore/url/find/id", IgnoreURLController{}.FindById, ginutil.POST)
	go ginutil.ControllerRegister("/ignore/url/find/key", IgnoreURLController{}.FindByKey, ginutil.POST)
	go ginutil.ControllerRegister("/ignore/url/find/all", IgnoreURLController{}.FindAll, ginutil.POST)
	go ginutil.ControllerRegister("/ignore/url/find/page/row", IgnoreURLController{}.FindByRow, ginutil.POST)
	go ginutil.ControllerRegister("/ignore/url/find/page", IgnoreURLController{}.FindByPage, ginutil.POST)
	go ginutil.ControllerRegister("/ignore/url/check", IgnoreURLController{}.CheckIgnoreUrl, ginutil.POST)
	go ginutil.ControllerRegister("/ignore/url/clear/cache", IgnoreURLController{}.ClearCache, ginutil.POST)
}

// #region @Api {title=新增}
// @param {name=data dataType=json paramType=body explain=IgnoreURL结构数据 explainType=IgnoreURL required=true}
// @return {type=json explainType=MsgEmity<IgnoreURL> explain=返回对象}
// @RequestMapping {name=Add type=POST value=/ignore/url/add}
// #endregion
func (control IgnoreURLController) Add(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Add(ctx, &control)
}

// #region @Api {title=删除数据}
// @param {name=sId dataType=string paramType=query explain=记录编号 required=true}
// @param {name=iVersion dataType=int paramType=query explain=版本号 required=true}
// @return {type=json explainType=MsgEmity<int> explain=返回影响数}
// @RequestMapping {name=Del type=POST value=/ignore/url/del}
// #endregion
func (control IgnoreURLController) Del(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Del(ctx, &control)
}

// #region @Api {title=修改数据}
// @param {name=data dataType=json paramType=body explain=IgnoreURL结构数据 explainType=IgnoreURL required=true}
// @return {type=json explainType=MsgEmity<int> explain=返回码值}
// @RequestMapping {name=Edit type=POST value=/ignore/url/edit}
// #endregion
func (control IgnoreURLController) Edit(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Edit(ctx, &control)
}

// #region @Api {title=查询全部}
// @param {name=data dataType=json paramType=body explain=map[string]interface结构数据 explainType=IgnoreURL required=true}
// @return {type=json explainType=MsgEmity<IgnoreURL> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindAll type=POST value=/ignore/url/find/all}
// #endregion
func (control IgnoreURLController) FindAll(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindAll(ctx, &control)
}

// #region @Api {title=查询指定时间内数据}
// @param {name=sDateSt dataType=string paramType=query explain=格式:YYYY-MM-DD HH:mm:ss required=true}
// @param {name=sDateEd dataType=string paramType=query explain=格式:YYYY-MM-DD HH:mm:ss required=true}
// @return {type=json explainType=MsgEmity<IgnoreURL> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByDate type=POST value=/ignore/url/find/date}
// #endregion
func (control IgnoreURLController) FindByDate(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByDate(ctx, &control)
}

// #region @Api {title=根据记录编号取对象}
// @param {name=sId dataType=string paramType=query explain=记录编号 required=true}
// @return {type=json explainType=MsgEmity<IgnoreURL> explain=返回对象}
// @RequestMapping {name=FindById type=POST value=/ignore/url/find/id}
// #endregion
func (control IgnoreURLController) FindById(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindById(ctx, &control)
}

// #region @Api {title=根据关键值取对象集合}
// @param {name=data dataType=json paramType=body explain=IgnoreURL结构数据 explainType=IgnoreURL required=true}
// @return {type=json explainType=MsgEmity<IgnoreURL> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByKey type=POST value=/ignore/url/find/key}
// #endregion
func (control IgnoreURLController) FindByKey(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByKey(ctx, &control)
}

// #region @Api {title=根据记录编号查询符合分页数据的某条记录}
// @param {name=sId dataType=string paramType=query explain=记录编号 required=true}
// @return {type=json explainType=MsgEmity<IgnoreURL> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByRow type=POST value=/ignore/url/find/page/row}
// #endregion
func (control IgnoreURLController) FindByRow(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByRow(ctx, &control)
}

// #region @Api {title=查询分页数据}
// @param {name=data dataType=json paramType=body explain=FindByPageParam结构数据 explainType=FindByPageParam<IgnoreURL> required=true}
// @return {type=json explainType=MsgEmity<Page<IgnoreURL>> explain=返回分页数据}
// @RequestMapping {name=FindByPage type=POST value=/ignore/url/find/page}
// #endregion
func (control IgnoreURLController) FindByPage(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByPage(ctx, &control)
}

// #region @Api {title=验证指定url是否在可忽略的访问路径中(给内部拦截器用,直接返回Boolean)}
// @param {name=sUrl dataType=string paramType=query explain=待检验的url required=true}
// @param {name=sUserType dataType=string paramType=query explain=待检验的用户类型 required=false}
// @param {name=iMustLogin dataType=int paramType=query explain=是否必须登录,1:是;2:否 required=false}
// @return {type=bool explain=返回是否在可忽略的访问路径中}
// @RequestMapping {name=FindByPage type=POST value=/ignore/url/check}
// #endregion
func (control IgnoreURLController) CheckIgnoreUrl(ctx ginutil.Context) interface{} {
	sUrl := urlutil.UrlUtil{}.GetParam(ctx, "sUrl", "").(string)           //记录开始时间
	sUserType := urlutil.UrlUtil{}.GetParam(ctx, "sUserType", "").(string) //记录开始时间
	iMustLogin := urlutil.UrlUtil{}.GetParam(ctx, "iMustLogin", 2).(int)   //记录开始时间

	return control.ModuleService.CheckIgnoreUrl(sUrl, iMustLogin == 1, sUserType)
}

// #region @Api {title=清理缓存 explain=直接操作数据库的情况下需要手工更新缓存}
// @return {type=MsgEmity explain=返回分页数据}
// @RequestMapping {name=FindByPage type=POST value=/ignore/url/clear/cache}
// #endregion
func (control IgnoreURLController) ClearCache(ctx ginutil.Context) interface{} {
	return control.ModuleService.ClearCache()
}
