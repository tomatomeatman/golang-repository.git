package tablekeylocal

import (
	"gitee.com/tomatomeatman/golang-repository/bricks/model/global"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/app"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/urlutil"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/ginutil"
)

// @Controller 记录编号序列管理表控制器
type TableKeyLocalController struct {
	app.ControllerBaseFunc                      //通用控制层
	ModuleEntity           TableKeyLocal        //对应模块数据实体
	ModuleService          TableKeyLocalService //对应模块业务实体
}

/**
 * 初始化
 */
func init() {

	//-- 控制操作,首位9不参与用7个数代表开关,0为不限制,1为限制 --//
	//-- 7个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆、物理删除 --//
	//-- 其中前5个为'是否仅创建者可操作'后续为其它控制开关 --//
	go global.GlobalVariable{}.RegisterVariable("TableKeyLocal_ModuleEnable", 90000001)

	//-- 检查待新增内容是否存在重复数据(多字段组合重复即重复)集合 --//
	//GlobalVariable{}.RegisterVariable("TableKeyLocal_CheckRepeatCombination", []string{"xxx"})

	//-- 检查待新增内容是否存在重复数据(单独字段重复即重复)集合,注意:int必须是1、10、100、1000 --//
	go global.GlobalVariable{}.RegisterVariable("TableKeyLocal_CheckRepeatAlone", map[string]int{
		"sType": 1,
	})

	//-- 记录编号值前缀,此属性用于给id字段添加前缀,以便于在分布式系统中进行数据合并 --//
	//-- 注意:必须规划好各个模块的前缀,如果没有数据合并的需要则不需要设置,默认没有前缀 --//
	//-- 前缀只有1个字符 --//
	//GlobalVariable{}.RegisterVariable("TableKeyLocal_IdValuePrefix", "")

	//-- 接口注册 --//
	go ginutil.RegisterController("/table/key/add", ginutil.POST, TableKeyLocalController{}.Add)
	go ginutil.RegisterController("/table/key/del", ginutil.POST, TableKeyLocalController{}.Del)
	go ginutil.RegisterController("/table/key/edit", ginutil.POST, TableKeyLocalController{}.Edit)
	go ginutil.RegisterController("/table/key/find/id", ginutil.POST, TableKeyLocalController{}.FindById)
	go ginutil.RegisterController("/table/key/find/key", ginutil.POST, TableKeyLocalController{}.FindByKey)
	go ginutil.RegisterController("/table/key/find/all", ginutil.POST, TableKeyLocalController{}.FindByKey)
	go ginutil.RegisterController("/table/key/find/page/row", ginutil.POST, TableKeyLocalController{}.FindByRow)
	go ginutil.RegisterController("/table/key/find/page", ginutil.POST, TableKeyLocalController{}.FindByPage)
	go ginutil.RegisterController("/table/key/new", ginutil.POST, TableKeyLocalController{}.GetNewId)
	go ginutil.RegisterController("/table/key/news", ginutil.POST, TableKeyLocalController{}.GetNewIds)
	go ginutil.RegisterController("/table/key/reset", ginutil.POST, TableKeyLocalController{}.Reset)
}

// #region @Api {title=新增}
// @param {name=data dataType=json paramType=body explain=TableKeyLocal结构数据 explainType=TableKeyLocal required=true}
// @return {type=json explainType=MsgEmity<TableKeyLocal> explain=返回对象}
// @RequestMapping {name=Add type=POST value=/table/key/add}
// #endregion
func (control TableKeyLocalController) Add(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Add(ctx, &control)
}

// #region @Api {title=删除数据}
// @param {name=iId dataType=int64 paramType=query explain=记录编号 required=true}
// @param {name=iVersion dataType=int paramType=query explain=版本号 required=true}
// @return {type=json explainType=MsgEmity<int> explain=返回影响数}
// @RequestMapping {name=Del type=POST value=/table/key/del}
// #endregion
func (control TableKeyLocalController) Del(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Del(ctx, &control)
}

// #region @Api {title=修改数据}
// @param {name=data dataType=json paramType=body explain=TableKeyLocal结构数据 explainType=TableKeyLocal required=true}
// @return {type=json explainType=MsgEmity<int> explain=返回码值}
// @RequestMapping {name=Edit type=POST value=/table/key/edit}
// #endregion
func (control TableKeyLocalController) Edit(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Edit(ctx, &control)
}

// #region @Api {title=查询全部}
// @param {name=data dataType=json paramType=body explain=map[string]interface结构数据 explainType=TableKeyLocal required=true}
// @return {type=json explainType=MsgEmity<TableKeyLocal> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindAll type=POST value=/table/key/find/all}
// #endregion
func (control TableKeyLocalController) FindAll(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindAll(ctx, &control)
}

// #region @Api {title=查询指定时间内数据}
// @param {name=sDateSt dataType=string paramType=query explain=格式:YYYY-MM-DD HH:mm:ss required=true}
// @param {name=sDateEd dataType=string paramType=query explain=格式:YYYY-MM-DD HH:mm:ss required=true}
// @return {type=json explainType=MsgEmity<TableKeyLocal> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByDate type=POST value=/table/key/find/date}
// #endregion
func (control TableKeyLocalController) FindByDate(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByDate(ctx, &control)
}

// #region @Api {title=根据记录编号取对象}
// @param {name=iId dataType=int64 paramType=query explain=记录编号 required=true}
// @return {type=json explainType=MsgEmity<TableKeyLocal> explain=返回对象}
// @RequestMapping {name=FindById type=POST value=/table/key/find/id}
// #endregion
func (control TableKeyLocalController) FindById(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindById(ctx, &control)
}

// #region @Api {title=根据关键值取对象集合}
// @param {name=data dataType=json paramType=body explain=TableKeyLocal结构数据 explainType=TableKeyLocal required=true}
// @return {type=json explainType=MsgEmity<TableKeyLocal> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByKey type=POST value=/table/key/find/key}
// #endregion
func (control TableKeyLocalController) FindByKey(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByKey(ctx, &control)
}

// #region @Api {title=根据记录编号查询符合分页数据的某条记录}
// @param {name=iId dataType=int64 paramType=query explain=记录编号 required=true}
// @return {type=json explainType=MsgEmity<TableKeyLocal> explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByRow type=POST value=/table/key/find/page/row}
// #endregion
func (control TableKeyLocalController) FindByRow(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByRow(ctx, &control)
}

// #region @Api {title=查询分页数据}
// @param {name=data dataType=json paramType=body explain=FindByPageParam结构数据 explainType=FindByPageParam<TableKeyLocal> required=true}
// @return {type=json explainType=MsgEmity<Page<TableKeyLocal>> explain=返回分页数据}
// @RequestMapping {name=FindByPage type=POST value=/table/key/find/page}
// #endregion
func (control TableKeyLocalController) FindByPage(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByPage(ctx, &control)
}

// #region @Api {title=取指定表(或序列)的新Id}
// @param {name=formatLong dataType=int paramType=query explain=格式化长度(不足长度+0) required=true}
// @param {name=seriesName dataType=string paramType=query explain=表名或序列名 required=true}
// @return {type=json explainType=string explain=返回新序列值}
// @RequestMapping {name=GetNewId type=POST value=/table/key/new}
// #endregion
func (control TableKeyLocalController) GetNewId(ctx ginutil.Context) interface{} {
	formatLong := urlutil.UrlUtil{}.GetParam(ctx, "formatLong", 0).(int)
	seriesName := urlutil.UrlUtil{}.GetParam(ctx, "seriesName", "").(string)

	return control.ModuleService.GetNewId(formatLong, seriesName)
}

// #region @Api {title=取指定表的一批新Id}
// @param {name=formatLong dataType=int paramType=query explain=格式化长度(不足长度+0) required=true}
// @param {name=seriesName dataType=string paramType=query explain=表名或序列名 required=true}
// @param {name=size dataType=int paramType=query explain=数量 required=true}
// @return {type=json explainType=string explain=返回新序列值}
// @RequestMapping {name=GetNewIds type=POST value=/table/key/news}
// #endregion
func (control TableKeyLocalController) GetNewIds(ctx ginutil.Context) interface{} {
	formatLong := urlutil.UrlUtil{}.GetParam(ctx, "formatLong", 0).(int)
	seriesName := urlutil.UrlUtil{}.GetParam(ctx, "seriesName", "").(string)
	size := urlutil.UrlUtil{}.GetParam(ctx, "size", 0).(int)
	return control.ModuleService.GetNewIds(formatLong, seriesName, size)
}

// #region @Api {title=重置指定表序号}
// @param {name=seriesName dataType=string paramType=query explain=表名或序列名 required=true}
// @return {type=json explainType=MsgEmity<int> explain=返回码值}
// @RequestMapping {name=Reset type=POST value=/table/key/reset}
// #endregion
func (control TableKeyLocalController) Reset(ctx ginutil.Context) interface{} {
	seriesName := urlutil.UrlUtil{}.GetParam(ctx, "seriesName", "").(string)
	return control.ModuleService.Reset(seriesName)
}
