package userandright

import (
	"time"

	"gitee.com/tomatomeatman/golang-repository/bricks/model/dbinfo"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/gorm"
)

var UserAndRightBase_tableName = ""

/**
 * 用户权限表'UserAndRight'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type UserAndRightBase struct {
	dbinfo.BaseFunc
	GiId           int64     `json:"iId" gorm:"column:iId; type:bigint AUTO_INCREMENT; NOT NULL; primary_key" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"UserAndRight\", \"sName\":\"iId\", \"sComment\":\"记录编号\", \"bDbField\":true, \"sDbFileType\":\"bigint\", \"sDefaultData\": null, \"bExtra\":true, \"bkey\":true, \"bNull\":false,\"iMaxLength\":20, \"bDecimal\":false, \"iIntegralLength\":20, \"iDecimalLength\":0, \"iIndex\":0}"`                          //记录编号
	GsUserId       string    `json:"sUserId" gorm:"column:sUserId; type:varchar; NOT NULL" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"UserAndRight\", \"sName\":\"sUserId\", \"sComment\":\"用户编号\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\": \"\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":8, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":1}"`                                        //用户编号
	GsRightId      string    `json:"sRightId" gorm:"column:sRightId; type:varchar; NOT NULL" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"UserAndRight\", \"sName\":\"sRightId\", \"sComment\":\"权限编号\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\": \"\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":20, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":2}"`                                    //权限编号
	GiType         int       `json:"iType" gorm:"column:iType; type:int; DEFAULT '1'" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"UserAndRight\", \"sName\":\"iType\", \"sComment\":\"权限类型\", \"bDbField\":true, \"sDbFileType\":\"int\", \"sDefaultData\": 1, \"bExtra\":false, \"bkey\":false, \"bNull\":true,\"iMaxLength\":11, \"bDecimal\":false, \"iIntegralLength\":11, \"iDecimalLength\":0, \"iIndex\":3}"`                                                     //权限类型
	GsMemo         string    `json:"sMemo" gorm:"column:sMemo; type:varchar" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"UserAndRight\", \"sName\":\"sMemo\", \"sComment\":\"备注\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\": \"\", \"bExtra\":false, \"bkey\":false, \"bNull\":true,\"iMaxLength\":200, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":4}"`                                                         //备注
	GsCreator      string    `json:"sCreator" gorm:"column:sCreator; type:varchar; NOT NULL; DEFAULT '00000000'" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"UserAndRight\", \"sName\":\"sCreator\", \"sComment\":\"创建者\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\": \"00000000\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":8, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":5}"`          //创建者
	GdCreateDate   time.Time `json:"dCreateDate" gorm:"column:dCreateDate; type:datetime; NOT NULL" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"UserAndRight\", \"sName\":\"dCreateDate\", \"sComment\":\"创建时间\", \"bDbField\":true, \"sDbFileType\":\"datetime\", \"sDefaultData\": \"\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":23, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":6}"`                         //创建时间
	GsModifieder   string    `json:"sModifieder" gorm:"column:sModifieder; type:varchar; NOT NULL; DEFAULT '00000000'" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"UserAndRight\", \"sName\":\"sModifieder\", \"sComment\":\"修改人\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\": \"00000000\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":8, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":7}"` //修改人
	GdModifiedDate time.Time `json:"dModifiedDate" gorm:"column:dModifiedDate; type:datetime; NOT NULL" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"UserAndRight\", \"sName\":\"dModifiedDate\", \"sComment\":\"修改时间\", \"bDbField\":true, \"sDbFileType\":\"datetime\", \"sDefaultData\": \"\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":23, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":8}"`                   //修改时间
	GiState        int       `json:"iState" gorm:"column:iState; type:int; NOT NULL; DEFAULT '1'" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"UserAndRight\", \"sName\":\"iState\", \"sComment\":\"状态\", \"bDbField\":true, \"sDbFileType\":\"int\", \"sDefaultData\": 1, \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":11, \"bDecimal\":false, \"iIntegralLength\":11, \"iDecimalLength\":0, \"iIndex\":9}"`                                         //状态
	GiIndex        int       `json:"iIndex" gorm:"column:iIndex; type:int; NOT NULL; DEFAULT '1'" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"UserAndRight\", \"sName\":\"iIndex\", \"sComment\":\"序号\", \"bDbField\":true, \"sDbFileType\":\"int\", \"sDefaultData\": 1, \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":11, \"bDecimal\":false, \"iIntegralLength\":11, \"iDecimalLength\":0, \"iIndex\":10}"`                                        //序号
	GiVersion      int       `json:"iVersion" gorm:"column:iVersion; type:int; NOT NULL; DEFAULT '1'" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"UserAndRight\", \"sName\":\"iVersion\", \"sComment\":\"版本号\", \"bDbField\":true, \"sDbFileType\":\"int\", \"sDefaultData\": 1, \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":11, \"bDecimal\":false, \"iIntegralLength\":11, \"iDecimalLength\":0, \"iIndex\":11}"`                                 //版本号
}

/**
 * 初始化
 */
func init() {
	// dbinfo.TableInfo{}.RegisterEntity("UserAndRightBase", UserAndRightBase{})           //注册注册数据库实体类结构体
	// dbinfo.TableInfo{}.RegisterTableInfo("UserAndRightBase", UserAndRightBase{}.Info()) //注册数据库表信息
}

/**
 * 创建结构实体,并赋予默认值
 */
func (UserAndRightBase) New() dbinfo.Entity {
	return &UserAndRightBase{
		GiId:           0,
		GsUserId:       "",
		GsRightId:      "",
		GiType:         1,
		GsMemo:         "",
		GsCreator:      "00000000",
		GdCreateDate:   time.Now(),
		GsModifieder:   "00000000",
		GdModifiedDate: time.Now(),
		GiState:        1,
		GiIndex:        1,
		GiVersion:      1,
	}
}

/**
 * 设置默认值
 */
func (o *UserAndRightBase) SetDefault() {
	o.GiId = 0
	o.GsUserId = ""
	o.GsRightId = ""
	o.GiType = 1
	o.GsMemo = ""
	o.GsCreator = "00000000"
	o.GdCreateDate = time.Now()
	o.GsModifieder = "00000000"
	o.GdModifiedDate = time.Now()
	o.GiState = 1
	o.GiIndex = 1
	o.GiVersion = 1
}

/**
 * 结构体映射表名,处理结构体名称与表名不一致的情况
 */
func (userAndRightBase UserAndRightBase) TableName() string {
	if UserAndRightBase_tableName != "" {
		return UserAndRightBase_tableName
	}

	UserAndRightBase_tableName = gorm.GetDbName("BaseSystem") + "UserAndRight"

	return UserAndRightBase_tableName
}

/**
 * 结构体映射表的字段名集合
 */
func (UserAndRightBase) BaseColumnNames() []string {
	return []string{"iId", "sUserId", "sRightId", "iType", "sMemo", "sCreator", "dCreateDate", "sModifieder", "dModifiedDate", "iState", "iIndex", "iVersion"}
}

/**
 * 结构体映射表简化信息
 */
func (UserAndRightBase) TableInfo() *dbinfo.TableInfo {
	return &dbinfo.TableInfo{
		GsDbName:          "",
		GsTableName:       "UserAndRight",
		GsKeyName:         "iId",
		GiKeyLen:          20,
		GbAutoKey:         true,
		GbHasPid:          false,
		GbHasPath:         false,
		GbHasRecordKey:    false,
		GbHasMemo:         true,
		GbHasCreator:      true,
		GbHasCreateDate:   true,
		GbHasModifieder:   true,
		GbHasModifiedDate: true,
		GbHasState:        true,
		GbHasIndex:        true,
		GbHasVersion:      true,
		GbHasPassword:     false,
		GbHasSign:         false,
		GbHasOnlyign:      false,
		GbHasDelSign:      false,
	}
}
