package userandright

import (
	"os"
	"strings"

	"gitee.com/tomatomeatman/golang-repository/bricks/model/emity"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/app"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data/jsonutil"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/file"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/ginutil"

	"gitee.com/tomatomeatman/golang-repository/bricks/utils/gorm"
	Log "github.com/cihub/seelog"
)

type UserAndRightService struct {
	app.CommonService
}

var (
	cUserRightCacheOrdinary = make(map[string][]string) //用户权限验证用缓存池(普通)
	cUserRightCacheSpecial  = make(map[string][]string) //用户权限验证用缓存池(特殊)
	sDbName                 string                      //数据库名
)

func init() {
	sDbName = app.AppUtil{}.ReadConfigKey("DbVariables", "", "").(string)
	//IgnoreURLService{}.initCacheMap() //初始化缓存
}

/**
 * 验证指定用户是否有访问指定url的权限
 * @param sUserId 验证的用户
 * @param url 请求验证的权限(URL地址)
 * @return
 */
func (service UserAndRightService) CheckRight(ctx ginutil.Context, sUserId, url string) *emity.MsgEmity {
	if "" == sUserId {
		return emity.MsgEmity{}.Err(8001, "待验证的用户编号为空")
	}

	if "" == url {
		return emity.MsgEmity{}.Err(8002, "待验证的访问权限为空")
	}

	urlOrdinary := ";" + url + ";" //添加前后缀以免误判

	//--先判断普通权限,普通权限的前后都加有';'--//
	list, ok := cUserRightCacheOrdinary[sUserId]
	if !ok {
		filePath := "./temp/cache/UserAndRight/" + sUserId + "_UrlOrdinary.txt"
		me := jsonutil.JsonUtil{}.FormFile(filePath, []string{})
		if me.Gsuccess {
			cUserRightCacheOrdinary[sUserId] = me.Gdata.([]string)
			list, ok = cUserRightCacheOrdinary[sUserId] //再判断一次
		}
	}

	if ok && (len(list) > 0) {
		for _, val := range list {
			if strings.Contains(val, urlOrdinary) {
				return emity.MsgEmity{}.Success(8999, "有权限")
			}
		}
	}

	if len(list) < 1 {
		me := service.LoadRight(sUserId) //读取指定用户的权限
		if !me.Gsuccess {
			return me
		}

		b := false
		array := []string{}
		simplRights := me.Gdata.([]SimpleRight)
		for _, simplRight := range simplRights {
			if "" == simplRight.GsControllerPath {
				continue
			}

			temp := strings.Split(simplRight.GsControllerPath, ";")
			for _, v := range temp {
				if ("" == v) || strings.Contains(v, "*") {
					continue
				}

				v = ";" + v + ";"
				array = append(array, v)
				if strings.Contains(v, urlOrdinary) {
					b = true
				}
			}
		}

		Array, ok := cUserRightCacheOrdinary[sUserId]
		if ok {
			for _, v := range array {
				Array = append(Array, v)
			}

			cUserRightCacheOrdinary[sUserId] = Array
		} else {
			cUserRightCacheOrdinary[sUserId] = array
		}

		if b {
			return emity.MsgEmity{}.Success(8999, "有权限")
		}
	}

	//--普通权限没有则查找通配符方式,通配符方式权限的前后没有加';'--//
	urlOrdinary = url //重新回到URL
	list, ok = cUserRightCacheSpecial[sUserId]
	if !ok {
		filePath := "./temp/cache/UserAndRight/" + sUserId + "_UrlSpecial.txt"
		me := jsonutil.JsonUtil{}.FormFile(filePath, []string{})
		if me.Gsuccess {
			cUserRightCacheSpecial[sUserId] = me.Gdata.([]string)
			list, ok = cUserRightCacheSpecial[sUserId] //再判断一次
		}
	}

	if ok && (len(list) > 0) {
		for _, val := range list {
			if strings.HasPrefix(urlOrdinary, val) { //判断是否以'val'为开头
				return emity.MsgEmity{}.Success(8999, "有权限")
			}
		}
	}

	if len(list) < 1 {
		me := service.LoadRight(sUserId) //读取指定用户的权限
		if !me.Gsuccess {
			return me
		}

		b := false
		array := []string{}
		simplRights := me.Gdata.([]SimpleRight)
		for _, simplRight := range simplRights {
			if "" == simplRight.GsControllerPath {
				continue
			}

			temp := strings.Split(simplRight.GsControllerPath, ";")
			for _, v := range temp {
				if ("" == v) || !strings.Contains(v, "*") {
					continue
				}

				iEd := strings.Index(v, "*")
				array = append(array, v[:iEd])
				if strings.HasPrefix(urlOrdinary, v) {
					b = true
				}
			}
		}

		Array, ok := cUserRightCacheSpecial[sUserId]
		if ok {
			for _, v := range array {
				Array = append(Array, v)
			}

			cUserRightCacheSpecial[sUserId] = Array
		} else {
			cUserRightCacheSpecial[sUserId] = array
		}

		cUserRightCacheSpecial[sUserId] = array

		if b {
			return emity.MsgEmity{}.Success(8999, "有权限")
		}
	}

	return emity.MsgEmity{}.Err(8004, "无权权访问:", url)
}

/**
 * 读取指定用户的权限,并返回权限集合
 * @param sUserId
 * @return
 */
func (service UserAndRightService) LoadRight(sUserId string) *emity.MsgEmity {
	//--读文件获取--//
	filePath := "./temp/cache/UserAndRight/" + sUserId + "_Simpl.txt"
	//CacheTxtUtil{}.ReadCacheFile(result, filePath)
	//temp := CacheTxtUtil{}.ReadCacheFile(filePath)
	me := jsonutil.JsonUtil{}.FormFile(filePath, SimpleRight{})
	if me.Gsuccess {
		list := me.Gdata.([]SimpleRight)
		service.crateRightCache(list, sUserId)
		return emity.MsgEmity{}.Success(list, "在文件中找到") //返回结果
	}

	//--文件没有则读库获取--//
	me = service.findRightListByInit(sUserId) //根据用户读取用户对应的所有权限,注意:只返回了3个值sRightId/sEnglish/sControllerPath
	if !me.Gsuccess {
		return me
	}

	list := me.Gdata.([]SimpleRight)
	if len(list) < 1 {
		return emity.MsgEmity{}.Err(8100, "未查询到用户相关权限信息!")
	}

	service.crateRightCache(list, sUserId)

	//CacheTxtUtil{}.CreateCacheFile(list, "./temp/cache/UserAndRight/"+sUserId+"_Simpl.txt") //写入文件，以便下次不用读库
	jsonutil.JsonUtil{}.ToFile(list, "./temp/cache/UserAndRight/"+sUserId+"_Simpl.txt")

	return emity.MsgEmity{}.Success(list, "在数据库中找到") //返回结果
}

/**
 * 初始化查询权限信息集合
 * @param sUserId
 * @return
 */
func (service UserAndRightService) findRightListByInit(sUserId string) *emity.MsgEmity {
	var build strings.Builder
	build.WriteString(" SELECT")
	build.WriteString(" 	RightManage.sRightId,")
	build.WriteString(" 	RightManage.sEnglish,")
	build.WriteString(" 	RightManage.sControllerPath")
	build.WriteString(" FROM ")

	where := map[string]interface{}{}

	if ("" != sUserId) && (strings.Contains("/00000000/00000001/", sUserId)) {
		build.WriteString(" 	${BricksBaseSystem}RightManage")
	} else {
		build.WriteString(" 	${BricksBaseSystem}RightManage AS a, ${BricksBaseSystem}UserAndRight")
		build.WriteString(" WHERE RightManage.sRightId = UserAndRight.sRightId")
		build.WriteString(" 	AND UserAndRight.sUserId = @sUserId ")
		build.WriteString(" UNION")
		build.WriteString(" SELECT ")
		build.WriteString(" 	RightManage.sRightId,")
		build.WriteString(" 	RightManage.sEnglish,")
		build.WriteString(" 	RightManage.sControllerPath ")
		build.WriteString(" FROM ${BricksBaseSystem}RightManage")
		build.WriteString(" WHERE RightManage.sRightId IN (")
		build.WriteString(" 	select sRightId from ${BricksBaseSystem}RoleAndRight")
		build.WriteString(" 	where RoleAndRight.sRoleId in (")
		build.WriteString(" 		select a.sRoleId from ${BricksBaseSystem}UserAndRole a")
		build.WriteString(" 		where a.sUserId = @sUserId")
		build.WriteString(" 	)")
		build.WriteString(" )")

		where["sUserId"] = sUserId
	}

	txt := build.String()
	txt = strings.Replace(txt, "${BricksBaseSystem}", sDbName, -1)

	list := []SimpleRight{}
	dbResult := gorm.Raw(txt, where).Find(&list)

	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return emity.MsgEmity{}.Err(1002, "查询数据发生异常:", dbResult.Error)
	}

	return emity.MsgEmity{}.Success(list, "新增成功")
}

/**
 * 创建缓存
 */
func (service UserAndRightService) crateRightCache(list []SimpleRight, sUserId string) *emity.MsgEmity {
	if len(list) < 1 {
		return emity.MsgEmity{}.Err(8100, "数据有错")
	}

	var sbEnglist strings.Builder
	listRightId := map[string]string{}
	listUrlsOrdinary := map[string]string{}
	listUrlsSpecial := map[string]string{}

	for _, simpleRight := range list {
		sbEnglist.WriteString(simpleRight.GsEnglish)
		sbEnglist.WriteString("/")
		listRightId[simpleRight.GsRightId] = ""

		temp := strings.TrimSpace(simpleRight.GsControllerPath)
		if temp == "" {
			continue
		}

		array := strings.Split(temp, ";")
		for _, val := range array {
			val = strings.TrimSpace(val)
			if ("" == val) || ("-" == val) {
				continue
			}

			str := ";" + val + ";"
			_, ok := listUrlsOrdinary[str]
			if (!strings.Contains(val, "*")) && (!ok) {
				listUrlsOrdinary[str] = ""
				continue
			}

			_, ok = listUrlsSpecial[val]
			if (strings.Contains(val, "*")) && (!ok) {
				iEd := strings.Index(val, "*")
				listUrlsSpecial[val[:iEd]] = ""
				continue
			}
		}
	}

	array := []string{}
	for key := range listUrlsOrdinary {
		array = append(array, key)
	}
	//CacheTxtUtil{}.CreateCacheFile(array, "./temp/cache/UserAndRight/"+sUserId+"_UrlOrdinary.txt") //写入文件，以便下次不用读库
	jsonutil.JsonUtil{}.ToFile(array, "./temp/cache/UserAndRight/"+sUserId+"_UrlOrdinary.txt")

	array = []string{}
	for key := range listUrlsSpecial {
		array = append(array, key)
	}
	//CacheTxtUtil{}.CreateCacheFile(array, "./temp/cache/UserAndRight/"+sUserId+"_UrlSpecial.txt") //写入文件，以便下次不用读库
	jsonutil.JsonUtil{}.ToFile(array, "./temp/cache/UserAndRight/"+sUserId+"_UrlSpecial.txt")

	array = []string{}
	for key := range listRightId {
		array = append(array, key)
	}
	//CacheTxtUtil{}.CreateCacheFile(array, "./temp/cache/UserAndRight/"+sUserId+"_UserRightIdList.txt") //写入文件，以便下次不用读库
	jsonutil.JsonUtil{}.ToFile(array, "./temp/cache/UserAndRight/"+sUserId+"_UserRightIdList.txt")

	file.FileUtil{}.Save(sbEnglist.String(), "./temp/cache/UserAndRight/"+sUserId+"_Englist.txt") //写入文件，以便下次不用读库

	return emity.MsgEmity{}.Success(8999, "创建缓存成功") //返回结果
}

/**
 * 根据用户查询用户所拥有的权限的最后更新时间
 * @param sUserId
 * @return
 */
func (service UserAndRightService) LastTime(ctx ginutil.Context, sUserId string) *emity.MsgEmity {
	sUserId = strings.TrimSpace(sUserId)
	if "" == sUserId {
		return emity.MsgEmity{}.Err(8001, "待验证的用户编号为空")
	}

	//--读文件获取--//
	filePath := "./temp/cache/UserAndRight/" + sUserId + "_Simpl.txt"
	if (file.FileUtil{}.IsExist(filePath)) {
		f, _ := os.Open(filePath)
		fi, _ := f.Stat()
		return emity.MsgEmity{}.Success(fi.ModTime().Unix(), "查询到权限信息缓存最后时间")
	}

	service.LoadRight(sUserId) //建立缓存

	if (file.FileUtil{}.IsExist(filePath)) {
		f, _ := os.Open(filePath)
		fi, _ := f.Stat()
		return emity.MsgEmity{}.Success(fi.ModTime().Unix(), "查询到权限信息缓存最后时间")
	}

	return emity.MsgEmity{}.Err(8002, "没有查询到权限信息缓存最后时间")
}

/**
 * 根据用户查询用户所拥有的权限编号集合
 * @param sUserId
 * @return
 */
func (service UserAndRightService) FindRightId(sUserId string) *emity.MsgEmity {
	sUserId = strings.TrimSpace(sUserId)
	if "" == sUserId {
		return emity.MsgEmity{}.Err(8001, "用户编号不能为空")
	}

	//-- 读取预存储的缓存文件 --//
	filePath := "./temp/cache/UserAndRight/" + sUserId + "_UserRightIdList.txt"
	me := file.FileUtil{}.ReadFromFile(filePath)
	if me.Gsuccess {
		array := strings.Split(me.Gdata.(string), ",")
		if len(array) > 0 {
			return emity.MsgEmity{}.Success(array, "查询成功")
		}
	}

	me = service.LoadRight(sUserId) //读取指定用户的权限
	if !me.Gsuccess {
		return me
	}

	me = file.FileUtil{}.ReadFromFile(filePath)
	if !me.Gsuccess {
		return me.IncCode(10)
	}

	array := strings.Split(me.Gdata.(string), ",")

	return emity.MsgEmity{}.Success(array, "查询成功")
}

/**
 * 根据用户取权限标识字符串(一个权限标识代表了多个可访问的url路径)
 * @param sUserId
 * @return
 */
func (service UserAndRightService) FindEnglishByUserId(sUserId string) *emity.MsgEmity {
	sUserId = strings.TrimSpace(sUserId)
	if "" == sUserId {
		return emity.MsgEmity{}.Err(8001, "用户编号不能为空")
	}

	//-- 读取预存储的缓存文件 --//
	filePath := "./temp/cache/UserAndRight/" + sUserId + "_Englist.txt"
	me := file.FileUtil{}.ReadFromFile(filePath)
	if me.Gsuccess {
		if me.Gdata.(string) != "" {
			return emity.MsgEmity{}.Success(me.Gdata.(string), "查询成功")
		}
	}

	me = service.LoadRight(sUserId) //读取指定用户的权限
	if !me.Gsuccess {
		return me
	}

	me = file.FileUtil{}.ReadFromFile(filePath)
	if !me.Gsuccess {
		return me.IncCode(10)
	}

	if me.Gdata.(string) == "" {
		return emity.MsgEmity{}.Err(8002, "没有找到用户对应的权限")
	}

	return emity.MsgEmity{}.Success(me.Gdata.(string), "查询成功")
}
