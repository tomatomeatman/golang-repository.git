package loginuser

import (
	"gitee.com/tomatomeatman/golang-repository/bricks/model/global"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/app"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/urlutil"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/ginutil"
)

// @Controller 用户表控制器
type LoginUserController struct {
	app.ControllerBaseFunc                  //通用控制层
	ModuleEntity           LoginUser        //对应模块数据实体
	ModuleService          LoginUserService //对应模块业务实体
}

/**
 * 初始化
 */
func init() {
	//-- 控制操作,首位9不参与用6个数代表开关,0为不限制,1为限制 --//
	//-- 6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆 --//
	//-- 其中前5个为'是否仅创建者可操作'后续为其它控制开关 --//
	go global.GlobalVariable{}.RegisterVariable("LoginUser_ModuleEnable", 9000000)

	//-- 检查待新增内容是否存在重复数据(多字段组合重复即重复)集合 --//
	//GlobalVariable{}.RegisterVariable("LoginUser_CheckRepeatCombination", []string{"xxx"})

	//-- 检查待新增内容是否存在重复数据(单独字段重复即重复)集合 --//
	go global.GlobalVariable{}.RegisterVariable("LoginUser_CheckRepeatAlone", map[string]int{
		"sName": 1,
		"sNo":   2,
	})

	//-- 记录编号值前缀,此属性用于给id字段添加前缀,以便于在分布式系统中进行数据合并 --//
	//-- 注意:必须规划好各个模块的前缀,如果没有数据合并的需要则不需要设置,默认没有前缀 --//
	//-- 前缀只有1个字符 --//
	//GlobalVariable{}.RegisterVariable("LoginUser_IdValuePrefix", "")

	//-- 接口注册 --//
	go ginutil.ControllerRegister("/login/user/add", LoginUserController{}.Add, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/del", LoginUserController{}.Del, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/edit", LoginUserController{}.Edit, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/find/id", LoginUserController{}.FindById, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/find/key", LoginUserController{}.FindByKey, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/find/all", LoginUserController{}.FindAll, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/find/page/row", LoginUserController{}.FindByRow, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/find/page", LoginUserController{}.FindByPage, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/password/edit", LoginUserController{}.EditPass, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/password/reset", LoginUserController{}.ResetPass, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/edit/type", LoginUserController{}.EditUserType, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/edit/head", LoginUserController{}.EditUserHead, ginutil.POST)
	go ginutil.ControllerRegister("/login/user/head", LoginUserController{}.LookHead, ginutil.GET)
}

// #region @Api {title=新增}
// @param {name=data dataType=json paramType=body explain=LoginUser结构数据 required=true}
// @return {type=MsgEmity explain=返回对象}
// @RequestMapping {name=Add type=POST value=/login/user/add}
// #endregion
func (control LoginUserController) Add(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Add(ctx, &control)
}

// #region @Api {title=删除数据}
// @param {name=sId dataType=string paramType=query explain=记录编号 required=true}
// @param {name=iVersion dataType=int paramType=query explain=版本号 required=true}
// @return {type=MsgEmity explain=返回影响数}
// @RequestMapping {name=Del type=POST value=/login/user/del}
// #endregion
func (control LoginUserController) Del(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Del(ctx, &control)
}

// #region @Api {title=修改数据}
// @param {name=data dataType=json paramType=body explain=LoginUser结构数据 required=true}
// @return {type=MsgEmity explain=返回码值}
// @RequestMapping {name=Edit type=POST value=/login/user/edit}
// #endregion
func (control LoginUserController) Edit(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.Edit(ctx, &control)
}

// #region @Api {title=查询全部}
// @param {name=data dataType=json paramType=body explain=map[string]interface结构数据 required=true}
// @return {type=MsgEmity explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindAll type=POST value=/login/user/find/all}
// #endregion
func (control LoginUserController) FindAll(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindAll(ctx, &control)
}

// #region @Api {title=查询指定时间内数据}
// @param {name=sDateSt dataType=string paramType=query explain=格式:YYYY-MM-DD HH:mm:ss required=true}
// @param {name=sDateEd dataType=string paramType=query explain=格式:YYYY-MM-DD HH:mm:ss required=true}
// @return {type=MsgEmity explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByDate type=POST value=/login/user/find/date}
// #endregion
func (control LoginUserController) FindByDate(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByDate(ctx, &control)
}

// #region @Api {title=根据记录编号取对象}
// @param {name=sId dataType=string paramType=query explain=记录编号 required=true}
// @return {type=MsgEmity explain=返回对象}
// @RequestMapping {name=FindById type=POST value=/login/user/find/id}
// #endregion
func (control LoginUserController) FindById(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindById(ctx, &control)
}

// #region @Api {title=根据关键值取对象集合}
// @param {name=data dataType=json paramType=body explain=LoginUser结构数据 required=true}
// @return {type=MsgEmity explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByKey type=POST value=/login/user/find/key}
// #endregion
func (control LoginUserController) FindByKey(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByKey(ctx, &control)
}

// #region @Api {title=根据记录编号查询符合分页数据的某条记录}
// @param {name=sId dataType=string paramType=query explain=记录编号 required=true}
// @return {type=MsgEmity explain=返回数组[]map[string]interface}
// @RequestMapping {name=FindByRow type=POST value=/login/user/find/page/row}
// #endregion
func (control LoginUserController) FindByRow(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByRow(ctx, &control)
}

// #region @Api {title=查询分页数据}
// @param {name=data dataType=json paramType=body explain=findByPageParam结构数据 required=true}
// @return {type=MsgEmity explain=返回分页数据}
// @RequestMapping {name=FindByPage type=POST value=/login/user/find/page}
// #endregion
func (control LoginUserController) FindByPage(ctx ginutil.Context) interface{} {
	return app.ControllerUtil{}.FindByPage(ctx, &control)
}

// #region @Api {title=修改当前用户密码}
// @param {name=sOldPass dataType=string paramType=query explain=旧密码 required=true}
// @param {name=sNewPass dataType=string paramType=query explain=新密码 required=true}
// @return {type=json explainType=MsgEmity<int> explain=返回码值}
// @RequestMapping {name=EditPass type=POST value=/login/user/password/edit}
// #endregion
func (control LoginUserController) EditPass(ctx ginutil.Context) interface{} {
	sLoginUserId := app.ModuleUtil{}.CurrentLoginUserId(ctx) //取当前登录用户编号
	sOldPass := urlutil.UrlUtil{}.GetParam(ctx, "sOldPass", "").(string)
	sNewPass := urlutil.UrlUtil{}.GetParam(ctx, "sNewPass", "").(string)
	sOwner := urlutil.UrlUtil{}.GetParam(ctx, "sOwner", "").(string)

	return control.ModuleService.EditPass(ctx, LoginUser{}, sLoginUserId, sOldPass, sNewPass, sOwner)
}

// 只有当前用户和管理员(admin)才能重置密码
// #region @Api {title=重置密码}
// @param {name=sId dataType=string paramType=query explain=表编号 required=true}
// @param {name=sPass dataType=int paramType=query explain=新密码 required=true}
// @return {type=json explainType=MsgEmity<int> explain=返回码值}
// @RequestMapping {name=ResetPass type=POST value=/login/user/password/reset}
// #endregion
func (control LoginUserController) ResetPass(ctx ginutil.Context) interface{} {
	sLoginUserId := app.ModuleUtil{}.CurrentLoginUserId(ctx) //取当前登录用户编号
	sId := urlutil.UrlUtil{}.GetParam(ctx, "sId", "").(string)
	sPass := urlutil.UrlUtil{}.GetParam(ctx, "sPass", "").(string)
	sOwner := urlutil.UrlUtil{}.GetParam(ctx, "sOwner", "").(string)

	return control.ModuleService.ResetPass(sLoginUserId, sId, sPass, sOwner)
}

// #region @Api {title=根据用户编号设置用户类型}
// @param {name=sId dataType=string paramType=query explain=用户编号 required=true}
// @param {name=sType dataType=string paramType=query explain=用户类型 required=true}
// @return {type=json explainType=MsgEmity<int> explain=返回码值}
// @RequestMapping {name=EditUserType type=POST value=/login/user/edit/type}
// #endregion
func (control LoginUserController) EditUserType(ctx ginutil.Context) interface{} {
	sLoginUserId := app.ModuleUtil{}.CurrentLoginUserId(ctx) //取当前登录用户编号
	sId := urlutil.UrlUtil{}.GetParam(ctx, "sId", "").(string)
	sType := urlutil.UrlUtil{}.GetParam(ctx, "sType", "").(string)
	sOwner := urlutil.UrlUtil{}.GetParam(ctx, "sOwner", "").(string)

	return control.ModuleService.EditUserType(sLoginUserId, sId, sType, sOwner)
}

// #region @Api {title=设置用户头像}
// @param {name=sHead dataType=string paramType=query explain=Base64图片 required=true}
// @return {type=json explainType=MsgEmity<int> explain=返回码值}
// @RequestMapping {name=EditUserHead type=POST value=/login/user/edit/head}
// #endregion
func (control LoginUserController) EditUserHead(ctx ginutil.Context) interface{} {
	sLoginUserId := app.ModuleUtil{}.CurrentLoginUserId(ctx) //取当前登录用户编号
	sHead := urlutil.UrlUtil{}.GetParam(ctx, "sHead", "").(string)

	return control.ModuleService.EditUserHead(sLoginUserId, sHead)
}

// #region @Api {title=取用户头像}
// @return {type=bit explainType=image/png explain=返回图片流}
// @RequestMapping {name=LookHead type=GET value=/login/user/head}
// #endregion
func (control LoginUserController) LookHead(ctx ginutil.Context) interface{} {
	sLoginUserId := app.ModuleUtil{}.CurrentLoginUserId(ctx) //取当前登录用户编号

	me := control.ModuleService.LookHead(sLoginUserId)
	if !me.Gsuccess {
		return me
	}

	// 设置HTTP响应头Content-Type为图片类型（这里是png）
	ctx.Header("Content-Type", "image/png")

	// 返回图片数据
	ctx.Data(200, "image/png", me.Gdata.([]byte))

	return nil
}
