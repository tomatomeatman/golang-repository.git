package loginuser

import (
	"strings"
	"time"

	"gitee.com/tomatomeatman/golang-repository/bricks/model/dbinfo"
	"gitee.com/tomatomeatman/golang-repository/bricks/utils/gorm"
)

var LoginUserBase_tableName = ""

/**
 * 用户表'LoginUser'表结构体
 * 警告:非数据库字段禁止在此添加,应该创建扩展结构体中添加
 * @author HuangXinBian
 */
type LoginUserBase struct {
	GsId           string    `json:"sId" gorm:"column:sId; type:varchar; NOT NULL; primary_key" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"LoginUser\", \"sName\":\"sId\", \"sComment\":\"记录编号\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\":\"\", \"bExtra\":false, \"bkey\":true, \"bNull\":false,\"iMaxLength\":8, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":0}"`                                         //记录编号
	GsName         string    `json:"sName" gorm:"column:sName; type:varchar; NOT NULL" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"LoginUser\", \"sName\":\"sName\", \"sComment\":\"名称\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\":\"\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":32, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":1}"`                                                //名称
	GsNo           string    `json:"sNo" gorm:"column:sNo; type:varchar; NOT NULL" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"LoginUser\", \"sName\":\"sNo\", \"sComment\":\"标识(组合唯一)\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\":\"\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":64, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":2}"`                                                //标识(组合唯一)
	GsPass         string    `json:"sPass" gorm:"column:sPass; type:varchar; NOT NULL" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"LoginUser\", \"sName\":\"sPass\", \"sComment\":\"密码\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\":\"\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":64, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":3}"`                                                //密码
	GsType         string    `json:"sType" gorm:"column:sType; type:varchar; NOT NULL; DEFAULT '管理员'" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"LoginUser\", \"sName\":\"sType\", \"sComment\":\"类型\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\":\"管理员\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":50, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":4}"`                              //类型
	GsOwner        string    `json:"sOwner" gorm:"column:sOwner; type:varchar; DEFAULT 'BaseSystem.LoginUser'" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"LoginUser\", \"sName\":\"sOwner\", \"sComment\":\"来源\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\":\"BaseSystem.LoginUser\", \"bExtra\":false, \"bkey\":false, \"bNull\":true,\"iMaxLength\":100, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":5}"`   //来源
	GsMemo         string    `json:"sMemo" gorm:"column:sMemo; type:varchar" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"LoginUser\", \"sName\":\"sMemo\", \"sComment\":\"备注\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\":\"\", \"bExtra\":false, \"bkey\":false, \"bNull\":true,\"iMaxLength\":200, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":6}"`                                                          //备注
	GsCreator      string    `json:"sCreator" gorm:"column:sCreator; type:varchar; NOT NULL; DEFAULT '00000000'" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"LoginUser\", \"sName\":\"sCreator\", \"sComment\":\"创建人员\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\":\"00000000\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":8, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":7}"`          //创建人员
	GdCreateDate   time.Time `json:"dCreateDate" gorm:"column:dCreateDate; type:datetime; NOT NULL" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"LoginUser\", \"sName\":\"dCreateDate\", \"sComment\":\"创建时间\", \"bDbField\":true, \"sDbFileType\":\"datetime\", \"sDefaultData\":\"\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":23, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":8}"`                          //创建时间
	GsModifieder   string    `json:"sModifieder" gorm:"column:sModifieder; type:varchar; NOT NULL; DEFAULT '00000000'" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"LoginUser\", \"sName\":\"sModifieder\", \"sComment\":\"修改人员\", \"bDbField\":true, \"sDbFileType\":\"varchar\", \"sDefaultData\":\"00000000\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":8, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":9}"` //修改人员
	GdModifiedDate time.Time `json:"dModifiedDate" gorm:"column:dModifiedDate; type:datetime; NOT NULL" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"LoginUser\", \"sName\":\"dModifiedDate\", \"sComment\":\"修改时间\", \"bDbField\":true, \"sDbFileType\":\"datetime\", \"sDefaultData\":\"\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":23, \"bDecimal\":false, \"iIntegralLength\":0, \"iDecimalLength\":0, \"iIndex\":10}"`                   //修改时间
	GiState        int       `json:"iState" gorm:"column:iState; type:int; NOT NULL; DEFAULT '1'" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"LoginUser\", \"sName\":\"iState\", \"sComment\":\"状态(枚举,1:启用;2:禁用)\", \"bDbField\":true, \"sDbFileType\":\"int\", \"sDefaultData\":\"1\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":11, \"bDecimal\":false, \"iIntegralLength\":11, \"iDecimalLength\":0, \"iIndex\":11}"`                       //状态(枚举,1:启用;2:禁用)
	GiIndex        int       `json:"iIndex" gorm:"column:iIndex; type:int; NOT NULL; DEFAULT '1'" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"LoginUser\", \"sName\":\"iIndex\", \"sComment\":\"序号\", \"bDbField\":true, \"sDbFileType\":\"int\", \"sDefaultData\":\"1\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":11, \"bDecimal\":false, \"iIntegralLength\":11, \"iDecimalLength\":0, \"iIndex\":12}"`                                     //序号
	GiVersion      int       `json:"iVersion" gorm:"column:iVersion; type:int; NOT NULL; DEFAULT '1'" dataInfo:"{\"sDbName\":\"\", \"sTableName\":\"LoginUser\", \"sName\":\"iVersion\", \"sComment\":\"版本号\", \"bDbField\":true, \"sDbFileType\":\"int\", \"sDefaultData\":\"1\", \"bExtra\":false, \"bkey\":false, \"bNull\":false,\"iMaxLength\":11, \"bDecimal\":false, \"iIntegralLength\":11, \"iDecimalLength\":0, \"iIndex\":13}"`                              //版本号
}

/**
 * 初始化
 */
func init() {
	dbinfo.TableInfo{}.RegisterEntity("LoginUserBase", LoginUserBase{})                //注册注册数据库实体类结构体
	dbinfo.TableInfo{}.RegisterTableInfo("LoginUserBase", LoginUserBase{}.TableInfo()) //注册数据库表信息
}

/**
 * 创建结构实体,并赋予默认值
 */
func (LoginUserBase) New() LoginUserBase {
	return LoginUserBase{
		GsId:           "",
		GsName:         "",
		GsNo:           "",
		GsPass:         "",
		GsType:         "管理员",
		GsOwner:        "BaseSystem.LoginUser",
		GsMemo:         "",
		GsCreator:      "00000000",
		GdCreateDate:   time.Now(),
		GsModifieder:   "00000000",
		GdModifiedDate: time.Now(),
		GiState:        1,
		GiIndex:        1,
		GiVersion:      1,
	}
}

/**
 * 设置默认值
 */
func (lu *LoginUserBase) SetDefault() {
	lu.GsId = ""
	lu.GsName = ""
	lu.GsNo = ""
	lu.GsPass = ""
	lu.GsType = "管理员"
	lu.GsOwner = "BaseSystem.LoginUser"
	lu.GsMemo = ""
	lu.GsCreator = "00000000"
	lu.GdCreateDate = time.Now()
	lu.GsModifieder = "00000000"
	lu.GdModifiedDate = time.Now()
	lu.GiState = 1
	lu.GiIndex = 1
	lu.GiVersion = 1
}

/**
 * 结构体映射表名,处理结构体名称与表名不一致的情况
 */
func (LoginUserBase) TableName() string {
	if LoginUserBase_tableName != "" {
		return LoginUserBase_tableName
	}

	LoginUserBase_tableName = gorm.GetDbName("BaseSystem") + "LoginUser" //通过配置文件的数据库全局变量'DbVariables'进行调整即可

	return LoginUserBase_tableName
}

/**
 * 结构体映射表的字段名集合
 */
func (LoginUserBase) BaseColumnNames() []string {
	return strings.Split("sId,sName,sNo,sPass,sType,sOwner,sMemo,sCreator,dCreateDate,sModifieder,dModifiedDate,iState,iIndex,iVersion", ",")
}

/**
 * 结构体映射表简化信息
 */
func (LoginUserBase) TableInfo() *dbinfo.TableInfo {
	return &dbinfo.TableInfo{
		GsDbName:          "",
		GsTableName:       "LoginUser",
		GsKeyName:         "sId",
		GiKeyLen:          8,
		GbAutoKey:         false,
		GbHasPid:          false,
		GbHasPath:         false,
		GbHasRecordKey:    false,
		GbHasMemo:         true,
		GbHasCreator:      true,
		GbHasCreateDate:   true,
		GbHasModifieder:   true,
		GbHasModifiedDate: true,
		GbHasState:        true,
		GbHasIndex:        true,
		GbHasVersion:      true,
		GbHasPassword:     true,
		GbHasSign:         false,
		GbHasOnlyign:      false,
	}
}
