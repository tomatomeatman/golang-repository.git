package log

import (
	"sync"

	"github.com/cihub/seelog"
)

var seelogOnce sync.Once

func init() {
	SetupLogger()
}

func SetupLogger() {
	go seelogOnce.Do(func() {
		txt := `<seelog levels="trace,debug,info,warn,error,critical"><outputs formatid="main"><filter levels="trace,debug"><console formatid="colored-default"/><rollingfile formatid="main" type="size" filename="./logs/debug.log" maxsize="10485760" maxrolls="99" /></filter><filter levels="trace,info"><console formatid="colored-default"/><rollingfile formatid="main" type="size" filename="./logs/info.log" maxsize="10485760" maxrolls="99" /></filter><filter levels="warn"><console formatid="colored-warn"/><rollingfile formatid="main" type="size" filename="./logs/warn.log" maxsize="10485760" maxrolls="99" /></filter><filter levels="error,critical"><console formatid="colored-error"/><rollingfile formatid="main" type="size" filename="./logs/error.log" maxsize="10485760" maxrolls="99" /></filter></outputs><formats><format id="colored-default" format="%EscM(38)%Date %Time [%LEV] %File:%Line | %Msg%n%EscM(0)"/><format id="colored-warn" format="%EscM(33)%Date %Time [%LEV] %File:%Line | %Msg%n%EscM(0)"/><format id="colored-error" format="%EscM(31)%Date %Time [%LEV] %File:%Line | %Msg%n%EscM(0)"/><format id="main" format="%Date %Time [%LEV] %File:%Line | %Msg%n"/></formats></seelog>`
		logger, err := seelog.LoggerFromConfigAsBytes([]byte(txt))
		if err != nil {
			return
		}

		seelog.ReplaceLogger(logger)
	})
}
