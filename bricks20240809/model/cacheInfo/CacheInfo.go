package cacheInfo

import "time"

//用于定义缓存信息的类
type CacheInfo struct {
	GsId     string      `json:"sId"`     //编号,作为关键字存在
	Gdata    interface{} `json:"data"`    //缓存的数据对象
	GdCreate time.Time   `json:"dCreate"` //创建时间
	GdLast   time.Time   `json:"dLast"`   //最后访问时间
}

/**
 * 新建缓存信息结构体
 * @param sId 编号,作为关键字存在
 * @param data 缓存的数据对象
 * @return 返回新创建的结构体
 */
func (CacheInfo) New(sId string, data interface{}) CacheInfo {
	result := CacheInfo{
		sId,
		data,
		time.Now(),
		time.Now(),
	}

	return result
}
