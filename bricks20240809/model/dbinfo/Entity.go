package dbinfo

// 实体接口定义,默认方法
type EntityDefaultFunc interface {
	HasColumnNames(name string) bool   //判断是否存在字段名
	AllDataInfo() []*DataInfo          //取所有数据结构信息
	BaseColumnNames() []string         //结构体映射表的字段名集合
	GetDataInfo(name string) *DataInfo //取数据结构信息
	NewList() interface{}              //创建类型未知的结构实体集合,并赋予默认值
	SetDefault()                       //设置默认值
	BaseEntity() Entity                //取基础实体,用于在子类(嵌套结构体)时同样获得基类
	New() Entity                       //创建结构实体,并赋予默认值
}

// 实体接口定义,用于规范实体结构体
type Entity interface {
	EntityDefaultFunc
	TableName() string     //结构体映射表名
	TableInfo() *TableInfo //结构体映射表简化信息
}

type BaseFunc struct{}

/**
 * 判断是否存在字段名
 */
func (bf *BaseFunc) HasColumnNames(name string) bool {
	array := bf.BaseColumnNames()
	for _, v := range array {
		if v == name {
			return true
		}
	}

	return false
}

/**
 * 取所有数据结构信息
 */
func (bf *BaseFunc) AllDataInfo() []*DataInfo {
	array := bf.BaseColumnNames()

	result := []*DataInfo{}
	for _, key := range array {
		result = append(result, bf.GetDataInfo(key))
	}

	return result
}

/**
 * 结构体映射表的字段名集合
 */
func (bf *BaseFunc) BaseColumnNames() []string {
	return []string{} //子类实现
}

/**
 * 取数据结构信息
 */
func (bf *BaseFunc) GetDataInfo(name string) *DataInfo {
	return nil //子类实现
}

/**
 * 创建结构实体集合,并赋予默认值
 */
func (bf *BaseFunc) NewList() interface{} {
	return nil //子类实现
}

/**
 * 设置默认值
 */
func (bf *BaseFunc) SetDefault() {
	//子类实现
}

// 取基础实体,用于在子类(嵌套结构体)时同样获得基类
func (bf *BaseFunc) BaseEntity() Entity {
	return bf.New() //子类实现
}

// 创建结构实体,并赋予默认值
func (bf *BaseFunc) New() Entity {
	return nil //子类实现
}
