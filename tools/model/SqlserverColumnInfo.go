package model

type SqlserverColumnInfo struct {
	GsTableName      string
	GiIndex          int
	GsField          string
	GsType           string
	GiTypeLength     int
	GisNull          int
	Giskey           int
	GsDefaultData    string
	GsExtra          string
	GsComment        string
	GsTypeByDelphi   string
	GsTypeByJava     string
	GsTableObj       string
	GsKeyColumnName  string
	GisDecimal       int
	GiIntegralLength int
	GiDecimalLength  int
}
