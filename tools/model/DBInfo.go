package model

import (
	"strings"

	"gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data"
)

/**
 * 数据库信息
 * @author HuangXinBian
 */
type DBInfo struct {
	Gsequence      string      `json:"sequence"`      //序列(每次查询都重建)
	GsName         string      `json:"sName"`         //库名
	GsDbCodeName   string      `json:"sDbCodeName"`   //数据库在代码中的应用名(去除标识如'dev_'等库前缀)
	GsSimplName    string      `json:"sSimplName"`    //简化库名 (去除标识如'dev_'、'Bricks'等库前缀)
	GsPath         string      `json:"sPath"`         //路径
	GsType         string      `json:"sType"`         //数据库类型
	GtableInfoList []TableInfo `json:"tableInfoList"` //表集合
}

/**
 * 初始化对象
 */
func (this *DBInfo) SetDefault() {
	this.Gsequence = data.UUIDUtil{}.Get()
	this.GtableInfoList = nil
}

/**
 * 设置 库名
 * @return sSimplName 简化库名
 */
func (this *DBInfo) SetsName(sName string) {
	sName = strings.TrimSpace(sName)

	this.GsName = sName

	temp := sName
	iSt := strings.Index(strings.ToUpper(sName), "DEV_")
	if iSt == 0 {
		temp = string([]byte(sName)[len("DEV_"):])

		this.GsDbCodeName = temp //数据库在代码中的应用名(去除标识如'dev_'等库前缀)
	}

	if strings.Index(strings.ToUpper(temp), "BRICKS") == 0 {
		temp = string([]byte(temp)[len("BRICKS"):])
	} else if strings.Index(strings.ToUpper(temp), "PUZZLES") == 0 {
		temp = string([]byte(temp)[len("PUZZLES"):])
	}

	this.GsSimplName = temp //数据库在代码中的应用名(去除标识如'dev_'、'Bricks'等库前缀)
}
