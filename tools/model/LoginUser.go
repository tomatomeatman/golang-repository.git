package model

//登录用户
type LoginUser struct {
	GsId   string `json:"sId"`
	GsPass string `json:"sPass"`
	GsKey  string `json:"sKey"`
}
