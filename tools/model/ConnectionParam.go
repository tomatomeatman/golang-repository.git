package model

type ConnectionParam struct {
	GsUrl    string `json:"sUrl"`
	GsUser   string `json:"sUser"`
	GsPass   string `json:"sPass"`
	GsDbName string `json:"sDbName"`
	GsType   string `json:"sType"`
	GsIp     string `json:"sIp"`
	GiPort   int    `json:"iPort"`
}
