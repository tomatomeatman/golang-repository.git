package model

import (
	"strings"
)

/**
 * 表统一字段信息
 * @author HuangXinBian
 */
type ColumnInfo struct {
	GsTableName        string            `json:"sTableName" gorm:"column:sTableName; type:varchar; DEFAULT:''"`           //字段所在表名
	GiIndex            int               `json:"iIndex" gorm:"column:iIndex; type:int; DEFAULT:'99'"`                     //序号
	GsField            string            `json:"sField" gorm:"column:sField; type:varchar; DEFAULT:''"`                   //在数据库中的名字段
	GsName             string            `json:"sName" gorm:"column:sName; type:varchar; DEFAULT:''"`                     //字段名(驼峰名称头字母小写)
	GsType             string            `json:"sType" gorm:"column:sType; type:varchar; DEFAULT:''"`                     //字段类型
	GiTypeLength       int               `json:"iTypeLength" gorm:"column:iTypeLength; type:int; DEFAULT:'1'"`            //字段长度
	GiNull             int               `json:"iNull" gorm:"column:iNull; type:int; DEFAULT:'1'"`                        //是否允许为空
	GiKey              int               `json:"iKey" gorm:"column:iKey; type:int; DEFAULT:'2'"`                          //是否主键
	GsDefaultData      string            `json:"sDefaultData" gorm:"column:sDefaultData; type:varchar; DEFAULT:''"`       //默认值
	GiExtra            int               `json:"iExtra" gorm:"column:iExtra; type:int; DEFAULT:'2'"`                      //是否自增长
	GsComment          string            `json:"sComment" gorm:"column:sComment; type:varchar; DEFAULT:''"`               //字段备注
	GsTypeByDelphi     string            `json:"sTypeByDelphi" gorm:"column:sTypeByDelphi; type:varchar; DEFAULT:''"`     //字段类型
	GsTypeByJava       string            `json:"sTypeByJava" gorm:"column:sTypeByJava; type:varchar; DEFAULT:''"`         //字段类型
	GsTableObj         string            `json:"sTableObj" gorm:"column:sTableObj; type:varchar; DEFAULT:''"`             //首字母小写的表名（用于java实体对象）
	GsKeyColumnName    string            `json:"sKeyColumnName" gorm:"column:sKeyColumnName; type:varchar; DEFAULT:''"`   //主键列明
	GiDecimal          int               `json:"iDecimal" gorm:"column:iDecimal; type:int; DEFAULT:'2'"`                  //类型是否有小数
	GiIntegralLength   int               `json:"iIntegralLength" gorm:"column:iIntegralLength; type:int; DEFAULT:''"`     //整数位的长度
	GiDecimalLength    int               `json:"iDecimalLength" gorm:"column:iDecimalLength; type:int; DEFAULT:''"`       //小数位的长度
	GsSimplComment     string            `json:"sSimplComment" gorm:"column:sSimplComment; type:varchar; DEFAULT:''"`     //字段简洁备注(去除括号的内容)
	GiDictionary       int               `json:"iDictionary" gorm:"column:iDictionary; type:int; DEFAULT:''"`             //是否字典
	GiDictionaryCommon int               `json:"iDictionaryCommon" gorm:"column:iDictionaryCommon; type:int; DEFAULT:''"` //通用字典
	GiEnum             int               `json:"iEnum" gorm:"column:iEnum; type:int; DEFAULT:''"`                         //枚举值
	GsEnumArray        map[string]string `json:"sEnumArray" gorm:"column:sEnumArray; type:varchaintr; DEFAULT:''"`        //枚举值集合
	GiBoolean          int               `json:"iBoolean" gorm:"column:iBoolean; type:int; DEFAULT:''"`                   //布尔值
	GiRelTable         int               `json:"iRelTable" gorm:"column:iRelTable; type:int; DEFAULT:''"`                 //关联表
	GsRelTableName     string            `json:"sRelTableName" gorm:"column:sRelTableName; type:varchar; DEFAULT:''"`     //关联表名(带库名)
	GsRelColumnValue   string            `json:"sRelColumnValue" gorm:"column:sRelColumnValue; type:varchar; DEFAULT:''"` //关联字段值
	GsRelColumnName    string            `json:"sRelColumnName" gorm:"column:sRelColumnName; type:varchar; DEFAULT:''"`   //关联字段名
	GsRelColumnAs      string            `json:"sRelColumnAs" gorm:"column:sRelColumnAs; type:varchar; DEFAULT:''"`       //关联后的别名
	GsNameObj          string            `json:"sNameObj"`                                                                //驼峰名称(头字母大写)
}

/**
 * 初始化对象
 */
func (this *ColumnInfo) SetDefault() {
	this.GiIndex = 999           //序号
	this.GiTypeLength = -1       //字段长度
	this.GiNull = -1             //是否允许为空
	this.GiKey = -1              //是否主键
	this.GsDefaultData = ""      //默认值
	this.GiExtra = -1            //是否自增长
	this.GiDecimal = -1          //类型是否有小数
	this.GiIntegralLength = -1   //整数位的长度
	this.GiDecimalLength = -1    //小数位的长度
	this.GiDictionary = -1       //是否字典
	this.GiDictionaryCommon = -1 //通用字典
	this.GiEnum = -1             //枚举值
	this.GsEnumArray = nil       //枚举值集合
	this.GiBoolean = -1          //布尔值
	this.GiRelTable = -1         //关联表
}

/**
 * 处理附属信息
 */
func (this *ColumnInfo) Supplementary() {
	this.GsName = hump(this.GsField, true)
	this.GsComment = strings.TrimSpace(this.GsComment)
	this.GsTableObj = hump(this.GsTableObj, true)
	this.GsSimplComment = this.getsSimplComment()
	this.GiDictionary = this.getiDictionary()
	this.GiDictionaryCommon = this.getiDictionaryCommon()
	this.GiEnum = this.getiEnum()
	this.GiBoolean = this.getiBoolean()
	this.GiRelTable = this.getiRelTable()
	this.GsRelTableName = this.getsRelTableName()
	this.GsRelColumnValue = this.getsRelColumnValue()
	this.GsRelColumnName = this.getsRelColumnName()
	this.GsRelColumnAs = this.getsRelColumnAs()
	this.GsEnumArray = this.getsEnumArray()
	this.GsNameObj = hump(this.GsField, false) //驼峰名称(头字母大写)
}

/**
 * 获得 字典
 * @return iDictionary 字典
 */
func (this ColumnInfo) getiDictionary() int {
	if "" == this.GsComment {
		return 0
	}

	if strings.Index(this.GsComment, "(字典") > -1 {
		return 1
	}

	return 0
}

/**
 * 获得 通用字典
 * @return iDictionaryCommon 通用字典
 */
func (this ColumnInfo) getiDictionaryCommon() int {
	if "" == this.GsComment {
		return 0
	}

	if strings.Index(this.GsComment, "(通用字典") > -1 {
		return 1
	}

	return 0
}

/**
 * 获得 枚举值
 * @return iEnum 枚举值
 */
func (this ColumnInfo) getiEnum() int {
	if "" == this.GsComment {
		return 0
	}

	if strings.Index(this.GsComment, "(枚举") > -1 {
		return 1
	}

	return 0
}

/**
 * 获得 布尔值
 * @return iBoolean 布尔值
 */
func (this ColumnInfo) getiBoolean() int {
	if "" == this.GsComment {
		return 0
	}

	if strings.Index(this.GsComment, "(布尔值") > -1 {
		return 1
	}

	return 0
}

/**
 * 获得 关联表
 * @return iRelTable 关联表
 */
func (this ColumnInfo) getiRelTable() int {
	if "" == this.GsComment {
		return 0
	}

	if strings.Index(this.GsComment, "(关联表") > -1 {
		return 1
	}

	return 0
}

/**
 * 获得 关联表名(带库名)
 * @return sRelTableName 关联表名(带库名)
 */
func (this ColumnInfo) getsRelTableName() string {
	if "" == this.GsComment {
		return ""
	}

	if "" == this.GsComment { //所属科室(关联表,|${BricksBaseSystem}Department|sId|sName|sDepartmentText|)
		return ""
	}

	iSt := strings.Index(this.GsComment, "(")
	iEd := strings.Index(this.GsComment, ")")
	if (iSt < 0) || (iEd < 0) || (iSt > iEd) {
		return ""
	}

	temp := this.GsComment[iSt+1 : iEd] //关联表,|${BricksBaseSystem}Department|sId|sName|sDepartmentText|

	array := strings.Split(temp, "|")
	if len(array) < 3 {
		return ""
	}

	return strings.TrimSpace(array[1])
}

/**
 * 获得 关联字段值
 * @return sRelColumnValue 关联字段值
 */
func (this ColumnInfo) getsRelColumnValue() string {
	if "" == this.GsComment {
		return ""
	}

	if "" == this.GsComment { //所属科室(关联表,|${BricksBaseSystem}Department|sId|sName|sDepartmentText|)
		return ""
	}

	iSt := strings.Index(this.GsComment, "(")
	iEd := strings.Index(this.GsComment, ")")
	if iSt < 0 || iEd < 0 || iSt > iEd {
		return ""
	}

	temp := this.GsComment[iSt+1 : iEd] //关联表,|${BricksBaseSystem}Department|sId|sName|sDepartmentText|

	array := strings.Split(temp, "|")
	if len(array) < 4 {
		return ""
	}

	return array[2]
}

/**
 * 获得 关联字段名
 * @return sRelColumnName 关联字段名
 */
func (this ColumnInfo) getsRelColumnName() string {
	if "" == this.GsComment {
		return ""
	}

	if "" == this.GsComment { //所属科室(关联表,|${BricksBaseSystem}Department|sId|sName|sDepartmentText|)
		return ""
	}

	iSt := strings.Index(this.GsComment, "(")
	iEd := strings.Index(this.GsComment, ")")
	if (iSt < 0) || (iEd < 0) || (iSt > iEd) {
		return ""
	}

	temp := this.GsComment[iSt+1 : iEd] //关联表,|${BricksBaseSystem}Department|sId|sName|sDepartmentText|

	array := strings.Split(temp, "|")
	if len(array) < 5 {
		return ""
	}

	return array[3]
}

/**
 * 获得 关联后的别名
 * @return sRelColumnAs 关联后的别名
 */
func (this ColumnInfo) getsRelColumnAs() string {
	if "" == this.GsComment {
		return ""
	}

	if "" == this.GsComment { //所属科室(关联表,|${BricksBaseSystem}Department|sId|sName|sDepartmentText|)
		return ""
	}

	iSt := strings.Index(this.GsComment, "(")
	iEd := strings.Index(this.GsComment, ")")
	if iSt < 0 || iEd < 0 || iSt > iEd {
		return ""
	}

	temp := this.GsComment[iSt+1 : iEd] //关联表,|${BricksBaseSystem}Department|sId|sName|sDepartmentText|

	array := strings.Split(temp, "|")
	if len(array) < 6 {
		return ""
	}

	return array[4]
}

/**
 * 获得 枚举值集合
 * @return
 */
func (this ColumnInfo) getsEnumArray() map[string]string {
	if "" == this.GsComment { //"固定资产(布尔值,1:是;0否)"
		return map[string]string{}
	}

	iSt := strings.Index(this.GsComment, "(")
	iEd := strings.Index(this.GsComment, ")")
	if (iSt < 0) || (iEd < 0) || (iSt > iEd) {
		return map[string]string{}
	}

	tempComment := this.GsComment[iSt+1 : iEd] //"布尔值,1:是;0否"

	array := strings.Split(tempComment, ",")
	if len(array) < 2 {
		return map[string]string{}
	}

	m := map[string]string{}
	array = strings.Split(array[1], ";")
	for _, val := range array {
		temp := strings.Split(val, ":")
		if len(temp) < 2 {
			m[temp[0]] = ""
			continue
		}

		m[temp[0]] = temp[1]
	}

	return m
}

/**
 * 获得 字段简洁备注(去除括号的内容)
 * @return sSimplComment 字段简洁备注(去除括号的内容)
 */
func (this ColumnInfo) getsSimplComment() string {
	if "" == this.GsComment {
		return ""
	}

	iEd := strings.Index(this.GsComment, "(")
	if iEd < 0 {
		return strings.TrimSpace(this.GsComment)
	}

	if this.GsEnumArray["xxx"] == "" {
	}

	return strings.TrimSpace(this.GsComment[0:iEd])
}

//-------------------------------------------------------------------------

// FirstUpper 字符串首字母大写
/**
* 字符串首字母大写
* @param str
* @return
 */
func firstUpperStr(s string) string {
	if s == "" {
		return ""
	}

	return strings.ToUpper(s[:1]) + s[1:]
}

/**
* 首字母小写
* @param str
* @return
 */
func firstLowerStr(str string) string {
	if str == "" {
		return ""
	}

	return strings.ToLower(str[:1]) + str[1:]
}

/**
* 转变驼峰字符串
* @param str 字符串
* @param firstLower 首字母小写
* @return
 */
func hump(str string, firstLower bool) string {
	str = strings.TrimSpace(str)
	if "" == str {
		return ""
	}

	if !strings.Contains(str, "_") && (str != strings.ToUpper(str)) && (str != strings.ToLower(str)) {
		if firstLower {
			return firstLowerStr(str) //不含下划线,不是全大写,不是全小写,则首字母小写后符合规则
		}

		return str
	}

	var build strings.Builder
	doUpper := true
	for _, ch := range str {
		if ch == 95 { //如果是下划线,则下一个字符转换大写,同时抛弃当前字符
			doUpper = true
			continue
		}

		if !doUpper { //转换大写标志未启动
			build.WriteString(strings.ToLower(string(ch)))
			continue
		}

		build.WriteString(strings.ToUpper(string(ch)))
		doUpper = false //下一个不转
	}

	if firstLower {
		return firstLowerStr(build.String()) //首字母小写
	}

	return build.String()
}

//-------------------------------------------------------------------------

// /**
//  * 初始化 '字段简洁备注(去除括号的内容)'
//  */
// func (this ColumnInfo) InitsSimplComment(sComment string) string {
// 	sComment = strings.TrimSpace(sComment)
// 	if "" == sComment {
// 		this.GsSimplComment = ""
// 		return ""
// 	}

// 	iEd := strings.Index(sComment, "(")
// 	if iEd < 0 {
// 		this.GsSimplComment = sComment
// 		return sComment
// 	}

// 	this.GsSimplComment = string([]byte(sComment)[:iEd])

// 	return this.GsSimplComment
// }

// /**
//  * 初始化 '是否字典'
//  */
// func (this ColumnInfo) InitiDictionary(sComment string) int {
// 	sComment = strings.TrimSpace(sComment)
// 	if "" == sComment {
// 		this.GiDictionary = 2
// 		return 2
// 	}

// 	iEd := strings.Index(sComment, "(字典")
// 	if iEd < 0 {
// 		this.GiDictionary = 2
// 		return 2
// 	}

// 	this.GiDictionary = 1
// 	return 1
// }

// /**
//  * 初始化 '是否通用字典'
//  */
// func (this ColumnInfo) getiDictionaryCommon(sComment string) int {
// 	sComment = strings.TrimSpace(sComment)
// 	if "" == sComment {
// 		this.GiDictionaryCommon = 2
// 		return 2
// 	}

// 	iEd := strings.Index(sComment, "(通用字典")
// 	if iEd < 0 {
// 		this.GiDictionaryCommon = 2
// 		return 2
// 	}

// 	this.GiDictionaryCommon = 1
// 	return 1
// }

// /**
//  * 初始化 '是否枚举值'
//  */
// func (this ColumnInfo) InitiEnum(sComment string) int {
// 	sComment = strings.TrimSpace(sComment)
// 	if "" == sComment {
// 		this.GiEnum = 2
// 		return 2
// 	}

// 	iEd := strings.Index(sComment, "(通用字典")
// 	if iEd < 0 {
// 		this.GiEnum = 2
// 		return 2
// 	}

// 	this.GiEnum = 1
// 	return 1
// }

// /**
//  * 初始化 '是否布尔值'
//  */
// func (this ColumnInfo) InitiBoolean(sComment string) int {
// 	sComment = strings.TrimSpace(sComment)
// 	if "" == sComment {
// 		this.GiBoolean = 2
// 		return 2
// 	}

// 	iEd := strings.Index(sComment, "(布尔值")
// 	if iEd < 0 {
// 		this.GiBoolean = 2
// 		return 2
// 	}

// 	this.GiBoolean = 1
// 	return 1
// }

// /**
//  * 初始化 '是否关联表'
//  */
// func (this ColumnInfo) InitiRelTable(sComment string) int {
// 	sComment = strings.TrimSpace(sComment)
// 	if "" == sComment {
// 		this.GiRelTable = 2
// 		return 2
// 	}

// 	iEd := strings.Index(sComment, "(关联表")
// 	if iEd < 0 {
// 		this.GiRelTable = 2
// 		return 2
// 	}

// 	this.GiRelTable = 1
// 	return 1
// }

// /**
//  * 初始化 '关联表名(带库名)'
//  */
// func (this ColumnInfo) InitsRelTableName(sComment string) string {
// 	sComment = strings.TrimSpace(sComment) //所属科室(关联表,|${BricksBaseSystem}Department|sId|sName|sDepartmentText|)
// 	if "" == sComment {
// 		this.GsRelTableName = ""
// 		return ""
// 	}

// 	iSt := strings.Index(sComment, "(")
// 	iEd := strings.Index(sComment, ")")
// 	if iSt < 0 || iEd < 0 || iSt > iEd {
// 		return ""
// 	}

// 	temp := string([]byte(sComment)[iSt+1 : iEd]) //关联表,|${BricksBaseSystem}Department|sId|sName|sDepartmentText|
// 	array := strings.Split(temp, "|")
// 	if len(array) < 3 {
// 		return ""
// 	}

// 	this.GsRelTableName = array[1]

// 	return this.GsRelTableName
// }

// /**
//  * 初始化 '关联字段值'
//  */
// func (this ColumnInfo) InitsRelColumnValue(sComment string) string {
// 	sComment = strings.TrimSpace(sComment) //所属科室(关联表,|${BricksBaseSystem}Department|sId|sName|sDepartmentText|)
// 	if "" == sComment {
// 		this.GsRelColumnValue = ""
// 		return ""
// 	}

// 	iSt := strings.Index(sComment, "(")
// 	iEd := strings.Index(sComment, ")")
// 	if iSt < 0 || iEd < 0 || iSt > iEd {
// 		return ""
// 	}

// 	temp := string([]byte(sComment)[iSt+1 : iEd]) //关联表,|${BricksBaseSystem}Department|sId|sName|sDepartmentText|
// 	array := strings.Split(temp, "|")
// 	if len(array) < 4 {
// 		return ""
// 	}

// 	this.GsRelColumnValue = array[2]

// 	return this.GsRelColumnValue
// }

// /**
//  * 初始化 '关联字段名'
//  */
// func (this ColumnInfo) InitsRelColumnName(sComment string) string {
// 	sComment = strings.TrimSpace(sComment) //所属科室(关联表,|${BricksBaseSystem}Department|sId|sName|sDepartmentText|)
// 	if "" == sComment {
// 		this.GsRelColumnName = ""
// 		return ""
// 	}

// 	iSt := strings.Index(sComment, "(")
// 	iEd := strings.Index(sComment, ")")
// 	if iSt < 0 || iEd < 0 || iSt > iEd {
// 		return ""
// 	}

// 	temp := string([]byte(sComment)[iSt+1 : iEd]) //关联表,|${BricksBaseSystem}Department|sId|sName|sDepartmentText|
// 	array := strings.Split(temp, "|")
// 	if len(array) < 5 {
// 		return ""
// 	}

// 	this.GsRelColumnName = array[3]

// 	return this.GsRelColumnName
// }

// /**
//  * 初始化 '关联后的别名'
//  */
// func (this ColumnInfo) InitsRelColumnAs(sComment string) string {
// 	sComment = strings.TrimSpace(sComment) //所属科室(关联表,|${BricksBaseSystem}Department|sId|sName|sDepartmentText|)
// 	if "" == sComment {
// 		this.GsRelColumnAs = ""
// 		return ""
// 	}

// 	iSt := strings.Index(sComment, "(")
// 	iEd := strings.Index(sComment, ")")
// 	if iSt < 0 || iEd < 0 || iSt > iEd {
// 		return ""
// 	}

// 	temp := string([]byte(sComment)[iSt+1 : iEd]) //关联表,|${BricksBaseSystem}Department|sId|sName|sDepartmentText|
// 	array := strings.Split(temp, "|")
// 	if len(array) < 5 {
// 		return ""
// 	}

// 	this.GsRelColumnAs = array[4]

// 	return this.GsRelColumnAs
// }

// /**
//  * 初始化 '枚举值集合'
//  */
// func (this ColumnInfo) InitsEnumArray(sComment string) map[string]string {
// 	sComment = strings.TrimSpace(sComment) //"固定资产(布尔值,1:是;0否)"
// 	if "" == sComment {
// 		this.GsEnumArray = nil
// 		return nil
// 	}

// 	iSt := strings.Index(sComment, "(")
// 	iEd := strings.Index(sComment, ")")
// 	if iSt < 0 || iEd < 0 || iSt > iEd {
// 		return nil
// 	}

// 	temp := string([]byte(sComment)[iSt+1 : iEd]) //"布尔值,1:是;0否"
// 	array := strings.Split(temp, ",")
// 	if len(array) < 2 {
// 		return nil
// 	}

// 	tempMap := make(map[string]string)

// 	array = strings.Split(array[1], ";")
// 	for _, item := range array {
// 		item = strings.TrimSpace(item)
// 		subItem := strings.Split(item, ":")
// 		if len(subItem) < 2 {
// 			tempMap[subItem[0]] = ""
// 			continue
// 		}

// 		tempMap[subItem[0]] = subItem[1]
// 	}

// 	if len(tempMap) < 1 {
// 		return nil
// 	}

// 	this.GsEnumArray = tempMap

// 	return this.GsEnumArray
// }

//-----------------------------------------------------
