cd /d %~dp0

::设置代理
SET GOPROXY=https://goproxy.cn
SET GONOSUMDB=gitee.com
SET GONOPROXY=gitee.com

for %%i in ("%cd%") do set dirName= %%~ni

go mod init %dirName%

go build -v -o %dirName%.exe %dirName%.go
go get gitee.com/tomatomeatman/golang-repository/bricks

pause
