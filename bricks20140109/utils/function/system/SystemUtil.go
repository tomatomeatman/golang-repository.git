package system

import (
	"fmt"
	_ "net/http/pprof"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/disk"
	"github.com/shirou/gopsutil/mem"
)

type SystemUtil struct{}

// 获取当前执行程序所在的绝对路径
func (this SystemUtil) AppPath() string {
	exePath, err := os.Executable()
	if err != nil {
		return "./"
	}

	res, _ := filepath.EvalSymlinks(filepath.Dir(exePath))

	return res
}

func (SystemUtil) GetCpuPercent() float64 {
	percent, _ := cpu.Percent(time.Second, false)
	return percent[0]
}

func (SystemUtil) GetMemPercent() float64 {
	memInfo, _ := mem.VirtualMemory()
	return memInfo.UsedPercent
}

func (SystemUtil) GetDiskPercent(p int) float64 {
	parts, _ := disk.Partitions(true)
	diskInfo, _ := disk.Usage(parts[p].Mountpoint)
	return diskInfo.UsedPercent
}

func (SystemUtil) GetDiskTotal(p int) uint64 {
	parts, _ := disk.Partitions(true)
	diskInfo, _ := disk.Usage(parts[p].Mountpoint)
	return diskInfo.Total
}

func (SystemUtil) GetDiskFree(p int) uint64 {
	parts, _ := disk.Partitions(true)
	diskInfo, _ := disk.Usage(parts[p].Mountpoint)
	return diskInfo.Free
}

// 取内存信息
func (this SystemUtil) GetMemInfo() map[string]interface{} {
	memInfo, _ := mem.VirtualMemory()

	result := map[string]interface{}{
		"iTotal":   memInfo.Total,
		"iFree":    memInfo.Free,
		"iUsed":    memInfo.Used,
		"fPercent": memInfo.Used,
		"sTotal":   this.memoryTxt(memInfo.Total),
		"sFree":    this.memoryTxt(memInfo.Free),
		"sUsed":    this.memoryTxt(memInfo.Used),
		"sPercent": fmt.Sprintf("%.2f%", memInfo.UsedPercent),
	}

	return result
}

// 程序内存信息
func (this SystemUtil) AppMemoryInfo() map[string]interface{} {
	result := map[string]interface{}{}

	var m runtime.MemStats
	runtime.ReadMemStats(&m)

	result["iAppFree"] = (m.Sys - m.HeapAlloc)
	result["iAppTotal"] = m.Sys
	result["iAppUsage"] = m.HeapAlloc
	result["fUsagePercent"] = m.HeapAlloc / m.Sys
	result["sAppFree"] = this.memoryTxt(m.Sys - m.HeapAlloc)
	result["sAppTotal"] = this.memoryTxt(m.Sys)
	result["sAppUsage"] = this.memoryTxt(m.HeapAlloc)
	result["sUsagePercent"] = fmt.Sprintf("%.2f%", m.HeapAlloc/m.Sys)

	return result
}

// 取磁盘信息
func (this SystemUtil) GetDiskInfo() []map[string]interface{} {
	result := []map[string]interface{}{}

	parts, _ := disk.Partitions(true)
	for i := 0; i < len(parts); i++ {
		diskInfo, err := disk.Usage(parts[i].Mountpoint)
		if err != nil {
			break
		}

		m := map[string]interface{}{}
		m["iTotal"] = diskInfo.Total
		m["iFree"] = diskInfo.Free
		m["iUsed"] = diskInfo.Used
		m["fPercent"] = diskInfo.UsedPercent
		m["sTotal"] = this.memoryTxt(diskInfo.Total)
		m["sFree"] = this.memoryTxt(diskInfo.Free)
		m["sUsed"] = this.memoryTxt(diskInfo.Used)
		m["sPercent"] = fmt.Sprintf("%.2f%", diskInfo.UsedPercent)

		result = append(result, m)
	}

	return result
}

// 存储显示
func (SystemUtil) memoryTxt(b uint64) string {
	temp := b
	array := []string{"bit", "Byte", "MB", "GB", "TB"}
	for i := 0; i < len(array); i++ {
		if (temp / 1024) < 1 {
			return fmt.Sprintf("%v", temp) + array[i]
		}

		temp = temp / 1024
	}

	return fmt.Sprintf("%vbyte", temp)
}

// 取CPU信息
func (SystemUtil) GetCpuInfo() map[string]interface{} {
	result := map[string]interface{}{}

	percent, _ := cpu.Percent(time.Second, false)

	result["percent"] = percent
	result["count"] = runtime.GOMAXPROCS(0)

	return result
}

// 取系统信息
func (this SystemUtil) GetOsInfo() map[string]interface{} {
	result := map[string]interface{}{}

	name, err := os.Hostname()
	if err == nil {
		result["sName"] = name
	}

	result["sOs"] = runtime.GOOS
	result["sArch"] = runtime.GOARCH //当前的系统架构 architecture

	return result
}

// GetCPUID 获取cpuid
func (this SystemUtil) GetCPUID() string {
	var cpuid string
	cmd := exec.Command("wmic", "cpu", "get", "processorid")
	b, e := cmd.CombinedOutput()

	if e != nil {
		return ""
	}

	cpuid = string(b)
	cpuid = cpuid[12 : len(cpuid)-2]
	cpuid = strings.ReplaceAll(cpuid, "\n", "")

	return cpuid
}
