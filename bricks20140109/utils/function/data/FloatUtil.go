package data

import (
	"fmt"
	"strconv"
	"unsafe"
)

type FloatUtil struct{}

//对象(字符串)转浮点数
func (this FloatUtil) ToStr(obj float64) string {
	return fmt.Sprintf("%f", obj)
}

//对象(字符串)转64整型
func (this FloatUtil) ToFloat(obj interface{}, bitSize int, iDefault float64) float64 {
	var str string
	switch obj.(type) {
	case []uint8:
		str = this.Byte2Str(obj.([]uint8))
	default:
		str = fmt.Sprintf("%v", obj)
	}

	if str == "" { //字符串不能判断nil
		return iDefault
	}

	result, err := strconv.ParseFloat(str, bitSize)
	if err != nil {
		return iDefault
	}

	return result
}

// Byte转Str
func (this FloatUtil) Byte2Str(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}
