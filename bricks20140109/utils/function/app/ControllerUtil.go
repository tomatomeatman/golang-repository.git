package app

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"reflect"
	"strings"
	"time"

	. "gitee.com/tomatomeatman/golang-repository/bricks/model"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/system"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/url"
	Log "github.com/cihub/seelog"
	"github.com/gin-gonic/gin"
)

type ControllerUtil struct{}

/**
 * 新增
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) Add(ctx *gin.Context, control interface{}) *MsgEmity {
	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例

	me := UrlUtil{}.GetParams(ctx, entity)
	if !me.Gsuccess {
		return me
	}

	params := me.Gdata.(map[string]interface{})
	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".Add"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "Add", ctx, entity, params)

	temp := result[0].Interface()
	me = temp.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 批量新增
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) Adds(ctx *gin.Context, control interface{}) *MsgEmity {
	objStr := UrlUtil{}.GetParam(ctx, "objs", "").(string)
	if "" == objStr {
		return MsgEmity{}.Err(9001, "未能获取'objs'参数")
	}

	var objs []map[string]interface{}
	err := json.Unmarshal([]byte(objStr), &objs)
	if err != nil {
		Log.Error("参数'objs'转换出错：", err)
		return MsgEmity{}.Err(9002, "参数'objs'转换出错")
	}

	if len(objs) < 1 {
		return MsgEmity{}.Err(9003, "参数'objs'转换后没有数据")
	}

	entitys := []interface{}{}
	for i := 0; i < len(objs); i++ {
		entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例
		entitys = append(entitys, entity)
	}

	service := ModuleUtil{}.GetModuleService(control)
	name := ModuleUtil{}.GetSimplName(control) + ".Adds"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "Adds", ctx, entitys, objs)

	temp := result[0].Interface()
	me := temp.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 删除
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) Del(ctx *gin.Context, control interface{}) *MsgEmity {
	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例

	tableInfo := TableInfo{}.GetByEntity(entity)
	id := UrlUtil{}.GetParamToId(ctx, tableInfo.GsKeyName) //取请求参数中的记录编号

	if (nil == id) || ("" == fmt.Sprintf("%v", id)) {
		id = UrlUtil{}.GetParamToId(ctx, "id")
	}

	iVersion := UrlUtil{}.GetParamToVersion(ctx, tableInfo.GbHasVersion) //取请求参数中的版本号

	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".Del"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "Del", ctx, entity, id, iVersion)

	temp := result[0].Interface()
	me := temp.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 修改
 * @param entity 对象类型
 * @param id 记录编号值
 * @param iVersion 记录版本号
 * @param data 待更新的字段和值
 * @return MsgEmity
 */
func (controlUtil ControllerUtil) Edit(ctx *gin.Context, control interface{}) *MsgEmity {
	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例

	tableInfo := TableInfo{}.GetByEntity(entity)
	id := UrlUtil{}.GetParamToId(ctx, tableInfo.GsKeyName)               //取请求参数中的记录编号
	iVersion := UrlUtil{}.GetParamToVersion(ctx, tableInfo.GbHasVersion) //取请求参数中的版本号

	me := UrlUtil{}.GetParams(ctx, entity)
	if !me.Gsuccess {
		return me
	}

	data := me.Gdata.(map[string]interface{})
	if id == nil || "" == id {
		id = data[tableInfo.GsKeyName]
	}

	if -1 == iVersion {
		if val, ok := data["iVersion"]; ok {
			iVersion = val.(int)
		}
	}

	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".Edit"

	me = AopUtil{}.CallBeforeFunc(name, ctx)
	if !me.Gsuccess {
		return me
	}

	result := ReflectUtils{}.DoMethod(service, "Edit", ctx, entity, id, iVersion, data)

	temp := result[0].Interface()
	me = temp.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 根据主键查询数据
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) FindById(ctx *gin.Context, control interface{}) *MsgEmity {
	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例

	tableInfo := TableInfo{}.GetByEntity(entity)
	id := UrlUtil{}.GetParamToId(ctx, tableInfo.GsKeyName) //取请求参数中的记录编号

	if (nil == id) || ("" == fmt.Sprintf("%v", id)) {
		id = UrlUtil{}.GetParamToId(ctx, "id")
	}

	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".FindById"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "FindById", ctx, entity, id)

	temp := result[0].Interface()
	me := temp.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 查询所有数据
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) FindAll(ctx *gin.Context, control interface{}) *MsgEmity {
	where := map[string]interface{}{} //map[string]interface{}{"sCreator": "00000001", "iState": 1}

	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例
	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".FindAll"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "FindAll", ctx, entity, where)

	temp := result[0].Interface()
	me := temp.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 查询时间范围内数据
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) FindByDate(ctx *gin.Context, control interface{}) *MsgEmity {
	sDateSt := UrlUtil{}.GetParam(ctx, "sDateSt", "").(string) //记录开始时间
	sDateEd := UrlUtil{}.GetParam(ctx, "sDateEd", "").(string) //记录结束时间

	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例
	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".FindByDate"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "FindByDate", ctx, entity, sDateSt, sDateEd)

	temp := result[0].Interface()
	me := temp.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 查找指定行数据
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) FindByRow(ctx *gin.Context, control interface{}) *MsgEmity {
	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例

	tableInfo := TableInfo{}.GetByEntity(entity)
	id := UrlUtil{}.GetParamToId(ctx, tableInfo.GsKeyName) //取请求参数中的记录编号

	if (nil == id) || ("" == fmt.Sprintf("%v", id)) {
		id = UrlUtil{}.GetParamToId(ctx, "id")
	}

	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".FindByRow"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "FindByRow", ctx, entity, id)

	temp := result[0].Interface()
	me := temp.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 查询分页数据
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) FindByPage(ctx *gin.Context, control interface{}) *MsgEmity {
	me := UrlUtil{}.GetBody(ctx, FindByPageParam{})
	if !me.Gsuccess {
		return me
	}

	findByPageParam := me.Gdata.(FindByPageParam)

	if findByPageParam.Gpage.GiSize < 1 {
		findByPageParam.Gpage.GiSize = 10
	}

	if findByPageParam.Gpage.GiCurrent < 1 {
		findByPageParam.Gpage.GiCurrent = 1
	}

	sLikeStr := strings.TrimSpace(findByPageParam.GsLikeStr) //全文检索条件
	if "" != sLikeStr {                                      //存在全文检索条件则需要考虑时间范围的问题
		sLikeDateSt := strings.TrimSpace(findByPageParam.GsLikeDateSt) //模糊查询记录修改时间范围条件-开始
		sLikeDateEd := strings.TrimSpace(findByPageParam.GsLikeDateEd) //模糊查询记录修改时间范围条件-结束

		var dLikeDateEd time.Time
		if "" == sLikeDateEd { //如果结束时间为空,则当前时间就是结束时间
			dLikeDateEd = time.Now()
			sLikeDateEd = dLikeDateEd.Format("2006-01-02 15:04:05")
		} else {
			dLikeDateEd = TimeUtil{}.ToDate(sLikeDateEd)
		}

		if "" == sLikeDateSt { //如果开始时间为空,则用结束时间-时间限制
			iLikeTimeLimit := ModuleUtil{}.GetLikeTimeLimit(control)
			sLikeDateSt = TimeUtil{}.AddDay(dLikeDateEd, -iLikeTimeLimit).Format("2006-01-02 15:04:05")
		}

		findByPageParam.GsLikeDateSt = sLikeDateSt
		findByPageParam.GsLikeDateEd = sLikeDateEd
	}

	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例
	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".FindByPage"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "FindByPage", ctx, entity, findByPageParam)

	temp := result[0].Interface()
	me = temp.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 取所有参数,并转换成对应实体属性类型(map[string]interface{})
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) GetParams(ctx *gin.Context, entity interface{}) *MsgEmity {
	if reflect.TypeOf(entity).Kind() == reflect.Struct {
		br, _ := io.ReadAll(ctx.Request.Body)
		ctx.Request.Body.Close()
		ctx.Request.Body = io.NopCloser(bytes.NewBuffer(br))
		json.NewDecoder(bytes.NewBuffer(br)).Decode(entity)

		fmt.Println(reflect.TypeOf(entity).String()) // map[string]interface {}

		return MsgEmity{}.Success(entity, "转换结束")
	}

	return UrlUtil{}.GetParams(ctx, entity)
}

/**
 * 读取树形结构数据
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) FindByTree(ctx *gin.Context, control interface{}) *MsgEmity {
	sGroupColumn := UrlUtil{}.GetParam(ctx, "sGroupColumn", "").(string)
	sGroupName := UrlUtil{}.GetParam(ctx, "sGroupName", "").(string)
	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例
	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".FindByTree"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "FindByTree", ctx, entity, sGroupColumn, sGroupName)

	temp := result[0].Interface()
	me := temp.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 根据字段名取指定记录编号的数据库表中对应字段的值
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) GetValueByFieldName(ctx *gin.Context, control interface{}) *MsgEmity {
	temp := UrlUtil{}.GetParam(ctx, "fieldNames", "").(string)
	fieldNames := strings.Split(temp, ",")

	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例
	tableInfo := TableInfo{}.GetByEntity(entity)
	id := UrlUtil{}.GetParamToId(ctx, tableInfo.GsKeyName) //取请求参数中的记录编号
	if (nil == id) || ("" == fmt.Sprintf("%v", id)) {
		id = UrlUtil{}.GetParamToId(ctx, "id")
	}

	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".GetValueByFieldName"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "GetValueByFieldName", ctx, entity, id, fieldNames)

	t := result[0].Interface()
	me := t.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 根据字段名取指定记录编号的数据库表中对应字段的值
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) GetValueByField(ctx *gin.Context, control interface{}) *MsgEmity {
	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例
	tableInfo := TableInfo{}.GetByEntity(entity)
	id := UrlUtil{}.GetParamToId(ctx, tableInfo.GsKeyName) //取请求参数中的记录编号
	if (nil == id) || ("" == fmt.Sprintf("%v", id)) {
		id = UrlUtil{}.GetParamToId(ctx, "id")
	}

	fieldName := UrlUtil{}.GetParam(ctx, "fieldName", "").(string)

	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".GetValueByField"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "GetValueByField", ctx, entity, id, fieldName)

	temp := result[0].Interface()
	me := temp.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 根据关键值取对象集合
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) FindByKey(ctx *gin.Context, control interface{}) *MsgEmity {
	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例

	me := UrlUtil{}.GetParams(ctx, entity)
	if !me.Gsuccess {
		return me
	}

	where := me.Gdata.(map[string]interface{})

	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".FindByKey"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "FindByKey", ctx, entity, where)

	temp := result[0].Interface()
	me = temp.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 根据关键值取对象集合中的第一个
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) FindOneByKey(ctx *gin.Context, control interface{}) *MsgEmity {
	temp := UrlUtil{}.GetParam(ctx, "fields", "").(string)
	fields := strings.Split(temp, ",")

	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例

	me := UrlUtil{}.GetParams(ctx, entity)
	if !me.Gsuccess {
		return me
	}

	where := me.Gdata.(map[string]interface{})

	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".FindOneByKey"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "FindOneByKey", ctx, entity, where, fields)

	t := result[0].Interface()
	me = t.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 根据关键值取对象集合中的符合条件的第一条记录的指定字段
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) FindValueByKey(ctx *gin.Context, control interface{}) *MsgEmity {
	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例

	me := UrlUtil{}.GetParams(ctx, entity)
	if !me.Gsuccess {
		return me
	}

	where := me.Gdata.(map[string]interface{})
	fieldName := UrlUtil{}.GetParam(ctx, "fieldName", "").(string)

	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".FindValueByKey"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "FindValueByKey", ctx, entity, where, fieldName)

	t := result[0].Interface()
	me = t.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 根据关键值查数量
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) FindCountByKey(ctx *gin.Context, control interface{}) *MsgEmity {
	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例

	me := UrlUtil{}.GetParams(ctx, entity)
	if !me.Gsuccess {
		return me
	}

	where := me.Gdata.(map[string]interface{})

	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".FindCountByKey"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "FindCountByKey", entity, where)

	temp := result[0].Interface()
	me = temp.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 根据字段名取分组数据
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) GroupByField(ctx *gin.Context, control interface{}) *MsgEmity {
	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例

	me := UrlUtil{}.GetParams(ctx, entity)
	if !me.Gsuccess {
		return me
	}

	fields := me.Gdata.(map[string]interface{})

	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".GroupByField"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "GroupByField", ctx, entity, "", fields)

	temp := result[0].Interface()
	me = temp.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 取表中指定字段的最大值
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) MaxByField(ctx *gin.Context, control interface{}) *MsgEmity {
	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例

	me := UrlUtil{}.GetParams(ctx, entity)
	if !me.Gsuccess {
		return me
	}

	where := me.Gdata.(map[string]interface{})
	field := UrlUtil{}.GetParams(ctx, "field")

	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".MaxByField"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "MaxByField", ctx, entity, "", field, where)

	temp := result[0].Interface()
	me = temp.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 取表中指定字段的最小值
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) MinByField(ctx *gin.Context, control interface{}) *MsgEmity {
	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例

	me := UrlUtil{}.GetParams(ctx, entity)
	if !me.Gsuccess {
		return me
	}

	where := me.Gdata.(map[string]interface{})
	field := UrlUtil{}.GetParams(ctx, "field")

	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".MinByField"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "MinByField", ctx, entity, "", field, where)

	temp := result[0].Interface()
	me = temp.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 取表中指定字段的最小值
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) HasById(ctx *gin.Context, control interface{}) *MsgEmity {
	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例

	tableInfo := TableInfo{}.GetByEntity(entity)
	id := UrlUtil{}.GetParamToId(ctx, tableInfo.GsKeyName) //取请求参数中的记录编号
	if (nil == id) || ("" == fmt.Sprintf("%v", id)) {
		id = UrlUtil{}.GetParamToId(ctx, "id")
	}

	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".HasById"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "HasById", ctx, entity, id)

	temp := result[0].Interface()
	me := temp.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 取表中指定字段的最小值
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) HasByKey(ctx *gin.Context, control interface{}) *MsgEmity {
	keyName := UrlUtil{}.GetParam(ctx, "keyName", "")   //取请求参数中的字段名
	keyValue := UrlUtil{}.GetParam(ctx, "keyValue", "") //取请求参数中的字段值

	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例
	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".HasByKey"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "HasByKey", ctx, entity, keyName, keyValue)

	temp := result[0].Interface()
	me := temp.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 清理指定用户的缓存
 * ctx GinHttp上下文对象
 * control 控制层
 */
func (controlUtil ControllerUtil) ClearCache(ctx *gin.Context, control interface{}) *MsgEmity {
	sUser := UrlUtil{}.GetParam(ctx, "sUser", "").(string)         //取请求参数中的用户名
	cacheName := UrlUtil{}.GetParam(ctx, "cacheName", "").(string) //取请求参数中的缓存名

	if "" == cacheName {
		return MsgEmity{}.Err(8001, "指定'缓存库名称'参数为空！")
	}

	if "" == sUser {
		cacheName = cacheName + sUser
	}

	if (GlobalVariable{}.Del(cacheName)) {
		return MsgEmity{}.Success(8999, "清理成功！")
	}

	return MsgEmity{}.Err(8002, "清理失败！")
}

/**
 * 查询组结构数据
 * ctx GinHttp上下文对象
 * control 控制层
 * @return
 */
func (controlUtil ControllerUtil) FindByGroup(ctx *gin.Context, control interface{}) *MsgEmity {
	sGroupColumn := UrlUtil{}.GetParam(ctx, "sGroupColumn", "").(string) //分组名(树节点)所在字段名
	sGroupName := UrlUtil{}.GetParam(ctx, "sGroupName", "").(string)     //分组名(树节点)

	if "" == sGroupName {
		return controlUtil.FindByTree(ctx, control)
	}

	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例
	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".FindByGroup"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "FindByGroup", ctx, entity, sGroupColumn, sGroupName)

	temp := result[0].Interface()
	me := temp.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

/**
 * 添加数据到指定组下
 * 警告:对象必须符合树形结构要求,如:sId、sPid
 * ctx GinHttp上下文对象
 * control 控制层
 * @param sGroupName 分组字段名称(树节点)
 * @param sGroupValue 分组字段值(树节点)
 * @param entity 实体对象
 * @return
 */
func (controlUtil ControllerUtil) AddToGroup(ctx *gin.Context, control interface{}) *MsgEmity {
	sGroupName := UrlUtil{}.GetParam(ctx, "sGroupName", "").(string)   //分组字段名称(树节点)
	sGroupValue := UrlUtil{}.GetParam(ctx, "sGroupValue", "").(string) //分组字段值(树节点)

	entity := ModuleUtil{}.NowModuleEntity(control) //按模块数据实体创建新实例
	service := ModuleUtil{}.GetModuleService(control)

	name := ModuleUtil{}.GetSimplName(control) + ".AddToGroup"

	meBefore := AopUtil{}.CallBeforeFunc(name, ctx)
	if !meBefore.Gsuccess {
		return meBefore
	}

	result := ReflectUtils{}.DoMethod(service, "AddToGroup", ctx, entity, sGroupName, sGroupValue)

	temp := result[0].Interface()
	me := temp.(*MsgEmity)
	if !me.Gsuccess {
		return me
	}

	meAfter := AopUtil{}.CallAfterFunc(name, ctx, me)
	if !meAfter.Gsuccess {
		return meAfter
	}

	return me
}

//-----------------------------------------------//
