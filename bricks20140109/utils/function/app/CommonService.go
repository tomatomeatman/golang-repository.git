package app

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"

	. "gitee.com/tomatomeatman/golang-repository/bricks/model"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/data"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/system"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/gorm"
	Log "github.com/cihub/seelog"
	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
)

var (
	recordKeyJam = "" //创建sRecordKey用的干扰串
)

type CommonService struct{}

/**
 * 新增
 * entity 检查用数据结构
 * data 数据
 */
func (service CommonService) Add(ctx *gin.Context, entity interface{}, data map[string]interface{}) *MsgEmity {
	// if (ReflectUtils{}.HasField(entity, "GsPid")) {
	// 	return service.AddNode(ctx, entity, data)
	// }

	return service.AddCommon(ctx, entity, data)
}

/**
 * 新增普通数据
 * entity 检查用数据结构
 * data 数据
 */
func (service CommonService) AddCommon(ctx *gin.Context, entity interface{}, data map[string]interface{}) *MsgEmity {
	StringUtil{}.TrimAttribute(data) //清除对象各个属性的值的前后空格

	//MapUtil{}.RemoveData(data, []string{"uId", "sId", "iId", "sCreator", "sModifieder", "dCreateDate", "dModifiedDate"}) //清理不能由前端定义的字段
	MapUtil{}.RemoveData(data, []string{TableMajorKeyString, TableMajorKeyAutoInt, TableMajorKeyUuId, TableCreatorName, TableCreateDateName, TableModifiederName, TableModifiedDateName, TableVersionName}) //清理不能由前端定义的字段

	//me := DataInfo{}.ValidAttr(data, entity, []string{"uId", "sId", "iId", "sCreator", "sModifieder", "dCreateDate", "dModifiedDate"}) //对对象中添加了DataInfo注解的不为nil的属性检查限制
	me := DataInfo{}.ValidAttr(data, entity, []string{TableMajorKeyString, TableMajorKeyAutoInt, TableMajorKeyUuId, TableCreatorName, TableCreateDateName, TableModifiederName, TableModifiedDateName}) //对对象中添加了DataInfo注解的不为nil的属性检查限制
	if !me.Gsuccess {
		return me.IncCode(7000)
	}

	me = DataInfo{}.MapToEntity(data, entity)
	if !me.Gsuccess {
		return me.IncCode(7010)
	}

	commons := me.Gdata
	DataInfo{}.SetDataInfoDefault(commons) //对对象中添加了dataInfo注解的属性添加默认值

	me = service.SupplyDbEntityAttrByAdd(ctx, commons, "", "")
	if !me.Gsuccess {
		return me.IncCode(7030)
	}

	me = DataInfo{}.ValidAttrByAdd(commons, []string{}) //对对象中添加了DataInfo注解的属性检查限制
	if !me.Gsuccess {
		return me.IncCode(7020)
	}

	me = service.ValidEntityRepeatByAdd(ctx, commons) //验证新增数据是否存在重复
	if !me.Gsuccess {
		return me.IncCode(7030)
	}

	return CommonDao{}.AddCommon(commons)
}

/**
 * 新增树节点
 * entity 检查用数据结构
 * data 数据
 */
func (service CommonService) AddNode(ctx *gin.Context, entity interface{}, data map[string]interface{}) *MsgEmity {
	StringUtil{}.TrimAttribute(data) //清除对象各个属性的值的前后空格

	//MapUtil{}.RemoveData(data, []string{"uId", "sId", "iId", "sCreator", "sModifieder", "dCreateDate", "dModifiedDate"}) //清理不能由前端定义的字段
	MapUtil{}.RemoveData(data, []string{TableMajorKeyString, TableMajorKeyAutoInt, TableMajorKeyUuId, TableCreatorName, TableCreateDateName, TableModifiederName, TableModifiedDateName, TableVersionName}) //清理不能由前端定义的字段

	//me := DataInfo{}.ValidAttr(data, entity, []string{"uId", "sId", "iId", "sCreator", "sModifieder", "dCreateDate", "dModifiedDate"}) //对对象中添加了DataInfo注解的不为nil的属性检查限制
	me := DataInfo{}.ValidAttr(data, entity, []string{TableMajorKeyString, TableMajorKeyAutoInt, TableMajorKeyUuId, TableCreatorName, TableCreateDateName, TableModifiederName, TableModifiedDateName}) //对对象中添加了DataInfo注解的不为nil的属性检查限制
	if !me.Gsuccess {
		return me.IncCode(7000)
	}

	me = DataInfo{}.MapToEntity(data, entity)
	if !me.Gsuccess {
		return me.IncCode(7010)
	}

	commons := me.Gdata
	DataInfo{}.SetDataInfoDefault(commons) //对对象中添加了dataInfo注解的属性添加默认值

	me = service.SupplyDbEntityAttrByAdd(ctx, commons, "", "")
	if !me.Gsuccess {
		return me.IncCode(7030)
	}

	me = DataInfo{}.ValidAttrByAdd(commons, []string{}) //对对象中添加了DataInfo注解的属性检查限制
	if !me.Gsuccess {
		return me.IncCode(7020)
	}

	me = service.ValidEntityRepeatByAdd(ctx, commons) //验证新增数据是否存在重复
	if !me.Gsuccess {
		return me.IncCode(7030)
	}

	return CommonDao{}.AddNode(commons)
}

// 批量新增
func (service CommonService) Adds(ctx *gin.Context, entitys []interface{}, list []map[string]interface{}) *MsgEmity {
	if len(list) < 1 {
		return MsgEmity{}.Err(8001, "没有需要保存的数据")
	}

	entitysNew := []interface{}{}
	for k, data := range list {
		StringUtil{}.TrimAttribute(data) //清除对象各个属性的值的前后空格

		entity := entitys[k]

		MapUtil{}.RemoveData(data, []string{TableMajorKeyString, TableMajorKeyAutoInt, TableMajorKeyUuId, TableCreatorName, TableCreateDateName, TableModifiederName, TableModifiedDateName, TableVersionName}) //清理不能由前端定义的字段

		me := DataInfo{}.ValidAttr(data, entity, []string{TableMajorKeyString, TableMajorKeyAutoInt, TableMajorKeyUuId, TableCreatorName, TableCreateDateName, TableModifiederName, TableModifiedDateName}) //对对象中添加了DataInfo注解的不为nil的属性检查限制
		if !me.Gsuccess {
			return me.IncCode(7000)
		}

		me = DataInfo{}.MapToEntity(data, entity)
		if !me.Gsuccess {
			return me.IncCode(7010)
		}

		commons := me.Gdata
		DataInfo{}.SetDataInfoDefault(commons) //对对象中添加了dataInfo注解的属性添加默认值

		me = service.SupplyDbEntityAttrByAdd(ctx, commons, "", "")
		if !me.Gsuccess {
			return me.IncCode(7030)
		}

		me = DataInfo{}.ValidAttrByAdd(commons, []string{}) //对对象中添加了DataInfo注解的属性检查限制
		if !me.Gsuccess {
			return me.IncCode(7020)
		}

		me = service.ValidEntityRepeatByAdd(ctx, commons) //验证新增数据是否存在重复
		if !me.Gsuccess {
			return me.IncCode(7030)
		}

		entitysNew = append(entitysNew, commons)
	}

	return CommonDao{}.Adds(entitysNew)
}

/**
 * 修改状态
 * @param entity 实体类
 * @param id 编号
 * @param iState 状态值
 * @param iVersion 记录版本号
 * @param sMemo 备注
 * @param unidirectional 是否单向 设置为单向则新值必须大于旧值才能执行
 * @return MsgEmity 返回执行情况
 */
func (service CommonService) ChangeState(ctx *gin.Context, entity interface{}, id interface{}, iState int, iVersion int, sMemo string, unidirectional bool) *MsgEmity {
	if "" == fmt.Sprintf("%v", id) {
		return MsgEmity{}.Err(8001, "记录编号参数为空！")
	}

	return CommonDao{}.ChangeState(entity, id, iState, iVersion, sMemo, unidirectional)
}

/**
 * 修改步骤值(如果设置为单向则新值必须大于旧值)
 * @param id 编号
 * @param iSetp 步骤值
 * @param iVersion 记录版本号
 * @param sMemo 备注
 * @param unidirectional 是否单向 设置为单向则新值必须大于旧值才能执行
 * @param entity 实体类
 * @return MsgEmity 返回执行情况
 */
func (service CommonService) ChangeSetp(ctx *gin.Context, entity interface{}, id interface{}, iSetp int, iVersion int, sMemo string, unidirectional bool) *MsgEmity {
	if "" == fmt.Sprintf("%v", id) {
		return MsgEmity{}.Err(8001, "记录编号参数为空！")
	}

	return CommonDao{}.ChangeSetp(entity, id, iSetp, iVersion, sMemo, unidirectional)
}

/**
 * 删除
 * @param entity 对象类型
 * @param id 记录编号值
 * @param iVersion 记录版本号
 * @return MsgEmity
 */
func (service CommonService) Del(ctx *gin.Context, entity interface{}, id interface{}, iVersion int) *MsgEmity {
	if id == nil {
		return MsgEmity{}.Err(8001, "记录编号为空")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	if tableInfo.GbHasVersion && (iVersion < 1) && (iVersion != IntegerUtil{}.MaxInt()) {
		return MsgEmity{}.Err(8002, "记录版本号不正确")
	}

	onlyCreator := !ModuleUtil{}.EnableTag(tableInfo.GsTableName, 6) //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆

	return CommonDao{}.Del(entity, id, iVersion, ModuleUtil{}.CurrentLoginUserId(ctx), onlyCreator)
}

/**
 * 按实体保留map中的数据
 * object 待检查对象
 * data 数据
 * fieldPrefix 字段前缀(可不传)
 */
func (service CommonService) HoldByEntity(entity interface{}, data map[string]interface{}, fieldPrefix ...string) map[string]interface{} {
	var rve reflect.Type
	typeOf := reflect.TypeOf(entity)  //通过反射获取type定义
	if typeOf.Kind() == reflect.Ptr { //是否指针类型
		rve = reflect.ValueOf(entity).Elem().Type() // 取得struct变量的指针
	} else if "reflect.Value" == typeOf.String() {
		rve = entity.(reflect.Value).Type()
	} else {
		rve = reflect.ValueOf(entity).Type()
	}

	prefix := ""
	for _, key := range fieldPrefix {
		prefix = prefix + key
	}

	for key, _ := range data {
		str := key
		if "" != prefix {
			str = prefix + key
		}

		field, b := rve.FieldByName(str)
		if !b {
			delete(data, key)
		}

		str = field.Tag.Get("dataInfo")
		if "" == str {
			delete(data, key)
		}
	}

	return data

	//typeOf := reflect.TypeOf(entity) //通过反射获取type定义

	//if typeOf.Kind() == reflect.Ptr { //是否指针类型
	//	typeOf = typeOf.Elem()
	//}

	//prefix := ""
	//for _, key := range fieldPrefix {
	//	prefix = prefix + key
	//}

	//for key, _ := range data {
	//	str := key
	//	if "" != prefix {
	//		str = prefix + key
	//	}

	//	_, b := typeOf.FieldByName(str)
	//	if !b {
	//		delete(data, key)
	//	}
	//}

	//return data
}

func (service CommonService) getBaseEntity(entity interface{}) interface{} {
	var rte reflect.Type
	var rve reflect.Value
	typeOf := reflect.TypeOf(entity)  //通过反射获取type定义
	if typeOf.Kind() == reflect.Ptr { //是否指针类型
		rve = reflect.ValueOf(entity).Elem()
		rte = rve.Type() // 取得struct变量的指针
	} else if "reflect.Value" == typeOf.String() {
		rve = entity.(reflect.Value)
		rte = rve.Type()
	} else {
		rve = reflect.ValueOf(entity)
		rte = reflect.ValueOf(entity).Type()
	}

	for k := 0; k < rte.NumField(); k++ {
		field := rte.Field(k)
		if field.Anonymous && strings.HasSuffix(field.Name, "Base") {
			return rve.Field(k) //找到
		}
	}

	return entity //没找到
}

/**
 * 修改
 * @param entity 对象类型
 * @param id 记录编号值
 * @param iVersion 记录版本号
 * @param data 待更新的字段和值
 * @return MsgEmity
 */
func (service CommonService) Edit(ctx *gin.Context, entity interface{}, id interface{}, iVersion int, data map[string]interface{}) *MsgEmity {
	if "" == fmt.Sprintf("%v", id) {
		return MsgEmity{}.Err(7001, "记录编号参数为空！")
	}

	baseEntity := ReflectUtils{}.GetBaseEntity(entity)        //取基础数据库实体
	data = ReflectUtils{}.HoldByEntity(baseEntity, data, "G") //按实体保留map中的数据
	// MapUtil{}.RemoveData(data, []string{"uId", "sId", "iId", "sPath", "sCreator", "sModifieder", "dCreateDate", "dModifiedDate", "iVersion",
	// 	"u_id", "s_id", "i_id", "s_path", "s_creator", "s_modifieder", "d_create_date", "d_modified_date", "i_version"}) //清理不能由前端定义的字段
	MapUtil{}.RemoveData(data, []string{TableMajorKeyString, TableMajorKeyAutoInt, TableMajorKeyUuId, TablePathKey, TableCreatorName, TableCreateDateName, TableModifiederName, TableModifiedDateName}) //清理不能由前端定义的字段

	if len(data) < 1 {
		return MsgEmity{}.Err(7002, "更新操作提供的参数为空！")
	}

	tableInfo := TableInfo{}.GetByEntity(baseEntity)

	if tableInfo.GbHasVersion && (1 > iVersion) {
		return MsgEmity{}.Err(7003, "记录版本号参数必须大于0！")
	}

	StringUtil{}.TrimAttribute(data) //清除对象各个属性的值的前后空格

	// me := DataInfo{}.ValidAttr(data, baseEntity, []string{"uId", "sId", "iId", "sCreator", "sModifieder", "dCreateDate", "dModifiedDate", //对对象中添加了DataInfo注解的不为nil的属性检查限制
	// 	"u_id", "s_id", "i_id", "s_path", "s_creator", "s_modifieder", "d_create_date", "d_modified_date", "i_version"})  //对对象中添加了DataInfo注解的不为nil的属性检查限制
	me := DataInfo{}.ValidAttr(data, baseEntity, []string{TableMajorKeyString, TableMajorKeyAutoInt, TableMajorKeyUuId, TablePathKey, TableCreateDateName, TableModifiedDateName}) //对对象中添加了DataInfo注解的不为nil的属性检查限制

	if !me.Gsuccess {
		return me.IncCode(7020)
	}

	me = service.ValidEntityRepeatByEdit(ctx, baseEntity, id, data) //验证更新数据是否存在重复
	if !me.Gsuccess {
		return me.IncCode(7030)
	}

	onlyCreator := !ModuleUtil{}.EnableTag(tableInfo.GsTableName, 5) //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆

	return CommonDao{}.Edit(baseEntity, id, iVersion, data, ModuleUtil{}.CurrentLoginUserId(ctx), onlyCreator)
}

/**
 * 根据主键查询数据
 * id 主键
 * entity 检查用数据结构
 */
func (service CommonService) FindById(ctx *gin.Context, entity interface{}, id interface{}) *MsgEmity {
	if id == nil {
		return MsgEmity{}.Err(1001, "记录编号为空")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	onlyCreator := !ModuleUtil{}.EnableTag(tableInfo.GsTableName, 4) //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆

	return CommonDao{}.FindById(entity, id, ModuleUtil{}.CurrentLoginUserId(ctx), onlyCreator)
}

// 查询所有数据
func (service CommonService) FindAll(ctx *gin.Context, entity interface{}, where map[string]interface{}) *MsgEmity {
	tableInfo := TableInfo{}.GetByEntity(entity)

	onlyCreator := !ModuleUtil{}.EnableTag(tableInfo.GsTableName, 4) //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆

	return CommonDao{}.FindAll(entity, where, ModuleUtil{}.CurrentLoginUserId(ctx), onlyCreator)
}

// 查询指定时间内数据
func (service CommonService) FindByDate(ctx *gin.Context, entity interface{}, sDateSt string, sDateEd string) *MsgEmity {
	tableInfo := TableInfo{}.GetByEntity(entity)

	onlyCreator := !ModuleUtil{}.EnableTag(tableInfo.GsTableName, 4) //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆

	return CommonDao{}.FindByDate(entity, sDateSt, sDateEd, ModuleUtil{}.CurrentLoginUserId(ctx), onlyCreator)
}

// 查询指定行数据
func (service CommonService) FindByRow(ctx *gin.Context, entity interface{}, id interface{}) *MsgEmity {
	if id == nil {
		return MsgEmity{}.Err(1001, "记录编号为空")
	}

	sId := fmt.Sprintf("%v", id)
	if sId == "" {
		return MsgEmity{}.Err(1002, "记录编号为空")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	onlyCreator := !ModuleUtil{}.EnableTag(tableInfo.GsTableName, 4) //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆

	return CommonDao{}.FindByRow(entity, id, ModuleUtil{}.CurrentLoginUserId(ctx), onlyCreator)
}

// 查询分页数据
func (service CommonService) FindByPage(ctx *gin.Context, entity interface{}, findByPageParam FindByPageParam) *MsgEmity {
	tableInfo := TableInfo{}.GetByEntity(entity)

	onlyCreator := !ModuleUtil{}.EnableTag(tableInfo.GsTableName, 4) //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆

	return CommonDao{}.FindByPage(entity, findByPageParam, ModuleUtil{}.CurrentLoginUserId(ctx), onlyCreator)
}

// func (service CommonService) FindData(OrderInfoList orders, String sLikeStr, Object condition,
// 			onlyCreator bool, entity interface{}) {

// 			}

/**
 * 补充数据库实体类的数据--新增
 * @param dbEntity
 * @param selfId 自提供的编号,防止从序列中获取
 * @return
 */
func (service CommonService) SupplyDbEntityAttrByAdd(ctx *gin.Context, dbEntity interface{}, selfId string, idValuePrefix string) *MsgEmity {
	if nil == dbEntity {
		return MsgEmity{}.Err(1001, "待存储的数据实体不能为nil")
	}

	DataInfo{}.SetDataInfoDefault(dbEntity) //对对象中添加了dataInfo注解的属性添加默认值

	sLoginUserId := ModuleUtil{}.CurrentLoginUserId(ctx) //取当前登录用户编号
	date := time.Now()

	ReflectUtils{}.SetFieldValue(dbEntity, GtableCreatorName, sLoginUserId)    // 设置当前操作用户
	ReflectUtils{}.SetFieldValue(dbEntity, GtableModifiederName, sLoginUserId) // 设置当前操作用户
	ReflectUtils{}.SetFieldValue(dbEntity, GtableCreateDateName, date)         // 设置新增时间
	ReflectUtils{}.SetFieldValue(dbEntity, GtableModifiedDateName, date)       // 设置修改时间
	ReflectUtils{}.SetFieldValue(dbEntity, GtableIndexName, 1)                 // 设置排序值
	ReflectUtils{}.SetFieldValue(dbEntity, GtableStateName, 1)                 // 设置状态值
	ReflectUtils{}.SetFieldValue(dbEntity, GtableVersionName, 1)               // 设置版本号

	tableInfo := TableInfo{}.GetByEntity(dbEntity)

	vNew := selfId
	if (TableMajorKeyUuId == tableInfo.GsKeyName) && "" == selfId { //按UUID设置值
		if tableInfo.GiKeyLen == 32 {
			vNew = strings.Replace(uuid.NewV4().String(), "-", "", -1) //取uuid
			ReflectUtils{}.SetFieldValue(dbEntity, GtableMajorKeyUuId, strings.ToUpper(vNew))
		} else if tableInfo.GiKeyLen == 22 {
			ReflectUtils{}.SetFieldValue(dbEntity, GtableMajorKeyUuId, U62Util{}.Get(idValuePrefix))
		} else {
			ReflectUtils{}.SetFieldValue(dbEntity, GtableMajorKeyUuId, U62Util{}.Get(idValuePrefix))
		}
	} else if (TableMajorKeyString == tableInfo.GsKeyName) && ("" == selfId) { //如果不是自增长,并且没有自定义id值,才能进行从序列中取新id的方法
		if tableInfo.GbHasPath { //对象中存在sPath,则id只需要4位
			pidValue := ReflectUtils{}.GetFieldValue(dbEntity, GtablePidKey)
			pid := fmt.Sprintf("%v", pidValue)
			if nil != pidValue {
				pid = TableTreeRootValue
			}

			me := CommonDao{}.NewChildId(tableInfo, pid)
			if !me.Gsuccess {
				return me
			}

			vNew = me.Gdata.(string)
			ReflectUtils{}.SetFieldValue(dbEntity, GtableMajorKeyString, vNew)
		} else if "" == idValuePrefix {
			vNew = ModuleUtil{}.GetNewId(tableInfo.GiKeyLen, tableInfo.GsTableName)
			ReflectUtils{}.SetFieldValue(dbEntity, GtableMajorKeyString, vNew)
		} else {
			iLength := tableInfo.GiKeyLen - len(idValuePrefix)
			vNew = idValuePrefix + ModuleUtil{}.GetNewId(iLength, tableInfo.GsTableName)
			ReflectUtils{}.SetFieldValue(dbEntity, GtableMajorKeyString, vNew)
		}
	}

	//--如果有sPid字段则进行部分处理--//
	if tableInfo.GbHasPid {
		sPid := ReflectUtils{}.GetFieldValue(dbEntity, GtablePidKey)
		if (nil == sPid) || ("" == sPid.(string)) { //如果sPid为空,则默认赋值'00'
			ReflectUtils{}.SetFieldValue(dbEntity, GtablePidKey, GtableTreeRootValue)
		}
	}

	//--如果有sPath字段则进行部分处理--//
	if tableInfo.GbHasPath {
		sPid := ReflectUtils{}.GetFieldValue(dbEntity, GtablePidKey)
		sPath := ReflectUtils{}.GetFieldValue(dbEntity, GtablePathKey)

		if ((nil == sPath) || ("" == sPath.(string)) || ("/00/" == sPath.(string))) && (GtableTreeRootValue == sPid.(string)) { //如果sPid为空,则默认赋值'00'
			ReflectUtils{}.SetFieldValue(dbEntity, GtablePathKey, "/00/"+vNew+"/")
		} else {
			sPath := CommonDao{}.GetPath(sPid.(string), tableInfo.GsDbName, tableInfo.GsTableName)
			ReflectUtils{}.SetFieldValue(dbEntity, GtablePathKey, sPath+vNew+"/")
		}
	}

	//--如果有sRecordKey字段则进行部分处理--//
	if tableInfo.GbHasRecordKey {
		if "" == recordKeyJam {
			recordKeyJam = AppUtil{}.ReadConfigKey("App", "RecordKeyJam", "12345678").(string)
		}

		key := ReflectUtils{}.DoMethod(dbEntity, "CreateRecordKey", reflect.ValueOf(recordKeyJam))
		if len(key) > 0 {
			ReflectUtils{}.SetFieldValue(dbEntity, GtableRecordKeyName, key[0])
		}
	}

	return MsgEmity{}.Success(1999, "补充数据完毕！")
}

/**
 * 验证新增数据是否存在重复
 *
 */
func (service CommonService) ValidEntityRepeatByAdd(ctx *gin.Context, dbEntity interface{}) *MsgEmity {
	tableInfo := TableInfo{}.GetByEntity(dbEntity)
	customService := GlobalVariable{}.Get(tableInfo.GsTableName + "_ModuleService")

	//-- 树形结构 --//
	if tableInfo.GbHasPid {
		if nil == customService { //如果没有自定义业务层
			return service.CommonCheckRepeatByAddAndTree(ctx, dbEntity) //通用树型结构表添加数据时重复检查方法
		}

		method := ReflectUtils{}.GetMethod(customService, "CheckRepeatByAddAndTree")
		if !method.IsValid() { //如果自定义业务层定义了自检方法
			return service.CommonCheckRepeatByAddAndTree(ctx, dbEntity) //通用树型结构表添加数据时重复检查方法
		}

		result := ReflectUtils{}.DoMethod(customService, "CheckRepeatByAddAndTree", dbEntity)
		me := result[0].Interface()

		return me.(*MsgEmity)
	}

	//--不是树形数据则使用普通方法检查--//
	if nil == customService { //如果没有自定义业务层
		return service.CommonCheckRepeatByAdd(ctx, dbEntity) //通用添加数据时重复检查方法
	}

	method := ReflectUtils{}.GetMethod(customService, "CheckRepeatByAdd")
	if !method.IsValid() { //如果自定义业务层定义了自检方法
		return service.CommonCheckRepeatByAdd(ctx, dbEntity) //通用添加数据时重复检查方法
	}

	result := ReflectUtils{}.DoMethod(customService, "CheckRepeatByAdd", dbEntity)
	me := result[0].Interface()

	return me.(*MsgEmity)
}

/**
 * 验证更新数据是否存在重复
 *
 */
func (service CommonService) ValidEntityRepeatByEdit(ctx *gin.Context, entity interface{}, id interface{}, data map[string]interface{}) *MsgEmity {
	tableInfo := TableInfo{}.GetByEntity(entity)
	customService := GlobalVariable{}.Get(tableInfo.GsTableName + "_ModuleService")

	//-- 树形结构 --//
	if tableInfo.GbHasPid {
		if nil == customService { //如果没有自定义业务层
			return service.CommonCheckRepeatByEditAndTree(ctx, entity, id, data["sName"]) //通用树型结构表添加数据时重复检查方法
		}

		method := ReflectUtils{}.GetMethod(customService, "CheckRepeatByEditAndTree")
		if !method.IsValid() { //如果自定义业务层定义了自检方法
			return service.CommonCheckRepeatByEditAndTree(ctx, entity, id, data["sName"]) //通用树型结构表添加数据时重复检查方法
		}

		result := ReflectUtils{}.DoMethod(customService, "CheckRepeatByEditAndTree", id, data["sName"])
		me := result[0].Interface()

		return me.(*MsgEmity)
	}

	//--不是树形数据则使用普通方法检查--//
	if nil == customService { //如果没有自定义业务层
		return service.CommonCheckRepeatByEdit(ctx, entity, id) //通用添加数据时重复检查方法
	}

	method := ReflectUtils{}.GetMethod(customService, "CheckRepeatByEdit")
	if !method.IsValid() { //如果自定义业务层定义了自检方法
		return service.CommonCheckRepeatByEdit(ctx, entity, id) //通用添加数据时重复检查方法
	}

	result := ReflectUtils{}.DoMethod(customService, "CheckRepeatByEdit", data, id)
	me := result[0].Interface()

	return me.(*MsgEmity)
}

/**
 * 通用树型结构表添加数据时重复检查方法
 * dbEntity
 */
func (service CommonService) CommonCheckRepeatByAddAndTree(ctx *gin.Context, dbEntity interface{}) *MsgEmity {
	vName := ReflectUtils{}.GetFieldValue(dbEntity, GtableTreeNodeName)
	if nil == vName {
		return MsgEmity{}.Err(1001, "节点名称为空")
	}

	tableInfo := TableInfo{}.GetByEntity(dbEntity)

	sName := vName.(string)

	var sPid string
	vPid := ReflectUtils{}.GetFieldValue(dbEntity, GtablePidKey)
	if nil != vPid {
		sPid = vPid.(string)
	} else {
		sPid = TableTreeRootValue
	}

	if sPid == "" {
		sPid = TableTreeRootValue
	}

	where := make(map[string]interface{})

	//同一层节点下,展现名不能相同//
	var build strings.Builder
	build.WriteString("SELECT SUM(iCount) AS iCount FROM (")

	if tableInfo.GbHasSign {
		build.WriteString(" 	select SIGN(COUNT(1) * 10) as iCount from ${sDbName}${sTableName}")
		build.WriteString(" 	where ")
		build.WriteString(TablesSign)
		build.WriteString(" = @sSign")
		build.WriteString(TablesSign)
		build.WriteString(" UNION ALL ")

		where[TablesSign] = ReflectUtils{}.GetFieldValue(dbEntity, GtablesSign)
	}

	build.WriteString(" 	select SIGN(COUNT(1)) as iCount from ${sDbName}${sTableName}")
	build.WriteString(" 	where ${TablePidKey} = @sPid and ${TableTreeNodeName} = @sName")
	build.WriteString(") TMP")

	txt := build.String()
	txt = strings.Replace(txt, "${sDbName}", SqlFactory{}.GetDbName(tableInfo.GsDbName), -1)
	txt = strings.Replace(txt, "${sTableName}", tableInfo.GsTableName, -1)
	txt = strings.Replace(txt, "${TablePidKey}", TablePidKey, -1)
	txt = strings.Replace(txt, "${TableTreeNodeName}", TableTreeNodeName, -1)

	where[TablePidKey] = sPid
	where[TableTreeNodeName] = sName

	var iCount int
	dbResult := SqlFactory{}.Raw(txt, where).Find(&iCount)
	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return MsgEmity{}.Err(1002, "查询数据发生异常:", dbResult.Error)
	}

	if iCount != 0 {
		return MsgEmity{}.Err(1003, "节点重复")
	}

	return MsgEmity{}.Success(1999, "节点未重复")
}

/**
 * 通用树型结构表添加数据时重复检查方法
 * entity
 */
func (service CommonService) CommonCheckRepeatByEditAndTree(ctx *gin.Context, entity interface{}, id interface{}, sName interface{}) *MsgEmity {
	if (nil == sName) || ("" == sName) || ("<nil>" == sName) {
		return MsgEmity{}.Err(1001, "节点名称为空")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	where := make(map[string]interface{})

	//同一层节点下,展现名不能相同//
	var build strings.Builder
	build.WriteString("SELECT SUM(iCount) AS iCount FROM (")

	if tableInfo.GbHasSign {
		build.WriteString(" 	select SIGN(COUNT(1) * 10) as iCount from ${sDbName}${sTableName}")
		build.WriteString(" 	where ")
		build.WriteString(TablesSign)
		build.WriteString(" = @")
		build.WriteString(TablesSign)
		build.WriteString(" 	and ${sId} <> @sId")
		build.WriteString(" UNION ALL ")

		where[TablesSign] = ReflectUtils{}.GetFieldValue(entity, GtablesSign)
	}

	build.WriteString(" 	select SIGN(COUNT(1)) as iCount from ${sDbName}${sTableName}")
	build.WriteString(" 	where ${sId} <> @sId")
	build.WriteString(" 	and ${TablePidKey} = (select a.${TablePidKey} from ${sDbName}${sTableName} a where a.${sId} = @sId)")
	build.WriteString(" 	and ${TableTreeNodeName} = #{sName}")
	build.WriteString(") TMP")

	txt := build.String()
	txt = strings.Replace(txt, "${sDbName}", SqlFactory{}.GetDbName(tableInfo.GsDbName), -1)
	txt = strings.Replace(txt, "${sTableName}", tableInfo.GsTableName, -1)
	txt = strings.Replace(txt, "${sId}", tableInfo.GsKeyName, -1)
	txt = strings.Replace(txt, "${TablePidKey}", TablePidKey, -1)
	txt = strings.Replace(txt, "${TableTreeNodeName}", TableTreeNodeName, -1)
	txt = strings.Replace(txt, "#{sName}", "@sName", -1)

	where["sId"] = id
	where["sName"] = sName

	var iCount int
	dbResult := SqlFactory{}.Raw(txt, where).Find(&iCount)
	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return MsgEmity{}.Err(1002, "查询数据发生异常:", dbResult.Error)
	}

	if iCount != 0 {
		return MsgEmity{}.Err(1003, "节点重复")
	}

	return MsgEmity{}.Success(1999, "节点未重复")
}

/**
 * 通用添加数据时重复检查方法
 * dbEntity
 */
func (service CommonService) CommonCheckRepeatByAdd(ctx *gin.Context, dbEntity interface{}) *MsgEmity {
	tableInfo := TableInfo{}.GetByEntity(dbEntity)

	vCheckRepeatCombination := GlobalVariable{}.Get(tableInfo.GsTableName + "_CheckRepeatCombination")
	vCheckRepeatAlone := GlobalVariable{}.Get(tableInfo.GsTableName + "_CheckRepeatAlone")

	k := 0

	//检查待新增内容是否存在重复数据(多字段组合重复即重复)集合
	if nil != vCheckRepeatCombination {
		checkRepeatCombination := vCheckRepeatCombination.([]string)

		var build strings.Builder
		build.WriteString("SELECT COUNT(1) AS iCount FROM ${sDbName}${sTableName} WHERE 1=1 ")

		var temp strings.Builder
		temp.WriteString("[")

		where := make(map[string]interface{})
		for _, value := range checkRepeatCombination {
			build.WriteString(" AND ")
			build.WriteString(value)
			build.WriteString(" = @")
			build.WriteString(value)

			where[value] = ReflectUtils{}.GetFieldValue(dbEntity, "G"+value)
			temp.WriteString("、")
			temp.WriteString(value)
		}

		txt := strings.Replace(build.String(), "1=1 AND ", "", -1)
		txt = strings.Replace(txt, "${sDbName}", SqlFactory{}.GetDbName(tableInfo.GsDbName), -1)
		txt = strings.Replace(txt, "${sTableName}", tableInfo.GsTableName, -1)

		iCount := 0
		dbResult := SqlFactory{}.Raw(txt, where).Find(&iCount)
		if dbResult.Error != nil {
			Log.Error("验证数据是否重复发生异常:", dbResult.Error)
			return MsgEmity{}.Err(1001, "验证数据是否重复发生异常:", dbResult.Error)
		}

		if iCount != 0 {
			temp.WriteString("]组合发现数据重复")
			return MsgEmity{}.Err(1002, strings.Replace(temp.String(), "、", "", 1))
		}

		k++
	}

	//检查待新增内容是否存在重复数据(单独字段重复即重复)集合
	if nil != vCheckRepeatAlone {
		checkRepeatAlone := vCheckRepeatAlone.(map[string]int)

		var build strings.Builder
		build.WriteString("SELECT SUM(iCount) FROM (")

		where := make(map[string]interface{})
		for key, value := range checkRepeatAlone {
			build.WriteString(" union all select (SIGN(COUNT(1)) * ")
			build.WriteString(strconv.Itoa(value))
			build.WriteString(") as iCount ")
			build.WriteString(" from ${sDbName}${sTableName} ")
			build.WriteString(" where ")
			build.WriteString(key)
			build.WriteString("= @")
			build.WriteString(key)

			where[key] = ReflectUtils{}.GetFieldValue(dbEntity, "G"+key)
		}

		build.WriteString(") TMP")

		txt := strings.Replace(build.String(), " union all ", " ", 1)
		txt = strings.Replace(txt, "${sDbName}", SqlFactory{}.GetDbName(tableInfo.GsDbName), -1)
		txt = strings.Replace(txt, "${sTableName}", tableInfo.GsTableName, -1)

		iCount := 0
		dbResult := SqlFactory{}.Raw(txt, where).Find(&iCount)
		if dbResult.Error != nil {
			Log.Error("验证数据是否重复发生异常:", dbResult.Error)
			return MsgEmity{}.Err(1003, "验证数据是否重复发生异常:", dbResult.Error)
		}

		if iCount != 0 {
			var temp strings.Builder
			str := fmt.Sprintf("%0*d", len(checkRepeatAlone), iCount)
			array := []rune(str) //得到字符数组

			ml := len(checkRepeatAlone)
			for key, value := range checkRepeatAlone {
				i := len(strconv.Itoa(value))
				if array[ml-i] == 48 { // ASCII对应: '0' => 48, '1' => 49
					continue
				}

				temp.WriteString("、")
				temp.WriteString(key)
			}

			temp.WriteString("存在重复")

			return MsgEmity{}.Err(1004, strings.Replace(temp.String(), "、", "", 1))
		}

		k++
	}

	if 0 == k {
		return MsgEmity{}.Success("没有设定验证函数,通过")
	}

	return MsgEmity{}.Success("经验证,通过")
}

/**
 * 通用更新数据时重复检查方法
 * entity
 */
func (service CommonService) CommonCheckRepeatByEdit(ctx *gin.Context, entity interface{}, id interface{}) *MsgEmity {
	tableInfo := TableInfo{}.GetByEntity(entity)

	vCheckRepeatCombination := GlobalVariable{}.Get(tableInfo.GsTableName + "_CheckRepeatCombination")
	vCheckRepeatAlone := GlobalVariable{}.Get(tableInfo.GsTableName + "_CheckRepeatAlone")

	k := 0

	//检查待修改内容是否存在重复数据(多字段组合重复即重复)集合
	if nil != vCheckRepeatCombination {
		checkRepeatCombination := vCheckRepeatCombination.([]string)

		var build strings.Builder
		build.WriteString("SELECT COUNT(1) AS iCount FROM ${sDbName}${sTableName} WHERE 1=1 ")

		var temp strings.Builder
		temp.WriteString("[")

		where := make(map[string]interface{})
		where[tableInfo.GsKeyName] = id
		build.WriteString(" AND ")
		build.WriteString(tableInfo.GsKeyName)
		build.WriteString(" = @")
		build.WriteString(tableInfo.GsKeyName)

		for _, value := range checkRepeatCombination {
			build.WriteString(" AND ")
			build.WriteString(value)
			build.WriteString(" = @")
			build.WriteString(value)

			where[value] = ReflectUtils{}.GetFieldValue(entity, "G"+value)
			temp.WriteString("、")
			temp.WriteString(value)
		}

		txt := strings.Replace(build.String(), "1=1 AND ", "", -1)
		txt = strings.Replace(txt, "${sDbName}", SqlFactory{}.GetDbName(tableInfo.GsDbName), -1)
		txt = strings.Replace(txt, "${sTableName}", tableInfo.GsTableName, -1)

		iCount := 0
		dbResult := SqlFactory{}.Raw(txt, where).Find(&iCount)
		if dbResult.Error != nil {
			Log.Error("验证数据是否重复发生异常:", dbResult.Error)
			return MsgEmity{}.Err(1001, "验证数据是否重复发生异常:", dbResult.Error)
		}

		if iCount != 0 {
			temp.WriteString("]组合发现数据重复")
			return MsgEmity{}.Err(1002, strings.Replace(temp.String(), "、", "", 1))
		}

		k++
	}

	//检查待修改内容是否存在重复数据(单独字段重复即重复)集合
	if nil != vCheckRepeatAlone {
		checkRepeatAlone := vCheckRepeatAlone.(map[string]int)

		var build strings.Builder
		build.WriteString("SELECT SUM(iCount) FROM (")

		where := make(map[string]interface{})
		where[tableInfo.GsKeyName] = id

		for key, value := range checkRepeatAlone {
			build.WriteString(" union all select (SIGN(COUNT(1)) * ")
			build.WriteString(strconv.Itoa(value))
			build.WriteString(") as iCount ")
			build.WriteString(" from ${sDbName}${sTableName} ")
			build.WriteString(" where ")
			build.WriteString(key)
			build.WriteString("= @")
			build.WriteString(key)
			build.WriteString(" and ")
			build.WriteString(tableInfo.GsKeyName)
			build.WriteString(" = @")
			build.WriteString(tableInfo.GsKeyName)

			where[key] = ReflectUtils{}.GetFieldValue(entity, "G"+key)
		}

		build.WriteString(") TMP")

		txt := strings.Replace(build.String(), " union all ", " ", 1)
		txt = strings.Replace(txt, "${sDbName}", SqlFactory{}.GetDbName(tableInfo.GsDbName), -1)
		txt = strings.Replace(txt, "${sTableName}", tableInfo.GsTableName, -1)

		iCount := 0
		dbResult := SqlFactory{}.Raw(txt, where).Find(&iCount)
		if dbResult.Error != nil {
			Log.Error("验证数据是否重复发生异常:", dbResult.Error)
			return MsgEmity{}.Err(1003, "验证数据是否重复发生异常:", dbResult.Error)
		}

		if iCount != 0 {
			var temp strings.Builder
			str := fmt.Sprintf("%0*d", len(checkRepeatAlone), iCount)
			array := []rune(str) //得到字符数组

			ml := len(checkRepeatAlone)
			for key, value := range checkRepeatAlone {
				i := len(strconv.Itoa(value))
				if array[ml-i] == 48 { // ASCII对应: '0' => 48, '1' => 49
					continue
				}

				temp.WriteString("、")
				temp.WriteString(key)
			}

			temp.WriteString("存在重复")

			return MsgEmity{}.Err(1004, strings.Replace(temp.String(), "、", "", 1))
		}

		k++
	}

	if 0 == k {
		return MsgEmity{}.Success("没有设定验证函数,通过")
	}

	return MsgEmity{}.Success("经验证,通过")
}

/**
 * 读取树形结构数据
 * @param entity
 * @param sGroupColumn
 * @param sGroupName
 * @return
 */
func (service CommonService) FindByTree(ctx *gin.Context, entity interface{}, sGroupColumn, sGroupName string) *MsgEmity {
	tableInfo := TableInfo{}.GetByEntity(entity)
	if !tableInfo.GbHasPid {
		return MsgEmity{}.Err(1001, "指定分组字段不存在！")
	}

	//-- 从文件读取 --//
	var filePath string

	if sGroupName == "" {
		filePath = "./temp/cache/" + tableInfo.GsTableName + "/tree.txt"
	} else {
		filePath = "./temp/cache/" + tableInfo.GsTableName + "/Group_" + sGroupName + ".txt"
	}

	me := JsonUtil{}.FormFile(filePath, map[string]interface{}{})
	if me.Gsuccess {
		return MsgEmity{}.Success(me.Gdata, "在文件中找到") //返回结果
	}

	//-- 从数据库读取 --//
	var build strings.Builder
	build.WriteString("SELECT * FROM ${sDbName}${sTableName} WHERE ")
	build.WriteString(TableMajorKeyString)
	build.WriteString(" > 0")

	where := []interface{}{}
	if "" != sGroupName {
		build.WriteString(" AND ")
		build.WriteString(TablePathKey)
		build.WriteString(" LIKE (")
		build.WriteString(" 	select CONCAT(a.")
		build.WriteString(TablePathKey)
		build.WriteString(", '%')")
		build.WriteString(" 	from ${sDbName}${sTableName} a")

		if "" != sGroupColumn {
			build.WriteString(" 	where a.")
			build.WriteString(sGroupColumn) //指定字段作为分组标识
			build.WriteString(" = ?")
		} else if tableInfo.GbHasOnlyign { //启用唯一标识作为关键字
			build.WriteString(" 	where a.")
			build.WriteString(TableOnlyignName) //启用唯一标识作为关键字
			build.WriteString(" = ?")           //启用唯一标识作为关键字
		} else {
			build.WriteString(" 	where a.")
			build.WriteString(TableTreeNodeName)
			build.WriteString(" = ?")
		}

		build.WriteString(" )")
		where = append(where, sGroupName)
	}

	build.WriteString(" ORDER BY ")
	build.WriteString(TablePathKey)

	txt := strings.Replace(build.String(), "${sDbName}", SqlFactory{}.GetDbName(tableInfo.GsDbName), -1)
	txt = strings.Replace(txt, "${sTableName}", tableInfo.GsTableName, -1)
	rows, err := SqlFactory{}.Raw(txt, where).Rows()
	defer rows.Close()

	if nil != err {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(1002, "查询发生异常:", err)
	}

	res := SqlFactory{}.ScanRows2mapI(rows)
	if res == nil {
		return MsgEmity{}.Err(1003, "查询后数据转换发生异常")
	}

	if len(res) < 1 {
		return MsgEmity{}.Err(1004, "数据为空")
	}

	sRootValue := TableTreeRootValue
	if sGroupName != "" {
		sRootValue = res[0][TableMajorKeyString].(string) //分组查询情况下必须要一个根节点(因为已经path排序)
	}

	me = CommonService{}.CreateTree(res, sRootValue, TableMajorKeyString, TablePidKey, "childs")
	if !me.Gsuccess {
		return me
	}

	if len((me.Gdata).([]interface{})) < 1 {
		return MsgEmity{}.Err(1005, "数据转换后构造树型数据为空")
	}

	JsonUtil{}.ToFile(me.Gdata, filePath) //保存到文件

	return MsgEmity{}.Success(me.Gdata, "查询成功")
}

/**
 * List转树形结构
 * @param source Map或切片结构
 * @param rootName
 * @param idFieldName
 * @param pIdFieldName
 * @param childFieldName
 * @return
 */
func (this CommonService) CreateTree(source interface{},
	rootName, idFieldName, pIdFieldName, childFieldName string) *MsgEmity {
	if nil == source {
		return MsgEmity{}.Err(5001, "没有数据无法进行树结构创建!")
	}

	if "" == strings.TrimSpace(idFieldName) {
		return MsgEmity{}.Err(5002, "对象字段中的编号属性名称必须提供!")
	}

	if "" == strings.TrimSpace(pIdFieldName) {
		return MsgEmity{}.Err(5003, "对象字段中的上级编号属性名称必须提供!")
	}

	if "" == strings.TrimSpace(childFieldName) {
		return MsgEmity{}.Err(5004, "对象字段中的子节点集合属性名称必须提供!")
	}

	sourceTypeName := reflect.TypeOf(source).String()
	if strings.HasPrefix(sourceTypeName, "[]map[string]interface") {
		return this.createTreeByMap(source.([]map[string]interface{}), rootName, idFieldName, pIdFieldName, childFieldName)
	}

	return this.createTreeByList(source.([]interface{}), rootName, idFieldName, pIdFieldName, childFieldName)
}

/**
 * List转树形结构
 * @param source 切片结构
 * @param rootName
 * @param idFieldName
 * @param pIdFieldName
 * @param childFieldName
 * @return
 */
func (service CommonService) createTreeByList(source []interface{},
	rootName, idFieldName, pIdFieldName, childFieldName string) *MsgEmity {
	if nil == source || (len(source) < 1) {
		return MsgEmity{}.Err(5001, "没有数据无法进行树结构创建!")
	}

	result := []interface{}{}

	rt := reflect.TypeOf(source[0])
	sourceTypeName := rt.String()

	isMap := strings.HasPrefix(sourceTypeName, "map[string]interface") //如果数据类型是Map则取值不能用反射方式

	if "" == strings.TrimSpace(rootName) {
		rootName = TableTreeRootValue //未指定就默认为'00'做根节点
	}

	//-- 将所有数据进行预分组存储,同时筛选出根节点 --//
	groupMap := map[string][]interface{}{} //待进行挂接的子分组
	allMap := map[string]interface{}{}
	for _, object := range source {
		sGroupName := ""
		if !isMap {
			sGroupName = ReflectUtils{}.GetFieldValue(object, pIdFieldName).(string)
		} else {
			sGroupName = (object.(map[string]interface{}))[pIdFieldName].(string)
		}

		if "" == strings.TrimSpace(sGroupName) {
			sGroupName = rootName //如果父节点信息为空,则默认为根节点
		}

		sGroupName = strings.TrimSpace(sGroupName) //所在组名

		sid := ""
		if isMap {
			sid = (object.(map[string]interface{}))[idFieldName].(string)
		} else {
			sid = (ReflectUtils{}.GetFieldValue(object, idFieldName)).(string)
		}

		allMap[sid] = object

		if rootName == sGroupName {
			result = append(result, object) //加入根节点列表
			continue
		}

		childs, ok := groupMap[sGroupName]
		if !ok {
			childs = []interface{}{}
			groupMap[sGroupName] = childs
		}

		childs = append(childs, object)
		groupMap[sGroupName] = childs
	}

	//--将所有分组寻找父节点--//
	for key, value := range groupMap {
		obj := allMap[key]
		if nil == obj {
			continue
		}

		sTypeName := reflect.TypeOf(obj).String()
		if strings.HasPrefix(sTypeName, "map[string]interface") {
			(obj.(map[string]interface{}))[childFieldName] = value
			continue
		}

		ReflectUtils{}.SetFieldValue(obj, childFieldName, value) // 设置子节点集合
	}

	return MsgEmity{}.Success(result, "构造成功!")
}

/**
 * List转树形结构
 * @param source Map结构
 * @param rootName
 * @param idFieldName
 * @param pIdFieldName
 * @param childFieldName
 * @return
 */
func (service CommonService) createTreeByMap(source []map[string]interface{},
	rootName, idFieldName, pIdFieldName, childFieldName string) *MsgEmity {
	if nil == source || (len(source) < 1) {
		return MsgEmity{}.Err(5001, "没有数据无法进行树结构创建!")
	}

	result := []interface{}{}

	rt := reflect.TypeOf(source[0])
	sourceTypeName := rt.String()

	isMap := strings.HasPrefix(sourceTypeName, "map[string]interface") //如果数据类型是Map则取值不能用反射方式

	if "" == strings.TrimSpace(rootName) {
		rootName = TableTreeRootValue //未指定就默认为'00'做根节点
	}

	//-- 将所有数据进行预分组存储,同时筛选出根节点 --//
	groupMap := map[string][]interface{}{} //待进行挂接的子分组
	allMap := map[string]interface{}{}
	for _, object := range source {
		sGroupName := ""
		if !isMap {
			sGroupName = ReflectUtils{}.GetFieldValue(object, pIdFieldName).(string)
		} else {
			sGroupName = object[pIdFieldName].(string)
		}

		if "" == strings.TrimSpace(sGroupName) {
			sGroupName = rootName //如果父节点信息为空,则默认为根节点
		}

		sGroupName = strings.TrimSpace(sGroupName) //所在组名

		sid := ""
		if isMap {
			sid = object[idFieldName].(string)
		} else {
			sid = (ReflectUtils{}.GetFieldValue(object, idFieldName)).(string)
		}

		allMap[sid] = object

		if rootName == sGroupName {
			result = append(result, object) //加入根节点列表
			continue
		}

		childs, ok := groupMap[sGroupName]
		if !ok {
			childs = []interface{}{}
			groupMap[sGroupName] = childs
		}

		childs = append(childs, object)
		groupMap[sGroupName] = childs
	}

	//--将所有分组寻找父节点--//
	for key, value := range groupMap {
		obj := allMap[key]
		if nil == obj {
			continue
		}

		sTypeName := reflect.TypeOf(obj).String()
		if strings.HasPrefix(sTypeName, "map[string]interface") {
			(obj.(map[string]interface{}))[childFieldName] = value
			continue
		}

		ReflectUtils{}.SetFieldValue(obj, childFieldName, value) // 设置子节点集合
	}

	return MsgEmity{}.Success(result, "构造成功!")
}

/**
 * 根据字段名取指定记录编号的数据库表中对应字段的值
 * @param entity 实体类
 * @param id
 * @param fieldNames 待取数据的字段名称集合
 * @return MsgEmity 返回内容data中存放的是Map
 */
func (service CommonService) GetValueByFieldName(ctx *gin.Context, entity interface{}, id interface{}, fieldNames []string) *MsgEmity {
	fieldNames = ReflectUtils{}.HoldByEntityToArray(entity, fieldNames, "G") //按实体保留数组中的数据
	if len(fieldNames) < 1 {
		return MsgEmity{}.Err(7001, "没有对应的数据可查询！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	var build strings.Builder
	build.WriteString("SELECT ")

	for _, val := range fieldNames {
		build.WriteString(",")
		build.WriteString(val)
	}

	build.WriteString(" FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE ")
	build.WriteString(tableInfo.GsKeyName)
	build.WriteString("=@")
	build.WriteString(tableInfo.GsKeyName)

	where := make(map[string]interface{})
	where[tableInfo.GsKeyName] = id

	if (!ModuleUtil{}.EnableTag(tableInfo.GsTableName, 4)) { //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆
		build.WriteString(" AND ")
		build.WriteString(TableCreatorName)
		build.WriteString("=@")
		build.WriteString(TableCreatorName)
		where[TableCreatorName] = ModuleUtil{}.CurrentLoginUserId(ctx)
	}

	sql := strings.Replace(build.String(), ",", "", 1)
	rows, err := SqlFactory{}.Raw(sql, where).Rows()

	defer rows.Close()

	if nil != err {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7002, "查询发生异常:", err)
	}

	res := SqlFactory{}.ScanRows2mapI(rows)
	if res == nil {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7003, "查询发生异常:", err)
	}

	if len(res) < 1 {
		return MsgEmity{}.Err(7004, "数据不存在！")
	}

	return MsgEmity{}.Success(res[0], "查询成功")
}

/**
 * 根据字段名取指定记录编号的数据库表中对应字段的值
 * @param entity 实体类
 * @param id
 * @param fieldName 待取数据的字段名称集合
 * @return MsgEmity 返回内容data中存放的是Map
 */
func (service CommonService) GetValueByField(ctx *gin.Context, entity interface{}, id interface{}, fieldName string) *MsgEmity {
	fieldName = strings.TrimSpace(fieldName)
	if "" == fieldName {
		return MsgEmity{}.Err(7001, "没有对应的数据可查询！")
	}

	if (!ReflectUtils{}.HasField(entity, fieldName)) {
		return MsgEmity{}.Err(7002, "指定字段不存在！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)

	var build strings.Builder
	build.WriteString("SELECT ")
	build.WriteString(fieldName)
	build.WriteString(" FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE ")
	build.WriteString(tableInfo.GsKeyName)
	build.WriteString("=@")
	build.WriteString(tableInfo.GsKeyName)

	where := make(map[string]interface{})
	where[tableInfo.GsKeyName] = id

	if (!ModuleUtil{}.EnableTag(tableInfo.GsTableName, 4)) { //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆
		build.WriteString(" AND ")
		build.WriteString(TableCreatorName)
		build.WriteString("=@")
		build.WriteString(TableCreatorName)
		where[TableCreatorName] = ModuleUtil{}.CurrentLoginUserId(ctx)
	}

	sql := strings.Replace(build.String(), ",", "", 1)
	rows, err := SqlFactory{}.Raw(sql, where).Rows()

	defer rows.Close()

	if nil != err {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7003, "查询发生异常:", err)
	}

	res := SqlFactory{}.ScanRows2mapI(rows)
	if res == nil {
		Log.Error("查询发生异常:", err)
		return MsgEmity{}.Err(7004, "查询发生异常:", err)
	}

	if len(res) < 1 {
		return MsgEmity{}.Err(7005, "数据不存在！")
	}

	return MsgEmity{}.Success(res[0], "查询成功")
}

/**
 * 取记录对应的版本号
 * @param entity 实体类
 * @param idName 编号
 * @return
 */
func (service CommonService) GetiVersion(ctx *gin.Context, entity interface{}, id interface{}) *MsgEmity {
	if "" == fmt.Sprintf("%v", id) {
		return MsgEmity{}.Err(8001, "记录编号参数为空！")
	}

	return CommonDao{}.GetiVersion(entity, id)
}

/**
 * 取记录对应的状态值
 * @param entity 实体类
 * @param idName 编号
 * @return
 */
func (service CommonService) GetiState(ctx *gin.Context, entity interface{}, id interface{}) *MsgEmity {
	if "" == fmt.Sprintf("%v", id) {
		return MsgEmity{}.Err(8001, "记录编号参数为空！")
	}

	return CommonDao{}.GetiState(entity, id)
}

/**
 * 根据关键值取对象集合
 * @Param entity 实体类
 * @Param where 存放查询条件
 * @return MsgEmity
 */
func (service CommonService) FindByKey(ctx *gin.Context, entity interface{}, where map[string]interface{}) *MsgEmity {
	where = ReflectUtils{}.HoldByEntity(entity, where) //按实体保留map中的数据
	if len(where) < 1 {
		return MsgEmity{}.Err(7001, "没有对应的查询条件！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)
	onlyCreator := !ModuleUtil{}.EnableTag(tableInfo.GsTableName, 4) //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆

	return CommonDao{}.FindByKey(entity, where, ModuleUtil{}.CurrentLoginUserId(ctx), onlyCreator)
}

/**
 * 根据关键值取对象集合中的第一个
 * @Param entity 实体类
 * @Param where 存放查询条件
 * @param fields 指定要查询的字段集合
 * @return MsgEmity
 */
func (service CommonService) FindOneByKey(ctx *gin.Context, entity interface{}, where map[string]interface{}, fields ...string) *MsgEmity {
	where = ReflectUtils{}.HoldByEntity(entity, where) //按实体保留map中的数据
	if len(where) < 1 {
		return MsgEmity{}.Err(8001, "没有对应的查询条件！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)
	onlyCreator := !ModuleUtil{}.EnableTag(tableInfo.GsTableName, 4) //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆

	return CommonDao{}.FindOneByKey(entity, where, ModuleUtil{}.CurrentLoginUserId(ctx), onlyCreator)
}

/**
 * 根据关键值取对象集合中的符合条件的第一条记录的指定字段
 * @Param entity 实体类
 * @Param where 存放查询条件
 * @param fieldName 指定要查询的字段
 * @return MsgEmity
 */
func (service CommonService) FindValueByKey(ctx *gin.Context, entity interface{}, where map[string]interface{}, fieldName string) *MsgEmity {
	where = ReflectUtils{}.HoldByEntity(entity, where) //按实体保留map中的数据
	if len(where) < 1 {
		return MsgEmity{}.Err(8001, "没有对应的查询条件！")
	}

	fieldName = strings.TrimSpace(fieldName)
	if "" == fieldName {
		return MsgEmity{}.Err(8002, "没有待查字段！")
	}

	if (!ReflectUtils{}.HasField(entity, fieldName)) {
		return MsgEmity{}.Err(8003, "指定字段不存在！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)
	onlyCreator := !ModuleUtil{}.EnableTag(tableInfo.GsTableName, 4) //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆

	return CommonDao{}.FindValueByKey(entity, where, fieldName, ModuleUtil{}.CurrentLoginUserId(ctx), onlyCreator)
}

/**
 * 根据关键值查数量
 * @Param entity 实体类
 * @Param where 存放查询条件
 * @return MsgEmity
 */
func (service CommonService) FindCountByKey(ctx *gin.Context, entity interface{}, where map[string]interface{}) *MsgEmity {
	where = ReflectUtils{}.HoldByEntity(entity, where) //按实体保留map中的数据

	if len(where) < 1 {
		return MsgEmity{}.Err(7001, "没有对应的查询条件！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)
	onlyCreator := !ModuleUtil{}.EnableTag(tableInfo.GsTableName, 4) //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆

	return CommonDao{}.FindCountByKey(entity, where, ModuleUtil{}.CurrentLoginUserId(ctx), onlyCreator)
}

// /**
//  * 以事务方式执行Mapper下的多个方法
//  * 注意:Mapper必须存在才能执行
//  * @param mapper iBatis实体
//  * @Param funcInfo 接口信息集合(以3对为一组) <函数名, 函数参数类型集合, 函数参数集合, 函数名, 函数参数类型集合, 函数参数集合....>
//  * @return MsgEmity 返回对象数组(各函数执行结果)
//  */
// func (service CommonService) transactionMapping(Object mapper, Object...funcInfo) *MsgEmity {

// 	return MsgEmity{}.Success(res, "查询成功")
// }

/**
 * 根据字段名取分组数据
 * @param entity 实体类
 * @param sCreator 指定用户
 * @param fields 字段名与别名对象
 * @return
 */
func (service CommonService) GroupByField(ctx *gin.Context, entity interface{}, sCreator string, fields map[string]string) *MsgEmity {
	if len(fields) < 1 {
		return MsgEmity{}.Err(8001, "没有对应的待查字段！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)
	onlyCreator := !ModuleUtil{}.EnableTag(tableInfo.GsTableName, 4) //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆

	return CommonDao{}.GroupByField(entity, sCreator, fields, ModuleUtil{}.CurrentLoginUserId(ctx), onlyCreator)
}

/**
 * 取表中指定字段的最大值
 * @param entity 实体类
 * @param sCreator 指定用户
 * @param field 字段名
 * @param condition 查询条件字符串
 * @return
 */
func (service CommonService) MaxByField(ctx *gin.Context, entity interface{}, sCreator string, field string, where map[string]interface{}) *MsgEmity {
	field = strings.TrimSpace(field)
	if "" == field {
		return MsgEmity{}.Err(7001, "没有待查字段！")
	}

	if len(where) < 1 {
		return MsgEmity{}.Err(7002, "没有对应的查询条件！")
	}

	if (!ReflectUtils{}.HasField(entity, field)) {
		return MsgEmity{}.Err(7003, "指定字段不存在！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)
	onlyCreator := !ModuleUtil{}.EnableTag(tableInfo.GsTableName, 4) //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆

	return CommonDao{}.MaxByField(entity, sCreator, field, where, ModuleUtil{}.CurrentLoginUserId(ctx), onlyCreator)
}

/**
 * 取表中指定字段的最小值
 * @param entity 实体类
 * @param sCreator 指定用户
 * @param field 字段名
 * @param whereStr 查询条件字符串
 * @return
 */
func (service CommonService) MinByField(ctx *gin.Context, entity interface{}, sCreator string, field string, where map[string]interface{}) *MsgEmity {
	field = strings.TrimSpace(field)
	if "" == field {
		return MsgEmity{}.Err(7001, "没有待查字段！")
	}

	if len(where) < 1 {
		return MsgEmity{}.Err(7002, "没有对应的查询条件！")
	}

	if (!ReflectUtils{}.HasField(entity, field)) {
		return MsgEmity{}.Err(7003, "指定字段不存在！")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)
	onlyCreator := !ModuleUtil{}.EnableTag(tableInfo.GsTableName, 4) //6个数的控制分别是:删除、修改、查询、导出、统计、步骤值可逆

	return CommonDao{}.MinByField(entity, sCreator, field, where, ModuleUtil{}.CurrentLoginUserId(ctx), onlyCreator)
}

/**
 * 检查关键值记录是否存在(返回1:存在;0:不存在)
 * @param entity 实体类
 * @Param id
 * @return MsgEmity
 */
func (service CommonService) HasById(ctx *gin.Context, entity interface{}, id interface{}) *MsgEmity {
	if "" == fmt.Sprintf("%v", id) {
		return MsgEmity{}.Err(8001, "记录编号参数为空！")
	}

	return CommonDao{}.HasById(entity, id)
}

/**
 * 检查关键值记录是否存在(返回1:存在;0:不存在)
 * @param entity 实体类
 * @Param keyName 字段名
 * @Param keyValue 字段值
 * @return MsgEmity
 */
func (service CommonService) HasByKey(ctx *gin.Context, entity interface{}, keyName string, keyValue interface{}) *MsgEmity {
	keyName = strings.TrimSpace(keyName)
	if "" == keyName {
		return MsgEmity{}.Err(7001, "字段名参数为空！")
	}

	if (nil == keyValue) || ("" == fmt.Sprintf("%v", keyValue)) || ("<nil>" == fmt.Sprintf("%v", keyValue)) {
		return MsgEmity{}.Err(7002, "字段值参数为空！")
	}

	if (!ReflectUtils{}.HasField(entity, keyName)) {
		return MsgEmity{}.Err(7003, "指定字段不存在！")
	}

	dataInfo := DataInfo{}.GetDataInfoByName(entity, keyName)
	if nil == dataInfo {
		return MsgEmity{}.Err(7004, "字段备注信息缺失")
	}

	switch dataInfo.GsDbFileType {
	case "int":
		temp, err := strconv.Atoi(keyValue.(string))
		if err != nil {
			return MsgEmity{}.Err(7005, "字段值参数为不符合规范！")
		}
		keyValue = temp
	case "bigint":
		temp, err := strconv.ParseInt(keyValue.(string), 10, 64)
		if err != nil {
			return MsgEmity{}.Err(7006, "字段值参数为不符合规范！")
		}
		keyValue = temp
	}

	return CommonDao{}.HasByKey(entity, keyName, keyValue)
}

/**
 * 清理指定用户的缓存
 * @param cacheName 缓存名
 * @Param sUser 用户名
 * @return MsgEmity
 */
func (service CommonService) ClearCache(ctx *gin.Context, cacheName string, sUser string) *MsgEmity {
	cacheName = strings.TrimSpace(cacheName)
	if "" == cacheName {
		return MsgEmity{}.Err(7001, "指定'缓存库名称'参数为空！")
	}

	sUser = strings.TrimSpace(sUser)
	if "" == sUser {
		cacheName = cacheName + sUser
	}

	if (GlobalVariable{}.Del(cacheName)) {
		return MsgEmity{}.Success(7999, "清理成功！")
	}

	return MsgEmity{}.Err(7002, "清理失败！")
}

/**
 * 查询组结构数据
 * @param r Http请求对象
 * @param entity 实体类
 * @param sGroupColumn 分组名(树节点)所在字段名
 * @param sGroupName 分组名(树节点)
 * @return
 */
func (service CommonService) FindByGroup(ctx *gin.Context, entity interface{}, sGroupColumn, sGroupName string) *MsgEmity {
	return service.FindByTree(ctx, entity, sGroupColumn, sGroupName)
}

/**
 * 添加数据到指定组下
 * 警告:对象必须符合树形结构要求,如:sId、sPid
 * @param r Http请求对象
 * @param sGroupName 分组字段名称(树节点)
 * @param sGroupValue 分组字段值(树节点)
 * @param entity 实体对象
 * @return
 */
func (service CommonService) AddToGroup(ctx *gin.Context, entity interface{}, sGroupName, sGroupValue string) *MsgEmity {
	if "" == sGroupName {
		return MsgEmity{}.Err(8001, "节点参数不能为空!")
	}

	if nil == entity {
		return MsgEmity{}.Err(8002, "实体对象不能为空!")
	}

	tableInfo := TableInfo{}.GetByEntity(entity)
	if !tableInfo.GbHasPid {
		return MsgEmity{}.Err(1001, "指定分组字段不存在！")
	}

	if TableMajorKeyString != tableInfo.GsKeyName {
		return MsgEmity{}.Err(8003, "实体类型没有sId字段,不符合格式要求!")
	}

	if !tableInfo.GbHasPid {
		return MsgEmity{}.Err(8004, "实体类型没有sPid字段,不符合格式要求!")
	}

	//-- 查组所在记录编号 --//
	var build strings.Builder
	build.WriteString("SELECT ")
	build.WriteString(TableMajorKeyString)
	build.WriteString(" FROM ")
	build.WriteString(SqlFactory{}.GetDbName(tableInfo.GsDbName))
	build.WriteString(tableInfo.GsTableName)
	build.WriteString(" WHERE ")
	build.WriteString(sGroupName)
	build.WriteString("=? LIMIT 0, 1")

	sPId := ""
	dbResult := SqlFactory{}.Raw(build.String(), sGroupValue).Find(&sPId)
	if dbResult.Error != nil {
		Log.Error("查询发生异常:", dbResult.Error)
		return MsgEmity{}.Err(8005, "查询发生异常:", dbResult.Error)
	}

	if "" == sPId {
		return MsgEmity{}.Err(8006, "指定组不存在,不能用此方法添加!")
	}

	ReflectUtils{}.SetFieldValue(entity, TablePidKey, sPId) // 父编号就是查出来的数据

	DataInfo{}.SetDataInfoDefault(entity) //对对象中添加了dataInfo注解的属性添加默认值

	me := service.SupplyDbEntityAttrByAdd(ctx, entity, "", "")
	if !me.Gsuccess {
		return me.IncCode(7030)
	}

	me = DataInfo{}.ValidAttrByAdd(entity, []string{}) //对对象中添加了DataInfo注解的属性检查限制
	if !me.Gsuccess {
		return me.IncCode(7020)
	}

	me = service.ValidEntityRepeatByAdd(ctx, entity) //验证新增数据是否存在重复
	if !me.Gsuccess {
		return me.IncCode(7030)
	}

	return CommonDao{}.AddCommon(entity)
}
