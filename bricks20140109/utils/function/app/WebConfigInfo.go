package app

import (
	"strings"
)

//web项目基本配置信息
type WebConfigInfo struct {
	Root  string //页面根路径
	Port  string //端口
	Title string //程序标题
	Name  string //名称
}

//取对应配置
func (this WebConfigInfo) Get() WebConfigInfo {
	me := AppUtil{}.ToEntity(this, "system")
	if !me.Gsuccess {
		return this
	}

	result := me.Gdata.(WebConfigInfo)
	if "" == result.Root {
		result.Root = "/webroot/"
	}

	if "" == result.Port {
		result.Port = "8080"
	}

	return result
}

//取开始运行提示信息
func (this WebConfigInfo) RunStartStr() string {
	var build strings.Builder
	build.WriteString("================ ")
	build.WriteString(this.Title)
	build.WriteString(this.Name)
	build.WriteString("启动完毕,使用端口:")
	build.WriteString(this.Port)
	build.WriteString(" ================")

	return build.String()
}
