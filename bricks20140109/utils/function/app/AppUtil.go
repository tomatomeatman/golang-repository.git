package app

import (
	"os"
	"reflect"
	"strings"

	"gitee.com/tomatomeatman/golang-repository/bricks/model"
	Log "github.com/cihub/seelog"
	"gopkg.in/ini.v1"
)

var (
	AppCfg          *ini.File //配置文件变量
	glbAppiCloudApp = 0       //是否要加入分布式系统(0:待初始化;1:是;2:否)
)

type AppUtil struct{}

// 判断程序是否要加入分布式系统
func (this AppUtil) IsCloudApp() bool {
	if 0 != glbAppiCloudApp {
		return glbAppiCloudApp == 1
	}

	if this.HasSection("CloudCenter") {
		glbAppiCloudApp = 1
		return true
	}

	glbAppiCloudApp = 2
	return false
}

// 判断程序是否非分布式系统
func (this AppUtil) IsNotCloudApp() bool {
	if 0 != glbAppiCloudApp {
		return glbAppiCloudApp != 1
	}

	if this.HasSection("CloudCenter") {
		glbAppiCloudApp = 1
		return false
	}

	glbAppiCloudApp = 2
	return true
}

// 判断配置组是否存在
func (this AppUtil) HasSection(sectionName string) bool {
	if !this.iniCfg() {
		return false
	}

	section, _ := AppCfg.GetSection(sectionName) // return AppCfg.HasSection(sectionName)//这种方式不安全

	if section == nil {
		return false
	}

	if len(section.Keys()) == 0 {
		return false
	}

	return true
}

// 读取配置值
func (this AppUtil) ReadConfigKey(section string, key string, def interface{}) interface{} {
	if !this.iniCfg() {
		return def
	}

	title := AppCfg.Section(section)
	value, _ := title.GetKey(key)
	if value == nil {
		return def
	}

	if def == nil {
		def = ""
	}

	switch def.(type) {
	case string: // 将interface转为string字符串类型
		return value.String()
	case int:
		result, err := value.Int()
		if err != nil {
			return def
		}

		return result
	case int64:
		result, err := value.Int64()
		if err != nil {
			return 0
		}

		return result
	case bool:
		result, err := value.Bool()
		if err != nil {
			return 0
		}

		return result
	default:
		return value.String()
	}
}

// 初始化配置文件变量
func (this AppUtil) iniCfg() bool {
	if nil != AppCfg {
		return true
	}

	configFilePath := "./config/app.ini"

	_, err := os.Stat(configFilePath) //os.Stat获取文件信息
	if err != nil {
		if !os.IsExist(err) {
			Log.Error("配置文件不存在", err)
			return false
		}
	}

	AppCfg, err = ini.Load(configFilePath)
	if err != nil {
		Log.Error("配置文件读取错误", err)
		return false
	}

	return true
}

/**
 * 通过结构体获取
 * @param entity 结构体样式
 * @param names 辅助'项名称',若不传递则按结构体名称做'项名称'
 * @return 返回新建结构体实体
 */
func (this AppUtil) ToEntity(entity interface{}, names ...string) *model.MsgEmity {
	if nil == entity {
		entity = map[string]interface{}{}
	}

	if !this.iniCfg() {
		return model.MsgEmity{}.Err(1001, "读取配置失败")
	}

	rt := reflect.TypeOf(entity)
	rve := reflect.New(rt).Elem()

	if len(names) < 1 {
		sName := rt.Name()
		if strings.Contains(sName, "map[string]") || strings.HasPrefix(sName, "[]") {
			return model.MsgEmity{}.Err(1002, "不明确的配置项,无法进行解析")
		}

		names = append(names, sName)
	}

	for _, sName := range names {
		section := AppCfg.Section(sName)
		for k := 0; k < rt.NumField(); k++ {
			value, _ := section.GetKey(rt.Field(k).Name)
			if value == nil {
				continue
			}

			field := rve.FieldByName(rt.Field(k).Name)
			if !field.IsValid() {
				continue
			}

			{
				temp := value.String()
				if strings.Contains(temp, "${") {
					continue //配置中含有变量符,不获取(待改进)
				}
			}

			switch field.Type().String() {
			case "string":
				field.Set(reflect.ValueOf(value.String()))
			case "int":
				temp, err := value.Int()
				if err != nil {
					continue
				}

				field.Set(reflect.ValueOf(temp))
			case "int64":
				temp, err := value.Int64()
				if err != nil {
					continue
				}

				field.Set(reflect.ValueOf(temp))
			case "bool":
				temp, err := value.Bool()
				if err != nil {
					continue
				}

				field.Set(reflect.ValueOf(temp))
			default:
				field.Set(reflect.ValueOf(value.String()))
			}
		}
	}

	result := rve.Interface()

	return model.MsgEmity{}.Success(result, "转换结束")
}
