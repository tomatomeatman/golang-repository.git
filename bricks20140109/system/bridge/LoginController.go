package bridge

import (
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/app"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/url"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/gin"
	"github.com/gin-gonic/gin"
)

// @Controller 桥接服务-登录桥接操作接口
type LoginController struct {
}

/**
 * 初始化
 */
func init() {
	if (AppUtil{}.IsNotCloudApp()) { //没有启用分布式,则不能用桥接接口
		return
	}

	//-- 接口注册 --//
	GinUtil{}.RegisterController("/login/in", POST, LoginController{}.In)
	GinUtil{}.RegisterController("/login/out", POST, LoginController{}.Out)
	GinUtil{}.RegisterController("/login/check", POST, LoginController{}.Check)
	GinUtil{}.RegisterController("/login/heartbeat", POST, LoginController{}.Heartbeat)
	GinUtil{}.RegisterController("/login/info", POST, LoginController{}.GetLogin)
	GinUtil{}.RegisterController("/login/info/current", POST, LoginController{}.GetCurrentLogin)
	GinUtil{}.RegisterController("/login/getid", POST, LoginController{}.GetUserId)
}

// #region @Api {title=登录,如果用户和密码正确则返回通行令牌}
// @param {name=sNameOrNo dataType=string paramType=query explain=名称或工号 required=true}
// @param {name=sPass dataType=int paramType=query explain=密码 required=true}
// @param {name=sOwner dataType=string paramType=query explain=用户来源表 required=true}
// @param {name=iDevice dataType=int paramType=query explain=设备类型,1:PC,2:手机,3:平板,4..... required=false}
// @return {type=json explain=返回对象}
// @RequestMapping {name=In type=POST value=/login/in}
// #endregion
func (control LoginController) In(ctx *gin.Context) interface{} {
	sNameOrNo := UrlUtil{}.GetParam(ctx, "sNameOrNo", "").(string)
	sPass := UrlUtil{}.GetParam(ctx, "sPass", "").(string)
	sOwner := UrlUtil{}.GetParam(ctx, "sOwner", "").(string)
	iDevice := UrlUtil{}.GetParam(ctx, "iDevice", 1).(int)

	return LoginService{}.In(ctx, sNameOrNo, sPass, sOwner, iDevice)
}

// #region @Api {title=用户登出}
// @param {name=sCookie dataType=string paramType=query explain=令牌 required=true}
// @return {type=json explain=返回对象}
// @RequestMapping {name=Out type=POST value=/login/out}
// #endregion
func (control LoginController) Out(ctx *gin.Context) interface{} {
	sCookie := UrlUtil{}.GetParam(ctx, "sCookie", "").(string)
	return LoginService{}.Out(ctx, sCookie)
}

// #region @Api {title=判断sCookie是否已经登录}
// @param {name=sCookie dataType=string paramType=query explain=令牌 required=true}
// @return {type=json explain=返回对象}
// @RequestMapping {name=Out type=POST value=/login/check}
// #endregion
func (control LoginController) Check(ctx *gin.Context) interface{} {
	sCookie := UrlUtil{}.GetParam(ctx, "sCookie", "").(string)
	return LoginService{}.Check(ctx, sCookie)
}

// #region @Api {title=维持登录的心跳操作}
// @param {name=sCookie dataType=string paramType=query explain=令牌 required=true}
// @return {type=json explain=返回对象}
// @RequestMapping {name=Out type=POST value=/login/heartbeat}
// #endregion
func (control LoginController) Heartbeat(ctx *gin.Context) interface{} {
	sCookie := UrlUtil{}.GetParam(ctx, "sCookie", "").(string)
	return LoginService{}.Heartbeat(ctx, sCookie)
}

// #region @Api {title=取登录用户信息,注意:限制为内部系统访问}
// @param {name=key dataType=string paramType=query explain=访问本系统的密钥 required=true}
// @param {name=sCookie dataType=string paramType=query explain=令牌 required=true}
// @return {type=json explain=返回对象}
// @RequestMapping {name=Out type=POST value=/login/info}
// #endregion
func (control LoginController) GetLogin(ctx *gin.Context) interface{} {
	key := UrlUtil{}.GetParam(ctx, "key", "").(string)
	sCookie := UrlUtil{}.GetParam(ctx, "sCookie", "").(string)
	return LoginService{}.GetLogin(ctx, key, sCookie)
}

// #region @Api {title=取当前登录用户简洁信息}
// @return {type=json explain=返回对象}
// @RequestMapping {name=Out type=POST value=/login/info/current}
// #endregion
func (control LoginController) GetCurrentLogin(ctx *gin.Context) interface{} {
	return LoginService{}.GetCurrentLogin(ctx)
}

// #region @Api {title=根据用户和密码取对应的用户编号}
// @param {name=sNameOrNo dataType=string paramType=query explain=名称或工号 required=true}
// @param {name=sPass dataType=string paramType=query explain=密码 required=true}
// @param {name=sOwner dataType=string paramType=query explain=用户来源表 required=true}
// @return {type=json explain=返回对象}
// @RequestMapping {name=Out type=POST value=/login/getid}
// #endregion
func (control LoginController) GetUserId(ctx *gin.Context) interface{} {
	sNameOrNo := UrlUtil{}.GetParam(ctx, "sNameOrNo", "").(string)
	sPass := UrlUtil{}.GetParam(ctx, "sPass", "").(string)
	sOwner := UrlUtil{}.GetParam(ctx, "sOwner", "").(string)

	return LoginService{}.GetUserId(ctx, sNameOrNo, sPass, sOwner)
}
