package bridge

import (
	. "gitee.com/tomatomeatman/golang-repository/bricks/model"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/app"
	"github.com/gin-gonic/gin"
)

type LoginService struct {
}

var (
	loginServiceName = "" //桥接到登录服务的服务名,默认GBaseSystemServer
	loginServiceKey  = "" //桥接到登录服务的服务密钥
)

// 初始化
func init() {
	if (AppUtil{}.IsNotCloudApp()) { //没有启用分布式,则不能用桥接接口
		return
	}

	loginServiceName = AppUtil{}.ReadConfigKey("CloudServer", "BaseServerName", "GBaseSystemServer").(string)
	loginServiceKey = AppUtil{}.ReadConfigKey("CloudServer", "BaseServerKey", "").(string)
}

/**
 * 登录,如果用户和密码正确则返回同行令牌
 * @param sNameOrNo 名称或工号
 * @param sPass 密码
 * @param sOwner 用户来源表
 * @param iDevice 设备类型,1:PC,2:手机,3:平板,4.....
 * @return
 */
func (service LoginService) In(ctx *gin.Context, sNameOrNo, sPass, sOwner string, iDevice int) *MsgEmity {
	m := map[string]interface{}{
		"sNameOrNo": sNameOrNo,
		"sPass":     sPass,
		"sOwner":    sOwner,
		"iDevice":   iDevice,
	}

	me := BridgeDao{}.Post(ctx, loginServiceName, "/login/in", m, &MsgEmity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*MsgEmity)
}

/**
 * 用户登出
 * @param sCookie
 * @return
 */
func (service LoginService) Out(ctx *gin.Context, sCookie string) *MsgEmity {
	m := map[string]interface{}{"sCookie": sCookie}

	me := BridgeDao{}.Post(ctx, loginServiceName, "/login/out", m, &MsgEmity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*MsgEmity)
}

/**
 * 判断sCookie是否已经登录
 * @param sCookie
 * @return
 */
func (service LoginService) Check(ctx *gin.Context, sCookie string) *MsgEmity {
	m := map[string]interface{}{"sCookie": sCookie}

	me := BridgeDao{}.Post(ctx, loginServiceName, "/login/check", m, &MsgEmity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*MsgEmity)
}

/**
 * 登录心跳操作,sCookie存在则更新并返回true,没有则返回false
 * @param sCookie
 * @return
 */
func (service LoginService) Heartbeat(ctx *gin.Context, sCookie string) *MsgEmity {
	m := map[string]interface{}{"sCookie": sCookie}

	me := BridgeDao{}.Post(ctx, loginServiceName, "/login/heartbeat", m, &MsgEmity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*MsgEmity)
}

/**
 * 取登录用户信息,注意:限制为内部系统访问
 * @param key
 * @param sCookie
 * @return
 */
func (service LoginService) GetLogin(ctx *gin.Context, key, sCookie string) *MsgEmity {
	m := map[string]interface{}{"key": key, "sCookie": sCookie}

	me := BridgeDao{}.Post(ctx, loginServiceName, "/login/info", m, &MsgEmity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*MsgEmity)
}

/**
 * 取当前登录用户简洁信息
 * @param request
 * @return
 */
func (service LoginService) GetCurrentLogin(ctx *gin.Context) *MsgEmity {
	me := BridgeDao{}.Post(ctx, loginServiceName, "/login/info/current", nil, &MsgEmity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*MsgEmity)
}

/**
 * 根据用户和密码取对应的用户编号
 * @param sNameOrNo
 * @param sPass
 * @param sOwner 用户来源表
 * @return
 */
func (service LoginService) GetUserId(ctx *gin.Context, sNameOrNo, sPass, sOwner string) *MsgEmity {
	m := map[string]interface{}{"sNameOrNo": sNameOrNo, "sPass": sPass, "sOwner": sOwner}

	me := BridgeDao{}.Post(ctx, loginServiceName, "/login/getid", m, &MsgEmity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*MsgEmity)
}
