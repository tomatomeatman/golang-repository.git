package bridge

import (
	. "gitee.com/tomatomeatman/golang-repository/bricks/model"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/app"
	"github.com/gin-gonic/gin"
)

type SystemParamsService struct {
}

var (
	systemParamsServiceName = "" //桥接到登录服务的服务名,默认GBaseSystemServer
	//systemParamsServiceName  = "" //桥接到登录服务的服务密钥
)

// 初始化
func init() {
	if (AppUtil{}.IsNotCloudApp()) { //没有启用分布式,则不能用桥接接口
		return
	}

	systemParamsServiceName = AppUtil{}.ReadConfigKey("CloudServer", "BaseServerName", "GBaseSystemServer").(string)
	//systemParamsServiceName = AppUtil{}.ReadConfigKey("CloudServer", "BaseServerKey", "").(string)
}

/**
 * 取所有免拦截系统参数对象集合
 * @return
 */
func (service SystemParamsService) FindByNoIntercept(ctx *gin.Context) *MsgEmity {
	me := BridgeDao{}.Post(ctx, systemParamsServiceName, "/system/params/insidevisit/find/not/intercept", nil, &MsgEmity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*MsgEmity)
}
