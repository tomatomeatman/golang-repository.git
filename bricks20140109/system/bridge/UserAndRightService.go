package bridge

import (
	. "gitee.com/tomatomeatman/golang-repository/bricks/model"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/app"
	"github.com/gin-gonic/gin"
)

type UserAndRightService struct {
}

var (
	userAndRightServiceName = "" //桥接到登录服务的服务名,默认GBaseSystemServer
	//userAndRightServiceKey  = "" //桥接到登录服务的服务密钥
)

// 初始化
func init() {
	if (AppUtil{}.IsNotCloudApp()) { //没有启用分布式,则不能用桥接接口
		return
	}

	userAndRightServiceName = AppUtil{}.ReadConfigKey("CloudServer", "BaseServerName", "GBaseSystemServer").(string)
	//userAndRightServiceKey = AppUtil{}.ReadConfigKey("CloudServer", "BaseServerKey", "").(string)
}

/**
 * 清理指定用户的缓存
 * @param sUser 用户编号
 * @return
 */
func (service UserAndRightService) ClearCache(ctx *gin.Context, sLoginId string) *MsgEmity {
	m := map[string]interface{}{"sLoginId": sLoginId}

	me := BridgeDao{}.Post(ctx, userAndRightServiceName, "/user/and/right/clear/cache", m, &MsgEmity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*MsgEmity)
}

/**
 * 根据用户取权限标识字符串(一个权限标识代表了多个可访问的url路径)
 * 不用判断请求用户是谁,因为其它人获取信息后没用,权限校验会在每次进行具体操作时进行再次判断
 * @param sUserId
 * @return
 */
func (service UserAndRightService) FindEnglishByUserId(ctx *gin.Context, sUserId string) *MsgEmity {
	m := map[string]interface{}{"sUserId": sUserId}

	me := BridgeDao{}.Post(ctx, userAndRightServiceName, "/user/and/right/find/english", m, &MsgEmity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*MsgEmity)
}

/**
 * 验证指定用户是否有访问指定url的权限(给内部拦截器用,直接返回Boolean)
 * @param sUserId
 * @param url
 * @return
 */
func (service UserAndRightService) CheckUrlRight(ctx *gin.Context, sUserId, url string) bool {
	m := map[string]interface{}{"sUserId": sUserId, "url": url}

	me := BridgeDao{}.Post(ctx, userAndRightServiceName, "/user/and/right/check", m, &MsgEmity{})
	if !me.Gsuccess {
		return false
	}

	return me.Gdata.(*MsgEmity).Gsuccess
}

/**
 * 验证指定用户是否有访问指定url的权限
 * @param sUserId 验证的用户
 * @param url 请求验证的权限(URL地址)
 * @return
 */
func (service UserAndRightService) CheckRight(ctx *gin.Context, sUserId, url string) *MsgEmity {
	m := map[string]interface{}{"sUserId": sUserId, "url": url}

	me := BridgeDao{}.Post(ctx, userAndRightServiceName, "/user/and/right/check/right", m, &MsgEmity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*MsgEmity)
}

/**
 * 根据用户查询用户所拥有的权限编号集合
 * @param sUserId
 * @return
 */
func (service UserAndRightService) FindRightId(ctx *gin.Context, sUserId string) *MsgEmity {
	m := map[string]interface{}{"sUserId": sUserId}

	me := BridgeDao{}.Post(ctx, userAndRightServiceName, "/user/and/right/find/rightid", m, &MsgEmity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*MsgEmity)
}

/**
 * 根据用户查询用户所拥有的权限的最后更新时间
 * @param sUserId
 * @return
 */
func (service UserAndRightService) LastTime(ctx *gin.Context, sUserId string) *MsgEmity {
	m := map[string]interface{}{"sUserId": sUserId}

	me := BridgeDao{}.Post(ctx, userAndRightServiceName, "/user/and/right/lasttime", m, &MsgEmity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*MsgEmity)
}
