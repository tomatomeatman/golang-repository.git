package bridge

import (
	. "gitee.com/tomatomeatman/golang-repository/bricks/model"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/app"
	. "gitee.com/tomatomeatman/golang-repository/bricks/utils/function/url"
	"github.com/gin-gonic/gin"
)

/**
 * 模块管理ModuleManage表基本业务操作结构体
 */
type ModuleManageService struct {
}

var (
	moduleManageServiceName = "" //桥接到登录服务的服务名,默认GBaseSystemServer
	//moduleManageServiceName  = "" //桥接到登录服务的服务密钥
)

// 初始化
func init() {
	if (AppUtil{}.IsNotCloudApp()) { //没有启用分布式,则不能用桥接接口
		return
	}

	moduleManageServiceName = AppUtil{}.ReadConfigKey("CloudServer", "BaseServerName", "GBaseSystemServer").(string)
	//moduleManageServiceName = AppUtil{}.ReadConfigKey("CloudServer", "BaseServerKey", "").(string)
}

/**
 * 读取树形结构数据
 * ctx Http请求对象
 */
func (service ModuleManageService) FindByTree(ctx *gin.Context) *MsgEmity {
	sGroupColumn := UrlUtil{}.GetParam(ctx, "sGroupColumn", "").(string)
	sGroupName := UrlUtil{}.GetParam(ctx, "sGroupName", "").(string)
	m := map[string]interface{}{
		"sGroupColumn": sGroupColumn,
		"sGroupName":   sGroupName,
	}

	me := BridgeDao{}.Post(ctx, moduleManageServiceName, "/module/manage/find/tree", m, &MsgEmity{})
	if !me.Gsuccess {
		return me
	}

	return me.Gdata.(*MsgEmity)
}
