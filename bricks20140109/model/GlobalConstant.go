package model

type GlobalConstant struct{}

const (

	/**
	 * 数据库表主键名称(字符串形式)
	 */
	TableMajorKeyString = "sId"

	/**
	 * 数据库表主键名称(自增长形式)
	 */
	TableMajorKeyAutoInt = "iId"

	/**
	 * 数据库表主键名称(UUID形式)
	 */
	TableMajorKeyUuId = "uId"

	/**
	 * 数据库表字段名称-上级字段名
	 */
	TablePidKey = "sPid"

	/**
	 * 数据库表字段名称-主键路径名
	 */
	TablePathKey = "sPath"

	/**
	 * 数据库表字段名称-树形节点名
	 */
	TableTreeNodeName = "sName"

	/**
	 * 数据库表字段名称-创建人
	 */
	TableCreatorName = "sCreator"

	/**
	 * 数据库表字段名称-创建时间
	 */
	TableCreateDateName = "dCreateDate"

	/**
	 * 数据库表字段名称-修改人
	 */
	TableModifiederName = "sModifieder"

	/**
	 * 数据库表字段名称-修改时间
	 */
	TableModifiedDateName = "dModifiedDate"

	/**
	 * 数据库表字段名称-状态值
	 */
	TableStateName = "iState"

	/**
	 * 数据库表字段名称-排序值
	 */
	TableIndexName = "iIndex"

	/**
	 * 数据库表字段名称-版本号
	 */
	TableVersionName = "iVersion"

	/**
	 * 数据库表字段名称-步骤值
	 */
	TableSetpName = "iSetp"

	/**
	 * 数据库表字段名称-唯一标识
	 */
	TableOnlyignName = "sOnlyign"

	/**
	 * 基本数据库库名
	 */
	BaseSystemName = "BaseSystem"

	/**
	 * 字典表表名
	 */
	TableNameDictionary = "Dictionary"

	/**
	 * 字典表值字段名
	 */
	TableDictionaryValueName = "sValue"

	/**
	 * 记录验证串字段名
	 */
	TableRecordKeyName = "sRecordKey"

	/**
	 * 标识字段名
	 */
	TablesSign = "sSign"

	/**
	* 备注字段名
	 */
	TablesMemo = "sMemo"

	/**
	 * 数据库树型表根节点默认值
	 */
	TableTreeRootValue = "00"

	//-------------------------------------------//

	/**
	 * 属性对应数据库表主键名称(字符串形式)
	 */
	GtableMajorKeyString = "GsId"

	/**
	 * 属性对应数据库表主键名称(自增长形式)
	 */
	GtableMajorKeyAutoInt = "GiId"

	/**
	 * 属性对应数据库表主键名称(UUID形式)
	 */
	GtableMajorKeyUuId = "GuId"

	/**
	 * 属性对应数据库表字段名称-上级字段名
	 */
	GtablePidKey = "GsPid"

	/**
	 * 属性对应数据库表字段名称-主键路径名
	 */
	GtablePathKey = "GsPath"

	/**
	 * 属性对应数据库表字段名称-树形节点名
	 */
	GtableTreeNodeName = "GsName"

	/**
	 * 属性对应数据库表字段名称-创建人
	 */
	GtableCreatorName = "GsCreator"

	/**
	 * 属性对应数据库表字段名称-创建时间
	 */
	GtableCreateDateName = "GdCreateDate"

	/**
	 * 属性对应数据库表字段名称-修改人
	 */
	GtableModifiederName = "GsModifieder"

	/**
	 * 属性对应数据库表字段名称-修改时间
	 */
	GtableModifiedDateName = "GdModifiedDate"

	/**
	 * 属性对应数据库表字段名称-状态值
	 */
	GtableStateName = "GiState"

	/**
	 * 属性对应数据库表字段名称-排序值
	 */
	GtableIndexName = "GiIndex"

	/**
	 * 属性对应数据库表字段名称-版本号
	 */
	GtableVersionName = "GiVersion"

	/**
	 * 属性对应数据库表字段名称-步骤值
	 */
	GtableSetpName = "GiSetp"

	/**
	 * 属性对应数据库表字段名称-唯一标识
	 */
	GtableOnlyignName = "GsOnlyign"

	/**
	 * 属性对应数据库树型表根节点默认值
	 */
	GtableTreeRootValue = "00"

	/**
	 * 属性对应基本数据库库名
	 */
	GBaseSystemName = "BaseSystem"

	/**
	 * 属性对应字典表表名
	 */
	GtableNameDictionary = "Dictionary"

	/**
	 * 属性对应字典表值字段名
	 */
	GtableDictionaryValueName = "GsValue"

	/**
	 * 属性对应记录验证串字段名
	 */
	GtableRecordKeyName = "GsRecordKey"

	/**
	 * 属性对应标识字段名
	 */
	GtablesSign = "GsSign"

	/**
	* 属性对应备注字段名
	 */
	GtablesMemo = "GsMemo"
)

// package model

// type GlobalConstant struct{}

// var (
// 	DbNamedRules = "common" //common 通用数据库命名格式定义;bricks 自定义的格式

// 	/**
// 	 * 数据库表主键名称(字符串形式)
// 	 */
// 	TableMajorKeyString = ifelse(DbNamedRules == "bricks", "sId", "s_id")

// 	/**
// 	 * 数据库表主键名称(自增长形式)
// 	 */
// 	TableMajorKeyAutoInt = ifelse(DbNamedRules == "bricks", "iId", "i_id")

// 	/**
// 	 * 数据库表主键名称(UUID形式)
// 	 */
// 	TableMajorKeyUuId = ifelse(DbNamedRules == "bricks", "uId", "u_id")

// 	/**
// 	 * 数据库表字段名称-上级字段名
// 	 */
// 	TablePidKey = ifelse(DbNamedRules == "bricks", "sPid", "s_pid")

// 	/**
// 	 * 数据库表字段名称-主键路径名
// 	 */
// 	TablePathKey = ifelse(DbNamedRules == "bricks", "sPath", "s_path")

// 	/**
// 	 * 数据库表字段名称-树形节点名
// 	 */
// 	TableTreeNodeName = ifelse(DbNamedRules == "bricks", "sName", "s_name")

// 	/**
// 	 * 数据库表字段名称-创建人
// 	 */
// 	TableCreatorName = ifelse(DbNamedRules == "bricks", "sCreator", "s_creator")

// 	/**
// 	 * 数据库表字段名称-创建时间
// 	 */
// 	TableCreateDateName = ifelse(DbNamedRules == "bricks", "dCreateDate", "d_creator_date")

// 	/**
// 	 * 数据库表字段名称-修改人
// 	 */
// 	TableModifiederName = ifelse(DbNamedRules == "bricks", "sModifieder", "s_modifieder")

// 	/**
// 	 * 数据库表字段名称-修改时间
// 	 */
// 	TableModifiedDateName = ifelse(DbNamedRules == "bricks", "dModifiedDate", "d_modified_date")

// 	/**
// 	 * 数据库表字段名称-状态值
// 	 */
// 	TableStateName = ifelse(DbNamedRules == "bricks", "iState", "i_state")

// 	/**
// 	 * 数据库表字段名称-排序值
// 	 */
// 	TableIndexName = ifelse(DbNamedRules == "bricks", "iIndex", "i_index")

// 	/**
// 	 * 数据库表字段名称-版本号
// 	 */
// 	TableVersionName = ifelse(DbNamedRules == "bricks", "iVersion", "i_version")

// 	/**
// 	 * 数据库表字段名称-步骤值
// 	 */
// 	TableSetpName = ifelse(DbNamedRules == "bricks", "iSetp", "i_setp")

// 	/**
// 	 * 数据库表字段名称-唯一标识
// 	 */
// 	TableOnlyignName = ifelse(DbNamedRules == "bricks", "sOnlyign", "s_onlyign")

// 	/**
// 	 * 字典表表名
// 	 */
// 	TableNameDictionary = ifelse(DbNamedRules == "bricks", "Dictionary", "dictionary")

// 	/**
// 	 * 字典表值字段名
// 	 */
// 	TableDictionaryValueName = ifelse(DbNamedRules == "bricks", "sValue", "s_value")

// 	/**
// 	 * 记录验证串字段名
// 	 */
// 	TableRecordKeyName = ifelse(DbNamedRules == "bricks", "sRecordKey", "s_record_key")

// 	/**
// 	 * 标识字段名
// 	 */
// 	TablesSign = ifelse(DbNamedRules == "bricks", "sSign", "s_sign")

// 	/**
// 	 * 备注字段名
// 	 */
// 	TablesMemo = ifelse(DbNamedRules == "bricks", "sMemo", "s_memo")
// )

// const (

// 	/**
// 	 * 数据库树型表根节点默认值
// 	 */
// 	TableTreeRootValue = "00"

// 	/**
// 	 * 基本数据库库名
// 	 */
// 	BaseSystemName = "BaseSystem"

// 	/**
// 	 * 属性对应数据库表主键名称(字符串形式)
// 	 */
// 	GtableMajorKeyString = "GsId"

// 	/**
// 	 * 属性对应数据库表主键名称(自增长形式)
// 	 */
// 	GtableMajorKeyAutoInt = "GiId"

// 	/**
// 	 * 属性对应数据库表主键名称(UUID形式)
// 	 */
// 	GtableMajorKeyUuId = "GuId"

// 	/**
// 	 * 属性对应数据库表字段名称-上级字段名
// 	 */
// 	GtablePidKey = "GsPid"

// 	/**
// 	 * 属性对应数据库表字段名称-主键路径名
// 	 */
// 	GtablePathKey = "GsPath"

// 	/**
// 	 * 属性对应数据库表字段名称-树形节点名
// 	 */
// 	GtableTreeNodeName = "GsName"

// 	/**
// 	 * 属性对应数据库表字段名称-创建人
// 	 */
// 	GtableCreatorName = "GsCreator"

// 	/**
// 	 * 属性对应数据库表字段名称-创建时间
// 	 */
// 	GtableCreateDateName = "GdCreateDate"

// 	/**
// 	 * 属性对应数据库表字段名称-修改人
// 	 */
// 	GtableModifiederName = "GsModifieder"

// 	/**
// 	 * 属性对应数据库表字段名称-修改时间
// 	 */
// 	GtableModifiedDateName = "GdModifiedDate"

// 	/**
// 	 * 属性对应数据库表字段名称-状态值
// 	 */
// 	GtableStateName = "GiState"

// 	/**
// 	 * 属性对应数据库表字段名称-排序值
// 	 */
// 	GtableIndexName = "GiIndex"

// 	/**
// 	 * 属性对应数据库表字段名称-版本号
// 	 */
// 	GtableVersionName = "GiVersion"

// 	/**
// 	 * 属性对应数据库表字段名称-步骤值
// 	 */
// 	GtableSetpName = "GiSetp"

// 	/**
// 	 * 属性对应数据库表字段名称-唯一标识
// 	 */
// 	GtableOnlyignName = "GsOnlyign"

// 	/**
// 	 * 属性对应数据库树型表根节点默认值
// 	 */
// 	GtableTreeRootValue = "00"

// 	/**
// 	 * 属性对应基本数据库库名
// 	 */
// 	GBaseSystemName = "BaseSystem"

// 	/**
// 	 * 属性对应字典表表名
// 	 */
// 	GtableNameDictionary = "Dictionary"

// 	/**
// 	 * 属性对应字典表值字段名
// 	 */
// 	GtableDictionaryValueName = "GsValue"

// 	/**
// 	 * 属性对应记录验证串字段名
// 	 */
// 	GtableRecordKeyName = "GsRecordKey"

// 	/**
// 	 * 属性对应标识字段名
// 	 */
// 	GtablesSign = "GsSign"

//	/**
//	* 属性对应备注字段名
//	 */
//	GtablesMemo = "GsMemo"
// )

// // 三目运算代替品
// func ifelse(bl bool, a, b string) string {
// 	if bl {
// 		return a
// 	}

// 	return b
// }
