package model

import (
	"github.com/cihub/seelog"
)

var isInitEd = 0

func init() {
	SetupLogger()
}

func SetupLogger() {
	if isInitEd == 1 {
		return
	}

	// root, _ := os.Getwd()
	// root = strings.Replace(root, "\\", "/", -1)

	//logger, err := seelog.LoggerFromConfigAsFile(root + "/config/seelog.xml")
	logger, err := seelog.LoggerFromConfigAsBytes(readConfig())
	if err != nil {
		return
	}

	seelog.ReplaceLogger(logger)

	isInitEd = 1
}

/**
 * 读取配置信息
 */
func readConfig() []byte {
	txt := `<seelog levels="trace,debug,info,warn,error,critical">
				<outputs formatid="main">
					<!-- 对控制台输出的Log按级别分别用颜色显示。6种日志级别仅分了三组颜色，如果想每个级别都用不同颜色则需要简单修改即可 -->
					<filter levels="trace,debug">
						<console formatid="colored-default"/>
						<!-- 将日志输出到磁盘文件，按文件大小进行切割日志，单个文件最大10M，最多99个日志文件 -->
						<rollingfile formatid="main" type="size" filename="./logs/debug.log" maxsize="10485760" maxrolls="99" />
					</filter>
					<filter levels="trace,info">
						<console formatid="colored-default"/>
						<!-- 将日志输出到磁盘文件，按文件大小进行切割日志，单个文件最大10M，最多99个日志文件 -->
						<rollingfile formatid="main" type="size" filename="./logs/info.log" maxsize="10485760" maxrolls="99" />
					</filter>
					<filter levels="warn">
						<console formatid="colored-warn"/>
						<!-- 将日志输出到磁盘文件，按文件大小进行切割日志，单个文件最大10M，最多99个日志文件 -->
						<rollingfile formatid="main" type="size" filename="./logs/warn.log" maxsize="10485760" maxrolls="99" />
					</filter>
					<filter levels="error,critical">
						<console formatid="colored-error"/>
						<!-- 将日志输出到磁盘文件，按文件大小进行切割日志，单个文件最大10M，最多99个日志文件 -->
						<rollingfile formatid="main" type="size" filename="./logs/error.log" maxsize="10485760" maxrolls="99" />
					</filter>

				</outputs>
				<formats>
					<format id="colored-default"  format="%EscM(38)%Date %Time [%LEV] %File:%Line | %Msg%n%EscM(0)"/>
					<format id="colored-warn"  format="%EscM(33)%Date %Time [%LEV] %File:%Line | %Msg%n%EscM(0)"/>
					<format id="colored-error"  format="%EscM(31)%Date %Time [%LEV] %File:%Line | %Msg%n%EscM(0)"/>
					<format id="main" format="%Date %Time [%LEV] %File:%Line | %Msg%n"/>
				</formats>
			</seelog>`

	return []byte(txt)
}
