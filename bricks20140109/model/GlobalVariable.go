package model

import (
	"strings"

	Log "github.com/cihub/seelog"
)

type GlobalVariable struct{}

var (
	globalVariable = make(map[string]interface{}) //注册的全局变量集合
)

//注册全局变量
func (gv GlobalVariable) RegisterVariable(name string, data interface{}) interface{} {
	_, ok := globalVariable[name]
	if ok {
		Log.Error("全局变量重复:" + name)
		return data
	}

	globalVariable[name] = data

	return data
}

//取指定全局变量
func (gv GlobalVariable) Get(name string) interface{} {
	name = strings.TrimSpace(name)
	if "" == name {
		return nil
	}

	result, ok := globalVariable[name]
	if !ok {
		return nil
	}

	return result
}

//删除指定全局变量
func (gv GlobalVariable) Del(name string) bool {
	name = strings.TrimSpace(name)
	if "" == name {
		return false
	}

	_, ok := globalVariable[name]
	if !ok {
		return true
	}

	delete(globalVariable, name)

	_, ok = globalVariable[name]
	if !ok {
		return true
	}

	return false
}
